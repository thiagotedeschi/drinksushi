$(document).ready(function () {

    // Expand Panel
    $("#open").toggle(function () {
        $("div#panel").slideDown("slow");
    }, function () {
        $("div#panel").slideUp("slow");
    });

    // Switch buttons from "Log In | Register" to "Close Panel" on click
    $("#toggleFav a").click(function () {
        $("#toggleFav a i").toggleClass("estrelaAmarela");
    });

});