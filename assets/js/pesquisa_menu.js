/**
 * Função encontrada na internet para remover acentuados e substituí-los por equivalentes
 * não acentuados
 * @param String string Texto que tem acentos que precisam ser retirados
 * @returns {String} Texto sem os acentos
 * @since 0.3 23/10/2013
 * @author Desconhecido
 */
function removeAcento(string) {
    var varString = string;
    var stringAcentos = new String('àâêôûãõáéíóúçüÀÂÊÔÛÃÕÁÉÍÓÚÇÜ');
    var stringSemAcento = new String('aaeouaoaeioucuAAEOUAOAEIOUCU');

    var i = new Number();
    var j = new Number();
    var cString = new String();
    var varRes = '';

    for (i = 0; i < varString.length; i++) {
        cString = varString.substring(i, i + 1);
        for (j = 0; j < stringAcentos.length; j++) {
            if (stringAcentos.substring(j, j + 1) == cString) {
                cString = stringSemAcento.substring(j, j + 1);
            }
        }
        varRes += cString;
    }
    return varRes;
}

function buscaMenus(keyCode) {
    var $search_list = $("#pesquisa"); // input com o termo de pesquisa
    $search_list.html('');//limpa a div onde ficam os resultados
    var $my_div = $('#menu'); // div onde a pesquisa será feita
    var my_search = removeAcento($('#searchTerm').val().toLowerCase()); // texto a ser pesquisado: sem maiúsculas ou acentos

    if (my_search.length > 0) {//se o tamanho do termo for maior que 0
        $my_div.slideUp(100);//esconde o menu
        countResult = 0;
        $my_div.find('li').each(function (idx, elem) {//para cada <li> da div onde a pesquisa será feita
            if ($(this).children('ul').length == 0) {//se ele não tiver outros filhos
                $elem = $(elem);//cada <li>
                $text_elem = $elem.children('a').text() + ',' + $elem.children('a').attr('alt');//seleciona o texto do <a> filho dele e as Tags do mesmo
                //limita o número de resultados exibidos pra 10
                if (countResult < 10) {
                    if (removeAcento($text_elem.toLowerCase()).indexOf(my_search) != -1) {//se o texto do <a> tiver o meu termo sem acentos ou maiusculas//
                        countResult++;
                        classePai = $elem.parents('.tipo-modulo').attr('class');
                        $elem.addClass(classePai + ' search-result');
                        $elem.removeClass('tipo-modulo');
                        $elem.removeClass('open');
                        $search_list.append($elem.clone());//clona o elemento e coloca na div de resultados da pesquisa
                    }
                }
            }
        });
        //conta quantos resultados foram exibidos
        totalResult = $search_list.children('li').length;
        $search_list.fadeIn('100');//mostra a div d pesquisa
        if (totalResult == 0) {//se não achar ninguem mostra a li de "No Results"
            $search_list.append('<li class="padding-left-5 text-center" id="not_found"><p>Nenhum Resultado Encontrado <i class="icon-frown-o icon-2x"></i><p></li>')
        }
        else {
            $search_list.find('li #not_found').remove();//se achar, remove a li de "No Results"
        }

        //Se for retornado apenas um resultado na pesquisa e for pressionado enter, abre esse resultado
        if (keyCode == 13 && countResult == 1) {
            $search_list.find('li a')[0].click();
        }
    }
    else//se não tiver nada digitado no input de pesquisa
    {
        $search_list.fadeOut('100')//esconde a div de resultados
        $my_div.slideDown('100');//mostra o menu
    }

}
