var Login = function() {

    var handleLogin = function() {
        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                $('.login-form').submit();
                return false;
            }
        });
    };

    return {
        //main function to initiate the module
        init: function()
        {
            handleLogin();
        },
        validate: function(form)
        {
            $form = $(form);
            $errorContainer = $('#error', $form);
            submit = true;
            $errorContainer.html('');
            $errorContainer.html('').hide();
            $('input.required', $form).each(function()
            {
                if ($(this).val() == '')
                {
                    $errorContainer.show();
                    submit = false;
                    $errorContainer.prepend('<li>Campo ' + $(this).attr('placeholder') + ' Obrigatório</li>');
                }
            });
            return submit;
        }

    };

}();