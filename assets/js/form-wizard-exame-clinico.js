var FormWizard = function () {

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }
            // default form wizard
            $('#exame-clinico').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index) {
                    return true;
                },
                onNext: function (tab, navigation, index) {
                    $('html, body').scrollTop(0);
                },
                onPrevious: function (tab, navigation, index) {
                    $('html, body').scrollTop(0);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#exame-clinico').find('.bar').css({
                        width: $percent - 4 + '%'
                    });
                    if (current >= total) {
                        $('#exame-clinico').find('.button-next').hide();
                        $('#exame-clinico').find('.button-previous').show();
                    }
                    else if (current == 1) {
                        $('#exame-clinico').find('.button-previous').hide();
                        $('#exame-clinico').find('.button-next').show();
                    }
                    else {
                        $('#exame-clinico').find('.button-next').show();
                        $('#exame-clinico').find('.button-previous').show();
                    }
                }
            });
        }

    };

}();