var Data = function () {
    brDatepickerOptions = {
        format: "dd/mm/yyyy",
        language: 'pt-BR',
        autoClose: true
    };

    brDatetimepickerOptions = {
        format: "dd/mm/yyyy hh:mm:ss",
        language: 'pt-BR',
        autoClose: true,
        showSeconds: true
    }

    var initDatePicker = function () {
        $(".date-picker").datepicker(brDatepickerOptions);
    };
    var initTimePicker = function () {
        $(".time-picker").timepicker({
            format: "hh:mm:ss",
            showMeridian: false,
            showSeconds: false,
            defaultTime: false
        });
    };
    var initDatetimePicker = function () {
        $(".datetime-picker").datetimepicker(brDatetimepickerOptions);
    };

    var initDateRangePicker = function () {
        $('.date-range').daterangepicker({
                ranges: {
                    'Hoje': ['today', 'today'],
                    'Ontem': ['yesterday', 'yesterday'],
                    'Este Mês': [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()],
                    'Mês Passado': [Date.today().moveToFirstDayOfMonth().add({
                        months: -1
                    }), Date.today().moveToFirstDayOfMonth().add({
                        days: -1
                    })],
                    'Próximo Mês': [Date.today().moveToFirstDayOfMonth().add({
                        months: +1
                    }), Date.today().add({
                        months: +1
                    }).moveToLastDayOfMonth()],
                    'Próximos Trimestre': [Date.today().moveToFirstDayOfMonth().add({
                        months: +1
                    }), Date.today().add({
                        months: +3
                    }).moveToLastDayOfMonth()],
                    'Próximo Ano': [Date.today().moveToFirstDayOfMonth().add({
                        months: +1
                    }), Date.today().add({
                        years: +1
                    }).moveToLastDayOfMonth()]
                },
                opens: (App.isRTL() ? 'left' : 'right'),
                format: 'dd/MM/yyyy',
                separator: ' à ',
                startDate: Date.today().add({
                    days: -29
                }),
                endDate: Date.today(),
                locale: {
                    applyLabel: 'Inserir',
                    fromLabel: 'De',
                    toLabel: 'Até',
                    customRangeLabel: 'Personalizar',
                    daysOfWeek: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sá'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    firstDay: 1
                },
                showWeekNumbers: true,
                buttonClasses: ['btn-danger']
            },
            function (start, end) {
                $('.date-range').parent('div').children('.date-range-inicio').val(start.toString('yyyy-MM-dd HH:mm:ss'));
                $('.date-range').parent('div').children('.date-range-fim').val(end.toString('yyyy-MM-dd 23:59:59'));
            });

    };

    return{
        init: function () {
            initDatePicker();
            initTimePicker();
            initDatetimePicker();
            initDateRangePicker();
        }
    }
}
