/**
 * Funções disparadas toda vez que uma tabela sofre um update via Ajax
 * O objetivo é baixar um JSON com todos os flashes, e em seguida
 * renderizar os flashes que tenham sido disparados para o usuário em ações 
 * como exclusão, inserção ou alterações.
 */
function getFlashes()
{
    $.ajax({
        url: urlFlashes,
        type: 'GET',
        dataType: 'json',
        success: function(json) {
            displayFlashes(json);
        }
    });
}
function displayFlashes(jFlashes)
{
    for (var type in jFlashes)
    {
        thisType = type;
        for (var message in jFlashes[type])
            toastr[thisType](jFlashes[type][message]);
    }
}