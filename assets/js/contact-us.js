var ContactUs = function() {

    return {
        //main function to initiate the module
        init: function() {
            var map;
            $(document).ready(function() {
                map = new GMaps({
                    div: '#map',
                    lat: -21.7598,
                    lng: -43.351392
                });
                var marker = map.addMarker({
                    lat: -21.7598,
                    lng: -43.351392,
                    title: 'Skytronic soluções em TI',
                    infoWindow: {
                        content: "<b>Skytronic soluções em TI</b> Av. Dr. Paulo Japiassu Coelho, 714. Juiz de Fora, MG, Brasil"
                    }
                });

                marker.infoWindow.open(map, marker);
            });
        }
    };

}();