
$('.grid-view-export').click(function () {
    from = $(this).attr('from');
    target = $(this).attr('target');
    url = $(this).attr('href');
    $from = $('#' + from)
    selectedIds = $.fn.yiiGridView.getSelection(from)
    var selectedRows = [];
    //se houver registros marcados, selecionar apenas as rows relacionadas
    if (selectedIds.length > 0) {   //função parecida com a getSelection(from) padrão, mas retorna a row toda e não o ID correspondente
        keys = $from.find('.keys span'),
            selection = [];
        $from.find('table').children('tbody').children().each(
            function (i) {
                if ($(this).hasClass('selected')) {
                    row = $.fn.yiiGridView.getRow(from, i);
                    data = [];
                    $(row, 'td').not('.checkbox-column').each(function (j) {
                        data[j] = $(this).text();
                    });
                    selectedRows[i] = data;
                }
            });
    }
    else {//se não houverem registros selecionados, imprimir todos daquela página
        row = [0]
        for (i = 0; row.length > 0; i++) {
            row = $.fn.yiiGridView.getRow(from, i)
            if (row.length > 0) {
                data = []
                $(row, 'td').not('.checkbox-column,.botao-group ').each(function (j) {
                    data[j] = $(this).text();
                });
                selectedRows[i] = data;
            }
        }
    }
    //pegar o cabeçalho, que possui as informações necessárias para organizar o documento de saida
    head = $from.find('table').children('thead').children('tr:first');
    header = [];
    $('th', head).not('.checkbox-column, .button-column').each(function (l) {
        header[l] = $(this).text();
    })

    //pegar os filtros, que serão bem úteis em alguns relatórios
    filt = $from.find('table').children('thead').children('tr.filters');
    filter = [];
    $('td input, td select', filt).each(function (k) {

        if ($(this).is('select')) {
            filter[k] = $(':selected', this).text();
        }
        else {
            filter[k] = $(this).val();
        }

    })

    today = new Date()
    date = today.getDate() + "-" + (today.getMonth() + 1) + "-" + today.getFullYear()

    title = prompt('Digite o nome do documento a ser gerado:', 'Relatório ' + $from.attr('title') + ' [' + date + ']')
    //enviar via ajax os dados coletados
    $.ajax({
        url: url,
        type: 'POST',
        beforeSend: function () {
            if (title === null)
                return false;
            $(".botao-group .open").removeClass("open");
            $("#" + from).addClass("grid-view-loading")
            return true;
        },
        success: function (data) {
            if (data == 'erro')
                alert('Ocorreu um problema na exportação. Recarregue a página e tente novamente mais tarde');
            $frame = $('<iframe />', {
                name: 'export',
                id: 'export'
            }).appendTo('body');
            $frame.attr('src', data);
            $("#" + from).removeClass("grid-view-loading")
        },
        data: {
            from: from,
            target: target,
            header: header,
            filter: filter,
            rows: selectedRows,
            title: title
        },
        error: function () {
            alert('Ocorreu um problema na exportação. Recarregue a página e tente novamente mais tarde')
            $("#" + from).removeClass("grid-view-loading")
        }
    });

    return false;
})