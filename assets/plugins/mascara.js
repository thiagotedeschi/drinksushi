jQuery(function () {

    var is_9_digitos;

    // array com ddds de tamnaho 10 (9 digitos + '-')
    var ddd9Digitos = new Array("11", "12", "13", "14", "15", "16", "17", "18", "19", "21", "22", "24", "27", "28");

    $(document).on('keyup', '.date-picker', function () {
        var valor = $(this).val();
        var novoValor = DataMask(valor);
        var tam_data = jQuery(this).val().length;
        var qtd_num_data = 10;

        if (tam_data > qtd_num_data) {
            novoValor = valor.substr(0, qtd_num_data);
        }

        $(this).val(novoValor);
    });

    $(document).on('keyup', '.telefone-mask', function () {

        var dddVal = jQuery(this).prev().val();
        var tam_tel = jQuery(this).val().length;
        var valor = jQuery(this).val();
        var novoValor;

        var qtd_num_tel = 9;
        //caso seja uma cidade onde o telefone tem um número a mais, acrescento um digito na validação
        if (ddd9Digitos.indexOf(dddVal) != -1) {
            // caso o ddd digitado esteja entre os que o tel tem 9 digitos
            qtd_num_tel = 10;
            novoValor = TelefoneMaskUp(valor);
        } else {
            novoValor = TelefoneMask(valor);
        }

        if (tam_tel >= qtd_num_tel) {
            novoValor = valor.substr(0, qtd_num_tel);
            //Quando o campo Telefone atinge o tamanho maximo de caracteres muda o foco para Ramal
            jQuery(this).next().focus();
        }

        jQuery(this).val(novoValor);
    });


    // em caso de alteração do campo ddd, salvo o valor no campo ddd antes de haver alteração nesse campo, para depois comparar com o novo valor
    // e apagar o campo tel e ramal, caso o telefone não tenha a mesma qtd de dígitos do telefone inserido anteriormente

    $(document).on('focusin', '.mask_ddd', function () {

        if (ddd9Digitos.indexOf(jQuery(this).val()) != -1) {
            // caso o ddd digitado esteja entre os que o tel tem 9 digitos
            is_9_digitos = true;
        } else {
            is_9_digitos = false;
        }

    });

    $(document).on('change', '.mask_ddd', function () {

        var isdig;

        if (ddd9Digitos.indexOf(jQuery(this).val()) == -1) {
            isdig = false;
        } else {
            isdig = true;
        }

        if ((isdig && !is_9_digitos) || (!isdig && is_9_digitos)) {
            var tel_mask = $(this).next();
            tel_mask.val("");
            tel_mask.next().val("");
        }
    });

    $(document).on('keyup', '.formata_moeda', function () {
        var valor = $(this).val();
        var novoValor = moeda(valor);
        $(this).val(novoValor);
    });

    $(document).on('keyup', '.formata_moedaPonto', function () {
        var valor = $(this).val();
        var novoValor = moedaPonto(valor);
        $(this).val(novoValor);
    });


});


function DataMask(v) {
    v = v.replace(/\D/g, "")
    v = v.replace(/(\d{2})(\d)/, "$1/$2")
    v = v.replace(/(\d{2})(\d)/, "$1/$2")
    return v
}

function TelefoneMask(v) {
    v = v.replace(/\D/g, "")
    v = v.replace(/(\d{4})(\d)/, "$1-$2")
    return v
}
function TelefoneMaskUp(v) {
    v = v.replace(/\D/g, "")
    v = v.replace(/(\d{5})(\d)/, "$1-$2")
    return v
}

function TelEDDDMask(v) {
    v = v.replace(/\D/g, "")
    v = v.replace(/^(\d\d)(\d)/g, "($1) $2")
    v = v.replace(/(\d{4})(\d)/, "$1-$2")
    return v
}

function moeda(v) {
    v = v.replace(/\D/g, "") // permite digitar apenas numero
    v = v.replace(/(\d{1})(\d{15})$/, "$1.$2") // coloca ponto antes dos ultimos digitos
    v = v.replace(/(\d{1})(\d{11})$/, "$1.$2") // coloca ponto antes dos ultimos 13 digitos
    v = v.replace(/(\d{1})(\d{8})$/, "$1.$2") // coloca ponto antes dos ultimos 10 digitos
    v = v.replace(/(\d{1})(\d{5})$/, "$1.$2") // coloca ponto antes dos ultimos 7 digitos
    v = v.replace(/(\d{1})(\d{1,2})$/, "$1,$2") // coloca virgula antes dos ultimos 4 digitos
    return v;
}

function moedaPonto(v) {
    v = v.replace(/\D/g, "") // permite digitar apenas numero
    v = v.replace(/(\d{1})(\d{15})$/, "$1.$2") // coloca ponto antes dos ultimos digitos
    v = v.replace(/(\d{1})(\d{11})$/, "$1.$2") // coloca ponto antes dos ultimos 13 digitos
    v = v.replace(/(\d{1})(\d{8})$/, "$1.$2") // coloca ponto antes dos ultimos 10 digitos
    v = v.replace(/(\d{1})(\d{5})$/, "$1.$2") // coloca ponto antes dos ultimos 7 digitos
    v = v.replace(/(\d{1})(\d{1,2})$/, "$1.$2") // coloca virgula antes dos ultimos 4 digitos
    return v;
}
