<?php
Yii::import("xupload.actions.XUploadAction");
class UploadCapituloAction extends XUploadAction
{

    protected function handleDeleting()
    {
        if (isset($_GET["_method"]) && $_GET["_method"] == "delete") {
            $success = false;
            if ($_GET["file"][0] !== '.' && Yii::app()->user->hasState($this->stateVariable)) {
                // pull our userFiles array out of state and only allow them to delete
                // files from within that array
                $userFiles = Yii::app()->user->getState($this->stateVariable, array());
                if ($this->fileExists($userFiles[$_GET["file"]])) {
                    $success = $this->deleteFile($userFiles[$_GET["file"]]);
                    if ($success) {
                        rmdir($userFiles[$_GET["file"]]['dir']);
                        unset($userFiles[$_GET["file"]]); // remove it from our session and save that info
                        Yii::app()->user->setState($this->stateVariable, $userFiles);
                    }
                }
            }
            echo json_encode($success);
            return true;
        }
        return false;
    }


    protected function handleUploading()
    {
        $this->init();
        $model = $this->formModel;
        $model->{$this->fileAttribute} = CUploadedFile::getInstance($model, $this->fileAttribute);

        if ($model->{$this->fileAttribute} !== null) {
            $model->{$this->mimeTypeAttribute} = $model->{$this->fileAttribute}->getType();
            $model->{$this->sizeAttribute} = $model->{$this->fileAttribute}->getSize();
            $model->{$this->displayNameAttribute} = $model->{$this->fileAttribute}->getName();
            $model->{$this->fileNameAttribute} = $model->{$this->displayNameAttribute};

            $imagemCapituloNR = new ImagemCapituloNR;
            $imagemCapituloNR->attributes = $_REQUEST['ImagemCapituloNR'][$model->{$this->displayNameAttribute}];

            if ($model->validate()) {

                $path = $this->getPath();

                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                    chmod($path, 0777);
                }

                $model->{$this->fileAttribute}->saveAs($path . $model->{$this->fileNameAttribute});
                chmod($path . $model->{$this->fileNameAttribute}, 0777);

                $returnValue = $this->beforeReturn();

                if ($returnValue === true) {
                    $imagemCapituloNR->imagem_capituloNR = base64_encode(
                        file_get_contents($path . $model->{$this->fileNameAttribute})
                    );
                    if ($imagemCapituloNR->validate()) {
                        echo json_encode(
                            array(
                                array(
                                    "name" => $model->{$this->displayNameAttribute},
                                    "type" => $model->{$this->mimeTypeAttribute},
                                    "size" => $model->{$this->sizeAttribute},
                                    "url" => $this->getFileUrl($model->{$this->fileNameAttribute}),
                                    "titulo" => $imagemCapituloNR->titulo_imagemCapituloNR,
                                    "legenda" => $imagemCapituloNR->legenda_imagemCapituloNR,
                                    "imagem" => $imagemCapituloNR->imagem_capituloNR,
                                    "path" => $path,
                                    "thumbnail_url" => $model->getThumbnailUrl($this->getPublicPath()),
                                    "delete_url" => $this->getController()->createUrl(
                                            $this->getId(),
                                            array(
                                                "_method" => "delete",
                                                "file" => $model->{$this->fileNameAttribute},
                                            )
                                        ),
                                    "delete_type" => "POST"
                                )
                            )
                        );
                    } else {
                        $error = "";
                        foreach ($imagemCapituloNR->getErrors() as $attribute) {
                            $error .= implode(", ", $attribute) . ' ';
                        }
                        echo json_encode(
                            array(
                                array(
                                    "name" => $model->{$this->displayNameAttribute},
                                    "type" => $model->{$this->mimeTypeAttribute},
                                    "size" => $model->{$this->sizeAttribute},
                                    "url" => $this->getFileUrl($model->{$this->fileNameAttribute}),
                                    "titulo" => $imagemCapituloNR->titulo_imagemCapituloNR,
                                    "legenda" => $imagemCapituloNR->legenda_imagemCapituloNR,
                                    "imagem" => $imagemCapituloNR->imagem_capituloNR,
                                    "path" => $path,
                                    "thumbnail_url" => $model->getThumbnailUrl($this->getPublicPath()),
                                    "delete_url" => $this->getController()->createUrl(
                                            $this->getId(),
                                            array(
                                                "_method" => "delete",
                                                "file" => $model->{$this->fileNameAttribute},
                                            )
                                        ),
                                    "delete_type" => "POST",
                                    "error" => $error
                                )
                            )
                        );
                    }
                } else {
                    echo json_encode(
                        array(
                            array(
                                "error" => $returnValue,
                                "titulo" => $imagemCapituloNR->titulo_imagemCapituloNR,
                                "legenda" => $imagemCapituloNR->legenda_imagemCapituloNR,
                                "path" => $path,
                            )
                        )
                    );
                }
            } else {
                echo json_encode(
                    array(
                        array(
                            "error" => $model->getErrors($this->fileAttribute),
                            "titulo" => $imagemCapituloNR->titulo_imagemCapituloNR,
                            "legenda" => $imagemCapituloNR->legenda_imagemCapituloNR,
                        )
                    )
                );
            }
        } else {
            throw new CHttpException(500, "Could not upload file");
        }
    }

} 