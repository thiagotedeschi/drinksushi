<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding-bottom:20px;">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
                    <tr>
                        <td valign="top" class="bodyContent">
                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top">
                                        <h2 class="h2">Contato pelo sistema</h2>
                                        <br/>

                                        <div class="textdark">
                                            Caro(a) Colega,<br/><br/>
                                            <b><?= $prof->nome_profissional; ?></b> acaba de enviar entrar em contato
                                            pelo formulário do <?= HAtributos::getGlobal('NOME_SISTEMA'); ?>:
                                            <br/><br/>
                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                <tr>
                                                    <div
                                                        style="background-color: #E8E8E8 ; padding: 7px; font-size: 110%"><?= $mensagem ?></div>
                                                </tr>
                                            </table>
                                            <br/><br/>
                                            Para obter mais informações sobre a notificação acima, entre no sistema ou
                                            em contato com o <a
                                                href="mailto:<?= HAtributos::getLocal('ADMIN_EMAIL') ?>">administrador
                                                do sistema</a>.

                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%"
       style="background-color:#f8f8f8;border-top:1px solid #e7e7e7;border-bottom:1px solid #e7e7e7;">
    <tr>
        <td>

        </td>
    </tr>
</table>
