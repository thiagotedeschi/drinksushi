<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding-bottom:20px;">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
                    <tr>
                        <td valign="top" class="bodyContent">
                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top">
                                        <h2 class="h2">Troca de Senha</h2>
                                        <br/>

                                        <div class="textdark">
                                            Caro(a) <?= $prof->nome_profissional; ?>,<br/><br/>
                                            Sua senha foi trocada com sucesso. Você pode alterá-la a qualquer momento em
                                            seu Perfil.
                                            Login: <b><?= $usuario->login_usuario ?></b> <br/>
                                            Senha: <b><?= $senha ?></b><span style="color:red;"> (gerada automaticamente)</span>
                                            <br/>
                                            <br/>
                                            Caso Você não saiba do que este email se trata, ou acredita que esta ação
                                            foi indevida, por favor entre em contato com o administrador do sistema.
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%"
       style="background-color:#f8f8f8;border-top:1px solid #e7e7e7;border-bottom:1px solid #e7e7e7;">
    <tr>
        <td>
            <center>
                <table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
                    <tr>
                        <td valign="top" style="padding:20px;">
                            <h2>Instruções Extras</h2>
                            <br/>

                            <div class="textdark">
                                <strong>Mudança de Senha: </strong>
                                Sua conta é sua identidade no sistema. Tendo acesso à ela, qualquer pessoa na internet
                                pode
                                executar todas as ações delegadas a você. Para evitar que isso aconteça, matenha sua
                                senha segura e sempre atualizada. Evite
                                sequências óbvias como "mudar123", "123456" e "senha123". Se você suspeitar que sua
                                senha tenha sido vazada, troque-a no seu perfil imediatamente.
                                <br/><br/>
                                <strong>Obtendo Ajuda: </strong>
                                Apesar dos esforços para que o sistema seja de fácil compreensão, sempre existirão
                                situações
                                que você poderá ficar com dúvidas em como proceder. Para isso, você pode entrar em
                                contato com
                                o suporte do sistema ou buscar a solução em nosso FAQ (perguntas frequentes.) Para isso,
                                entre no sistema, vá no menu superior direito e clique na opção <b>Ajuda</b>.
                                <br/><br/>
                            </div>
                            <br/>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
