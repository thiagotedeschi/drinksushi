<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding-bottom:20px;">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
                    <tr>
                        <td valign="top" class="bodyContent">
                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top">
                                        <h2 class="h2">Você Recebeu uma Notificação</h2>
                                        <br/>

                                        <div class="textdark">
                                            Caro(a) <?= $prof->nome_profissional; ?>,<br/><br/>
                                            Você acaba de receber uma notificação gerada automaticamente pelo sistema,
                                            com informações relevantes à sua utilização:
                                            <br/><br/>
                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                <tr>
                                                    <div
                                                        style="background-color: #E8E8E8 ; padding: 7px; font-size: 110%"><?= $notificacao->conteudo_notificacao; ?></div>
                                                </tr>
                                            </table>
                                            <br/><br/>
                                            Para obter mais informações sobre a notificação acima, entre no sistema ou
                                            em contato com o <a
                                                href="mailto:<?= HAtributos::getLocal('ADMIN_EMAIL') ?>">administrador
                                                do sistema</a>.

                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%"
       style="background-color:#f8f8f8;border-top:1px solid #e7e7e7;border-bottom:1px solid #e7e7e7;">
    <tr>
        <td>
            <center>
                <table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
                    <tr>
                        <td valign="top" style="padding:20px;">
                            <h2>Instruções Extras</h2>
                            <br/>

                            <div class="textdark">
                                <strong>Obtendo Ajuda: </strong>
                                Apesar dos esforços para que o sistema seja de fácil compreensão, sempre existirão
                                situações
                                que você poderá ficar com dúvidas em como proceder. Para isso, você pode entrar em
                                contato com
                                o suporte do sistema ou buscar a solução em nosso FAQ (perguntas frequentes.) Para isso,
                                entre no sistema, vá no menu superior direito e clique na opção <b>Ajuda</b>.
                                <br/><br/>
                            </div>
                            <br/>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
