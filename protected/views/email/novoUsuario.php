<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding-bottom:20px;">
            <center>
                <table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
                    <tr>
                        <td valign="top" class="bodyContent">
                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                <tr>
                                    <td valign="top">
                                        <h2 class="h2">Novo Usuário Criado</h2>
                                        <br/>

                                        <div class="textdark">
                                            Caro(a) <?= $prof->nome_profissional; ?>,<br/><br/>
                                            Sua conta no <b><?= HAtributos::getGlobal('NOME_SISTEMA') ?></b> foi criada.
                                            Agora, você pode utilizar as funcionalidades delegadas a você.<br/>
                                            Para acessar o sistema, por favor acesse o link <a href="<?= MAIN_LINK ?>"
                                                                                               target="_blank"><?= MAIN_LINK ?></a>,
                                            e utilize os seguintes dados para acesso:<br/>
                                            Login: <b><?= $usuario->login_usuario ?></b> <br/>
                                            Senha: <b><?= $senha ?></b><span style="color:red;"></span>
                                            <br/>
                                            <br/><br/>
                                            Você pode trocar sua senha a qualquer momento no seu Perfil.
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%"
       style="background-color:#f8f8f8;border-top:1px solid #e7e7e7;border-bottom:1px solid #e7e7e7;">
    <tr>
        <td>
            <center>
                <table border="0" cellpadding="0" cellspacing="0" width="600px" style="height:100%;">
                    <tr>
                        <td valign="top" style="padding:20px;">
                            <h2>Instruções Extras</h2>
                            <br/>

                            <div class="textdark">
                                <strong>Mudança de Senha: </strong>
                                Sua conta é sua identidade no sistema. Tendo acesso à ela, qualquer pessoa na internet
                                pode
                                executar todas as ações delegadas a você. Para evitar que isso aconteça, matenha sua
                                senha segura e sempre atualizada. Evite
                                sequências óbvias como "mudar123", "123456" e "senha123". Se você suspeitar que sua
                                senha tenha sido vazada, troque-a no seu perfil imediatamente.
                                <br/><br/>
                                <strong>Obtendo Ajuda: </strong>
                                Existem diversas formas para encontrar suporte. para conhecê-las,
                                entre no sistema, vá no menu superior direito e clique na opção <b>Ajuda</b>.
                                <br/><br/>
                            </div>
                            <br/>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
