<?php
/* @var $this ProfissionalController */
/* @var $model Profissional */
/* @var $form CActiveForm */
$model = $profissional;
$nome_profissao = null;
$responsabilidades = null;
if (!is_null($model->IDProfissao)) {
    $nome_profissao = $model->iDProfissao->nome_profissao;
    $responsabilidades = $model->iDProfissao->viewResponsabilidades();
}
?>
<div id="tab_1-1" class="tab-pane active">
<div style="height: auto;" id="accordion1-1" class="accordion collapse">

<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'profissional-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'action' => $this->createUrl('updateProfissional', array('id' => $model->IDProfissional)),
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
<p class="note note-warning">Campos com * são obrigatórios.</p>
<?php echo $form->errorSummary($model); ?>
<div class="control-group">
    <?php echo $form->labelEx($model, 'nome_profissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $model,
            'nome_profissional',
            array('class' => 'm-wrap span6', 'maxlength' => 255)
        ); ?>
        <?php echo $form->error($model, 'nome_profissional', array('class' => 'help-inline')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'email_profissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $model,
            'email_profissional',
            array('class' => 'm-wrap span6', 'maxlength' => 60)
        ); ?>
        <?php echo $form->error($model, 'email_profissional', array('class' => 'help-inline')); ?>
    </div>

</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'receber_emails', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->checkBox($model, 'receber_emails'); ?>
        <?php echo $form->error($model, 'receber_emails', array('class' => 'help-inline')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'cpf_profissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php
        $this->widget(
            'CMaskedTextField',
            array(
                'model' => $model,
                'attribute' => 'cpf_profissional',
                'mask' => HTexto::MASK_CPF,
                'htmlOptions' => array('class' => 'span6 m-wrap')
            )
        );
        ?>
        <?php echo $form->error($model, 'cpf_profissional', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'rg_profissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $model,
            'rg_profissional',
            array('class' => 'm-wrap span6', 'maxlength' => 15)
        ); ?>
        <?php echo $form->error($model, 'rg_profissional', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'genero_profissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->dropDownList(
            $model,
            'genero_profissional',
            ['M' => 'Masculino', 'F' => 'Feminino'],
            array('class' => 'm-wrap span3', 'maxlength' => 1)
        ); ?>
        <?php echo $form->error($model, 'genero_profissional', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'IDProfissao', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo '<span class="muted">' . $nome_profissao . '</span>'; ?>
        <?php echo $form->error($model, 'IDProfissao', array('class' => 'help-inline')); ?>
    </div>
</div>
<?php
//            $resps = $profissional->profissionalResponsabilidades(array('condition' => 'dt_inicio < now() AND (dt_desativacao IS NULL OR dt_desativacao > now())'));
//            $stringResps = '';
//            foreach ($resps as $resp)
//            {
//                $stringResps .= $resp->iDResponsabilidade()->nome_responsabilidade . ',';
//            }
?>

<div class="control-group">
    <?php echo $form->labelEx($model, 'responsabilidadeProfissaos', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo '<span class="muted">' . $responsabilidades . '</span>'; ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($model, 'org_classeProfissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $model,
            'org_classeProfissional',
            array('class' => 'm-wrap span6', 'maxlength' => 15)
        ); ?>
        <?php echo $form->error($model, 'org_classeProfissional', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'n_registroProfissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $model,
            'n_registroProfissional',
            array('class' => 'm-wrap span6', 'maxlength' => 10)
        ); ?>
        <?php echo $form->error($model, 'n_registroProfissional', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'agencia_profissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $model,
            'agencia_profissional',
            array('class' => 'm-wrap span6', 'maxlength' => 10)
        ); ?>
        <?php echo $form->error($model, 'agencia_profissional', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'conta_profissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $model,
            'conta_profissional',
            array('class' => 'm-wrap span6', 'maxlength' => 10)
        ); ?>
        <?php echo $form->error($model, 'conta_profissional', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'variacao', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'variacao', array('class' => 'm-wrap span6', 'maxlength' => 3)); ?>
        <?php echo $form->error($model, 'variacao', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'IDBanco', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->dropDownList(
            $model,
            'IDBanco',
            Html::listdata(Banco::model()->findAll(), 'IDBanco', 'nome_banco'),
            array('class' => 'm-wrap span6 select2')
        ); ?>
        <?php echo $form->error($model, 'IDBanco', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'inscricao_mteProfissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $model,
            'inscricao_mteProfissional',
            array('class' => 'm-wrap span6', 'maxlength' => 15)
        ); ?>
        <?php echo $form->error($model, 'inscricao_mteProfissional', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'telefone_profissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php
        $this->widget(
            'application.widgets.WCampoTelefone',
            array(
                'model' => $model,
                'form' => $form,
                'html_optionsDDD' => array('class' => 'span2'),
                'html_optionsTel' => array('class' => 'span3'),
                'html_optionsRamal' => array('class' => 'span2'),
                'atributoDDD' => 'ddd_telefoneProfissional',
                'atributoTel' => 'telefone_profissional',
                'atributoRamal' => 'ramal_profissional',
            )
        )
        ?>
        <?php echo $form->error($model, 'telefone_profissional', array('class' => 'help-inline')); ?>
    </div>
</div>


<div class="control-group">
    <?php echo $form->labelEx($model, 'celular_profissional', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php
        $this->widget(
            'application.widgets.WCampoTelefone',
            array(
                'model' => $model,
                'form' => $form,
                'html_optionsDDD' => array('class' => 'span2'),
                'html_optionsTel' => array('class' => 'span3', 'placeholder' => 'Celular'),
                'atributoDDD' => 'ddd_celularProfissional',
                'atributoTel' => 'celular_profissional',
            )
        )
        ?>
        <?php echo $form->error($model, 'celular_profissional', array('class' => 'help-inline')); ?>
    </div>
</div>


<?php
$formEnderecoC = $this->widget(
    'application.widgets.WFormEndereco',
    array(
        'endereco' => $model->iDEndereco(),
        'form' => $form,
        'label' => $model->attributeLabels()['IDEndereco'],
        'id' => '1'
    )
);
?>

<div class="form-actions">
    <?php echo Html::submitButton('Salvar Alterações', array('class' => 'botao')); ?>
</div>
</div>
<?php $this->endWidget(); ?>
</div>
</div>
