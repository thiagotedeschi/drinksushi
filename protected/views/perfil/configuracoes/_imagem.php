<style>
    .bar {
        height: 2px;
        background: green;
        margin-top: 15px;
        width: 0%;
    }

    #modal-submit {
        float: right;
    }
</style>


<div id="alterar_imagem" class="modal modal-allow-scroll hide container fade" tabindex="-1"
     style="position: absolute;  overflow-x: auto; ">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div clas="col-md-12">
                <p>A imagem de perfil será utilizada em diversas partes do sistema para identificar este Usuário!</p>

                <div class="space10"></div>

                <input id="upload_foto" type="file" name="foto_perfil"
                       data-url="<?= Yii::app()->createUrl('Administracao/Usuario/uploadFoto') ?>" class="">
                <input type="hidden" id="x" name="x" value="0"/>
                <input type="hidden" id="y" name="y" value="0"/>
                <input type="hidden" id="w" name="w" value="245"/>
                <input type="hidden" id="h" name="h" value="245"/>
                <input type="hidden" id="imageName" name="imageName" value=""/>

                <p>
                    <span class="label label-warning">Obs:</span>&nbsp;
                    A imagem deve ter no máximo um 1mb e as dimensões 1024x768px.
                </p>

                <div style="margin-left:100px;">
                    <div id="progress">
                        <div class="bar"></div>
                    </div

                    <p id="error_upload" class="hidden">
                        <span id="error_upload_span"></span>
                    </p>

                    <div id="cropper_foto"></div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div id="modal-submit"></div>
    </div>
</div>