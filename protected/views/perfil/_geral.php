<script>
    $(document).ready(function () {
        getFlashes();
    })
</script>

<style>
    .male-profile #nome_profissional {
        color: #57B5E3;
    }

    .female-profile #nome_profissional {
        color: #f24d9a;
    }

    .female-profile .sale-summary li .sale-num {
        color: #f24d9a;
    }

    .female-profile ul.profile-nav a span {
        background: #f24d9a;
    }

    .female-profile ul.profile-nav a:hover span {
        background: #f22883;
    }

    .female-profile ul.profile-nav li a {
        color: #f28abe;
        background: #faefff;
        border-left-color: #f28abe;
    }

    .female-profile ul.profile-nav li a.margin-top-10:hover {
        color: #f24d9a;
        background: #fee5ff;
        border-left-color: #f22883;
    }
</style>

<div class="tab-pane row-fluid active <?= $profissional->classePerfil ?>" id="tab_1_1">
    <ul class="unstyled profile-nav span3" style="margin-bottom: 10px">
        <li style="text-align: center">
            <a data-toggle="modal" href="#alterar_imagem" class="profile-edit">Alterar Foto</a>
            <img src="<?= Usuario::model()->findByPk(Yii::app()->user->IDUsuario)->getFoto() ?>"
                 width="255" alt=""/>
        </li>

        <li><a class="margin-top-10">Mensagens<span><?= count($usuario->mensagensRecebidas); ?></span></a></li>
    </ul>
    <div class="span9 float-right " style="float: right;">
        <div class="row-fluid">
            <div class="span8 profile-info">
                <h1 id="nome_profissional"><?= $profissional->nome_profissional ?></h1>
            </div>
            <!--end span8-->

            <!--end span4-->
        </div>
        <!--end row-fluid-->
        <div class="tabbable tabbable-custom tabbable-custom-profile">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1_22" data-toggle="tab">Notificações</a></li>
                <li><a href="#tab_1_11" data-toggle="tab">Últimos Logins</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane " id="tab_1_11">
                    <div class="portlet-body" style="display: block;">
                        <div class="scroller" data-height="500px" data-always-visible="1" data-rail-visible1="1">
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                <tr>
                                    <th><i class="icon-calendar"></i> Data</th>
                                    <th class="hidden-phone"><i class="icon-globe"></i> Endereço de IP</th>
                                    <th><i class="icon-flag-checkered"></i> Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $historico = $usuario->historicoLogins(
                                    array('order' => '"dt_historicoLogin" DESC', 'limit' => '20')
                                );
                                foreach ($historico as $key => $value) {
                                    ?>
                                    <tr>
                                        <td><?=
                                            HData::dataInteligente(
                                                $value->dt_historicoLogin
                                            ) . ' -<i> ' . HData::formataData(
                                                $value->dt_historicoLogin,
                                                HData::BR_TIME_FORMAT
                                            ) ?></i></td>
                                        <td><?= $value->ip_historicoLogin ?></td>
                                        <td><?= $value->error_historicoLogin == '0' ? '<span class="label label-success">Sucesso</span>' : '<span class="label label-warning">Falha</span>' ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--tab-pane-->
                <div class="tab-pane active" id="tab_1_22">
                    <div class="tab-pane active" id="tab_1_1_1">
                        <div class="scroller" data-height="500px" data-always-visible="1" data-rail-visible1="1">
                            <ul class="feeds">
                                <?php
                                $this->widget(
                                    'application.widgets.WNotificacoes',
                                    array(
                                        'view' => 'NotificacaoPerfil'
                                    )
                                );
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--tab-pane-->
            </div>
        </div>
    </div>

    <!--end span9-->
    <div class="span3 float-left margin-0" style="padding-top: 20px !important; float: left;">
        <div class="portlet sale-summary">
            <div class="portlet-title">
                <div class="caption">Informações</div>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body">
                <ul class="unstyled">
                    <li>
                        <span class="sale-info">Profissional:</span>
                        <span class="sale-num"><a href="<?=
                            $this->createUrl(
                                '/Profissional/profissional/view&id='
                            ) . $profissional->IDProfissional ?>"><?= $profissional->nome_profissional ?></a></span>
                    </li>
                    <li>
                        <span class="sale-info">Ativo:</span>
                        <span class="sale-num"><?= $usuario->ativo_usuario ? 'Sim' : 'Não' ?></span>
                    </li>
                    <li>
                        <span class="sale-info">Administrador:</span>
                        <span class="sale-num"><?= $usuario->admin_usuario ? 'Sim' : 'Não' ?></span>
                    </li>
                    <li>
                        <span class="sale-info">Último Login</span>
                        <span class="sale-num"><?=
                            HData::dataHoraInteligente(
                                $historico[0]->dt_historicoLogin
                            ) ?></span>
                    </li>
                    <li>
                        <span class="sale-info">Criação:</span>
                        <span class="sale-num"><?= HData::dataHoraInteligente($usuario->dt_criacaoUsuario) ?></span>
                    </li>
                    <li>
                        <span class="sale-info">Poder de Concessão:</span>
                        <span class="sale-num"><?= $usuario->grant_usuario ? 'Sim' : 'Não' ?></span>
                    </li>
                    <li>
                        <span class="sale-info">Última troca de senha:</span>
                        <span class="sale-num"><?=
                            $usuario->dt_ultimaTrocaSenhaUsuario != '' ? HData::dataInteligente(
                                $usuario->dt_ultimaTrocaSenhaUsuario
                            ) : '<span class="label label-warning">Nunca</span>' ?></span>
                    </li>
                    <li>
                        <span class="sale-info">Próxima Troca de senha:</span>
                        <span class="sale-num"><?= $usuario->getLabelTempoSenhaExpirar(); ?></span>
                    </li>

                </ul>
            </div>
        </div>
    </div>

</div>


<link href="<?= ASSETS_LINK ?>/plugins/jcrop/css/jquery.Jcrop.min.css" rel="stylesheet"/>
<link href="<?= ASSETS_LINK ?>/css/pages/image-crop.css" rel="stylesheet"/>
<link href="<?= ASSETS_LINK ?>/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet"/>
<script src="<?= ASSETS_LINK ?>/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
<script src="<?= ASSETS_LINK ?>/plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<script src="<?= ASSETS_LINK ?>/plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<script src="<?= ASSETS_LINK ?>/plugins/jcrop/js/jquery.color.js"></script>
<script src="<?= ASSETS_LINK ?>/plugins/jcrop/js/jquery.Jcrop.min.js"></script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
<!--[if gte IE 8]>
<script src="assets/plugins/jquery-file-upload/js/cors/jquery.xdr-transport.js"></script><![endif]-->
<!-- END:File Upload Plugin JS files-->

<script>
    $(document).ready(function () {
        $('#upload_foto').fileupload({
            dataType: 'json',
            done: function (e, data) {
                if (data.result.error == '') {
                    $('#imageName').val(data.result.url);
                    $("#error_upload").attr('class', 'hidden');
                    //exibe a imagem com comprimento de 600px para ser recortada
                    var img = $('<img id="crop_img" style="max-width:600px;">');
                    img.attr('src', 'data:image/png;base64,' + data.result.url);
                    img.css('border', '2px doted gray;');
                    $('#cropper_foto').html(img);
                    $('#modal-submit').html('');
                    $('<br /><a class="botao botao-success" id="botao-enviar" onclick="cortarFoto()" id="btnTrocaFoto">Trocar Foto</a>').appendTo('#modal-submit');
                    $('#crop_img').Jcrop({
                        bgFade: true,
                        setSelect: [150, 150, 0, 0],
                        onSelect: function (c) {
                            $('#x').val(c.x);
                            $('#y').val(c.y);
                            $('#w').val(c.w);
                            $('#h').val(c.h);
                        },
                        //tamanho mínimo que a imagem pode ser recortada
                        minSize: [150, 150],
                        maxSize: [350, 350]
                    });
                    return 0;
                }
                $("#error_upload_span").html(data.result.error);
                $("#error_upload").attr('class', 'show');
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .bar').css(
                    'width',
                    progress + '%'
                );
            }
        });

    });

    var fotoEnviada = false;

    function cortarFoto() {
        //se a foto já foi enviada para a função
        if (fotoEnviada == true) {
            return false;
        }

        $("#botao-enviar").html('<i class="icon-spinner icon-spin"></i> Enviando...');
        //calcula a proporção entre a altura real da imagem e a altural exibida na tela
        var proporcao_vertical = $('#crop_img').prop('naturalWidth') / $('#crop_img').width();
        //calcula a proporção entre o comprimento real da imagem e a altural exibida na tela
        var proporcao_horizontal = $('#crop_img').prop('naturalHeight') / $('#crop_img').height();
        $.ajax({
            url: '<?= Yii::app()->createUrl('/Administracao/Usuario/cortarFoto') ?>',
            data: {
                x: $('#x').val() * proporcao_vertical,
                y: $('#y').val() * proporcao_horizontal,
                h: $('#h').val() * proporcao_horizontal,
                w: $('#w').val() * proporcao_vertical,
                imageName: $('#imageName').val()
            },
            'type': 'POST',
            'success': function () {
                fotoEnviada = true;
                $("#botao-enviar").html('<i class="icon-check"></i> Concluído!');
                window.location = '<?= Yii::app()->createUrl('/perfil') ?>'
            },
            'error': function () {
                fotoEnviada = true;
                $("#botao-enviar").html('<i class="icon-check"></i> Erro ao enviar o arquivo!');
                window.location = '<?= Yii::app()->createUrl('/perfil') ?>'
            }
        })
    }
</script>