<div class="tab-pane row-fluid profile-account" id="tab_1_3">
    <div class="row-fluid">
        <div class="span12">
            <div class="span3">
                <ul class="ver-inline-menu tabbable margin-bottom-10">
                    <li class="active"><a data-toggle="tab" href="#tab_1-1"><i class="icon-cog"></i>Informações Pessoais</a><span
                            class="after"></span></li>
                    <!--                    <li ><a data-toggle="tab" href="#tab_2-2"><i class="icon-picture"></i>Alterar Imagem</a></li>-->
                    <li><a data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i>Mudar Senha</a></li>
                    <!--<li ><a data-toggle="tab" href="#tab_4-4"><i class="icon-eye-open"></i>Configurações</a></li>-->
                </ul>
            </div>
            <div class="span9">
                <div class="tab-content">
                    <?php $this->renderPartial('configuracoes/_informacoes', array('profissional' => $profissional)) ?>
                    <?php //$this->renderPartial('configuracoes/_imagem', array('profissional' => $profissional, 'usuario'=>$usuario)) ?>
                    <?php $this->renderPartial(
                        'configuracoes/_senha',
                        array('usuario' => $usuario, 'profissional' => $profissional)
                    ) ?>
                    <?php //$this->renderPartial('configuracoes/_configuracoes', array('profissional' => $profissional)) ?>
                </div>
            </div>
            <!--end span9-->
        </div>
    </div>
</div>