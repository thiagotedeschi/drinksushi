<link rel="stylesheet" type="text/css"
      href="<?= ASSETS_LINK ?>/plugins/bootstrap-tree/bootstrap-tree/css/bootstrap-tree.css"/>
<script src="<?= ASSETS_LINK ?>/plugins/bootstrap-tree/bootstrap-tree/js/bootstrap-tree.js"></script>
<?php
$concessoes = ($usuario->concessaoGrupos(
    array('condition' => "((dt_fim IS NULL) OR (dt_fim > now())) AND ((dt_inicio IS NULL) OR (dt_inicio < now()))")
));
$grupos = array();
foreach ($concessoes as $concessao) {
    $grupos[] = $concessao->iDGrupo();
}
?>
<div class="tab-pane" id="tab_1_4">

    <?php if (!isset($grupos[0])) { ?>
        <label class="alert alert-warning text-center">Nenhum grupo foi encontrado.</label>
    <?php
    } else {
        foreach ($grupos as $grupo) {
            ?>
            <div class="portlet" style="margin-right: 15px;">
                <div class="portlet-title">
                    <div class="caption" style="color:#727272 !important"><i
                            class="icon-group"></i>&nbsp; <?= $grupo->nome_grupo; ?> </div>
                    <div class="tools">
                        <a href="javascript:;" class="expand"></a>
                    </div>
                </div>
                <div class="portlet-body hide">
                    <ul>
                        <?php
                        $permissoes = $grupo->permissaos;
                        foreach ($permissoes as $permissao) {
                            $acao = $permissao->iDAcao;
                            ?>
                            <li>
                                <?= $acao->nome_acao ?>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        <?php
        }
    }
    ?>
    <!--end row-fluid-->

    <!--end row-fluid-->
</div>