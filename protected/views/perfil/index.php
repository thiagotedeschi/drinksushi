<?php

$mesmoUsuario = ($usuario->IDUsuario == YII::app(
    )->user->IDUsuario); //Verificação para saber se eh o perfil do usuario corrente
?>

<?php $this->renderPartial('configuracoes/_imagem', array('profissional' => $profissional, 'usuario' => $usuario)) ?>

<div class="row-fluid profile">
    <div class="span12">
        <!--BEGIN TABS-->
        <div>
            <ul class="nav nav-tabs">
                <!--<li class="active"><a href="#tab_1_1" data-toggle="tab">Geral</a></li>-->
                <?= $mesmoUsuario ? '<li class="active"><a href="#tab_1_1" data-toggle="tab">Geral</a></li>' : '' ?>
                <?= $mesmoUsuario ? '<li><a href="#tab_1_3" data-toggle="tab">Configurações de conta</a></li>' : '<li class="active"><a href="#tab_1_2" data-toggle="tab">Informações de usuário</a></li>' ?>
                <li><a href="#tab_1_4" data-toggle="tab">Grupos</a></li>
                <!--<li><a href="<?=Yii::app()->createUrl('site/faq')?>" data-toggle="link">Ajuda</a></li>-->
            </ul>
            <div class="tab-content">

                <?=
                $mesmoUsuario ? $this->renderPartial(
                    '_geral',
                    array('profissional' => $profissional, 'usuario' => $usuario)
                ) : ''; ?>
                <?=
                $mesmoUsuario ? $this->renderPartial(
                    '_perfilEditar',
                    array('profissional' => $profissional, 'usuario' => $usuario)
                ) : $this->renderPartial(
                    '_perfilInfo',
                    array('profissional' => $profissional, 'usuario' => $usuario)
                ); ?>
                <?= $this->renderPartial('_grupos', array('profissional' => $profissional, 'usuario' => $usuario)); ?>
            </div>
        </div>
        <!--END TABS-->
    </div>
</div>