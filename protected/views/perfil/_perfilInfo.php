<div class="tab-pane profile-classic row-fluid active" id="tab_1_2">
    <div class="span2"><img src="<?= $usuario->getFoto(245) ?>" alt=""/></div>
    <ul class="unstyled span10">
        <li><span>Nome:</span> <?= $profissional->nome_profissional ?></li>
        <li><span>Criação do Usuário:</span><?= HData::dataHoraInteligente($usuario->dt_criacaoUsuario); ?></li>
        <li><span>Último Login:</span><?php
            $ultimo = $usuario->historicoLogins(
                array('order' => '"dt_historicoLogin" DESC', 'limit' => '1')
            );
            if (isset($ultimo[0])) {
                echo HData::dataHoraInteligente($ultimo[0]->dt_historicoLogin);
            }; ?></li>
        <li><span>Ativo:</span> <a href="#"><?= $usuario->ativo_usuario ? 'Sim' : 'Não' ?></a></li>
        <li><span>Email:</span> <a href="#"><?= $profissional->email_profissional ?></a></li>
        <li>
            <span>Telefone:</span><?= $profissional->telefone_profissional != '' ? (($profissional->ddd_telefoneProfissional != '' ? '(' . $profissional->ddd_telefoneProfissional . ') ' : '') . '' . $profissional->telefone_profissional) : '-' ?>
        </li>
        <li>
            <span>Ramal:</span><?= ($profissional->ramal_profissional != '' ? $profissional->ramal_profissional : ' - ') ?>
        </li>
        <li>
            <span>Celular:</span><?= $profissional->celular_profissional != '' ? (($profissional->ddd_celularProfissional != '' ? '(' . $profissional->ddd_celularProfissional . ') ' : '') . '' . $profissional->celular_profissional) : '-' ?>
        </li>
        <?php
        if (!is_null($profissional->iDProfissao)) {
            echo '<li><span>Profissão:</span>' . $profissional->iDProfissao->nome_profissao . '</li>';
        }
        ?>

    </ul>
</div>