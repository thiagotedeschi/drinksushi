<style>
    .nodeVersao {
        font-size: 15px;
        color: #2d2726;
    }

    .nodeSubversao {
        font-size: 14px;
    }

    .icon.jstree-icon {
        color: #6b6765;
        font-size: 14px;
    }

</style>
<link rel="stylesheet" href="<?php echo ASSETS_LINK; ?>/plugins/jstree/dist/themes/default/style.min.css"/>
<div class="row-fluid">
    <p>Nós, da equipe do <span class="bold"><?= HAtributos::getGlobal('NOME_REDUZIDO_SISTEMA') ?></span> nos esforçamos
        para manter uma relação transparente e direta com nossos parceiros. Por isso, disponibilizamos aqui o histórico
        de todas
        as atualizações que o sistema recebe de forma clara e objetiva. Fez uma requisição de correção ou quer
        conhecer as funcionalidades agregadas ao <span class="bold"><?=
            HAtributos::getGlobal(
                'NOME_REDUZIDO_SISTEMA'
            ) ?></span>?
        Não deixe de visitar esporadicamente esta página, para conhecer o que temos de novo (ou melhor) para você.</p>

    <div>
        <h4>Localizar:</h4>
        <input type="text" id="search_tree" placeholder="Digite 3 ou mais caracteres" class="span3">
    </div>

    <div id="jstree">
        <ul>
            <?php
            $versoes = Versao::model()->findAll(
                [
                    'with' => 'subversaos',
                    'order' => 't."dt_lancamento" DESC, t."IDVersao" DESC, subversaos."dt_lancamento" DESC'
                ]
            );
            foreach ($versoes as $versao) {
                ?>
                <li data-jstree='{"type":"versao", "opened":true}'>
                    <span class="nodeVersao"><?= $versao->getLabelVersao(true, true) ?></span>
                    <ul>
                        <?php
                        foreach ($versao->subversaos as $subversao) {
                            ?>
                            <li data-jstree='{"type": "subversao"}'>
                                <span class="nodeSubversao">
                                    <?= $subversao->getLabelSubversao(true, true) ?>
                                </span>
                                <ul>
                                    <?php
                                    $aprimoramentos = Modificacao::model()->aprimoramentos()->decrescente(
                                    )->findAllByAttributes(['IDSubversao' => $subversao->IDSubversao]);
                                    if ($aprimoramentos) {
                                        echo Html::tag(
                                            'li',
                                            array('data-jstree' => '{"type": "apr"}'),
                                            'Aprimoramentos' . Html::htmlList($aprimoramentos, 'desc_modificacao')
                                        );
                                    }

                                    $novosRecursos = Modificacao::model()->novosRecursos()->decrescente(
                                    )->findAllByAttributes(['IDSubversao' => $subversao->IDSubversao]);
                                    if ($novosRecursos) {
                                        echo Html::tag(
                                            'li',
                                            array('data-jstree' => '{"type": "novos"}'),
                                            'Novos Recursos' . Html::htmlList($novosRecursos, 'desc_modificacao')
                                        );
                                    }

                                    $bugfix = Modificacao::model()->bugfix()->decrescente()->findAllByAttributes(
                                        ['IDSubversao' => $subversao->IDSubversao]
                                    );
                                    if ($bugfix) {
                                        echo Html::tag(
                                            'li',
                                            array('data-jstree' => '{"type": "bugfix"}'),
                                            'Bugfix' . Html::htmlList($bugfix, 'desc_modificacao')
                                        );
                                    }
                                    ?>
                                </ul>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>

<script src="<?php echo ASSETS_LINK; ?>/plugins/jstree/dist/jstree.min.js"></script>
<script>
    $(function () {
        $("#jstree").jstree({
            "plugins": [ "types" , "search"],
            "types": {
                "default": {
                    "icon": "icon icon-check"
                },
                "versao": {
                    "icon": "icon icon-cube"
                },
                "subversao": {
                    "icon": "icon icon-cubes"
                },
                "apr": {
                    "icon": "icon icon-cogs"
                },
                "novos": {
                    "icon": "icon icon-bolt"
                },
                "bugfix": {
                    "icon": "icon icon-bug"
                }
            }
        });

        var to = false;
        $('#search_tree').keyup(function () {
            if (to) {
                clearTimeout(to);
            }
            to = setTimeout(function () {
                var v = $('#search_tree').val();
                if (v.length >= 3)
                    $('#jstree').jstree("search", v);
                else
                    $('#jstree').jstree("search", "");
            }, 250);
        });

        // 7 bind to events triggered on the tree
        $('#jstree').on("changed.jstree", function (e, data) {
            console.log(data.selected);
        });
    });

</script>

