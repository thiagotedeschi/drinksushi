<div class="tab-pane active" id="tab_conhecendo">
    <div class="tabbable tabbable-custom tabs-left">
        <ul class="nav nav-tabs tabs-left">
            <li class="active">
                <a href="#tab_3_1" data-toggle="tab">
                    <i class="icon icon-large icon-file-text-o"></i>
                    Introdução
                </a>
            </li>
            <li>
                <a href="#tab_3_2" data-toggle="tab">
                    <i class="icon icon-mail-reply"></i>
                    O que o
                    <?= HAtributos::getGlobal('NOME_REDUZIDO_SISTEMA') ?> pode fazer por você?</h2>
                </a>
            </li>
            <li>
                <a href="#tab_3_3" data-toggle="tab">
                    <i class="icon icon-large icon-mail-forward"></i>
                    O Que eu posso fazer no <?= HAtributos::getGlobal('NOME_REDUZIDO_SISTEMA') ?>?
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_3_1">
                <h2>Introdução ao <?= HAtributos::getGlobal('NOME_SISTEMA') ?></h2>

                <p>Antes de mais nada, agradeçemos por utilizar nossos produtos e serviços.
                    Temos uma equipe multidisciplinar trabalhando constantemente para melhorar
                    e criar novas funcionalidades que irão aprimorar sua experiência,
                    e nada nos motiva mais que sua satisfação.
                </p>

                <p>
                    <b>
                        <?= HAtributos::getGlobal('NOME_REDUZIDO_SISTEMA') ?></b> é um Sistema de Informação focado
                    no
                    gerenciamento completo de seu restaurante.
                </p>

                <p>
                    Nós fazemos o melhor para que você tenha controle total do seu restaurante.
                </p>
            </div>
            <div class="tab-pane" id="tab_3_2">
                <h2>O que o
                    <?= HAtributos::getGlobal('NOME_REDUZIDO_SISTEMA') ?> pode fazer por você?</h2>

                <p>
                    Tendo as informações corretas e suficientes,
                    o <?= HAtributos::getGlobal('NOME_REDUZIDO_SISTEMA') ?>
                    irá gerar relatórios e documentos claros e de fácil compreensão.
                </p>
            </div>
            <div class="tab-pane" id="tab_3_3">
                <h2>O Que eu posso fazer no <?= HAtributos::getGlobal('NOME_REDUZIDO_SISTEMA') ?>?</h2>

                <p>
                    O <b>
                        <?= HAtributos::getGlobal('NOME_SISTEMA') ?></b> Possui diversos <i>Módulos</i> que
                    irão lhe fornecer ferramentas úteis e precisas para a sua necessidade.<b>
                        <?= HAtributos::getGlobal('NOME_REDUZIDO_SISTEMA') ?></b>.
                    Basicamente, isto é tudo que você precisa fazer.
                </p>
            </div>
        </div>
    </div>
</div>
