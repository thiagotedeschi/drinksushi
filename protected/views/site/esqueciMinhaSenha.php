<?php
/* @var $this FEsqueciMinhaSenhaController */
/* @var $model FEsqueciMinhaSenha */
/* @var $form CActiveForm */
$display = isset($_POST['FEsqueciMinhaSenha']) ? 'block;' : 'none;'
?>

    <div class="form" style="min-height: 0 !important;">

        <?php
        $form = $this->beginWidget(
            'CActiveForm',
            array(
                'id' => 'esqueci-minha-senha-form',
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // See class documentation of CActiveForm for details on this,
                // you need to use the performAjaxValidation()-method described there.
                'enableAjaxValidation' => false,
                'action' => '/index.php?r=site/login',
                'htmlOptions' => array('class' => '', 'style' => 'display: ' . $display)
            )
        );
        ?>


        <?php //echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'emailUsuario', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'emailUsuario'); ?>
                <?php echo $form->error($model, 'emailUsuario', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'captcha'); ?>
            <div class="controls captcha">
                <?php echo $form->textField($model, 'captcha', array('class' => 'control-label span2')); ?>
                <?php $this->widget(
                    'CCaptcha',
                    array(
                        'showRefreshButton' => true,
                        'clickableImage' => true,

                    )
                ); ?>
                <?php echo $form->error($model, 'captcha', array('class' => 'help-inline')); ?>
            </div>
        </div>


        <div class="form-actions">
            <?php echo Html::submitButton('Trocar Minha Senha', array('class' => 'btn newGreen pull-right')); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->

<?php
Yii::app()->clientScript->registerScript(
    'initCaptcha',
    '$(".captcha a").trigger("click");',
    CClientScript::POS_READY
);
?>