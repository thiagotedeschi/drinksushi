<?php
$prof = Usuario::model()->findByPk(Yii::app()->user->IDUsuario)->iDProfissional();
?>
<div class="row-fluid">
    <div class="span12">
        <!-- Google Map -->
        <div class="row-fluid">
<!--            <div id="map" class="gmaps margin-bottom-40" style="height:400px;"></div>-->
        </div>
        <div class="row-fluid margin-bottom-20">
            <div class="span6">
                <div class="space20"></div>
                <h3 class="form-section">Contatos</h3>

                <p>
                    <b>Para nós, seu contato é muito importante!</b><br/>
                    Use este espaço para entrar em contato com os responsáveis pela manutenção e suporte do sistema.
                    Dúvidas, sugestões, dicas e erros serão recebidos com muita atenção, e responderemos assim que
                    possível.

                </p>

                <div class="well">
                    <h4>Venha nos visitar</h4>
                    <address>
                        <strong>Skytronic soluções em TI</strong><br>
                        <abbr title="Endereço"><i class="icon-map-marker"></i>&nbsp;&nbsp;</abbr> Av. Dr. Paulo Japiassu Coelho, 714<br>
                        &nbsp;&nbsp;&nbsp;&nbsp; Bairro Centro, Juiz de Fora, MG - Brasil.<br>
                        <abbr title="Telefone"><i class="icon-phone"></i>&nbsp;&nbsp;</abbr> (32) 8495-6226
                    </address>
                    <address>
                        <strong>Emails</strong><br>
                        <ul>
                            <li>Contato: <a href="mailto:<?=
                                HAtributos::getGlobal(
                                    'EMAIL_SUPORTE'
                                ) ?>"><?= HAtributos::getGlobal('EMAIL_SUPORTE') ?> </a></li>
                            <li>Financeiro: <a href="mailto:<?=
                                HAtributos::getGlobal(
                                    'EMAIL_FINANCEIRO'
                                ) ?>"><?= HAtributos::getGlobal('EMAIL_FINANCEIRO') ?> </a></li>
                            <li>Administração: <a
                                    href="mailto:<?= HAtributos::getGlobal('EMAIL_ADMIN') ?>"><?=
                                    HAtributos::getGlobal(
                                        'EMAIL_ADMIN'
                                    ) ?> </a></li>
                    </address>
<!--                    <ul>-->
<!--                        <li>Versão Atual: <b>--><?//= HAtributos::getGlobal('CURRENT_VERSION') ?><!--</b></li>-->
<!--                        <li>Correção: <b>--><?//= HAtributos::getGlobal('MINOR_VERSION') ?><!--</b></li>-->
<!--                        <li>Próxima Versão (previsão): <b>--><?//= HAtributos::getGlobal('NEXT_VERSION') ?><!--</b></li>-->
<!--                    </ul>-->

                    <ul class="social-icons margin-bottom-10">
                        <li><a href="<?= HAtributos::getGlobal('LINK_FACEBOOK') ?>" data-original-title="facebook"
                               class="facebook"></a></li>
                        <li><a href="<?= HAtributos::getGlobal('LINK_PLUS') ?>" data-original-title="Google Plus"
                               class="googleplus"></a></li>
                        <li><a href="<?= HAtributos::getGlobal('LINK_YOUTUBE') ?>" data-original-title="youtube"
                               class="youtube"></a></li>
                    </ul>
                </div>
            </div>
            <div class="span6">
                <div class="space20"></div>
                <!-- BEGIN FORM-->
                <div action="#" class="horizontal-form">
                    <h3 class="form-section">Dê sua opinião</h3>

                    <div class="control-group">
                        <label class="control-label">Nome</label>

                        <div class="controls">
                            <input disabled="disabled" value="<?= $prof->nome_profissional ?>" type="text"
                                   class="m-wrap span12 disabled"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">E-mail</label>

                        <div class="controls">
                            <input disabled="disabled" value="<?= $prof->email_profissional ?>" type="text"
                                   class="m-wrap span12 disabled"/>
                        </div>
                    </div>
                    <div id="mensagem_control" class="control-group">
                        <label class="control-label">Mensagem *</label>

                        <div class="controls">
                            <textarea id="mensagem" class="m-wrap span12" rows="3"></textarea>

                            <div class="help-inline hidden"></div>
                        </div>
                    </div>
                    <button type="submit" onclick="enviaMensagem()" class="botao botao-info"><i class="icon-ok"></i>
                        Enviar
                    </button>
                </div>
                <!-- END FORM-->
            </div>
        </div>
    </div>
</div>

<script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
<script src="<?= ASSETS_LINK ?>/plugins/gmaps/gmaps.js" type="text/javascript"></script>
<script src="<?= ASSETS_LINK ?>/js/contact-us.js"></script>
<script>
    function enviaMensagem() {
        $msg = $("#mensagem");
        $control = $("#mensagem_control");
        $erro = $('.help-inline', $control);
        if ($msg.val() == '') {
            $control.removeClass('success warning');
            $control.addClass('error');
            $erro.text('A mensagem não pode estar vazia');
            $erro.removeClass('hidden');
        }
        else {
            $.ajax({
                url: '<?=Yii::app()->createUrl('/Site/contato', array('IDProfissional'=>$prof->IDProfissional))?>',
                data: {
                    msg: $msg.val()
                },
                dataType: 'JSON',
                type: 'POST',
                success: function (data) {
                    console.log(data);
                    if (!data.error) {
                        $control.removeClass('error warning');
                        $control.addClass('success');
                        $erro.text('Mensagem enviada com sucesso. Obrigado!');
                        $erro.removeClass('hidden');
                    }
                    else {
                        $control.removeClass('success warning');
                        $control.addClass('error');
                        $erro.text('Ocorreu um erro. tente novamente mais tarde');
                        $erro.removeClass('hidden');
                    }
                },
                error: function () {
                    $control.removeClass('success warning');
                    $control.addClass('error');
                    $erro.text('Ocorreu um erro. tente novamente mais tarde');
                    $erro.removeClass('hidden');
                },
                beforeSend: function () {
                    $control.removeClass('success error');
                    $control.addClass('warning');
                    $erro.text('Enviando...');
                    $erro.removeClass('hidden');
                }
            })
        }
    }
    jQuery(document).ready(function () {
        ContactUs.init();
    });
</script>
