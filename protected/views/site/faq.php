<?php
/**
 * @var array $faqs Objetos da classe ClasseFaq que já trazem suas relações com as perguntas/repostas
 * Agora é só organizar
 */
?>
<div class="span3">
    <ul class="ver-inline-menu tabbable margin-bottom-10">

        <?php
        $primeiro = $faqs[0];
        unset($faqs[0]);
        ?>
        <li class="active">
            <a href="#tab_<?= $primeiro->IDClasseFaq; ?>" data-toggle="tab">
                <i class="icon-<?= $primeiro->icone_classeFaq; ?>"></i>
                <?= $primeiro->nome_classeFaq; ?>
            </a>
            <span class="after"></span>
        </li>
        <?php
        foreach ($faqs as $faq) {
            echo '<li><a href="#tab_' . $faq->IDClasseFaq . '" data-toggle="tab"><i class="icon-' . $faq->icone_classeFaq . '"></i> ' . $faq->nome_classeFaq . '</a></li>';
        }
        $faqs[0] = $primeiro;
        ?>
    </ul>
</div>
<div class="span9">
    <div class="tab-content">
        <?php foreach ($faqs as $faq) {
            ?>
            <div class="tab-pane <?= ($faq->IDClasseFaq == $primeiro->IDClasseFaq ? 'active' : '') ?>"
                 id="tab_<?= $faq->IDClasseFaq ?>">
                <div class="accordion in collapse" id="accordion1" style="height: auto;">
                    <?php
                    $questoes = $faq->faqs();
                    foreach ($questoes as $questao) {
                        ?>
                        <div class="accordion-group">
                            <div class="accordion-heading">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1"
                                   href="#collapse_<?= $questao->IDFaq ?>">
                                    <?= $questao->pergunta_faq; ?>
                                </a>
                            </div>
                            <div id="collapse_<?= $questao->IDFaq ?>" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <?= $questao->resposta_faq; ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>


                </div>
            </div>

        <?php } ?>

    </div>
</div>


