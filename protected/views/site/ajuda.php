<style>
    .tabs-left.tabbable-custom .nav-tabs li {
        border-left: 3px solid transparent;
    }

    .tabs-left.tabbable-custom .nav-tabs li.active {
        border-left: 3px solid #999;
    }
</style>
<div class="tabbable tabbable-custom tabbable-full-width">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab_conhecendo" data-toggle="tab">
                <i class="icon-large icon icon-laptop"></i>
                Conhecendo o <?= HAtributos::getGlobal('NOME_REDUZIDO_SISTEMA') ?>
            </a>
        </li>
    </ul>
    <br/>

    <div class="tab-content">
        <?php
        $this->renderPartial('ajuda/_conhecendo');
        ?>
    </div>
</div>

