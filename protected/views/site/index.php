<?php
/* @var $this SiteController */

$this->pageTitle =  HAtributos::getGlobal('NOME_SISTEMA') . ' - Página Inicial';
?>
<link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/star-menu.css"/>

<div class="row-fluid">
    <div class="span12">
        <!-- END DROPDOWNS PORTLET-->
        <!-- BEGIN CUSTOM BUTTONS WITH ICONS PORTLET-->
        <div id="favoritos-portlet" class="portlet box light-green">
            <div class="portlet-title">
                <div class="caption"><i class="icon-building"> </i>Restaurante ativo</div>
            </div>
            <div class="portlet-body">
                <div class="row-fluid">
                    <div class="label-search">
                        <b>Filial da empresa</b>
                    </div>
                    <div>
                        <?=
                        Html::dropDownList(
                            'escolheEmpregador',
                            Yii::app()->user->getState('IDEmpregador'),
                            Yii::app()->session['empregadores'],
                            array(
                                'class' => 'select2 span12',
                                'empty' => 'Selecione o Restaurante',
                                'id' => 'selectEmpregador'
                            )
                        )
                        ?>
                    </div>

                </div>
            </div>
<!-- END CUSTOM BUTTONS WITH ICONS PORTLET-->
        </div>
    </div>
<!--    <div class="span12 margin-0">-->
        <!-- BEGIN INLINE NOTIFICATIONS PORTLET-->
<!--        <div class="portlet box empresa">-->
<!--            <div class="portlet-title">-->
<!--                <div class="caption"><i class="icon-cogs"></i> Melhores Clientes</div>-->
<!--                <div class="tools">-->
<!--                    <a href="javascript:;" class="collapse"></a>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="portlet-body">-->
<!--                <div class="scroller" data-height="270px" data-always-visible="1" data-rail-visible1="1">-->
<!--                    <ul class="feeds">-->
<!---->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <!-- END INLINE NOTIFICATIONS PORTLET-->
<!--    </div>-->

