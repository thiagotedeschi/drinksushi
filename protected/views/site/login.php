<!-- BEGIN LOGIN FORM -->

<form class="form-vertical login-form" action="index.php?r=site/login" method="post">
    <h3 class="form-title">Fazer Login</h3>

    <?php
    if (isset($_GET['trocasenha'])) {
        ?>
        <div class="alert alert-success">
            Sua senha foi trocada com sucesso! Faça o Login novamente com sua nova senha.
        </div>
    <?php } ?>

    <div id="error" class="alert alert-error <?= !isset($msgErro) ? 'hide' : '' ?>">
        <!--<button class="close" data-dismiss="alert"></button>-->
        <span id="contador"><?= isset($msgErro) ? $msgErro : '' ?></span>
    </div>
    <div class="control-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Usuário</label>

        <div class="controls">
            <div class="input-icon left">
                <i class="icon-user"></i>
                <input class="m-wrap required placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="Usuário" name="Login[username]"/>
            </div>
        </div>
    </div>
    <br/>
    <br/>

    <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">Senha</label>

        <div class="controls">
            <div class="input-icon left">
                <i class="icon-lock"></i>
                <input class="m-wrap required placeholder-no-fix" type="password" autocomplete="off"
                       placeholder="Senha" name="Login[password]"/>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <button type="submit" id="btnLogin" class="btn newGreen pull-right">
            Entrar <i class="m-icon-swapright m-icon-white"></i>
        </button>
    </div>
</form>
<div class="forget-password">
    <h4>Esqueceu sua senha?</h4>

    <p>
        Clique <a href="javascript:$('#esqueci-minha-senha-form').slideToggle();" id="forget-password">aqui</a>
        para trocá-la.
    </p>

    <?php
    $esqueciMinhaSenha = isset($esqueciMinhaSenha) ? $esqueciMinhaSenha : new FEsqueciMinhaSenha;
    if ($esqueciMinhaSenha->message != '') {
        echo $esqueciMinhaSenha->message;
    } else {
        $this->renderPartial('esqueciMinhaSenha', array('model' => $esqueciMinhaSenha));
    }
    ?>
</div>

<?php
if (isset($tempo)) {
    ?>
    <script>
        var msg = '<?= $msgErro ?>';
        var tempo = new Number();
        // Tempo em segundos
        tempo = <?= $tempo ?>;
    </script>
    <script src="<?php echo ASSETS_LINK; ?>/js/contador.js"></script>
<?php } ?>

<script src="<?php echo ASSETS_LINK; ?>/js/login.js"></script>
<script>
    jQuery(document).ready(function () {
        App.init();
        $('.login-form').submit(function () {
            submit = Login.validate(this);
            if (submit) {
                $('#btnLogin').children('i').attr('class', 'icon-cog icon-spin');
                $('#btnLogin').attr('disabled', true);
            }
            return submit;
        });
    });
</script>

<!-- END LOGIN FORM -->
