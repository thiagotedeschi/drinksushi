<?php
/* @var $this SiteController */
/* @var $error array */
?>
<div class="page-404">
    <link href="<?= ASSETS_LINK; ?>/css/error.css" rel="stylesheet" type="text/css"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <div class="number">
        <?php echo $code; ?>
    </div>
    <div class="details">
        <?php
        if ($code == '404') {
            ?>
            <h3>Oops! Não existe nada aqui!<br/> <i class="icon-frown-o icon-3x"></i></h3>
            <h4>Tem certeza que você queria entrar neste link?</h4>
        <?php
        } elseif ($code == '500') {
            ?>
            <h3>Isso é embaraçoso...Temos um probleminha aqui<br/> <i class="icon-ambulance icon-4x"></i></h3>
            <h4>Estamos trabalhando duro para resolver esta questão! Tente novamente mais tarde.</h4>
        <?php
        } elseif ($code == '403') {
            ?>
            <h3>Você não parece estar autorizado a visitar esta página!<br/> <i class="icon-ban-circle icon-4x"></i>
            </h3>
            <h4>Tem certeza que você pode vir aqui?</h4>
        <?php
        } else {
            ?>
            <h3>Isto é constrangedor...não sabemos o que fazer!<br/> <i class="icon-ban-circle icon-4x"></i></h3>
            <h4><?= $message ?></h4>
        <?php } ?>
        <?php
        if (!isset($externo)) {
            ?>
            <p>
                Você está perdido? Não se preocupe, agora você pode:
            <ul>
                <li><a href="index.php">Voltar para página inicial</a>.</li>
                <li><a href="index.php?r=site/faq">Buscar ajuda em nosso Faq</a>.</li>
                <li><a href="index.php?r=site/contatos">Reportar o acontecido</a>.</li>
            </ul>

            </p>
        <?php } ?>
    </div>
</div>
