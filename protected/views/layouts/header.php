<?php

Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="pt-br" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="pt-br" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br"> <!--<![endif]-->
<!-- BEGIN HTML HEAD -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= HTexto::DEFAULT_ENCODING; ?>"/>
    <meta name="language" content="<?= HTexto::DEFAULT_LANGUAGE; ?>"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="shortcut icon" type="image/gif" href="<?php echo ASSETS_LINK; ?>/images/main/favicon.png">
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/print.css" media="print"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/screen.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/form.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/slide.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/profile.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo ASSETS_LINK; ?>/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/toastr.css"/>
    <link href="<?php echo ASSETS_LINK; ?>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo ASSETS_LINK; ?>/css/main-metro.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo ASSETS_LINK; ?>/css/main-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo ASSETS_LINK; ?>/css/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo ASSETS_LINK; ?>/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/menu.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo ASSETS_LINK; ?>/plugins/jquery-tags-input/jquery.tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_LINK; ?>/plugins/select2/select2_metro.css"/>
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_LINK; ?>/plugins/bootstrap-datepicker/css/datepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?= ASSETS_LINK; ?>/plugins/bootstrap-timepicker/compiled/timepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?= ASSETS_LINK; ?>/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?= ASSETS_LINK; ?>/plugins/bootstrap-daterangepicker/daterangepicker.css"/>

    <!--<link href="<?= ASSETS_LINK; ?>/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>-->

    <!-- END GLOBAL MANDATORY STYLES -->


    <title><?php echo Html::encode($this->pageTitle); ?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>

</head>
<!-- END HTML HEAD -->

<body class="page-header-fixed">
