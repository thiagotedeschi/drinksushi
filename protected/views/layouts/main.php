<?php
/**
 * Layout do corpo da página
 * Contém o menu lateral, barra superior, breadcrumbs, menu do usuário (perfil, etc), rodapé e
 * principalmente o conteúdo das páginas renderizadas
 * @since 0.8 10/03/2014
 */
?>
<script>
</script>
<!-- BEGIN HEADER (cabeçalho da página) -->
<div class="header navbar navbar-inverse navbar-fixed-top">

    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
        <div class="container-fluid">
            <!-- BEGIN LOGO -->
            <a class="brand" href="<?= Yii::app()->homeUrl ?>">
                <img height="34" width="85" src="<?= ASSETS_LINK; ?>/images/clientes/drink/logo_drink.png" alt="logo"/>
            </a>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER (botão de menu que aparece quando a página ocupa espaço reduzido de tela) -->


            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- Cabeçalho de impressão -->
            <div class="header-print visible-print">
                <p>Gerado <?= HData::agora() ?> por <?= Yii::app()->user->login_usuario ?></p>
            </div>
            <!-- Fim Cabeçalho de impressão -->
            <!-- BEGIN TOP NAVIGATION MENU (icones e outros elementos do topo) -->
            <ul class="nav pull-right">
                <li id="nome_empregador" class="dropdown">
                    <?php
                    $this->widget(
                        'application.widgets.WContatoEmpregador',
                        array(
                            'view' => 'ContatoEmpregadorHeader',
                            'IDEmpregador' => Yii::app()->user->getState('IDEmpregador')
                        )
                    );
                    // Empregador::getLabelMenu(Yii::app()->user->getState('IDEmpregador'));
                    ?>
                </li>

                <!-- BEGIN USER INFO DROPDOWN -->
                <li class="dropdown user">
                    <a title="Configurações de Usuário" href="#" class="dropdown-toggle" data-toggle="dropdown"
                       data-hover="dropdown" data-close-others="true">
                        <img alt="Minha Foto" style="height: 32px; margin-right: 3px"
                             src="<?= Usuario::model()->findByPk(Yii::app()->user->IDUsuario)->getFoto(32) ?>"/>
                        <span class="username"><?=
                            isset(Yii::app()->user->login_usuario) ? Yii::app()->user->login_usuario : Yii::app(
                            )->user->logout(); ?></span>
                        <i class="icon-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=
                            $this->createUrl(
                                '/Perfil/',
                                array('IDUsuario' => Yii::app()->user->IDUsuario)
                            ) ?>"><i class="icon-user"></i> Meus Dados</a></li>
                        <!--<li><a href="<?php // $this->createUrl('/Mensagem/view');                  ?>"><i class="icon-envelope"></i> Caixa de Entrada </a></li>-->
                        <li><a href="<?= $this->createUrl('/site/contatos'); ?>"><i class="icon-phone"></i> Contatos</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href=<?= $this->createUrl('/site/ajuda', array()) ?>" id=""><i class="icon-question-circle"></i> Ajuda</a></li>
                        <li><a href="index.php?r=site/faq" id=""><i class="icon-comment"></i> Dúvidas</a></li>
                        <li><a href="index.php?r=site/logout"><i class="icon-key"></i> Sair</a></li>
                    </ul>
                </li>
                <!-- END USER INFO DROPDOWN -->
            </ul>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar nav-collapse collapse ">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu">

            <li>
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <div class="sidebar-search">

                    <div class="input-box">
                        <div class="label-search">
                            <b>Buscar</b>
                            <i class="icon-search"></i>
                        </div>
                        <input class="form-control" id='searchTerm' onkeyup="buscaMenus(event.keyCode)" type="text"
                               placeholder="Exemplo: Pedido"/>
                    </div>
                </div>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            <?php
            $this->widget(
                'zii.widgets.CMenu',
                array(
                    'items' => $this->getMenu(),
                    'htmlOptions' => array('class' => 'page-sidebar-menu', 'id' => 'menu'),
                    'submenuHtmlOptions' => array('class' => 'sub-menu'),
                )
            );
            ?>

        </ul>
        <ul id="pesquisa" class='page-sidebar-menu'></ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
            <div class="modal-header">
                <button data-dismiss="modal" class="close" type="button"></button>
                <h3>portlet Settings</h3>
            </div>
            <div class="modal-body">
                <p>Here will be a configuration form</p>
            </div>
        </div>
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE  BREADCRUMB-->
                    <?php
                    $crumbs = $this->widget(
                        'application.widgets.WBreadCrumbs',
                        array(
                            'idAcao' => $this->IDAcao,
                            'crumbs' => $this->getCrumbs(),
                            //ação atual
                            'outrasCrumbs' => isset($this->outrasCrumbs) ? $this->outrasCrumbs : array()
                            //usar para inserir manualmente outras crumbs
                        )
                    );
                    ?>
                    <!-- END PAGE  BREADCRUMB-->

                </div>
            </div>
            <?php
            $classeTipoModulo = $this->getClassTipoModulo();
            ?>

            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <?php
                    if($classeTipoModulo != 'tipo-modulo-default'){
                    ?>
                    <div class="portlet box <?= $classeTipoModulo ?>">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="portlet-title main">
                            <div class="caption">
                                <span class="icon-stack ">
                                    <i class="icon-circle icon-stack-2x"></i>
                                    <i class="icon-stack-1x icon-<?= $this->getIconePagina() ?>"></i>
                                </span>
                                <?= $this->pageTitle; ?>
                            </div>
                        </div>
                        <!-- end PAGE TITLE -->
                        <div class="portlet-body main-portlet flip-scroll clearfix">
                            <?= $content; ?>
                        </div>
                        <!-- END PAGE CONTENT-->
                    </div>
                    <?php
                    }else{
                    ?>
                        <div class="portlet-body main-portlet flip-scroll clearfix">
                            <?= $content; ?>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTAINER-->
    </div>
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="footer-inner">
        2015 &copy; <b class="">Skytronic soluções em TI</b>
    </div>
    <div class="footer-markup visible-print">
        <br/> <?= $this->createAbsoluteRequestUrl(); ?>
    </div>
    <div class="footer-tools">
        <a target="_blank" href="<?= HAtributos::getGlobal('SITE_Drink'); ?>" class="logoFooter">
        </a>
    </div>
</div>
<script>
    $('.form-actions').not('.no-buttom-form-action').prepend("<i title=\"Voltar\" style=\"cursor:pointer; margin: 8px 8px 10px 8px;\" onclick=\"window.history.back()\" id=\"btn_voltar_form\" class=\"m-icon-big-swapleft float-left\"></i>");
</script>
