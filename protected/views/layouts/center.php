<?php
/* @var $msgErro Mensagem de erro do Login */
/* @var $tempo Tempo em que o usuário não poderá logar */
?>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="pt-br" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="pt-br" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="pt-br"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="pt-br"/>
    <link rel="shortcut icon" type="image/gif" href="<?php echo ASSETS_LINK; ?>/images/main/favicon.png">

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/screen.css" media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/form.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/bootstrap.css"/>
    <link href="<?php echo ASSETS_LINK; ?>/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo ASSETS_LINK; ?>/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo ASSETS_LINK; ?>/css/main-metro.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo ASSETS_LINK; ?>/css/main-responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo ASSETS_LINK; ?>/css/default.css" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<?php echo ASSETS_LINK; ?>/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ASSETS_LINK; ?>/css/main.css"/>
    <link href="<?php echo ASSETS_LINK; ?>/css/login.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->


    <title><?php echo Html::encode($this->pageTitle); ?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login" style="background-image: url(<?php echo ASSETS_LINK; ?>/images/main/bg.jpg) !important; background-repeat: no-repeat !important; background-position: center;">

<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGO -->
    <div class="logo">
        <a href="http://drinksushi.com.br" target="_blank">
            <img src="<?php echo ASSETS_LINK; ?>/images/main/logoLogin.png" alt="Drink Sushi"/>
        </a>
    </div>
    <?php echo $content ?>

</div>



<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    2015 &copy; <b>Skytronic soluções em TI</b>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<script type="text/javascript" src="<?php echo ASSETS_LINK; ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo ASSETS_LINK; ?>/js/bootstrap/bootstrap.js"></script>

<script src="<?php echo ASSETS_LINK; ?>/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js"
        type="text/javascript"></script>
<script src="<?php echo ASSETS_LINK; ?>/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="<?php echo ASSETS_LINK; ?>/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS_LINK; ?>/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS_LINK; ?>/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS_LINK; ?>/js/app.js"></script>

</body>
<!-- END BODY -->
</html>
