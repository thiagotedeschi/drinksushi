<?php
/**
 * Layout dos Iframes/Modais do sistema
 * Contém o somente o conteúdo que deve ser renderizado nos Modais
 * @since 0.8 10/03/2014
 * @var $this Controller
 */
$this->beginContent('//layouts/header');
$this->endContent();
?>
    <style>
        body{
            background: #fff !important;
        }
    </style>
    <!-- BEGIN PAGE CONTENT-->
    <div id="content">
        <div class="row-fluid">
            <div class="span12">
                <div class="portlet box <?= $this->getClassTipoModulo() ?>">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="portlet-title main">
                        <div class="caption">
                            <i class="icon-<?= $this->getIconePagina() ?>"></i>
                            <?= $this->pageTitle; ?>
                        </div>
                    </div>
                    <!-- end PAGE TITLE -->
                    <div class="portlet-body clearfix">
                        <?= $content; ?>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
        </div>
    </div>

<?php
$this->beginContent('//layouts/footer');
$this->endContent();
?>