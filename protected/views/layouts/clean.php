<?php


$this->beginContent('//layouts/header');
$this->endContent();
?>
    <style>
        .fundoBranco {
            background-color: #ffffff !important;
        }

        body {
            background: #fff !important;
            overflow-x: hidden;
        }
    </style>
    <div class="row-fluid">
        <div class="fundoBranco">
            <?php echo $content; ?>
        </div>
    </div>

    <script>
        $('.form-actions').prepend("<i title=\"Voltar\" style=\"cursor:pointer; margin: 8px 8px 10px 8px;\" onclick=\"history.back()\" class=\"m-icon-big-swapleft float-left\"></i>");
    </script>
<?php
$this->beginContent('//layouts/footer');
$this->endContent();
?>