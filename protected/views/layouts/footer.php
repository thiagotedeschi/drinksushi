
<script type="text/javascript" src="<?php echo ASSETS_LINK; ?>/js/bootstrap/bootstrap.js"></script>
<script src="<?php echo ASSETS_LINK; ?>/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="<?php echo ASSETS_LINK; ?>/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS_LINK; ?>/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS_LINK; ?>/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo ASSETS_LINK; ?>/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js"
        type="text/javascript"></script>
<!--[if lt IE 9]>
<script src="<?php echo ASSETS_LINK; ?>/plugins/excanvas.min.js"></script>
<script src="<?php echo ASSETS_LINK; ?>/plugins/respond.min.js"></script>
<![endif]-->
<script src="<?php echo ASSETS_LINK; ?>/js/app.js"></script>
<script src="<?php echo ASSETS_LINK; ?>/js/pesquisa_menu.js"></script>
<script src="<?php echo ASSETS_LINK; ?>/js/export.js"></script>
<script src="<?php echo ASSETS_LINK; ?>/js/slide.js"></script>
<script src="<?php echo ASSETS_LINK; ?>/js/toastr.js"></script>
<script src="<?php echo ASSETS_LINK; ?>/js/flash.js"></script>
<script src="<?php echo ASSETS_LINK; ?>/js/data.js"></script>

<script type="text/javascript" src="<?= ASSETS_LINK; ?>/plugins/select2/select2.js"></script>
<script type="text/javascript" src="<?= ASSETS_LINK; ?>/plugins/select2/select2_locale_pt-BR.js"></script>
<script type="text/javascript"
        src="<?= ASSETS_LINK; ?>/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
        src="<?= ASSETS_LINK; ?>/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?= ASSETS_LINK; ?>/plugins/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript"
        src="<?= ASSETS_LINK; ?>/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript"
        src="<?= ASSETS_LINK; ?>/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script type="text/javascript" src="<?= ASSETS_LINK; ?>/js/bootstrap/bootstrap-language.pt-BR.js"></script>
<script src="<?= ASSETS_LINK; ?>/plugins/jquery-tags-input/jquery.tagsinput.min.js"
        type="text/javascript"></script>

<script src="<?= ASSETS_LINK; ?>/plugins/mascara.js"
        type="text/javascript"></script>

<script src="<?= ASSETS_LINK; ?>/plugins/scripts.js"
        type="text/javascript"></script>

<script async>
    var acao = 'I<?= $this->IDAcao ?>';
    var urlEmp = '<?= Yii::app()->createUrl('site/mudaEmpregador') ?>';
    var urlFlashes = '<?= Yii::app()->createUrl('site/getFlashes') ?>';

    jQuery(document).ready(function () {
        App.init();
        insereSetasMenu();
        abreMenu();
        if ($.cookie('collapse_menuHO') == "true")
            $(".sidebar-toggler").trigger("click");
        //Inicializa as bibliotecas de data/datetime/time
        Data().init();
        $('.form-actions .botao').addClass('botao-success');
    });
</script>
</body>
</html>