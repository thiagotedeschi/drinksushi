<?php foreach ($this->notificacoes as $key => $value) { ?>
    <li>
        <div class="col1">
            <div class="cont">
                <div class="cont-col1">
                    <div class="margin-top-10 label label-<?= $value['tipo_notificacao'] ?>">
                        <i class="icon-<?= $value['icone_notificacao'] ?>"></i>
                    </div>
                </div>
                <div class="cont-col2">
                    <div class="desc" <?= $value['dt_leituraNotificacao'] != '' ? : 'style="font-weight:bold"' ?> >
                        <?= $value['conteudo_notificacao'] ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col2">
            <div class="date">
                <?= HData::dataHoraInteligente($value['dt_envioNotificacao']) ?>
            </div>
        </div>
    </li>
    <?php
    if ($value->dt_leituraNotificacao == '') {
        $value->dt_leituraNotificacao = new CDbExpression('CURRENT_TIMESTAMP(0)');
        $value->save();
    }
}?>