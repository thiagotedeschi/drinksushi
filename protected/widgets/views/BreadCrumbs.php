<ul class="breadcrumb">
    <?php
    if (isset($this->crumbs)) {
        $i = 0;
        foreach ($this->crumbs as $crumb) //para cada crumb criada na Widget
        {
            $i++;
            echo '<li>';
            if ($crumb['icone'] != '') {
                echo '<i class="icon-', $crumb['icone'], '">', '</i>';
            } //imprime o icone, caso existente
            if ($crumb['url'] != '') {
                echo Html::link($crumb['nome'], Yii::app()->createUrl($crumb['url']));
            } //escreve o link, caso existente
            else {
                if (isset($crumb['desc'])) {
                    echo Html::link(
                        '' . $crumb['nome'],
                        'javascript:;',
                        array(
                            'class' => 'popovers',
                            'data-html' => 'true',
                            'data-trigger' => 'hover',
                            'data-original-title' => 'Sobre',
                            'data-placement' => 'bottom',
                            'data-content' => $crumb['desc']
                        )
                    );
                } else {
                    echo $crumb['nome'];
                }
            } //se não tiver link, é só imprimir o nome
            if (isset($this->crumbs[$i])) //se existir proxima crumb, imprimir o separador
            {
                echo '<i class="icon-', $this->separador, '"></i>';
            }
            echo '</li>';
        }
    }
    ?>
</ul>