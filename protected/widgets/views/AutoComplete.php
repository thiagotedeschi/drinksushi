<?php
//Não atribua previamente a classe select2, para que não haja conflito
echo $form->hiddenField($model, $attributeName, $htmlOptions);
?>

<script>
    $(document).ready(function () {
        $('#<?=Html::activeId($model,$attributeName)?>').select2({
            multiple: '<?=$multiple?>',
            placeholder: '<?=$placeholder?>',
            minimumInputLength: '<?=$minimumInput?>',
            initSelection: function (element, callback) {
                var id = $('#<?=Html::activeId($model,$attributeName)?>').val();
                if (id !== "") {
                    //É necessário repetir esse seletor para evitar conflitos
                    <?php if($multiple){?>
                    callback(<?=CJSON::encode($selectedList)?>);
                    <?php
                    }
                    else{?>
                    callback({id: id, title: '<?=isset($selectedText) ? $selectedText : ''?>'});
                    <?php }?>
                }
            },

            ajax: {
                url: '<?=$ajaxUrl?>',
                dataType: 'json',
                data: function (term, page) { // page is the one-based page number tracked by Select2
                    JSONParams = <?= !empty($params) ? $params : '' ?>;
                    if (JSONParams == '') {
                        JSONParams = {};
                    }
                    JSONParams.term = term;
                    JSONParams.page = page;
                    return JSONParams;
                },
                results: function (data, page) {
                    // notice we return the value of more so Select2 knows if more results can be loaded
                    return {results: data};
                }
            },
            formatResult: textFormatResult, // omitted for brevity, see the source of this page
            formatSelection: textFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            escapeMarkup: function (m) {
                return m;
            } // we do not want to escape markup since we are displaying html in results
        });
    });

    function textFormatResult(data) {
        return data.title;
    }

    function textFormatSelection(data) {
        return data.title;
    }

</script>