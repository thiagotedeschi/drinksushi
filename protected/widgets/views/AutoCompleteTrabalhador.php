<?php
//Não atribua previamente a classe select2, para que não haja conflito
echo $form->hiddenField($model, $attributeName, $htmlOptions);
?>

<a id="<?= $IDLink ?>" target="_blank" style="display: none" title="Visualizar Trabalhador">
    <i class="icon-search icon-large detalhes-trabalhador"> </i>
</a>

<div id="<?= $IDModal ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div style="padding: 10px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <iframe src="" style="width:95%; overflow: hidden; min-height:440px;" class="modal-body">
    </iframe>
</div>

<style>
    .detalhes-trabalhador {
        padding-top: 5px;
        padding-left: 5px;
        color: #47423F;
    }
</style>

<script>
    $('#<?= $IDLink ?>').click(function () {
        $('#<?= $IDModal ?>').modal("show");
        return false;
    });

    $('#<?=Html::activeId($model,$attributeName)?>').change(function () {
        IDTrab = $('#<?=Html::activeId($model,$attributeName)?>').val(); //É necessário repetir esse seletor para evitar conflitos
        $('#<?= $IDLink ?>').attr('href', '<?=$linkView?>&id=' + IDTrab + '&in-modal=in-modal');
        $('#<?= $IDModal ?>').children("iframe").attr("src", $('#<?= $IDLink ?>').attr("href"));
        $('#<?= $IDLink ?>').css('display', 'block');
    });

    $(document).ready(function () {
        $('#<?=Html::activeId($model,$attributeName)?>').select2({
            placeholder: '<?=$placeholder?>',
            multiple: '<?=$multiple?>',
            minimumInputLength: '<?=$minimumInput?>',

            initSelection: function (element, callback) {
                var id = $('#<?=Html::activeId($model,$attributeName)?>').val();
                if (id !== "") {
                    callback({id: id, title: '<?=isset($selectedText) ? $selectedText : ''?>'});
                }
            },

            ajax: {
                url: '<?=$ajaxUrl?>',
                dataType: 'json',
                data: function (term, page) { // page is the one-based page number tracked by Select2
                    JSONParams = <?= !empty($params) ? $params : '' ?>;
                    if (JSONParams == '') {
                        JSONParams = {};
                    }
                    JSONParams.term = term;
                    JSONParams.page = page;
                    return JSONParams;
                },
                results: function (data, page) {
                    // notice we return the value of more so Select2 knows if more results can be loaded
                    return {results: data};
                }
            },
            formatResult: textFormatResult, // omitted for brevity, see the source of this page
            formatSelection: textFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            escapeMarkup: function (m) {
                return m;
            } // we do not want to escape markup since we are displaying html in results
        });
    });

    function textFormatResult(data) {
        return data.title;
    }

    function textFormatSelection(data) {
        return data.title;
    }

</script>