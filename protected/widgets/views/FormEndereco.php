<?php
/* @var $this EnderecoClienteController */
/* @var $endereco EnderecoCliente */
/* @var $form CActiveForm */
/* @var $id */
$n = $id;
if ($id != '') {
    $id = '[' . $id . ']';
}
?>

<script>
    var nomeCidadeCep = '';
    $(document).ready(function () {
        $('#<?= Html::activeId($endereco, $id . 'cep_endereco') ?>').keypress(function (e) {
            if (e.which == 13) {
                $('#btnBuscarCep<?= $n ?>').trigger('click');
                return false;
            }
        });
    });
</script>


<fieldset id="EnderecoCliente_<?= $n ?>">
<legend><?= $label ?></legend>
<?php
if ($aproveitarEndereco !== null) {
    ?>
    <?php
    Yii::app()->clientScript->registerScript(
        'aproveitarEndereco',
        "$(function () {
        $('#aproveitarEndereco').change(function(event){
           if($(this).parent().hasClass('checked')) {
                 $('#EnderecoCliente_$n  select, #EnderecoCliente_$n input[type!=\"checkbox\"]').attr('disabled', false);
                 $('#EnderecoCliente_$n  input[type!=\"checkbox\"]').attr('value', '');
                 $(this).parent().removeClass('checked')
                 $('#aproveitarEndereco').attr('checked', false);
            }
           else{
                 $(this).parent().addClass('checked')
                 $('#EnderecoCliente_$n  select, #EnderecoCliente_$n input[type!=\"checkbox\"]').attr('disabled', true);
                 $('#EnderecoCliente_$n  input[type!=\"checkbox\"]').attr('value', '');
                 $('#aproveitarEndereco').attr('checked', true);
            }
              
           }
         
        );
        });"
        ,
        CClientScript::POS_READY
    );

    if ($desabilitarForm)
        Yii::app()->clientScript->registerScript(
            'desabilitaForm',
            "
                            $(document).ready(function(){
                            $('#aproveitarEndereco').checked = true;
                           $('#aproveitarEndereco').trigger('change');

                           })
                       ",
            CClientScript::POS_READY
        )
    ?>


    <p> <?=
        Html::checkBox(
            'Endereco[aproveitarEndereco]',
            false,
            array('id' => 'aproveitarEndereco', 'value' => 1, 'uncheckValue' => 0, 'class' => 'toggle')
        ) ?>Aproveitar Endereço Acima?</p>

<?php } ?>

<?php echo $form->errorSummary($endereco); ?>

<?php
if ($n == '') {
    $nEstado = '_';
} else {
    $nEstado = '_' . $n . '_';
}
$this->widget(
    'ext.correios.BuscaPorCep',
    array(
        'target' => '#btnBuscarCep' . $n,
        'model' => $endereco,
        'attribute' => $id . 'cep_endereco',
        'url' => 'Endereco/buscaPorCep',
        'config' => array(
            'location' => $id . 'logradouro_endereco',
            'district' => $id . 'bairro_endereco',
            'state' => array(0 => 'Estado' . $nEstado . 'IDEstado'),
            'city' => $id . 'IDCidade',
        ),
    )
);
?>


<div class="control-group input-append">
    <?php echo $form->labelEx($endereco, $id . 'cep_endereco', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php
        $this->widget(
            'CMaskedTextField',
            array(
                'model' => $endereco,
                'attribute' => $id . 'cep_endereco',
                'mask' => HTexto::MASK_CEP,
                'htmlOptions' => array('maxlenght' => '9', 'class' => 'm-wrap span6')
            )
        );
        ?>
        <?php echo Html::htmlButton(
            "<i class='icon-search icon-large'></i>",
            array("id" => "btnBuscarCep" . $n, 'class' => 'botao')
        ); ?>
        <?php echo $form->error($endereco, $id . 'cep_endereco', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($endereco, $id . 'logradouro_endereco', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($endereco, $id . 'logradouro_endereco', array('class' => 'm-wrap span6')); ?>
        <?php echo $form->error($endereco, $id . 'logradouro_endereco', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($endereco, $id . 'bairro_endereco', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($endereco, $id . 'bairro_endereco', array('class' => 'm-wrap span6')); ?>
        <?php echo $form->error($endereco, $id . 'bairro_endereco', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($endereco, $id . 'IDPais', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->dropDownList(
            $endereco,
            $id . 'IDPais',
            Html::listData(PaisIBGE::model()->findAll(), 'IDPais_ibge', 'nome_pais'),
            array('class' => 'select2 span4')
        ) ?>
        <?php echo $form->error($endereco, $id . 'IDPais', array('class' => 'help-inline')); ?>
    </div>
</div>
<?php $estado = new Estado(); ?>
<div class="control-group">
    <?php
    if (!$endereco->isNewRecord) {
        $cidade = $endereco->iDCidade();
        $estado = $cidade->iDEstadoCidade();
        $sigla_estado = $estado->sigla_estado;
        $id_estado = $estado->IDEstado;
        $id_cidade = $cidade->IDCidade_ibge;
    } else {
        $sigla_estado = '';
        $id_estado = '';
        $id_cidade = '';
    }
    ?>
    <div class="control-group">
        <?php echo $form->labelEx($estado, $id . 'Estado', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            echo $form->dropDownList(
                $estado,
                $id . 'IDEstado',
                Html::listData(Estado::model()->findAll(), 'IDEstado', 'sigla_estado'),
                array(
                    'class' => 'select2 span3',
                    'empty' => array('' => '--Escolha o Estado--'),
                    'options' => array($id_estado => array('selected' => true)),
                    'ajax' => array(
                        'type' => 'POST', //request type
                        'url' => Yii::app()->createUrl('/Endereco/cidadesPorEstado'), //url to call.
                        'success' => 'function(html){
                                        jQuery("' . '#' . Html::activeId($endereco, $id . 'IDCidade') . '").html(html);

                                           jQuery("' . '#' . Html::activeId($endereco, $id . 'IDCidade') . ' option").each(function(){
                                                   var $this = $(this);
                                                   if($this.text() == nomeCidadeCep.toUpperCase())
                                                   {
                                                        $this.attr("selected", "selected");
                                                        $this.trigger("change");
                                                   }
                                           });

                        }',
                        'data' => array('estado' => 'js:jQuery(\'option:selected\', this).val()'),
                    )
                )
            );
            ?>
            <?php echo $form->error($endereco, $id . 'IDPais', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($endereco, $id . 'IDCidade', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $endereco,
                $id . 'IDCidade',
                $endereco->isNewRecord ? array('' => '--Escolha um estado --') : Html::listData(
                    CidadeIbge::model()->findAll($id_estado != '' ? '"IDEstado_cidade" = ' . $id_estado : ''),
                    'IDCidade_ibge',
                    'nome_cidadeIbge'
                ),
                array(
                    'class' => 'select2 span4'
                )
            ); ?>

            <?php echo $form->error($endereco, $id . 'IDCidade', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($endereco, $id . 'numero_endereco', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($endereco, $id . 'numero_endereco', array('class' => 'm-wrap span6')); ?>
            <?php echo $form->error($endereco, $id . 'numero_endereco', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($endereco, $id . 'complemento_endereco', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $endereco,
                $id . 'complemento_endereco',
                array('class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($endereco, $id . 'complemento_endereco', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($endereco, $id . 'referencia_endereco', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $endereco,
                $id . 'referencia_endereco',
                array('class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($endereco, $id . 'referencia_endereco', array('class' => 'help-inline')); ?>
        </div>
    </div>
</fieldset>
