<style>
    .password-strength .progress {
        height: 3px !important;
    }

    .error-list {
        list-style-type: none;
        color: #b94a48;
    }
</style>

<script src="assets/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        var initialized = false;
        var repeat = $("#<?= Html::activeId($model, 'newPassword_repeat') ?>");
        var input = $("#<?= Html::activeId($model, 'newPassword') ?>");
        input.keydown(function () {
            if (initialized === false) {
                // set base options
                input.pwstrength({
                    usernameField: '.username',
                    raisePower: 0.9,
                    minChar: 7,
                    verdicts: ["Péssima. Escolha outra senha", "Fraca", "Média", "Forte", "Muito Forte"],
                    scores: [17, 26, 40, 50]
                });

                // set progress bar's width according to the repeat width
                $('.progress', input.parents('.password-strength')).css('width', input.outerWidth() - 2);

                // set as initialized 
                initialized = true;
            }

        });

    })</script>
<!--        <form action="#">
            <label class="control-label">Senha Atual</label>
            <input type="password" class="m-wrap span8" />
            <label class="control-label">Nova Senha</label>
            <input id='password' type="password" class="m-wrap span8" />
            <label class="control-label">Repita a Nova Senha</label>
            <div class="control-group password-strength">
                <input type="password" class="m-wrap span8" name="password" id="password_repeat">
            </div>
            <div class="submit-btn">
                <a href="#" onclick="submit()"class="botao">Trocar Senha</a>
            </div>
        </form>-->

<?php
/** @var BootActiveForm $form */
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'mudar-senha-form',
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'action' => Yii::app()->createUrl($actionURL),
        'htmlOptions' => array('class' => 'form-horizontal'),
    )
);
?>

<div class="form" style="min-height: 0px !important">
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'currentPassword'); ?>
        <div class="">
            <?php echo $form->passwordField(
                $model,
                'currentPassword',
                array('class' => 'm-wrap span3', 'placeholder' => 'Senha Atual')
            ); ?>
            <?php echo $form->error($model, 'currentPassword', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">

        <?php echo $form->labelEx($model, 'newPassword'); ?>
        <div class="">
            <?php echo $form->passwordField(
                $model,
                'newPassword',
                array('class' => 'm-wrap span3', 'placeholder' => 'Nova Senha')
            ); ?>
            <?php $form->error($model, 'newPassword', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group password-strength">
        <?php echo $form->labelEx($model, 'newPassword_repeat'); ?>
        <div class="">
            <?php echo $form->passwordField(
                $model,
                'newPassword_repeat',
                array('class' => ' m-wrap span3', 'placeholder' => 'Repetir Nova Senha')
            ); ?>
            <?php echo $form->error(
                $model,
                'newPassword_repeat',
                array('class' => 'help-inline')
            ); ?>
        </div>
    </div>
    <div class="">
        <?php echo Html::submitButton('Trocar Senha', array('class' => 'botao botao-success')); ?>
    </div>
</div>


<?php
$this->endWidget();


