<div class='date-range-wrapper'>
    <?php echo Html::activeTextField($model, $labelFieldAttribute, $labelFieldHtmlOptions); ?>
    <?php echo Html::activeHiddenField($model, $startAttribute, array('class' => 'date-range-inicio')); ?>
    <?php echo Html::activeHiddenField($model, $endAttribute, array('class' => 'date-range-fim')); ?>

</div>