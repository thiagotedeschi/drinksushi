<script src="assets/js/jquery.tinycarousel.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="assets/css/als.css"/>
<script type="text/javascript">
    $(document).ready(function () {
        $('#slider1').tinycarousel({display: 2});
        var t;
        $(".page-container").mouseleave(function () {
            clearTimeout(t);
        });
        $(".page-container").mouseenter(function () {
            t = setTimeout(function () {
                $('.estrelaAmarela').trigger('click');
            }, 3000);
        });
        $(".page-container").click(function () {
            $('.estrelaAmarela').trigger('click');
        });
    });
</script>
<div id="slider1">
    <a class="buttons prev" href="#">left</a>

    <div class="viewport">
        <div class="row-fluid">
            <ul class="overview">
                <?php
                $favoritos = FavoritoUsuario::model()->findAll('"IDUsuario" =' . Yii::app()->user->IDUsuario);
                foreach ($favoritos as $favorito) {
                    $itemMenu = ItemMenu::model()->findByPk($favorito->IDItemMenu);
                    ?>
                    <li>
                        <div class="icon-btn span24 clearfix">
                            <a href="<?= $itemMenu->getUrl() ?>">
                                <i class="icon-<?= $itemMenu->icone_itemMenu . ' icon-2x ' . 'IM' . $itemMenu->IDItem_menu ?>"></i>

                                <div><?= $itemMenu->nome_menu ?></div>
                            </a>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <a class="buttons next" href="#">right</a>
</div>


