<li class="dropdown" id="header_notification_bar">
    <a title="Minhas Notificações" href="#" style="height:10px" class="dropdown-toggle" data-toggle="dropdown"
       data-hover="dropdown" data-close-others="true">
        <i class="icon-warning"></i>
        <?= $this->qt_naoLidas > 0 ? '<span id="qt_notif" class="badge">  ' . $this->qt_naoLidas . '</span>' : '' ?>
    </a>
    <ul class="dropdown-menu extended notification">
        <li>
            <p><?= $this->qt_naoLidas > 0 ? 'Você têm ' . $this->qt_naoLidas . ' novas Notificações.' : 'Você não possui novas Notificações.' ?></p>

        </li>
        <li>
            <ul class="dropdown-menu-list scroller" style="height:250px">
                <?php
                foreach ($this->notificacoes as $notificacao) {
                    ?>
                    <li class="<?= $notificacao->dt_leituraNotificacao == '' ? 'nao-lida' : '' ?>"
                        id="<?= $notificacao->IDNotificacao; ?>">
                        <a href="<?= $notificacao->link_notificacao != '' ? $notificacao->link_notificacao : '#' ?>">
                            <?=
                            '<span class="label label-' . $notificacao->tipo_notificacao . '"><i class="icon-' . $notificacao->icone_notificacao . '"></i></span>',
                            $notificacao->dt_leituraNotificacao == '' ? $notificacao->conteudo_notificacao : '<strike>' . $notificacao->conteudo_notificacao . '</strike>', ' ',
                                '<span class="time">' . HData::dataHoraInteligente(
                                    $notificacao->dt_envioNotificacao
                                ) . '</span>';
                            ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </li>
        <li class="external">
            <a href="#">Ver todas notificações <i class="m-icon-swapright"></i></a>
        </li>
    </ul>
</li>