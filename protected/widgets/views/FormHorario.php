<?php
/* @var $this HorarioController */
/* @var $horario Horario */
/* @var $localHorario LocalHorario */
/* @var $form CActiveForm */
/* @var $id int */
/* @var $folga boolean */
/* @var $htmlOptions array */


if (!$localHorario->isNewRecord) {
    $localHorario->dt_inicio = HData::formataData(
        $localHorario->dt_inicio,
        HData::BR_DATE_FORMAT,
        HData::SQL_DATE_FORMAT
    );
    $localHorario->dt_fim = HData::formataData($localHorario->dt_fim, HData::BR_DATE_FORMAT, HData::SQL_DATE_FORMAT);
}

$n = $id;
$id = '[' . $id . ']';
?>
<fieldset id="Horario_<?= $n ?>">
    <legend><?= $label . ($n > 0 ? ' - ' . $n : '') ?></legend>
    <?php echo Html::errorSummary($horario); ?>
    <?php echo Html::activeHiddenField($localHorario, $id . 'folga', array('value' => $folga)) ?>

    <div class="control-group">
        <?php echo Html::activeLabelEx($horario, $id . 'inicio_horario', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeTextField(
                $horario,
                $id . 'inicio_horario',
                array('class' => 'm-wrap span6 time-picker')
            ); ?>
            <?php echo Html::error($horario, $id . 'inicio_horario', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo Html::activeLabelEx($horario, $id . 'fim_horario', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeTextField(
                $horario,
                $id . 'fim_horario',
                array('class' => 'm-wrap span6 time-picker')
            ); ?>
            <?php echo Html::error($horario, $id . 'fim_horario', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo Html::activeLabelEx($horario, $id . 'diaSemana', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeDropDownList(
                $horario,
                $id . "diaSemana",
                $horario->diaSemana,
                array("class" => "m-wrap span6 select2", "multiple" => "multiple")
            ) ?>
            <?php echo Html::error($horario, $id . 'diaSemana', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <?php echo Html::errorSummary($localHorario); ?>
    <div class="control-group">
        <?php echo Html::activeLabelEx($localHorario, $id . 'dt_inicio', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeTextField(
                $localHorario,
                $id . 'dt_inicio',
                array('class' => 'm-wrap span6 date-picker')
            ); ?>
            <?php echo Html::error($localHorario, $id . 'dt_inicio', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo Html::activeLabelEx($localHorario, $id . 'dt_fim', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeTextField(
                $localHorario,
                $id . 'dt_fim',
                array('class' => 'm-wrap span6 date-picker')
            ); ?>
            <?php echo Html::error($localHorario, $id . 'dt_fim', array('class' => 'help-inline')); ?>
        </div>
    </div>

</fieldset>
<script>
    $('#<?= Html::activeId($horario, $id.'diaSemana')?>').ready(function () {
        $('#<?=Html::activeId($horario, $id . 'diaSemana')?> option[value=domingo]').attr('selected', <?=$horario->domingo ? 'true' : 'false'?>);
        $('#<?=Html::activeId($horario, $id . 'diaSemana')?> option[value=segunda]').attr('selected', <?=$horario->segunda ? 'true' : 'false'?>);
        $('#<?=Html::activeId($horario, $id . 'diaSemana')?> option[value=terca]').attr('selected', <?=$horario->terca ? 'true' : 'false'?>);
        $('#<?=Html::activeId($horario, $id . 'diaSemana')?> option[value=quarta]').attr('selected', <?=$horario->quarta ? 'true' : 'false'?>);
        $('#<?=Html::activeId($horario, $id . 'diaSemana')?> option[value=quinta]').attr('selected', <?=$horario->quinta ? 'true' : 'false'?>);
        $('#<?=Html::activeId($horario, $id . 'diaSemana')?> option[value=sexta]').attr('selected', <?=$horario->sexta ? 'true' : 'false'?>);
        $('#<?=Html::activeId($horario, $id . 'diaSemana')?> option[value=sabado]').attr('selected', <?=$horario->sabado ? 'true' : 'false'?>);
    });

    /* Função para a mascará dos campos data sem DatePicker */
    function Data(obj) {
        data = $(obj).val();
        data = data.replace(/\D/g, "");
        data = data.replace(/(\d{2})(\d)/, "$1/$2");
        data = data.replace(/(\d{2})(\d)/, "$1/$2");

        $(obj).val(data);

        dia = (data.substring(0, 2));
        mes = (data.substring(3, 5));
        ano = (data.substring(6, 10));

        // verifica o dia valido para cada mes 
        if (dia < 01 || dia > 31) {
            $(obj).addClass('error');
            return false;
        }

        // verifica se o mes e valido 
        if (mes < 01 || mes > 12) {
            $(obj).addClass('error');

            return false;
        }

        // verifica se e ano bissexto 
        if (mes == 2 && (dia < 01 || dia > 29 || (dia > 28 && (parseInt(ano / 4) != ano / 4)))) {
            $(obj).addClass('error');
            return false;
        }

        $(obj).removeClass('error');
    }
</script>