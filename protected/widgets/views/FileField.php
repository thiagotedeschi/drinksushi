
<link rel="stylesheet" type="text/css"
      href="<?php echo ASSETS_LINK; ?>/plugins/bootstrap-fileupload/bootstrap-fileupload.css"/>
<div class="fileupload fileupload-new" data-provides="fileupload">
    <div class="input-append">
        <div class="uneditable-input">
            <i class="<?= $icone ?> fileupload-exists"></i>
            <span class="fileupload-preview"></span>
        </div>
        <span class="btn btn-file">
            <span class="fileupload-new"><?= $texto_botao ?></span>
            <span class="fileupload-exists">Alterar</span>
            <?php echo $form->fileField($model, $atributo, $html_options); ?>
        </span>
        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
    </div>
</div>
<script src="<?php echo ASSETS_LINK; ?>/plugins/bootstrap-fileupload/bootstrap-fileupload.js"
        type="text/javascript"></script>
