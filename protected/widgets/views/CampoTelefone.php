
<span id="span-telefone">
<?php
if ($atributoDDD) {
    echo $form->textField($model, $atributoDDD, $html_optionsDDD);
}

if ($atributoTel) {
    echo $form->textField($model, $atributoTel, $html_optionsTel);
}

if ($atributoRamal) {
    echo $form->textField($model, $atributoRamal, $html_optionsRamal);
}
?>
</span>
<script language='JavaScript'>
    //Chama a função soNums ao apertar qualquer tecla em um dos campos do Widget
    $('.w-telefone').bind('keydown', soNums);

    //Quando o campo DDD tem 2 caracteres muda o foco para Telefone
    $("#<?=Html::activeId($model, $atributoDDD)?>").keyup(function (e) {
        var tamanho = $(this).val().length;
        if (tamanho == 2 && e.which != 9)
            $("#<?=Html::activeId($model, $atributoTel)?>").focus();
    })

    /* Função transferida para o arquivo mascara.js */
    //Quando o campo Telefone tem 9 caracteres muda o foco para Ramal
    /*$("#<?=Html::activeId($model, $atributoTel)?>").keyup(function (e) {
     var tamanho = $(this).val().length;
        if (tamanho == 9 && e.which != 9)
            $("#<?=Html::activeId($model, $atributoRamal)?>").focus();
     })*/

    //Função que controla a entrada de caracteres
    function soNums(e) {
        //Pega a tecla digitada
        keyCode = e.which;
        var allowedKeys = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 8, 9, 37, 39, 46];
        //Verifica se a tecla digitada é permitida
        if ($.inArray(keyCode, allowedKeys) != -1)
            return true;

        return false;
    }
</script>

<style type="text/css">
    #span-telefone input {
        margin-right: 2px;
    }
</style>