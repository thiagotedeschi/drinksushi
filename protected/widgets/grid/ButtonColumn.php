<?php

Yii::import('zii.widgets.grid.CGridColumn');


class ButtonColumn extends CButtonColumn
{

    //opções dos elementos html (botões)
    public $viewButtonOptions = array('class' => 'botao view', 'title' => 'Visualizar');
    public $updateButtonOptions = array('class' => 'botao edit', 'title' => 'Editar');
    public $deleteButtonOptions = array('class' => 'botao botao-danger', 'title' => 'Excluir');
    //como não vamos utilizar imagens, setamos estas propriedades como false
    public $deleteButtonImageUrl = false;
    public $updateButtonImageUrl = false;
    public $viewButtonImageUrl = false;
    //label (com icones) para cada um dos botões
    public $deleteButtonLabel = '"<i class=\"icon-minus-square icon-large\"></i>"';
    public $updateButtonLabel = '"<i class=\"icon-edit icon-large\"></i>"';
    public $viewButtonLabel = '"<i class=\"icon-eye icon-large\"></i>"';
    //opções  para o conjunto de botões
    public $htmlOptions = array('style' => '', 'class' => 'botao-group displayTableCell');
    //array contendo as opções que o usuário tem de elementos por página
    public $header = array('10' => '10', '20' => '20', '50' => '50', PHP_INT_MAX => 'Todos');
    //tamanho escolhido para o número de elementos por página (padrão=20)
    public $pageSize;
    //
    public $showHeader = true;

    /**
     * Renders the header cell content.
     * The default implementation simply renders {@link header}.
     * This method may be overridden to customize the rendering of the header cell.
     * sobreescrito para personalização do número de itens por página
     */
    protected function renderHeaderCellContent()
    {
        if ($this->showHeader) {
            $this->pageSize !== null ? : $this->pageSize = Yii::app()->user->getState(
                'pageSize',
                Yii::app()->params['defaultPageSize']
            );
            if (!(in_array($this->pageSize, array_keys($this->header)))) {
                $this->pageSize = Yii::app()->params['defaultPageSize'];
                Yii::app()->user->setState('pageSize', Yii::app()->params['defaultPageSize']);
            }
            echo is_array($this->header) ? Html::dropDownList(
                'nItensPorPagina',
                $this->pageSize,
                $this->header,
                array('onchange' => "$.fn.yiiGridView.update('{$this->grid->id}',{ data:{pageSize: $(this).val() }})")
            ) : $this->grid->blankDisplay;
        }
    }


    public function init()
    {

        $renderModalContent = false;
        foreach ($this->buttons as $id => $button) {
            if (strpos($this->template, '{' . $id . '}') === false) {
                unset($this->buttons[$id]);
            } elseif (isset($button['isModal'])) {
                $renderModalContent = true;
                if (!isset($this->buttons[$id]['url']) || $this->buttons[$id]['url'] == null) {
                    $this->buttons[$id]['url'] = 'Yii::app()->controller->createUrl("' . $id . '",array("id"=>$data->primaryKey,"in-modal"=>"in-modal"))';
                }
                if (!isset($this->buttons[$id]['options'])) {
                    $this->buttons[$id]['options'] = $this->{$id . 'ButtonOptions'};
                }
                $this->buttons[$id]['options']['onclick'] = '$("#modal-' . $id . ' > .modal-body > div > div").children("iframe").attr("src",$(this).attr("href"));$("#modal-' . $id . '").modal("show");return false;';
            }
        }
        if ($renderModalContent) {
            Yii::app()->controller->renderPartial(
                'application.widgets.grid.views.ModalContent',
                array('model' => $this->grid->dataProvider->model)
            );
        }
        return parent::init();
    }

    /**
     * Renders a link button.
     * @param string $id the ID of the button
     * @param array $button the button configuration which may contain 'label', 'url', 'imageUrl' and 'options' elements.
     * See {@link buttons} for more details.
     * @param integer $row the row number (zero-based)
     * @param mixed $data the data object associated with the row
     */
    protected function renderButton($id, $button, $row, $data)
    {
        if (isset($button['visible']) && !$this->evaluateExpression(
                $button['visible'],
                array('row' => $row, 'data' => $data)
            )
        ) {
            return;
        }
        $label = isset($button['label']) ? $this->evaluateExpression(
            $button['label'],
            array('data' => $data, 'row' => $row)
        ) : $id;
        $url = isset($button['url']) ? $this->evaluateExpression(
            $button['url'],
            array('data' => $data, 'row' => $row)
        ) : '#';
        $options = isset($button['options']) ? $button['options'] : array();
        if (!isset($options['title'])) {
            $options['title'] = $label;
        }
        if (isset($button['imageUrl']) && is_string($button['imageUrl'])) {
            echo Html::link(Html::image($button['imageUrl'], $label), $url, $options);
        } else {
            echo Html::link($label, $url, $options);
        }
    }


}

?>
