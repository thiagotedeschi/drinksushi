<?php

Yii::import('zii.widgets.grid.CGridView');
Yii::import('application.widgets.grid.ButtonColumn');
Yii::import('application.widgets.grid.CheckBoxColumn');
Yii::import('application.widgets.grid.DataColumn');


class GridView extends CGridView
{

    /**
     * Configurações de html/css do <table> da CGridView
     * @var string configurações html/css
     */
    public $itemsCssClass = 'table table-striped table-hover table-bordered flip-scroll';

    /**
     * Texto a ser mostrado que traz informações sobre a página atual e a quantidade de registros encontrados
     * @var string Texto a ser mostrado
     */
    public $summaryText = 'Exibindo de {start} à {end} de {count} registro(s).';
    public $summaryCssClass = 'float-right contador-grid-view';
    public $cssFile = false;

    /**
     * Valor a se renderizar em caso de entrada nula
     * @var string texto a ser mostrado
     */
    public $nullDisplay = ' - ';

    /**
     * Valor a se renderizar em caso de entrada em branco
     * @var string texto a ser mostrado
     */
    public $blankDisplay = '&nbsp;';

    /**
     * caso verdadeiro, adicionar o botão de novo registro, que se abrirá em um "modal"
     * @var bool
     */
    public $addNew = true;

    /**
     * @var String classe CSS para ser colocada no menu da paginação
     */
    public $pagerCssClass = 'pagination pagination-centered';

    /**
     * @var Classe da paginação a ser utilizada
     */
    public $pager = array('class' => 'application.widgets.grid.LinkPager');

    /**
     * Habilitar o historico de navegação da tabela na url, via get
     */
    public $enableHistory = true;
    /**
     *
     * Mostrar os flashes sempre que a tabela for atualizada
     */
    public $afterAjaxUpdate = 'function(id, data){getFlashes();}';

    /**
     * @var bool Mostrar Create em um modal
     */
    public $addInModal = false;

    /**
     * @var String Monta o Create sem menu, cabeçalho e rodapé
     */
    public $addInIframe = false;

    public $rowCssClass = array();

    public $htmlOptions = [];

    /**
     * @var String Texto que será exibido no botão de cadastro do model
     */
    public $labelBotaoCadastro = '';

    public function init()
    {
        if (empty($this->htmlOptions['title'])) {
            $this->htmlOptions['title'] = $this->dataProvider->model->labelModel();
        }
        parent::init();
        !$this->addNew ? : $this->widget(
            'application.widgets.grid.WMenuAcoes',
            array(
                'AddRoute' => (!$this->addInModal ? ($this->addInIframe ? $this->controller->createUrl(
                            'create'
                        ) . "&in-modal=in-modal" : $this->controller->createUrl('create')) : ''),
                'addInModal' => $this->addInModal,
                'gridId' => $this->id,
                'labelBotaoCadastro' => $this->labelBotaoCadastro,
                'model' => $this->dataProvider->model
            )
        );
    }


}

?>
