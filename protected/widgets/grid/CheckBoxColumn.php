<?php

/**
 * @package base.Widgets.Grid
 */
Yii::import('zii.widgets.grid.CCheckBoxColumn');

class CheckBoxColumn extends CCheckBoxColumn
{

    public $checkBoxHtmlOptions = array('class' => 'toggle');

    public function __construct($grid)
    {
        parent::__construct($grid);
    }

    /**
     * Renderiza um ícone de busca para indicar os filtros mais facilmente
     */
    public function renderFilterCellContent()
    {
        echo '<i '
            . 'class="icon icon-search icon-large"'
            . 'title="Utilize os campos ao lado para filtrar a tabela pelas informações contidas em suas respectivas colunas "'
            . '></i>';
    }

}

?>
