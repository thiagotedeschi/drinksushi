<?php


class LinkPager extends CLinkPager
{

    const CSS_FIRST_PAGE = 'first';
    const CSS_LAST_PAGE = 'last';
    const CSS_PREVIOUS_PAGE = 'previous';
    const CSS_NEXT_PAGE = 'next';
    const CSS_INTERNAL_PAGE = 'page';
    const CSS_HIDDEN_PAGE = 'hidden';
    const CSS_SELECTED_PAGE = 'disabled';

    public $hiddenPageCssClass = self::CSS_SELECTED_PAGE;
    public $selectedPageCssClass = self::CSS_SELECTED_PAGE;
    public $cssFile = false;
    public $header = false;

}

?>
