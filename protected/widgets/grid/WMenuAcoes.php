<?php


class WMenuAcoes extends CWidget
{

    /**
     * @var String Caminho para a action de criação no formato ModuleID/ControllerID/ActionID
     */
    public $AddRoute = '';

    /**
     * @var Boolean se a action de criar deve ser renderizada dentro do modal ou em outra página
     */
    public $addInModal = false;

    /**
     * @var String ID da grid (GridView) que deve ser atualizada depois da inserção
     */
    public $gridId = '';

    /**
     * @var String Texto que será exibido no botão de cadastro do model
     */
    public $labelBotaoCadastro = '';

    /**
     * @var Array links que perfomam ações coletivas na GridView0
     */
    public $acoesColetivas = array();

    /**
     * @var Ação básica de exclusão em massa de registros selecionados desta tabela
     * caso seja setada como false, está opção não será inserida no menu de acoes
     */
    public $acaoDeletarColetiva = array();
    public $model;


    public function init()
    {
        if ($this->acaoDeletarColetiva !== false && empty($this->acaoDeletarColetiva)) {
            $this->acaoDeletarColetiva = array(
                'label' => '<i class="icon-trash-o"></i> Excluir Marcados',
                'href' => $this->controller->createUrl('delete', array('id' => '')),
                'ajax' => array(
                    'type' => 'POST',
                    'data' => 'js:{id:$.fn.yiiGridView.getSelection("' . $this->gridId . '")}',
                    'id' => 'simple-link-' . uniqid(),
                    'beforeSend' => 'function()
                        {
                        $(".botao-group .open").removeClass("open");
                          if(confirm("Deseja realmente remover permanentemente todos os registros selecionados?")){
                            if($.fn.yiiGridView.getSelection("' . $this->gridId . '").length >0)
                                {
                                    $("#' . $this->gridId . '").addClass("grid-view-loading")
                                } 
                                else 
                                {
                                    alert("Selecione os registros que deseja excluir");
                                    return false
                                }
                           }
                         else return false;
                         }',
                    'success' => 'function(html){
                        $.fn.yiiGridView.update("' . $this->gridId . '"); 
                            
                        }'
                ),
                'htmlOptions' => array(),
            );
        }
        if (!empty($this->acaoDeletarColetiva)) {
            $this->acoesColetivas[] = $this->acaoDeletarColetiva;
        }
    }

    public function run()
    {
        if (!$this->labelBotaoCadastro) {
            $this->labelBotaoCadastro = 'Cadastrar ' . $this->model->labelModel();
        }
        echo Html::openTag('div', array('class' => 'menu-acoes', 'style' => 'margin-bottom:10px'));
        //botão "Novo Registro"
        if ($this->controller->verificaAcao('create')) {
            echo $this->getBotaoNovoRegistro();
        }
        //Botão de atualizar
        echo $this->getBotaoAtualizar();
        echo Html::closeTag('div');
    }

    /**
     * Retorna a parte do menu com as acoes coletivas
     * caso não existam açoes coletivas no array self::acoesColetivas, null será retornado
     * @return null|String
     */
    public function getAcoesColetivas()
    {
        if (empty($this->acoesColetivas)) {
            return null;
        }
        $menu = '<li class="dropdown-submenu">
                <a class="dropdown-toggle" data-toggle="dropdown">Ações</a>
                <ul class="dropdown-menu">';
        foreach ($this->acoesColetivas as $acao) {
            $menu .= '<li>' . Html::ajaxLink(
                    $acao['label'],
                    $this->controller->createUrl('delete', array('id' => '')),
                    $acao['ajax'],
                    $acao['htmlOptions']
                ) . '</li>';
        }
        $menu .= '</ul></li>';
        return $menu;
    }

    public function getBotaoNovoRegistro()
    {
        $linkOptions = array('class' => 'botao botao-success');
        if ($this->addInModal) {
            $linkOptions['data-target'] = '#modal-create';
            $linkOptions['data-toggle'] = 'modal';
            Yii::app()->controller->renderPartial(
                'application.widgets.grid.views.AddModalContent',
                array('model' => $this->model)
            );
        }
        echo Html::link(
            '<i class="icon-plus"></i> ' . $this->labelBotaoCadastro,
            $this->AddRoute,
            $linkOptions
        );

    }


    public function getAcoesExportar()
    {
        $menu = '<li class="dropdown-submenu">
                <a class="dropdown-toggle" data-toggle="dropdown">Exportar </a>
                <ul class="dropdown-menu">';
        $menu .= '<li><a class="grid-view-export" from="' . $this->gridId . '" target="PDF" href="' . YII::app(
            )->createUrl('export/export') . '"><i class="icon-print"></i> PDF</a></li>';
        $menu .= '<li><a class="grid-view-export" from="' . $this->gridId . '" target="CSV" href="' . YII::app(
            )->createUrl('export/export') . '"><i class="icon-th"></i> CSV</a></li>';
        $menu .= '<li><a class="grid-view-export" from="' . $this->gridId . '" target="XML" href="' . YII::app(
            )->createUrl('export/export') . '"><i class="icon-file"></i> XML</a></li>';
        $menu .= '</ul></li>';
        return $menu;
    }

    public function getBotaoAtualizar()
    {
//        $botaoAtualizar = Html::link(
//            '<i class="icon-refresh"></i> Atualizar',
//            '#',
//            array(
//                'onclick' => '$.fn.yiiGridView.update(\'' . $this->gridId . '\')',
//                'class' => 'botao',
//                // 'style'=>'height: 20px;',
//                'title' => 'Atualizar'
//            )
//        );
//        $botaoAtualizar .= Html::openTag('div', array('class' => 'botao-group'));
//        $botaoAtualizar .= Html::tag(
//            'button',
//            array(
//                'class' => 'botao dropdown-toggle',
//                'style' => 'height:30px',
//                'data-toggle' => 'dropdown'
//            ),
//            html::tag('i', array('class' => 'icon-chevron-down'), '', true),
//            true
//        );
//        $botaoAtualizar .= Html::openTag('ul', array('class' => 'dropdown-menu'));
//        if ($this->controller->verificaAcao('delete')) {
//            $botaoAtualizar .= $this->getAcoesColetivas();
//        }
//        //Ações de Exportar
//        $botaoAtualizar .= $this->getAcoesExportar();
//        $botaoAtualizar .= Html::closeTag('ul');
//        $botaoAtualizar .= Html::closeTag('div');
//        return $botaoAtualizar;

    }

}

?>