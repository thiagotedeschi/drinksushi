<?php
Yii::import('zii.widgets.CDetailView');


class DetailView extends CDetailView
{
    public $htmlOptions = array('class' => ' detail-view table table-hover table-bordered table-striped');

    public $nullDisplay = '<span class="null-display">(Vazio)</span>';

    public $showBotaoVoltar = true;


    public function init()
    {
        if ($this->cssFile === null) {
            $this->cssFile = ASSETS_LINK . '/css/detail-view.css';
        }
        $init = parent::init();

        return $init;

    }


    public function run()
    {
        $run = parent::run();
        if ($this->showBotaoVoltar && !$this->getController()->isModalRequest()) {
            echo Html::link(
                '',
                $this->getController()->createUrl('search'),
                array('class' => 'm-icon-big-swapleft print-hidden float-left', 'style' => 'margin: 8px 8px 10px 8px')
            );
        }
        return $run;

    }
}
