<?php


class WDataRange extends CWidget
{

    /**
     * Representação do início do intervalo
     * @var String
     */
    public $startAttribute = '';

    /**
     * Representação do fim do intervalo
     * @var String
     */
    public $endAttribute = '';

    /**
     * Modelo
     * @var CAtiveRecord
     */
    public $model = '';

    /**
     * HtmlOptions para o atributo a ser renderizado
     * @var Array
     */
    public $labelFieldHtmlOptions = array();

    /**
     * atributo a ser renderizado
     * @var String
     */
    public $labelFieldAttribute = '';

    public function init()
    {
        if (isset($this->labelFieldHtmlOptions['class'])) {
            $this->labelFieldHtmlOptions['class'] .= ' date-range';
        } else {
            $this->labelFieldHtmlOptions['class'] = ' date-range m-wrap span3';
        }
        $this->labelFieldHtmlOptions['readonly'] = 'readonly';
        $this->labelFieldHtmlOptions['style'] = 'cursor:pointer;';
    }

    public function run()
    {
        $this->render(
            'DataRange',
            array(
                'startAttribute' => $this->startAttribute,
                'endAttribute' => $this->endAttribute,
                'model' => $this->model,
                'labelFieldHtmlOptions' => $this->labelFieldHtmlOptions,
                'labelFieldAttribute' => $this->labelFieldAttribute,
            )
        );
    }

}
