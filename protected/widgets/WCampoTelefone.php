<?php


class WCampoTelefone extends CWidget
{

    /**
     *
     * @var CAtiveForm form onde a widget está localizada
     */
    public $form;

    /**
     *
     * @var CActiveRecord model ao qual o formulário pertence
     */
    public $model;

    /**
     *
     * @var String nome do atributo correspondente ao DDD
     */
    public $atributoDDD;

    /**
     *
     * @var String nome do atributo correspondente ao Telefone
     */
    public $atributoTel;

    /**
     *
     * @var String nome do atributo correspondente ao Ramal
     */
    public $atributoRamal;

    /**
     *
     * @var Array html_options vetor contendo as propriedades html do campo DDD
     * @example 'class'=>'botao'
     */
    public $html_optionsDDD = array();

    /**
     *
     * @var Array html_options vetor contendo as propriedades html do campo Telefone
     * @example 'class'=>'botao'
     */
    public $html_optionsTel = array();

    /**
     *
     * @var Array html_options vetor contendo as propriedades html do campo Ramal
     * @example 'class'=>'botao'
     */
    public $html_optionsRamal = array();

    /**
     *
     * Renderiza a Widget
     */
    public function run()
    {
        $this->render(
            'CampoTelefone',
            array(
                'form' => $this->form,
                'model' => $this->model,
                'atributoDDD' => $this->atributoDDD,
                'atributoTel' => $this->atributoTel,
                'atributoRamal' => $this->atributoRamal,
                'html_optionsDDD' => $this->html_optionsDDD,
                'html_optionsTel' => $this->html_optionsTel,
                'html_optionsRamal' => $this->html_optionsRamal,
            )
        );
    }


    public function init()
    {
        if (isset($this->html_optionsDDD['class'])) {
            $this->html_optionsDDD['class'] .= ' w-telefone mask_ddd';
        } else {
            $this->html_optionsDDD['class'] = 'span1 m-wrap w-telefone mask_ddd';
        }

        if (isset($this->html_optionsTel['class'])) {
            $this->html_optionsTel['class'] .= ' w-telefone telefone-mask';
        } else {
            $this->html_optionsTel['class'] = 'span2 m-wrap w-telefone telefone-mask';
        }

        if (isset($this->html_optionsRamal['class'])) {
            $this->html_optionsRamal['class'] .= ' w-telefone';
        } else {
            $this->html_optionsRamal['class'] = 'span1 m-wrap w-telefone';
        }

        if (!isset($this->html_optionsTel['placeholder'])) {
            $this->html_optionsTel['placeholder'] = 'Telefone';
        }

        $this->html_optionsRamal['maxlength'] = '5';
        $this->html_optionsRamal['placeholder'] = 'Ramal';
        $this->html_optionsDDD['maxlength'] = '2';
        $this->html_optionsDDD['placeholder'] = 'DDD';
        $this->html_optionsTel['maxlength'] = '10';
    }

}
