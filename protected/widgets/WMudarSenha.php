<?php
/**
 * @package base.Widgets
 */
class WMudarSenha extends CWidget
{

    /**
     * URL da action que será usada pra mudar a senha
     * @var String
     */
    public $actionURL = '/Administracao/usuario/MudarSenha';

    /**
     * Model que está sendo usado
     * @var FMudarSenha
     */
    public $model = null;


    public function run()
    {
        $this->render(
            'MudarSenha',
            array(
                'model' => $this->model,
                'actionURL' => $this->actionURL
            )
        );
    }

    public function init()
    {
        if (!$this->model) {
            $this->model = new FMudarSenha;
        }
    }

}
