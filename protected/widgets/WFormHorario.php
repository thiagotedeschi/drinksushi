<?php


class WFormHorario extends CWidget
{

    /**
     *
     * @var horario model do Hndereco a ser preenchido
     */
    public $horario;

    /**
     *
     * @var localHorario model da ligação entre Horario e Local
     */
    public $localHorario;

    /**
     *
     * @var CAtiveForm form no qual a widget está
     */
    public $form;

    /**
     *
     * @var String label para o fieldset
     */
    public $label;

    /**
     *
     * @var string ID da widget, que irá identificar a mesma e evitar conflitos caso exista
     * mais de um WFormEndereco no form
     */
    public $id = '';

    /**
     *
     * @var boolean para ser o horario de folga ou não
     */
    public $folga = false;

    /**
     * @var array com as opçoes Htmls do dropDowList
     */
    public $htmlOptions;

    public function run()
    {
        $this->render(
            'FormHorario',
            array(
                'horario' => $this->horario,
                'localHorario' => $this->localHorario,
                'form' => $this->form,
                'label' => $this->label,
                'id' => $this->id,
                'folga' => $this->folga,
            )
        );
    }

}

?>
