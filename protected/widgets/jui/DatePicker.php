<?php


Yii::import('zii.widgets.jui.CJuiDatePicker');

class DatePicker extends CJuiDatePicker
{
    /*
     * Idioma que será exibido a Widget
     */
    public $language = 'pt-BR';

    public $options = array(
        'dateFormat' => 'dd/mm/yy',
        'showButtonPanel' => 'true',
        'constrainInput' => 'true',
        'duration' => 'slow',
        'showAnim' => 'drop'
    );
    public $htmlOptions = array('class' => 'span6 m-wrap');
}

?>
