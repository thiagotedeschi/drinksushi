<?php


class WFormEndereco extends CWidget
{

    /**
     *
     * @var EnderecoCliente model do Endereco a ser preenchido
     */
    public $endereco;

    /**
     *
     * @var CAtiveForm form no qual a widget está
     */
    public $form;

    /**
     *
     * @var String label para o fieldset
     */
    public $label;

    /**
     *
     * @var string ID da widget, que irá identificar a mesma e evitar conflitos caso exista
     * mais de um WFormEndereco no form
     */
    public $id = '';

    /**
     *
     * @var String ID de qual outro WFormEndereco esse deve aproveitar informações se necessário for
     */
    public $aproveitarEndereco = null;

    /**
     *
     * @var boolean desabilitar o form por default ou não
     */
    public $desabilitarForm = false;

    public function run()
    {
        $this->render(
            'FormEndereco',
            array(
                'endereco' => $this->endereco,
                'form' => $this->form,
                'label' => $this->label,
                'id' => $this->id,
                'aproveitarEndereco' => $this->aproveitarEndereco,
                'desabilitarForm' => $this->desabilitarForm,
            )
        );
    }

}

?>
