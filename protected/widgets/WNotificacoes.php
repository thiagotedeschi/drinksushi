<?php


class WNotificacoes extends CWidget
{

    public $notificacoes = array();
    public $qt_naoLidas = 0;
    public $view = '';

    function init()

    {
        //busca somente até 10 notificções relacionadas a este usuario em ordem decrescente de notificação
        $this->notificacoes = Usuario::model()->findByPk(YII::app()->user->IDUsuario)->iDProfissional->notificacoes(
            array('limit' => '10', 'order' => '"dt_envioNotificacao" DESC')
        );

        $i = 0;

        //conta somente as não lidas para aparecer marcado em vermelho
        foreach ($this->notificacoes as $notificacao) {
            if ($notificacao->dt_leituraNotificacao == '') {
                $i++;
            }
        }
        $this->qt_naoLidas = $i;
    }

    function run()
    {
        $this->render($this->view);
    }
}

?>
