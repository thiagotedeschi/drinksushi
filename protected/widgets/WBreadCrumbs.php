<?php


class WBreadCrumbs extends CWidget
{

    /**
     * @var Id da ação atual
     */
    public $idAcao = '';

    /**
     * @var array crumbs
     */
    public $crumbs = array();
    /**
     * @var $separador icone da biblioteca Font Awesome)
     */
    public $separador = 'angle-right';
    /**
     * @var Outras crumbs para serem adicionadas ao final das geradas automaticamente
     */
    public $outrasCrumbs = array();

    public function init()
    {
        $this->crumbs = array_merge($this->crumbs, $this->outrasCrumbs);
        $this->render('BreadCrumbs'); //com os crumbs prontos, hora de renderizar
    }

}

?>