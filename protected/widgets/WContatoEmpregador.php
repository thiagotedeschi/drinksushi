<?php



class WContatoEmpregador extends CWidget
{
    /**
     * @var string view a ser renderizada
     */
    public $view = 'ContatoEmpregadorHeader';

    /**
     * @var String  Id do empregador
     */
    public $IDEmpregador;


    function run()
    {
        if (($empregador = Empregador::model()->findByPk($this->IDEmpregador)) == null) {
            return false;
        }
        $this->render(
            $this->view,
            array(
                'empregador' => $empregador,
            )
        );
    }

}