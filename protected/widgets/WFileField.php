<?php


class WFileField extends CWidget
{

    /**
     *
     * @var CAtiveForm form onde a widget está localizada
     */
    public $form;

    /**
     *
     * @var String atributo do modelo corresponde ao arquivo que será enviado
     */
    public $atributo;

    /**
     *
     * @var CActiveRecord model ao qual o formulário pertence
     */
    public $model;

    /**
     *
     * @var String texto_botao texto do botão que irá selecionar o arquivo para upload
     * como padrão exibe o texto  'Selecionar Arquivo';
     */
    public $texto_botao = 'Selecionar Arquivo';

    /**
     *
     * @var Array html_options vetor contendo as propriedades html do campo file
     * @example 'class'=>'botao'
     */
    public $html_options = array();

    /**
     *
     * @var String icone deve ser informada a classe do icone que deseja-se exibir
     * caso não seja informado, como padrão o icone será da classe 'icon-file'
     */
    public $icone = 'icon-file';


    public function init()
    {
        if (isset($this->html_options['class'])) {
            $this->html_options['class'] .= ' default';
        } else {
            $this->html_options['class'] = 'default';
        }
    }


    public function run()
    {
        $this->render(
            'FileField',
            array(
                'form' => $this->form,
                'atributo' => $this->atributo,
                'model' => $this->model,
                'html_options' => $this->html_options,
                'icone' => $this->icone,
                'texto_botao' => $this->texto_botao,
            )
        );
    }

}
