<?php

/**
 * @package base.Widgets
 */
class WIcones extends CWidget
{

    public $model;
    public $form;
    public $field;

    public function init()
    {
        ?>
        <script>

            function mostraIcone(input) {
                icone = $(input).val();
                $("#preview").attr('class', '');
                $("#preview").addClass('icon-' + icone + ' large');
            }
            jQuery(document).ready(function () {
                mostraIcone($('#<?= Html::activeId($this->model, $this->field) ?>'));
            });

        </script>
        <?php
        echo $this->form->labelEx($this->model, $this->field, array('class' => 'control-label'));
        echo '<div class="controls">';
        echo $this->form->textField(
            $this->model,
            $this->field,
            array('class' => 'span3 m-wrap', 'maxlength' => 20, 'onkeyup' => 'mostraIcone(this)')
        );
        echo '<a target="_blank" href="http://fontawesome.io/icons/"> Buscar Ícones</a>';
        echo '&nbsp;&nbsp;<i id="preview" class ="large" style="display:inline-block;padding-bottom:6px;" ></i>';
        echo $this->form->error($this->model, $this->field, array('class' => 'help-inline'));
        echo '</div>';
        echo '<br />';
    }

    public function run()
    {

    }

}

?>
