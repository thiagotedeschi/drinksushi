<?php

Yii::import('application.widgets.WAutoComplete');

class WAutoCompleteTrabalhador extends WAutoComplete
{
    /**
     * @var $linkView String link que direciona para a visualização dos dados do Trabalhador
     */
    public $linkView;


    public function init()
    {
        if (!isset($this->linkView)) {
            $this->linkView = Yii::app()->createUrl('Trabalhador/trabalhador/view');
        }
        if (!isset($this->ajaxUrl)) {
            $this->ajaxUrl = Yii::app()->createUrl("Trabalhador/trabalhador/ajaxTrabalhadorByTermoEmpregador");
        }
    }

    public function run()
    {
        $this->render(
            'AutoCompleteTrabalhador',
            array(
                'form' => $this->form,
                'model' => $this->model,
                'attributeName' => $this->attributeName,
                'ajaxUrl' => $this->ajaxUrl,
                'placeholder' => $this->placeholder,
                'multiple' => $this->multiple,
                'minimumInput' => $this->minimumInput,
                'params' => CJSON::encode($this->params),
                'htmlOptions' => $this->htmlOptions,
                'maxPageResults' => $this->maxPageResults,
                'linkView' => $this->linkView,
                'selectedText' => $this->selectedText,
                'IDLink' => $this->getIdlink(),
                'IDModal' => $this->getIdModal()
            )
        );
    }


    public function getIdLink()
    {
        return "link-trab-" . $this->attributeName;
    }

    public function getIdModal()
    {
        return "modal-trab-" . $this->attributeName;
    }

} 