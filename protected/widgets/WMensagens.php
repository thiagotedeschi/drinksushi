<?php

class WMensagens extends CWidget
{

    public $mensagens = array();
    public $qt_naoLidas = 0;

    function init()
    {
        //busca somente até 10 mensagens enviadas a este usuario em ordem decrescente de data de leitura
        $this->mensagens = Usuario::model()->findByPk(YII::app()->user->IDUsuario)->mensagensRecebidas(
            array('limit' => '10', 'order' => '"dt_leituraMensagem" DESC')
        );
        $i = 0;
        foreach ($this->mensagens as $mensagem) {
            if ($mensagem->dt_leituraMensagem == '') {
                $i++;
            }
        }
        $this->qt_naoLidas = $i;
    }

    function run()
    {
        ?>
        <li class="dropdown" id="header_inbox_bar">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <i class="icon-envelope"></i>
                <?= $this->qt_naoLidas > 0 ? '<span id="qt_notif" class="badge">  ' . $this->qt_naoLidas . '</span>' : '' ?>
            </a>

            <ul class="dropdown-menu extended inbox">
                <li>
                    <p><?= $this->qt_naoLidas > 0 ? 'Você têm ' . $this->qt_naoLidas . ' Mensagens novas.' : 'Você não possui mensagens não lidas.' ?></p>
                </li>
                <li>
                    <ul class="dropdown-menu-list scroller" style="height:250px">
                        <?php
                        foreach ($this->mensagens as $mensagem) {
                            $user = $mensagem->iDUsuarioRemetente();
                            ?>

                            <li class="<?= $mensagem->dt_leituraMensagem == '' ? 'msg_nao_lida' : '' ?>">
                                <a href="<?=
                                Yii::app()->createUrl(
                                    'Mensagem/view',
                                    array('IDMensagem' => $mensagem->IDMensagem)
                                ) ?>">
                                    <span class="photo"><img src="<?= $user->getFoto('32'); ?>" alt=""/></span>
                                    <span class="subject">
                                        <span class="from"><?= $user->login_usuario; ?></span>
                                        <span class="time"><?=
                                            HData::formataData(
                                                $mensagem->dt_envioMensagem,
                                                HData::BR_DATETIME_SIMPLE_FORMAT
                                            ); ?></span>
                                    </span>
                                    <span class="message">
                                        <?= $mensagem->conteudo_mensagem; ?>
                                    </span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </li>
                <li class="external">
                    <a href="#">Ver todas mensagens <i class="m-icon-swapright"></i></a>
                </li>
            </ul>
        </li>
    <?php
    }

}

?>
