<?php


class WAviso extends CWidget
{
    public $avisos;

    public $tipo = 'note-info-fillout';

    public function init()
    {
        if (is_array($this->avisos)) {
            foreach ($this->avisos as $aviso) {
                ?>
                <div class="note <?= $this->tipo ?>">
                    <!--<i class="icon-info-sign"></i>-->
                    <span><?= $aviso ?></span>
                </div>
            <?php
            }
        } elseif (!empty($this->avisos)) {
            ?>
            <div class="note <?= $this->tipo ?>">
                <!-- <i class="icon-info-sign"></i>-->
                <span><?= $this->avisos ?></span>
            </div>
        <?php
        }
    }

    public function run()
    {

    }
}

?>
