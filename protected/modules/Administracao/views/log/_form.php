<?php
/* @var $this LogController */
/* @var $model Log */
/* @var $form CActiveForm */
?>


<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'log-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
); ?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDAcao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'IDAcao', array('class' => 'm-wrap span6')); ?>
            <?php echo $form->error($model, 'IDAcao', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_log', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'dt_log', array('class' => 'm-wrap span6')); ?>
            <?php echo $form->error($model, 'dt_log', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDUsuario', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'IDUsuario', array('class' => 'm-wrap span6')); ?>
            <?php echo $form->error($model, 'IDUsuario', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'data_log', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'data_log', array('class' => 'm-wrap span6')); ?>
            <?php echo $form->error($model, 'data_log', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'id_dataLog', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'id_dataLog', array('class' => 'm-wrap span6')); ?>
            <?php echo $form->error($model, 'id_dataLog', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="form-actions">
        <?php echo Html::submitButton(
            $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações',
            array('class' => 'botao')
        ); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
