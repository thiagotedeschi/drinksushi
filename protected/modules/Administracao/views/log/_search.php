<?php
/* @var $this LogController */
/* @var $model Log */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
            'htmlOptions' => array(
                'class' => 'form-horizontal margin-0',
            )
        )
    );
    ?>


    <div class="control-group">
        <?php echo $form->label($model, 'IDAcao'); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDAcao',
                Html::listData(Acao::model()->ordemAlfabetica()->findAll(), 'IDAcao', 'nome_acao'),
                array('class' => 'm-wrap span4 select2', 'multiple' => 'multiple')
            ); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->label($model, 'inicioPeriodoLog'); ?>
        <div class="controls">
            <?php $this->widget(
                'application.widgets.WDataRange',
                array(
                    'model' => $model,
                    'startAttribute' => 'inicioPeriodoLog',
                    'endAttribute' => 'fimPeriodoLog',
                    'labelFieldAttribute' => 'dt_log',
                )
            )?>
        </div>
    </div>

    <div class="control-group">

        <?php echo $form->label($model, 'IDUsuario'); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDUsuario',
                Html::listData(Usuario::model()->ordemAlfabetica()->findAll(), 'IDUsuario', 'login_usuario'),
                array('class' => 'm-wrap span4 select2', 'multiple' => 'multiple')
            ); ?>
        </div>
    </div>


    <div class="control-group">
        <?php echo $form->label($model, 'id_dataLog'); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'id_dataLog', array('class' => 'm-wrap span2')); ?>
        </div>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Buscar', array('class' => 'botao')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->