<?php
/* @var $this LogController */
/* @var $model Log */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
//Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
Yii::app()->clientScript->registerScript(
    'search',
    "
   $('.search-button').click(function(){
       $('.search-form').slideToggle();
       return false;
   });
   $('.search-form form').submit(function(){
       $('#log-grid').yiiGridView('update', {
           data: $(this).serialize()
       });
       return false;
   });
   "
);
?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>
<?php echo Html::link(
    '<i class="icon-search"> </i>Pesquisa Avançada',
    '#',
    array('class' => 'search-button botao')
); ?>
<br/>
<div class="search-form" style="display:none">
    <br/>
    <?php
    $this->renderPartial(
        '_search',
        array(
            'model' => $model,
        )
    );
    ?>
</div><!-- search-form -->
<br/>

<?php
$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'log-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDAcao' => array(
                'name' => 'IDAcao',
                'value' => '$data->iDAcao->nome_acao',
                'header' => 'Nome da Ação',
                'filter' => Html::listData(Acao::model()->ordemAlfabetica()->findAll(), 'IDAcao', 'nome_acao')
            ),
            'dt_log' => array(
                'name' => 'dt_log',
                'filter' => '',
                'type' => 'datetime'
            ),
            'IDUsuario' => array(
                'name' => 'IDUsuario',
                'value' => '$data->iDUsuario->login_usuario',
                'header' => 'Login do Usuário',
                'filter' => Html::listData(
                        Usuario::model()->ordemAlfabetica()->findAll(),
                        'IDUsuario',
                        'login_usuario'
                    )
            ),
            'id_dataLog',
            /*
              'IDLog',
              'data_log',
             */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'header' => array('10' => '10', '20' => '20', '50' => '50', '100' => '100', '200' => '200'),
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
