<?php
/* @var $this LogController */
/* @var $data Log */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDLog')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDLog), array('view', 'id' => $data->IDLog)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDAcao')); ?>:</b>
    <?php echo Html::encode($data->IDAcao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_log')); ?>:</b>
    <?php echo Html::encode($data->dt_log); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDUsuario')); ?>:</b>
    <?php echo Html::encode($data->IDUsuario); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('data_log')); ?>:</b>
    <?php echo Html::encode($data->data_log); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('id_dataLog')); ?>:</b>
    <?php echo Html::encode($data->id_dataLog); ?>
    <br/>


</div>