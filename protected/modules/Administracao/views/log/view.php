<?php
/* @var $this LogController */
/* @var $model Log */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDLog:text:ID Log',
            'iDAcao.nome_acao:text:Ação Executada',
            'dt_log:datetime:Hora da Execução',
            'iDUsuario.login_usuario',
            'id_dataLog:text:Provável ID da Entidade Envolvida',
            'data_log' => array(
                'value' => $this->renderPartial('_dadosLog', array('dataLog' => $model->getDataLog()), true),
                'label' => 'Dados Envolvidos na Transação',
                'type' => 'raw'
            ),
        ),
    )
); ?>
