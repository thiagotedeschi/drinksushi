<?php
/* @var $this AtributoLocalController */
/* @var $model AtributoLocal */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

?>

<p>
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php
$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'atributo-local-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'addInModal' => true,
        'columns' => array(
            'IDAtributo',
            'atributo',
            'valor_atributo',
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false'),
                            'isModal' => true
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false'),
                            'isModal' => true
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
