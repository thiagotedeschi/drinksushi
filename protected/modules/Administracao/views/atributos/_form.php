<?php
/* @var $this AtributoLocalController */
/* @var $model AtributoLocal */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'atributo-local-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('class' => 'form-horizontal margin-0')
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'atributo', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'atributo',
                array('class' => 'm-wrap span6', 'size' => 50, 'maxlength' => 50)
            ); ?>
            <?php echo $form->error($model, 'atributo', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'valor_atributo', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'valor_atributo',
                array('class' => 'm-wrap span6', 'size' => 80, 'maxlength' => 250)
            ); ?>
            <?php echo $form->error($model, 'valor_atributo', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
