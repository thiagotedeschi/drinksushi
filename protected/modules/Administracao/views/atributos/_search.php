<?php
/* @var $this AtributoLocalController */
/* @var $model AtributoLocal */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDAtributo'); ?>
        <?php echo $form->textField($model, 'IDAtributo'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'atributo'); ?>
        <?php echo $form->textField($model, 'atributo', array('size' => 50, 'maxlength' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'valor_atributo'); ?>
        <?php echo $form->textArea($model, 'valor_atributo', array('rows' => 6, 'cols' => 50)); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->