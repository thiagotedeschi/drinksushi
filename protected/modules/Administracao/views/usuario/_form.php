<?php
/* @var $this UsuarioController */
/* @var $model Usuario */
/* @var $form CActiveForm */
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'usuario-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('class' => 'form-horizontal margin-0')
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>

    <?php echo $form->errorSummary($model); ?>
    <fieldset>
        <legend>Dados Gerais</legend>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'login_usuario', array('class' => 'control-label')); ?>
            <div class="controls">
                <?=
                $model->isNewRecord ? $form->textField(
                    $model,
                    'login_usuario',
                    array('class' => 'm-wrap span6', 'size' => 60, 'maxlength' => 255)
                ) : '<p class="muted"><b>' . $model->login_usuario . '</b></p>' ?>
                <?php echo $form->error($model, 'login_usuario', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDProfissional', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                $profs = Html::listData(
                    Profissional::model()->findDesassociados($model->isNewRecord, $model->IDProfissional),
                    'IDProfissional',
                    'nome_profissional'
                );
                ?>
                <?php echo $form->dropDownList($model, 'IDProfissional', $profs, array('class' => 'select2')); ?>
                <?php echo $form->error($model, 'IDProfissional', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'tempo_utilizacaoUsuario', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'tempo_utilizacaoUsuario', array('class' => 'm-wrap span6')); ?>
                <?php echo $form->error($model, 'tempo_utilizacaoUsuario', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <?php
        if (!$model->isNewRecord) {
            echo Html::ajaxLink(
                '<i class="icon-random icon-white"> </i> Forçar Mudança de Senha',
                $this->createUrl(
                    'mudarSenhaForcada',
                    array(
                        'id' => $model->IDUsuario
                    )
                ),
                array(
                    'beforeSend' => 'function(){'
                        . '$("#trocaSenha").text("Carregando...")'
                        . ''
                        . '}',
                    'complete' => 'function(){'
                        . '$("form").submit()'
                        . ''
                        . '}'
                ),
                array(
                    'class' => 'botao botao-danger',
                    'id' => 'trocaSenha',
                    'confirm' => 'Deseja Mesmo Forçar a troca de senha? Uma nova senha randômica será enviada para o email do profissional.'
                )
            );
        }
        ?>
    </fieldset>
    <fieldset>
        <legend>Opções de Utilização</legend>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'grant_usuario', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->checkBox(
                    $model,
                    'grant_usuario',
                    array('class' => 'm-wrap span6', 'size' => 1, 'maxlength' => 1)
                ); ?>
                <?php echo $form->error($model, 'grant_usuario', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'admin_usuario', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->checkBox($model, 'admin_usuario'); ?>
                <?php echo $form->error($model, 'admin_usuario', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Grupos e Permissões</legend>
        <?php
        $usuario = Usuario::model()->findByPk(Yii::app()->user->IDUsuario);
        if ($usuario->admin_usuario) {
            ?>
            <div class="control-group">
                <?php echo $form->labelEx(new Grupo(), 'associar_grupo', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo Html::listBox(
                        'Grupos[IDGrupo]',
                        $model->getIdGrupos(),
                        Html::listData(
                            Grupo::model()->findAll(array('condition' => 'ativo_grupo = \'t\'')),
                            'IDGrupo',
                            'nome_grupo'
                        ),
                        array('class' => 'select2', 'multiple' => 'multiple')
                    ); ?>
                    <?php echo $form->error($model, 'concessaoGrupos', array('class' => 'help-inline')); ?>
                </div>
            </div>
        <?php
        } else {
            echo '<p class="alert alert-warning">Apenas Usuários Administradores podem atribuir grupos à usuários por aqui.'
                . ' Para utilizar seu poder de concessão (caso você tenha), vá no seu <b>Perfil > Configurações de Conta > Conceder Acesso</b>'
                . '.</p><br />';
        }
        ?>

    </fieldset>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
