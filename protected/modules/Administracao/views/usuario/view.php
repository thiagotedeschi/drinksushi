<?php

/* @var $this UsuarioController */
/* @var $model Usuario */
?>


<?php

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDUsuario',
            'login_usuario',
            'grant_usuario:boolean',
            'todas_empresasUsuario:boolean',
            'dt_criacaoUsuario:datetime',
            'tempo_utilizacaoUsuario',
            'dt_ultimaTrocaSenhaUsuario:date',
            'ativo_usuario:boolean',
            //'foto_usuario',
            //'senha_usuario',
            'IDProfissional' =>
                array(
                    'type' => 'raw',
                    'value' => $model->iDProfissional->nome_profissional,
                    'label' => $model->attributeLabels()['IDProfissional']
                ),
            'admin_usuario:boolean',
        ),
    )
);
?>
