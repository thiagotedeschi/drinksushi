<?php

/* @var $this UsuarioController */
/* @var $model Usuario */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php

$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'usuario-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDUsuario',
            'login_usuario',
            array(
                'name' => 'ativo_usuario',
                'id' => 'ativo_usuario',
                'value' => '$data->ativo_usuario ? "Sim" : "Não"',
                'filter' => array('1' => 'Sim', '0' => 'Não')
            ),
            // 'IDProfissional',
            array(
                'name' => 'IDProfissional',
                'id' => 'IDProfissional',
                'value' => '$data->iDProfissional()->nome_profissional',
                'filter' => Html::listData(Profissional::model()->findAll(), 'IDProfissional', 'nome_profissional')
            ),
//		'todas_empresasUsuario',
//		'dt_criacaoUsuario',
//		'tempo_utilizacaoUsuario',
//		'dt_ultimaTrocaSenhaUsuario',
//		'ativo_usuario',
//		'foto_usuario',
//		'senha_usuario',
//		'admin_usuario',
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{disable}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'disable' => array(
                            'visible' => ($this->verificaAcao('disable') ? 'true' : 'false'),
                            'options' => array('class' => 'botao disable', 'title' => 'Desabilitar'),
                            'imageUrl' => false,
                            // 'url' => 'Yii::app()->createUrl("users/email")',
                            'label' => '"<i class=\"icon-power-off icon-large\"></i>"',
                            'url' => 'Yii::app()->controller->createUrl("disable",array("id"=>$data->primaryKey))',
                            'click' => 'function(){
                        if(!confirm("deseja mesmo mudar o status deste Usuário?"))
                            return false;
                        $.ajax({
                            beforeSend: function(){
                               $(".grid-view").addClass("grid-view-loading")
                            },
                            method: "GET",
                            url: $(this).attr("href"),
                            success: function(){
                            	$(".grid-view").yiiGridView("update");
                                return false;
                            },
                            error: function(){return false;}
                            
                        });
                        return false;
                        }'
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
