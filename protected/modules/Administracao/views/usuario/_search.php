<?php
/* @var $this UsuarioController */
/* @var $model Usuario */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDUsuario'); ?>
        <?php echo $form->textField($model, 'IDUsuario'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'login_usuario'); ?>
        <?php echo $form->textField($model, 'login_usuario', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'grant_usuario'); ?>
        <?php echo $form->textField($model, 'grant_usuario', array('size' => 1, 'maxlength' => 1)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'todas_empresasUsuario'); ?>
        <?php echo $form->textField($model, 'todas_empresasUsuario', array('size' => 1, 'maxlength' => 1)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_criacaoUsuario'); ?>
        <?php echo $form->textField($model, 'dt_criacaoUsuario'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'tempo_utilizacaoUsuario'); ?>
        <?php echo $form->textField($model, 'tempo_utilizacaoUsuario'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_ultimaTrocaSenhaUsuario'); ?>
        <?php echo $form->textField($model, 'dt_ultimaTrocaSenhaUsuario'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ativo_usuario'); ?>
        <?php echo $form->textField($model, 'ativo_usuario', array('size' => 1, 'maxlength' => 1)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'foto_usuario'); ?>
        <?php echo $form->textField($model, 'foto_usuario', array('size' => 50, 'maxlength' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'senha_usuario'); ?>
        <?php echo $form->textField($model, 'senha_usuario', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDProfissional'); ?>
        <?php echo $form->textField($model, 'IDProfissional'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'admin_usuario'); ?>
        <?php echo $form->checkBox($model, 'admin_usuario'); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->