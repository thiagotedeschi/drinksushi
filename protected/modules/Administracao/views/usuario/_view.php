<?php
/* @var $this UsuarioController */
/* @var $data Usuario */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDUsuario')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDUsuario), array('view', 'id' => $data->IDUsuario)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('login_usuario')); ?>:</b>
    <?php echo Html::encode($data->login_usuario); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('grant_usuario')); ?>:</b>
    <?php echo Html::encode($data->grant_usuario); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('todas_empresasUsuario')); ?>:</b>
    <?php echo Html::encode($data->todas_empresasUsuario); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_criacaoUsuario')); ?>:</b>
    <?php echo Html::encode($data->dt_criacaoUsuario); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('tempo_utilizacaoUsuario')); ?>:</b>
    <?php echo Html::encode($data->tempo_utilizacaoUsuario); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_ultimaTrocaSenhaUsuario')); ?>:</b>
    <?php echo Html::encode($data->dt_ultimaTrocaSenhaUsuario); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('ativo_usuario')); ?>:</b>
	<?php echo Html::encode($data->ativo_usuario); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('foto_usuario')); ?>:</b>
	<?php echo Html::encode($data->foto_usuario); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('senha_usuario')); ?>:</b>
	<?php echo Html::encode($data->senha_usuario); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDProfissional')); ?>:</b>
	<?php echo Html::encode($data->IDProfissional); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('admin_usuario')); ?>:</b>
	<?php echo Html::encode($data->admin_usuario); ?>
	<br />

	*/
    ?>

</div>