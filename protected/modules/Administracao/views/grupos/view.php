<?php
/* @var $this GruposController */
/* @var $model Grupo */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDGrupo',
            'nome_grupo',
            'desc_grupo',
            'dt_criacaoGrupo:datetime',
            'ativo_grupo' => array(
                'label' => $model->attributeLabels()['ativo_grupo'],
                'value' => $model->ativo_grupo == 't' ? 'Sim' : 'Não'
            ),
            'acoes' => array(
                'label' => 'Ações Delegadas',
                'value' => $model->getListAcoes()
            )
        ),
    )
); ?>
