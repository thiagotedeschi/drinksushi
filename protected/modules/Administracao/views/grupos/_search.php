<?php
/* @var $this GruposController */
/* @var $model Grupo */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDGrupo'); ?>
        <?php echo $form->textField($model, 'IDGrupo'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_grupo'); ?>
        <?php echo $form->textField($model, 'nome_grupo', array('size' => 60, 'maxlength' => 60)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_grupo'); ?>
        <?php echo $form->textField($model, 'desc_grupo', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_criacaoGrupo'); ?>
        <?php echo $form->textField($model, 'dt_criacaoGrupo'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ativo_grupo'); ?>
        <?php echo $form->textField($model, 'ativo_grupo', array('size' => 1, 'maxlength' => 1)); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->