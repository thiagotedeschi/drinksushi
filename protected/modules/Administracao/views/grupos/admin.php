<?php

/* @var $this GruposController */
/* @var $model Grupo */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php

$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'grupo-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDGrupo',
            'nome_grupo',
            'desc_grupo',
            /*
              'dt_criacaoGrupo', */
            'ativo_grupo' => array(
                'id' => 'ativo_grupo',
                'name' => 'ativo_grupo',
                'value' => "\$data->ativo_grupo == 't'? 'Sim' : 'Não'",
                'filter' => array('t' => 'Sim', 'f' => 'Não')

            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{disable}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                        'disable' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false'),
                            'options' => array('class' => 'botao disable', 'title' => 'Desabilitar'),
                            'imageUrl' => false,
                            // 'url' => 'Yii::app()->createUrl("users/email")',
                            'label' => '"<i class=\"icon-power-off icon-large\"></i>"',
                            'url' => 'Yii::app()->controller->createUrl("disable",array("id"=>$data->primaryKey))',
                            'click' => 'function(){
                        if(!confirm("deseja mesmo mudar o status deste Grupo?"))
                            return false;
                        $.ajax({
                            beforeSend: function(){
                               $(".grid-view").addClass("grid-view-loading")
                            },
                            method: "GET",
                            url: $(this).attr("href"),
                            success: function(){
                            	$(".grid-view").yiiGridView("update");
                                return false;
                            },
                            error: function(){return false;}
                            
                        });
                        return false;
                        }'
                        ),
                    ),
            ),
        ),
    )
);
?>
