<?php
/* @var $this GruposController */
/* @var $data Grupo */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDGrupo')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDGrupo), array('view', 'id' => $data->IDGrupo)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_grupo')); ?>:</b>
    <?php echo Html::encode($data->nome_grupo); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_grupo')); ?>:</b>
    <?php echo Html::encode($data->desc_grupo); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_criacaoGrupo')); ?>:</b>
    <?php echo Html::encode($data->dt_criacaoGrupo); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('ativo_grupo')); ?>:</b>
    <?php echo Html::encode($data->ativo_grupo); ?>
    <br/>


</div>