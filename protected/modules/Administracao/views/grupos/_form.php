<?php
/* @var $this GruposController */
/* @var $model Grupo */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'grupo-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('class' => 'form-horizontal margin-0'),
    )
);
?>
<div class="form clearfix positionRelative">

    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>

    <?php echo $form->errorSummary($model); ?>
    <fieldset>
        <legend>Informações Básicas</legend>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'nome_grupo', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'nome_grupo',
                    array('class' => 'm-wrap span6', 'maxlength' => 60)
                ); ?>
                <?php echo $form->error($model, 'nome_grupo', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'desc_grupo', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textArea(
                    $model,
                    'desc_grupo',
                    array('class' => 'm-wrap span3', 'maxlength' => 255)
                ); ?>
                <?php echo $form->error($model, 'desc_grupo', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Permissões</legend>

        <div class="control-group">
            <?php echo $form->labelEx(new Acao(), 'nome_acao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo Html::listBox(
                    'Acao[IDAcao]',
                    $model->getAcoes(),
                    Html::listData($this->getAcoesCliente(CLIENTE), 'IDAcao', 'nome_acao', 'nomeModulo'),
                    array('class' => 'm-wrap span6 select2', 'multiple' => 'multiple')
                ); ?>
                <?php echo $form->error($model, 'desc_grupo', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </fieldset>
    <div class="form-actions">
        <?php echo Html::submitButton(
            $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações',
            array('class' => 'botao')
        ); ?>
    </div>


</div>
<?php $this->endWidget(); ?>
