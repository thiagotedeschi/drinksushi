<?php

/**
 * @package base.Administracao
 */
class UsuarioController extends Controller
{

    public $acoesActions = array(
        'create' => '161',
        'search' => '162',
        'view' => '163',
        'update' => '164',
        'delete' => '165',
        'disable' => '166',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Usuario::model()->labelModel();
        $model = new Usuario;

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation(array($model));

        if (isset($_POST['Usuario'])) {
            $model->attributes = $_POST['Usuario'];
            $model->ativo_usuario = true; // todo usuario e criado ativo
            $model->dt_criacaoUsuario = new CDbExpression('CURRENT_TIMESTAMP(0)'); //data de criaçao é sempre nesse instante
            $senha = HPassword::gerarPassword(); //gerar password randomico de 8 digitos
            $model->senha_usuario = new CDbExpression("crypt('" . $senha . "', gen_salt('md5'))"); //guardar o password criptografando com um salt

            if ($model->save()) { //salvando grupos caso tenham sido setados
                if (isset($_POST['Grupos']['IDGrupo']) && is_array($_POST['Grupos']['IDGrupo'])) {
                    foreach ($_POST['Grupos']['IDGrupo'] as $IDGrupo) {
                        $concessao = new ConcessaoGrupo();
                        $concessao->IDGrupo = $IDGrupo;
                        $concessao->IDUsuario = $model->IDUsuario;
                        $concessao->IDUsuario_concedeu = Yii::app()->user->IDUsuario;
                        $concessao->dt_inicio = $model->dt_criacaoUsuario;
                        $concessao->validate();
                        $concessao->save();
                    }
// die;
                }
//echo $this->createUrl('email/novoUsuario', array('id' => $model->IDUsuario, 'senha' => $senha)); die;
                $console = new CConsole();
                $console->runCommand('email novoUsuario', array('--id=' . $model->IDUsuario, '--senha=' . $senha));

                $this->redirect(array('search', 'id' => $model->IDUsuario));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Usuario::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);
        //var_dump($model->ativo_usuario);die;// !== NULL ? : $model->ativo_usuario == false;

        $this->performAjaxValidation($model);

        if (isset($_POST['Usuario'])) {
            $model->attributes = $_POST['Usuario'];
            //Prepara a foto
            if ($model->foto_usuario) {
                $model->foto_usuario = stream_get_contents($model->foto_usuario);
            }

            if ($model->save()) {

//salvando grupos caso tenham sido setados
                isset($_POST['Grupos']['IDGrupo']) ? $gruposAtuais = $_POST['Grupos']['IDGrupo'] : $gruposAtuais = array();
                $grupos = $model->getGrupos();
                $gruposAntigos = array();
                foreach ($grupos as $grupo) {
                    $gruposAntigos[] = $grupo->IDGrupo;
                }

//pega os ids dos dps grupos que não estarão mais associados ao usuário
                $excluirGrupos = array_diff($gruposAntigos, $gruposAtuais);
//os ids a serem acresentados eliminandos os já existentes
                $addGrupos = array_diff($gruposAtuais, $gruposAntigos);

                foreach ($addGrupos as $IDGrupo) {
                    $concessao = new ConcessaoGrupo();
                    $concessao->IDGrupo = $IDGrupo;
                    $concessao->IDUsuario = $model->IDUsuario;
                    $concessao->IDUsuario_concedeu = Yii::app()->user->IDUsuario;
                    $concessao->dt_inicio = new CDbExpression('CURRENT_TIMESTAMP(0)');
                    $concessao->validate();
                    $concessao->save();
                }
                foreach ($excluirGrupos as $IDGrupo) {
                    $concessoes = ConcessaoGrupo::model()->findAllByAttributes(
                        array('IDGrupo' => $IDGrupo, 'IDUsuario' => $model->IDUsuario)
                    );

                    foreach ($concessoes as $concessao) {
                        if ($concessao->dt_fim === null) {
                            $concessao->dt_fim = new CDbExpression('CURRENT_TIMESTAMP(0)');
                        }
                        $concessao->save();
                    }
                }

                $this->redirect(array('search', 'id' => $model->IDUsuario));
            }
        }
        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        $user = $this->loadModel($id);
        if ($user->IDUsuario == Yii::app()->user->IDUsuario) {
            Yii::app()->user->setFlash('error', array('Você não pode excluir seu próprio usuário!'));
        } else {
            $user->delete();
        }
// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function actionDisable($id)
    {
        $user = $this->loadModel($id);
        if ($user->IDUsuario == Yii::app()->user->IDUsuario) {
            Yii::app()->user->setFlash('error', array('Você não pode desativar seu próprio usuário!'));
            return;
        }
        $user->ativo_usuario = !$user->ativo_usuario;
        $user->update(['ativo_usuario']);

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('search'));
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Usuario::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Usuario('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Usuario'])) {
            $model->attributes = $_GET['Usuario'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Usuario the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Usuario::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Usuario $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usuario-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionMudarSenhaForcada($id)
    {
        $user = $this->loadModel($id);
        $senha = HPassword::gerarPassword(); //gerar password randomico de 8 digitos
        $user->senha_usuario = new CDbExpression("crypt('" . $senha . "', gen_salt('md5'))");
        $user->dt_ultimaTrocaSenhaUsuario = new CDbExpression('CURRENT_TIMESTAMP(0)');
        if ($user->save(array('senha_usuario', 'dt_ultimaTrocaSenhaUsuario'))) //enviar email com a senha
        {
            $console = new CConsole();
            $console->runCommand('email trocaSenhaForcada', array('--id=' . $user->IDUsuario, '--senha=' . $senha));
        }
    }


    public function actionMudarSenha()
    {
        $model = new FMudarSenha();
        if (isset($_POST["ajax"]) && $_POST["ajax"] === 'mudar-senha-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

// collect user input data
        if (isset($_POST['FMudarSenha'])) {
            $model->attributes = $_POST['FMudarSenha'];
            if ($model->validate()) {
                $model->changePassword();
            }
        }
        $this->redirect($this->createUrl('/perfil/', array('IDUsuario')));
    }


    public function actionMudarSenhaExpirada()
    {
        $model = new FMudarSenha();
        $this->layout = '//layouts/center';
        if (isset($_POST["ajax"]) && $_POST["ajax"] === 'mudar-senha-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
// collect user input data
        if (isset($_POST['FMudarSenha'])) {
            $model->attributes = $_POST['FMudarSenha'];
            if ($model->validate()) {
                $model->changePassword();
                $this->redirect($this->createUrl('/site/login', array('trocasenha' => 1)));
            }
        }
        $this->render('application.views.site.senhaExpirada', array('model' => $model));
    }

    public function actionUploadFoto()
    {
        Yii::import('ext.upload.Upload');
        if (!isset($_FILES['foto_perfil'])) {
            die;
        }

        $image = base64_encode(file_get_contents($_FILES['foto_perfil']['tmp_name']));

        echo CJSON::encode(
            array(
                'error' => '',
                'url' => $image,
                'imageName' => $_FILES['foto_perfil']['name']
            )
        );

    }


    public function actionCortarFoto()
    {
        if (!isset($_POST['imageName'])) {
            die;
        }

        //Mensagem a ser exibida caso haja algum erro no envio da imagem
        $mensagemErro = 'Ocorreu um erro.<br> Verifique se o arquivo enviado é uma imagem válida.';

        //carrega o usuário
        $user = $this->loadmodel(Yii::app()->user->IDUsuario);

        //caminho da pasta temporária
        $destPath = PATH . 'assets' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR;
        $filePath = $destPath . uniqid();
        $imagem = base64_decode($_POST['imageName']);
        //salva o arquivo na pasta temporário
        file_put_contents($filePath, $imagem);

        Yii::import('ext.upload.Upload');
        $Upload = new Upload($filePath);
        if ($Upload->file_is_image) {
            $dimensoes = getimagesize($filePath);
            //Corta a imagem
            $Upload->image_ratio_crop = true;
            $margemDireita = $dimensoes[0] - $_POST['w'] - $_POST['x'];
            $margemFundo = $dimensoes[1] - $_POST['h'] - $_POST['y'];
            $Upload->image_crop = "{$_POST['y']} $margemDireita $margemFundo {$_POST['x']}";
//            if ($_POST['w'] > 250) {
//                $Upload->process();
//                $Upload->image_ratio_crop = false;
//                $Upload->image_ratio_x = true;
//                $Upload->image_resize = true;
//                $Upload->image_x = 250;
//            }
            $user->foto_usuario = base64_encode($Upload->process());
        } else {
            Yii::app()->user->setFlash(
                'error',
                array($mensagemErro)
            );
        }

        //Deleta o arquivo temporário
        unlink($filePath);

        if ($user->save()) //seta o flash para dizer que deu certo
        {
            Yii::app()->user->setFlash('success', array('Foto alterada com sucesso!'));
        } else {
            Yii::app()->user->setFlash(
                'error',
                array($mensagemErro)
            );
        }
    }

}
