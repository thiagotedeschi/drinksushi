<?php

/**
 * @package base.Administracao
 */
class GruposController extends Controller
{

    public $acoesActions = array(
        'create' => '175',
        'search' => '174',
        'view' => '173',
        'update' => '176',
        'delete' => '177',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Grupo::model()->labelModel();
        $model = new Grupo;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Grupo'])) {
            $model->attributes = $_POST['Grupo'];
            $model->dt_criacaoGrupo = new CDbExpression('CURRENT_TIMESTAMP(0)');
            $model->ativo_grupo = 't';

            if ($model->save()) {
                if (isset($_POST['Acao']["IDAcao"])) {
                    $acoes = $_POST['Acao']["IDAcao"];
                    foreach ($acoes as $acao) {
                        $p = new Permissao();
                        $p->IDAcao = $acao;
                        $p->IDUsuario_concedeu = Yii::app()->user->IDUsuario;
                        $p->dt_associacaoPermissao = new CDbExpression('CURRENT_TIMESTAMP(0)');
                        $p->IDGrupo = $model->IDGrupo;
                        $p->save();
                    }
                }

                $this->redirect(array('search', 'id' => $model->IDGrupo));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Grupo::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['Grupo'])) {
            $model->attributes = $_POST['Grupo'];
            if ($model->save()) {
                //salvando as permissões caso tenham sido setadas

                isset($_POST['Acao']["IDAcao"]) ? $acoesAtuais = $_POST['Acao']["IDAcao"] : $acoesAtuais = array();

                $acoesAntigas = $model->getAcoes();

                //pega os ids dos dps grupos que não estarão mais associados ao usuário
                $excluirAcoes = array_diff($acoesAntigas, $acoesAtuais);
                //os ids a serem acresentados eliminandos os já existentes
                $addAcoes = array_diff($acoesAtuais, $acoesAntigas);

                foreach ($addAcoes as $acao) {
                    $p = new Permissao();
                    $p->IDAcao = $acao;
                    $p->IDUsuario_concedeu = Yii::app()->user->IDUsuario;
                    $p->dt_associacaoPermissao = new CDbExpression('CURRENT_TIMESTAMP(0)');
                    $p->IDGrupo = $model->IDGrupo;
                    $p->save();
                }
                foreach ($excluirAcoes as $acao) {
                    $permissoes = Permissao::model()->findAllByAttributes(
                        array('IDAcao' => $acao, 'IDGrupo' => $model->IDGrupo)
                    );

                    foreach ($permissoes as $permissao) {
                        if ($permissao->dt_fimPermissao === null) {
                            $permissao->dt_fimPermissao = new CDbExpression('CURRENT_TIMESTAMP(0)');
                        }
                        $permissao->save();
                    }
                }

                $this->redirect(array('search', 'id' => $model->IDGrupo));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Grupo::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Grupo('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Grupo'])) {
            $model->attributes = $_GET['Grupo'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Grupo the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Grupo::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Grupo $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'grupo-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionDisable($id)
    {
        $grupo = $this->loadModel($id);
        $grupo->ativo_grupo = $grupo->ativo_grupo == 't' ? 'f' : 't';
        $grupo->update(['ativo_grupo']);

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('search'));
        }
    }

}
