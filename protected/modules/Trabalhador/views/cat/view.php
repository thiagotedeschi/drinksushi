<?php
/* @var $this CatController */
/* @var $model CAT */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDCAT',
            'IDEmpregador' => array(
                'label' => $model->attributeLabels()['IDEmpregador'],
                'value' => Yii::app()->session['empregadores'][$model->IDEmpregador]
            ),
            'nome_trabalhador',
            array(
                'label' => $model->attributeLabels()['dt_emissaoCAT'],
                'value' => HData::sqlToBr($model->dt_emissaoCAT)
            ),
            'nr_CAT',
            array(
                'label' => $model->attributeLabels()['situacaoTrabalhadors'],
                'type' => 'raw',
                'value' => Html::activeList($model, 'situacaoTrabalhadors', 'labelSituacaoSetorFuncao')
            ),
            array(
                'label' => $model->attributeLabels()['dt_registroCAT'],
                'value' => HData::sqlToBr($model->dt_registroCAT)
            ),
            'dt_inicioAfastamento:date',
            'dt_fimAfastamento:date',
            'iDMotivoCAT.nome_motivoCAT:text:Motivo',
        ),
    )
); ?>
