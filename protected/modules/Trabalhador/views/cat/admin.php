<?php
/* @var $this CatController */
/* @var $model CAT */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada
    pesquisa.
</p>


<?php $this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'cat-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'enableHistory' => false,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'nr_CAT',
            'iDEmpregadorTrabalhador.iDEmpregador:text:Empregador',
            array(
                'header' => 'Trabalhador',
                'name' => 'nome_trabalhador',
                'value' => '$data->iDEmpregadorTrabalhador->iDTrabalhador'
            ),
            /*
            'nr_CAT',
            'dt_registroCAT',
            'dt_inicioAfastamento',
            'dt_fimAfastamento',
            'IDMotivoCAT',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
); ?>
