<?php
/* @var $this CatController */
/* @var $model CAT */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDCAT'); ?>
        <?php echo $form->textField($model, 'IDCAT', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDEmpregadorTrabalhador'); ?>
        <?php echo $form->textField($model, 'IDEmpregadorTrabalhador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_emissaoCAT'); ?>
        <?php echo $form->textField($model, 'dt_emissaoCAT', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nr_CAT'); ?>
        <?php echo $form->textField($model, 'nr_CAT', array('class' => 'm-wrap span6', 'maxlength' => 25)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_registroCAT'); ?>
        <?php echo $form->textField($model, 'dt_registroCAT', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_inicioAfastamento'); ?>
        <?php echo $form->textField($model, 'dt_inicioAfastamento', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_fimAfastamento'); ?>
        <?php echo $form->textField($model, 'dt_fimAfastamento', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDMotivoCAT'); ?>
        <?php echo $form->textField($model, 'IDMotivoCAT', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->