<?php
/* @var $this CatController */
/* @var $model CAT */
/* @var $form CActiveForm */

$model->IDEmpregador = $model->isNewRecord ? Yii::app()->user->getState('IDEmpregador') : $model->IDEmpregador;
if (!$model->isNewRecord) {
    $IDSituacaoTrabalhadors = array();
    foreach ($model->situacaoTrabalhadors as $situacaoTrabalhador) {
        $IDSituacaoTrabalhadors[] = $situacaoTrabalhador->IDSituacaoTrabalhador;
    }

    Yii::app()->clientScript->registerScript(
        'search',
        "
           $(document).ready(function(){
           $.ajax({
                url : '" . CController::createUrl('cat/AjaxSetorFuncaoByTrabalhador') . "',
                type: 'post',
                success: function(text){
                    jQuery('#" . Html::activeId($model, 'situacaoTrabalhadors') . "').html(text);
                },
                data: {
                    IDTrabalhador: $(\"#" . Html::activeId($model, 'IDTrabalhador') . "\").val(),
                    IDCAT: " . CJSON::encode($model->IDCAT) . ",
                },
                complete: function(){
                    $('#" . Html::activeId($model, 'situacaoTrabalhadors') . "').select2();
                    $('#" . Html::activeId($model, 'situacaoTrabalhadors') . "').select2(\"val\", " . CJSON::encode(
            $IDSituacaoTrabalhadors
        ) . ");
                    console.log(" . CJSON::encode($IDSituacaoTrabalhadors) . ");
                },
            })
        });"
    );
}
?>

<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'cat-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com * são obrigatórios.</p>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'IDEmpregador',
                    Yii::app()->session['empregadores'],
                    array(
                        'class' => 'select2 span4',
                        'prompt' => 'Escolha um Empregador',
                        'disabled' => $model->isNewRecord ? (Yii::app()->user->getState(
                                'IDEmpregador'
                            ) == '' ? false : true) : true,
                        'ajax' => array(
                            'url' => Yii::app()->createUrl('site/mudaEmpregador'),
                            'data' => "js:{IDEmpregador: $(this).val()}",
                            'type' => 'POST',
                            'complete' => ' function(){
                                                window.location = "";
                                            }'
                        )
                    )
                );
                if (Yii::app()->user->getState('IDEmpregador') != '') {
                    echo $form->hiddenField($model, 'IDEmpregador');
                }?>
                <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDTrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                if ($model->IDEmpregador != ''):
                    $this->widget(
                        'application.widgets.WAutoCompleteTrabalhador',
                        array(
                            'model' => $model,
                            'form' => $form,
                            'attributeName' => 'IDTrabalhador',
                            'placeholder' => 'Selecione o Trabalhador',
                            'params' => array('IDEmpregador' => $model->IDEmpregador),
                            'htmlOptions' => array(
                                'class' => 'span4',
                                'disabled' => !$model->isNewRecord,
                                'onchange' => '$.ajax({
                            url: "' . CController::createUrl('Cat/AjaxSetorFuncaoByTrabalhador') . '",
                            type: "post",
                            data: {
                                IDTrabalhador: $("#' . Html::activeId($model, 'IDTrabalhador') . '").val(),
                                IDEmpregador: ' . $model->IDEmpregador . ',
                                ' . ($model->isNewRecord ? '' : "IDCAT:" . $model->IDCAT) . '
                            },
                            beforeSend: function(){
                                $("#spinner").removeClass("hide");
                                $("#' . Html::activeId($model, 'situacaoTrabalhadors') . '").select2("val", "");
                            },
                            success: function(text){
                                $("#' . Html::activeId($model, 'situacaoTrabalhadors') . '").select2("val", "");
                                $("#' . Html::activeId($model, 'situacaoTrabalhadors') . '").html(text);
                                $("#spinner").addClass("hide");
                            },
                        })',
                            ),
                            'selectedText' => $model->nome_trabalhador
                        )
                    );
                else:
                    echo $form->dropDownList(
                        $model,
                        'IDTrabalhador',
                        array(),
                        array(
                            'class' => 'm-wrap span4',
                            'prompt' => 'Por favor, selecione um empregador!',
                            'disabled' => true
                        )
                    );
                endif;
                echo $form->error($model, 'IDTrabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'situacaoTrabalhadors', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'situacaoTrabalhadors',
                    array(),
                    array(
                        'class' => 'select2 span4',
                        'multiple' => true,
                        'placeholder' => 'Escolha a situaçao do trabalhador',
                    )
                );
                ?>
                <span style="margin-left: 1%;" id="spinner"><i class="icon-spinner icon-2x icon-spin"></i></span>
                <?php echo $form->error($model, 'situacaoTrabalhadors', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx(
                $model,
                'constar_documentosCat',
                array('class' => 'control-label')
            ); ?>
            <div class="controls">
                <?php echo $form->checkBox($model, 'constar_documentosCat'); ?>
                <?php echo $form->error(
                    $model,
                    'constar_documentosCat',
                    array('class' => 'help-inline')
                ); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDMotivoCAT', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                echo $form->dropDownList(
                    $model,
                    'IDMotivoCAT',
                    Html::listData(MotivoCAT::model()->findAll(), 'IDMotivoCAT', 'nome_motivoCAT'),
                    array(
                        'class' => 'span4 select2',
                        'prompt' => 'Selecione o Motivo',
                    )
                );
                ?>
                <?php echo $form->error($model, 'IDMotivoCAT', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_emissaoCAT', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'dt_emissaoCAT',
                    array('class' => 'm-wrap span6 date-picker', 'maxlength' => 10)
                ); ?>
                <?php echo $form->error($model, 'dt_emissaoCAT', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'nr_CAT', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'nr_CAT', array('class' => 'm-wrap span6', 'maxlength' => 25)); ?>
                <?php echo $form->error($model, 'nr_CAT', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_registroCAT', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'dt_registroCAT',
                    array('class' => 'm-wrap span6 date-picker', 'maxlength' => 10)
                );
                ?>
                <?php echo $form->error($model, 'dt_registroCAT', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_inicioAfastamento', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'dt_inicioAfastamento',
                    array('class' => 'm-wrap span6 date-picker', 'maxlength' => 10)
                );
                ?>
                <?php echo $form->error($model, 'dt_inicioAfastamento', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_fimAfastamento', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'dt_fimAfastamento',
                    array('class' => 'm-wrap span6 date-picker', 'maxlength' => 10)
                );
                ?>
                <?php echo $form->error($model, 'dt_fimAfastamento', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <?php echo Html::submitButton(
            $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
            array('class' => 'botao')
        );
        ?>
    </div>
    <script>
        $(function () {
            $("#spinner").addClass("hide");
            $("#spinner1").addClass("hide");
        });
    </script>
<?php $this->endWidget(); ?>