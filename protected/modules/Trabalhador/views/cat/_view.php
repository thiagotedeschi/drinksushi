<?php
/* @var $this CatController */
/* @var $data CAT */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDCAT')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDCAT), array('view', 'id' => $data->IDCAT)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDEmpregadorTrabalhador')); ?>:</b>
    <?php echo Html::encode($data->IDEmpregadorTrabalhador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_emissaoCAT')); ?>:</b>
    <?php echo Html::encode($data->dt_emissaoCAT); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nr_CAT')); ?>:</b>
    <?php echo Html::encode($data->nr_CAT); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDSituacaoTrabalhador')); ?>:</b>
    <?php echo Html::encode($data->IDSituacaoTrabalhador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_registroCAT')); ?>:</b>
    <?php echo Html::encode($data->dt_registroCAT); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_inicioAfastamento')); ?>:</b>
    <?php echo Html::encode($data->dt_inicioAfastamento); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_fimAfastamento')); ?>:</b>
    <?php echo Html::encode($data->dt_fimAfastamento); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('IDMotivoCAT')); ?>:</b>
	<?php echo Html::encode($data->IDMotivoCAT); ?>
	<br />

	*/
    ?>

</div>