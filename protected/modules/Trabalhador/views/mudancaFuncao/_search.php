<?php
/* @var $this MudancaFuncaoController */
/* @var $model TrabalhadorSetorFuncao */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'IDTrabalhadorSetorFuncao'); ?>
            <?php echo $form->textField($model,'IDTrabalhadorSetorFuncao',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDSetorFuncao'); ?>
            <?php echo $form->textField($model,'IDSetorFuncao',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dt_cadastro'); ?>
            <?php echo $form->textField($model,'dt_cadastro',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDTrabalhador'); ?>
            <?php echo $form->textField($model,'IDTrabalhador',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDOcupacao'); ?>
            <?php echo $form->textField($model,'IDOcupacao',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'cargo_TrabSetFunc'); ?>
            <?php echo $form->textField($model,'cargo_TrabSetFunc',array('class'=>'m-wrap span6','maxlength'=>30)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dt_inicioTrabSetFunc'); ?>
            <?php echo $form->textField($model,'dt_inicioTrabSetFunc',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dt_fimTrabSetFunc'); ?>
            <?php echo $form->textField($model,'dt_fimTrabSetFunc',array('class'=>'m-wrap span6')); ?>
        </div>

        <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->