<?php
/* @var $this MudancaFuncaoController */
/* @var $model TrabalhadorSetorFuncao */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget('application.widgets.DetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'IDTrabalhadorSetorFuncao',
        'iDEmpregadorTrabalhador.iDTrabalhador:text:Trabalhador',
        'iDSetorFuncao.iDSetor:text:Setor',
		'iDSetorFuncao.iDFuncao:text:Função',
		'dt_cadastro:date',
		'iDOcupacao.labelCBO:text:Ocupação CBO',
		'cargo_TrabSetFunc',
		'dt_inicioTrabSetFunc',
		'dt_fimTrabSetFunc',
	),
)); ?>
