<?php
/* @var $this MudancaFuncaoController */
/* @var $data TrabalhadorSetorFuncao */
?>

<div class="view">

    	<b><?php echo Html::encode($data->getAttributeLabel('IDTrabalhadorSetorFuncao')); ?>:</b>
	<?php echo Html::link(Html::encode($data->IDTrabalhadorSetorFuncao), array('view', 'id'=>$data->IDTrabalhadorSetorFuncao)); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDSetorFuncao')); ?>:</b>
	<?php echo Html::encode($data->IDSetorFuncao); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('dt_cadastro')); ?>:</b>
	<?php echo Html::encode($data->dt_cadastro); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->IDTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDOcupacao')); ?>:</b>
	<?php echo Html::encode($data->IDOcupacao); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('cargo_TrabSetFunc')); ?>:</b>
	<?php echo Html::encode($data->cargo_TrabSetFunc); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('dt_inicioTrabSetFunc')); ?>:</b>
	<?php echo Html::encode($data->dt_inicioTrabSetFunc); ?>
	<br />

	<?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('dt_fimTrabSetFunc')); ?>:</b>
	<?php echo Html::encode($data->dt_fimTrabSetFunc); ?>
	<br />

	*/ ?>

</div>