<?php
/* @var $this MudancaFuncaoController */
/* @var $model TrabalhadorSetorFuncao */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada
    pesquisa.
</p>

<?php if (!Yii::app()->user->getState('IDEmpregador')) { ?>
    <p class="note note-danger">
        Para visualizar essa página é necessário selecionar um <b>Empregador</b>.
    </p>
<?php } ?>

<?php $this->widget(
    'application.widgets.grid.GridView',
    array(
        'labelBotaoCadastro' => 'Nova Mudança de Função',
        'id' => 'trabalhador-setor-funcao-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            array(
                'name' => 'nomeTrabalhador',
                'value' => '$data->iDEmpregadorTrabalhador->iDTrabalhador',
                'header' => 'Trabalhador'
            ),
            array(
                'name' => 'nomeFuncao',
                'value' => '$data->iDSetorFuncao->iDFuncao',
                'header' => 'Função'
            ),
            array(
                'name' => 'nomeSetor',
                'value' => '$data->iDSetorFuncao->iDSetor',
                'header' => 'Setor'
            ),
            'dt_inicioTrabSetFunc',
            /*
            'IDTrabalhador',
            'IDOcupacao',
            'cargo_TrabSetFunc',
            'dt_inicioTrabSetFunc',
            'dt_fimTrabSetFunc',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);

?>
