<?php
/* @var $this MudancaFuncaoController */
/* @var $model TrabalhadorSetorFuncao */
/* @var $form CActiveForm */

$listaOcupacao = Html::listData(
    OcupacaoCBO::model()->with('iDFamilia')->findAll(),
    'IDOcupacao',
    'labelCBO',
    'iDFamilia.titulo_familia'
);

$listaSFOrigem = !empty($model->IDEmpregadorTrabalhador) ? EmpregadorTrabalhador::model()->findByPk(
    $model->iDEmpregadorTrabalhador->IDEmpregadorTrabalhador
)->getSetorFuncao() : [];
$listaSFOrigem = Html::listData($listaSFOrigem, 'IDSetorFuncao', 'labelSetorFuncao');

$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'trabalhador-setor-funcao-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
); ?>
<div class="form clearfix positionRelative">
<p class="note note-warning">Campos com * são obrigatórios.</p>
<?php echo $form->errorSummary($model);
$IDEmpregador = Yii::app()->user->getState('IDEmpregador');
?>

<div class="control-group">
    <?php echo $form->labelEx($model, 'IDEmpregadorTrabalhador', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php
        if (!empty($IDEmpregador)) {
            if (!empty($model->IDEmpregadorTrabalhador)) {
                $trabalhador = Trabalhador::model()->findByPk(
                    $model->iDEmpregadorTrabalhador->IDTrabalhador
                )->nome_trabalhador;
            }

            if (!$model->isNewRecord) {
                ?>
                <div class="control-label" style="width:auto; color: #70A2B4; font-weight: bold">
                    <?= $trabalhador ?>
                </div>
            <?php
            } else {
                $this->widget(
                    'application.widgets.WAutoCompleteTrabalhador',
                    array(
                        'model' => $model,
                        'form' => $form,
                        'attributeName' => 'IDEmpregadorTrabalhador',
                        'placeholder' => 'Selecione o Trabalhador',
                        'params' => array(
                            'IDEmpregador' => $IDEmpregador,
                            'attributeID' => 'IDEmpregadorTrabalhador',
                            'statusSituacao' => [0]
                        ),
                        'htmlOptions' => array(
                            'class' => 'span4',
                            'onChange' => "javascript: cleanFields()"
                        ),
                        'selectedText' => isset($trabalhador) ? $trabalhador : ''
                    )
                );
                echo $form->hiddenField($model, 'IDEmpregador', array('value' => $IDEmpregador));
            }
        } else {
            echo $form->dropDownList(
                $model,
                'IDEmpregadorTrabalhador',
                array(),
                array(
                    'class' => 'm-wrap span4',
                    'prompt' => 'Por favor, selecione um empregador!',
                    'disabled' => true
                )
            );
        }
        ?>
    </div>
</div>
<?php if ($model->isNewRecord) { ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDSetorFuncaoOrigem', array('class' => 'control-label')); ?>
        <div class="controls">
            <?=
            $form->dropDownList(
                $model,
                'IDSetorFuncaoOrigem',
                $listaSFOrigem,
                array(
                    'class' => 'span4 select2',
                    'onChange' => "javascript: cleanFields()"
                )
            ) ?>
        </div>
    </div>

<?php } ?>
<div class="control-group">
    <?php echo $form->labelEx($model, 'IDSetor', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php if (!$model->isNewRecord) { ?>
            <div class="control-label" style="width:auto; color: #70A2B4; font-weight: bold">
                <?= $model->iDSetorFuncao->iDSetor ?>
            </div>
        <?php
        } else {
            if (isset($model->IDEmpregador)) {
                $IDEmpregador = $model->IDEmpregador;
            } else {
                $IDEmpregador = Yii::app()->user->getState('IDEmpregador');
            }
            echo $form->dropDownList(
                $model,
                'IDSetor',
                Html::listData(
                    $IDEmpregador ? Setor::model()->getSetoresEmpregador($IDEmpregador) :
                        array(),
                    'IDSetor',
                    'nome_setor'
                ),
                array(
                    'class' => 'm-wrap select2 span4',
                    'prompt' => 'Selecione um Setor',
                    'ajax' => array(
                        'type' => 'POST',
                        //request type
                        'url' => Yii::app()->createURL('SetorFuncao/setorFuncao/ajaxFuncoesBySetor'),
                        //url to call.
                        'update' => 'select#' . Html::activeId($model, 'IDFuncao'),
                        //selector to update
                        'data' => array('IDSetor' => 'js:this.value'),
                        'complete' => "function(){ $('#" . Html::activeId(
                                $model,
                                'IDFuncao'
                            ) . "').trigger('change'); }"
                    )
                )
            );
        } ?>
        <?php echo $form->error($model, 'IDSetor', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'IDFuncao', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php
        if (!$model->isNewRecord) {
            ?>
            <div class="control-label" style="width:auto; color: #70A2B4; font-weight: bold"
                ><?= $model->iDSetorFuncao->iDFuncao ?></div>
        <?php
        } else {
            if (!empty($model->IDSetor)) {
                $funcoes = Html::listData(
                    SetorFuncao::model()->findAllByAttributes(['IDSetor' => $model->IDSetor]),
                    'IDFuncao',
                    'iDFuncao.nome_funcao'
                );
            } else {
                $funcoes = array();
            }
            echo $form->dropDownList(
                $model,
                'IDFuncao',
                $funcoes,
                array(
                    'class' => 'select2',
                    'prompt' => 'Selecione a Funcão',
                    'onChange' => "javascript: cleanFields()"
                )
            ); ?>
            <?php echo $form->error(
                $model,
                'IDFuncao',
                array('class' => 'help-inline')
            );
        }?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'IDOcupacao', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->dropDownList(
            $model,
            'IDOcupacao',
            $listaOcupacao,
            array('class' => 'span6 select2', 'prompt' => 'Escolha uma CBO')
        )?>
        <?php echo $form->error($model, 'IDOcupacao', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'cargo_TrabSetFunc', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $model,
            'cargo_TrabSetFunc',
            array('class' => 'm-wrap span6', 'maxlength' => 30)
        ); ?>
        <?php echo $form->error($model, 'cargo_TrabSetFunc', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'dt_inicioTrabSetFunc', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php
        if ($model->getIsNewRecord() || (!$model->dt_fimTrabSetFunc && $model->getSituacao()->IDTipoSituacao
                != PendenciaExameClinico::ID_SITUACAO_MUDANCA_FUNCAO)
        ) {
            echo $form->textField(
                $model,
                'dt_inicioTrabSetFunc',
                array('class' => 'm-wrap span2 date-picker')
            );
            echo $form->error($model, 'dt_inicioTrabSetFunc', array('class' => 'help-inline'));
        } else {
            ?>
            <div class="control-label" style="width:auto; color: #70A2B4; font-weight: bold">
                <?= $model->dt_inicioTrabSetFunc ?>
            </div>
        <?php
        }
        ?>
    </div>
</div>

<?php if ($model->getIsNewRecord()): ?>
    <div class="control-group">
        <div class="controls">
            <input type="button" class="botao botao-info" id="botao-calcula-pendencias"
                   value="Calcular Pendências de Exame"/>

            <div id="div-ativo-trabalhador" style="display: none; margin-top: 5px">
                <div class="note note-info-fillout">
                    <label>A Função escolhida possui os mesmos riscos da Função atual.
                        Deseja que o trabalhador já entre na nova Função com a Situação "Ativo"?</label>
                    <?php
                    echo $form->checkBox(
                        $model,
                        'ativoTrabalhador'
                    );
                    ?> Sim
                    <label style="margin-top: 20px;">Caso contrário, serão cobrados os exames discriminados
                        abaixo:</label>
                </div>
            </div>

            <table class="table table-striped table-bordered flip-scroll"
                   style="width: 45%; margin-top: 25px; display: none"
                   id="tabela-pendencias-exame">
                <thead>
                <th>Exames Complementares</th>
                <th>Periodicidade</th>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
<?php endif; ?>
</div>


<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Mudar  ' . $model->labelModel() : 'Salvar Alterações',
        array(
            'class' => 'botao',
            'disabled' => $model->isNewRecord ? true : false,
            'id' => 'botao-salvar-mudanca-funcao'
        )
    );
    ?>
</div>
<?php $this->endWidget(); ?>

<script>
    $(document).ready(function () {
        getFlashes();
    });

    $('#<?=Html::activeId($model, 'IDEmpregadorTrabalhador')?>').change(function () {
        $.ajax({
            url: '<?=Yii::app()->createUrl('Trabalhador/trabalhador/ajaxSetorFuncaoByEmpregadorTrabalhador')?>',
            type: 'get',
            dataType: 'JSON',
            data: {IDEmpregadorTrabalhador: $(this).val()},
            success: function (response) {
                if ($.isEmptyObject(response)) {
                    alert('Não existe função associada a esse trabalhador.');
                } else {
                    $.each(response, function (i, value) {
                        $('#<?=  Html::activeId($model,'IDSetorFuncaoOrigem') ?>').append($('<option>').text(value).attr('value', i));
                    });
                }

            },
            beforeSend: function () {
                $('#<?=  Html::activeId($model,'IDSetorFuncaoOrigem') ?>').empty().attr('disabled', true);
            },
            complete: function () {
                $('#<?= Html::activeId($model, 'IDSetorFuncaoOrigem')?>').trigger('change').attr('disabled', false);
            }
        });
    });

    $('#botao-calcula-pendencias').click(function () {
        var IDSetorFuncaoOrigem = $('#<?=Html::activeId($model, 'IDSetorFuncaoOrigem')?>').val();
        var IDSetor = $('#<?=Html::activeId($model, 'IDSetor')?>').val();
        var IDFuncao = $('#<?=Html::activeId($model, 'IDFuncao')?>').val();
        var IDEmpTrab = $('#<?=Html::activeId($model, 'IDEmpregadorTrabalhador')?>').val();
        if (!IDEmpTrab) {
            alert('Favor preencher o campo "Trabalhador"');
        }
        else if (!IDSetor) {
            alert('Favor preencher o campo "Setor"');
        }
        else {
            $.ajax({
                    url: '<?=$this->createUrl('ajaxCalculaPendencias')?>',
                    type: 'post',
                    dataType: 'JSON',
                    data: {
                        IDSetor: IDSetor,
                        IDFuncao: IDFuncao,
                        IDEmpregadorTrabalhador: IDEmpTrab,
                        IDSetorFuncaoOrigem: IDSetorFuncaoOrigem
                    },
                    success: function (response) {
                        $('#botao-salvar-mudanca-funcao').removeAttr('disabled');
                        $tabela = $('#tabela-pendencias-exame');
                        $corpoTabela = $('#tabela-pendencias-exame tbody');
                        $corpoTabela.html('');

                        if (response) {
                            console.log(response);
                            funcao = response.novaFuncaos;
                            if (funcao.mesmoRisco == true) {
                                $('#div-ativo-trabalhador').fadeIn();
                            }

                            if (funcao.pendencias) {
                                $tr = $('<tr>').appendTo($corpoTabela);
                                $('<td>', {
                                    text: 'Cliníco - Mudança de Função'
                                }).appendTo($tr);
                                $('<td>', {
                                    text: '-'
                                }).appendTo($tr);
                                $.each(funcao.pendencias, function (i) {
                                    $tr = $('<tr>').appendTo($corpoTabela);
                                    $('<td>', {
                                        text: funcao.pendencias[i].nome_exameComplementar
                                    }).appendTo($tr);

                                    $('<td>', {
                                        text: funcao.pendencias[i].periodicidade_padrao + ' Meses'
                                    }).appendTo($tr);
                                });
                            }
                            else {
                                $tr = $('<tr>').appendTo($corpoTabela);
                                $('<td>', {
                                    text: 'Cliníco - Mudança de Função'
                                }).appendTo($tr);
                                $('<td>', {
                                    text: '-'
                                }).appendTo($tr);
                            }
                            $tabela.fadeIn();
                        }
                        else {
                            $tr = $('<tr>').appendTo($corpoTabela);
                            $('<td>', {
                                text: 'Cliníco - Mudança de Função'
                            }).appendTo($tr);
                            $('<td>', {
                                text: '-'
                            }).appendTo($tr);
                            $('#div-ativo-trabalhador').fadeIn();
                            $tabela.fadeIn();
                        }
                    }
                }
            )
        }
    })
    ;

    function cleanFields() {
        $('#tabela-pendencias-exame').hide();
        $('#div-ativo-trabalhador').hide();
        $('#botao-salvar-mudanca-funcao').attr('disabled', true);
        $('#<?=Html::activeId($model, 'ativoTrabalhador')?>').parent('span').removeClass('checked');
        $('#<?=Html::activeId($model, 'ativoTrabalhador')?>').removeAttr('checked');
    }

</script>