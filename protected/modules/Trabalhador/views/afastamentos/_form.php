<?php
/* @var $this AcaoPreventivaRecomendadaController */
/* @var $model FRiscosExames */
/* @var $form CActiveForm */
?>


<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'afastamentos-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);

// coloca o formato das datas para o padrão brasileiro
$model->dt_emissao = !empty($model->dt_emissao)
    ? HData::sqlToBr($model->dt_emissao)
    : HData::hoje();

$listaSetor = [];
$listaFuncao = [];

if (!empty($model->IDEmpregador)) {

    $listaSetor = Html::listData(
        SetorFuncao::model()->with('iDSetor')->ativo()->findAll(
            '"iDSetor"."IDEmpregador" = ' . $model->IDEmpregador
        ),
        'IDSetor',
        'iDSetor.nome_setor'
    );
}
if (!empty($model->IDSetor)) {

    $listaFuncao = Html::listData(
        SetorFuncao::model()->with('iDSetor')->ativo()->findAll(
            '"iDSetor"."IDSetor" IN (' . implode(',', $model->IDSetor) . ')'
        ),
        'IDSetorFuncao',
        'labelSetorFuncao'
    );
}


?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDEmpregador',
                Yii::app()->session['empregadores'],
                array(
                    'class' => 'select2 span6',
                    'multiple' => false,
                    'prompt' => 'Escolha o Empregador',
                    'ajax' => array(
                        'type' => 'POST',
                        //request type
                        'url' => CController::createUrl(
                                'afastamentos/retornaSetores',
                                array()
                            ),
                        //url to call.
                        'update' => 'select#' . Html::activeId($model, 'IDSetor'),
                        //selector to update
                        'data' => 'js:{IDEmpregador: $("#' . Html::activeId($model, 'IDEmpregador') . '").val()}',
                    )
                )
            ); ?>
            <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline'));
            echo $form->hiddenField($model, 'download_PDF', array('value' => 0));
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'periodo_inicio', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php $this->widget(
                'application.widgets.WDataRange',
                array(
                    'startAttribute' => 'periodo_inicio',
                    'endAttribute' => 'periodo_fim',
                    'model' => $model,
                    'labelFieldAttribute' => 'periodo'
                )
            )?>
            <?php echo $form->error(
                $model,
                'periodo_inicio',
                array('class' => 'help-inline')
            );
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->label($model, 'IDSetor', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDSetor',
                $listaSetor,
                array(
                    'class' => 'select2 span6',
                    'multiple' => true,
                    'placeholder' => 'Escolha um Setor',
                    'ajax' => array(
                        'type' => 'POST',
                        //request type
                        'url' => CController::createUrl(
                                'afastamentos/retornaFuncoes',
                                array()
                            ),
                        //url to call.
                        'update' => 'select#' . Html::activeId($model, 'IDSetorFuncao'),
                        //selector to update
                        'data' => 'js:{IDSetor: $("#' . Html::activeId($model, 'IDSetor') . '").val()}',
                        'beforeSend' => 'function(){
                        IDSetorFuncao = $("#' . Html::activeId($model, 'IDSetorFuncao') . '").val();
                        $("#' . Html::activeId($model, 'IDSetorFuncao') . '").select2("val", "");
                        $("#' . Html::activeId($model, 'IDSetorFuncao') . '").select2("enable", false);
                    }',
                        'complete' => 'function(){
                        $("#' . Html::activeId($model, 'IDSetorFuncao') . '").select2("val", IDSetorFuncao);
                        $("#' . Html::activeId($model, 'IDSetorFuncao') . '").select2("enable", true);
                    }'
                    )
                )
            ); ?>
            <?php echo $form->error($model, 'IDSetor', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDSetorFuncao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDSetorFuncao',
                $listaFuncao,
                array(
                    'class' => 'select2 span6',
                    'multiple' => true,
                    'placeholder' => 'Escolha uma Função'
                )
            ); ?>
            <?php echo $form->error($model, 'IDSetorFuncao', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDTipoAfastamento', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDTipoAfastamento',
                Html::listData(
                    TipoSituacao::model()->findAllByAttributes(['status_situacao' => 2]),
                    'IDTipoSituacao',
                    'labelTipoSituacao'
                ),
                array(
                    'class' => 'select2 span6',
                    'multiple' => false,
                    'prompt' => 'Escolha o Tipo de Afastamento'
                )
            ); ?>
            <?php echo $form->error($model, 'IDTipoAfastamento', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(
            $model,
            'dt_emissao',
            array('class' => 'control-label')
        ); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'dt_emissao',
                array('class' => 'm-wrap span2 date-picker')
            ); ?>
            <?php echo $form->error(
                $model,
                'dt_emissao',
                array('class' => 'help-inline')
            ); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        'Gerar Afastamentos',
        array('class' => 'botao', 'target' => '_blank')
    ); ?>
</div>
<?php $this->endWidget(); ?>

<script>
    $(document).ready(function () {
        if (isIE) {
            $('#<?=Html::activeId($model, 'download_PDF')?>').val('1');
        }

        <?php if($model->pdf){?>
        abrePDF('<?=$model->pdf?>');
        <?php }?>
    })
</script>