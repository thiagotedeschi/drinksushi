<?php
/* @var $this ObservacaoTrabalhadorController */
/* @var $model ObservacaoTrabalhador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDObservacaoTrabalhador',
            'iDEmpregadorTrabalhador.iDEmpregador:text:Empregador',
            'iDEmpregadorTrabalhador.iDTrabalhador:text:Trabalhador',
            'observacao_trabalhador',
            'nome_responsavelObservacao',
            'email_responsavelObservacao',
            'visualizar_prontuarioObservacao:boolean',
            'listaProfissionais:raw:Profissionais',
            'dt_cadastroObservacao:date',
            'dt_desativacaoObservacao:date',
            'motivo_desativacaoObservacao',
        ),
    )
); ?>
