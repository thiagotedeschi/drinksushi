<?php
/* @var $this ObservacaoTrabalhadorController */
/* @var $model ObservacaoTrabalhador */
/* @var $form CActiveForm */

if (Yii::app()->user->getState('IDEmpregador')) {
    $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'observacao-trabalhador-form',
            'enableAjaxValidation' => true,
            'htmlOptions' => array(
                'class' => 'form-horizontal margin-0',
            )
        )
    );
    $model->dt_cadastroObservacao = HData::sqlToBr($model->dt_cadastroObservacao);

    ?>
    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com * são obrigatórios.</p>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'IDEmpregador',
                    Yii::app()->session['empregadores'],
                    array(
                        'class' => 'm-wrap span6 select2',
                        'prompt' => 'Selecione um Empregador',
                        'options' => array(
                            Yii::app()->user->getState('IDEmpregador') => array('selected' => true)
                        ),
                        'ajax' => array(
                            'type' => 'POST',
                            //request type
                            'url' => Yii::app()->createURL('Trabalhador/trabalhador/ajaxEmpregadorTrabalhador'),
                            //url to call.
                            'update' => 'select#' . Html::activeId($model, 'IDEmpregadorTrabalhador'),
                            //selector to update
                            'data' => array('IDEmpregador' => 'js:this.value'),
                            'complete' => "$('.select2-search-choice').remove()"
                        )
                    )
                ); ?>
                <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDEmpregadorTrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                $IDEmpregador = Yii::app()->user->getState('IDEmpregador');
                if (!empty($model->IDEmpregador)) {
                    $empTrab = EmpregadorTrabalhador::model()->findAllByAttributes(
                        ['IDEmpregador' => $model->IDEmpregador]
                    );
                } elseif ($IDEmpregador) {
                    $empTrab = EmpregadorTrabalhador::model()->findAllByAttributes(
                        ['IDEmpregador' => $IDEmpregador]
                    );
                } else {
                    $empTrab = array();
                }
                echo $form->dropDownList(
                    $model,
                    'IDEmpregadorTrabalhador',
                    Html::listData(
                        $empTrab,
                        'IDEmpregadorTrabalhador',
                        'iDTrabalhador.nome_trabalhador'
                    )
                    ,
                    array('class' => 'm-wrap span4 select2', 'prompt' => 'Selecione um Trabalhador')
                );
                ?>
                <?php echo $form->error($model, 'IDEmpregadorTrabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'observacao_trabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textArea(
                    $model,
                    'observacao_trabalhador',
                    array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
                ); ?>
                <?php echo $form->error($model, 'observacao_trabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'nome_responsavelObservacao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'nome_responsavelObservacao',
                    array(
                        'class' => 'm-wrap span6',
                        'maxlength' => 80,
                        'value' => Usuario::model()->findByPk(
                                Yii::app()->user->IDUsuario
                            )->iDProfissional->nome_profissional
                    )
                ); ?>
                <?php echo $form->error($model, 'nome_responsavelObservacao', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'email_responsavelObservacao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'email_responsavelObservacao',
                    array(
                        'class' => 'm-wrap span6',
                        'maxlength' => 80,
                        'value' => Usuario::model()->findByPk(
                                Yii::app()->user->IDUsuario
                            )->iDProfissional->email_profissional
                    )
                ); ?>
                <?php echo $form->error($model, 'email_responsavelObservacao', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'visualizar_prontuarioObservacao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->checkBox($model, 'visualizar_prontuarioObservacao'); ?>
                <?php echo $form->error($model, 'visualizar_prontuarioObservacao', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'Profissionals', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'Profissionals',
                    Html::listData(
                        Profissional::model()->findAllByEmpregador(
                            Yii::app()->user->getState('IDEmpregador')
                        )->findAll(),
                        'IDProfissional',
                        'nome_profissional'
                    ),
                    array('multiple' => true, 'class' => 'select2')
                ); ?>
                <?php echo $form->error($model, 'Profissionals', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_cadastroObservacao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'dt_cadastroObservacao', array('class' => 'span2 date-picker')); ?>
                <?php echo $form->error($model, 'dt_cadastroObservacao', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php echo Html::submitButton(
            $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
            array('class' => 'botao')
        ); ?>
    </div>
    <?php $this->endWidget();
} else {
    ?>
    <p class="note note-danger">
        Para visualizar essa página é necessário selecionar um <b>Empregador</b>.
    </p>
<?php } ?>
