<?php
/* @var $this ObservacaoTrabalhadorController */
/* @var $data ObservacaoTrabalhador */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDObservacaoTrabalhador')); ?>:</b>
    <?php echo Html::link(
        Html::encode($data->IDObservacaoTrabalhador),
        array('view', 'id' => $data->IDObservacaoTrabalhador)
    ); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDEmpregadorTrabalhador')); ?>:</b>
    <?php echo Html::encode($data->IDEmpregadorTrabalhador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('observacao_trabalhador')); ?>:</b>
    <?php echo Html::encode($data->observacao_trabalhador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_responsavelObservacao')); ?>:</b>
    <?php echo Html::encode($data->nome_responsavelObservacao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('email_responsavelObservacao')); ?>:</b>
    <?php echo Html::encode($data->email_responsavelObservacao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('visualizar_prontuarioObservacao')); ?>:</b>
    <?php echo Html::encode($data->visualizar_prontuarioObservacao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_desativacaoObservacao')); ?>:</b>
    <?php echo Html::encode($data->dt_desativacaoObservacao); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('motivo_desativacaoObservacao')); ?>:</b>
	<?php echo Html::encode($data->motivo_desativacaoObservacao); ?>
	<br />

	*/
    ?>

</div>