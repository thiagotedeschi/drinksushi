<?php
/* @var $this ObservacaoTrabalhadorController */
/* @var $model ObservacaoTrabalhador */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDObservacaoTrabalhador'); ?>
        <?php echo $form->textField($model, 'IDObservacaoTrabalhador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDEmpregadorTrabalhador'); ?>
        <?php echo $form->textField($model, 'IDEmpregadorTrabalhador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'observacao_trabalhador'); ?>
        <?php echo $form->textArea(
            $model,
            'observacao_trabalhador',
            array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_responsavelObservacao'); ?>
        <?php echo $form->textField(
            $model,
            'nome_responsavelObservacao',
            array('class' => 'm-wrap span6', 'maxlength' => 80)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'email_responsavelObservacao'); ?>
        <?php echo $form->textField(
            $model,
            'email_responsavelObservacao',
            array('class' => 'm-wrap span6', 'maxlength' => 80)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'visualizar_prontuarioObservacao'); ?>
        <?php echo $form->checkBox($model, 'visualizar_prontuarioObservacao'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_desativacaoObservacao'); ?>
        <?php echo $form->textField($model, 'dt_desativacaoObservacao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'motivo_desativacaoObservacao'); ?>
        <?php echo $form->textField(
            $model,
            'motivo_desativacaoObservacao',
            array('class' => 'm-wrap span6', 'maxlength' => 255)
        ); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->