<?php
/* @var $this ObservacaoTrabalhadorController */
/* @var $model ObservacaoTrabalhador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

<div id="modal-desativar" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div clas="col-md-12">
                <iframe src="" style="width:100%; overflow: hidden; min-height:400px;"></iframe>
            </div>
        </div>
    </div>
</div>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada
    pesquisa.
</p>

<?php if (!Yii::app()->user->getState('IDEmpregador')) { ?>
    <p class="note note-danger">
        Para visualizar essa página é necessário selecionar um <b>Empregador</b>.
    </p>
<?php } ?>


<?php $this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'observacao-trabalhador-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDObservacaoTrabalhador',
            array(
                'header' => 'Trabalhador',
                'name' => 'nome_trabalhador',
                'value' => '$data->iDEmpregadorTrabalhador->iDTrabalhador'
            ),
            'IDEmpregador' => array(
                'header' => 'Empregador',
                'name' => 'IDEmpregador',
                'value' => '$data->iDEmpregadorTrabalhador->iDEmpregador',
                'filter' => ''
            ),
            array(
                'name' => 'ativo',
                'value' => '($data->ativo ? "Sim" : "Não")',
                'filter' => array('1' => 'Sim', '0' => 'Não'),
            ),
            /*
            /*
            'nome_responsavelObservacao',
            'email_responsavelObservacao',
            'visualizar_prontuarioObservacao',
            'dt_desativacaoObservacao',
            'motivo_desativacaoObservacao',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{desativar}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                        'desativar' => array(
                            'options' => array(
                                'class' => 'botao desativar',
                                'title' => 'Desativar Observação do Trabalhador'
                            ),
                            'imageUrl' => false,
                            'label' => '"<i class=\'icon-power-off icon-large\'></i>"',
                            'url' => 'Yii::app()->createUrl("Trabalhador/observacaoTrabalhador/desativar", array("id"=>$data->primaryKey,"in-modal"=>"in-modal"))',
                            'isModal' => true,
                            'visible' => ($this->verificaAcao(
                                    'disable'
                                ) ? '($data->dt_desativacaoObservacao ? false : true)' : 'false')
                        ),
                    ),
            ),
        ),
    )
);

?>
