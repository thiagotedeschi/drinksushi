<?php
/* @var $this AgenteQuimicoController */
/* @var $model AgenteRisco */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'historico-setor-funcao-disable-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>

    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com * são obrigatórios.</p>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'motivo_desativacaoObservacao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textArea($model, 'motivo_desativacaoObservacao'); ?>
                <?php echo $form->error($model, 'motivo_desativacaoObservacao', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_desativacaoObservacao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'dt_desativacaoObservacao',
                    array('class' => 'date-picker')
                ); ?>
                <?php echo $form->error($model, 'dt_desativacaoObservacao', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="form-actions">
            <?php echo Html::submitButton('Desativar ' . $model->labelModel(), array('class' => 'botao')); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>