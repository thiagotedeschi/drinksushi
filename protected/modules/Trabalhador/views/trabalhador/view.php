
<!-- end PAGE TITLE -->
<div class="portlet-body main-portlet clearfix">
    <div class="span3">
        <?php $this->renderPartial(
            'visualizacao/_dadosPessoais',
            array(
                'model' => $model,
                'trabalhadorCondicao' => $trabalhadorCondicao
            )
        ) ?>
    </div>
    <div class="span9">
        <?php $this->renderPartial(
            'visualizacao/_infoAdicionais',
            array(
                'model' => $model,
                'trabalhadorCondicao' => $trabalhadorCondicao
            )
        ) ?>
    </div>
</div>
<!-- END PAGE CONTENT-->

