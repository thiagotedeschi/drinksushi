<?php

/* @var $this TrabalhadorController */
/* @var $model Trabalhador */
/* @var $empregadorTrabalhador EmpregadorTrabalhador */
/* @var $trabalhadorSetorFuncao TrabalhadorSetorFuncao */
/* @var $cipaTrabalhador CipaTrabalhador */
/* @var $situacaoTrabalhador SituacaoTrabalhador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php $this->renderPartial(
    '_form',
    array(
        'model' => $model,
        'endereco' => $endereco,
        'empregadorTrabalhador' => $empregadorTrabalhador,
        'trabalhadorCondicao' => $trabalhadorCondicao,
        'trabalhadorSetorFuncao' => $trabalhadorSetorFuncao,
        'cipaTrabalhador' => $cipaTrabalhador,
        'situacaoTrabalhador' => $situacaoTrabalhador
    )
); ?>