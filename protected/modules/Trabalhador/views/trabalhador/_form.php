<?php
/* @var $this TrabalhadorController */
/* @var $model Trabalhador */
/* @var $endereço Endereço */
/* @var $empregadorTrabalhador EmpregadorTrabalhador */
/* @var $trabalhadorSetorFuncao TrabalhadorSetorFuncao */
/* @var $cipaTrabalhador CipaTrabalhador */
/* @var $situacaoTrabalhador SituacaoTrabalhador */
$IDEmpregador = Yii::app()->user->getState('IDEmpregador');
?>


<?php

$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'trabalhador-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
            'enctype' => 'multipart/form-data'
        )
    )
);
$model->dt_nascimentoTrabalhador = HData::sqlToBr($model->dt_nascimentoTrabalhador);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php
    if (isset($IDEmpregador)) {
    ?>
    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->errorSummary($empregadorTrabalhador); ?>
    <?php echo $form->errorSummary($trabalhadorSetorFuncao); ?>
    <?php echo $form->errorSummary($situacaoTrabalhador); ?>
    <div class="tabbable tabbable-custom tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_pessoal" data-toggle="tab">Informações Pessoais</a></li>
            <li><a href="#tab_documento" data-toggle="tab">Documentos</a></li>
            <li><a href="#tab_vinculoEmpregaticio" data-toggle="tab">Vínculo Empregatício</a></li>
            <li><a href="#tab_infoAdicionais" data-toggle="tab">Informações Adicionais</a></li>
        </ul>
        <div class="tab-content">
            <?php
            $this->renderPartial('cadastro/_pessoal', array('model' => $model, 'form' => $form));
            $this->renderPartial('cadastro/_documentos', array('model' => $model, 'form' => $form));
            if ($model->isNewRecord) {
                $this->renderPartial(
                    'cadastro/_vinculoEmpregaticio',
                    array(
                        'model' => $model,
                        'form' => $form,
                        'empregadorTrabalhador' => $empregadorTrabalhador,
                        'trabalhadorSetorFuncao' => $trabalhadorSetorFuncao,
                        'cipaTrabalhador' => $cipaTrabalhador,
                        'situacaoTrabalhador' => $situacaoTrabalhador
                    )
                );
            } else {
                $this->renderPartial(
                    'update/_vinculoEmpregaticio',
                    array(
                        'model' => $model,
                        'form' => $form,
                        'empregadorTrabalhadors' => $model->empregadorTrabalhadors,
                        'cipaTrabalhador' => $cipaTrabalhador,
                    )
                );
            }
            $this->renderPartial(
                'cadastro/_infoAdicionais',
                array('model' => $model, 'form' => $form, 'endereco' => $endereco)
            );
            ?>
        </div>
    </div>
</div>
    <div class="form-actions">
        <?php echo Html::submitButton(
            $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
            array('class' => 'botao')
        ); ?>
    </div>
<?php
} else {
    echo '
                <div class="alert alert-error text-center">
                    Por favor, selecione um empregador antes de cadastrar um trabalhador.
                </div>
            ';
}
?>
<?php $this->endWidget(); ?>
