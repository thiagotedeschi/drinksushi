<?php
/* @var $this TrabalhadorController */
/* @var $model Trabalhador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php
$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'trabalhador-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'nome_trabalhador',
            'empregador' => [
                'name' => 'empregadorTrabalhadors.iDEmpregador',
                'value' => '$data->getLabelEmpregadors()',
                'type' => 'raw',
                'header' => 'Empregador',
                'visible' => is_null(Yii::app()->user->getState('IDEmpregador')) ? true : false,
            ],
            /*
            'setorFuncao' => array(
                'header' => $model->attributeLabels()['setorFuncao'],
                'name' => 'setorFuncao',
                'type' => 'raw',
                'value' => 'TrabalhadorSetorFuncao::model()->getUltimoSetorFuncao($data->IDTrabalhador)'
            ),*/
            array(
                'name' => 'cpf_trabalhador',
                'value' => 'HTexto::formataString(HTexto::MASK_CPF, $data->cpf_trabalhador)',
                'header' => 'CPF',
            ),
            array(
                'name' => 'genero_trabalhador',
                'value' => '$data->getGenero($data->genero_trabalhador)',
                'header' => 'Gênero',
                'filter' => array(
                    '0' => 'Feminino',
                    '1' => 'Masculino'
                ),
            ),
            array(
                'header' => 'Data de Nascimento',
                'name' => 'dt_nascimentoTrabalhador',
                'value' => 'HData::sqlToBr($data->dt_nascimentoTrabalhador)',
            ),
            /*

            'genero_trabalhador',
            'cpf_trabalhador',
            'ctps_trabalhador',
            'n_serieCtpsTrabalhador',
            'rg_trabalhador',
            'orgao_expedidorRgTrabalhador',
            'uf_rgTrabalhador',
            'nome_maeTrabalhador',
            'nome_paiTrabalhador',
            'nit_trabalhador',
            'resp_legalTrabalhador',
            'contato_respLegalTrabalhador',
            'telefone_trabalhador',
            'email_trabalhador',
            'IDEndereco',
            'foto_trabalhador',
            */

            array(
                'header' => array('10' => '10', '20' => '20', '50' => '50', '100' => '100', '200' => '200'),
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false'),
                            'url' => 'Yii::app()->createUrl("Trabalhador/trabalhador/view", array("id"=>$data->IDTrabalhador))',
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false'),
                            'url' => 'Yii::app()->createUrl("Trabalhador/trabalhador/update", array("id"=>$data->IDTrabalhador))',
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false'),
                            'url' => 'Yii::app()->createUrl("Trabalhador/trabalhador/delete", array("id"=>$data->IDTrabalhador))',
                        ),
                    ),
            ),
        ),
    )
); ?>
