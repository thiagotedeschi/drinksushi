<?php
/* @var $this TrabalhadorController */
/* @var $data Trabalhador */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDTrabalhador')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDTrabalhador), array('view', 'id' => $data->IDTrabalhador)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_trabalhador')); ?>:</b>
    <?php echo Html::encode($data->nome_trabalhador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_nascimentoTrabalhador')); ?>:</b>
    <?php echo Html::encode($data->dt_nascimentoTrabalhador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('genero_trabalhador')); ?>:</b>
    <?php echo Html::encode($data->genero_trabalhador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('cpf_trabalhador')); ?>:</b>
    <?php echo Html::encode($data->cpf_trabalhador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('ctps_trabalhador')); ?>:</b>
    <?php echo Html::encode($data->ctps_trabalhador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('n_serieCtpsTrabalhador')); ?>:</b>
    <?php echo Html::encode($data->n_serieCtpsTrabalhador); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('rg_trabalhador')); ?>:</b>
	<?php echo Html::encode($data->rg_trabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('orgao_expedidorRgTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->orgao_expedidorRgTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('uf_rgTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->uf_rgTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('nome_maeTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->nome_maeTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('nome_paiTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->nome_paiTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('nit_trabalhador')); ?>:</b>
	<?php echo Html::encode($data->nit_trabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('resp_legalTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->resp_legalTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('contato_respLegalTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->contato_respLegalTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('telefone_trabalhador')); ?>:</b>
	<?php echo Html::encode($data->telefone_trabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('email_trabalhador')); ?>:</b>
	<?php echo Html::encode($data->email_trabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDEndereco')); ?>:</b>
	<?php echo Html::encode($data->IDEndereco); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('foto_trabalhador')); ?>:</b>
	<?php echo Html::encode($data->foto_trabalhador); ?>
	<br />

	*/
    ?>

</div>