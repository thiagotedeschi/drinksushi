<?php
/* @var $this TrabalhadorController */
/* @var $model Trabalhador */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDTrabalhador'); ?>
        <?php echo $form->textField($model, 'IDTrabalhador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_trabalhador'); ?>
        <?php echo $form->textField(
            $model,
            'nome_trabalhador',
            array('class' => 'm-wrap span6', 'maxlength' => 100)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_nascimentoTrabalhador'); ?>
        <?php echo $form->textField($model, 'dt_nascimentoTrabalhador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'genero_trabalhador'); ?>
        <?php echo $form->checkBox($model, 'genero_trabalhador'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'cpf_trabalhador'); ?>
        <?php echo $form->textField($model, 'cpf_trabalhador', array('class' => 'm-wrap span6', 'maxlength' => 11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ctps_trabalhador'); ?>
        <?php echo $form->textField($model, 'ctps_trabalhador', array('class' => 'm-wrap span6', 'maxlength' => 20)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'n_serieCtpsTrabalhador'); ?>
        <?php echo $form->textField(
            $model,
            'n_serieCtpsTrabalhador',
            array('class' => 'm-wrap span6', 'maxlength' => 10)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'rg_trabalhador'); ?>
        <?php echo $form->textField($model, 'rg_trabalhador', array('class' => 'm-wrap span6', 'maxlength' => 12)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'orgao_expedidorRgTrabalhador'); ?>
        <?php echo $form->textField(
            $model,
            'orgao_expedidorRgTrabalhador',
            array('class' => 'm-wrap span6', 'maxlength' => 10)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'uf_rgTrabalhador'); ?>
        <?php echo $form->textField($model, 'uf_rgTrabalhador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_maeTrabalhador'); ?>
        <?php echo $form->textField(
            $model,
            'nome_maeTrabalhador',
            array('class' => 'm-wrap span6', 'maxlength' => 100)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_paiTrabalhador'); ?>
        <?php echo $form->textField(
            $model,
            'nome_paiTrabalhador',
            array('class' => 'm-wrap span6', 'maxlength' => 100)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nit_trabalhador'); ?>
        <?php echo $form->textField($model, 'nit_trabalhador', array('class' => 'm-wrap span6', 'maxlength' => 15)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'resp_legalTrabalhador'); ?>
        <?php echo $form->textField(
            $model,
            'resp_legalTrabalhador',
            array('class' => 'm-wrap span6', 'maxlength' => 100)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'contato_respLegalTrabalhador'); ?>
        <?php echo $form->textField(
            $model,
            'contato_respLegalTrabalhador',
            array('class' => 'm-wrap span6', 'maxlength' => 20)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'telefone_trabalhador'); ?>
        <?php echo $form->textField(
            $model,
            'telefone_trabalhador',
            array('class' => 'm-wrap span6', 'maxlength' => 20)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'email_trabalhador'); ?>
        <?php echo $form->textField(
            $model,
            'email_trabalhador',
            array('class' => 'm-wrap span6', 'maxlength' => 60)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDEndereco'); ?>
        <?php echo $form->textField($model, 'IDEndereco', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'foto_trabalhador'); ?>
        <?php echo $form->textField($model, 'foto_trabalhador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->