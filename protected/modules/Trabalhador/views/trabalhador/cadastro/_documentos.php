<?php


if ($model->isNewRecord) {
    $model->tipoDocObrigatorio = array('rg', 'ctps');
}
?>

<!-- Ínicio Portlet Documentos do trabalhador-->
<div class="tab-pane row-fluid" id="tab_documento">
    <div class="row-fluid">
        <div class="control-group">
            <?php echo $form->labelEx($model, 'cpf_trabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                $this->widget(
                    'CMaskedTextField',
                    array(
                        'model' => $model,
                        'attribute' => 'cpf_trabalhador',
                        'mask' => HTexto::MASK_CPF,
                        'htmlOptions' => array('size' => 6, 'class' => 'span6 m-wrap')
                    )
                );
                ?>
                <?php echo $form->error($model, 'cpf_trabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <fieldset class="span7" style="margin:1px;">
            <?php echo $this->renderPartial(
                'cadastro/_documentosObrigatorios',
                array('form' => $form, 'model' => $model)
            ) ?>

            <div class="control-group ctps_obrigatoria">
                <?php echo $form->labelEx($model, 'ctps_trabalhador', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField(
                        $model,
                        'ctps_trabalhador',
                        array('class' => 'm-wrap span6', 'maxlength' => 20)
                    ); ?>
                    <?php echo $form->error($model, 'ctps_trabalhador', array('class' => 'help-inline')); ?>
                </div>
            </div>

            <div class="control-group ctps_obrigatoria">
                <?php echo $form->labelEx($model, 'n_serieCtpsTrabalhador', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField(
                        $model,
                        'n_serieCtpsTrabalhador',
                        array('class' => 'm-wrap span6', 'maxlength' => 10)
                    ); ?>
                    <?php echo $form->error($model, 'n_serieCtpsTrabalhador', array('class' => 'help-inline')); ?>
                </div>
            </div>

            <div class="control-group rg_obrigatorio">
                <?php echo $form->labelEx($model, 'rg_trabalhador', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField(
                        $model,
                        'rg_trabalhador',
                        array('class' => 'm-wrap span6', 'maxlength' => 12)
                    ); ?>
                    <?php echo $form->error($model, 'rg_trabalhador', array('class' => 'help-inline')); ?>
                </div>
            </div>

            <div class="control-group rg_obrigatorio">
                <?php echo $form->labelEx($model, 'orgao_expedidorRgTrabalhador', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField(
                        $model,
                        'orgao_expedidorRgTrabalhador',
                        array('class' => 'm-wrap span3', 'maxlength' => 10)
                    ); ?>
                    <?php echo $form->error($model, 'orgao_expedidorRgTrabalhador', array('class' => 'help-inline')); ?>
                </div>
            </div>

            <div class="control-group rg_obrigatorio">
                <?php echo $form->labelEx($model, 'uf_rgTrabalhador', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->dropDownList(
                        $model,
                        'uf_rgTrabalhador',
                        Html::listData(Estado::model()->findAll(), 'IDEstado', 'sigla_estado'),
                        array('class' => 'select2 span3', 'prompt' => '')
                    ); ?>
                    <?php echo $form->error($model, 'uf_rgTrabalhador', array('class' => 'help-inline')); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'nit_trabalhador', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField(
                        $model,
                        'nit_trabalhador',
                        array('class' => 'm-wrap span6', 'maxlength' => 15)
                    ); ?>
                    <?php echo $form->error($model, 'nit_trabalhador', array('class' => 'help-inline')); ?>
                </div>
            </div>
        </fieldset>
    </div>
    <!-- Fim Documentos-->
</div>
