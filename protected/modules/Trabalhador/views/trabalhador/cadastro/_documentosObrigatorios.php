
<div class="control-group">
    <?php echo $form->labelEx($model, 'tipoDocObrigatorio', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->checkBoxList(
            $model,
            'tipoDocObrigatorio',
            array(
                'rg' => 'RG',
                'ctps' => 'CTPS'
            ),
            array(
                'class' => 'toggle tipoDocObrigatorio',
                'separator' => '',
                'container' => 'div',
                'template' => '<div class="span3">{label} {input}</div>'
            )
        ); ?>
        <?php echo $form->error($model, 'tipoDocObrigatorio', array('class' => 'help-inline')); ?>
    </div>
</div>
<div class="control-group">
    <div class="controls">
        <div id="erroTipoDoc" class="help-inline"></div>
    </div>
</div>

<script>
    jQuery(function () {
        jQuery(".tipoDocObrigatorio").click(function () {

            if (jQuery(this).val() == "rg") {
                // não deixar desmarcar os dois campos ao mesmo tempo
                if (jQuery(".tipoDocObrigatorio:checked").length > 0) {
                    jQuery(".rg_obrigatorio").toggle(300);
                    $("#erroTipoDoc").slideUp(300);
                    $("#erroTipoDoc").html("");

                } else {
                    jQuery(this).attr("checked", "checked");
                    $("#erroTipoDoc").fadeIn(300).html("Você não pode desmarcar os dois campos ao mesmo tempo.");
                }

            }
            if (jQuery(this).val() == "ctps") {
                // não deixar desmarcar os dois campos ao mesmo tempo
                if (jQuery(".tipoDocObrigatorio:checked").length > 0) {
                    jQuery(".ctps_obrigatoria").toggle(300);
                    $("#erroTipoDoc").slideUp(300);
                    $("#erroTipoDoc").html("");
                } else {
                    jQuery(this).attr("checked", "checked");
                    $("#erroTipoDoc").fadeIn(300).html("Você não pode desmarcar os dois campos ao mesmo tempo.");
                }
            }

        });

        <?php
        if(!$model->isNewRecord){
            if($model->rg_trabalhador){
                echo 'jQuery(".ctps_obrigatoria").toggle(300);';
            }else{
               echo 'jQuery(".rg_obrigatorio").toggle(300)';
            }
        }
        ?>

    });
</script>