
<style>
    .tabs-left.tabbable-custom .nav-tabs li {
        border-left: 3px solid transparent;
    }

    .tabs-left.tabbable-custom .nav-tabs li.active {
        border-left: 3px solid #999;
    }
</style>
<div class="tab-pane" id="tab_vinculoEmpregaticio">
    <div class="tabbable tabbable-custom tabs-left">
        <ul class="nav nav-tabs tabs-left">
            <li class="active">
                <a href="#tab_empregadorTrabalhador" data-toggle="tab">
                    <i class="icon-large icon icon-briefcase"></i>
                    Empregador
                </a>
            </li>
            <li>
                <a href="#tab_trabalhadorSetorFuncao" data-toggle="tab">
                    <i class="icon-large icon icon-compress"></i>
                    Setor/Função
                </a>
            </li>
            <li>
                <a href="#tab_situacaoTrabalhador" data-toggle="tab">
                    <i class="icon-large icon icon-laptop"></i>
                    Situação do Trabalhador
                </a>
            </li>
            <li>
                <a href="#tab_informacoesMedicas" data-toggle="tab">
                    <i class="icon-large icon icon-stethoscope"></i>
                    Informações Médicas
                </a>
            </li>
            <li>
            <a href="#tab_cipaTrabalhador" data-toggle="tab">
                    <i class="icon-large icon icon-shield"></i>
                    CIPA do Trabalhador (Opcional)
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <?php
            $this->renderPartial(
                'cadastro/vinculoEmpregaticio/_empregador',
                array(
                    'form' => $form,
                    'empregadorTrabalhador' => $empregadorTrabalhador,
                    'trabalhadorSetorFuncao' => $trabalhadorSetorFuncao
                )
            );
            $this->renderPartial(
                'cadastro/vinculoEmpregaticio/_setorFuncao',
                array('form' => $form, 'trabalhadorSetorFuncao' => $trabalhadorSetorFuncao, 'model' => $model)
            );
            $this->renderPartial(
                'cadastro/vinculoEmpregaticio/_situacaoTrabalhador',
                array('form' => $form, 'situacaoTrabalhador' => $situacaoTrabalhador)
            );
            $this->renderPartial(
                'cadastro/vinculoEmpregaticio/_informacoesMedicas',
                array(
                    'form' => $form,
                    'empregadorTrabalhador' => $empregadorTrabalhador,
                    'trabalhadorSetorFuncao' => $trabalhadorSetorFuncao
                )
            );
            $this->renderPartial(
                'cadastro/vinculoEmpregaticio/_cipaTrabalhador',
                array('form' => $form, 'cipaTrabalhador' => $cipaTrabalhador)
            );
            ?>
        </div>
    </div>
</div>
<div class="tab-pane" id="tab_vinculoEmpregaticio">
    <div class="tabbable tabbable-custom tabbable-full-width">
    </div>
</div>
