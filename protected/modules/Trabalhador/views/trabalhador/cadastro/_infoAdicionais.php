

<!-- Ínicio Portlet Informações Adicionais-->
<div class="tab-pane row-fluid" id="tab_infoAdicionais">
    <div class="row-fluid">
        <div class="control-group">
            <?php echo $form->labelEx($model, 'telefone_trabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                $this->widget(
                    'application.widgets.WCampoTelefone',
                    array(
                        'model' => $model,
                        'form' => $form,
                        'atributoTel' => 'telefone_trabalhador',
                        'atributoDDD' => 'ddd_telefoneTrabalhador',
                    )
                )
                ?>
                <?php echo $form->error($model, 'telefone_trabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'email_trabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'email_trabalhador',
                    array('class' => 'm-wrap span6', 'maxlength' => 60)
                ); ?>
                <?php echo $form->error($model, 'email_trabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <?php
        $formEnderecoC = $this->widget(
            'application.widgets.WFormEndereco',
            array(
                'endereco' => $endereco,
                'form' => $form,
                'label' => $model->attributeLabels()['IDEndereco'],
            )
        );
        ?>
    </div>
    <!-- Fim Informações Adicionais-->
</div>

