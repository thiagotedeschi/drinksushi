<?php
/**
 * @var $this TrabalhadorController
 * @var $model Trabalhador
 * @var $form CActiveForm
 * @var $situacaoTrabalhador SituacaoTrabalhador
 */
?>
<div class="tab-pane" id="tab_situacaoTrabalhador">
    <div class="tabbable tabbable-custom tabs-left">
        <h2>Situação do Trabalhador</h2></br>
        <?php
        $situacaoTrabalhador->scenario = 'formTrabalhador';
        $this->renderPartial(
            'application.modules.Trabalhador.views.situacaoTrabalhador.cadastro._cadastro',
            array('model' => $situacaoTrabalhador, 'form' => $form, 'formTrabalhador' => true)
        );
        ?>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#tab_situacaoTrabalhador .tabbable .control-group').first().remove();
        <?php
        if(!$situacaoTrabalhador->isNewRecord){ ?>
        $("#tab_situacaoTrabalhador input").prop("disabled", true);
        $("#tab_situacaoTrabalhador select").prop("disabled", true);
        <?php
        }
        ?>
    })
</script>