<?php
/**
 * @var $empregadorTrabalhador EmpregadorTrabalhador
 * @var $trabalhadorSetorFuncao TrabalhadorSetorFuncao
 * @var $form CActiveForm
 */


empty($empregadorTrabalhador->dt_admissaoTrabEmp) ? :
    $empregadorTrabalhador->dt_admissaoTrabEmp = HData::sqlToBr($empregadorTrabalhador->dt_admissaoTrabEmp);

Yii::app()->clientScript->registerScript(
    'search',
    "
       $(document).ready(function(){
       $.ajax({
            url : '" . CController::createUrl('trabalhador/retornaSetores', array()) . "',
            type: 'post',
            success: function(text){
                jQuery('#'+'" . Html::activeId($trabalhadorSetorFuncao, 'IDSetor') . "').html(text);
            },
            data: jQuery(\"option:selected\", \"#" . Html::activeId($empregadorTrabalhador, 'IDEmpregador') . "\"),
            complete: function(){
                $('#" . Html::activeId(
        $trabalhadorSetorFuncao,
        'IDSetor'
    ) . " option[value=" . $trabalhadorSetorFuncao->IDSetor . "]').attr('selected',true);
                jQuery('#'+'" . Html::activeId($trabalhadorSetorFuncao, 'IDSetor') . "').trigger('change');
            },
        })
    });"
);
?>
<div class="tab-pane active" id="tab_empregadorTrabalhador">
    <div class="tabbable tabbable-custom tabs-left">
        <h2>Empregador</h2>
        <br>

        <div class="control-group">
            <?php echo $form->labelEx($empregadorTrabalhador, 'IDEmpregador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $empregadorTrabalhador,
                    'IDEmpregador',
                    Yii::app()->session['empregadores'],
                    array(
                        'class' => 'select2',
                        'empty' => 'Escolha um Empregador',
                        'disabled' => $trabalhadorSetorFuncao->isNewRecord ? null : 'disabled',
                        'options' => array(
                            Yii::app()->user->getState('IDEmpregador') => array('selected' => true)
                        ),
                        'ajax' => array(
                            'type' => 'POST',
                            //request type
                            'url' => CController::createUrl(
                                    'trabalhador/retornaSetores',
                                    array()
                                ),
                            //url to call.
                            'update' => 'select#' . Html::activeId($trabalhadorSetorFuncao, 'IDSetor'),
                            //selector to update
                            'data' => 'js:jQuery(\'option:selected\', this)',
                        )
                    )
                )?>
                <?php echo $form->error($empregadorTrabalhador, 'IDEmpregador', array('class' => 'help-inline')); ?>
            </div>
        </div>


        <div class="control-group">
            <?php echo $form->labelEx(
                $empregadorTrabalhador,
                'regime_revezamentoTrabEmp',
                array('class' => 'control-label')
            );
            ?>
            <div class="controls">
                <?php echo $form->textField(
                    $empregadorTrabalhador,
                    'regime_revezamentoTrabEmp',
                    array('class' => 'm-wrap span6', 'maxlength' => 20)
                );
                ?>
                <?php echo $form->error(
                    $empregadorTrabalhador,
                    'regime_revezamentoTrabEmp',
                    array('class' => 'help-inline')
                );
                ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($empregadorTrabalhador, 'jornada_trabEmp', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $empregadorTrabalhador,
                    'jornada_trabEmp',
                    array('class' => 'm-wrap span6', 'maxlength' => 20)
                );
                ?>
                <?php echo $form->error($empregadorTrabalhador, 'jornada_trabEmp', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($empregadorTrabalhador, 'folga_trabEmp', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->numberField(
                    $empregadorTrabalhador,
                    'folga_trabEmp',
                    array('class' => 'm-wrap span6', 'min' => 0, 'max' => 6)
                );
                ?>
                <small class="muted bold"><br> Dias por Semana</small>
                <?php echo $form->error($empregadorTrabalhador, 'folga_trabEmp', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx(
                $empregadorTrabalhador,
                'matricula_trabEmp',
                array('class' => 'control-label')
            );
            ?>
            <div class="controls">
                <?php echo $form->textField(
                    $empregadorTrabalhador,
                    'matricula_trabEmp',
                    array('class' => 'm-wrap span6', 'maxlength' => 20)
                );
                ?>
                <?php echo $form->error(
                    $empregadorTrabalhador,
                    'matricula_trabEmp',
                    array('class' => 'help-inline')
                );
                ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx(
                $empregadorTrabalhador,
                'dt_admissaoTrabEmp',
                array('class' => 'control-label')
            );
            ?>
            <div class="controls">
                <?php echo $form->textField(
                    $empregadorTrabalhador,
                    'dt_admissaoTrabEmp',
                    array('class' => 'm-wrap span6 date-picker')
                );
                ?>
                <?php echo $form->error(
                    $empregadorTrabalhador,
                    'dt_admissaoTrabEmp',
                    array('class' => 'help-inline')
                );
                ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx(
                $empregadorTrabalhador,
                'matricula_esocialTrabEmp',
                array('class' => 'control-label')
            );
            ?>
            <div class="controls">
                <?php echo $form->textField(
                    $empregadorTrabalhador,
                    'matricula_esocialTrabEmp',
                    array('class' => 'm-wrap span6', 'maxlength' => 35)
                );
                ?>
                <?php echo $form->error(
                    $empregadorTrabalhador,
                    'matricula_esocialTrabEmp',
                    array('class' => 'help-inline')
                );
                ?>
            </div>
        </div>
    </div>
</div>
<script>
    $('#<?=Html::activeId($empregadorTrabalhador, 'dt_admissaoTrabEmp')?>').change(function () {
        $dtInicioTrabSF = $('#<?=Html::activeId($trabalhadorSetorFuncao, 'dt_inicioTrabSetFunc')?>');
        var data1 = $dtInicioTrabSF.val().split('/');
        data1 = new Date(data1[2], data1[1], data1[0]);
        var data2 = $(this).val().split('/');
        data2 = new Date(data2[2], data2[1], data2[0]);
        if ($dtInicioTrabSF.val() == '' || data1.getTime() > data2.getTime()) {
            $dtInicioTrabSF.val($(this).val());
            $dtInicioTrabSF.datepicker('setStartDate', $(this).val());
        }
    });
</script>