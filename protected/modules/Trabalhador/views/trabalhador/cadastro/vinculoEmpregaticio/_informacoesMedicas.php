<div class="tab-pane" id="tab_informacoesMedicas">
    <div class="tabbable tabbable-custom tabs-left">
        <h2>Informações Médicas</h2></br>

        <!-- Ínicio Tab Informações Médicas-->
        <div class="tab-pane row-fluid" id="tab_infoMedicas">
            <div class="row-fluid">
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $trabalhadorSetorFuncao,
                        'dt_ultimoExameClinicoTrabEmp',
                        array('class' => 'control-label')
                    ); ?>
                    <div class="controls">
                        <?php echo $form->textField(
                            $trabalhadorSetorFuncao,
                            'dt_ultimoExameClinicoTrabEmp',
                            array('class' => 'm-wrap span3 date-picker')
                        ); ?>
                        <?php echo $form->error(
                            $trabalhadorSetorFuncao,
                            'dt_ultimoExameClinicoTrabEmp',
                            array('class' => 'help-inline')
                        ); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $empregadorTrabalhador,
                        'exameComplementars',
                        array('class' => 'control-label')
                    ); ?>
                    <div class="controls">
                        <?php
                        $exames = Html::listData(
                            ExameComplementar::model()->findAll(),
                            'IDExameComplementar',
                            'nome_exameComplementar'
                        );
                        echo $form->dropDownList(
                            $empregadorTrabalhador,
                            'exameComplementars',
                            $exames,
                            array('class' => 'm-wrap span6 select2', 'multiple' => true)
                        ); ?>
                        <?php echo $form->error(
                            $empregadorTrabalhador,
                            'exameComplementars',
                            array('class' => 'help-inline')
                        ); ?>
                    </div>
                </div>
                <div class="span8" style="float:none; margin:0 auto">
                    <table id="tabela-exames" class="table table-striped flip-scroll table-bordered">
                        <thead>
                        <th>Exame Complementar</th>
                        <th>Data de realização</th>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($_POST['EmpregadorTrabalhador']['data_realizacaoExame'])) {
                            $datasExame = $_POST['EmpregadorTrabalhador']['data_realizacaoExame'];
                        } elseif (isset($empregadorTrabalhador->IDEmpregadorTrabalhador)) {
                            $datasExame = ResultadoExame::model()->findUltimosByTrabalhador(
                                $empregadorTrabalhador->IDEmpregadorTrabalhador
                            )->findAll();
                            $datasExame = Html::listData($datasExame, 'IDExameComplementar', 'dt_resultadoExame');
                        } else {
                            $datasExame = array();
                        }
                        foreach ($datasExame as $IDExame => $datasExame) {
                            ?>
                            <tr id="tr_<?= $IDExame ?>">
                                <td><?= $exames[$IDExame] ?></td>
                                <td>
                                    <input type="text"
                                           name="EmpregadorTrabalhador[data_realizacaoExame][<?= $IDExame ?>]"
                                           class="m-wrap span6 date-picker dataExame"
                                           value="<?= HData::sqlToBr($datasExame) ?>"/>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2">Selecione um ou mais Exames Complementares</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    $("#<?= Html::activeId($empregadorTrabalhador, 'exameComplementars')?>").on("change", function (e) {
        if (e.removed) {
            removeID = e.removed.id;
            $('#tr_' + removeID).remove();
        }
        if (e.added) {
            addID = e.added.id;
            $row = $('<tr/>', {id: 'tr_' + addID}).appendTo('#tabela-exames tbody');
            $col1 = $('<td/>', {text: e.added.text}).appendTo($row);
            $col2 = $('<td/>').appendTo($row);
            $input = $('<input/>', {
                placeholder: 'Data',
                type: 'text',
                class: 'm-wrap span6 date-picker',
                name: 'EmpregadorTrabalhador[data_realizacaoExame][' + addID + ']'
            }).appendTo($col2);
            $input.datepicker(brDatepickerOptions);
        }
    })
    ;
</script>