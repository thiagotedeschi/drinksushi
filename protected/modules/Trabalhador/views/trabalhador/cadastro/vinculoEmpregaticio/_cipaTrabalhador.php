<?php
/**
 * @var $cipaTrabalhador CipaTrabalhador
 */
?>
<div class="tab-pane" id="tab_cipaTrabalhador">
    <div class="tabbable tabbable-custom tabs-left">
        <h2>CIPA do Trabalhador</h2></br>
        <div class="control-group">
            <?php echo $form->labelEx($cipaTrabalhador, 'cargo_cipaTrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $cipaTrabalhador,
                    'cargo_cipaTrabalhador',
                    array('class' => 'm-wrap span6', 'maxlength' => 20)
                );
                ?>
                <?php echo $form->error($cipaTrabalhador, 'cargo_cipaTrabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx(
                $cipaTrabalhador,
                'membro_eleitoCipaTrabalhador',
                array('class' => 'control-label')
            );
            ?>
            <div class="controls">
                <?php echo $form->checkBox($cipaTrabalhador, 'membro_eleitoCipaTrabalhador'); ?>
                <?php echo $form->error(
                    $cipaTrabalhador,
                    'membro_eleitoCipaTrabalhador',
                    array('class' => 'help-inline')
                );
                ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx(
                $cipaTrabalhador,
                'dt_posseCipaTrabalhador',
                array('class' => 'control-label')
            ); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $cipaTrabalhador,
                    'dt_posseCipaTrabalhador',
                    array('class' => 'm-wrap span6 date-picker')
                );
                ?>
                <?php echo $form->error(
                    $cipaTrabalhador,
                    'dt_posseCipaTrabalhador',
                    array('class' => 'help-inline')
                ); ?>
            </div>
        </div>
    </div>
</div>
