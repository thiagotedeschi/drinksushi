<?php
/**
 * @var $model Trabalhador
 * @var $trabalhadorSetorFuncao TrabalhadorSetorFuncao
 */

$listaOcupacao = Html::listData(
    OcupacaoCBO::model()->with('iDFamilia')->findAll(),
    'IDOcupacao',
    'labelCBO',
    'iDFamilia.titulo_familia'
);

$listaCondicao = Html::listData(
    TipoCondicao::model()->semFatorRisco()->findAll(),
    'IDTipoCondicao',
    'labelTipoCondicao'
);
$trabalhadorCondicao = new TrabalhadorCondicao();

if (!$model->isNewRecord) {
    $dadosCondicao = TrabalhadorCondicao::model()->findCondicaoByTrabalhador($model->IDTrabalhador)->findAll();

    $IDCondicoes = array();

    foreach ($dadosCondicao as $IDTC) {
        $IDCondicoes[] = $IDTC['IDTipoCondicao'];
    }

    $trabalhadorCondicao->IDTipoCondicao = $IDCondicoes;
}

$dt_fimSetFunc = '01/01/1900';
if (isset($trabalhadorSetorFuncao)) {
    $dt_fimSetFunc = HData::sqlToBr($trabalhadorSetorFuncao->dt_fimTrabSetFunc);
}



?>
<div class="tab-pane" id="tab_trabalhadorSetorFuncao">
    <div class="tabbable tabbable-custom tabs-left">
        <h2>Setor/Função</h2></br>
        <div class="control-group">
            <?php echo $form->labelEx($trabalhadorSetorFuncao, 'IDSetor', array('class' => 'control-label')) ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $trabalhadorSetorFuncao,
                    'IDSetor',
                    array(),
                    array(
                        'class' => 'select2',
                        'prompt' => 'Escolha um Setor',
                        'disabled' => $trabalhadorSetorFuncao->isNewRecord ? null : 'disabled',
                        'ajax' => array(
                            'type' => 'POST',
                            //request type
                            'url' => CController::createUrl(
                                    'trabalhador/retornaFuncoes',
                                    array()
                                ),
                            //url to call.
                            'update' => 'select#' . Html::activeId($trabalhadorSetorFuncao, 'IDSetorFuncao'),
                            //selector to update
                            'data' => 'js:jQuery(\'option:selected\', this)',
                            //Clear options selected
                            'complete' => "function(){
                                $('#" . Html::activeId(
                                    $trabalhadorSetorFuncao,
                                    'IDSetorFuncao'
                                ) . " option[value=" . $trabalhadorSetorFuncao->IDSetorFuncao . "]').attr('selected',true);
                                $('#" . Html::activeId($trabalhadorSetorFuncao, 'IDSetorFuncao') . "').trigger('change');
                            }",
                        )
                    )
                ) ?>
                <?php echo $form->error($trabalhadorSetorFuncao, 'IDSetor', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($trabalhadorSetorFuncao, 'IDFuncao', array('class' => 'control-label')) ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $trabalhadorSetorFuncao,
                    'IDSetorFuncao',
                    array(),
                    array(
                        'class' => 'select2',
                        'prompt' => 'Escolha uma Função',
                        'disabled' => $trabalhadorSetorFuncao->isNewRecord ? null : 'disabled'
                    )
                ) ?>
                <?php echo $form->error($trabalhadorSetorFuncao, 'IDSetorFuncao', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx(
                $trabalhadorSetorFuncao,
                'dt_inicioTrabSetFunc',
                array('class' => 'control-label')
            );
            ?>

            <div class="controls">
                <?php echo $form->textField(
                    $trabalhadorSetorFuncao,
                    'dt_inicioTrabSetFunc',
                    array('class' => 'm-wrap span6 date-picker')
                );
                ?>
                <?php echo $form->error(
                    $trabalhadorSetorFuncao,
                    'dt_inicioTrabSetFunc',
                    array('class' => 'help-inline')
                );
                ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($trabalhadorSetorFuncao, 'IDOcupacao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $trabalhadorSetorFuncao,
                    'IDOcupacao',
                    $listaOcupacao,
                    array('class' => 'span6 select2', 'prompt' => 'Escolha uma CBO')
                )?>
                <?php echo $form->error($trabalhadorSetorFuncao, 'IDOcupacao', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx(
                $trabalhadorSetorFuncao,
                'cargo_TrabSetFunc',
                array('class' => 'control-label')
            ); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $trabalhadorSetorFuncao,
                    'cargo_TrabSetFunc',
                    array('class' => 'm-wrap span6', 'maxlength' => 30)
                );
                ?>
                <?php echo $form->error(
                    $trabalhadorSetorFuncao,
                    'cargo_TrabSetFunc',
                    array('class' => 'help-inline')
                ); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($trabalhadorCondicao, 'IDTipoCondicao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $trabalhadorCondicao,
                    'IDTipoCondicao',
                    $listaCondicao,
                    array(
                        'class' => 'm-wrap span6 select2',
                        'multiple' => true,
                        'prompt' => 'Escolha uma Condição'

                    )
                )?>
                <?php echo $form->error($trabalhadorCondicao, 'IDTipoCondicao', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="span10" style="float:none; margin:0 auto">
            <table id="tabela-condicoes" class="table table-striped flip-scroll table-bordered">
                <thead>
                <th>Condição do trabalhador</th>
                <th>Data de início</th>
                </thead>
                <tbody>
                <?php

                if (isset($_POST['trabalhadorCondicao']['dt_inicioCondicao'])) {
                    $datasCondicao = $_POST['TrabalhadorCondicao']['dt_inicioCondicao'];
                } elseif (isset($model->IDTrabalhador)) {
                    $datasCondicao = TrabalhadorCondicao::model()->findCondicaoByTrabalhador(
                        $model->IDTrabalhador
                    )->findAll();
                    $datasCondicao = Html::listData($datasCondicao, 'IDTipoCondicao', 'dt_inicioCondicao');
                } else {
                    $datasCondicao = array();
                }

                foreach ($datasCondicao as $IDCondicao => $datasCondicao) {

                    ?>
                    <tr id="tr_<?= $IDCondicao ?>">
                        <td><?= $listaCondicao[$IDCondicao] ?></td>
                        <td>
                            <input type="text" name="TrabalhadorCondicao[dt_inicioCondicao][<?= $IDCondicao ?>]"
                                   class="m-wrap span6 date-picker dataExame"
                                   value="<?= HData::sqlToBr($datasCondicao) ?>"/>
                        </td>
                    </tr>
                <?php
                }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="2">Selecione uma ou mais Condições</td>
                </tr>
                </tfoot>
            </table>
        </div>

    </div>
</div>

<script>
    $("#<?= Html::activeId($trabalhadorCondicao, 'IDTipoCondicao')?>").on("change", function (e) {
        if (e.removed) {
            removeID = e.removed.id;
            $('#tr_' + removeID).remove();
        }
        if (e.added) {
            addID = e.added.id;
            $row = $('<tr/>', {id: 'tr_' + addID}).appendTo('#tabela-condicoes tbody');
            $col1 = $('<td/>', {text: e.added.text}).appendTo($row);
            $col2 = $('<td/>').appendTo($row);
            $input = $('<input/>', {
                placeholder: 'Data',
                type: 'text',
                class: 'm-wrap span6 date-picker',
                name: 'TrabalhadorCondicao[dt_inicioCondicao][' + addID + ']'
            }).appendTo($col2);
            $input.datepicker(brDatepickerOptions);
        }
    })
    ;
</script>
