<?php


$ehMaiorDeIdade = HData::ehMaiorDeIdade($model->dt_nascimentoTrabalhador, HData::BR_DATE_FORMAT);
//var_dump($model->resp_legalTrabalhador);
//die;
if (!($model->resp_legalTrabalhador == null) || !$ehMaiorDeIdade) {
    $hide = '';
} else {
    $hide = 'hide';
}
?>
<div class="tab-pane row-fluid active" id="tab_pessoal">
    <div class="row-fluid">
        <div class="control-group">
            <?php echo $form->labelEx($model, 'nome_trabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'nome_trabalhador',
                    array('class' => 'm-wrap span6', 'maxlength' => 100)
                ); ?>
                <?php echo $form->error($model, 'nome_trabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_nascimentoTrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'dt_nascimentoTrabalhador',
                    array('class' => 'm-wrap span6 date-picker')
                ) ?>
                <?php echo Html::hiddenField('today', '', array('class' => 'date-picker')) ?>
                <?php echo $form->error($model, 'dt_nascimentoTrabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group <?= $hide ?>" id="div<?= Html::activeId($model, 'resp_legalTrabalhador') ?>">
            <?php echo $form->labelEx($model, 'resp_legalTrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'resp_legalTrabalhador', array('class' => 'm-wrap span6')) ?>
                <?php echo $form->error($model, 'resp_legalTrabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group <?= $hide ?>"
             id="div<?= Html::activeId($model, 'contato_respLegalTrabalhador') ?>">
            <?php echo $form->labelEx($model, 'contato_respLegalTrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                $this->widget(
                    'application.widgets.WCampoTelefone',
                    array(
                        'model' => $model,
                        'form' => $form,
                        'atributoTel' => 'contato_respLegalTrabalhador',
                        'atributoDDD' => 'ddd_respLegalTrabalhador',
                    )
                )
                ?>
                <?php echo $form->error($model, 'contato_respLegalTrabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'genero_trabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'genero_trabalhador',
                    array('0' => 'Feminino', '1' => 'Masculino')
                ); ?>
                <?php echo $form->error($model, 'genero_trabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDRacaCor', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'IDRacaCor',
                    Html::listData(RacaCor::model()->findAll(), 'IDRacaCor', 'desc_racaCor'),
                    array('class' => 'm-wrap span2 select2')
                ); ?>
                <?php echo $form->error($model, 'IDRacaCor', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'nome_maeTrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'nome_maeTrabalhador',
                    array('class' => 'm-wrap span6', 'maxlength' => 100)
                ); ?>
                <?php echo $form->error($model, 'nome_maeTrabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'nome_paiTrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'nome_paiTrabalhador',
                    array('class' => 'm-wrap span6', 'maxlength' => 100)
                ); ?>
                <?php echo $form->error($model, 'nome_paiTrabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'eh_PNETrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->checkBox(
                    $model,
                    'eh_PNETrabalhador',
                    array('class' => 'm-wrap span6', 'maxlength' => 100)
                ); ?>
                <?php echo $form->error($model, 'eh_PNETrabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <?php
        if (!$model->isNewRecord) {
            ?>
            <div class="control-group">
                <div class="controls">
                    <img src="<?= $this->createUrl('fotoTrabalhador', array('id' => $model->IDTrabalhador)) ?>"/>
                </div>
            </div>
        <?php
        }
        ?>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'foto_trabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                $this->widget(
                    'application.widgets.WFileField',
                    array(
                        'form' => $form,
                        'atributo' => 'foto_trabalhador',
                        'model' => $model,
                        'texto_botao' => 'Selecionar Imagem',
                        'html_options' => array('maxlength' => 100),
                        'icone' => 'icon-picture'
                    )
                );
                ?>
                (somente foto com extensão png)
                <?php echo $form->error($model, 'foto_trabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </div>
</div>

<script>
    $("#<?= Html::activeId($model, 'dt_nascimentoTrabalhador')?>").change(function () {
        var start = $(this).datepicker('getDate');
        var end = $('#today').datepicker('getDate');
        var years = (end - start) / 1000 / 60 / 60 / 24 / 365;
        if (years >= 18) {
            $("#div<?= Html::activeId($model, 'resp_legalTrabalhador')?>").addClass('hide');
            $("#<?= Html::activeId($model, 'resp_legalTrabalhador')?>").attr('disabled', true);
            $("#div<?= Html::activeId($model, 'contato_respLegalTrabalhador')?>").addClass('hide');
            $("#<?= Html::activeId($model, 'contato_respLegalTrabalhador')?>").attr('disabled', true);
            $("#<?= Html::activeId($model, 'resp_legalTrabalhador')?>").focus;
        } else {
            $("#div<?= Html::activeId($model, 'resp_legalTrabalhador')?>").removeClass('hide');
            $("#<?= Html::activeId($model, 'resp_legalTrabalhador')?>").attr('disabled', false);
            $("#div<?= Html::activeId($model, 'contato_respLegalTrabalhador')?>").removeClass('hide');
            $("#<?= Html::activeId($model, 'contato_respLegalTrabalhador')?>").attr('disabled', false);
        }
    })
</script>