<?php
/* @var $model Trabalhador */

?>
<style>
    .sale-summary li .sale-num {
        text-align: right;
    }

    .sale-summary li {
        border-top-color: #b1b1b1;
    }
</style>
<div class="portlet row-fluid box grey">
    <div class="portlet-title">
        <div class="caption"><?= $model->nome_trabalhador ?></div>
    </div>
    <div class="portlet-body">
        <ul class="unstyled sale-summary row-fluid">
            <li>
                <center><img src="<?= $this->createUrl('fotoTrabalhador', array('id' => $model->IDTrabalhador)) ?>"/>
                </center>
            </li>
            <li>
                <p><span class="sale-info">Data de Nascimento</span></p>

                <p><span
                        class="sale-num"><?=
                        HData::formataData(
                            $model->dt_nascimentoTrabalhador,
                            HData::BR_DATE_FORMAT,
                            HData::SQL_DATE_FORMAT
                        ) ?></span>
                </p>
            </li>
            <li>
                <p><span class="sale-info">Gênero</span></p>

                <p><span class="sale-num"><?= $model->genero ?></span></p>
            </li>
            <?php if ($model->iDRacaCor): ?>
                <li>
                    <p><span class="sale-info">Raça/Cor</span></p>

                    <p><span class="sale-num"><?= $model->iDRacaCor ?></span></p>
                </li>
            <?php endif; ?>
            <li>
                <p><span class="sale-info">Trabalhador PNE? <i class="icon icon-wheelchair"></i></span></p>

                <p><span class="sale-num"><?= $model->eh_PNETrabalhador ? 'Sim' : 'Não' ?></span></p>
            </li>
            <?php
            if ($model->nome_maeTrabalhador): ?>
                <li>
                    <p><span class="sale-info">Nome da Mãe</span></p>

                    <p><span class="sale-num"><?= $model->nome_maeTrabalhador ?></span></p>
                </li>
            <?php endif;
            if ($model->nome_paiTrabalhador):
                ?>
                <li>
                    <p><span class="sale-info">Nome do Pai</span></p>

                    <p><span class="sale-num"><?= $model->nome_paiTrabalhador ?></span></p>
                </li>
            <?php endif;
            if ($model->cpf_trabalhador):
                ?>
                <li>
                    <p><span class="sale-info">CPF</span></p>

                    <p><span class="sale-num"><?= $model->cpf_trabalhador ?></span></p>
                </li>
            <?php
            endif;
            if ($model->rg_trabalhador):
                ?>
                <li>
                    <p><span class="sale-info">RG</span></p>

                    <p><span class="sale-num"><?= $model->rg_trabalhador ?></span></p>
                </li>
                <li>
                    <p><span class="sale-info">Orgão Expedidor</span></p>

                    <p><span class="sale-num"><?= $model->orgao_expedidorRgTrabalhador ?></span></p>
                </li>
                <li>
                    <p><span class="sale-info">UF</span></p>

                    <p><span class="sale-num"><?= $model->ufRgTrabalhador->sigla_estado ?></span></p>
                </li>
            <?php
            endif;
            if ($model->ctps_trabalhador):
                ?>
                <li>
                    <p><span class="sale-info">CTPS/Nº de Série</span></p>

                    <p><span class="sale-num"><?= $model->ctps_trabalhador ?>
                            /<?= $model->n_serieCtpsTrabalhador ?></span></p>
                </li>
            <?php endif;
            if ($model->nit_trabalhador):
                ?>
                <li>
                    <p><span class="sale-info">NIT</span></p>

                    <p><span class="sale-num"><?= $model->nit_trabalhador ?></span></p>
                </li>
            <?php
            endif;
            ?>
        </ul>
    </div>
</div>