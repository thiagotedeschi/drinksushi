<?php
/* @var $this TrabalhadorController */
/* @var $model Trabalhador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

    <h3><strong>Setor/Função do Trabalhador:</strong> <?php echo $model->iDEmpregadorTrabalhador->iDTrabalhador; ?></h3>
<?php
$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'iDSetorFuncao.iDSetor:text:Setor',
            'iDSetorFuncao.iDFuncao:text:Função',
            'iDOcupacao.labelCBO:text:Ocupação CBO',
            'cargo_TrabSetFunc',
            'dt_inicioTrabSetFunc',
            'dt_fimTrabSetFunc'
        ),
    )
);
?>

    <h3>Dados da Situação deste Trabalhador</h3>

<?php
$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model->getSituacao(),
        'attributes' => array(
            'iDTipoSituacao.nome_situacao:text:Situação do Trabalhador',
            'dt_situacaoTrabalhador'
        ),
    )
);

?>