<?php
/* @var $model Trabalhador */
?>
<!--inicio portlet Informações Complementares-->
<div class="portlet clearfix">
    <div class="portlet-title">
        <div class="caption">Informações Complementares</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="profile-classic row-fluid">
            <ul class="unstyled row-fluid">
                <?php
                $infAdicional = false;
                if ($model->resp_legalTrabalhador):
                    $infAdicional = true;
                    ?>
                    <li>
                        <span class="muted bold"><?=
                            $model->attributeLabels(
                            )['resp_legalTrabalhador'] ?></span><?= $model->resp_legalTrabalhador ?>
                    </li>
                    <li>
                        <span class="muted bold"><?=
                            $model->attributeLabels(
                            )['contato_respLegalTrabalhador'] ?></span><?= $model->contato_respLegalTrabalhador ?>
                    </li>
                <?php endif;
                if ($model->telefone_trabalhador):
                    $infAdicional = true;
                    ?>
                    <li>
                        <span class="muted bold"><?=
                            $model->attributeLabels(
                            )['telefone_trabalhador'] ?></span> <?= $model->telefone_trabalhador ?>
                    </li>
                <?php endif;
                if ($model->email_trabalhador):
                    $infAdicional = true;
                    ?>
                    <li>
                        <span class="muted bold"><?=
                            $model->attributeLabels()['email_trabalhador'] ?></span> <?= $model->email_trabalhador ?>
                    </li>
                <?php endif;
                if ($model->iDEndereco):
                    $infAdicional = true;
                    ?>
                    <li>
                        <span class="muted bold"><?= $model->attributeLabels()['IDEndereco'] ?></span>

                        <p><?= $model->iDEndereco ?></p>
                    </li>
                <?php endif;
                if (!$infAdicional):
                    ?>
                    <li>
                        <span class="muted bold">Não há informações complementares para esse trabalhador</span>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
<!--fim portlet Informações Complementares-->

<!--inicio portlet Empregador-->
<div class="portlet clearfix">

    <?php
    $empregadoresTrabalhador = $model->empregadorTrabalhadors;

    if ($empregadoresTrabalhador):

        foreach ($empregadoresTrabalhador as $empregador) {

            $this->renderPartial(
                'visualizacao/_viewEmpregadorTrabalhador',
                array(
                    'model' => $model,
                    'empregadorTrabalhador' => $empregador
                )
            );
        }
    endif;

    ?>

</div>
<!--fim portlet Empregador-->

<!--inicion portlet Condiçao Trabalhador-->

<div class="portlet clearfix">
    <div class="portlet-title">
        <div class="caption">Condição do trabalhador</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <ul class="unstyled row-fluid">
            <?php
            $condicaos = TrabalhadorCondicao::model()->findAllByAttributes(
                array('IDTrabalhador' => $model->IDTrabalhador)
            );
            $infAdicional = false;
            if ($condicaos):
                $infAdicional = true;
                ?>
                <table class="table table-striped flip-scroll table-bordered">
                    <thead>
                    <th>Condição</th>
                    <th>Data de início</th>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($condicaos as $condicaoTrabalhador) {
                        ?>
                        <tr>
                            <td><?= $condicaoTrabalhador->iDTipoCondicao; ?></td>
                            <td><?= HData::sqlToBr($condicaoTrabalhador->dt_inicioCondicao); ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
            <?php
            endif;
            if (!$infAdicional):
                ?>
                <li>
                    <span class="muted bold">Não há informações de condição deste trabalhador</span>
                </li>
            <?php endif;
            ?>
        </ul>
        </ul>
    </div>
</div>

<!--fim portlet Condição trabalhador-->