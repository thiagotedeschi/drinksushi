<?php
if ($empregadorTrabalhador) {
    ?>
    <div class="portlet-title">
        <div class="caption"><span class="muted bold"><?= $empregadorTrabalhador->iDEmpregador->labelModel() ?>
                :</span> <?= $empregadorTrabalhador->iDEmpregador->labelEmpregador ?></div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <ul class="unstyled row-fluid">
            <?php
            if ($empregadorTrabalhador->dt_admissaoTrabEmp):
                ?>
                <li>
                    <span class="muted bold"><?=
                        $empregadorTrabalhador->attributeLabels()['dt_admissaoTrabEmp'] ?></span>

                    <p><?= HData::sqlToBr($empregadorTrabalhador->dt_admissaoTrabEmp) ?></p>
                </li>
            <?php
            endif;
            if ($empregadorTrabalhador->matricula_trabEmp):
                ?>
                <li>
                    <span class="muted bold"><?=
                        $empregadorTrabalhador->attributeLabels()['matricula_trabEmp'] ?></span>

                    <p><?= $empregadorTrabalhador->matricula_trabEmp ?></p>
                </li>
            <?php
            endif;
            if ($empregadorTrabalhador->matricula_esocialTrabEmp):
                ?>
                <li>
                    <span class="muted bold"><?=
                        $empregadorTrabalhador->attributeLabels()['matricula_esocialTrabEmp'] ?></span>

                    <p><?= $empregadorTrabalhador->matricula_esocialTrabEmp ?></p>
                </li>
            <?php
            endif;
            if ($empregadorTrabalhador->jornada_trabEmp):
                ?>
                <li>
                    <span class="muted bold"><?= $empregadorTrabalhador->attributeLabels()['jornada_trabEmp'] ?></span>

                    <p><?= empty($empregadorTrabalhador->jornada_trabEmp) ? '---' : $empregadorTrabalhador->jornada_trabEmp ?></p>
                </li>
            <?php
            endif;
            if ($empregadorTrabalhador->regime_revezamentoTrabEmp):
                ?>
                <li>
                    <span class="muted bold"><?=
                        $empregadorTrabalhador->attributeLabels()['regime_revezamentoTrabEmp'] ?></span>

                    <p><?= $empregadorTrabalhador->regime_revezamentoTrabEmp ?></p>
                </li>
            <?php
            endif;
            if ($empregadorTrabalhador->folga_trabEmp):
                ?>
                <li>
                    <span class="muted bold"><?= $empregadorTrabalhador->attributeLabels()['folga_trabEmp'] ?></span>

                    <p><?= $empregadorTrabalhador->folga_trabEmp ?></p>
                </li>
            <?php
            endif;
            ?>
        </ul>

        <h4>Funções do Trabalhador</h4>

        <table class="table table-striped flip-scroll table-bordered">
            <thead>
            <th>Setor</th>
            <th>Função</th>
            <th>Situação</th>
            <?php if(!isset($_GET['in-modal'])){?><th>Visualizar</th><?php }?>
            </thead>
            <tbody>
            <?php
            $funcoes = $empregadorTrabalhador->trabalhadorSetorFuncaos;

            foreach ($funcoes as $trabSf) {
                $situacao = $trabSf->getSituacao();
                ?>
                <tr>
                    <td>
                        <?= $trabSf->iDSetorFuncao->iDSetor; ?>
                    </td>

                    <td>
                        <?= $trabSf->iDSetorFuncao->iDFuncao; ?>
                    </td>

                    <td>
                        <?Php
                        if (isset($situacao)) {
                            echo $situacao->iDTipoSituacao;
                        } else {
                            echo "(Não existe situação cadastrada)";
                        }
                        ?>
                    </td>
                    <?php if(!isset($_GET['in-modal'])){?>
                    <td>
                        <i class="icon icon-search" style="cursor: pointer"
                           onclick="abreModalSetorFuncao(<?= $trabSf->IDTrabalhadorSetorFuncao; ?>)"></i>
                    </td>
                    <?php }?>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>

        <h4>CIPA</h4>

        <?php
        $cipaTrabalhador = CipaTrabalhador::model()->findAllByAttributes(
            array('IDEmpregadorTrabalhador' => $empregadorTrabalhador->IDEmpregadorTrabalhador)
        );

        if ($cipaTrabalhador) {
            ?>
            <table class="table table-striped flip-scroll table-bordered">
                <thead>
                <th>Cargo</th>
                <th>Data da Posse</th>
                <th>Membro eleito</th>
                </thead>
                <tbody>
                <?php
                foreach ($cipaTrabalhador as $cipa) {

                    ?>
                    <tr>
                        <td>
                            <p><?= $cipa->cargo_cipaTrabalhador ?>
                        </td>

                        <td>
                            <?= $cipa->dt_posseCipaTrabalhador ?>
                        </td>

                        <td>
                            <?php echo $cipa->membro_eleitoCipaTrabalhador ? 'Sim' : 'Não'; ?>
                        </td>

                    </tr>
                <?php
                }
                ?>
                </tbody>
            </table>
        <?php
        } else {
            echo '<span class="muted bold">Não há informações de CIPA deste trabalhador</span>';
        }
        ?>

    </div>
<?php
}
?>

<div id="modalSetorFuncao" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="col-md-12">
                <iframe id="iframeSetorFuncao" style="width:100%; overflow: hidden; min-height:400px;"></iframe>
            </div>
        </div>
    </div>
</div>

<script>
    function abreModalSetorFuncao(id) {
        $modal = $('#modalSetorFuncao');
        $modal.modal("show");
        $("#iframeSetorFuncao").attr("src", "<?php echo Yii::app()->createUrl('Trabalhador/Trabalhador/trabalhadorSetorFuncao');
         ?>&in-modal=in-modal&id=" + id);
        return false;
    }
</script>
