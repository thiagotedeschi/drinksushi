
<div class="tab-pane" id="tab_vinculoEmpregaticio">
    <?php
    if (count($empregadorTrabalhadors) > 0):
        foreach ($empregadorTrabalhadors as $empregadorTrabalhador) {
            if (isset(Yii::app()->session['empregadores'][$empregadorTrabalhador->IDEmpregador])):
                ?>
                <div class="portlet box empresa">
                    <div class="portlet-title">
                        <div class="caption">
                            <?= $empregadorTrabalhador->iDEmpregador ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body padding-left-20 padding-top-10">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">Data de Admissão: </label>

                                    <div class="controls">
                                <span class="text"><b><?=
                                        HData::sqlToBr(
                                            $empregadorTrabalhador->dt_admissaoTrabEmp
                                        ) ?></b></span>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">Matrícula do Trabalhador: </label>

                                    <div class="controls">
                                        <span
                                            class="text"><b><?= $empregadorTrabalhador->matricula_trabEmp ?></b></span>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">Matrícula do eSocial: </label>

                                    <div class="controls">
                                    <span
                                        class="text"><b><?= $empregadorTrabalhador->matricula_esocialTrabEmp ?></b></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">Jornada do Trabalhador:</label>

                                    <div class="controls">
                                        <span class="text"><b><?= $empregadorTrabalhador->jornada_trabEmp ?></b></span>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">Regime Revezamento:</label>

                                    <div class="controls">
                                <span class="text">
                                    <b><?= $empregadorTrabalhador->regime_revezamentoTrabEmp ?></b>
                                </span>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                <div class="control-group">
                                    <label class="control-label">Folga:</label>

                                    <div class="controls">
                                <span class="text">
                                    <b><?= $empregadorTrabalhador->folga_trabEmp ?></b>
                                </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4>Setor/Função</h4>

                        <div class="row-fluid">
                            <div class="span12">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>
                                            Setor
                                        </th>
                                        <th>
                                            Função
                                        </th>
                                        <th>
                                            Situação
                                        </th>
                                        <th>
                                            Visualizar
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($empregadorTrabalhador->trabalhadorSetorFuncaos as $trabalhadorSetorFuncao) {
                                        $situacaoTrabalhadors = $trabalhadorSetorFuncao->getSituacao(true);
                                        ?>
                                        <tr>
                                            <td>
                                                <?= $trabalhadorSetorFuncao->iDSetorFuncao->iDSetor ?>
                                            </td>
                                            <td>
                                                <?= $trabalhadorSetorFuncao->iDSetorFuncao->iDFuncao ?>
                                            </td>
                                            <td>
                                                <?Php
                                                if (isset($situacaoTrabalhadors)) {
                                                    echo $situacaoTrabalhadors->iDTipoSituacao;
                                                } else {
                                                    echo "(Não existe situação cadastrada)";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <i class="icon icon-search" style="cursor: pointer"
                                                   onclick="abreModalSetorFuncao(<?= $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao; ?>)"></i>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <h4>CIPA</h4>

                        <?php
                        $cipaTrabalhador = CipaTrabalhador::model()->findAllByAttributes(
                            array('IDEmpregadorTrabalhador' => $empregadorTrabalhador->IDEmpregadorTrabalhador)
                        );

                        if ($cipaTrabalhador) {
                            ?>
                            <table class="table table-striped flip-scroll table-bordered">
                                <thead>
                                <th>Cargo</th>
                                <th>Data da Posse</th>
                                <th>Membro eleito</th>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($cipaTrabalhador as $cipa) {

                                    ?>
                                    <tr>
                                        <td>
                                            <p><?= $cipa->cargo_cipaTrabalhador ?>
                                        </td>

                                        <td>
                                            <?= $cipa->dt_posseCipaTrabalhador ?>
                                        </td>

                                        <td>
                                            <?php echo $cipa->membro_eleitoCipaTrabalhador ? 'Sim' : 'Não'; ?>
                                        </td>

                                    </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        <?php
                        } else {
                            echo '<span class="muted bold">Não há informações de CIPA deste trabalhador</span>';
                        }
                        ?>

                    </div>
                </div>
            <?php
            endif;
        }
    endif; ?>
    <a href="<?=
    $this->createUrl(
        'empregadorTrabalhador/search',
        array('EmpregadorTrabalhador[nome_trabalhador]' => $model->nome_trabalhador)
    ) ?>" target="_blank">
        <div class="row-fluid icon-btn span12"
             style="border-style: dashed; border-width: 3px;min-height: 150px;padding-top: 2%;">
            <i class="icon-plus icon-4x"></i>
            <?= Html::tag('h3', array('class' => 'align-center'), 'Criar novo Vínculo Empregatício', true) ?>
        </div>
    </a>
</div>


<div id="modalSetorFuncao" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="col-md-12">
                <iframe id="iframeSetorFuncao" style="width:100%; overflow: hidden; min-height:400px;"></iframe>
            </div>
        </div>
    </div>
</div>

<script>
    function abreModalSetorFuncao(id) {
        $modal = $('#modalSetorFuncao');
        $modal.modal("show");
        $("#iframeSetorFuncao").attr("src", "<?php echo Yii::app()->createUrl('Trabalhador/Trabalhador/trabalhadorSetorFuncao');
         ?>&in-modal=in-modal&id=" + id);
        return false;
    }
</script>
