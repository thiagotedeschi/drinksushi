<?php
/* @var $this EmpregadorTrabalhadorController */
/* @var $model EmpregadorTrabalhador */
/* @var $form CActiveForm */

$model->IDEmpregador = empty($model->IDEmpregador) ? Yii::app()->user->getState('IDEmpregador') : $model->IDEmpregador;
$IDEmpregador = Yii::app()->user->getState('IDEmpregador');
?>


<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'empregador-trabalhador-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="tabbable tabbable-custom tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_empregador" data-toggle="tab">Empregador</a></li>
            <li><a href="#tab_setorFuncao" data-toggle="tab">Setor/Função</a></li>
            <li><a href="#tab_cipaTrabalhador" data-toggle="tab">CIPA do Trabalhador</a></li>
            <li><a href="#tab_exameComplementar" data-toggle="tab">Exames Complementares</a></li>
        </ul>
        <div class="tab-content">
            <?php $this->renderPartial('cadastro/_empregador', array('model' => $model, 'form' => $form)); ?>
            <?php $this->renderPartial('cadastro/_setorFuncao', array('model' => $model, 'form' => $form)); ?>
            <?php $this->renderPartial('cadastro/_cipaTrabalhador', array('model' => $model, 'form' => $form)); ?>
            <?php $this->renderPartial('cadastro/_exameComplementar', array('model' => $model, 'form' => $form)); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>

<?php $this->endWidget(); ?>
