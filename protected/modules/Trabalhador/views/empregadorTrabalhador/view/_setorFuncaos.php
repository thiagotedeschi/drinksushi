<?php
/* @var $this EmpregadorTrabalhadorController */
/* @var $model EmpregadorTrabalhador */
?>
<div class="row-fluid">
    <div class="span12">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>
                    Setor
                </th>
                <th>
                    Função
                </th>
                <th>
                    Situação
                </th>
                <th>
                    Visualizar
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model->trabalhadorSetorFuncaos as $trabalhadorSetorFuncao) {
                $situacaoTrabalhadors = $trabalhadorSetorFuncao->getSituacao();
                ?>
                <tr>
                    <td>
                        <?= $trabalhadorSetorFuncao->iDSetorFuncao->iDSetor ?>
                    </td>
                    <td>
                        <?= $trabalhadorSetorFuncao->iDSetorFuncao->iDFuncao ?>
                    </td>
                    <td>
                        <?Php
                        if (isset($situacaoTrabalhadors)) {
                            echo $situacaoTrabalhadors->iDTipoSituacao;
                        } else {
                            echo "(Não existe situação cadastrada)";
                        }
                        ?>
                    </td>
                    <td style="text-align: center; cursor: pointer;"
                        onclick="abreModalSetorFuncao(<?= $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao; ?>)">
                        <i class="icon icon-search"
                           onclick="abreModalSetorFuncao(<?= $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao; ?>)"></i>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
