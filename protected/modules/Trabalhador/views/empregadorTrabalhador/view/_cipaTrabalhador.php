<?php
/* @var $this EmpregadorTrabalhadorController */
/* @var $model EmpregadorTrabalhador */

$cipaTrabalhadors = CipaTrabalhador::model()->findAllByAttributes(
    array('IDEmpregadorTrabalhador' => $model->IDEmpregadorTrabalhador)
);

if (count($cipaTrabalhadors) > 0) {
    ?>
    <table class="table table-striped flip-scroll table-bordered">
        <thead>
        <th>Cargo</th>
        <th>Data da Posse</th>
        <th>Membro eleito</th>
        </thead>
        <tbody>
        <?php
        foreach ($cipaTrabalhadors as $cipa) {

            ?>
            <tr>
                <td>
                    <p><?= $cipa->cargo_cipaTrabalhador ?>
                </td>

                <td>
                    <?= $cipa->dt_posseCipaTrabalhador ?>
                </td>

                <td>
                    <?php echo $cipa->membro_eleitoCipaTrabalhador ? 'Sim' : 'Não'; ?>
                </td>

            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
<?php
} else {
    echo '<span class="muted bold">Não há informações de CIPA deste trabalhador</span>';
}
?>
