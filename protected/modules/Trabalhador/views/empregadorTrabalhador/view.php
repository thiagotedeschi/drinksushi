<?php
/* @var $this EmpregadorTrabalhadorController */
/* @var $model EmpregadorTrabalhador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDEmpregadorTrabalhador',
            'iDEmpregador:text:Empregador',
            'iDTrabalhador:text:Trabalhador',
            'regime_revezamentoTrabEmp',
            'jornada_trabEmp',
            'folga_trabEmp',
            'matricula_trabEmp',
            'dt_admissaoTrabEmp:date',
            'matricula_esocialTrabEmp',
            'trabalhadorSetorFuncaos' => array(
                'label' => 'Setores/Funções',
                'type' => 'raw',
                'value' => $this->renderPartial('view/_setorFuncaos', array('model' => $model), true, true)
            ),
            'cipaTrabalhadors' => array(
                'label' => 'CIPA',
                'type' => 'raw',
                'value' => $this->renderPartial('view/_cipaTrabalhador', array('model' => $model), true, true)
            )
        ),
    )
); ?>

<div id="modalSetorFuncao" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="col-md-12">
                <iframe id="iframeSetorFuncao" style="width:100%; overflow: hidden; min-height:400px;"></iframe>
            </div>
        </div>
    </div>
</div>

<script>
    function abreModalSetorFuncao(id) {
        $modal = $('#modalSetorFuncao');
        $modal.modal("show");
        $("#iframeSetorFuncao").attr("src", "<?php echo Yii::app()->createUrl('Trabalhador/Trabalhador/trabalhadorSetorFuncao');
         ?>&in-modal=in-modal&id=" + id);
        return false;
    }
</script>
