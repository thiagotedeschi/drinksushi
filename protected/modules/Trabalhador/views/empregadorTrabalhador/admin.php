<?php
/* @var $this EmpregadorTrabalhadorController */
/* @var $model EmpregadorTrabalhador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada
    pesquisa.
</p>


<?php $this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'empregador-trabalhador-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            array(
                'name' => 'nome_empregador',
                'value' => '$data->iDEmpregador',
                'visible' => (Yii::app()->user->getState('IDEmpregador') == null) ? true : false,
            ),
            array(
                'name' => 'nome_trabalhador',
                'value' => '$data->iDTrabalhador',
            ),
            array(
                'header' => 'Setor/Função',
                'name' => 'setorFuncao',
                'type' => 'raw',
                'value' => '$data->getLabelSetorFuncao()'
            ),
            'dt_admissaoTrabEmp:date',
            /*
            'dt_cadastro',
            'regime_revezamentoTrabEmp',
            'jornada_trabEmp',
            'folga_trabEmp',
            'matricula_trabEmp',
            'matricula_esocialTrabEmp',
            'dt_ultimoExameClinicoTrabEmp',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
); ?>
