<?php
/* @var $this EmpregadorTrabalhadorController */
/* @var $model EmpregadorTrabalhador */
/* @var $form CActiveForm */

if (isset($_GET['IDTrabalhador'])) {
    $model->IDTrabalhador = $_GET['IDTrabalhador'];
    $trabalhador = $model->iDTrabalhador->nome_trabalhador;
} else {
    $trabalhador = $model->isNewRecord ? '' : $model->iDTrabalhador->nome_trabalhador;
}
$model->dt_admissaoTrabEmp = HData::sqlToBr($model->dt_admissaoTrabEmp);
?>
<div class="tab-pane row-fluid active" id="tab_empregador">
    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')) ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDEmpregador',
                Yii::app()->session['empregadores'],
                array(
                    'class' => 'select2 span4',
                    'prompt' => 'Escolha um Empregador',
                    'disabled' => $model->isNewRecord ? null : 'disabled',
                    'ajax' => array(
                        'url' => Yii::app()->createUrl('site/mudaEmpregador'),
                        'data' => "js:{IDEmpregador: $(this).val(), url: window.location.href}",
                        'type' => 'POST',
                        'complete' => 'function(){
                            window.location = "";
                        }'
                    )
                )
            ) ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDTrabalhador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php if (Yii::app()->user->getState('IDEmpregador') || !$model->isNewRecord): ?>
                <?php
                $this->widget(
                    'application.widgets.WAutoCompleteTrabalhador',
                    array(
                        'model' => $model,
                        'form' => $form,
                        'attributeName' => 'IDTrabalhador',
                        'placeholder' => 'Selecione o Trabalhador',
                        'params' => array('IDEmpregador' => $model->IDEmpregador),
                        'ajaxUrl' => Yii::app()->createUrl("Trabalhador/trabalhador/ajaxTrabalhadorSemVinculo"),
                        'htmlOptions' => array(
                            'class' => 'span4',
                            'disabled' => !$model->isNewRecord
                        ),
                        'selectedText' => $trabalhador
                    )
                );
                ?>
                <?php echo $form->error($model, 'IDTrabalhador', array('class' => 'help-inline')); ?>
            <?php
            else:
                echo $form->dropDownList(
                    $model,
                    'IDEmpregadorTrabalhador',
                    array(),
                    array(
                        'class' => 'm-wrap span4',
                        'prompt' => 'Por favor, selecione um empregador!',
                        'disabled' => true
                    )
                );
            endif; ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'regime_revezamentoTrabEmp', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'regime_revezamentoTrabEmp',
                array('class' => 'm-wrap span6', 'maxlength' => 20)
            ); ?>
            <?php echo $form->error($model, 'regime_revezamentoTrabEmp', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'jornada_trabEmp', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'jornada_trabEmp',
                array('class' => 'm-wrap span6', 'maxlength' => 20)
            ); ?>
            <?php echo $form->error($model, 'jornada_trabEmp', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'folga_trabEmp', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->numberField($model, 'folga_trabEmp', array('class' => 'm-wrap span6')); ?>
            <?php echo $form->error($model, 'folga_trabEmp', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'matricula_trabEmp', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'matricula_trabEmp',
                array('class' => 'm-wrap span6', 'maxlength' => 20)
            ); ?>
            <?php echo $form->error($model, 'matricula_trabEmp', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_admissaoTrabEmp', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'dt_admissaoTrabEmp',
                array('class' => 'm-wrap span6 date-picker')
            ); ?>
            <?php echo $form->error($model, 'dt_admissaoTrabEmp', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'matricula_esocialTrabEmp', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'matricula_esocialTrabEmp',
                array('class' => 'm-wrap span6', 'maxlength' => 35)
            ); ?>
            <?php echo $form->error($model, 'matricula_esocialTrabEmp', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>
