<?php
$ultimaCipa = CipaTrabalhador::model()->ultimaCipa()->findAllByAttributes(
    array('IDEmpregadorTrabalhador' => $model->IDEmpregadorTrabalhador)
);
$ultimaCipa = array_shift($ultimaCipa);
$cipaTrabalhador = CipaTrabalhador::model()->findAllByAttributes(
    array('IDEmpregadorTrabalhador' => $model->IDEmpregadorTrabalhador)
);
$qtdCipa = count($cipaTrabalhador);
if ($qtdCipa > 1) {
    ?>
    <h4>CIPAs vencidas</h4>

    <table class="table table-striped flip-scroll table-bordered">
        <thead>
        <th>Cargo</th>
        <th>Data da Posse</th>
        <th>Membro eleito</th>
        </thead>
        <tbody>
        <?php
        foreach ($cipaTrabalhador as $cipa) {
            if ($cipa->IDCipaTrabalhador != $ultimaCipa->IDCipaTrabalhador) {
                ?>
                <tr>
                    <td>
                        <p><?= $cipa->cargo_cipaTrabalhador ?>
                    </td>

                    <td>
                        <?= $cipa->dt_posseCipaTrabalhador ?>
                    </td>

                    <td>
                        <?php echo $cipa->membro_eleitoCipaTrabalhador ? 'Sim' : 'Não'; ?>
                    </td>

                </tr>
            <?php
            }
        }
        ?>
        </tbody>
    </table>
<?php
}
?>