<?php
/* @var $this TrabalhadorController */
/* @var $model EmpregadorTrabalhador */
/* @var $form CActiveForm */

$cipaTrabalhador = empty($model->cipaTrabalhadors) ? CipaTrabalhador::model() : array_pop(
    $model->cipaTrabalhadors(
        array('order' => '"dt_posseCipaTrabalhador" DESC', 'limit' => '1')
    )
);

?>

<div class="tab-pane row-fluid" id="tab_cipaTrabalhador">
    <?php
    if (!$model->isNewRecord) {
        ?>
        <div class="control-group" id="btnCipa">
            <?php echo Html::link(
                '<i class="icon-plus"></i> Incluir nova CIPA',
                '#',
                array('class' => 'botao botao-success', 'id' => 'addCipa')
            )
            ?>
        </div>
    <?php
    }
    ?>
    <div id="formCipa">
        <div id="tituloCipa" class="hide"><h4>Nova Cipa</h4></div>
        <?php echo $form->hiddenField(
            $cipaTrabalhador,
            'IDCipaTrabalhador',
            array('class' => 'm-wrap span6')
        );
        ?>

        <div class="control-group">
            <?php echo $form->labelEx($cipaTrabalhador, 'cargo_cipaTrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $cipaTrabalhador,
                    'cargo_cipaTrabalhador',
                    array('class' => 'm-wrap span6', 'maxlength' => 20)
                );
                ?>
                <?php echo $form->error($cipaTrabalhador, 'cargo_cipaTrabalhador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx(
                $cipaTrabalhador,
                'membro_eleitoCipaTrabalhador',
                array('class' => 'control-label')
            );
            ?>
            <div class="controls">
                <?php echo $form->checkBox($cipaTrabalhador, 'membro_eleitoCipaTrabalhador'); ?>
                <?php echo $form->error(
                    $cipaTrabalhador,
                    'membro_eleitoCipaTrabalhador',
                    array('class' => 'help-inline')
                );
                ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx(
                $cipaTrabalhador,
                'dt_posseCipaTrabalhador',
                array('class' => 'control-label')
            ); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $cipaTrabalhador,
                    'dt_posseCipaTrabalhador',
                    array('class' => 'm-wrap span6 date-picker')
                );
                ?>
                <?php echo $form->error(
                    $cipaTrabalhador,
                    'dt_posseCipaTrabalhador',
                    array('class' => 'help-inline')
                ); ?>
            </div>
        </div>
    </div>

    <?Php
    if (!$model->isNewRecord) {
        $this->renderPartial(
            'cadastro/cipaTrabalhador/_tabelaCipa',
            array(
                'model' => $model,
                'form' => $form
            )
        );
    }
    ?>
</div>

<script>
    $(function () {
        $("#addCipa").click(function () {
            $("#btnCipa").hide(300);
            $("#formCipa").hide(300);
            $("#tituloCipa").show(300);
            $("#CipaTrabalhador_IDCipaTrabalhador").val("");
            $("#CipaTrabalhador_cargo_cipaTrabalhador").val("");
            $("#CipaTrabalhador_dt_posseCipaTrabalhador").val("");
            $("#formCipa").show(300);

        });
    });
</script>