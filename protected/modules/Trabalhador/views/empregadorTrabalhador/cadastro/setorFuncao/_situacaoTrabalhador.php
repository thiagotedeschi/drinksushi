<?php
/**
 * @var $this EmpregadorTrabalhadorController
 * @var $trabalhadorSetorFuncao TrabalhadorSetorFuncao
 * @var $i int
 * @var $form CActiveForm
 */
?>

<?php
if (!$trabalhadorSetorFuncao->isNewRecord) {
    echo $form->hiddenField(
        $trabalhadorSetorFuncao,
        "[$i]" . 'situacao' . '[IDSituacaoTrabalhador]',
        array(
            'class' => 'm-wrap span10',
        )
    );
}
?>

<div class="tab-pane row-fluid" id="situacaoTrab<?= $i ?>" style="min-height: 412px;">
    <div class="control-group">
        <?php echo $form->labelEx(
            SituacaoTrabalhador::model(),
            'IDTipoSituacao',
            array('class' => 'control-label')
        );?>
        <div class="controls">
            <?php echo Html::activeDropDownList(
            $trabalhadorSetorFuncao,
                "[$i]" . 'situacao' . '[IDTipoSituacao]',
                Html::listData(TipoSituacao::model()->findAll(), 'IDTipoSituacao', 'nome_situacao'),
                array(
                    'class' => 'span10 select2',
                    'prompt' => 'Escolha uma Situação',
                    'disabled' => $trabalhadorSetorFuncao->getIsNewRecord() ? null : 'disabled'
                )
            );
            ?>
            <?php echo $form->error(
                $trabalhadorSetorFuncao,
                "[$i]" . 'situacao' . '[IDTipoSituacao]',
                array('class' => 'help-inline')
            );
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(
            SituacaoTrabalhador::model(),
            'dt_situacaoTrabalhador',
            array('class' => 'control-label')
        );
        ?>
        <div class="controls">
            <?php echo Html::activeTextField(
            $trabalhadorSetorFuncao,
                "[$i]" . 'situacao' . '[dt_situacaoTrabalhador]',
                array(
                    'class' => 'm-wrap span10 date-picker verificaDtSetorFuncao',
                    'data-role' => $i,
                    'autocomplete' => 'off',
                    'disabled' => $trabalhadorSetorFuncao->isNewRecord ? null : 'disabled'
                )
            );
            ?>
            <?php echo $form->error(
                $trabalhadorSetorFuncao,
                "[$i]" . 'situacao' . '[dt_situacaoTrabalhador]',
                array('class' => 'help-inline')
            );
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(
            SituacaoTrabalhador::model(),
            'motivo_situacaoTrabalhador',
            array('class' => 'control-label')
        );
        ?>
        <div class="controls">
            <?php echo $form->textArea(
                $trabalhadorSetorFuncao,
                "[$i]" . 'situacao' . '[motivo_situacaoTrabalhador]',
                array(
                    'class' => 'm-wrap span10',
                    'maxlength' => 255,
                    'disabled' => !isset($trabalhadorSetorFuncao->dt_fimTrabSetFunc) ? null : 'disabled'
                )
            );
            ?>
            <?php echo $form->error(
                $trabalhadorSetorFuncao,
                "[$i]" . 'situacao' . '[motivo_situacaoTrabalhador]',
                array('class' => 'help-inline')
            );
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(
            SituacaoTrabalhador::model(),
            'visivel_situacaoTrabalhador',
            array('class' => 'control-label')
        );
        ?>
        <div class="controls">
            <?php echo $form->checkBox(
                $trabalhadorSetorFuncao,
                "[$i]" . 'situacao' . '[visivel_situacaoTrabalhador]',
                array(
                    'disabled' => !isset($trabalhadorSetorFuncao->dt_fimTrabSetFunc) ? null : 'disabled'
                )
            );
            ?>
            <?php echo $form->error(
                $trabalhadorSetorFuncao,
                "[$i]" . 'situacao' . '[visivel_situacaoTrabalhador]',
                array('class' => 'help-inline')
            );
            ?>
        </div>
    </div>
</div>
