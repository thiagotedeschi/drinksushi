<?php
/**
 * @var $this EmpregadorTrabalhadorController
 * @var $trabalhadorSetorFuncao TrabalhadorSetorFuncao
 * @var $i int
 * @var $form CActiveForm
 */


$funcaos = array();
$setors = array();

$listaOcupacao = Html::listData(
    OcupacaoCBO::model()->with('iDFamilia')->findAll(),
    'IDOcupacao',
    'labelCBO',
    'iDFamilia.titulo_familia'
);

if (!$trabalhadorSetorFuncao->isNewRecord) {

    $funcaos = Html::listData(
        SetorFuncao::model()->findAllByAttributes(array('IDSetor' => $trabalhadorSetorFuncao->IDSetor)),
        'IDSetorFuncao',
        'iDFuncao'
    );

    $setors = Html::listData(
        Setor::model()->findAllByAttributes(array('IDSetor' => $trabalhadorSetorFuncao->IDSetor)),
        'IDSetor',
        'nome_setor'
    );
    echo $form->hiddenField(
        $trabalhadorSetorFuncao,
        "[$i]" . 'IDTrabalhadorSetorFuncao',
        array(
            'class' => 'm-wrap span10',
        )
    );

    if (isset($trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp)) {
        $trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp = HData::sqlToBr(
            $trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp
        );
    }
} else {

    $funcaos = Html::listData(
        SetorFuncao::model()->findAllByAttributes(array('IDSetor' => $trabalhadorSetorFuncao->IDSetor)),
        'IDSetorFuncao',
        'iDFuncao'
    );

    $setors = Html::listData(
        Setor::model()->findAllByAttributes(array('IDEmpregador' => Yii::app()->user->getState('IDEmpregador'))),
        'IDSetor',
        'nome_setor'
    );
}
?>
<div class="tab-pane row-fluid active" id="infoSetorFuncao<?= $i ?>" style="min-height: 412px;">
    <?php echo $form->errorSummary($trabalhadorSetorFuncao); ?>
    <div class="control-group">
        <?php echo $form->labelEx(
            $trabalhadorSetorFuncao,
            'IDSetor',
            array('class' => 'control-label')
        ) ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $trabalhadorSetorFuncao,
                "[$i]" . 'IDSetor',
                $setors,
                array(
                    'class' => 'span10 select2 buscaFuncao',
                    'prompt' => 'Escolha um Setor',
                    'disabled' => $trabalhadorSetorFuncao->getIsNewRecord() ? null : 'disabled'
                )
            ) ?>
            <?php echo $form->error(
                $trabalhadorSetorFuncao,
                "[$i]" . 'IDSetor',
                array('class' => 'help-inline')
            ); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(
            $trabalhadorSetorFuncao,
            'IDFuncao',
            array('class' => 'control-label')
        ) ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $trabalhadorSetorFuncao,
                "[$i]" . 'IDSetorFuncao',
                $funcaos,
                array(
                    'class' => 'span10 select2',
                    'prompt' => 'Escolha uma Função',
                    'disabled' => $trabalhadorSetorFuncao->getIsNewRecord() ? null : 'disabled',
                )
            ) ?>
            <?php echo $form->error(
                $trabalhadorSetorFuncao,
                "[$i]" . 'IDSetorFuncao',
                array('class' => 'help-inline')
            ); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(
            $trabalhadorSetorFuncao,
            'dt_inicioTrabSetFunc',
            array('class' => 'control-label')
        );
        ?>

        <div class="controls">
            <?php echo $form->textField(
                $trabalhadorSetorFuncao,
                "[$i]" . 'dt_inicioTrabSetFunc',
                array(
                    'class' => 'm-wrap span10 date-picker',
                    'autocomplete' => 'off',
                    'disabled' => $trabalhadorSetorFuncao->getIsNewRecord() ? null : 'disabled'
                )
            );
            ?>
            <?php echo $form->error(
                $trabalhadorSetorFuncao,
                "[$i]" . 'dt_inicioTrabSetFunc',
                array('class' => 'help-inline')
            );
            ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(
            $trabalhadorSetorFuncao,
            'dt_ultimoExameClinicoTrabEmp',
            array('class' => 'control-label')
        ); ?>
        <div class="controls">
            <?php echo $form->textField(
                $trabalhadorSetorFuncao,
                "[$i]" . 'dt_ultimoExameClinicoTrabEmp',
                array(
                    'class' => 'm-wrap span10 date-picker',
                    'autocomplete' => 'off',
                    'maxlength' => 30,
                    'disabled' => !isset($trabalhadorSetorFuncao->dt_fimTrabSetFunc) ? null : 'disabled'
                )
            );
            ?>
            <?php echo $form->error(
                $trabalhadorSetorFuncao,
                "[$i]" . 'dt_ultimoExameClinicoTrabEmp',
                array('class' => 'help-inline')
            ); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(
            $trabalhadorSetorFuncao,
            'cargo_TrabSetFunc',
            array('class' => 'control-label')
        ); ?>
        <div class="controls">
            <?php echo $form->textField(
                $trabalhadorSetorFuncao,
                "[$i]" . 'cargo_TrabSetFunc',
                array(
                    'class' => 'm-wrap span10',
                    'maxlength' => 30,
                    'disabled' => !isset($trabalhadorSetorFuncao->dt_fimTrabSetFunc) ? null : 'disabled'
                )
            );
            ?>
            <?php echo $form->error(
                $trabalhadorSetorFuncao,
                "[$i]" . 'cargo_TrabSetFunc',
                array('class' => 'help-inline')
            ); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(
            $trabalhadorSetorFuncao,
            'IDOcupacao',
            array('class' => 'control-label')
        ); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $trabalhadorSetorFuncao,
                "[$i]" . 'IDOcupacao',
                $listaOcupacao,
                array(
                    'class' => 'span10 select2',
                    'prompt' => 'Escolha uma CBO',
                    'disabled' => !isset($trabalhadorSetorFuncao->dt_fimTrabSetFunc) ? null : 'disabled'
                )
            )?>
            <?php echo $form->error(
                $trabalhadorSetorFuncao,
                "[$i]" . 'IDOcupacao',
                array('class' => 'help-inline')
            ); ?>
        </div>
    </div>
</div>
