<?php
/* @var $this EmpregadorTrabalhadorController */
/* @var $model EmpregadorTrabalhador */
/* @var $form CActiveForm */

$trabalhadorSetorFuncao = new TrabalhadorSetorFuncao;
$situacaoTrabalhador = new SituacaoTrabalhador;
?>
<div class="tab-pane row-fluid" id="tab_setorFuncao">

    <div class="span12" id="div_setorFuncaos" style="margin-left: 0px">

        <div id="SetoresFuncao">

            <?php

            if (isset($model->trabalhadorSetorFuncaos)) {
                foreach ($model->trabalhadorSetorFuncaos as $j => $trabalhadorSetorFuncao) {
                    $this->renderPartial(
                        'cadastro/_portletSetorFuncao',
                        array('trabalhadorSetorFuncao' => $trabalhadorSetorFuncao, 'form' => $form, 'i' => $j)
                    );
                }
            }

            ?>

            <div id="novoSetorFuncao" class="hide"></div>
            <div id="exclusoes"></div>
            <div class="icon-btn span6"
                 style="min-height: 570px; border-style: dashed; margin-left: 1%; border-width: 3px; margin-top: 0;"
                 onclick="addSetorFuncao()">
                <i class="icon-plus icon-4x align-center margin-top-20"
                   title="Adicionar novo Setor/Função"></i>
            </div>

        </div>
    </div>
</div>

<div id="static" class="modal hide fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-body">
        <p>Você deseja realmente excluir este Setor/Função?</p>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn">Cancelar</button>
        <button type="button"
                data-dismiss="modal" id="excluiSetorFuncao" value="" class="btn green">Excluir
        </button>
    </div>
</div>

<script>
    i = $("#SetoresFuncao .portlet.box").length;

    function addSetorFuncao() {
        $span = $('<div class="hide" style="margin-left: 0px;">');

        $.ajax({
            url: "<?php echo $this->createUrl('criaPortletSetorFuncao')?>",
            data: {
                i: i
            },
            success: function (text) {

                $span.append(text);

            },
            complete: function () {
                $("#novoSetorFuncao").append($span, $span.fadeIn(100));
                $("#novoSetorFuncao").show();
                $('#TrabalhadorSetorFuncao_' + i + '_IDSetor').select2();
                $('#TrabalhadorSetorFuncao_' + i + '_dt_inicioTrabSetFunc').datepicker(brDatepickerOptions);
                $('#TrabalhadorSetorFuncao_' + i + '_dt_ultimoExameClinicoTrabEmp').datepicker(brDatepickerOptions);
                $('#TrabalhadorSetorFuncao_' + i + '_IDOcupacao').select2();
                $('#TrabalhadorSetorFuncao_' + i + '_IDSetorFuncao').select2();
                $('#TrabalhadorSetorFuncao_' + i + '_situacao_IDTipoSituacao').select2();
                $('#TrabalhadorSetorFuncao_' + i + '_situacao_dt_situacaoTrabalhador').datepicker(brDatepickerOptions);
                i++;
            }
        });
    }

    function removePortlet(id) {
        if (id !== -1) {
            $('#static').modal({
                show: 'true'
            });
            $("#excluiSetorFuncao").val(id);
        } else {
            var span = "#" + id;
            $(span).parent('div').remove();
        }
        return false;
    }

    $("#excluiSetorFuncao").click(function () {

        var url = "<?php echo CController::createUrl('trabalhador/AjaxExcluiSetorFuncao', array()); ?>";
        var id = $(this).val();
        var dados = "IDSetorFuncao=" + id;
        var span = "#" + id;

        $.ajax({
            url: url,
            dataType: "html",
            type: "POST",
            data: dados,
            success: function (resp) {
                if (resp == "OK") {
                    if (id != undefined) {
                        $("#exclusoes").append('<input type="hidden" name="excluirPonto[]" value="' + id + '">');
                    }
                    //remove visualmente
                    $(span).parent('div').remove();
                } else {
                    alert("Este Setor/Função não pode ser excluído.");
                }
            },
            error: function () {
                alert("Houve um erro, por favor tente novamente.");
            }
        });
    });

    $(function () {
        $(document).on('change', '.buscaFuncao', function () {
            var url = "<?php echo CController::createUrl('trabalhador/RetornaFuncoesByTrabalhador', array()); ?>";
            var idTrabalhador = "<?php echo $model->IDTrabalhador; ?>";
            var dados = "IDSetor=" + $(this).val() + "&IDTrabalhador=" + idTrabalhador;
            var seletor = "#" + $(this).attr("id") + "Funcao";
            $.ajax({
                url: url,
                dataType: "html",
                type: "POST",
                data: dados,
                beforeSend: function () {
                    $(seletor).attr("disabled", true);
                },
                success: function (resp) {
                    $(seletor).html(resp);
                },
                complete: function () {
                    $(seletor).attr("disabled", false);
                }
            });
        });

        $(document).on('change', '.verificaDtSetorFuncao', function () {
            var i = $(this).attr('data-role');

            var seletor = $('#TrabalhadorSetorFuncao_' + i + '_dt_inicioTrabSetFunc');

            var dt_inicioSetFunc = seletor.val();
            var dataInicioSplit = dt_inicioSetFunc.split("/");
            dataInicioSplit = dataInicioSplit[2] + dataInicioSplit[1] + dataInicioSplit[0];

            var dt_Situacao = $(this).val();
            var dataSitSplit = dt_Situacao.split("/");
            dataSitSplit = dataSitSplit[2] + dataSitSplit[1] + dataSitSplit[0];

            if (dataSitSplit < dataInicioSplit) {
                seletor.val(dt_Situacao);
            } else {
                seletor.val(dt_inicioSetFunc);
            }

        });
    });
</script>