<?php
/* @var $this EmpregadorTrabalhadorController */
/* @var $model EmpregadorTrabalhador */
/* @var $form CActiveForm */
?>
<div class="tab-pane row-fluid" id="tab_exameComplementar">
    <div class="control-group">
        <?php echo $form->labelEx(
            $model,
            'exameComplementars',
            array('class' => 'control-label')
        ); ?>
        <div class="controls">
            <?php
            $exames = Html::listData(
                ExameComplementar::model()->findAll(),
                'IDExameComplementar',
                'nome_exameComplementar'
            );
            echo $form->dropDownList(
                $model,
                'exameComplementars',
                $exames,
                array('class' => 'm-wrap span6 select2 exameComplementars', 'multiple' => true)
            ); ?>
            <?php echo $form->error(
                $model,
                'exameComplementars',
                array('class' => 'help-inline')
            ); ?>
        </div>
    </div>
    <div class="span8" style="float:none; margin:0 auto">
        <table id="tabela-exames" class="table table-striped flip-scroll table-bordered">
            <thead>
            <th>Exame Complementar</th>
            <th>Data de realização</th>
            </thead>
            <tbody>
            <?php
            if (isset($_POST['EmpregadorTrabalhador']['data_realizacaoExame'])) {
                $datasExame = $_POST['EmpregadorTrabalhador']['data_realizacaoExame'];
            } elseif (isset($model->IDEmpregadorTrabalhador)) {
                $datasExame = ResultadoExame::model()->findUltimosByTrabalhador(
                    $model->IDEmpregadorTrabalhador
                )->findAll();
                $datasExame = Html::listData(
                    $datasExame,
                    'IDExameComplementar',
                    'dt_resultadoExame'
                );
            } else {
                $datasExame = array();
            }
            foreach ($datasExame as $IDExame => $dt_Exame) {
                ?>
                <tr id="tr_<?= $IDExame ?>">
                    <td><?= $exames[$IDExame] ?></td>
                    <td>
                        <?php
                        echo $form->textField(
                            $model,
                            'data_realizacaoExame' . "[$IDExame]",
                            array(
                                'class' => 'm-wrap span4 date-picker dataExame',
                                'value' => HData::sqlToBr($dt_Exame)
                            )
                        );
                        ?>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2">Selecione um ou mais Exames Complementares</td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
<script>
    $("#<?= Html::activeId($model, 'exameComplementars')?>").on("change", function (e) {
        if (e.removed) {
            removeID = e.removed.id;
            $('#tr_' + removeID).remove();
        }
        if (e.added) {
            addID = e.added.id;
            $row = $('<tr/>', {id: 'tr_' + addID}).appendTo('#tabela-exames tbody');
            $col1 = $('<td/>', {text: e.added.text}).appendTo($row);
            $col2 = $('<td/>').appendTo($row);
            $input = $('<input/>', {
                placeholder: 'Data',
                type: 'text',
                class: 'm-wrap span4 date-picker',
                name: 'EmpregadorTrabalhador[data_realizacaoExame][' + addID + ']'
            }).appendTo($col2);
            $input.datepicker(brDatepickerOptions);
        }
    })
    ;
</script>
