<div class="row-fluid span6" style="margin-left: 1%;">
    <div class="portlet box
                <?= is_null($trabalhadorSetorFuncao->dt_fimTrabSetFunc) ? 'dark-blue' : 'light-grey' ?>"
         id="<?= isset($trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao) ? $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao : -1 ?>">
        <div class="portlet-title">
            <div class="caption">
                #<?= $i + 1 ?>
                - <?= $trabalhadorSetorFuncao->IDSetorFuncao ? $trabalhadorSetorFuncao->iDSetorFuncao : "Novo Setor/Função" ?>
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="javascript:;"
                   onclick="removePortlet(<?php if ($trabalhadorSetorFuncao->isNewRecord) {
                       echo -1;
                   } else {
                       echo $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao;
                   } ?>);"><i
                        class="icon-remove icon-large" style="color: #FFFFFF;"></i></a>
            </div>

        </div>
        <div class="portlet-body">
            <div class="tabbable tabbable-custom tabbable-full-width">
                <ul class="nav nav-tabs">
                    <li class="li_portlet active">
                        <a href="#infoSetorFuncao<?= $i ?>" data-toggle="tab">
                            Informações</a>
                    </li>
                    <li class="li_portlet">
                        <a href="#situacaoTrab<?= $i ?>" data-toggle="tab">
                            Situação do Trabalhador </a>
                    </li>
                </ul>
                <div class="tab-content" style="min-height: 250px">
                    <?php
                    $this->renderPartial(
                        'cadastro/setorFuncao/_infoBasica',
                        array(
                            'trabalhadorSetorFuncao' => $trabalhadorSetorFuncao,
                            'form' => $form,
                            'i' => $i
                        )
                    );
                    $this->renderPartial(
                        'cadastro/setorFuncao/_situacaoTrabalhador',
                        array(
                            'trabalhadorSetorFuncao' => $trabalhadorSetorFuncao,
                            'form' => $form,
                            'i' => $i
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>