<?php
/* @var $this SituacaoTrabalhadorController */
/* @var $model SituacaoTrabalhador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

$url = $this->createUrl('situacaoTrabalhador/viewContent');

$situacoesTrabalhador = $model->findAllByAttributes(
    [
        'IDTrabalhadorSetorFuncao' => $model->IDTrabalhadorSetorFuncao
    ],
    ['order' => '"dt_situacaoTrabalhador" DESC']
);
?>

<style>

    .tabs-left.tabbable-custom .nav-tabs li.active {
        border-left: 3px solid #70A2B4;
    }

    .tabs-left.tabbable-custom .nav-tabs li.main a {
        font-weight: bold;
    }

    .tabs-left.tabbable-custom .nav-tabs li {
        max-width: 150px;
    }

    iframe {
        min-height: 100%;
        min-width: 100%;
    }
</style>


<div class="tabbable tabbable-custom tabs-left">
    <ul class="nav nav-tabs tabs-left">
        <?php
        $i = 0;
        foreach ($situacoesTrabalhador as $situacao) {
            $i++;
            $class = $situacao->IDSituacaoTrabalhador == $model->IDSituacaoTrabalhador ? 'active main' : null;
            echo '<li class="' . $class . '">
            <a href="#tab_situacao_trabalhador_' . $i . ' " data-toggle="tab" data-id="' . $situacao->IDSituacaoTrabalhador . '">
            ' . $situacao . '
            </a></li>';
        }
        ?>
    </ul>
    <div class="tab-content">
        <?php
        $i = 0;
        foreach ($situacoesTrabalhador as $situacao) {
            $i++;
            $class = $situacao->IDSituacaoTrabalhador == $model->IDSituacaoTrabalhador ? 'active' : null;
            $urlActive = $situacao->IDSituacaoTrabalhador == $model->IDSituacaoTrabalhador ? $url . '&id=' . $model->IDSituacaoTrabalhador : '';
            echo '<div class="tab-pane ' . $class . '" id="tab_situacao_trabalhador_' . $i . '">
                  <div class="tabbable tabbable-custom tabs-left">';
            echo '<iframe class="frame-auto-resize" src="' . $urlActive . '"></iframe>';
            echo '</div></div>';
        }
        ?>

    </div>
</div>

<?php if (!isset($_GET['in-modal'])) { ?>
    <a class="m-icon-big-swapleft print-hidden float-left" style="margin: 8px 8px 10px 8px"
       href="<?= $this->createUrl('search') ?>"></a>
<?php } ?>
<script language="javascript" type="text/javascript">
    //Essa função é chamada sempre que se muda de tab
    $('a[data-toggle="tab"]').on('shown', function (e) {
        $target = $(e.target);
        //pega o id tab selecionada
        IDTab = $target.attr('href');
        //pega o iframe
        $iframe = $(IDTab + " > div> iframe");
        //muda a src do iframe
        if ($iframe.attr("src") == '') {
            //pega o ID do model a ser carregado no iframe
            targetID = $(this).data("id");
            $iframe.attr("src", "<?=$url?>&id=" + targetID, function () {
                //redimensiona a tab selecionada
                resizeIframe($(IDTab + " > div> iframe"));
            });
        }
    });
</script>


