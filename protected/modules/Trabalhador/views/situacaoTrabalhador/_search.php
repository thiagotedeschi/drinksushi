<?php
/* @var $this SituacaoTrabalhadorController */
/* @var $model SituacaoTrabalhador */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'IDSituacaoTrabalhador'); ?>
            <?php echo $form->textField($model,'IDSituacaoTrabalhador',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDEmpregadorTrabalhador'); ?>
            <?php echo $form->textField($model,'IDEmpregadorTrabalhador',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDTipoSituacao'); ?>
            <?php echo $form->textField($model,'IDTipoSituacao',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dt_situacaoTrabalhador'); ?>
            <?php echo $form->textField($model,'dt_situacaoTrabalhador',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dt_cadastroSituacaoTrabalhador'); ?>
            <?php echo $form->textField($model,'dt_cadastroSituacaoTrabalhador',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'motivo_situacaoTrabalhador'); ?>
            <?php echo $form->textField($model,'motivo_situacaoTrabalhador',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDBeneficio'); ?>
            <?php echo $form->textField($model,'IDBeneficio',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDExameClinico'); ?>
            <?php echo $form->textField($model,'IDExameClinico',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'visivel_situacaoTrabalhador'); ?>
            <?php echo $form->checkBox($model,'visivel_situacaoTrabalhador'); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dt_fimAfastamento'); ?>
            <?php echo $form->textField($model,'dt_fimAfastamento',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'dt_concessaoBeneficio'); ?>
            <?php echo $form->textField($model,'dt_concessaoBeneficio',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'n_requerimentoBeneficio'); ?>
            <?php echo $form->textField($model,'n_requerimentoBeneficio',array('class'=>'m-wrap span6','maxlength'=>25)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'n_beneficio'); ?>
            <?php echo $form->textField($model,'n_beneficio',array('class'=>'m-wrap span6','maxlength'=>25)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'afastamento_anteriorB91'); ?>
            <?php echo $form->checkBox($model,'afastamento_anteriorB91'); ?>
        </div>

        <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->