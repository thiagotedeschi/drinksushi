<?php
/* @var $this SituacaoTrabalhadorController */
/* @var $data SituacaoTrabalhador */
?>

<div class="view">

    	<b><?php echo Html::encode($data->getAttributeLabel('IDSituacaoTrabalhador')); ?>:</b>
	<?php echo Html::link(Html::encode($data->IDSituacaoTrabalhador), array('view', 'id'=>$data->IDSituacaoTrabalhador)); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDEmpregadorTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->IDEmpregadorTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDTipoSituacao')); ?>:</b>
	<?php echo Html::encode($data->IDTipoSituacao); ?>
	<br />

    <b><?php echo Html::encode($data->getAttributeLabel('IDCAT')); ?>:</b>
    <?php echo Html::encode($data->IDCAT); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_situacaoTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->dt_situacaoTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('dt_cadastroSituacaoTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->dt_cadastroSituacaoTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('motivo_situacaoTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->motivo_situacaoTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDBeneficio')); ?>:</b>
	<?php echo Html::encode($data->IDBeneficio); ?>
	<br />

	<?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('IDExameClinico')); ?>:</b>
	<?php echo Html::encode($data->IDExameClinico); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('visivel_situacaoTrabalhador')); ?>:</b>
	<?php echo Html::encode($data->visivel_situacaoTrabalhador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('dt_fimAfastamento')); ?>:</b>
	<?php echo Html::encode($data->dt_fimAfastamento); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('dt_concessaoBeneficio')); ?>:</b>
	<?php echo Html::encode($data->dt_concessaoBeneficio); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('n_requerimentoBeneficio')); ?>:</b>
	<?php echo Html::encode($data->n_requerimentoBeneficio); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('n_beneficio')); ?>:</b>
	<?php echo Html::encode($data->n_beneficio); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('afastamento_anteriorB91')); ?>:</b>
	<?php echo Html::encode($data->afastamento_anteriorB91); ?>
	<br />

	*/ ?>

</div>