<?php
$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'showBotaoVoltar' => false,
        'attributes' => array(
            'IDSituacaoTrabalhador',
            array(
                'label' => 'Empregador',
                'value' => $model->iDTrabalhadorSetorFuncao->iDEmpregadorTrabalhador->iDEmpregador
            ),
            array(
                'label' => 'Trabalhador',
                'value' => $model->iDTrabalhadorSetorFuncao->iDEmpregadorTrabalhador->iDTrabalhador
            ),
            array(
                'label' => 'Setor/Função',
                'value' => $model->iDTrabalhadorSetorFuncao->iDSetorFuncao
            ),
            array(
                'label' => 'Situação',
                'value' => $model->iDTipoSituacao
            ),
            array(
                'label' => 'IDCAT',
                'value' => $model->iDCAT
            ),
            'dt_situacaoTrabalhador',
            'dt_cadastroSituacaoTrabalhador',
            'motivo_situacaoTrabalhador',
            'IDExameClinico',
            'visivel_situacaoTrabalhador:boolean',
            'IDBeneficio',
            'dt_concessaoBeneficio',
            'n_requerimentoBeneficio',
            'n_beneficio',
            'dt_fimAfastamento',
            'afastamento_anteriorB91:boolean',
        ),
    )
); ?>