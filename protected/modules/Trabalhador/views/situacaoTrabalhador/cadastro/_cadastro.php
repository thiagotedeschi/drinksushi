<?php echo $form->errorSummary($model);
$IDEmpregador = Yii::app()->user->getState('IDEmpregador');
//if (!isset($IDEmpregador) && !$model->isNewRecord) {
//    $IDEmpregador = $model->iDTrabalhadorSetorFuncao->iDEmpregadorTrabalhador->IDEmpregador;
//}
?>

    <div class="control-group">

        <?php
        if (isset($IDEmpregador)) {
            ?>
            <?php echo $form->labelEx($model, 'IDEmpregadorTrabalhador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                if (isset($model->IDEmpregadorTrabalhador)) {
                    $trabalhador = EmpregadorTrabalhador::model()->findByPk(
                        $model->IDEmpregadorTrabalhador
                    )->iDTrabalhador->nome_trabalhador;
                }

                if (!$model->isNewRecord) {
                    ?>
                    <div class="control-label" style="width:auto; color: #70A2B4; font-weight: bold"
                         id="label-situacao-atual"><?= $trabalhador ?>
                    </div>
                <?php
                } else {
                    $this->widget(
                        'application.widgets.WAutoCompleteTrabalhador',
                        array(
                            'model' => $model,
                            'form' => $form,
                            'attributeName' => 'IDEmpregadorTrabalhador',
                            'placeholder' => 'Selecione o Trabalhador',
                            'params' => array(
                                'IDEmpregador' => $IDEmpregador,
                                'attributeID' => 'IDEmpregadorTrabalhador'
                            ),
                            'htmlOptions' => array(
                                'class' => 'span4',
                            ),
                            'linkView' => Yii::app()->createUrl('Trabalhador/trabalhador/viewByEmpregadorTrabalhador'),
                            'selectedText' => isset($trabalhador) ? $trabalhador : ''
                        )
                    );
                }
                ?>
            </div>
        <?php
        } else {
            echo '
                <div class="alert alert-error text-center">
                    Por favor, selecione um empregador antes de criar uma situação do trabalhador.
                </div>
            ';

        }
        ?>

    </div>
<?php
if (isset($IDEmpregador)) {
    if (isset($model->IDEmpregadorTrabalhador)) {
        $listaSF = EmpregadorTrabalhador::model()->findByPk($model->IDEmpregadorTrabalhador)->getTrabalhadorSetorFuncao(
        );
        $listaSF = Html::listData(
            $listaSF,
            'IDTrabalhadorSetorFuncao',
            'iDSetorFuncao.labelSetorFuncao'
        );
    } else {
        $listaSF = array();
    }

    if (!isset($formTrabalhador)) {
        ?>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDTrabalhadorSetorFuncao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'IDTrabalhadorSetorFuncao',
                    $listaSF,
                    array(
                        'class' => 'm-wrap span3 select2',
                        'prompt' => 'Selecione o Setor/Função do Trabalhador',
                        'disabled' => !$model->isNewRecord
                    )
                ); ?>
                <?php echo $form->error(
                    $model,
                    'IDTrabalhadorSetorFuncao',
                    array('class' => 'help-inline')
                ); ?>
            </div>
        </div>
    <?php } ?>
    <div class="control-group" style="display: none">
        <?= Html::label('Situação atual', '', array('class' => 'control-label')) ?>
        <div class="controls">
            <div class="control-label" style="width:auto; color: #70A2B4; font-weight: bold"
                 id="label-situacao-atual"></div>
        </div>
    </div>

    <?php
    if ($model->IDTipoSituacao) {
        $status_situacao = $model->iDTipoSituacao->status_situacao;
        $situacoes = Html::listData(
            TipoSituacao::model()->alfabetica()->findAllByAttributes(['status_situacao' => $status_situacao]),
            'IDTipoSituacao',
            'nome_situacao'
        );
    } elseif (isset($formTrabalhador)) {
        $situacoes = Html::listData(
            TipoSituacao::model()->alfabetica()->findAll(),
            'IDTipoSituacao',
            'nome_situacao'
        );
    } else {
        $situacoes = array();
    }
    ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDTipoSituacao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDTipoSituacao',
                $situacoes,
                array(
                    'class' => 'm-wrap span6 select2',
                    'prompt' => 'Selecione a nova Situação do Trabalhador',
                    'onchange' => isset($formTrabalhador) ? '' : '
                        $.ajax({
                            url: "' . CController::createUrl('AjaxLiberaCat') . '",
                            type: "post",
                            dataType: "JSON",
                            data: {
                                IDTipoSituacao: $("#' . Html::activeId($model, 'IDTipoSituacao') . '").val(),
                                IDTrabalhadorSetorFuncao: $("#' . Html::activeId(
                                $model,
                                'IDTrabalhadorSetorFuncao'
                            ) . '").val()' . '
                            },
                            success: function(response){

                                if(response){

                                    $selectCAT = $("#' . Html::activeId($model, 'IDCAT') . '");
                                    $selectCAT.html("");
                                    $.each(response.data, function (i, text) {
                                    $option = $("<option>", {
                                         value: i,
                                         text: text
                                         }).appendTo($selectCAT);
                                    });

                                    if(response.afastamento){
                                        $(".idcat").slideDown(300);
                                        $("#' . Html::activeId($model, 'IDCAT') . '").select2("enable", true);
                                    }else{
                                        $(".idcat").slideUp(300);
                                        $("#' . Html::activeId($model, 'IDCAT') . '").select2("enable", false);
                                    }
                                }

                            },
                        })'
                )
            ); ?>
            <?php echo $form->error($model, 'IDTipoSituacao', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group idcat">
        <?php echo $form->labelEx($model, 'IDCAT', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            if (!$model->isNewRecord && $status_situacao == 2) {
                $identificaCAT = CAT::model()->findAllByTrabalhadorSetorFuncao(
                    $model->iDTrabalhadorSetorFuncao->IDEmpregadorTrabalhador
                )->findAll();

                $listaCat = Html::listData(
                    $identificaCAT,
                    'IDCAT',
                    'labelCAT'
                );

            } else {
                $listaCat = array();
            }

            echo $form->dropDownList(
                $model,
                'IDCAT',
                $listaCat,
                array(
                    'class' => 'select2 span4',
                    'prompt' => 'Selecione uma CAT',
                    'options' => [$model->IDCAT => ['selected' => true]]
                )
            );

            ?>
            <?php echo $form->error($model, 'IDTrabalhador', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_situacaoTrabalhador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            if ($model->isNewRecord) {
                echo $form->textField(
                    $model,
                    'dt_situacaoTrabalhador',
                    array('class' => 'm-wrap span3 date-picker')
                );
                echo $form->error($model, 'dt_situacaoTrabalhador', array('class' => 'help-inline'));
            } else {
                ?>
                <div class="control-label"
                     style="width:auto; color: #70A2B4; font-weight: bold"><?= $model->dt_situacaoTrabalhador ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'motivo_situacaoTrabalhador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'motivo_situacaoTrabalhador',
                array('class' => 'm-wrap span6', 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'motivo_situacaoTrabalhador', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <?php
    if (isset($status_situacao) && (($status_situacao == 2 && in_array(
                    $model->iDTipoSituacao->IDTipoSituacao,
                    [3, 14, 15]
                )) || $model->IDTipoSituacao == 24 || $model->IDTipoSituacao == 29)
    ) {
        //se for afastado ou aposentado
        $temBeneficio = true;
    } else {
        //se não for aposentado nem afastado
        $temBeneficio = false;
    }
    ?>
    <fieldset id="dados-beneficio" style="<?= $temBeneficio ? '' : 'display: none' ?>">
        <legend>Dados do Benefício</legend>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDBeneficio', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                if ($temBeneficio) {
                    if ($status_situacao == 2 && in_array($model->iDTipoSituacao->IDTipoSituacao, [3, 14, 15])) {
                        $afastado = true;
                        $beneficios = TipoBeneficio::model()->findAllByAttributes(['afastamento_beneficio' => true]);
                    } elseif ($model->IDTipoSituacao == 24 || $model->IDTipoSituacao == 29) {
                        $beneficios = TipoBeneficio::model()->findAllByAttributes(['afastamento_beneficio' => false]);
                    }
                }

                if (!empty($beneficios)) {
                    $beneficios = Html::listData($beneficios, 'IDTipoBeneficio', 'desc_beneficio');
                } else {
                    $beneficios = array();
                }

                echo $form->dropDownList(
                    $model,
                    'IDBeneficio',
                    $beneficios,
                    array('class' => 'm-wrap span6 select2', 'prompt' => 'Selecione o tipo de Benefício')
                ); ?>
                <?php echo $form->error($model, 'IDBeneficio', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_concessaoBeneficio', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'dt_concessaoBeneficio',
                    array('class' => 'm-wrap span2 date-picker')
                ); ?>
                <?php echo $form->error($model, 'dt_concessaoBeneficio', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'n_requerimentoBeneficio', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'n_requerimentoBeneficio',
                    array('class' => 'm-wrap span2', 'maxlength' => 25)
                ); ?>
                <?php echo $form->error($model, 'n_requerimentoBeneficio', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'n_beneficio', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'n_beneficio',
                    array('class' => 'm-wrap span2', 'maxlength' => 25)
                ); ?>
                <?php echo $form->error($model, 'n_beneficio', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </fieldset>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'visivel_situacaoTrabalhador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->checkBox($model, 'visivel_situacaoTrabalhador'); ?>
            <?php echo $form->error($model, 'visivel_situacaoTrabalhador', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div id="dados-afastamento" style="<?= !empty($afastado) ? '' : 'display: none' ?>">
        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_fimAfastamento', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'dt_fimAfastamento',
                    array('class' => 'm-wrap span2 date-picker')
                ); ?>
                <?php echo $form->error($model, 'dt_fimAfastamento', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'afastamento_anteriorB91', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->checkBox($model, 'afastamento_anteriorB91'); ?>
                <?php echo $form->error($model, 'afastamento_anteriorB91', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </div>


    <script>
        $('#<?=Html::activeId($model,'IDEmpregadorTrabalhador')?>').change(function () {
            $.ajax(
                {
                    url: "<?=Yii::app()->createUrl('Trabalhador/trabalhador/ajaxSituacaoTrabalhador')?>",
                    type: 'GET',
                    dataType: 'JSON',
                    data: {IDEmpregadorTrabalhador: $(this).val()},
                    error: function () {
                        alert("Houve um erro ao carregar os dados do trabalhador");
                    },
                    success: function (response) {
                        $.each(response, function (i, value) {
                            $('#<?=  Html::activeId($model,'IDTrabalhadorSetorFuncao') ?>').append($('<option>').text(value).attr('value', i));
                        });
                    },
                    beforeSend: function () {
                        $('#<?=  Html::activeId($model,'IDTrabalhadorSetorFuncao') ?>').empty().attr('disabled', true);
                    },
                    complete: function () {
                        $('#<?= Html::activeId($model, 'IDTrabalhadorSetorFuncao')?>').trigger('change').attr('disabled', false);
                    }
                }
            );
        });

        $('#<?=Html::activeId($model,'IDTrabalhadorSetorFuncao')?>').change(function () {
            if ($(this).val()) {
                $.ajax(
                    {
                        url: "<?=Yii::app()->createUrl('Trabalhador/trabalhador/ajaxSituacoesDisponiveis')?>",
                        type: 'GET',
                        dataType: 'JSON',
                        data: {IDTrabalhadorSetorFuncao: $(this).val()},
                        error: function () {
                            alert("Houve um erro ao carregar os dados de situação do trabalhador");
                        },
                        success: function (response) {
                            $.each(response, function (i, value) {
                                $('#<?=  Html::activeId($model,'IDTipoSituacao') ?>').append($('<option>').text(value).attr('value', i));
                            });
                        },
                        beforeSend: function () {
                            $('#<?=  Html::activeId($model,'IDTipoSituacao') ?>').empty().attr('disabled', true);
                        },
                        complete: function () {
                            $('#<?= Html::activeId($model, 'IDTipoSituacao')?>').trigger('change').attr('disabled', false);
                        }
                    }
                );
            }
        });


        $('#<?=Html::activeId($model,'IDTipoSituacao')?>').change(function () {
            $.ajax({
                url: "<?=Yii::app()->createUrl('Trabalhador/situacaoTrabalhador/ajaxBeneficios')?>",
                type: 'POST',
                dataType: 'JSON',
                data: {"IDTipoSituacao": $(this).val()},
                error: function () {
                    alert("Houve um erro ao carregar as opções de benefício");
                },
                success: function (response) {
                    if (response) {
                        $selectBeneficio = $('#<?=Html::activeId($model,'IDBeneficio')?>');
                        //Limpa os campos anteriores
                        $selectBeneficio.html('');
                        //Insere as opções
                        $.each(response.data, function (i, text) {
                            $option = $('<option>', {
                                value: i,
                                text: text
                            }).appendTo($selectBeneficio);
                        });
                        $('#dados-beneficio').fadeIn();
                        //Verificar se é afastamento
                        if (response.afastamento) {
                            $('#dados-afastamento').fadeIn();
                        }
                        else {
                            $('#dados-afastamento').fadeOut();
                        }
                    }
                    else {
                        $('#dados-beneficio').fadeOut();
                        $('#dados-afastamento').fadeOut();
                    }
                }
            });
        });
    </script>

    <?php
    if ($model->isNewRecord || (isset($status_situacao) && $status_situacao != 2)) {
        echo '<script>$(".idcat").addClass("hide");</script>';
    }
}
?>