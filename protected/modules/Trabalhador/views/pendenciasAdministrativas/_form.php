<?php
/* @var $this AcaoPreventivaRecomendadaController */
/* @var $model FRiscosExames */
/* @var $form CActiveForm */
?>


<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'pendencias-administrativas-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);

// coloca o formato das datas para o padrão brasileiro
$model->dt_emissao = !empty($model->dt_emissao)
    ? HData::sqlToBr($model->dt_emissao)
    : HData::hoje();

?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDEmpregador',
                Yii::app()->session['empregadores'],
                array(
                    'class' => 'select2 span6',
                    'multiple' => false,
                    'prompt' => 'Escolha o Empregador',
                )
            ); ?>
            <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(
            $model,
            'dt_emissao',
            array('class' => 'control-label')
        ); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'dt_emissao',
                array('class' => 'm-wrap span2 date-picker')
            ); ?>
            <?php echo $form->error(
                $model,
                'dt_emissao',
                array('class' => 'help-inline')
            ); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        'Gerar Pendências',
        array('class' => 'botao', 'target' => '_blank')
    ); ?>
</div>
<?php $this->endWidget(); ?>
