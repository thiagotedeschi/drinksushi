<?php
/**
 * Controller do CRUD TrabalhadorSetorFuncao
 *
 * @package base.Trabalhador
 */

class MudancaFuncaoController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '690',
        'search' => '689',
        'view' => '692',
        'update' => '691',
        'delete' => '693',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Mudar ' . TrabalhadorSetorFuncao::model()->labelModel();
        $model = new TrabalhadorSetorFuncao;
        $model->scenario = 'mudarFuncao';

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['TrabalhadorSetorFuncao'])) {
            $model->attributes = $_POST['TrabalhadorSetorFuncao'];
            if ($model->IDSetor && $model->IDFuncao) {
                $model->IDSetorFuncao = SetorFuncao::model()->findByAttributes(
                    array(
                        'IDSetor' => $model->IDSetor,
                        'IDFuncao' => $model->IDFuncao
                    )
                )->IDSetorFuncao;
            }

            //Se o formulário for validado
            if ($model->validate()) {
                //Salva os dados
                if ($model->save()) {
                    //Insere situacao do Trabalhador nas funções antigas
                    $SituacaoTrabalhador = new SituacaoTrabalhador();
                    $SituacaoTrabalhador->IDTipoSituacao = PendenciaExameComplementar::ID_SITUACAO_MUDANCA_FUNCAO;
                    $trabalhadorSetorFuncao =
                        TrabalhadorSetorFuncao::model()->findByAttributes(
                            [
                                'IDSetorFuncao' => $model->IDSetorFuncaoOrigem,
                                'IDEmpregadorTrabalhador' => $model->IDEmpregadorTrabalhador
                            ]
                        );
                    $SituacaoTrabalhador->IDTrabalhadorSetorFuncao = $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao;
                    $SituacaoTrabalhador->dt_situacaoTrabalhador = $model->dt_inicioTrabSetFunc;
                    $SituacaoTrabalhador->dt_cadastroSituacaoTrabalhador = $model->dt_cadastro;

                    if ($SituacaoTrabalhador->save()) {
                        if ($model->ativoTrabalhador) {
                            $trabalhadorSetorFuncao->dt_fimTrabSetFunc = $model->dt_inicioTrabSetFunc;
                            $trabalhadorSetorFuncao->save();
                        } else {
                            //Salva a pendencia de saida de função
                            PendenciaExameComplementar::criaPendencias(
                                $SituacaoTrabalhador->IDTrabalhadorSetorFuncao,
                                $SituacaoTrabalhador->IDSituacaoTrabalhador,
                                null,
                                true
                            );
                        }

                        //Insere situação do trabalhador na nova função
                        $novaSituacao = new SituacaoTrabalhador();
                        if ($model->ativoTrabalhador) {
                            $novaSituacao->IDTipoSituacao = PendenciaExameComplementar::ID_SITUACAO_ATIVO;
                        } else {
                            $novaSituacao->IDTipoSituacao = PendenciaExameComplementar::ID_SITUACAO_MUDANCA_FUNCAO;
                        }
                        $novaSituacao->IDTrabalhadorSetorFuncao = $model->IDTrabalhadorSetorFuncao;
                        $novaSituacao->dt_situacaoTrabalhador = $model->dt_inicioTrabSetFunc;
                        $novaSituacao->dt_cadastroSituacaoTrabalhador = $model->dt_cadastro;
                        if ($novaSituacao->save()) {
                            PendenciaExameComplementar::criaPendencias(
                                $model->IDTrabalhadorSetorFuncao,
                                $novaSituacao->IDSituacaoTrabalhador,
                                null,
                                false
                            );
                            PendenciaExameClinico::criaPendencia($novaSituacao, $model->dt_inicioTrabSetFunc);

                            $this->redirect(array('search', 'id' => $model->IDTrabalhadorSetorFuncao));
                        }
                    }

                }
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar Mudança de ' . TrabalhadorSetorFuncao::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['TrabalhadorSetorFuncao'])) {
            $model->attributes = $_POST['TrabalhadorSetorFuncao'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->IDTrabalhadorSetorFuncao));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $model = $this->loadModel($id);
                $delete = true;
                if ($model->dt_fimTrabSetFunc == null) {
                    //Salva a pendencia de saída da nova função
                    $novoTrabalhadorSF = TrabalhadorSetorFuncao::model()->ultimoInativo()->findByAttributes(
                        ['IDEmpregadorTrabalhador' => $model->IDEmpregadorTrabalhador]
                    );
                    if ($novoTrabalhadorSF) {
                        $novoTrabalhadorSF->dt_fimTrabSetFunc = null;
                        $novoTrabalhadorSF->save();
//                        PendenciaExameComplementar::criaPendencias(
//                            $novoTrabalhadorSF->IDTrabalhadorSetorFuncao,
//                            $novoTrabalhadorSF->getSituacao()->IDSituacaoTrabalhador,
//                            $novoTrabalhadorSF->IDSetorFuncao
//                        );
//                        PendenciaExameClinico::criaPendencia(
//                            $empregadorTrabalhador->getSituacaoTrabalhador()->IDSituacaoTrabalhador
//                        );
                    } else {
                        $delete = false;
                        Yii::app()->user->setFlash(
                            'error',
                            array('Não foi possível excluir essa mudança de Função, verifique se o Trabalhador possui funções anteriores ')
                        );
                    }
                }
                if ($delete) {
                    $model->delete();
                }
            }
        } else {
            $model = $this->loadModel($id);
            $delete = true;
            if ($model->dt_fimTrabSetFunc == null) {
                //Salva a pendencia de saída da nova função
                $novoTrabalhadorSF = TrabalhadorSetorFuncao::model()->ultimoInativo()->findByAttributes(
                    ['IDEmpregadorTrabalhador' => $model->IDEmpregadorTrabalhador]
                );
                if ($novoTrabalhadorSF) {
                    $novoTrabalhadorSF->dt_fimTrabSetFunc = null;
                    $novoTrabalhadorSF->save();
//                    $empregadorTrabalhador = $model->iDEmpregadorTrabalhador;
//                    PendenciaExameComplementar::criaPendencias(
//                        $empregadorTrabalhador->IDEmpregadorTrabalhador,
//                        $empregadorTrabalhador->getSituacaoTrabalhador()->IDSituacaoTrabalhador,
//                        $novoTrabalhadorSF->IDSetorFuncao
//                    );
//                    PendenciaExameClinico::criaPendencia(
//                        $empregadorTrabalhador->getSituacaoTrabalhador()->IDSituacaoTrabalhador
//                    );
                } else {
                    $delete = false;
                    Yii::app()->user->setFlash(
                        'error',
                        array('Não foi possível excluir essa mudança de Função, verifique se o Trabalhador possui funções anteriores ')
                    );
                }
            }
            if ($delete) {
                $model->delete();
            }
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . TrabalhadorSetorFuncao::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new TrabalhadorSetorFuncao('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['TrabalhadorSetorFuncao'])) {
            $model->attributes = $_GET['TrabalhadorSetorFuncao'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return TrabalhadorSetorFuncao the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = TrabalhadorSetorFuncao::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param TrabalhadorSetorFuncao $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'trabalhador-setor-funcao-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Calcula as pendências dos Exames que deverão ser feitos, decorrentes da mudança de função
     * E retorna em um JSON as pendencias e um valor booleano mesmoRisco, que indica se a nova função tem ou não
     * os mesmos riscos da função atual.
     * Recebe via POST IDSetor, IDFuncao, e IDEmpregadorTrabalhador
     *
     */
    public function actionAjaxCalculaPendencias()
    {
        if (isset($_POST['IDSetor']) && isset($_POST['IDSetorFuncaoOrigem']) && isset($_POST['IDFuncao']) && isset($_POST['IDEmpregadorTrabalhador'])) {
            $IDSetor = $_POST['IDSetor'];
            $IDFuncao = $_POST['IDFuncao'];
            $IDEmpTrab = $_POST['IDEmpregadorTrabalhador'];
            $IDSetorFuncaoOrigem = $_POST['IDSetorFuncaoOrigem'];

            $atualTrabSetorFuncao = TrabalhadorSetorFuncao::model()->findByAttributes(
                [
                    'IDEmpregadorTrabalhador' => $IDEmpTrab,
                    'IDSetorFuncao' => $IDSetorFuncaoOrigem,
                ]
            );

            $novoSetorFuncao = SetorFuncao::model()->findByAttributes(
                [
                    'IDSetor' => $IDSetor,
                    'IDFuncao' => $IDFuncao
                ]
            );

            if ($atualTrabSetorFuncao && $novoSetorFuncao) {

                $pendenciasSaidaFuncao = PendenciaExameComplementar::calculaDadosPendencias(
                    $atualTrabSetorFuncao,
                    PendenciaExameComplementar::ID_SITUACAO_MUDANCA_FUNCAO,
                    $atualTrabSetorFuncao->IDSetorFuncao,
                    true
                );

                if (!$pendenciasSaidaFuncao) {
                    $pendenciasSaidaFuncao = array();
                }

                $pendenciasEntradaFuncao = PendenciaExameComplementar::calculaDadosPendencias(
                    $atualTrabSetorFuncao,
                    PendenciaExameComplementar::ID_SITUACAO_MUDANCA_FUNCAO,
                    $novoSetorFuncao->IDSetorFuncao
                );

                if (!$pendenciasEntradaFuncao) {
                    $pendenciasEntradaFuncao = array();
                }

                $comparaRiscos = PendenciaExameComplementar::comparaRiscos(
                    $atualTrabSetorFuncao->IDSetorFuncao,
                    $novoSetorFuncao->IDSetorFuncao
                );

                $totalPendencias = array_merge($pendenciasEntradaFuncao, $pendenciasSaidaFuncao);

                if (!empty($totalPendencias)) {
                    $novaFuncao['pendencias'] = $totalPendencias;
                    $novaFuncao['mesmoRisco'] = $comparaRiscos;

                    echo CJSON::encode(
                        array(
                            'novaFuncaos' => $novaFuncao
                        )
                    );
                } else {
                    null;
                }
            }
        }
    }
} 
