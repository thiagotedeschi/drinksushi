<?php

/**
 * Controller do CRUD SituacaoTrabalhador
 *
 * @package base.Trabalhador
 */
class SituacaoTrabalhadorController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '220',
        'search' => '652',
        'view' => '653',
        'update' => '654',
        'delete' => '655',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . SituacaoTrabalhador::model()->labelModel();
        $model = new SituacaoTrabalhador;
        $model->scenario = 'create';

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['SituacaoTrabalhador'])) {
            $model->attributes = $_POST['SituacaoTrabalhador'];

            $model->dt_concessaoBeneficio = empty($model->dt_concessaoBeneficio) ? null : $model->dt_concessaoBeneficio;
            $model->dt_fimAfastamento = empty($model->dt_fimAfastamento) ? null : $model->dt_fimAfastamento;
            $model->dt_situacaoTrabalhador = empty($model->dt_situacaoTrabalhador) ? null : $model->dt_situacaoTrabalhador;
            if ($model->save()) {
                PendenciaExameComplementar::criaPendencias(
                    $model->IDTrabalhadorSetorFuncao,
                    $model->IDSituacaoTrabalhador
                );
                PendenciaExameClinico::criaPendencia($model->IDSituacaoTrabalhador);
                $this->redirect(array('search', 'id' => $model->IDSituacaoTrabalhador));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . SituacaoTrabalhador::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);
        $this->performAjaxValidation($model);
        $model->IDEmpregadorTrabalhador = $model->iDTrabalhadorSetorFuncao->IDEmpregadorTrabalhador;

        if (isset($_POST['SituacaoTrabalhador'])) {
            $model->attributes = $_POST['SituacaoTrabalhador'];

            //Se não for aposentado nem afastado limpar dados de benefício
            if ($model->IDTipoSituacao != 24 && $model->IDTipoSituacao != 29 && $model->iDTipoSituacao->status_situacao != 2) {
                $model->IDBeneficio = null;
                $model->dt_concessaoBeneficio = null;
                $model->n_beneficio = null;
                $model->n_requerimentoBeneficio = null;
            }

            $model->dt_concessaoBeneficio = empty($model->dt_concessaoBeneficio) ? null : $model->dt_concessaoBeneficio;
            $model->dt_fimAfastamento = empty($model->dt_fimAfastamento) ? null : $model->dt_fimAfastamento;
            $model->dt_situacaoTrabalhador = empty($model->dt_situacaoTrabalhador) ? null : $model->dt_situacaoTrabalhador;

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->IDSituacaoTrabalhador));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . SituacaoTrabalhador::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new SituacaoTrabalhador('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['SituacaoTrabalhador'])) {
            $model->attributes = $_GET['SituacaoTrabalhador'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SituacaoTrabalhador the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = SituacaoTrabalhador::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param SituacaoTrabalhador $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'situacao-trabalhador-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAjaxBeneficios()
    {
        $IDTipoSituacao = $_POST['IDTipoSituacao'];
        $TipoSituacao = TipoSituacao::model()->findByPk($IDTipoSituacao);
        if ($TipoSituacao) {
            if ($TipoSituacao->status_situacao == 2 && in_array($TipoSituacao->IDTipoSituacao, [3, 14, 15])) {
                //se a situação for afastado (acidente do trabalho, doença, doença recorrente)
                $beneficios = TipoBeneficio::model()->findAllByAttributes(['afastamento_beneficio' => true]);
                $afastamento = true;
            } //se a situação for aposentado
            elseif ($TipoSituacao->IDTipoSituacao == 24 || $TipoSituacao->IDTipoSituacao == 29) {
                $beneficios = TipoBeneficio::model()->findAllByAttributes(['afastamento_beneficio' => false]);
                $afastamento = false;
            }
            if (isset($beneficios)) {
                $beneficios = Html::listData($beneficios, 'IDTipoBeneficio', 'desc_beneficio');
                $data = array(
                    'data' => $beneficios,
                    'afastamento' => $afastamento,
                );
                echo CJSON::encode($data);
            }
        }
    }

    public function actionAjaxLiberaCat()
    {
        if (isset($_POST['IDTipoSituacao']) && isset($_POST['IDTrabalhadorSetorFuncao'])) {
            $IDTipoSituacao = $_POST['IDTipoSituacao'];
            $IDTrabalhador = $_POST['IDTrabalhadorSetorFuncao'];

            $TipoSituacao = TipoSituacao::model()->findByPk($IDTipoSituacao);

            $identificaCAT = CAT::model()->findAllByTrabalhadorSetorFuncao($IDTrabalhador)->findAll();

            if ($TipoSituacao) {
                if ($TipoSituacao->status_situacao == 2 && isset($identificaCAT)) {

                    $listaCat = Html::listData($identificaCAT, 'IDCAT', 'labelCAT');
                    $data = array(
                        'data' => $listaCat,
                        'afastamento' => true,
                    );

                } else {
                    $data = array(
                        'data' => array(),
                        'afastamento' => false,
                    );
                }
            }
            echo CJSON::encode($data);
        }
        else{
            echo null;
        }
    }


    public function actionViewContent($id)
    {
        $model = $this->loadModel($id);
        $this->layout = '//layouts/clean';
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'visualizacao/_content',
            array(
                'model' => $model,
            )
        );
    }

}
