<?php

class CatController extends Controller
{
    public $acoesActions = array(
        'create' => '370',
        'search' => '373',
        'view' => '371',
        'update' => '372',
        'delete' => '374',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;


    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . CAT::model()->labelModel();
        $model = new CAT;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['CAT'])) {
            $model->attributes = $_POST['CAT'];
            if ($model->validate()) {
                $model->IDEmpregadorTrabalhador = EmpregadorTrabalhador::model()->findByAttributes(
                    array('IDEmpregador' => $model->IDEmpregador, 'IDTrabalhador' => $model->IDTrabalhador)
                )->IDEmpregadorTrabalhador;

                if ($model->dt_emissaoCAT == '') {
                    $model->dt_emissaoCAT = null;
                }
                if ($model->dt_fimAfastamento == '') {
                    $model->dt_fimAfastamento = null;
                }
                if ($model->dt_inicioAfastamento == '') {
                    $model->dt_inicioAfastamento = null;
                }
                if ($model->dt_registroCAT == '') {
                    $model->dt_registroCAT = null;
                }

                if ($model->save()) {
                    if (isset($_POST['CAT']['situacaoTrabalhadors'])) {
                        // salvo na tabela situacaoTrabalhador o id da cat
                        foreach ($_POST['CAT']['situacaoTrabalhadors'] as $IDST) {
                            $newST = new SituacaoTrabalhador();
                            $newST->insereCat($IDST, $model->IDCAT);
                        }
                    }
                    $this->redirect(array('search', 'id' => $model->IDCAT));
                }
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . CAT::model()->labelModel() . ' #' . $id;
        $model = CAT::model()->findByPk($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['CAT'])) {
            $model->attributes = $_POST['CAT'];

            if ($model->dt_emissaoCAT == '') {
                $model->dt_emissaoCAT = null;
            }
            if ($model->dt_fimAfastamento == '') {
                $model->dt_fimAfastamento = null;
            }
            if ($model->dt_inicioAfastamento == '') {
                $model->dt_inicioAfastamento = null;
            }
            if ($model->dt_registroCAT == '') {
                $model->dt_registroCAT = null;
            }

            if ($model->save()) {
                // atualizo na tabela situacaoTrabalhador o id da cat
                $IDSituacaoTrabalhadors = SituacaoTrabalhador::model()->findAllByCat($model->IDCAT)->findAll();

                if (isset($_POST['CAT']['situacaoTrabalhadors'])) {
                    $postST = $_POST['CAT']['situacaoTrabalhadors'];
                    foreach ($postST as $ST) {
                        // cadastro a cat nas novas situações de afastamento
                        if (!in_array($ST, $IDSituacaoTrabalhadors)) {
                            $newST = new SituacaoTrabalhador();
                            $newST->insereCat($ST, $model->IDCAT);
                        }
                    }
                } else {
                    $postST = array();
                }

                foreach ($IDSituacaoTrabalhadors as $IDST) {
                    // se a situação não existe no vetor passado, então podemos setar como null o identificador da cat
                    if (!in_array($IDST['IDSituacaoTrabalhador'], $postST)) {
                        $newST = new SituacaoTrabalhador();
                        $newST->insereCat($IDST['IDSituacaoTrabalhador'], null);
                    }
                }

                $this->redirect(array('view', 'id' => $model->IDCAT));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . CAT::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new CAT('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['CAT'])) {
            $model->attributes = $_GET['CAT'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return CAT the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = CAT::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CAT $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cat-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAjaxTrabalhadorByEmpregador()
    {
        $IDEmpregador = $_POST["IDEmpregador"];
        if (Yii::app()->request->isAjaxRequest && $IDEmpregador) {
            $empregadorTrabalhador = EmpregadorTrabalhador::model()->findAllByAttributes(
                ['IDEmpregador' => $IDEmpregador]
            );
            $data = Html::listData(
                $empregadorTrabalhador,
                'IDTrabalhador',
                'iDTrabalhador.nome_trabalhador'
            );
            if ($data) {
                foreach ($data as $value => $name) {
                    echo Html::tag('option', array('value' => $value), Html::encode($name), true);
                }
            }
        }
    }


    public function actionAjaxSetorFuncaoByTrabalhador()
    {
        if (Yii::app()->request->isAjaxRequest) {
            if (!empty($_POST['IDTrabalhador'])) {
                $empregadorTrabalhador = EmpregadorTrabalhador::model()->findAllByAttributes(
                    array("IDTrabalhador" => $_POST['IDTrabalhador'])
                );
                if (isset($empregadorTrabalhador)) {
                    foreach ($empregadorTrabalhador as $empTrab) {
                        $trabalhadorSetorFuncao = $empTrab->trabalhadorSetorFuncaos;
                        if (isset($trabalhadorSetorFuncao)) {
                            foreach ($trabalhadorSetorFuncao as $trabSetFunc) {
                                $situacao = SituacaoTrabalhador::model()->findAllByAttributes(
                                    array("IDTrabalhadorSetorFuncao" => $trabSetFunc->IDTrabalhadorSetorFuncao)
                                );
                                $situacao = array_shift($situacao);
                                echo Html::tag(
                                    'option',
                                    array('value' => $situacao->IDSituacaoTrabalhador),
                                    Html::encode($trabSetFunc->getLabelSetorFuncaoSituacao()),
                                    true
                                );

                            }
                        }
                    }
                }
            }
        }
    }


    public function actionRetornaSituacoes()
    {
        if (!empty($_POST['IDTrabalhador'])) {
            $data = SituacaoTrabalhador::model()->with('situacaoTrabalhadors')->setorFuncao()->iDCatNull()->findAll(
                '"situacaoTrabalhadors"."IDTrabalhador" = ' . $_POST['IDTrabalhador']
            );
            $data = Html::listData($data, 'IDSituacaoTrabalhador', 'situacaoTrabalhadors.motivo_situacaoTrabalhador');
            foreach ($data as $value => $name) {
                echo Html::tag('option', array('value' => $value), Html::encode($name), true);
            }
        }
    }


}
