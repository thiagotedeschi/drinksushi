<?php

/**
 * Controller do CForm Afastamentos
 *
 * @package
 */
class AfastamentosController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '',
        'search' => '',
        'view' => '',
        'update' => '',
        'delete' => '',
        'gerar' => '781',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'gerar';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionGerar()
    {
        $model = new FAfastamentos;
        $this->pageTitle = 'Gerar Relatório de Afastamentos';

        if (isset($_POST['FAfastamentos'])) {
            $model->attributes = $_POST['FAfastamentos'];

            if ($model->validate()) {
                $model->iDEmpregador = Empregador::model()->findByPk($model->IDEmpregador);
                $model->situacaoTrabalhadors = SituacaoTrabalhador::model()->with(
                    "iDTrabalhadorSetorFuncao.iDEmpregadorTrabalhador"
                )->findAllByPeriodo(
                    $model->periodo_inicio,
                    $model->periodo_fim
                )->findAllByStatusSituacao([2])->findAllByTipoSituacao($model->IDTipoAfastamento)->findAllBySetorFuncao(
                        $model->IDSetorFuncao
                    )->findAll('"iDEmpregadorTrabalhador"."IDEmpregador" = ' . $model->IDEmpregador);

                $this->actionPrint($model);
            }
        }
        $this->render(
            '_form',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Performs the AJAX validation.
     * @param Afastamentos $modelForm the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'afastamentos-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionPrint($model)
    {
        $exportPdf = PAfastamentos::doc();
        $exportPdf->setData(array('title' => 'Relatório de Afastamentos', 'model' => $model));
        if ($model->download_PDF) {
            $model->pdf = $exportPdf->output('D');
        } else {
            $model->pdf = base64_encode($exportPdf->output('S'));
        }
    }


    public function actionRetornaSetores()
    {
        if (Yii::app()->request->isAjaxRequest && !empty($_POST['IDEmpregador'])) {
            $data = SetorFuncao::model()->with('iDSetor')->ativo()->findAll(
                '"iDSetor"."IDEmpregador" = ' . $_POST['IDEmpregador']
            );
            $data = Html::listData($data, 'IDSetor', 'iDSetor.nome_setor');
            foreach ($data as $value => $name) {
                echo Html::tag('option', array('value' => $value), Html::encode($name), true);
            }
        }
    }


    public function actionRetornaFuncoes()
    {
        if (Yii::app()->request->isAjaxRequest && !empty($_POST['IDSetor'])) {
            $data = SetorFuncao::model()->ativo()->findAllByAttributes(["IDSetor" => $_POST['IDSetor']]);
            $data = Html::listData($data, 'IDSetorFuncao', 'labelSetorFuncao');
            foreach ($data as $value => $name) {
                echo Html::tag('option', array('value' => $value), Html::encode($name), true);
            }
        }
    }
}
