<?php

/**
 * Controller do CRUD ObservacaoTrabalhador
 *
 * @package base.Trabalhador
 */
class ObservacaoTrabalhadorController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '466',
        'search' => '468',
        'view' => '465',
        'update' => '467',
        'delete' => '469',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . ObservacaoTrabalhador::model()->labelModel();
        $model = new ObservacaoTrabalhador;

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['ObservacaoTrabalhador'])) {
            $model->attributes = $_POST['ObservacaoTrabalhador'];
            if (isset($_POST['ObservacaoTrabalhador']['Profissionals'])) {
                $model->Profissionals = $_POST['ObservacaoTrabalhador']['Profissionals'];
            }
            $model->dt_desativacaoObservacao = null;
            if ($model->save()) {
                $usuarios = array();
                foreach ($model->Profissionals as $profissional) {
                    $profObsTrabalhador = new ProfissionalObservacaoTrabalhador();
                    $profObsTrabalhador->IDProfissional = $profissional;
                    $profObsTrabalhador->IDObservacaoTrabalhador = $model->IDObservacaoTrabalhador;
                    $profObsTrabalhador->save();
                }

                Notificacao::enviaNotificacao(
                    $model->Profissionals,
                    'NOVA_OBS_TRABALHADOR',
                    array('nome_trabalhador' => $model->iDEmpregadorTrabalhador->iDTrabalhador),
                    'envelope',
                    $this->createUrl('view', array('id' => $model->IDObservacaoTrabalhador))
                );
                $this->redirect(array('search', 'id' => $model->IDObservacaoTrabalhador));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . ObservacaoTrabalhador::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['ObservacaoTrabalhador'])) {
            $model->attributes = $_POST['ObservacaoTrabalhador'];
            if (isset($_POST['ObservacaoTrabalhador']['Profissionals'])) {
                $model->Profissionals = $_POST['ObservacaoTrabalhador']['Profissionals'];
            }
            if ($model->save()) {
                $model->atualizaProfissionais($model->Profissionals);
                $this->redirect(array('view', 'id' => $model->IDObservacaoTrabalhador));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . ObservacaoTrabalhador::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new ObservacaoTrabalhador('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['ObservacaoTrabalhador'])) {
            $model->attributes = $_GET['ObservacaoTrabalhador'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ObservacaoTrabalhador the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ObservacaoTrabalhador::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ObservacaoTrabalhador $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'observacao-trabalhador-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionDesativar($id)
    {
        $model = $this->loadModel($id);
        $model->scenario = 'disable';
        $this->performAjaxValidation($model);
        $this->pageTitle = 'Desativar ' . $model->labelModel() . " #" . $model->IDObservacaoTrabalhador;
        $enviarNotificacao = ($model->dt_desativacaoObservacao == null);

        if (isset($_POST['ObservacaoTrabalhador'])) {

            $model->attributes = $_POST['ObservacaoTrabalhador'];
            if ($model->save()) {
                if ($enviarNotificacao) {
                    $usuarios = array();
                    foreach ($model->Profissionals as $profissional) {
                        $usuarios[] = $profissional->IDProfissional;
                    }

                    Notificacao::enviaNotificacao(
                        $usuarios,
                        'DESATIVAR_OBS_TRABALHADOR',
                        array('nome_trabalhador' => $model->iDEmpregadorTrabalhador->iDTrabalhador),
                        'file-text',
                        Yii::app()->createUrl(
                            'Trabalhador/observacaoTrabalhador/view',
                            array('id' => $model->IDObservacaoTrabalhador)
                        )
                    );
                }
                $this->redirect('admin');
            }
        }

        $this->render(
            'disable',
            array(
                'model' => $model,
            )
        );
    }
}
