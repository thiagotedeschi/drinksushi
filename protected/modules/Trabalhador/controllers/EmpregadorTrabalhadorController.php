<?php
/**
 * Controller do CRUD EmpregadorTrabalhador
 *
 * @package base.Controllers
 */

class EmpregadorTrabalhadorController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '797',
        'search' => '798',
        'view' => '799',
        'update' => '800',
        'delete' => '801',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . EmpregadorTrabalhador::model()->labelModel();
        $model = new EmpregadorTrabalhador;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['EmpregadorTrabalhador'])) {
            $model->attributes = $_POST['EmpregadorTrabalhador'];
            if ($model->validate()) {

                // Trabalhador Setor
                if (!empty($_POST['TrabalhadorSetorFuncao'])) {
                    foreach ($_POST['TrabalhadorSetorFuncao'] as $trabSetFunc) {
                        $trabalhadorSetorFuncao = new TrabalhadorSetorFuncao;
                        $trabalhadorSetorFuncao->attributes = $trabSetFunc;

                        $trabalhadorSetorFuncao->dt_inicioTrabSetFunc = HData::brToSql(
                            $trabSetFunc['dt_inicioTrabSetFunc']
                        );

                        $trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp = HData::brToSql(
                            $trabSetFunc['dt_ultimoExameClinicoTrabEmp']
                        );

                        $situacaoTrabalhador = new SituacaoTrabalhador;
                        $situacaoTrabalhador->attributes = $trabSetFunc['situacao'];

                        if ($trabalhadorSetorFuncao->validate()) {
                            if ($situacaoTrabalhador->validate()) {
                                if ($model->save()) {
                                    $trabalhadorSetorFuncao->IDEmpregadorTrabalhador = $model->IDEmpregadorTrabalhador;
                                    if ($trabalhadorSetorFuncao->save()) {

                                        $situacaoTrabalhador->IDTrabalhadorSetorFuncao = $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao;

                                        $situacaoTrabalhador->dt_situacaoTrabalhador = HData::brToSql(
                                            $trabSetFunc['situacao']['dt_situacaoTrabalhador']
                                        );
                                        $situacaoTrabalhador->dt_concessaoBeneficio = empty($situacaoTrabalhador->dt_concessaoBeneficio) ? null : $situacaoTrabalhador->dt_concessaoBeneficio;
                                        $situacaoTrabalhador->dt_fimAfastamento = empty($situacaoTrabalhador->dt_fimAfastamento) ? null : $situacaoTrabalhador->dt_fimAfastamento;

                                        $situacaoTrabalhador->save();

                                    }
                                    if (isset($_POST['EmpregadorTrabalhador']['exameComplementars'])) {
                                        foreach ($_POST['EmpregadorTrabalhador']['exameComplementars'] as $IDExame) {
                                            if (isset($_POST['EmpregadorTrabalhador']['data_realizacaoExame'][$IDExame])) {
                                                $resultExame = new ResultadoExame();
                                                $resultExame->scenario = 'UltimoExame';
                                                $resultExame->IDEmpregadorTrabalhador = $model->IDEmpregadorTrabalhador;
                                                $resultExame->IDExameComplementar = $IDExame;
                                                $resultExame->ultimo_complementarResultadoExame = true;
                                                $resultExame->dt_resultadoExame = $_POST['EmpregadorTrabalhador']['data_realizacaoExame'][$IDExame];
                                                $resultExame->save();

                                            }
                                        }
                                    }

                                    $cipaTrabalhador = new CipaTrabalhador;
                                    $cipaTrabalhador->IDEmpregadorTrabalhador = $model->IDEmpregadorTrabalhador;
                                    $cipaTrabalhador->attributes = $_POST['CipaTrabalhador'];
                                    if ($cipaTrabalhador->validate()) {
                                        $cipaTrabalhador->save();
                                    }

                                    $this->redirect(array('search', 'id' => $model->IDEmpregadorTrabalhador));
                                }
                            } else {
                                $model->addError(
                                    'situacaoTrabalhador',
                                    'Não foi preenchida a Situação do trabalhador no formulário de cadastro de novo setor/função.'
                                );
                            }
                        } else {
                            $model->addError(
                                'trabalhadorSetorFuncao',
                                'O formulário de cadastro de novo setor/função tem campos não preenchidos.'
                            );
                        }
                    }
                } else {
                    $model->addError(
                        'trabalhadorSetorFuncao',
                        'Você deve cadastrar um Setor/Função para o trabalhador.'
                    );
                }


            } else {
                $model->getErrors();
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . EmpregadorTrabalhador::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);
        $cipaTrabalhador = new CipaTrabalhador;
        $this->performAjaxValidation($model);
        $erroSave = false;
        if (isset($_POST['EmpregadorTrabalhador'])) {
            $model->attributes = $_POST['EmpregadorTrabalhador'];

            if ($model->save()) {
                // Trabalhador Setor
                if (!empty($_POST['TrabalhadorSetorFuncao'])) {
                    if (isset($_POST['excluirPonto'])) {
                        foreach ($_POST['excluirPonto'] as $IDExcluir) {
                            TrabalhadorSetorFuncao::model()->deleteByPk($IDExcluir);
                        }
                    }

                    foreach ($_POST['TrabalhadorSetorFuncao'] as $trabSetFunc) {
                        if (!empty($trabSetFunc)) {
                            if (isset($trabSetFunc['IDTrabalhadorSetorFuncao'])) {
                                $trabalhadorSetorFuncao = TrabalhadorSetorFuncao::model()->findByPk(
                                    $trabSetFunc['IDTrabalhadorSetorFuncao']
                                );
                            }

                            if (!isset($trabalhadorSetorFuncao)) {
                                $trabalhadorSetorFuncao = new TrabalhadorSetorFuncao;
                            }

                            $trabalhadorSetorFuncao->attributes = $trabSetFunc;
                            $trabalhadorSetorFuncao->IDEmpregadorTrabalhador = $model->IDEmpregadorTrabalhador;
                            if (isset($trabSetFunc['dt_inicioTrabSetFunc']) && !empty($trabSetFunc['dt_inicioTrabSetFunc'])) {
                                $trabalhadorSetorFuncao->dt_inicioTrabSetFunc = HData::brToSql(
                                    $trabSetFunc['dt_inicioTrabSetFunc']
                                );
                            }

                            if (isset($trabSetFunc['dt_ultimoExameClinicoTrabEmp']) && !empty($trabSetFunc['dt_ultimoExameClinicoTrabEmp'])) {
                                $trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp = HData::brToSql(
                                    $trabSetFunc['dt_ultimoExameClinicoTrabEmp']
                                );
                            } else {
                                $trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp = null;
                            }

                            if (isset($trabSetFunc['situacao']['IDSituacaoTrabalhador'])) {
                                $situacaoTrabalhador = SituacaoTrabalhador::model()->findByPk(
                                    $trabSetFunc['situacao']['IDSituacaoTrabalhador']
                                );
                            }

                            if (!isset($situacaoTrabalhador)) {
                                $situacaoTrabalhador = new SituacaoTrabalhador;
                            }

                            $situacaoTrabalhador->attributes = $trabSetFunc['situacao'];

                            if (isset($trabSetFunc['situacao']['dt_situacaoTrabalhador']) && !empty($trabSetFunc['situacao']['dt_situacaoTrabalhador'])) {
                                $situacaoTrabalhador->dt_situacaoTrabalhador = HData::brToSql(
                                    $trabSetFunc['situacao']['dt_situacaoTrabalhador']
                                );
                            } else {

                                if (isset($trabSetFunc['dt_inicioTrabSetFunc']) && !empty($trabSetFunc['dt_inicioTrabSetFunc'])) {
                                    $trabalhadorSetorFuncao->dt_inicioTrabSetFunc = HData::sqlToBr(
                                        $trabSetFunc['dt_inicioTrabSetFunc']
                                    );
                                }

                                if (isset($trabSetFunc['dt_ultimoExameClinicoTrabEmp']) && !empty($trabSetFunc['dt_ultimoExameClinicoTrabEmp'])) {
                                    $trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp = HData::sqlToBr(
                                        $trabSetFunc['dt_ultimoExameClinicoTrabEmp']
                                    );
                                }
                            }

                            $situacaoTrabalhador->dt_concessaoBeneficio = empty($situacaoTrabalhador->dt_concessaoBeneficio) ? null : $situacaoTrabalhador->dt_concessaoBeneficio;
                            $situacaoTrabalhador->dt_fimAfastamento = empty($situacaoTrabalhador->dt_fimAfastamento) ? null : $situacaoTrabalhador->dt_fimAfastamento;

                            if ($trabalhadorSetorFuncao->validate()) {
                                if ($situacaoTrabalhador->validate()) {
                                    if ($trabalhadorSetorFuncao->save()) {
                                        $situacaoTrabalhador->IDTrabalhadorSetorFuncao = $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao;
                                        $situacaoTrabalhador->save();
                                    }
                                } else {
                                    $erroSave = true;
                                    $situacaoTrabalhador->getErrors();
                                    $model->addError(
                                        'situacaoTrabalhador',
                                        'O formulário de cadastro de novo setor/função tem campos não preenchidos na situação do trabalhador.'
                                    );
                                }
                            } else {
                                $erroSave = true;
                                $trabalhadorSetorFuncao->getErrors();
                                $model->addError(
                                    'trabalhadorSetorFuncaos',
                                    'O formulário de cadastro de novo setor/função tem campos não preenchidos nas informações basicas.'
                                );
                            }
                            $trabalhadorSetorFuncaos[] = $trabalhadorSetorFuncao;

                            unset($trabalhadorSetorFuncao);
                            unset($situacaoTrabalhador);
                        }
                    }
                    $model->trabalhadorSetorFuncaos = $trabalhadorSetorFuncaos;
                }

                if (isset($_POST['EmpregadorTrabalhador']['exameComplementars'])) {
                    foreach ($_POST['EmpregadorTrabalhador']['exameComplementars'] as $IDExame) {
                        if (isset($_POST['EmpregadorTrabalhador']['data_realizacaoExame'][$IDExame])) {
                            $resultExame = new ResultadoExame();
                            $resultExame->scenario = 'UltimoExame';
                            $resultExame->IDEmpregadorTrabalhador = $model->IDEmpregadorTrabalhador;
                            $resultExame->IDExameComplementar = $IDExame;
                            $resultExame->ultimo_complementarResultadoExame = true;
                            $resultExame->dt_resultadoExame = $_POST['EmpregadorTrabalhador']['data_realizacaoExame'][$IDExame];
                            $resultExame->save();
                        }
                    }
                }

                if (isset($_POST['CipaTrabalhador']['IDCipaTrabalhador'])) {
                    if (!empty($_POST['CipaTrabalhador']['IDCipaTrabalhador'])) {
                        $cipaTrabalhador = CipaTrabalhador::model()->findByPk(
                            $_POST['CipaTrabalhador']['IDCipaTrabalhador']
                        );
                    } else {
                        $cipaTrabalhador->IDEmpregadorTrabalhador = $model->IDEmpregadorTrabalhador;

                    }
                }

                $cipaTrabalhador->attributes = $_POST['CipaTrabalhador'];
                if ($cipaTrabalhador->validate()) {
                    $cipaTrabalhador->save();
                }

                if (!$erroSave) {
                    $this->redirect(array('view', 'id' => $model->IDEmpregadorTrabalhador));
                }

            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'cipaTrabalhador' => $cipaTrabalhador
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . EmpregadorTrabalhador::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new EmpregadorTrabalhador('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['EmpregadorTrabalhador'])) {
            $model->attributes = $_GET['EmpregadorTrabalhador'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return EmpregadorTrabalhador the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = EmpregadorTrabalhador::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param EmpregadorTrabalhador $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'empregador-trabalhador-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionAjaxMontaSetorFuncao()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $trabalhadorSetorFuncao = new TrabalhadorSetorFuncao;
            $trabalhadorSetorFuncao->IDEmpregador = $_REQUEST['IDEmpregador'];
            $form = new CActiveForm;
            $this->renderPartial(
                'cadastro/setorFuncao/_infoBasica',
                array(
                    'trabalhadorSetorFuncao' => $trabalhadorSetorFuncao,
                    'form' => $form,
                    'i' => $_REQUEST
                ),
                true
            );
        }
    }


    public function actionCriaPortletSetorFuncao()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $form = new CActiveForm;
            $this->renderPartial(
                'cadastro/_portletSetorFuncao',
                array('trabalhadorSetorFuncao' => new TrabalhadorSetorFuncao(), 'form' => $form, 'i' => $_REQUEST['i'])
            );
        }
    }

}
