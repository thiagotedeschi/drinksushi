<?php

class TrabalhadorController extends Controller
{

    public $acoesActions = array(
        'create' => '213',
        'search' => '214',
        'view' => '215',
        'viewByEmpregadorTrabalhador' => '215',
        'update' => '216',
        'delete' => '217',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;


    public function actionViewByEmpregadorTrabalhador($id)
    {
        $empTrab = EmpregadorTrabalhador::model()->findByPk($id);
        $this->actionView($empTrab->IDTrabalhador);
    }


    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = Trabalhador::model()->findByPk($id);

        $trabalhadorCondicao = TrabalhadorCondicao::model()->findCondicaoByTrabalhador($model->IDTrabalhador)->findAll(
        );

        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;

        $this->render(
            'view',
            array(
                'model' => $model,
                'trabalhadorCondicao' => $trabalhadorCondicao
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Trabalhador::model()->labelModel();
        $model = new Trabalhador;
        $endereco = new EnderecoTrabalhador();
        $empregadorTrabalhador = new EmpregadorTrabalhador;
        $trabalhadorSetorFuncao = new TrabalhadorSetorFuncao;
        $trabalhadorCondicao = new TrabalhadorCondicao;
        $cipaTrabalhador = new CipaTrabalhador;
        $situacaoTrabalhador = new SituacaoTrabalhador;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation(
            array(
                $model,
                $empregadorTrabalhador,
                $trabalhadorSetorFuncao,
                $situacaoTrabalhador,
                $cipaTrabalhador,
                $trabalhadorCondicao
            )
        );

        if (isset($_POST['Trabalhador'])) {

            $empregadorTrabalhador->attributes = $_POST['EmpregadorTrabalhador'];
            $trabalhadorSetorFuncao->attributes = $_POST['TrabalhadorSetorFuncao'];
            $situacaoTrabalhador->attributes = $_POST['SituacaoTrabalhador'];
            $situacaoTrabalhador->scenario = 'Create';

            $endereco->attributes = $_POST['EnderecoTrabalhador'];
            if ($endereco->logradouro_endereco != '') {
                if ($endereco->save()) {
                    $model->IDEndereco = $endereco->IDEndereco;
                }
            }

            $model->attributes = $_POST['Trabalhador'];
            !empty($model->rg_trabalhador) ? : $model->uf_rgTrabalhador = null;
            if ($trabalhadorSetorFuncao->validate() && $situacaoTrabalhador->validate() && $model->save()) {
                $empregadorTrabalhador->IDTrabalhador = $model->IDTrabalhador;
                $empregadorTrabalhador->dt_admissaoTrabEmp = empty($empregadorTrabalhador->dt_admissaoTrabEmp) ? null : $empregadorTrabalhador->dt_admissaoTrabEmp;
                $situacaoTrabalhador->IDTrabalhadorSetorFuncao = $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao;
                $situacaoTrabalhador->dt_concessaoBeneficio = empty($situacaoTrabalhador->dt_concessaoBeneficio) ? null : $situacaoTrabalhador->dt_concessaoBeneficio;
                $situacaoTrabalhador->dt_fimAfastamento = empty($situacaoTrabalhador->dt_fimAfastamento) ? null : $situacaoTrabalhador->dt_fimAfastamento;

                if ($empregadorTrabalhador->save()) {

                    $trabalhadorSetorFuncao->dt_inicioTrabSetFunc = HData::brToSql(
                        $trabalhadorSetorFuncao->dt_inicioTrabSetFunc
                    );
                    $trabalhadorSetorFuncao->dt_fimTrabSetFunc = empty($trabalhadorSetorFuncao->dt_fimTrabSetFunc) ? null : $trabalhadorSetorFuncao->dt_fimTrabSetFunc;
                    $trabalhadorSetorFuncao->IDEmpregadorTrabalhador = $empregadorTrabalhador->IDEmpregadorTrabalhador;
                    $trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp = empty($trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp) ? null : $trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp;

                    if ($trabalhadorSetorFuncao->save()) {
                        //Salva últimos exames
                        if (isset($empregadorTrabalhador->exameComplementars)) {
                            foreach ($empregadorTrabalhador->exameComplementars as $IDExame) {
                                if (isset($_POST['EmpregadorTrabalhador']['data_realizacaoExame'][$IDExame])) {
                                    $resultExame = new ResultadoExame();
                                    $resultExame->scenario = 'UltimoExame';
                                    $resultExame->IDEmpregadorTrabalhador = $empregadorTrabalhador->IDEmpregadorTrabalhador;
                                    $resultExame->IDExameComplementar = $IDExame;
                                    $resultExame->ultimo_complementarResultadoExame = true;
                                    $resultExame->dt_resultadoExame = $_POST['EmpregadorTrabalhador']['data_realizacaoExame'][$IDExame];
                                    $resultExame->save();
                                }
                            }
                        }

//                        $cipaTrabalhador->attributes = $_POST['CipaTrabalhador'];
//                        if ($cipaTrabalhador->validate()) {
//                            $cipaTrabalhador->IDTrabalhadorSetorFuncao = $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao;
//                            $cipaTrabalhador->save();
//                        }

                        $situacaoTrabalhador->IDTrabalhadorSetorFuncao = $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao;

                        if ($situacaoTrabalhador->save()) {
                            PendenciaExameComplementar::criaPendencias(
                                $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao,
                                $situacaoTrabalhador->IDSituacaoTrabalhador
                            );

                            PendenciaExameClinico::criaPendencia($situacaoTrabalhador);


                            if (isset($_POST['TrabalhadorCondicao']['IDTipoCondicao'])) {
                                foreach ($_POST['TrabalhadorCondicao']['IDTipoCondicao'] as $tpCondicao) {
                                    $trabalhadorCondicaos = new TrabalhadorCondicao;
                                    $trabalhadorCondicaos->IDTrabalhador = $model->IDTrabalhador;
                                    $trabalhadorCondicaos->dt_inicioCondicao = HData::brToSql(
                                        $_POST['TrabalhadorCondicao']['dt_inicioCondicao'][$tpCondicao]
                                    );
                                    $trabalhadorCondicaos->IDTipoCondicao = $tpCondicao;
                                    $trabalhadorCondicaos->save();

                                }
                            }

                            $this->redirect(array('search', 'id' => $model->IDTrabalhador));
                        }
                    }
                }
            } else {
                if (!$endereco->isNewRecord) {
                    $endereco->delete();
                }
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
                'endereco' => $endereco,
                'empregadorTrabalhador' => $empregadorTrabalhador,
                'trabalhadorSetorFuncao' => $trabalhadorSetorFuncao,
                'trabalhadorCondicao' => $trabalhadorCondicao,
                'cipaTrabalhador' => $cipaTrabalhador,
                'situacaoTrabalhador' => $situacaoTrabalhador
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        /* @var $model Trabalhador */
        $this->pageTitle = 'Editar ' . Trabalhador::model()->labelModel() . ' #' . $id;
        $model = Trabalhador::model()->findByPk($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        $situacaoTrabalhador = new SituacaoTrabalhador;
        $trabalhadorCondicao = TrabalhadorCondicao::model()->findCondicaoByTrabalhador($model->IDTrabalhador)->findAll(
        );
        $cipaTrabalhador = new CipaTrabalhador;
        $empregadorTrabalhador = new EmpregadorTrabalhador;
        $trabalhadorSetorFuncao = new TrabalhadorSetorFuncao;
        $endereco = $model->iDEndereco;
        if ($endereco == null) {
            $endereco = new EnderecoTrabalhador;
        }

        //Verifica se o Trabalhador já possui EmpregadorTrabalhador, SituacaoTrabalhador e CipaTrabalhador
        if (isset($model->empregadorTrabalhadors['0'])) {
            $empregadorTrabalhador = $model->empregadorTrabalhadors[0];
            $IDEmpregadorTrabalhador = $empregadorTrabalhador->IDEmpregadorTrabalhador;

            $trabalhadorSetorFuncao = $model->empregadorTrabalhadors[0]->trabalhadorSetorFuncaos;
            $trabalhadorSetorFuncao = array_shift($trabalhadorSetorFuncao);

            $IDTrabalhadorSetorFuncao = $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao;
            $situacaoTrabalhador = SituacaoTrabalhador::model()->findByAttributes(
                array('IDTrabalhadorSetorFuncao' => $IDTrabalhadorSetorFuncao)
            );
            $cipaTrabalhador = CipaTrabalhador::model()->findByAttributes(
                array('IDEmpregadorTrabalhador' => $IDEmpregadorTrabalhador)
            );
            if ($situacaoTrabalhador == null) {
                $situacaoTrabalhador = new SituacaoTrabalhador;
            }
            if ($cipaTrabalhador == null) {
                $cipaTrabalhador = new CipaTrabalhador;
            }
            if ($trabalhadorCondicao == null) {
                $trabalhadorCondicao = new TrabalhadorCondicao;
            }

            //Verifica se existe associação do Trabalhador com algum SetorFuncao
            $resultadosExame = ResultadoExame::model()->findUltimosByTrabalhador(
                $empregadorTrabalhador->IDEmpregadorTrabalhador
            )->findAll();

            $empregadorTrabalhador->exameComplementars = Html::listData(
                $resultadosExame,
                'IDExameComplementar',
                'IDExameComplementar'
            );
        }

        if (isset($_POST['Trabalhador'])) {
            $endereco->attributes = $_POST['EnderecoTrabalhador'];
            $model->attributes = $_POST['Trabalhador'];

            if ($endereco->logradouro_endereco != '') {
                if ($endereco->save()) {
                    $model->IDEndereco = $endereco->IDEndereco;
                }
            }

            if ($model->save()) {
                // Fim da Atualização das condições de um trabalhador
                $this->redirect(array('search', 'id' => $model->IDTrabalhador));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'endereco' => $endereco,
                'empregadorTrabalhador' => $empregadorTrabalhador,
                'trabalhadorSetorFuncao' => $trabalhadorSetorFuncao,
                'trabalhadorCondicao' => $trabalhadorCondicao,
                'cipaTrabalhador' => $cipaTrabalhador,
                'situacaoTrabalhador' => $situacaoTrabalhador
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $model = Trabalhador::model()->findByPk($id);
                if (!$model->empregadorTrabalhadors) {
                    $model->delete();
                } else {
                    $mensagem = 'Você não pode excluir múltiplos Trabalhadores que estejam associdados a mais de um Empregador. <br>';
                    Yii::app()->user->setFlash('error', array($mensagem));
                }
            }
        } else {
            $model = Trabalhador::model()->findByPk($id);
            $model->delete();
            /*if (!$model->empregadorTrabalhadors) {
                $model->delete();
            }*/
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Trabalhador::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Trabalhador('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Trabalhador'])) {
            $model->attributes = $_GET['Trabalhador'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Trabalhador the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Trabalhador::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Trabalhador $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'trabalhador-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionFotoTrabalhador($id)
    {
        $model = Trabalhador::model()->find(
            array('select' => "foto_trabalhador, genero_trabalhador", 'condition' => '"IDTrabalhador" = ' . $id)
        );
        if ($model == null) {
            return;
        }
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Transfer-Encoding: binary');
        header('Content-Type: ' . 'image/png');
        if ($model->foto_trabalhador != '') {
            $stringImagem = stream_get_contents($model->foto_trabalhador);
            echo base64_decode($stringImagem);
        } else {
            if ($model->genero_trabalhador) {
                echo file_get_contents(ASSETS_LINK . '/images/main/usuarios/default_male.png');
            } else {
                echo file_get_contents(ASSETS_LINK . '/images/main/usuarios/default_female.png');
            }
        }
    }


    public function actionRetornaSetores()
    {
        if (Yii::app()->request->isAjaxRequest && !empty($_POST['undefined'])) {
            $data = Setor::model()->findAll('"IDEmpregador"=' . $_POST['undefined']);
            $data = Html::listData($data, 'IDSetor', 'nome_setor');
            echo Html::tag('option', array('value' => ''), Html::encode('Escolha um Setor'), true);
            foreach ($data as $value => $name) {
                echo Html::tag('option', array('value' => $value), Html::encode($name), true);
            }
        }
    }

    public function actionRetornaFuncoes()
    {
        if (isset($_POST['IDSetor']) && !empty($_POST['IDSetor'])) {
            $IDSetor = $_POST['IDSetor'];
        } elseif (!empty($_POST['undefined'])) {
            $IDSetor = $_POST['undefined'];
        }
        if (Yii::app()->request->isAjaxRequest && isset($IDSetor) && !empty($IDSetor)) {
            $data = SetorFuncao::model()->findAll('"IDSetor"=' . $IDSetor);
            $data = Html::listData($data, 'IDSetorFuncao', 'iDFuncao.nome_funcao');
            echo Html::tag('option', array('value' => ''), Html::encode('Escolha uma Função'), true);
            foreach ($data as $value => $name) {
                echo Html::tag('option', array('value' => $value), Html::encode($name), true);
            }
        }
    }


    public function actionRetornaFuncoesByTrabalhador()
    {
        if (isset($_POST['IDSetor']) && !empty($_POST['IDSetor'])) {
            $IDSetor = $_POST['IDSetor'];
        } elseif (!empty($_POST['undefined'])) {
            $IDSetor = $_POST['undefined'];
        }
        if (Yii::app()->request->isAjaxRequest && isset($IDSetor) && !empty($IDSetor)) {
            if (isset($_POST['IDTrabalhador']) && !empty($_POST['IDTrabalhador'])) {
                $IDTrabalhador = $_POST['IDTrabalhador'];
                $funcoes = Funcao::model()->getFuncaobyTrabalhador($IDTrabalhador);
            }

            echo Html::tag('option', array('value' => ''), Html::encode('Escolha uma Função'), true);
            $mesmoSetor = false;

            if (isset($funcoes) && !empty($funcoes)) {
                foreach ($funcoes as $valor) {

                    if ($valor["setor"] == $IDSetor) {

                        $mesmoSetor = true;
                        $data = SetorFuncao::model()->findAllByAttributes(array("IDSetor" => $IDSetor));
                        $data = Html::listData($data, 'IDSetorFuncao', 'iDFuncao.IDFuncao');
                        foreach ($data as $value => $name) {

                            if ($valor["funcao"] != $name) {
                                $nome_Funcao = Funcao::model()->findAllByAttributes(array("IDFuncao" => $name));
                                $nome_Funcao = array_shift($nome_Funcao);
                                echo Html::tag('option', array('value' => $value), Html::encode($nome_Funcao), true);
                            }

                        }

                    }
                }

            }

            if (!$mesmoSetor) {
                $data = SetorFuncao::model()->findAllByAttributes(array("IDSetor" => $IDSetor));
                $data = Html::listData($data, 'IDSetorFuncao', 'iDFuncao.nome_funcao');
                foreach ($data as $value => $name) {
                    echo Html::tag('option', array('value' => $value), Html::encode($name), true);
                }
            }

        }
    }


    public function actionAjaxTrabalhadorByTermoEmpregador()
    {
        $attributeID = isset($_GET['attributeID']) ? $_GET['attributeID'] : 'IDTrabalhador';
        $attributeLabel = isset($_GET['attributeLabel']) ? $_GET['attributeLabel'] : 'nome_trabalhador';
        $statusSituacao = isset($_GET['statusSituacao']) ? $_GET['statusSituacao'] : [];
        if (Yii::app()->request->isAjaxRequest) {
            if (isset($_GET['limite'])) {
                $limite = $_GET['limite'];
            } else {
                $limite = 10;
            }

            $empregador = $_GET['IDEmpregador'];
            $term = $_GET['term'];

            $trabEmps = EmpregadorTrabalhador::model()->findAllByStatusSituacao($statusSituacao)->findAllByEmpregador(
                $empregador,
                $term,
                $limite
            )->findAll();
            $data = array();
            foreach ($trabEmps as $trabEmp) {
                if (isset($trabEmp->iDTrabalhador->$attributeLabel)) {
                    $title = $trabEmp->iDTrabalhador->$attributeLabel;
                } else {
                    $title = $trabEmp->$attributeLabel;
                }
                $data[] = array(
                    'title' => $title,
                    'id' => $trabEmp->$attributeID
                );
            }
            echo CJSON::encode($data);
        }
    }


    public function actionajaxTrabalhadorSemVinculo()
    {
        if (Yii::app()->request->isAjaxRequest && isset($_GET['IDEmpregador'])) {
            if (isset($_GET['limite'])) {
                $limite = $_GET['limite'];
            } else {
                $limite = 10;
            }

            $empregador = empty($_GET['IDEmpregador']) ? null : $_GET['IDEmpregador'];
            $term = empty($_GET['term']) ? null : '%' . strtolower($_GET['term']) . '%';

            $trabalhadors = Trabalhador::model()->findAllSemVinculo($empregador)->findAll(
                array(
                    'condition' => 'LOWER("t"."nome_trabalhador") LIKE ' . "'$term'",
                    'limit' => $limite,
                )
            );
            $data = array();
            foreach ($trabalhadors as $trabalhador) {
                $data[] = array(
                    'title' => $trabalhador->getLabelTrabalhador(),
                    'id' => $trabalhador->IDTrabalhador
                );
            }
            echo CJSON::encode($data);
        }
    }


    public function actionAjaxTrabalhadorBySetorFuncao()
    {
        if (Yii::app()->request->isAjaxRequest && $_GET['IDSetor'] && $_GET['IDFuncao']) {
            $setorFuncao = SetorFuncao::model()->findByAttributes(
                ["IDSetor" => $_GET['IDSetor'], "IDFuncao" => $_GET['IDFuncao']]
            );
            $trabalhadores = TrabalhadorSetorFuncao::model()->getTrabalhadores(
                $setorFuncao->IDSetorFuncao
            )->findAll();
            if ($trabalhadores != null) {
                $options = '';
                foreach ($trabalhadores as $trabalhador) {
                    $options .= Html::tag(
                        'option',
                        array('value' => $trabalhador->IDTrabalhador),
                        $trabalhador->iDTrabalhador->nome_trabalhador,
                        true
                    );
                }
                $data = array(
                    "options" => $options,
                    "IDSetorFuncao" => $setorFuncao->IDSetorFuncao
                );
                echo CJSON::encode($data);
            }
        }
    }


    public function actionAjaxSetorFuncaoByEmpregadorTrabalhador()
    {
        $IDSetorFuncao = null;
        if (Yii::app()->request->isAjaxRequest && isset($_GET['IDEmpregadorTrabalhador'])) {
            $empTrab = EmpregadorTrabalhador::model()->findByAttributes(
                ['IDEmpregadorTrabalhador' => $_GET['IDEmpregadorTrabalhador']]
            );
            if ($empTrab) {
                $SetorFuncao = $empTrab->getSetorFuncao();
                $SetorFuncao = Html::listData($SetorFuncao, 'IDSetorFuncao', 'labelSetorFuncao');
                echo CJSON::encode($SetorFuncao);
            }
        }
    }

    public function actionAjaxEmpregadorTrabalhador()
    {
        $IDEmpregador = $_POST["IDEmpregador"];
        if (Yii::app()->request->isAjaxRequest && $IDEmpregador) {
            $empregadorTrabalhador = EmpregadorTrabalhador::model()->findAllByAttributes(
                ['IDEmpregador' => $IDEmpregador]
            );
            $data = Html::listData(
                $empregadorTrabalhador,
                'IDTrabalhadorSetorFuncao',
                'iDTrabalhador.nome_trabalhador'
            );
            if ($data) {
                foreach ($data as $value => $name) {
                    echo Html::tag('option', array('value' => $value), Html::encode($name), true);
                }
            }
        }
    }


    public function actionAjaxSituacaoTrabalhador()
    {
        $IDSetorFuncao = null;
        if (Yii::app()->request->isAjaxRequest && isset($_GET['IDEmpregadorTrabalhador'])) {
            $empTrab = EmpregadorTrabalhador::model()->findByAttributes(
                ['IDEmpregadorTrabalhador' => $_GET['IDEmpregadorTrabalhador']]
            );
            if ($empTrab) {
                $trabSF = $empTrab->getTrabalhadorSetorFuncao(false);
                $trabSF = Html::listData($trabSF, 'IDTrabalhadorSetorFuncao', 'labelSetorFuncaoSituacao');
                echo json_encode($trabSF);
            }
        }
    }

    public function actionTrabalhadorSetorFuncao($id)
    {
        $model = TrabalhadorSetorFuncao::model()->findByPk($id);

        $this->pageTitle = 'Visualizar Dados';
        $this->render(
            'visualizacao/_trabalhadorSetorFuncao',
            array(
                'model' => $model
            )
        );
    }

    public function actionAjaxSituacoesDisponiveis()
    {
        if (isset($_GET['IDTrabalhadorSetorFuncao'])) {
            $situacoes = TipoSituacao::model()->findSituacoesDisponiveis($_GET['IDTrabalhadorSetorFuncao'])->findAll();
            if ($situacoes) {
                $situacoes = Html::listData($situacoes, 'IDTipoSituacao', 'nome_situacao');
                echo json_encode($situacoes);
            }
        }
    }

    public function actionAjaxExcluiSetorFuncao()
    {
        if (isset($_POST['IDSetorFuncao']) && !empty($_POST['IDSetorFuncao'])) {
            $IDSetorFuncao = $_POST['IDSetorFuncao'];
        } elseif (!empty($_POST['undefined'])) {
            $IDSetorFuncao = $_POST['undefined'];
        }

        $sucesso = false;
        $existeExame = false;
//        $contaExameComplementar = 0;

        if (Yii::app()->request->isAjaxRequest && isset($IDSetorFuncao) && !empty($IDSetorFuncao)) {


            $trabalhadorSetorFuncao = TrabalhadorSetorFuncao::model()->findByPk($IDSetorFuncao);

            // procuro se existe algum exame clínico associado a situação do trabalhador
            $situacaoTrabalhador = SituacaoTrabalhador::model()->findAllByAttributes(
                array("IDTrabalhadorSetorFuncao" => $trabalhadorSetorFuncao->IDTrabalhadorSetorFuncao)
            );

            if (isset($situacaoTrabalhador) && !empty($situacaoTrabalhador)) {
                $situacaoTrabalhador = array_shift($situacaoTrabalhador);

                if ($situacaoTrabalhador->IDExameClinico != null) {
                    $existeExame = true;
                }
            }

            $trabalhadorSetorFuncaos = TrabalhadorSetorFuncao::model()->findAllByAttributes(
                array("IDEmpregadorTrabalhador" => $trabalhadorSetorFuncao->IDEmpregadorTrabalhador)
            );
            $podeExcluir = count($trabalhadorSetorFuncaos);

            // não pode excluir se existir somente um vínculo ou algum exame para o setor função
            if ($podeExcluir > 1 && !$existeExame && !isset($trabalhadorSetorFuncao->dt_ultimoExameClinicoTrabEmp)) {
                $sucesso = $trabalhadorSetorFuncao->delete();
            }

            if ($sucesso) {
                echo "OK";
            } else {
                echo "ERRO";
            }
        }
    }

}