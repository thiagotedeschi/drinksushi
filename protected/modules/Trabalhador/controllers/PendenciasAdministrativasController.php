<?php

/**
 * Controller do CForm Pendências Administrativas
 *
 * @package base.SetorFuncao
 */
class PendenciasAdministrativasController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '',
        'search' => '',
        'view' => '',
        'update' => '',
        'delete' => '',
        'gerar' => '775',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'gerar';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     */
    public function actionGerar()
    {
        $model = new FPendenciasAdministrativas;
        $this->pageTitle = 'Gerar Pendências Administrativas';

        if (isset($_POST['FPendenciasAdministrativas'])) {
            $model->attributes = $_POST['FPendenciasAdministrativas'];
            $model->iDEmpregador = Empregador::model()->findByPk($model->IDEmpregador);
            $model->empregadorTrabalhadors = EmpregadorTrabalhador::model()->findAllByTrabalhadoresAtivos(
            )->findAllByEmpregador($model->IDEmpregador)->findAll(
                    [
                        'group' => '"t"."IDEmpregadorTrabalhador", "trab"."nome_trabalhador"',
                        'order' => '"trab"."nome_trabalhador"'
                    ]
                );
            if ($model->validate()) {
                $this->actionPrint($model);
            }
        }
        $this->render(
            '_form',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Performs the AJAX validation.
     * @param RiscoExames $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'riscos-exames-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionPrint($model)
    {
        $exportPdf = PPendenciasAdministrativas::doc();
        $exportPdf->setData(array('title' => 'Pendências Administrativas', 'model' => $model));
        $exportPdf->output();
    }
}
