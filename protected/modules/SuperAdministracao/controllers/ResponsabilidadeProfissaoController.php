<?php

class ResponsabilidadeProfissaoController extends Controller
{

    public $acoesActions = array(
        'create' => '232',
        'search' => '230',
        'view' => '231',
        'update' => '233',
        'delete' => '235',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . ResponsabilidadeProfissao::model()->labelModel();
        $model = new ResponsabilidadeProfissao;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['ResponsabilidadeProfissao'])) {
            $model->attributes = $_POST['ResponsabilidadeProfissao'];
            if ($model->save()) {

                $this->redirect(array('search', 'id' => $model->IDResponsabilidadeProfissao));
            }
        }

        $this->render('create', array('model' => $model));
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . ResponsabilidadeProfissao::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['ResponsabilidadeProfissao'])) {
            $model->attributes = $_POST['ResponsabilidadeProfissao'];
            if ($model->save()) {

                $this->redirect(array('search', 'id' => $model->IDResponsabilidadeProfissao));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . ResponsabilidadeProfissao::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new ResponsabilidadeProfissao('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['ResponsabilidadeProfissao'])) {
            $model->attributes = $_GET['ResponsabilidadeProfissao'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ResponsabilidadeProfissao the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ResponsabilidadeProfissao::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ResponsabilidadeProfissao $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'responsabilidade-profissao-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
