<?php


class ItemMenuController extends Controller
{

    public $pageTitle;
    public $defaultAction = 'search';
    public $acoesActions = array(
        'create' => '7',
        'search' => '8',
        'view' => '9',
        'update' => '11',
        'delete' => '10',
    );

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar Item de Menu #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . ItemMenu::model()->labelModel();
        $model = new ItemMenu;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['ItemMenu'])) {
            $model->attributes = $_POST['ItemMenu'];
            if ($model->save()) {
                unset(Yii::app()->session['acoes']);
                unset(YII::app()->session['menu']);
                $this->getMenu();

                $IDTagItemMenus = isset($_POST['ItemMenu']['tags']) ? explode(
                    ',',
                    $_POST['ItemMenu']['tags']
                ) : array();
                $model->atualizaTagItemMenu($IDTagItemMenus);

                $this->redirect(array('search', 'id' => $model->IDItem_menu));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . ItemMenu::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['ItemMenu'])) {
            $model->attributes = $_POST['ItemMenu'];
            if ($model->save()) {
                unset(Yii::app()->session['acoes']);
                unset(YII::app()->session['menu']);
                $this->getMenu();

                $IDTagItemMenus = isset($_POST['ItemMenu']['tags']) ? explode(
                    ',',
                    $_POST['ItemMenu']['tags']
                ) : array();
                $model->atualizaTagItemMenu($IDTagItemMenus);

                $this->redirect(array('search', 'id' => $model->IDItem_menu));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Manages all models.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . ItemMenu::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
        }
        $model = new ItemMenu('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['ItemMenu'])) {
            $model->attributes = $_GET['ItemMenu'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ItemMenu the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ItemMenu::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ItemMenu $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'item-menu-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionItemsDinamicos($tipo)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $condicao = '';
            if ($tipo == 'comfilhos') {
                $condicao = 'AND "IDMenu_pai" IS NULL';
            }
            if ($tipo == 'semfilhos') {
                $condicao = 'AND "IDMenu_pai" IS NOT NULL';
            }

            $data = ItemMenu::model()->findAll('"IDModulo"=' . $_POST['undefined'] . $condicao);
            $data = Html::listData($data, 'IDItem_menu', 'nome_menu');
            echo Html::tag('option', array('value' => ''), Html::encode('Nenhum'), true);
            foreach ($data as $value => $name) {
                echo Html::tag('option', array('value' => $value), Html::encode($name), true);
            }
        }
    }

}
