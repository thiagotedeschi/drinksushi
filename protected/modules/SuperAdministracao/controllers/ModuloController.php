<?php


class ModuloController extends Controller
{

    public $acoesActions = array(
        'create' => '1',
        'search' => '2',
        'view' => '3',
        'update' => '5',
        'delete' => '4',
    );
    public $defaultAction = 'search';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Visualizar ' . Modulo::model()->labelModel();
        $model = $this->loadModel($id);
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Modulo::model()->labelModel();
        $model = new Modulo;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Modulo'])) {
            $model->attributes = $_POST['Modulo'];
            if ($model->save()) {
                $projeto = new Projeto;
                $projeto = Projeto::model()->findByAttributes(array('slug_projeto' => 'drink'));
                if (!is_null($projeto)) {
                    $modProjeto = new ProjetosTemModulos;

                    $modProjeto->IDModulo = $model->IDModulo;
                    $modProjeto->IDProjeto = $projeto->IDProjeto;
                    $modProjeto->dt_inicio = HData::hoje();
                    $modProjeto->dt_fim = null;

                    $modProjeto->save();
                }


                $this->redirect(array('search', 'id' => $model->IDModulo));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Modulo::model()->labelModel();
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Modulo'])) {

            $model->attributes = $_POST['Modulo'];
            if ($_POST['Modulo']['eh_basicoModulo'] == '1') {
                $model->eh_basicoModulo = true;
            } else {
                $model->eh_basicoModulo = false;
            }
            if ($model->save()) {
                $this->redirect(array('search', 'id' => $model->IDModulo));

            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->pageTitle = 'Criar ' . Modulo::model()->labelModel();
        $this->render('index');
    }

    /**
     * Manages all models.
     */
    public function actionSearch()
    {
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
        }
        $this->pageTitle = 'Buscar ' . Modulo::model()->labelModel();
        $model = new Modulo('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Modulo'])) {
            $model->attributes = $_GET['Modulo'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Modulo the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Modulo::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Modulo $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'modulo-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
