<?php


class AcaoController extends Controller
{

    public $acoesActions = array(
        'create' => '13',
        'search' => '14',
        'view' => '35',
        'update' => '16',
        'delete' => '15',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $actionDefault = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar Ação #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Acao::model()->labelModel();
        $model = new Acao;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Acao'])) {
            $model->attributes = $_POST['Acao'];
            if ($model->save()) {
                $this->redirect(array('search', 'id' => $model->IDAcao));
            }
        }
        $this->render('create', array('model' => $model));
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Acao::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['Acao'])) {
            $model->attributes = $_POST['Acao'];

            if ($model->save()) {
                unset(Yii::app()->session['acoes']);
                unset(YII::app()->session['menu']);
                $this->getMenu();

                $this->redirect(array('search', 'id' => $model->IDAcao));
            }
        }
        $this->render('update', array('model' => $model));
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
        }
        $this->pageTitle = 'Buscar ' . Acao::model()->labelModel();
        $model = new Acao('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Acao'])) {
            $model->attributes = $_GET['Acao'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Acao the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Acao::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Acao $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'acao-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
