<?php
/* @var $this ItemMenuController */
/* @var $data ItemMenu */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDItem_menu')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDItem_menu), array('view', 'id' => $data->IDItem_menu)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_menu')); ?>:</b>
    <?php echo Html::encode($data->nome_menu); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('link_menu')); ?>:</b>
    <?php echo Html::encode($data->link_menu); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('n_ordemItemMenu')); ?>:</b>
    <?php echo Html::encode($data->n_ordemItemMenu); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDMenu_pai')); ?>:</b>
    <?php echo Html::encode($data->IDMenu_pai); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDModulo')); ?>:</b>
    <?php echo Html::encode($data->IDModulo); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('icone_itemMenu')); ?>:</b>
    <?php echo Html::encode($data->icone_itemMenu); ?>
    <br/>


</div>