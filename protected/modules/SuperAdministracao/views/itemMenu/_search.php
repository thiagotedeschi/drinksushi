<?php
/* @var $this ItemMenuController */
/* @var $model ItemMenu */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDItem_menu'); ?>
        <?php echo $form->textField($model, 'IDItem_menu'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_menu'); ?>
        <?php echo $form->textField($model, 'nome_menu', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'link_menu'); ?>
        <?php echo $form->textField($model, 'link_menu', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'n_ordemItemMenu'); ?>
        <?php echo $form->textField($model, 'n_ordemItemMenu'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDMenu_pai'); ?>
        <?php echo $form->textField($model, 'IDMenu_pai'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDModulo'); ?>
        <?php echo $form->textField($model, 'IDModulo'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'icone_itemMenu'); ?>
        <?php echo $form->textField($model, 'icone_itemMenu', array('size' => 20, 'maxlength' => 20)); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->