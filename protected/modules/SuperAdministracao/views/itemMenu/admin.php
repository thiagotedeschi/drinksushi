<?php

/* @var $this ItemMenuController */
/* @var $model ItemMenu */

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php

$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'item-menu-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            array(
                'name' => 'nome_menu',
                'id' => 'nome_menu',
                'value' => '($data->getLink())',
                'type' => 'raw'
            ),
            array(
                'name' => 'IDMenu_pai',
                'id' => 'IDMenu_pai',
                'value' => '($data->IDMenuPai() != null) ? $data->IDMenuPai()->nome_menu : "-"',
                'filter' => Html::listData(
                        ItemMenu::model()->findAll('"IDMenu_pai" IS NULL'),
                        'IDItem_menu',
                        'nome_menu'
                    )
            ),
            array(
                'name' => 'IDModulo',
                'id' => 'IDModulo',
                'header' => 'Módulo',
                'value' => '$data->IDModulo()->nome_modulo',
                'filter' => Html::listData(Modulo::model()->findAll(), 'IDModulo', 'nome_modulo')
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
