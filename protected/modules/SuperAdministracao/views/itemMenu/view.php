<?php

/* @var $this ItemMenuController */
/* @var $model ItemMenu */

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDItem_menu',
            'nome_menu',
            'link_menu',
            'IDMenu_pai' => array(
                'label' => $model->attributeLabels()['IDMenu_pai'],
                'type' => 'raw',
                'value' => ($model->IDMenu_pai != '') ? $model->IDMenuPai->nome_menu : null
            ),
            'IDModulo' => array(
                'label' => $model->attributeLabels()['IDModulo'],
                'type' => 'raw',
                'value' => $model->IDModulo()->nome_modulo
            ),
            'icone_itemMenu' => array(
                'name' => 'Ícone',
                'type' => 'raw',
                'value' => Html::decode('<i class="icon-' . $model->icone_itemMenu . ' large"></i>')
            ),
            'tagItemMenus' => array(
                'name' => $model->attributeLabels()['tags'],
                'type' => 'raw',
                'value' => Html::activeList($model, 'tagItemMenus', 'valor_tagItemMenu')
            )
        ),
    )
);
?>
