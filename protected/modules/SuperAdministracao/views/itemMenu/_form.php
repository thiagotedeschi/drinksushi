<?php
/* @var $this ItemMenuController */
/* @var $model ItemMenu */
/* @var $form CActiveForm */


Yii::app()->clientScript->registerScript(
    'search',
    "
       $(document).ready(function(){
       $.ajax({
           url : '" . CController::createUrl('itemMenu/itemsDinamicos', array('tipo' => 'comfilhos')) . "',
        type: 'post',
        success: function(text){
            jQuery('#'+'" . Html::activeId($model, 'IDMenu_pai') . "').html(text)
            selected = jQuery(\"option[value=" . $model->IDMenu_pai . "]\", \"#" . Html::activeId(
        $model,
        'IDMenu_pai'
    ) . "\").attr('selected','selected')
            },
        data: jQuery(\"option:selected\", \"#" . Html::activeId($model, 'IDModulo') . "\"),
        })
    });"
);
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'item-menu-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_menu', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'nome_menu', array('class' => 'span6 m-wrap', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'nome_menu', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'link_menu', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'link_menu', array('class' => 'span6 m-wrap', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'link_menu', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <!--	<div class="">
    <?php //echo $form->labelEx($model,'n_ordemItemMenu');    ?>
    <?php //echo $form->textField($model,'n_ordemItemMenu');    ?>
    <?php //echo $form->error($model,'n_ordemItemMenu');  ?>
    </div>-->
    <div class="control-group ">
        <?php echo $form->labelEx($model, 'IDModulo', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            echo $form->dropDownList(
                $model,
                'IDModulo',
                Html::listData($mod = Modulo::model()->findAll(), 'IDModulo', 'nome_modulo'),
                array(
                    'class' => 'm-wrap select2',
                    'ajax' => array(
                        'type' => 'POST',
                        //request type
                        'url' => CController::createUrl('itemMenu/itemsDinamicos', array('tipo' => 'comfilhos')),
                        //url to call.
                        'update' => 'select#' . Html::activeId($model, 'IDMenu_pai'),
                        //selector to update
                        'data' => 'js:jQuery(\'option:selected\', this)',
                    )
                )
            );
            ?>
            <?php echo $form->error($model, 'IDModulo', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDMenu_pai', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDMenu_pai',
                Html::listData(array(), 'IDItem_menu', 'nome_menu'),
                array('class' => 'select2')
            ); ?>
            <?php echo $form->error($model, 'IDMenu_pai', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'n_ordemItemMenu', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->numberField($model, 'n_ordemItemMenu', array('class' => 'm-wrap span1')); ?>
            <?php echo $form->error($model, 'n_ordemItemMenu', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'tags', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'tags', array('class' => 'm-wrap span2')); ?>
            <?php echo $form->error($model, 'tags', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php $this->widget(
            'application.widgets.WIcones',
            array('model' => $model, 'form' => $form, 'field' => 'icone_itemMenu')
        ); ?>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<script>
    $(document).ready(function () {
        $('#<?= Html::activeId($model, 'tags') ?>').tagsInput({
            width: '45%',
            defaultText: '+ Tag'
        });
    });
</script>
<?php $this->endWidget(); ?>
