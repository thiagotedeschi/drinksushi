<?php
/* @var $this AcaoController */
/* @var $model Acao */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDAcao'); ?>
        <?php echo $form->textField($model, 'IDAcao'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_acao'); ?>
        <?php echo $form->textField($model, 'nome_acao', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_acao'); ?>
        <?php echo $form->textField($model, 'desc_acao', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDItem_menu'); ?>
        <?php echo $form->textField($model, 'IDItem_menu'); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->