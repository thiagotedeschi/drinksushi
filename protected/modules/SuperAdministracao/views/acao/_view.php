<?php
/* @var $this AcaoController */
/* @var $data Acao */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDAcao')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDAcao), array('view', 'id' => $data->IDAcao)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_acao')); ?>:</b>
    <?php echo Html::encode($data->nome_acao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_acao')); ?>:</b>
    <?php echo Html::encode($data->desc_acao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDItem_menu')); ?>:</b>
    <?php echo Html::encode($data->IDItem_menu); ?>
    <br/>


</div>