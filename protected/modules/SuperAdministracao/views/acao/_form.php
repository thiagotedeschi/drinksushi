<?php
/* @var $this AcaoController */
/* @var $model Acao */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'acao-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_acao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'nome_acao', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'nome_acao', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_acao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea($model, 'desc_acao', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'desc_acao', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx(new Modulo(), 'Módulo', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            echo $form->dropDownList(
                new Modulo(),
                'IDModulo',
                Html::listData($mod = Modulo::model()->findAll(), 'IDModulo', 'nome_modulo'),
                array(
                    'ajax' => array(
                        'type' => 'POST', //request type
                        'url' => CController::createUrl('itemMenu/itemsDinamicos', array('tipo' => '')), //url to call.
                        'update' => '#' . Html::activeId($model, 'IDItem_menu'), //selector to update
                        'data' => 'js:jQuery(\'option:selected\', this)',
                    ),
                    'class' => 'm-wrap select2'
                )
            );
            ?>
            <?php echo $form->error($model, 'IDModulo', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDItem_menu', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            echo $form->dropDownList(
                $model,
                'IDItem_menu',
                Html::listData(ItemMenu::model()->findAll(), 'IDItem_menu', 'nome_menu'),
                array(
                    'class' => 'm-wrap select2'
                )
            );
            ?>
            <?php echo $form->error($model, 'IDItem_menu', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>


<?php $this->endWidget(); ?>
