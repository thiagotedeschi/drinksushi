<?php

/* @var $this AcaoController */
/* @var $model Acao */
$sPermissoes = '';
$permissoes = $model->with('iDGrupo')->IDPermissoes();
foreach ($permissoes as $permissao) {
    $sPermissoes .= $permissao->iDGrupo->nome_grupo . ", ";
}

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDAcao',
            'nome_acao',
            'desc_acao',
            'IDItem_menu' => array(
                'name' => 'Item de Menu',
                //'type' => 'raw',
                'value' => $model->iDItemMenu()->nome_menu
            ),
            'Grupos Associados' => array(
                'name' => 'Grupos Associados',
                //'type' => 'raw',
                'value' => $sPermissoes
            ),
        ),
    )
);
?>
