<?php
/* @var $this AcaoController */
/* @var $model Acao */

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

    <p class="note note-warning">
        Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>
            &lt;&gt;</b>
        ) No início de cada pesquisa.
    </p>


<?php
$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'acao-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDAcao',
            'nome_acao',
            'desc_acao',
            array(
                'name' => 'IDItem_menu',
                'id' => 'IDItem_menu',
                'value' => '@$data->iDItemMenu()->nome_menu',
                'filter' => Html::listData(ItemMenu::model()->findAll(), 'IDItem_menu', 'nome_menu')
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false'),
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);