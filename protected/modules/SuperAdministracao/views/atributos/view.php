<?php
/* @var $this AtributosController */
/* @var $model AtributoGlobal */

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDAtributo',
            'atributo',
            'valor_atributo',
        ),
    )
); ?>
