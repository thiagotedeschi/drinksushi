<?php
/* @var $this AtributosController */
/* @var $data AtributoGlobal */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDAtributo')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDAtributo), array('view', 'id' => $data->IDAtributo)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('atributo')); ?>:</b>
    <?php echo Html::encode($data->atributo); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('valor_atributo')); ?>:</b>
    <?php echo Html::encode($data->valor_atributo); ?>
    <br/>


</div>