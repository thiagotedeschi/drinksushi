<?php
/* @var $this AtributosController */
/* @var $model AtributoGlobal */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDAtributo'); ?>
        <?php echo $form->textField($model, 'IDAtributo'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'atributo'); ?>
        <?php echo $form->textField($model, 'atributo', array('size' => 60, 'maxlength' => 100)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'valor_atributo'); ?>
        <?php echo $form->textField($model, 'valor_atributo', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->