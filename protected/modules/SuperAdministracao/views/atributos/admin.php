<?php
/* @var $this AtributosController */
/* @var $model AtributoGlobal */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

Yii::app()->clientScript->registerScript(
    'search',
    "
   $('.search-button').click(function(){
       $('.search-form').toggle();
       return false;
   });
   $('.search-form form').submit(function(){
       $('#atributo-global-grid').yiiGridView('update', {
           data: $(this).serialize()
       });
       return false;
   });
   "
);
?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>

<div class="search-form" style="display:none">
    <?php
    $this->renderPartial(
        '_search',
        array(
            'model' => $model,
        )
    );
    ?>
</div><!-- search-form -->

<?php
$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'atributo-global-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'addInModal' => true,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDAtributo',
            'atributo',
            'valor_atributo',
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false'),
                            'isModal' => true
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false'),
                            'isModal' => true
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
