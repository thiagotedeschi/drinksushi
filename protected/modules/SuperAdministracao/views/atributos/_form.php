<?php
/* @var $this AtributosController */
/* @var $model AtributoGlobal */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'atributo-global-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>

<div class='form clearfix positionRelative ' style="">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'atributo', array('class' => 'control-label')); ?>
        <div class='controls'>
            <?php echo $form->textField($model, 'atributo', array('class' => 'span6 m-wrap', 'maxlength' => 100)); ?>
            <?php echo $form->error($model, 'atributo', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'valor_atributo', array('class' => 'control-label')); ?>
        <div class='controls'>
            <?php echo $form->textField(
                $model,
                'valor_atributo',
                array('class' => 'span6 m-wrap', 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'valor_atributo', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>
<div class="form-actions " style=''>
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>


<?php $this->endWidget(); ?>
