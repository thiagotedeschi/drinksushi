<?php
/* @var $this ModuloController */
/* @var $data Modulo */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDModulo')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDModulo), array('view', 'id' => $data->IDModulo)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_modulo')); ?>:</b>
    <?php echo Html::encode($data->nome_modulo); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_modulo')); ?>:</b>
    <?php echo Html::encode($data->desc_modulo); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('slug_modulo')); ?>:</b>
    <?php echo Html::encode($data->slug_modulo); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('eh_basicoModulo')); ?>:</b>
    <?php echo Html::encode($data->eh_basicoModulo); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('n_ordemModulo')); ?>:</b>
    <?php echo Html::encode($data->n_ordemModulo); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDTipo_modulo')); ?>:</b>
    <?php echo Html::encode($data->IDTipo_modulo); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('icone_modulo')); ?>:</b>
	<?php echo Html::encode($data->icone_modulo); ?>
	<br />

	*/
    ?>

</div>