<script>
    //    // temporary fix for the bug of supporting live change in IE
    //    $(inputSelector).live($.browser.msie ? 'click keyup' : 'change', function() {
    //        if ($('#submit_filter').is(':checked')) {
    ////  var data = $.param($(inputSelector))+'&'+settings.ajaxVar+'='+id;
    //            var data = $('.filters :input').serialize();
    //            $.fn.yiiGridView.update(id, {data: data});
    //        }
    //    });
</script>
<?php
/* @var $this ModuloController */
/* @var $model Modulo */
//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

Yii::app()->clientScript->registerScript(
    'search',
    "
   $('.search-button').click(function(){
       $('.search-form').toggle();
       return false;
   });
   $('.search-form form').submit(function(){
       $('#modulo-grid').yiiGridView('update', {
           data: $(this).serialize()
       });
       return false;
   });
   "
);
?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php
$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'modulo-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDModulo',
            'nome_modulo',
//        'desc_modulo',
//        'slug_modulo',
            array(
                'name' => 'buscar_ehbasicoModulo',
                'id' => 'buscar_ehbasicoModulo',
                'value' => '$data->eh_basicoModulo ? "Sim" : "Não"',
                'filter' => array(1 => 'Sim', 0 => 'Não')
            ),
            array(
                'name' => 'IDTipo_modulo',
                'id' => 'IDTipo_modulo',
                'value' => '$data->IDTipoModulo()->nome_tipoModulo',
                //    'filter' => Html::dropDownList('Modulo[IDTipo_modulo]', '', Html::listData(TipoModulo::model()->findAll(), 'IDTipo_modulo', 'nome_tipoModulo'), array('multiple' => 'true', 'size' => 3)),
                'filter' => Html::listData(TipoModulo::model()->findAll(), 'IDTipo_modulo', 'nome_tipoModulo')
            ),
//        array('name' => 'icone_modulo',
//            'value' => '"<i class=\'icon-$data->icone_modulo\'></i>"',
//            'type' => 'raw'
//        ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
