<?php
/* @var $this ModuloController */
/* @var $model Modulo */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'modulo-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>


<div class='form clearfix positionRelative'>
    <p class="note note-warning">Campos com <b> * </b> são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_modulo', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'nome_modulo', array('maxlength' => 30, 'class' => 'span6 m-wrap')); ?>
            <?php echo $form->error($model, 'nome_modulo', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_modulo', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'desc_modulo',
                array('rows' => '5', 'cols' => 50, 'class' => 'span6 m-wrap')
            ); ?>
            <?php echo $form->error($model, 'desc_modulo', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'slug_modulo', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'slug_modulo', array('maxlength' => 20, 'class' => 'span6 m-wrap')); ?>
            <?php echo $form->error($model, 'slug_modulo', array('class' => 'help-inline')); ?>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'eh_basicoModulo', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->checkBox($model, 'eh_basicoModulo', array('class' => 'toggle')); ?>Sim, será um módulo
                básico.
                <?php echo $form->error($model, 'eh_basicoModulo'); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDTipo_modulo', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'IDTipo_modulo',
                    Html::listData(TipoModulo::model()->findAll(), 'IDTipo_modulo', 'nome_tipoModulo'),
                    array('class' => 'm-wrap select2')
                ); ?>
                <?php echo $form->error($model, 'IDTipo_modulo', array('class' => 'help-inline')); ?>
            </div>

            <div class="control-group">
                <?php echo $form->labelEx($model, 'n_ordemModulo', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->numberField($model, 'n_ordemModulo', array('class' => 'm-wrap span1')); ?>
                    <?php echo $form->error($model, 'n_ordemModulo', array('class' => 'help-inline')); ?>
                </div>
            </div>

        </div>
        <div class="control-group">
            <?php $this->widget(
                'application.widgets.WIcones',
                array('model' => $model, 'form' => $form, 'field' => 'icone_modulo')
            ); ?>
        </div>
    </div>

    <div class="form-actions">
        <?php echo Html::submitButton(
            $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
            array('class' => 'botao')
        ); ?>
    </div>

    <?php $this->endWidget(); ?>
