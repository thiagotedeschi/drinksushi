<?php
/* @var $this ModuloController */
/* @var $model Modulo */

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

<?php
$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDModulo',
            'nome_modulo',
            'desc_modulo',
            'slug_modulo',
            'eh_basicoModulo' => array(
                'name' => 'Módulo Básico?',
                'value' => ($model->eh_basicoModulo ? 'Sim' : 'Não')
            ),
            'IDTipo_modulo' => array('name' => 'Super Módulo', 'value' => $model->IDTipoModulo->nome_tipoModulo),
            'icone_modulo' => array(
                'name' => 'Ícone',
                'type' => 'raw',
                'value' => Html::decode('<i class="icon-' . $model->icone_modulo . ' large"></i>')
            ),
        ),
    )
);
?>
