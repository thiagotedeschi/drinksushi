<?php

/**
 * @package base.Empregador
 */
class GrupoEconomicoController extends Controller
{

    public $acoesActions = array(
        'create' => '234',
        'search' => '200',
        'view' => '199',
        'update' => '201',
        'delete' => '202',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . GrupoEconomico::model()->labelModel();
        $model = new GrupoEconomicoEmpregador;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['GrupoEconomicoEmpregador'])) {
            $model->attributes = $_POST['GrupoEconomicoEmpregador'];
            if ($model->save()) {

                $this->redirect(array('search', 'id' => $model->IDGrupo_economico));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . GrupoEconomico::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['GrupoEconomicoEmpregador'])) {
            $model->attributes = $_POST['GrupoEconomicoEmpregador'];
            if ($model->save()) {

                $mensagem[] = 'Grupo Econômico alterado! <b>' . $model->labelGrupoEconomico . '</b> sob responsabilidade de <b>' . $model->iDResponsavelGrupoEconomico->labelEmpregador . '</b>';
                Yii::app()->user->setFlash('warning', $mensagem);
                $this->redirect(array('search', 'id' => $model->IDGrupo_economico));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . GrupoEconomico::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new GrupoEconomicoEmpregador('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['GrupoEconomicoEmpregador'])) {
            $model->attributes = $_GET['GrupoEconomicoEmpregador'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return GrupoEconomicoEmpregador the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = GrupoEconomicoEmpregador::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param GrupoEconomicoEmpregador $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'grupo-economico-empregador-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
