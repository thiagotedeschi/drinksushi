<?php

/**
 * @package base.Empregador
 */
class ContatoController extends Controller
{

    public $acoesActions = array(
        'create' => '193',
        'search' => '197',
        'view' => '194',
        'update' => '195',
        'delete' => '196',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Contato::model()->labelModel();
        $model = new Contato;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Contato'])) {
            $model->attributes = $_POST['Contato'];
            if ($model->save()) {
                $this->saveResponsabilidades($_POST['Responsabilidades'], $model, true);
                $this->saveEmails($_POST['Emails'], $model, true);
                $this->saveTelefones($_POST['Telefones'], $model, true);

                $this->redirect(array('search', 'id' => $model->IDContato));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Contato::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['Contato'])) {
            $model->attributes = $_POST['Contato'];
            if ($model->save()) {
                $this->saveResponsabilidades($_POST['Responsabilidades'], $model, false);
                $this->saveEmails($_POST['Emails'], $model, false);
                $this->saveTelefones($_POST['Telefones'], $model, false);

                $this->redirect(array('search', 'id' => $model->IDContato));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Contato::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Contato('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Contato'])) {
            $model->attributes = $_GET['Contato'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Contato the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Contato::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Contato $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'contato-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function saveResponsabilidades($IDResps, $contato, $newRecord = false)
    {
        if ($newRecord) {
            if (isset($IDResps)) {
                foreach ($IDResps as $IDResp) {
                    $r = new ContatosTemResponsabilidades;
                    $r->IDContato_Contato = $contato->IDContato;
                    $r->IDResponsabilidade_contato = $IDResp;
                    $r->save();
                }
            }
        } else {
            $respsA = $contato->ResponsabilidadeContatos();
            $respsAntigas = array();
            foreach ($respsA as $resps) {
                $respsAntigas[] = $resps->IDResponsabilidade_contato;
            }

            //pega os ids dos dps grupos que não estarão mais associados ao usuário
            $excluirResps = array_diff($respsAntigas, $IDResps);
            //os ids a serem acresentados eliminandos os já existentes
            $addResps = array_diff($IDResps, $respsAntigas);
            foreach ($excluirResps as $excluir) {
                $r = ContatosTemResponsabilidades::model()->findByAttributes(
                    array("IDResponsabilidade_contato" => $excluir, 'IDContato_Contato' => $contato->IDContato)
                );
                if ($r != null) {
                    $r->delete();
                }
            }
            foreach ($addResps as $add) {
                $a = new ContatosTemResponsabilidades();
                $a->IDContato_Contato = $contato->IDContato;
                $a->IDResponsabilidade_contato = $add;
                $a->save();
            }
        }
    }

    public function saveTelefones($telefones, $contato, $newRecord = false)
    {
        if ($newRecord) {
            if (isset($telefones)) {
                if (isset($telefones['principal'])) {
                    $principal = new TelefoneContato;
                    $principal->ddd_telefoneContato = $telefones['principal']['ddd'];
                    $principal->numero_telefoneContato = $telefones['principal']['numero'];
                    $principal->ramal_telefoneContato = $telefones['principal']['ramal'];
                    $principal->principal_telefoneContato = true;
                    $principal->tipo_telefoneContato = $telefones['principal']['tipo'];
                    $principal->IDContato = $contato->IDContato;
                    $principal->save();
                }
                if (isset($telefones['secundario'])) {
                    foreach ($telefones['secundario'] as $telefone) {
                        $t = new TelefoneContato;
                        $t->ddd_telefoneContato = $telefone['ddd'];
                        $t->numero_telefoneContato = $telefone['numero'];
                        $t->ramal_telefoneContato = $telefone['ramal'];
                        $t->principal_telefoneContato = 0;
                        $t->tipo_telefoneContato = $telefone['tipo'];
                        $t->IDContato = $contato->IDContato;
                        $t->save();
                    }
                }
            }
        } else {
            $tels = $contato->telefoneContatos();
            foreach ($tels as $tel) {
                $tel->delete();
            }
            $this->saveTelefones($telefones, $contato, true);
        }
    }

    public function saveEmails($emails, $contato, $newRecord = false)
    {
        if ($newRecord) {
            if (isset($emails)) {
                if (isset($emails['principal'])) {
                    $principal = new EmailContato;
                    $principal->email_contato = $emails['principal'];
                    $principal->principal_emailContato = true;
                    $principal->IDContato = $contato->IDContato;
                    $principal->save();
                }
                if (isset($emails['secundarios'])) {
                    $emailsList = explode(',', $emails['secundarios']);
//                    var_dump($emailsList);
//                    die;
                    foreach ($emailsList as $email) {
                        if ($email != null) {
                            $e = new EmailContato;
                            $e->email_contato = $email;
                            $e->principal_emailContato = 0;
                            $e->IDContato = $contato->IDContato;
                            $e->save();
                        }
                    }
                }
            }
        } else {
            $emailsA = $contato->emailContatos();
            foreach ($emailsA as $email) {
                $email->delete();
            }
            $this->saveEmails($emails, $contato, true);
        }
    }

}
