<?php

/**
 * @package base.Empregador
 */
class EmpregadorController extends Controller
{

    public $acoesActions = array(
        'create' => '188',
        'search' => '189',
        'view' => '192',
        'update' => '190',
        'delete' => '191',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Empregador::model()->labelModel();
        $empregador = new Empregador;
        $enderecoC = new EnderecoEmpregador;
        $enderecoF = new EnderecoEmpregador;
        $responsavel = new ResponsavelLegal;

        // Valida os 3 models
        $this->performAjaxValidation($empregador, $enderecoC, $enderecoF);

        if (isset($_POST['Empregador'], $_POST['EnderecoEmpregador'][1])) {
            $responsavel = array();
            $empregador->attributes = $_POST['Empregador']; //preenche o empregador
            $enderecoC->attributes = $_POST['EnderecoEmpregador'][1]; //preenche o endereco principal (obrigatório)

            if ($enderecoC->save()) { //se inserir o endereco certinho
                $empregador->IDEndereco_correspondencia = $enderecoC->IDEndereco;
            }

            if (isset($_POST['EnderecoEmpregador'][2])) { //se o usuário tiver optado por um endereço fiscal diferente
                $enderecoF = new EnderecoEmpregador();
                $enderecoF->attributes = $_POST['EnderecoEmpregador'][2]; //preenche o novo endereco
                if ($valid = $enderecoF->save()) { //salva o novo enderecso
                    $empregador->IDEndereco_fiscal = $enderecoF->IDEndereco;
                }
            } else {
                //se ele não tiver preenchido o endereco fiscal
                //guarda na posição referente ao End. Fiscal
                //o já cadastrado endereco de correspondencia
                $empregador->IDEndereco_fiscal = $enderecoC->IDEndereco;
            }

            $error = false;
            //salvar responsaveis legais

            foreach ($_POST['ResponsavelLegal'] as $responsavelLegal) {
                $resp = new ResponsavelLegal();
                $resp->attributes = $responsavelLegal;
                if (!$resp->validate()) {
                    $error = true;
                }
                $responsavel[] = $resp;
            }

            if (!$empregador->validate()) {
                $error = true;
            }

            if (!$error) {
                if ($empregador->save(false)) { //se der tudo certo
                    //salva responsaveis legais
                    foreach ($responsavel as $resp) {
                        $resp->IDEmpregador = $empregador->IDEmpregador;
                        $resp->save(false);
                    }

                    if (!Yii::app()->user->todas_empresas) {
                        Yii::app()->session['empregadores'] = Profissional::model()->findByPk(
                            Yii::app()->session['IDProfissional']
                        )->getLabelEmpregadores();
                    } else {
                        Yii::app()->session['empregadores'] = Empregador::model()->getLabelEmpregadores();
                    }
                    $this->saveCnaes($_POST['Cnaes'], $empregador, true);

                    $this->redirect(array('view', 'id' => $empregador->IDEmpregador));
                }
            }
        }
        $this->render(
            'create',
            array(
                'empregador' => $empregador,
                'enderecoC' => $enderecoC,
                'enderecoF' => $enderecoF,
                'responsavel' => $responsavel,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Empregador::model()->labelModel() . ' #' . $id;
        $empregador = $this->loadModel($id); //carrega o empregador
        $enderecoC = $empregador->iDEnderecoCorrespondencia; //carrega o end. Corresp
        $enderecoF = $empregador->iDEnderecoFiscal; //carrega o End. Fiscal
        //carrega os responsáveis legais
        $responsaveis = ResponsavelLegal::model()->orderByID()->findAllByAttributes(
            ['IDEmpregador' => $empregador->IDEmpregador]
        );

        if (empty($responsaveis)) {
            $responsaveis = array(new ResponsavelLegal());
        }

        $this->performAjaxValidation($empregador, $enderecoC, $enderecoF); // Valida os 3 models
        $excluirEndFiscal = false;

        if (isset($_POST['Empregador'], $_POST['EnderecoEmpregador'][1])) {
            $valid = false;
            $empregador->attributes = $_POST['Empregador'];
            $enderecoC->attributes = $_POST['EnderecoEmpregador'][1];
            //se estiver setado o endereco 2, referente ao fiscal
            if (isset($_POST['EnderecoEmpregador'][2])) {
                $enderecoF = new EnderecoEmpregador();
                //atribui os dados ao model enderecoF
                $enderecoF->attributes = $_POST['EnderecoEmpregador'][2];
                //se salvar direitinho
                if ($valid = $enderecoF->save()) {
                    /*
                     * Duas possibilidades: ou o End Fiscal já existia e o ID é sobreescrevido por ele mesmo
                     * ou caso ele fosse o mesmo que o end de Corresp, agora não é mais
                     */
                    $empregador->IDEndereco_fiscal = $enderecoF->IDEndereco;
                }
                //se não tiver End 2 setado
            } else {
                $valid = true;
                $empregador->IDEndereco_fiscal = $enderecoC->IDEndereco; //End F fica sendo o mesmo que o End C
                if ($enderecoF->IDEndereco != '' && $enderecoF->IDEndereco != $enderecoC->IDEndereco) //Se existia end F que era diferente do end C
                {
                    $excluirEndFiscal = true;
                } //Exclui ele
            }

            $valid = $empregador->validate() && $valid;
            $valid = $enderecoC->validate() && $valid;
            if ($valid) {
                if ($enderecoC->save() && $empregador->save()) {
                    if ($excluirEndFiscal) {
                        $enderecoF->delete();
                    }
                    $this->saveCnaes($_POST['Cnaes'], $empregador);
                    $responsaveis = $empregador->atualizaResp($this, $_POST);
                }
            }
        }
        $this->render(
            'update',
            array(
                'empregador' => $empregador,
                'enderecoC' => $enderecoC,
                'enderecoF' => $enderecoF,
                'responsavel' => $responsaveis,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Empregador::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Empregador('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Empregador'])) {
            $model->attributes = $_GET['Empregador'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Empregador the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Empregador::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Empregador $model the model to be validated
     */
    protected function performAjaxValidation($cliente, $enderecoC, $enderecoF)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'empregador-form') {
            if (isset($_POST['EnderecoEmpregador'][2])) {
                echo CActiveForm::validate(array($enderecoC, $cliente, $enderecoF));
                Yii::app()->end();
            } else {
                echo CActiveForm::validate(array($cliente, $enderecoC));
                Yii::app()->end();
            }
        }
    }

    public function saveCnaes($cnaes, $empregador, $newRecord = false)
    {
        if ($newRecord) {
            if (isset($cnaes['primario'])) {
                $principal = new EmpregadorTemCnaes();
                $principal->IDCnae = $cnaes['primario'];
                $principal->IDEmpregador = $empregador->IDEmpregador;
                $principal->principal_CNAE = true;
                $principal->save();
            }
            if (isset($cnaes['secundario'])) {
                foreach ($cnaes['secundario'] as $IDCnae) {
                    $secundario = new EmpregadorTemCnaes();
                    $secundario->IDCnae = $IDCnae;
                    $secundario->IDEmpregador = $empregador->IDEmpregador;
                    $secundario->principal_CNAE = false;
                    $secundario->save();
                }
            }
        } else {
            $cnaesA = $empregador->getCnaesSecundarios();
            $cnaesAntigos = array();
            foreach ($cnaesA as $cnae) {
                $cnaesAntigos[] = $cnae->IDCnae;
            }

            if (isset($cnaes['secundario'])) {
                $cnaesAtuais = $cnaes['secundario'];
            } else {
                $cnaesAtuais = array();
            }
            //pega os ids dos dps grupos que não estarão mais associados ao usuário
            $excluirCnaes = array_diff($cnaesAntigos, $cnaesAtuais);
            //os ids a serem acresentados eliminandos os já existentes
            $addCnaes = array_diff($cnaesAtuais, $cnaesAntigos);

            foreach ($excluirCnaes as $IDCnae) {
                //$cnae = EmpregadorTemCnaes::model()->findByAttributes(array('IDCnae' => $IDCnae, 'IDEmpregador' => $empregador->IDEmpregador, 'principal_CNAE' => false));
                $cnae = $empregador->empregadorTemCnaes(
                    array('condition' => '"IDCnae" = ' . $IDCnae . ' AND NOT "principal_CNAE"')
                );
                if ($cnae != null) {
                    $cnae[0]->delete();
                }
            }
            $primario = array_pop(
                $empregador->empregadorTemCnaes(array('condition' => '"principal_CNAE"', 'limit' => '1'))
            );
            if ($primario == null) {
                $primario = new EmpregadorTemCnaes();
                $primario->IDEmpregador = $empregador->IDEmpregador;
            }
            $primario->IDCnae = $cnaes['primario'];
            $primario->principal_CNAE = true;
            $primario->save();


            $this->saveCnaes(array('secundario' => $addCnaes), $empregador, true);
        }
    }

}
