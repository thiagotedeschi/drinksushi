<?php

/* @var $this EmpregadorController */
/* @var $model Empregador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php
$dados = array(
    'data' => $model,
    'attributes' => array(
        'IDEmpregador',
        'nome_empregador',
        'razao_empregador',
        'labelTipoPessoa:text:Tipo de Pessoa',
        'DocumentoEmpregador',
        'email_empregador',
        'site_empregador',
        'NFEgrupo_empregador:boolean',
        'optante_simplesEmpregador:boolean',
        'tel_empregador',
        'ddd_telEmpregador',
        'ins_estadualEmpregador',
        'ins_municipalEmpregador',
        'iDEnderecoCorrespondencia:raw:Endereço Correspondência',
        'iDEnderecoFiscal:raw:Endereço Fiscal',
        'IDGrupo_economico' => array(
            'label' => $model->attributeLabels()['IDGrupo_economico'],
            'value' => is_null($model->iDGrupoEconomico) ? null : $model->iDGrupoEconomico->nome_grupoEconomico
        ),
        array(
            'label' => 'CNAE Principal',
            'value' => $model->getCnaePrincipal()
        ),
        array(
            'label' => 'CNAEs Secundários',
            'value' => Html::activeList($model, 'cnaesSecundarios', 'labelCnae'),
            'type' => 'raw'
        ),
        'iDClassificacaoTributaria.desc_classificacao',
        'CNO_empregador',
        'CAEPF_empregador'

    )
);
$i = 0;
$responsaveis = $model->responsavelLegals;
foreach ($responsaveis as $responsavel) {
    $i++;
    $dadosResp = array('label' => "Responsável #$i", 'value' => $responsavel, 'type' => 'raw');
    $dados['attributes'][] = $dadosResp;
}

//contatos na view
$contatos = Html::activeList($model, 'contatos', 'labelContato');
$dadosContatos = array('label' => 'Contatos', 'value' => $contatos, 'type' => 'raw');
$dados['attributes'][] = $dadosContatos;

$this->widget('application.widgets.DetailView', $dados);
?>
