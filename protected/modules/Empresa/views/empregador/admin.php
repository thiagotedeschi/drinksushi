<?php

/* @var $this EmpregadorController */
/* @var $model Empregador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>

<?php

$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'empregador-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDEmpregador',
            'nome_empregador',
            'razao_empregador',
            /*
              'tipo_pessoaEmpregador',
              'documento_empregador',
              'email_empregador',
              'site_empregador',
              'NFEgrupo_empregador',
              'tel_empregador',
              'ddd_telEmpregador',
              'ins_estadualEmpregador',
              'ins_municipalEmpregador',
              'IDEndereco_correspondencia',
              'IDEndereco_fiscal',
              'IDGrupo_economico',
             */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false'),
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false'),
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
