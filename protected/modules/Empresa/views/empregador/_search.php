<?php
/* @var $this EmpregadorController */
/* @var $model Empregador */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDEmpregador'); ?>
        <?php echo $form->textField($model, 'IDEmpregador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_empregador'); ?>
        <?php echo $form->textField($model, 'nome_empregador', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'razao_empregador'); ?>
        <?php echo $form->textField(
            $model,
            'razao_empregador',
            array('class' => 'm-wrap span6', 'maxlength' => 255)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'tipo_pessoaEmpregador'); ?>
        <?php echo $form->textField(
            $model,
            'tipo_pessoaEmpregador',
            array('class' => 'm-wrap span6', 'maxlength' => 1)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'documento_empregador'); ?>
        <?php echo $form->textField(
            $model,
            'documento_empregador',
            array('class' => 'm-wrap span6', 'maxlength' => 255)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'email_empregador'); ?>
        <?php echo $form->textField(
            $model,
            'email_empregador',
            array('class' => 'm-wrap span6', 'maxlength' => 255)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'site_empregador'); ?>
        <?php echo $form->textField($model, 'site_empregador', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'NFEgrupo_empregador'); ?>
        <?php echo $form->textField(
            $model,
            'NFEgrupo_empregador',
            array('class' => 'm-wrap span6', 'maxlength' => 255)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'tel_empregador'); ?>
        <?php echo $form->textField($model, 'tel_empregador', array('class' => 'm-wrap span6', 'maxlength' => 60)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ddd_telEmpregador'); ?>
        <?php echo $form->textField($model, 'ddd_telEmpregador', array('class' => 'm-wrap span6', 'maxlength' => 5)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ins_estadualEmpregador'); ?>
        <?php echo $form->textField(
            $model,
            'ins_estadualEmpregador',
            array('class' => 'm-wrap span6', 'maxlength' => 50)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ins_municipalEmpregador'); ?>
        <?php echo $form->textField(
            $model,
            'ins_municipalEmpregador',
            array('class' => 'm-wrap span6', 'maxlength' => 255)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDEndereco_correspondencia'); ?>
        <?php echo $form->textField($model, 'IDEndereco_correspondencia', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDEndereco_fiscal'); ?>
        <?php echo $form->textField($model, 'IDEndereco_fiscal', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDGrupo_economico'); ?>
        <?php echo $form->textField($model, 'IDGrupo_economico', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->