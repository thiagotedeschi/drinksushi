<?php


Yii::app()->clientScript->registerScript(
    'mascara',
    '$("#' . Html::activeId($empregador, 'tipo_pessoaEmpregador') . '").change(function(){
           tipoPessoa = jQuery(this).val();
           if( tipoPessoa == "f"){
               jQuery("#' . Html::activeId($empregador, 'documento_empregador') . '").unmask();
               jQuery("#' . Html::activeId($empregador, 'documento_empregador') . '").mask("' . HTexto::MASK_CPF . '");
           } else{
               jQuery("#' . Html::activeId($empregador, 'documento_empregador') . '").unmask();
               jQuery("#' . Html::activeId($empregador, 'documento_empregador') . '").mask("' . HTexto::MASK_CNPJ . '");
           }

           jQuery("#' . Html::activeId($empregador, 'documento_empregador') . '").removeAttr("disabled");
    })'
);

?>

<div class="tab-pane row-fluid active" id="tab_dadosBasicos">
<div class="control-group">
    <?php echo $form->labelEx($empregador, 'nome_empregador', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $empregador,
            'nome_empregador',
            array('maxlength' => 55, 'class' => 'span6 m-wrap')
        ); ?>
        <?php echo $form->error($empregador, 'nome_empregador', array('class' => 'help-inline')); ?>
    </div>
</div>


<div class="control-group">
    <?php echo $form->labelEx(
        $empregador,
        'tipo_pessoaEmpregador',
        array('class' => 'control-label')
    ); //3 tipos padrão: fisica, juridica e publica. se for edição do registro, o campo fica read-only
    ?>
    <div class="controls">
        <?php echo $form->dropDownList(
            $empregador,
            'tipo_pessoaEmpregador',
            HTexto::$tiposPessoa,
            array('class' => 'select2', 'maxlength' => 1, 'prompt' => 'Selecione o Tipo de Pessoa')
        ); ?>
        <?php echo $form->error($empregador, 'tipo_pessoaEmpregador', array('class' => 'help-inline')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($empregador, 'documento_empregador', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $empregador,
            'documento_empregador',
            array(
                'class' => 'm-wrap span6',
                'maxlength' => 255,
                'disabled' => $empregador->isNewRecord ? 'disabled' : ''
            )
        );
        ?>
        <?php echo $form->error($empregador, 'documento_empregador', array('class' => 'help-inline')); ?>
    </div>
</div>
<div class="cnpj">
    <div class="control-group">
        <?php echo $form->labelEx($empregador, 'razao_empregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $empregador,
                'razao_empregador',
                array('size' => 80, 'maxlength' => 255, 'class' => 'span6 m-wrap')
            ); ?>
            <?php echo $form->error($empregador, 'razao_empregador', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($empregador, 'ins_estadualEmpregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $empregador,
                'ins_estadualEmpregador',
                array('size' => 50, 'maxlength' => 50, 'class' => 'span6 m-wrap')
            ); ?>
            <?php echo $form->error($empregador, 'ins_estadualEmpregador', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($empregador, 'ins_municipalEmpregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $empregador,
                'ins_municipalEmpregador',
                array('size' => 60, 'maxlength' => 255, 'class' => 'span6 m-wrap')
            ); ?>
            <?php echo $form->error($empregador, 'ins_municipalEmpregador', array('class' => 'help-inline')); ?>
        </div>
    </div>


    <div class="control-group">
        <?php echo $form->labelEx($empregador, 'IDGrupo_economico', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $empregador,
                'IDGrupo_economico',
                CHtml::listData(
                    GrupoEconomicoEmpregador::model()->findAll(),
                    'IDGrupo_economico',
                    'nome_grupoEconomico'
                ),
                array('empty' => array('' => 'Nenhum'), 'onchange' => 'habilitaNfeGrupo(this)')
            ); ?>
            <?php echo $form->error($empregador, 'IDGrupo_economico', array('class' => 'help-inline')); ?>
        </div>
    </div>


    <div class="control-group">
        <?php echo $form->labelEx($empregador, 'NFEgrupo_empregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->checkBox(
                $empregador,
                'NFEgrupo_empregador',
                $empregador->isNewRecord || (!$empregador->isNewRecord && $empregador->IDGrupo_economico == '') ? array('disabled' => 'disabled') : array()
            ); ?>
            <?php echo $form->error($empregador, 'NFEgrupo_empregador', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($empregador, 'IDClassificacaoTributaria', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $empregador,
                'IDClassificacaoTributaria',
                Html::listData(
                    ClassificacaoTributaria::model()->findAll(),
                    'IDClassificacao',
                    'desc_classificacao'
                ),
                array('class' => 'm-wrap span6 select2', 'prompt' => 'Selecione a Classificação')
            ); ?>
            <?php echo $form->error($empregador, 'IDClassificacaoTributaria', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($empregador, 'CNO_empregador', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $empregador,
            'CNO_empregador',
            array('size' => 60, 'maxlength' => 12, 'class' => 'span4 m-wrap')
        ); ?>
        <?php echo $form->error($empregador, 'CNO_empregador', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="cpf">
    <div class="control-group">
        <?php echo $form->labelEx($empregador, 'CAEPF_empregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $empregador,
                'CAEPF_empregador',
                array('size' => 60, 'maxlength' => 255, 'class' => 'span4 m-wrap')
            ); ?>
            <?php echo $form->error($empregador, 'CAEPF_empregador', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($empregador, 'email_empregador', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $empregador,
            'email_empregador',
            array('maxlength' => 255, 'class' => 'span6 m-wrap')
        ); ?>
        <?php echo $form->error($empregador, 'email_empregador', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($empregador, 'optante_simplesEmpregador', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->checkBox(
            $empregador,
            'optante_simplesEmpregador',
            array()
        ); ?>
        <?php echo $form->error($empregador, 'optante_simplesEmpregador', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
<?php echo $form->labelEx($empregador, 'tel_empregador', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php
        $this->widget(
            'application.widgets.WCampoTelefone',
            array(
                'model' => $empregador,
                'form' => $form,
                'atributoTel' => 'tel_empregador',
                'atributoDDD' => 'ddd_telEmpregador',
            )
        )
        ?>
    </div>
    <p>Para cadastrar mais contatos
        <?php
        echo Html::link(
            'clique aqui',
            Yii::app()->createUrl('Empresa/contato'),
            array('target' => '_blank')
        );
        ?>
    </p>
</div>


<div class="control-group">
    <?php echo $form->labelEx($empregador, 'site_empregador', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $empregador,
            'site_empregador',
            array('size' => 60, 'maxlength' => 255, 'class' => 'span6 m-wrap')
        ); ?>
        <?php echo $form->error($empregador, 'site_empregador', array('class' => 'help-inline')); ?>
    </div>
</div>

</div>