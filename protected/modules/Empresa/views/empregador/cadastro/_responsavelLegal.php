

<div class="tab-pane row-fluid" id="tab_responsavelLegal">
    <div id="responsaveis">
        <?php
        $i = 0;
        foreach ($responsavel as $responsavel) {
            $i++;
            ?>
            <fieldset id="<?= $i ?>">
                <legend>Responsável #<?= $i ?></legend>
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $responsavel,
                        'nome_responsavelLegal',
                        array('class' => 'control-label')
                    ); ?>
                    <div class="controls">
                        <?php echo $form->hiddenField($responsavel, "[$i]IDResponsavel_Legal"); ?>
                        <?php echo $form->textField(
                            $responsavel,
                            "[$i]nome_responsavelLegal",
                            array('size' => 60, 'maxlength' => 255, 'class' => 'span6 m-wrap')
                        ); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $responsavel,
                        "[$i]cpf_responsavelLegal",
                        array('class' => 'control-label')
                    ); ?>
                    <div class="controls">
                        <?php
                        $this->widget(
                            'CMaskedTextField',
                            array(
                                'model' => $responsavel,
                                'attribute' => "[$i]cpf_responsavelLegal",
                                'mask' => HTexto::MASK_CPF,
                                'htmlOptions' => array('size' => 6, 'class' => 'span2 m-wrap')
                            )
                        );
                        ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $responsavel,
                        'identidade_responsavelLegal',
                        array('class' => 'control-label')
                    ); ?>
                    <div class="controls">
                        <?php echo $form->textField(
                            $responsavel,
                            "[$i]identidade_responsavelLegal",
                            array('class' => 'm-wrap span2', 'maxlength' => 12)
                        ); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $responsavel,
                        'orgao_expedidor_responsavelLegal',
                        array('class' => 'control-label')
                    ); ?>
                    <div class="controls">
                        <?php echo $form->textField(
                            $responsavel,
                            "[$i]orgao_expedidor_responsavelLegal",
                            array('class' => 'm-wrap span2', 'maxlength' => 10)
                        ); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $responsavel,
                        'uf_rg_responsavelLegal',
                        array('class' => 'control-label')
                    ); ?>
                    <div class="controls">
                        <?php echo $form->dropDownList(
                            $responsavel,
                            "[$i]uf_rg_responsavelLegal",
                            Html::listData(Estado::model()->findAll(), 'IDEstado', 'sigla_estado'),
                            array('class' => 'select2 span3')
                        ); ?>
                        <?php echo $form->error(
                            $responsavel,
                            'uf_rg_responsavelLegal',
                            array('class' => 'help-inline')
                        ); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $responsavel,
                        'ocupacao_responsavelLegal',
                        array('class' => 'control-label')
                    ); ?>
                    <div class="controls">
                        <?php echo $form->textField(
                            $responsavel,
                            "[$i]ocupacao_responsavelLegal",
                            array('class' => 'm-wrap span6')
                        ); ?>
                    </div>
                </div>
                <a class="link linkRemover" id="linkRemover_<?= $i ?>" href="" onclick="return removeResponsavel(this)">
                    Remover Responsável Legal
                </a>
            </fieldset>
        <?php
        }
        ?>
    </div>
    <a class="link" href="javascript:addResponsavel()">Adicionar outro Responsável Legal...</a>
    <br>
</div>

<script>
    //quantidade atual de responsáveis
    var qt = <?= $empregador->isNewRecord ? 1 : $i ?>;
    //indice crescente
    var indice = qt;

    $(document).ready(function () {
        if (qt == 1) {
            $('.linkRemover').addClass('muted');
        }
    });

    /*
     * Adiciona os campos para incluir outro responsável legal
     * faz uma copia do primeito fieldset e inclui na div #responsaveis
     * depois muda a legenda dessa copia, o nome/ID dos campos e o ID do link de remoção.
     */
    function addResponsavel() {
        //habilita o botao de remover
        $('.linkRemover').removeClass('muted');
        qt++;
        id = ++indice;
        //faz uma copia do formulario modelo
        $atual = $("#responsaveis  > fieldset:first-child").clone();
        $container = $("#responsaveis");
        //acrescenta a copia
        $container.append($atual);
        //insere a legenda
        $atual.children('legend').html('Responsavel #' + id);
        //define o id do botao remover
        $atual.children('a').attr('id', 'linkRemover_' + id);
        //pega cada campo input
        $atual.children('.control-group').children('.controls').children('input').each(function () {
                //limpa o value dos campos do novo formulario
                $(this).val('');
                //quebra as string de nome $htmlOptionse id
                nomeAntigo = $(this).attr('name');
                IDAntigo = $(this).attr('id');
                vetNome = nomeAntigo.split("1");
                vetID = IDAntigo.split("1");

                //atribui o nome e id dos campos corretamente
                if (vetNome.length == 2) {
                    nomeNovo = vetNome[0] + id + vetNome[1];
                    IDNovo = vetID[0] + id + vetID[1];
                    $(this).attr('name', nomeNovo);
                    $(this).attr('id', IDNovo);
                    //se for o campo de cpf, coloca a máscara de cpf
                    if ($(this).attr('id').indexOf('cpf') != -1) {
                        $('#' + IDNovo).mask('<?=HTexto::MASK_CPF?>');
                    }
                }
            }
        )
    }

    /**
     * Recebe como parametro um objeto HTML e remove da página o fieldset pai
     * Se a variavel qt for igual a 1, não será possivel remover o fieldset e os links da classe "linkremover"
     * recebem a classe "muted"
     */

    function removeResponsavel(obj) {
        if (qt > 1) {
            IDObj = $(obj).attr('id');
            $('#' + IDObj).parent('fieldset').remove();
            qt--;
        }
        if (qt == 1) {
            $('.linkRemover').addClass('muted');
        }
        return false;
    }
</script>

