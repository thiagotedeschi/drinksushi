<div class="tab-pane row-fluid" id="tab_enderecos">
    <?php
    $formEnderecoC = $this->widget(
        'application.widgets.WFormEndereco',
        array(
            'endereco' => $enderecoC,
            'form' => $form,
            'label' => $empregador->attributeLabels()['IDEndereco_correspondencia'],
            'id' => '1'
        )
    );
    ?>
    <?php
    $this->widget(
        'application.widgets.WFormEndereco',
        array(
            'endereco' => $enderecoF,
            'form' => $form,
            'label' => $empregador->attributeLabels()['IDEndereco_fiscal'],
            'id' => '2',
            'aproveitarEndereco' => $formEnderecoC->id,
            'desabilitarForm' => ($empregador->IDEndereco_correspondencia == $empregador->IDEndereco_fiscal && $empregador->IDEndereco_fiscal != '')
        )
    );
    ?>
</div>