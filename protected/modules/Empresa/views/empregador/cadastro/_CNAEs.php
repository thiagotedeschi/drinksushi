<div class="tab-pane row-fluid" id="tab_cnaes">
    <div class="control-group">
        <?php $lista = CHtml::listData(
            CNAE::model()->with('iDGrupo')->findAll(),
            'CNAE',
            'labelCnae',
            'labelGrupo'
        ); ?>
        <?php
        $IDSecundarios = array();
        $secundarios = $empregador->empregadorTemCnaes(array('condition' => ('NOT "principal_CNAE"')));
        foreach ($secundarios as $secundario) {
            $IDSecundarios[] = $secundario->IDCnae;
        }
        $principal = $empregador->empregadorTemCnaes(array('condition' => ('"principal_CNAE"')));
        if (isset($principal[0])) {
            $IDPrincipal = $principal[0]->IDCnae;
        } else
            $IDPrincipal = ''
        ?>
        <b>CNAE Principal:</b><br/><br/>
        <?=
        CHtml::dropDownList(
            "Cnaes[primario]",
            $IDPrincipal,
            $lista,
            array('class' => 'select2', 'style' => 'width:600px;')
        ) ?>
    </div>
    <div class="control-group">
        <b>CNAEs secundários:</b><br/><br/>
        <?=
        CHtml::dropDownList(
            "Cnaes[secundario]",
            $IDSecundarios,
            $lista,
            array('class' => 'select2', 'multiple' => 'multiple', 'style' => 'width:600px;')
        ) ?>
    </div>
</div>
