

<?php

Yii::app()->clientScript->registerScript(
    'habilitaNfeGrupo',
    'function habilitaNfeGrupo($dropDown){
       grupoEconomico = jQuery("option:selected",$dropDown).val()
       if(grupoEconomico == 0)
       {
           jQuery("#' . Html::activeId($empregador, 'NFEgrupo_empregador') . '").attr("disabled", "disabled");
        jQuery("#' . Html::activeId($empregador, 'NFEgrupo_empregador') . '").parent().removeClass("checked");

    }
    else
    {
        jQuery("#' . Html::activeId($empregador, 'NFEgrupo_empregador') . '").attr("disabled", false);
        jQuery("#' . Html::activeId($empregador, 'NFEgrupo_empregador') . '").closest("div").removeClass("disabled")
    }
    }',
    CClientScript::POS_HEAD
);

$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'empregador-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0', //classes form-horizontal e margin-0 no form
        )
    )
);

?>

    <div class='form clearfix positionRelative'>
        <p class="note note-warning">Campos com <b>*</b> são obrigatórios.</p>
        <?php
        //transforma o $responsavel em array se for um novo registro
        if (!is_array($responsavel)) {
            $responsavel = array($responsavel);
        }
        //cria o array de modelos a serem validados
        $validacoes = $responsavel;
        $validacoes[] = $empregador;
        //    echo '<pre>';var_dump($validacoes);die;
        echo $form->errorSummary($validacoes);
        ?>
        <div class="tabbable tabbable-custom tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_dadosBasicos" data-toggle="tab">Dados Básicos</a></li>
                <li><a href="#tab_cnaes" data-toggle="tab">CNAEs</a></li>
                <li><a href="#tab_enderecos" data-toggle="tab">Endereços</a></li>
                <li><a href="#tab_responsavelLegal" data-toggle="tab">Responsável Legal</a></li>
            </ul>
            <div class="tab-content">
                <?php
                $this->renderPartial('cadastro/_basico', array('empregador' => $empregador, 'form' => $form));
                $this->renderPartial('cadastro/_CNAEs', array('empregador' => $empregador, 'form' => $form));
                $this->renderPartial(
                    'cadastro/_enderecos',
                    array(
                        'empregador' => $empregador,
                        'form' => $form,
                        'enderecoC' => $enderecoC,
                        'enderecoF' => $enderecoF
                    )
                );
                $this->renderPartial(
                    'cadastro/_responsavelLegal',
                    array('empregador' => $empregador, 'form' => $form, 'responsavel' => $responsavel)
                );
                ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php echo Html::submitButton(
            $empregador->isNewRecord ? 'Criar ' . $empregador->labelModel() : 'Salvar Alterações',
            array('class' => 'botao')
        ); ?>
        <?php echo $form->error($empregador, 'email_empregador', array('class' => 'help-inline')); ?>
    </div>

<?php $this->endWidget('empregador-form'); ?>