<?php
/* @var $this EmpregadorController */
/* @var $data Empregador */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDEmpregador')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDEmpregador), array('view', 'id' => $data->IDEmpregador)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_empregador')); ?>:</b>
    <?php echo Html::encode($data->nome_empregador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('razao_empregador')); ?>:</b>
    <?php echo Html::encode($data->razao_empregador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('tipo_pessoaEmpregador')); ?>:</b>
    <?php echo Html::encode($data->tipo_pessoaEmpregador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('documento_empregador')); ?>:</b>
    <?php echo Html::encode($data->documento_empregador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('email_empregador')); ?>:</b>
    <?php echo Html::encode($data->email_empregador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('site_empregador')); ?>:</b>
    <?php echo Html::encode($data->site_empregador); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('NFEgrupo_empregador')); ?>:</b>
	<?php echo Html::encode($data->NFEgrupo_empregador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('tel_empregador')); ?>:</b>
	<?php echo Html::encode($data->tel_empregador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('ddd_telEmpregador')); ?>:</b>
	<?php echo Html::encode($data->ddd_telEmpregador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('ins_estadualEmpregador')); ?>:</b>
	<?php echo Html::encode($data->ins_estadualEmpregador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('ins_municipalEmpregador')); ?>:</b>
	<?php echo Html::encode($data->ins_municipalEmpregador); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDEndereco_correspondencia')); ?>:</b>
	<?php echo Html::encode($data->IDEndereco_correspondencia); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDEndereco_fiscal')); ?>:</b>
	<?php echo Html::encode($data->IDEndereco_fiscal); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDGrupo_economico')); ?>:</b>
	<?php echo Html::encode($data->IDGrupo_economico); ?>
	<br />

	*/
    ?>

</div>