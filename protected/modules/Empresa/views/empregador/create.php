<?php
/* @var $this EmpregadorController */
/* @var $model Empregador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>



<?php $this->renderPartial(
    '_form',
    array(
        'empregador' => $empregador,
        'enderecoC' => $enderecoC,
        'enderecoF' => $enderecoF,
        'responsavel' => $responsavel
    )
); ?>