<?php
/* @var $this GrupoEconomicoController */
/* @var $data GrupoEconomicoEmpregador */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDGrupo_economico')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDGrupo_economico), array('view', 'id' => $data->IDGrupo_economico)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_grupoEconomico')); ?>:</b>
    <?php echo Html::encode($data->nome_grupoEconomico); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDResponsavel_grupoEconomico')); ?>:</b>
    <?php echo Html::encode($data->IDResponsavel_grupoEconomico); ?>
    <br/>


</div>