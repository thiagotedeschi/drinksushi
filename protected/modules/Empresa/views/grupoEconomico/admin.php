<?php

/* @var $this GrupoEconomicoController */
/* @var $model GrupoEconomicoEmpregador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php

$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'grupo-economico-empregador-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'addInModal' => true,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDGrupo_economico',
            'nome_grupoEconomico',
            array(
                'header' => 'Responsável',
                'name' => 'nome_empregador',
                'value' => '$data->iDResponsavelGrupoEconomico->getLabelEmpregador()',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false'),
                            'isModal' => true,
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false'),
                            'isModal' => true,

                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
