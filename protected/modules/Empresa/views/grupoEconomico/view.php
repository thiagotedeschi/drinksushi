<?php
/* @var $this GrupoEconomicoController */
/* @var $model GrupoEconomicoEmpregador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDGrupo_economico',
            'nome_grupoEconomico',
            array(
                'label' => 'Responsável',
                'value' => $model->iDResponsavelGrupoEconomico->getLabelEmpregador()
            ),
        ),
    )
); ?>
