<?php
/* @var $this GrupoEconomicoController */
/* @var $model GrupoEconomicoEmpregador */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDGrupo_economico'); ?>
        <?php echo $form->textField($model, 'IDGrupo_economico', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_grupoEconomico'); ?>
        <?php echo $form->textField(
            $model,
            'nome_grupoEconomico',
            array('class' => 'm-wrap span6', 'maxlength' => 255)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDResponsavel_grupoEconomico'); ?>
        <?php echo $form->textField($model, 'IDResponsavel_grupoEconomico', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->