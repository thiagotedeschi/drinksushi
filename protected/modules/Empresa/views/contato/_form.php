<?php
/* @var $this ContatoController */
/* @var $model Contato */
/* @var $form CActiveForm */
$telefonePrincipal = $model->getTelefonePrincipal();
$telefonesSecundarios = $model->getTelefonesSecundarios();
$id = count($telefonesSecundarios);
?>
<script src="<?= ASSETS_LINK ?>/plugins/jquery-tags-input/jquery.tagsinput.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?= ASSETS_LINK ?>/plugins/jquery-tags-input/jquery.tagsinput.css"/>
<script>
    var qt = <?= $model->isNewRecord ? 1 : $id+1 ?>;
    $(document).ready(function () {
        $('#emails').tagsInput({
            width: 'auto',
            defaultText: '+ Email'
        });

        $(document).on('keyup', '.mask_ddd', function () {
            mudaCampo($(this));
        });

    });

    function addTelefone() {
        $container = $("#telefones");
        id = ++qt;
        $container.append('<div class="control-group">\
            <label class="control-label">Telefone ' + id + ' (Somente Números)</label>\
            <div class="controls">\
                <div class="telefone">\
                    <i class="icon-trash-o remove" style="cursor:pointer;" onclick="removeTelefone(this)"> </i> \
                    <input id="" name="Telefones[secundario][' + id + '][ddd]" type="text" placeholder="DDD" maxlength="2" class="m-wrap span1 w-telefone mask_ddd" value="" /> \
                    <input id="" name="Telefones[secundario][' + id + '][numero]" type="text" placeholder="Número" class="m-wrap span2 w-telefone telefone-mask" value="" /> \
                    <input id="" name="Telefones[secundario][' + id + '][ramal]" type="text" placeholder="Ramal" maxlength="5" class="m-wrap span1 w-telefone" value="" />\
                    <span style="padding-bottom: -10px;">\
                        <label title="Fixo" class="checkbox inline">\
                            <input type="radio" name="Telefones[secundario][' + id + '][tipo]" class="toggle" checked="checked" value="f"> &#xf095;\
                        </label>\
                        <label title="Celular" class="checkbox inline">\
                            <input type="radio" name="Telefones[secundario][' + id + '][tipo]" class="toggle" value="c"> &#xf10b;\
                        </label>\
                    </span>\
                </div>\
            </div>\
        </div>\
        ');
    }

    function removeTelefone(icon) {
        $(icon).closest('.control-group').remove();
    }
    function mudaCampo(obj) {
        if ($(obj).val().length == 2) {
            $(obj).next().focus();
        }
    }

</script>
<style>
    .telefone label {
        font-family: FontAwesome;
        font-size: 170%;
    }
</style>
<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'contato-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_contato', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'nome_contato',
                array('class' => 'm-wrap span6', 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'nome_contato', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Responsabilidades: </label>

        <div class="controls">
            <?php
            $resps = array();
            if (!$model->isNewRecord) {
                $resps = Html::listData($model->ResponsabilidadeContatos(), 'IDResponsabilidade_contato', 'nome_resp');
            }
            //var_dump($resps);
            ?>
            <?=
            Html::dropDownList(
                "Responsabilidades",
                array_keys($resps),
                Html::listData(ResponsabilidadeContato::model()->findAll(), 'IDResponsabilidade_contato', 'nome_resp'),
                array('class' => 'select2', 'style' => 'width:600px;', 'multiple' => 'multiple')
            ) ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'principal_contato', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->checkbox(
                $model,
                'principal_contato',
                array('uncheckValue' => '0', 'class' => 'toggle')
            ); ?>
            <?php echo $form->error($model, 'principal_contato', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php $model->isNewRecord ? $model->IDEmpregador = Yii::app()->user->getState('IDEmpregador') : null ?>
            <?php echo $form->dropDownList(
                $model,
                'IDEmpregador',
                Yii::app()->session['empregadores'],
                array('class' => 'm-wrap span6 select2', 'empty' => 'Escolha um Empregador')
            ); ?>
            <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <fieldset>
        <legend>Emails</legend>
        <div class="control-group">
            <label class="control-label">Email Principal</label>

            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-envelope"></i>
                    <?php
                    $emailPrincipal = $model->getEmailPrincipal();
                    $emailsSecundarios = $model->getEmailsSecundarios();
                    ?>
                    <input id="email" name="Emails[principal]" type="text" placeholder="Email Principal" class="m-wrap"
                           value="<?= $emailPrincipal ?>"/>
                </div>

            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Outros Emails (separados por vírgula)</label>

            <div class="controls">
                <input id="emails" type="text" name="Emails[secundarios]" class="m-wrap tags span2"
                       value="<?= implode(',', $emailsSecundarios) ?>"/>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Telefones</legend>
        <div id="telefones">
            <div class="control-group">
                <label class="control-label">Telefone Principal (Somente Números)</label>

                <div class="controls">
                    <div class="telefone">
                        &nbsp;&nbsp;<input id="" name="Telefones[principal][ddd]" type="text" maxlength="2"
                                           placeholder="DDD"
                                           class="m-wrap span1 w-telefone mask_ddd"
                                           value="<?= $telefonePrincipal->ddd_telefoneContato ?>"/>
                        <input id="" name="Telefones[principal][numero]" type="text" placeholder="Número"
                               class="m-wrap span2 w-telefone telefone-mask"
                               value="<?= $telefonePrincipal->numero_telefoneContato ?>"/>
                        <input id="" name="Telefones[principal][ramal]" type="text" maxlength="5" placeholder="Ramal"
                               class="m-wrap span1 w-telefone"
                               value="<?= $telefonePrincipal->ramal_telefoneContato ?>"/>
                        <span style="">
                            <label title="Fixo" class="checkbox inline">
                                <input type="radio" name="Telefones[principal][tipo]"
                                       class="toggle" <?= $telefonePrincipal->tipo_telefoneContato == 'c' ? : 'checked="checked"' ?>
                                       value="f"> &#xf095;
                            </label>
                            <label title="Celular" class="checkbox inline">
                                <input type="radio"
                                       name="Telefones[principal][tipo]" <?= $telefonePrincipal->tipo_telefoneContato == 'f' ? : 'checked="checked"' ?>
                                       class="toggle" value="c"> &#xf10b;
                            </label>
                        </span>
                    </div>
                </div>
            </div>
            <?php
            foreach ($telefonesSecundarios as $telefone) {
                echo '<div class="control-group">
            <label class="control-label">Telefone ' . $id . ' (Somente Números)</label>
            <div class="controls">
                <div class="telefone">
                    <i class="icon-trash-o remove" style="cursor:pointer;" onclick="removeTelefone(this)"> </i>
                    <input id="" name="Telefones[secundario][' . $id . '][ddd]" type="text" placeholder="DDD" maxlength="2" class="m-wrap span1 w-telefone mask_ddd" value="' . $telefone->ddd_telefoneContato . '" />
                    <input id="" name="Telefones[secundario][' . $id . '][numero]" type="text" placeholder="Número" class="m-wrap span2 w-telefone telefone-mask" value="' . $telefone->numero_telefoneContato . '" />
                    <input id="" name="Telefones[secundario][' . $id . '][ramal]" type="text" placeholder="Ramal" maxlength="5" class="m-wrap span1 w-telefone" value="' . $telefone->ramal_telefoneContato . '" />
                    <span style="padding-bottom: -10px;">
                        <label title="Fixo" class="checkbox inline">
                            <input type="radio" name="Telefones[secundario][' . $id . '][tipo]" class="toggle" ' . ($telefone->tipo_telefoneContato == 'c' ? : 'checked="checked"') . ' value="f"> &#xf095;
                        </label>
                        <label title="Celular" class="checkbox inline">
                            <input type="radio" name="Telefones[secundario][' . $id . '][tipo]" class="toggle" ' . ($telefone->tipo_telefoneContato == 'f' ? : 'checked="checked"') . ' value="c"> &#xf10b;
                        </label>
                    </span>
                </div>
            </div>
        </div>';
                $id++;
            }
            ?>
        </div>
        <a class="link" href="javascript:addTelefone()">Adicionar outro telefone...</a>
    </fieldset>

</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
