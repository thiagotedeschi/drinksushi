<?php
/* @var $this ContatoController */
/* @var $data Contato */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDContato')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDContato), array('view', 'id' => $data->IDContato)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_contato')); ?>:</b>
    <?php echo Html::encode($data->nome_contato); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('email_contato')); ?>:</b>
    <?php echo Html::encode($data->email_contato); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('principal_contato')); ?>:</b>
    <?php echo Html::encode($data->principal_contato); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDEmpregador')); ?>:</b>
    <?php echo Html::encode($data->IDEmpregador); ?>
    <br/>


</div>