<?php
/* @var $this ContatoController */
/* @var $model Contato */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDContato'); ?>
        <?php echo $form->textField($model, 'IDContato', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_contato'); ?>
        <?php echo $form->textField($model, 'nome_contato', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'email_contato'); ?>
        <?php echo $form->textField($model, 'email_contato', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'principal_contato'); ?>
        <?php echo $form->textField($model, 'principal_contato', array('class' => 'm-wrap span6', 'maxlength' => 1)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDEmpregador'); ?>
        <?php echo $form->textField($model, 'IDEmpregador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->