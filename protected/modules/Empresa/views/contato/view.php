<?php

/* @var $this ContatoController */
/* @var $model Contato */
$telefonePrincipal = $model->getTelefonePrincipal();
$telefonesSecundarios = $model->getTelefonesSecundarios();
$emailPrincipal = $model->getEmailPrincipal();
$emailsSecundarios = $model->getEmailsSecundarios();
//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDContato',
            'nome_contato',
            'principal_contato:boolean',
            'IDEmpregador' => array(
                'name' => 'Empregador',
                'value' => $model->iDEmpregador->getLabelEmpregador()
            ),
            'Email Principal' => array(
                'name' => 'Email Principal',
                'value' => $emailPrincipal
            ),
            'Outros Emails' => array(
                'name' => 'Outros Emails',
                'value' => implode(', ', $emailsSecundarios)
            ),
            'Telefone Principal' => array(
                'name' => 'Telefone Principal',
                'value' => $telefonePrincipal
            ),
            'Outros Telefones' => array(
                'name' => 'Outros Telefones',
                'value' => implode(', ', $telefonesSecundarios)
            ),
        ),
    )
);
?>
