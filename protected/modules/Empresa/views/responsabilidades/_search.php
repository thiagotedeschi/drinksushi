<?php
/* @var $this ResponsabilidadesController */
/* @var $model ResponsabilidadeContato */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDResponsabilidade_contato'); ?>
        <?php echo $form->textField($model, 'IDResponsabilidade_contato', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_resp'); ?>
        <?php echo $form->textField($model, 'nome_resp', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->