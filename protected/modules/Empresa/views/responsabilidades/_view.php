<?php
/* @var $this ResponsabilidadesController */
/* @var $data ResponsabilidadeContato */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDResponsabilidade_contato')); ?>:</b>
    <?php echo Html::link(
        Html::encode($data->IDResponsabilidade_contato),
        array('view', 'id' => $data->IDResponsabilidade_contato)
    ); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_resp')); ?>:</b>
    <?php echo Html::encode($data->nome_resp); ?>
    <br/>


</div>