<?php
/* @var $this MesaController */
/* @var $model Mesa */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>




<?php $this->widget('application.widgets.grid.GridView', array(
	'id'=>'mesa-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
            'selectableRows' => 2,
	'columns'=>array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),

//		'IDMesa',
		'numero_mesa',
		'localizacao_mesa',
		/*
		'IDEmpregador',
		*/
            array(
                 'class' => 'ButtonColumn',
                 'template' => '{view}{update}{delete}',
                 'buttons' =>
                 array(
                      'view' => array(
                           'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                      ),
                      'update' => array(
                           'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                      ),
                      'delete' => array(
                           'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                      ),
                 ),
            ),
	),
)); ?>
