<?php
/* @var $this MesaController */
/* @var $model Mesa */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget('application.widgets.DetailView', array(
	'data'=>$model,
	'attributes'=>array(
//		'IDMesa',
		'numero_mesa',
		'localizacao_mesa',
        'IDEmpregador' => array(
            'name' => $model->attributeLabels()['IDEmpregador'],
            'value' => Empregador::model()->findbyPk($model->IDEmpregador)->getLabelEmpregador()
        ),
	),
)); ?>
