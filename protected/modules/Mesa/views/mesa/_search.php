<?php
/* @var $this MesaController */
/* @var $model Mesa */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'IDMesa'); ?>
            <?php echo $form->textField($model,'IDMesa',array('class'=>'m-wrap span6')); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'numero_mesa'); ?>
            <?php echo $form->textField($model,'numero_mesa',array('class'=>'m-wrap span6','maxlength'=>50)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'localizacao_mesa'); ?>
            <?php echo $form->textField($model,'localizacao_mesa',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
        </div>

                    <div class="row">
            <?php echo $form->label($model,'IDEmpregador'); ?>
            <?php echo $form->textField($model,'IDEmpregador',array('class'=>'m-wrap span6')); ?>
        </div>

        <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->