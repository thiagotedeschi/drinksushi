<?php
/* @var $this MesaController */
/* @var $model Mesa */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mesa-form',
	'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
            'class' => 'form-horizontal margin-0',
            )
        )); ?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php
    echo $form->errorSummary($model);
    $IDEmpregador = Yii::app()->user->getState('IDEmpregador');

        if (isset($IDEmpregador)) {
            ?>
            <div class="control-group">
                        <?php echo $form->labelEx($model,'numero_mesa',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'numero_mesa',array('class'=>'m-wrap span6','maxlength'=>50)); ?>
                <?php echo $form->error($model,'numero_mesa',array('class'=>'help-inline')); ?>
            </div>
        </div>
            <div class="control-group">
                        <?php echo $form->labelEx($model,'localizacao_mesa',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'localizacao_mesa',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'localizacao_mesa',array('class'=>'help-inline')); ?>
            </div>
        </div>

    <?php
            echo Html::activeHiddenField(
                $model,
                'IDEmpregador',
                array('value' => $IDEmpregador)
            );

        }else{
            echo '
                <div class="alert alert-error text-center">
                    Por favor, selecione um empregador antes de criar uma situação do trabalhador.
                </div>
            ';
        }
    ?>
    </div>

<div class="form-actions">
    <?php echo Html::submitButton($model->isNewRecord ? 'Cadastrar  '.$model->labelModel() : 'Salvar Alterações', array('class'=>'botao')); ?>
</div>
<?php $this->endWidget(); ?>
