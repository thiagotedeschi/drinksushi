<?php
/* @var $this MesaController */
/* @var $data Mesa */
?>

<div class="view">

    	<b><?php echo Html::encode($data->getAttributeLabel('IDMesa')); ?>:</b>
	<?php echo Html::link(Html::encode($data->IDMesa), array('view', 'id'=>$data->IDMesa)); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('numero_mesa')); ?>:</b>
	<?php echo Html::encode($data->numero_mesa); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('localizacao_mesa')); ?>:</b>
	<?php echo Html::encode($data->localizacao_mesa); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDEmpregador')); ?>:</b>
	<?php echo Html::encode($data->IDEmpregador); ?>
	<br />


</div>