<?php
/* @var $this FolgaProfissionalController */
/* @var $model Horario */
/* @var $localHorarioProfissional LocalProfissionalHorario */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));



?>


<?php $this->renderPartial(
    '_form',
    array(
        'model' => $model,
        'localHorarioProfissional' => $localHorarioProfissional
    )
); ?>