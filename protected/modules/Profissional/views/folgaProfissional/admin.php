<?php
/* @var $this FolgaProfissionalController */
/* @var $model Horario */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada
    pesquisa.
</p>


<?php $this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'horario-grid',
        'dataProvider' => $model->search('Profissional'),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDHorario',
            'localProfissionalHorarios.iDLocal' => array(
                'value' => '$data->localProfissionalHorarios->iDLocal->nome_local',
                'header' => 'Local',
                'name' => 'nome_local',
            ),
            'localProfissionalHorarios.iDProfissional' => array(
                'value' => '$data->localProfissionalHorarios->iDProfissional->nome_profissional',
                'header' => 'Profissional',
                'name' => 'nome_profissional'
            ),
            'inicio_horario' => array(
                'value' => 'HData::formataData($data->inicio_horario, HData::BR_TIME_SIMPLE_FORMAT, HData::SQL_TIME_FORMAT)." às ".HData::formataData($data->fim_horario, HData::BR_TIME_SIMPLE_FORMAT, HData::SQL_TIME_FORMAT)',
                'header' => 'Horário',
                'filter' => false
            ),
            /*
            'segunda',
            'terca',
            'quarta',
            'quinta',
            'sexta',
            'sabado',
            'domingo',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
); ?>
