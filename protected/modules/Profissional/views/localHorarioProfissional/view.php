<?php
/* @var $this LocalHorarioProfissionalController */
/* @var $model Horario */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDHorario',
            'iDProfissional' => array(
                'name' => 'Profissional',
                'value' => $model->iDProfissional[0]->nome_profissional
            ),
            'inicio_horario' => array(
                'name' => $model->attributeLabels()['inicio_horario'],
                'value' => HData::formataData(
                        $model->inicio_horario,
                        HData::BR_TIME_SIMPLE_FORMAT,
                        HData::SQL_TIME_FORMAT
                    )
            ),
            'fim_horario' => array(
                'name' => $model->attributeLabels()['fim_horario'],
                'value' => HData::formataData($model->fim_horario, HData::BR_TIME_SIMPLE_FORMAT, HData::SQL_TIME_FORMAT)
            ),
            'dias' => array(
                'name' => 'Dias da Semana',
                'type' => 'raw',
                'value' => $model->getDias()
            ),
            'local' => array(
                'name' => 'Local',
                'value' => $model->iDLocalProfissional[0]->nome_local
            ),
            'dt_inicio' => array(
                'name' => 'Data Início',
                'value' => HData::formataData(
                        $model->localProfissionalHorarios->dt_inicio,
                        HData::BR_DATE_FORMAT,
                        HData::SQL_DATE_FORMAT
                    )
            ),
            'dt_fim' => array(
                'name' => 'Data Fim',
                'value' => HData::formataData(
                        $model->localProfissionalHorarios->dt_fim,
                        HData::BR_DATE_FORMAT,
                        HData::SQL_DATE_FORMAT
                    )
            )
            /*'segunda',
    'terca',
    'quarta',
    'quinta',
    'sexta',
    'sabado',
    'domingo',*/
        ),
    )
); ?>
