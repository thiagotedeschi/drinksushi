<?php
/* @var $this LocalHorarioProfissionalController */
/* @var $model Horario */
/* @var $localHorarioProfissional LocalProfissionalHorario */
/* @var $form CActiveForm */

if (!$localHorarioProfissional->isNewRecord) {
    $localHorarioProfissional->dt_inicio = HData::formataData(
        $localHorarioProfissional->dt_inicio,
        HData::BR_DATE_FORMAT,
        HData::SQL_DATE_FORMAT
    );
    $localHorarioProfissional->dt_fim = HData::formataData(
        $localHorarioProfissional->dt_fim,
        HData::BR_DATE_FORMAT,
        HData::SQL_DATE_FORMAT
    );
}

?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'horario-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo Html::errorSummary($model); ?>
    <?php echo Html::errorSummary($localHorarioProfissional); ?>

    <div class="control-group">
        <?php echo Html::activeLabelEx(
            $localHorarioProfissional,
            'IDProfissional',
            array('class' => 'control-label')
        ); ?>
        <div class="controls">
            <?php echo Html::activeDropDownList(
                $localHorarioProfissional,
                "IDProfissional",
                Html::listData(Profissional::model()->findAll(), 'IDProfissional', 'nome_profissional'),
                array("class" => "m-wrap span6 select2", 'prompt' => 'Escolha um Profissional')
            ) ?>
            <?php echo Html::error($model, 'IDProfissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo Html::activeLabelEx($localHorarioProfissional, 'IDLocal', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeDropDownList(
                $localHorarioProfissional,
                "IDLocal",
                Html::listData(Local::model()->findAll(), 'IDLocal', 'nome_local'),
                array("class" => "m-wrap span6 select2", 'prompt' => 'Escolha um Local')
            ) ?>
            <?php echo Html::error($model, 'IDLocal', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo Html::activeLabelEx($model, 'inicio_horario', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeTextField(
                $model,
                'inicio_horario',
                array('class' => 'm-wrap span6 time-picker')
            ); ?>
            <?php echo Html::error($model, 'inicio_horario', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo Html::activeLabelEx($model, 'fim_horario', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeTextField($model, 'fim_horario', array('class' => 'm-wrap span6 time-picker')); ?>
            <?php echo Html::error($model, 'fim_horario', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo Html::activeLabelEx($model, 'diaSemana', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeDropDownList(
                $model,
                "diaSemana",
                $model->diaSemana,
                array(
                    "class" => "m-wrap span6 select2",
                    "multiple" => "multiple",
                    'onchange' => '
                        $.ajax({
                            url: "' . CController::createUrl('AjaxConfereHorarioLocalProfissional') . '",
                            type: "post",
                            dataType: "JSON",
                            data: {
                                diaSemana: $("#' . Html::activeId($model, 'diaSemana') . '").val(),
                                IDLocal: $("#' . Html::activeId($localHorarioProfissional, 'IDLocal') . '").val(),
                                fim_horario: $("#' . Html::activeId($model, 'fim_horario') . '").val(),
                                inicio_horario: $("#' . Html::activeId($model, 'inicio_horario') . '").val()' . '
                            },
                            success: function(response){
                                if(response){
                                    $("#erroHorario").slideDown();
                                    $("#erroHorario").html(response);
                                }else{
                                    $("#erroHorario").slideUp();
                                }
                            },
                        })'
                )
            ) ?>
            <?php echo Html::error($model, 'diaSemana', array('class' => 'help-inline')); ?>
            <div id="erroHorario" style="clear: both; display: none;" class="note note-warning-fillout"></div>
        </div>
    </div>


    <div class="control-group">
        <?php echo Html::activeLabelEx($localHorarioProfissional, 'dt_inicio', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeTextField(
                $localHorarioProfissional,
                'dt_inicio',
                array('class' => 'm-wrap span6 date-picker')
            ); ?>
            <?php echo Html::error($localHorarioProfissional, 'dt_inicio', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo Html::activeLabelEx($localHorarioProfissional, 'dt_fim', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeTextField(
                $localHorarioProfissional,
                'dt_fim',
                array('class' => 'm-wrap span6 date-picker')
            ); ?>
            <?php echo Html::error($localHorarioProfissional, 'dt_fim', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<script>
    $('#<?= Html::activeId($model, 'diaSemana')?>').ready(function () {
        $('#<?=Html::activeId($model, 'diaSemana')?> option[value=domingo]').attr('selected', <?=$model->domingo ? 'true' : 'false'?>);
        $('#<?=Html::activeId($model, 'diaSemana')?> option[value=segunda]').attr('selected', <?=$model->segunda ? 'true' : 'false'?>);
        $('#<?=Html::activeId($model, 'diaSemana')?> option[value=terca]').attr('selected', <?=$model->terca ? 'true' : 'false'?>);
        $('#<?=Html::activeId($model, 'diaSemana')?> option[value=quarta]').attr('selected', <?=$model->quarta ? 'true' : 'false'?>);
        $('#<?=Html::activeId($model, 'diaSemana')?> option[value=quinta]').attr('selected', <?=$model->quinta ? 'true' : 'false'?>);
        $('#<?=Html::activeId($model, 'diaSemana')?> option[value=sexta]').attr('selected', <?=$model->sexta ? 'true' : 'false'?>);
        $('#<?=Html::activeId($model, 'diaSemana')?> option[value=sabado]').attr('selected', <?=$model->sabado ? 'true' : 'false'?>);
    });
</script>
<?php $this->endWidget(); ?>
