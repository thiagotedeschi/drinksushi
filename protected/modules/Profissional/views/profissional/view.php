<?php
/* @var $this ProfissionalController */
/* @var $model Profissional */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDProfissional',
            'nome_profissional',
            'genero_profissional' => array(
                'name' => $model->attributeLabels()['genero_profissional'],
                'value' => $model->genero[$model->genero_profissional]
            ),
            'cpf_profissional' => array(
                'name' => $model->attributeLabels()['cpf_profissional'],
                'value' => HTexto::formataString(HTexto::MASK_CPF, $model->cpf_profissional)
            ),
            'rg_profissional',
            'orgao_expedidor_Profissional',
            'uf_rg_Profissinal' => array(
                'name' => $model->attributeLabels()['uf_rg_Profissinal'],
                'value' => Estado::model()->getEstado($model->uf_rg_Profissinal)
            ),
            'IDProfissao' => array(
                'name' => $model->attributeLabels()['IDProfissao'],
                'type' => 'raw',
                'value' => isset($model->iDProfissao->nome_profissao) ? $model->iDProfissao->nome_profissao : ''
            ),
            'nit_profissional',
            'inscricao_mteProfissional',
            'org_classeProfissional',
            'n_registroProfissional',
            'IDBanco' => array(
                'name' => $model->attributeLabels()['IDBanco'],
                'type' => 'raw',
                'value' => isset($model->iDBanco->nome_banco) ? $model->iDBanco->nome_banco : ''
            ),
            'agencia_profissional',
            'conta_profissional',
            'variacao',
            'telefone_profissional',
            'ramal_profissional',
            'celular_profissional',
            'email_profissional',
            'ddd_telefoneProfissional',
            'ddd_celularProfissional',
            'receber_emails' => array(
                'name' => $model->attributeLabels()['receber_emails'],
                'type' => 'raw',
                'value' => $model->receber_emails ? 'Sim' : 'Não'
            ),
            'IDEndereco' => array(
                'name' => $model->attributeLabels()['IDEndereco'],
                'type' => 'raw',
                'value' => $model->iDEndereco->getLabelEndereco(),
            ),
            array(
                'label' => 'Empregadores',
                'value' => Html::htmlList($model->labelEmpregadores, array()),
                'type' => 'raw',
            )
        ),
    )
); ?>
