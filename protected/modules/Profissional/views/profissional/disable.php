<?php
/* @var $this HistoricoProfissionalController */
/* @var $model HistoricoProfissional */
/* @var $form CActiveForm */

$model->dt_modificacaoHistoricoProfissional = !empty($model->dt_modificacaoHistoricoProfissional)
    ? HData::sqlToBr($model->dt_modificacaoHistoricoProfissional)
    : HData::hoje();
?>

<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'historico-profissional-disable-form',
//        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="portlet-body clearfix">
    <div class="scroller" data-height="230px" data-always-visible="1" data-rail-visible1="1">
        <?php
        $this->widget('application.widgets.WAviso', array('avisos' => $avisos));
        ?>
        <div class="form clearfix positionRelative">
            <p class="note note-warning">Campos com * são obrigatórios.</p>
            <?php echo $form->errorSummary($model); ?>

            <div class="control-group">
                <?php echo $form->labelEx(
                    $model,
                    'motivo_historicoProfissional',
                    array('class' => 'control-label')
                ); ?>
                <div class="controls">
                    <?php echo $form->textArea(
                        $model,
                        'motivo_historicoProfissional',
                        ['class' => 'span6', 'rows' => 5, 'cols' => 10]
                    ); ?>
                    <?php echo $form->error(
                        $model,
                        'motivo_historicoProfissional',
                        array('class' => 'help-inline')
                    ); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <?php echo Html::submitButton(
            $profissional->labelAcaoDesativar . ' ' . Profissional::model()->labelModel(),
            array('class' => 'botao')
        ); ?>
    </div>
    <div>
        <?php $this->endWidget(); ?>

        <script>
            $(document).ready(function () {
                $('.button-toggle').click(function () {
                    $('#div-' + this.id).slideToggle();
                    $icon = $('#icon-' + this.id);
                    if ($icon.hasClass('icon-plus')) {
                        $icon.addClass('icon-minus');
                        $icon.removeClass('icon-plus');
                    } else {
                        $icon.removeClass('icon-minus');
                        $icon.addClass('icon-plus');
                    }
                });
            });
        </script>