<?php
/* @var $this ProfissionalController */
/* @var $model Profissional */
/* @var $endereco EnderecoProfissional */
/* @var $profEmpregador ProfissionalEmpregador */
/* @var $form CActiveForm */

?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'profissional-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
$erros = array($model);
if ($model->isNewRecord) {
    $erros[] = $profEmpregador;
}
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($erros); ?>
    <div class="tabbable tabbable-custom tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_pessoal" data-toggle="tab">Informações Pessoais</a></li>
            <li id="li_profissao"><a href="#tab_profissao" data-toggle="tab">Dados Profissão</a></li>
            <li><a href="#tab_bancarios" data-toggle="tab">Dados Bancários</a></li>
            <li><a href="#tab_contato" data-toggle="tab">Contato</a></li>
            <li><a href="#tab_endereco" data-toggle="tab">Endereço</a></li>
            <li><a href="#tab_empregador" data-toggle="tab">Empregador</a></li>
        </ul>
        <div class="tab-content">
            <?php
            $this->renderPartial(
                'cadastro/_formTabs',
                array('model' => $model, 'form' => $form, 'endereco' => $endereco, 'profEmpregador' => $profEmpregador)
            );
            ?>
        </div>
    </div>
</div>
<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<script>
    $('#<?= Html::activeId($model, 'IDProfissao'); ?>').ready(function () {
        montaCampo(true);
    });

    $('#<?= Html::activeId($model, 'IDProfissao'); ?>').change(function () {
        montaCampo(false);
    });

    function montaCampo(load) {
        $.ajax({
            url: "<?= CController::createUrl('profissional/montaCampos') ?>",
            type: "POST",
            data: {
                IDProfissao: $('#<?= Html::activeId($model, 'IDProfissao'); ?>').val(),
                <?php echo $model->isNewRecord ? '' : 'IDProfissional:' . $model->IDProfissional ?>
            },
            beforeSend: function () {
                $("#tab_profissao").addClass('grid-view-loading');
            },
            success: function (html) {
                if (html) {
                    $div = $("#tab_profissao");
                    $div.html(html);
                    if (!load) {
                        $('.tab-pane').removeClass('active');
                        $('.nav-tabs > li').removeClass('active');
                        $('#li_profissao').addClass('active');
                        $div.addClass('active');
                    }
                }
                else {
                    $div.html('');
                }
            },
            complete: function () {
                $("#tab_profissao").removeClass('grid-view-loading');
            }
        });
    }
</script>
<?php $this->endWidget(); ?>
