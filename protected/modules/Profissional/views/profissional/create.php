<?php
/* @var $this ProfissionalController */
/* @var $model Profissional */
/* @var $endereco EnderecoProfissional */
/* @var $profEmpregador ProfissionalEmpregador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->renderPartial(
    '_form',
    array('model' => $model, 'endereco' => $endereco, 'profEmpregador' => $profEmpregador)
); ?>