<?php
/* @var $this ProfissionalController */
/* @var $model Profissional */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

<div id="modal-desativar" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="col-md-12">
                <iframe src="" style="width:100%; overflow: hidden; min-height:400px;"></iframe>
            </div>
        </div>
    </div>
</div>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php $this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'profissional-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDProfissional',
            'nome_profissional',
            'cpf_profissional' => array(
                'value' => 'HTexto::formataString(HTexto::MASK_CPF, $data->cpf_profissional)',
                'name' => 'cpf_profissional',
                'header' => 'CPF',
                'id' => 'cpf_profissional',
                'filter' => Html::activeTextField(
                        $model,
                        'cpf_profissional',
                        array('maxlength' => 15, 'placeholder' => 'Buscar por: CPF')
                    )
            ),
            'nome_profissao' => array(
                'value' => '$data->iDProfissao',
                'name' => 'nome_profissao',
                'header' => 'Profissão'
            ),
            'ativo_profissional' => array(
                'value' => '$data->estaAtivo() ? "Sim" : "Não"',
                'name' => 'ativo_profissional',
                'header' => 'Profissional Ativo?',
                'filter' => array('true' => 'Sim', 'false' => 'Não')
            ),
            /*
            'genero_profissional',
            'rg_profissional',
            'org_classeProfissional',
            'n_registroProfissional',
            'agencia_profissional',
            'conta_profissional',
            'variacao',
            'IDBanco',

            'IDEndereco',
            'inscricao_mteProfissional',
            'telefone_profissional',
            'ramal_profissional',
            'celular_profissional',
            'email_profissional',
            'ddd_telefoneProfissional',
            'ddd_celularProfissional',
            'receber_emails',
            'nit_profissional',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{desativar}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false'),
                        ),
                        'desativar' => array(
                            'options' => array('class' => 'botao desativar', 'title' => 'Ativar/Desativar'),
                            'imageUrl' => false,
                            'label' => '"<i class=\'icon-power-off icon-large\'></i>"',
                            'isModal' => true,
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
); ?>

<script>
    $(document).ready(function () {
        getFlashes();
    })
</script>