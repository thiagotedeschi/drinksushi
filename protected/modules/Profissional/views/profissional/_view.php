<?php
/* @var $this ProfissionalController */
/* @var $data Profissional */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDProfissional')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDProfissional), array('view', 'id' => $data->IDProfissional)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_profissional')); ?>:</b>
    <?php echo Html::encode($data->nome_profissional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('genero_profissional')); ?>:</b>
    <?php echo Html::encode($data->genero_profissional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('cpf_profissional')); ?>:</b>
    <?php echo Html::encode($data->cpf_profissional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('rg_profissional')); ?>:</b>
    <?php echo Html::encode($data->rg_profissional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('org_classeProfissional')); ?>:</b>
    <?php echo Html::encode($data->org_classeProfissional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('n_registroProfissional')); ?>:</b>
    <?php echo Html::encode($data->n_registroProfissional); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('agencia_profissional')); ?>:</b>
	<?php echo Html::encode($data->agencia_profissional); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('conta_profissional')); ?>:</b>
	<?php echo Html::encode($data->conta_profissional); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('variacao')); ?>:</b>
	<?php echo Html::encode($data->variacao); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDBanco')); ?>:</b>
	<?php echo Html::encode($data->IDBanco); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDProfissao')); ?>:</b>
	<?php echo Html::encode($data->IDProfissao); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDEndereco')); ?>:</b>
	<?php echo Html::encode($data->IDEndereco); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('inscricao_mteProfissional')); ?>:</b>
	<?php echo Html::encode($data->inscricao_mteProfissional); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('telefone_profissional')); ?>:</b>
	<?php echo Html::encode($data->telefone_profissional); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('ramal_profissional')); ?>:</b>
	<?php echo Html::encode($data->ramal_profissional); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('celular_profissional')); ?>:</b>
	<?php echo Html::encode($data->celular_profissional); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('email_profissional')); ?>:</b>
	<?php echo Html::encode($data->email_profissional); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('ddd_telefoneProfissional')); ?>:</b>
	<?php echo Html::encode($data->ddd_telefoneProfissional); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('ddd_celularProfissional')); ?>:</b>
	<?php echo Html::encode($data->ddd_celularProfissional); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('receber_emails')); ?>:</b>
	<?php echo Html::encode($data->receber_emails); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('nit_profissional')); ?>:</b>
	<?php echo Html::encode($data->nit_profissional); ?>
	<br />

	*/
    ?>

</div>