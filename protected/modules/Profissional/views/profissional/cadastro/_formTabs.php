<div class="tab-pane row-fluid active" id="tab_pessoal">
    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_profissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'nome_profissional',
                array('class' => 'm-wrap span6', 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'nome_profissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'genero_profissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeDropDownList(
                $model,
                "genero_profissional",
                $model->genero,
                array("class" => "m-wrap span6 select2", 'prompt' => 'Escolha um Gênero')
            ) ?>
            <?php echo $form->error($model, 'genero_profissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'cpf_profissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            $this->widget(
                'CMaskedTextField',
                array(
                    'model' => $model,
                    'attribute' => 'cpf_profissional',
                    'mask' => HTexto::MASK_CPF,
                    'htmlOptions' => array('class' => 'span6 m-wrap')
                )
            );
            ?>
            <?php echo $form->error($model, 'cpf_profissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'rg_profissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'rg_profissional',
                array('class' => 'm-wrap span6', 'maxlength' => 15)
            ); ?>
            <?php echo $form->error($model, 'rg_profissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'orgao_expedidor_Profissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'orgao_expedidor_Profissional',
                array('class' => 'm-wrap span2', 'maxlength' => 10)
            ); ?>
            <?php echo $form->error($model, 'orgao_expedidor_Profissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx(
            $model,
            'uf_rg_Profissinal',
            array('class' => 'control-label')
        ); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                "uf_rg_Profissinal",
                Html::listData(Estado::model()->findAll(), 'IDEstado', 'sigla_estado'),
                array('class' => 'select2 span3')
            ); ?>
            <?php echo $form->error(
                $model,
                'uf_rg_Profissinal',
                array('class' => 'help-inline')
            ); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDProfissao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeDropDownList(
                $model,
                "IDProfissao",
                Html::listData(Profissao::model()->findAll(), 'IDProfissao', 'nome_profissao'),
                array("class" => "m-wrap span6 select2", 'prompt' => 'Escolha uma Profissão')
            ) ?>
            <?php echo $form->error($model, 'IDProfissao', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="tab-pane row-fluid" id="tab_profissao">
</div>

<div class="tab-pane row-fluid" id="tab_contato">
    <div class="control-group">
        <?php echo $form->labelEx($model, 'telefone_profissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            $this->widget(
                'application.widgets.WCampoTelefone',
                array(
                    'model' => $model,
                    'form' => $form,
                    'atributoDDD' => 'ddd_telefoneProfissional',
                    'atributoTel' => 'telefone_profissional',
                    'atributoRamal' => 'ramal_profissional',
                )
            )
            ?>
            <?php echo $form->error($model, 'ddd_telefoneProfissional', array('class' => 'help-inline')); ?>
            <?php echo $form->error($model, 'telefone_profissional', array('class' => 'help-inline')); ?>
            <?php echo $form->error($model, 'ramal_profissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'celular_profissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            $this->widget(
                'application.widgets.WCampoTelefone',
                array(
                    'model' => $model,
                    'form' => $form,
                    'html_optionsTel' => array('placeholder' => 'Celular'),
                    'atributoDDD' => 'ddd_celularProfissional',
                    'atributoTel' => 'celular_profissional',
                )
            )
            ?>
            <?php echo $form->error($model, 'ddd_celularProfissional', array('class' => 'help-inline')); ?>
            <?php echo $form->error($model, 'celular_profissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'email_profissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'email_profissional',
                array('class' => 'm-wrap span6', 'maxlength' => 60)
            ); ?>
            <?php echo $form->error($model, 'email_profissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'receber_emails', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->checkBox($model, 'receber_emails'); ?>
            <?php echo $form->error($model, 'receber_emails', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="tab-pane row-fluid" id="tab_bancarios">
    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDBanco', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeDropDownList(
                $model,
                "IDBanco",
                Html::listData(Banco::model()->findAll(), 'IDBanco', 'nome_banco'),
                array("class" => "m-wrap span6 select2", 'prompt' => 'Escolha um Banco')
            ) ?>
            <?php echo $form->error($model, 'IDBanco', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'agencia_profissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'agencia_profissional',
                array('class' => 'm-wrap span6', 'maxlength' => 10)
            ); ?>
            <?php echo $form->error($model, 'agencia_profissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'conta_profissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'conta_profissional',
                array('class' => 'm-wrap span6', 'maxlength' => 10)
            ); ?>
            <?php echo $form->error($model, 'conta_profissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'variacao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'variacao',
                array('class' => 'm-wrap span6', 'maxlength' => 3)
            ); ?>
            <?php echo $form->error($model, 'variacao', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="tab-pane row-fluid" id="tab_endereco">
    <?php
    $formEndereco = $this->widget(
        'application.widgets.WFormEndereco',
        array(
            'endereco' => $endereco,
            'form' => $form,
            'label' => $model->attributeLabels()['IDEndereco'],
            'id' => '1'
        )
    );
    ?>
</div>

<div class="tab-pane row-fluid" id="tab_empregador">
    <?php
    $checkBoxOptions = array('class' => 'm-wrap span6', 'size' => 1, 'maxlength' => 1);
    if (!Yii::app()->user->getState('todas_empresas')) {
        echo '<div class="span12"><p class="alert span10">Obs: O campo "Todas as Empresas" só pode ser alterado por usuários que tenham acesso a todas as empresas.</p></div>';
        $checkBoxOptions['disabled'] = true;
    } ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'todas_empresasProfissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->checkBox(
                $model,
                'todas_empresasProfissional',
                $checkBoxOptions
            ); ?>
            <?php echo $form->error($model, 'todas_empresasProfissional', array('class' => 'help-inline')); ?>
        </div>
    </div>


    <?php
    if ($model->isNewRecord) {
        $empregadores = Yii::app()->session['empregadores'];
    } else {
        $empregadores = Html::listData($profEmpregador, 'IDProfissionalEmpregador', 'iDEmpregador.nome_empregador');
        echo Html::tag(
            'p',
            array('class' => 'alert alert-danger'),
            'Aqui, você só pode <u>desativar</u> as relações de empregadores com este profissional.
             Para outra operações envolvendo estas relações, clique ' . '<a target="_blank" href="' . Yii::app(
            )->createUrl("Profissional/profissionalEmpregador/search") . '">aqui</a>.'
        );
    } ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'profissionalEmpregadors', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            echo Html::activeDropDownList(
                $model,
                "profissionalEmpregadors",
                $empregadores,
                array("class" => "m-wrap span6 select2", 'multiple' => true)
            )?>
            <?php echo $form->error($model, 'profissionalEmpregadors', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <?php if ($model->isNewRecord): ?>
        <div class="control-group">
            <?php echo $form->labelEx($profEmpregador, 'dt_inicio', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $profEmpregador,
                    'dt_inicio',
                    array('class' => 'm-wrap date-picker', 'maxlength' => 10)
                ); ?>
                <?php echo $form->error($profEmpregador, 'dt_inicio', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($profEmpregador, 'dt_fim', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $profEmpregador,
                    'dt_fim',
                    array('class' => 'm-wrap date-picker', 'maxlength' => 10)
                ); ?>
                <?php echo $form->error($profEmpregador, 'dt_fim', array('class' => 'help-inline')); ?>
            </div>
        </div>
    <?php endif; ?>
</div>


