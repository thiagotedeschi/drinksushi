<?php
/* @var $this LocalController */
/* @var $model Local */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));



?>


<?php $this->renderPartial(
    '_form',
    array(
        'model' => $model,
        'endereco' => $endereco,
        'horario' => $horario,
        'localHorario' => $localHorario,
    )
); ?>