<?php
/* @var $this LocalController */
/* @var $model Local */
/* @var $endereco Endereco */
/* @var $horario Horario */
/* @var $localHorario LocalHorario */
/* @var $form CActiveForm */

if (!$model->isNewRecord) {
    $nr_horarios = count($localHorario);
}
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'local-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_local', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'nome_local', array('class' => 'm-wrap span6', 'maxlength' => 50)); ?>
            <?php echo $form->error($model, 'nome_local', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <?php
    $formEndereco = $this->widget(
        'application.widgets.WFormEndereco',
        array(
            'endereco' => $endereco,
            'form' => $form,
            'label' => $model->attributeLabels()['IDEndereco'],
            'id' => '1'
        )
    );
    ?>

    <div class="control-group" id="divHorarios">
        <?php echo Html::label('Número de Horários', 'nr_horarios', array('class' => 'control-label')) ?>
        <div class="controls">
            <?php echo Html::numberField(
                'nr_horarios',
                (isset($nr_horarios) ? $nr_horarios : '1'),
                array('class' => 'span6 m-wrap')
            ) ?>
        </div>
        <div id="horariosGerados">
            <?php
            if ($model->isNewRecord) {
                $formHorario = $this->widget(
                    'application.widgets.WFormHorario',
                    array(
                        'horario' => $horario,
                        'localHorario' => $localHorario,
                        'form' => $form,
                        'label' => 'Horário',
                        'id' => '1'
                    )
                );
            } else {
                $i = 0;
                foreach ($localHorario as $key => $objLocalHorario) {
                    $formHorario[] = $this->widget(
                        'application.widgets.WFormHorario',
                        array(
                            'horario' => $objLocalHorario->iDHorario,
                            'localHorario' => $objLocalHorario,
                            'form' => $form,
                            'label' => 'Horário',
                            'id' => $i,
                        )
                    );
                    $i++;
                }
            }
            ?>
        </div>
    </div>

</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<script>


    $("#nr_horarios").change(function () {
        var tam = $("#horariosGerados fieldset").length;
        var nr_horarios = $("#nr_horarios").val();

        if ($(this).val() <= 0) {
            $(this).val(1);
            return false;
        }

        if (tam < nr_horarios)
            $.ajax({
                url: "<?=CController::createUrl('local/horario')?>",
                type: "POST",
                data: {
                    i: $("#horariosGerados fieldset").length,
                },
                beforeSend: function () {
                    $("#nr_horarios").attr('disabled', true);
                    $("#divHorarios").addClass('grid-view-loading');
                },
                success: function (html) {
                    var div = $("#horariosGerados");
                    $(html).appendTo(div);
                    var i = $("#horariosGerados fieldset").length;
                    //'Reiniciar' os js de alguns plugins
                    $('#Horario_' + i + '_diaSemana').select2();
                    $(".time-picker").timepicker({
                        format: "hh:mm",
                        showMeridian: false,
                        showSeconds: false,
                        defaultTime: false
                    });
                    $(".date-picker").datepicker({
                        format: "dd/mm/yyyy",
                        language: 'pt-BR',
                        autoClose: true
                    });
                },
                complete: function () {
                    $("#nr_horarios").attr('disabled', false);
                    $("#divHorarios").removeClass('grid-view-loading');
                }
            });
        else if (tam > nr_horarios) {
            while (tam > nr_horarios) {
                $('#horariosGerados fieldset:last').remove();
                tam = tam - 1;
            }
        }
    });
</script>
<?php $this->endWidget(); ?>
