<?php
/* @var $this LocalController */
/* @var $data Local */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDLocal')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDLocal), array('view', 'id' => $data->IDLocal)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_local')); ?>:</b>
    <?php echo Html::encode($data->nome_local); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDEndereco')); ?>:</b>
    <?php echo Html::encode($data->IDEndereco); ?>
    <br/>


</div>