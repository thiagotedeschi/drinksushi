<?php
/* @var $this LocalController */
/* @var $model Local */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


$horarios = '';
foreach ($model->localHorarios(
             array(
                 'condition' => 'NOT "folga" AND "dt_exclusao" IS NULL AND ("dt_fim" > \'' . HData::hoje(
                         HData::SQL_DATE_FORMAT
                     ) . '\' OR "dt_fim" IS NULL)'
             )
         ) as $localHorario) {
    $horarios .= $localHorario->iDHorario;
}

$horariosFim = '';
foreach ($model->localHorarios(
             array(
                 'condition' => 'NOT "folga" AND "dt_exclusao" IS NULL AND "dt_fim" >= \'' . HData::hoje(
                         HData::SQL_DATE_FORMAT
                     ) . "'"
             )
         ) as $localHorario) {
    $horariosFim .= '<ul>* ' . HData::formataData(
            $localHorario->dt_inicio,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATE_FORMAT
        ) . ' à ' . HData::formataData(
            $localHorario->dt_fim,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATE_FORMAT
        ) . ':' . $localHorario->iDHorario . '</ul>';
}

$horariosExcluidos = '';
foreach ($model->localHorarios(array('condition' => 'NOT "folga" AND "dt_exclusao" IS NOT NULL')) as $localHorario) {
    $horariosExcluidos .= '<ul>* ' . HData::formataData(
            $localHorario->dt_inicio,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATE_FORMAT
        ) . ' à ' . HData::formataData(
            $localHorario->dt_exclusao,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATETIME_FORMAT
        ) . ':' . $localHorario->iDHorario . '</ul>';
}

$folgas = '';
foreach ($model->localHorarios(array('condition' => '"folga" AND "dt_exclusao" IS NULL')) as $localHorario) {
    $folgas .= '<ul>* ' . HData::formataData(
            $localHorario->dt_inicio,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATE_FORMAT
        ) . ' à ' . HData::formataData(
            $localHorario->dt_fim,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATE_FORMAT
        ) . ':' . $localHorario->iDHorario . '</ul>';
}

$folgasExcluidas = '';
foreach ($model->localHorarios(array('condition' => '"folga" AND "dt_exclusao" IS NOT NULL')) as $localHorario) {
    $folgasExcluidas .= '<ul>* ' . HData::formataData(
            $localHorario->dt_inicio,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATE_FORMAT
        ) . ' à ' . HData::formataData(
            $localHorario->dt_exclusao,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATETIME_FORMAT
        ) . ':' . $localHorario->iDHorario . '</ul>';
}

?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDLocal',
            'nome_local',
            'IDEndereco' => array(
                'label' => $model->attributeLabels()['IDEndereco'],
                'type' => 'raw',
                'value' => $model->iDEndereco
            ),
            'Horario' => array(
                'label' => 'Horários Vigentes',
                'type' => 'raw',
                'value' => $horarios
            ),
            'HorarioFim' => array(
                'label' => 'Horários Terminados',
                'type' => 'raw',
                'value' => $horariosFim
            ),
            'HorarioExc' => array(
                'label' => 'Horários Excluídos',
                'type' => 'raw',
                'value' => $horariosExcluidos
            ),
            'Folga' => array(
                'label' => 'Folgas',
                'type' => 'raw',
                'value' => $folgas
            ),
            'FolgaExcluida' => array(
                'label' => 'Folgas Excluídas',
                'type' => 'raw',
                'value' => $folgasExcluidas
            )
        ),
    )
); ?>
