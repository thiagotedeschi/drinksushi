<?php
/* @var $this SalarioProfissionalController */
/* @var $data SalarioProfissional */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDSalarioProfissional')); ?>:</b>
    <?php echo Html::link(
        Html::encode($data->IDSalarioProfissional),
        array('view', 'id' => $data->IDSalarioProfissional)
    ); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_inicioSalarioProfissional')); ?>:</b>
    <?php echo Html::encode($data->dt_inicioSalarioProfissional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_fimSalarioProfissional')); ?>:</b>
    <?php echo Html::encode($data->dt_fimSalarioProfissional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_desativacaoSalarioProfissional')); ?>:</b>
    <?php echo Html::encode($data->dt_desativacaoSalarioProfissional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDProfissional')); ?>:</b>
    <?php echo Html::encode($data->IDProfissional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('valor_salarioProfissional')); ?>:</b>
    <?php echo Html::encode($data->valor_salarioProfissional); ?>
    <br/>


</div>