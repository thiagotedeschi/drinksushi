<?php
/* @var $this SalarioProfissionalController */
/* @var $model SalarioProfissional */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'salario-profissional-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_inicioSalarioProfissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'dt_inicioSalarioProfissional',
                array('class' => 'm-wrap span6 date-picker')
            ); ?>
            <?php echo $form->error($model, 'dt_inicioSalarioProfissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_fimSalarioProfissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'dt_fimSalarioProfissional',
                array('class' => 'm-wrap span6 date-picker')
            ); ?>
            <?php echo $form->error($model, 'dt_fimSalarioProfissional', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDProfissional', array('class' => 'control-label')); ?>
        <?php
        $profissionais = Html::listData(
            Profissional::model()->findAll(),
            'IDProfissional',
            'labelProfissional'
        );
        ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDProfissional',
                $profissionais,
                array('class' => 'select2', 'maxlength' => 1, 'prompt' => 'Selecione o Profissional')
            ); ?>
            <?php echo $form->error($model, 'IDProfissional', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'valor_salarioProfissional', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'valor_salarioProfissional',
                array('class' => 'm-wrap span6 formata_moeda')
            ); ?>
            <?php echo $form->error($model, 'valor_salarioProfissional', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Adicionais: </label>

        <div class="controls">
            <?php
            $adicionais = array();
            if (!$model->isNewRecord) {
                $adicionais = Html::listData($model->Adicionals(), 'IDAdicional', 'nome_adicional');
            }
            //var_dump($resps);
            ?>
            <?=
            Html::dropDownList(
                "Adicionais",
                array_keys($adicionais),
                Html::listData(Adicional::model()->findAll(), 'IDAdicional', 'nome_adicional'),
                array('class' => 'select2', 'style' => '', 'multiple' => 'multiple')
            ) ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
