<?php
/* @var $this SalarioProfissionalController */
/* @var $model SalarioProfissional */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php $this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'salario-profissional-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDSalarioProfissional',
            array(
                'name' => 'dt_inicioSalarioProfissional',
                'filter' => '',
                'type' => 'date',
            ),
            array(
                'name' => 'dt_fimSalarioProfissional',
                'filter' => '',
                'type' => 'date',
            ),
            /*
            'dt_desativacaoSalarioProfissional',
            'IDProfissional',
            */
            array(
                'name' => 'valor_salarioProfissional',
                'value' => 'HTexto::formataMoeda(HTexto::MOEDA_BRL,$data->valor_salarioProfissional)',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
); ?>
