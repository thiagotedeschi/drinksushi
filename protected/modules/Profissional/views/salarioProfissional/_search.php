<?php
/* @var $this SalarioProfissionalController */
/* @var $model SalarioProfissional */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDSalarioProfissional'); ?>
        <?php echo $form->textField($model, 'IDSalarioProfissional', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_inicioSalarioProfissional'); ?>
        <?php echo $form->textField($model, 'dt_inicioSalarioProfissional', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_fimSalarioProfissional'); ?>
        <?php echo $form->textField($model, 'dt_fimSalarioProfissional', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_desativacaoSalarioProfissional'); ?>
        <?php echo $form->textField($model, 'dt_desativacaoSalarioProfissional', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDProfissional'); ?>
        <?php echo $form->textField($model, 'IDProfissional', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'valor_salarioProfissional'); ?>
        <?php echo $form->textField($model, 'valor_salarioProfissional', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->