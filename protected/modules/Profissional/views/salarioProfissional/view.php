<?php
/* @var $this SalarioProfissionalController */
/* @var $model SalarioProfissional */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

$profissionals = Profissional::model()->findAllByAttributes(['IDProfissional' => $model->IDProfissional]);
$profissionals = array_shift($profissionals);
?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDSalarioProfissional',
            'dt_inicioSalarioProfissional' => array(
                'value' => HData::sqlToBr($model->dt_inicioSalarioProfissional),
                'label' => 'Data Início Salário Profissional'
            ),
            'dt_fimSalarioProfissional' => array(
                'value' => HData::sqlToBr($model->dt_fimSalarioProfissional),
                'label' => 'Data Fim Salário Profissional'
            ),
            'dt_desativacaoSalarioProfissional',
            'IDProfissional' => array('value' => $profissionals->labelProfissional, 'label' => 'Profissional'),
            'valor_salarioProfissional' => array(
                'value' => HTexto::formataMoeda(
                        HTexto::MOEDA_BRL,
                        $model->valor_salarioProfissional
                    ),
                'label' => 'Valor do salário do profissional'
            ),
            'adicionais' => array('value' => implode(', ', $model->Adicionals), 'label' => 'Adicionais'),
        ),

    )
); ?>
