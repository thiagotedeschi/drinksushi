<?php
/* @var $this RemuneracaoVariavelController */
/* @var $model RemuneracaoVariavel */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDRemuneracaoVariavel',
            'nome_remuneracaoVariavel',
            'desc_remuneracaoVariavel',
        ),
    )
); ?>
