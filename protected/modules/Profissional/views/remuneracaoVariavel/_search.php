<?php
/* @var $this RemuneracaoVariavelController */
/* @var $model RemuneracaoVariavel */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDRemuneracaoVariavel'); ?>
        <?php echo $form->textField($model, 'IDRemuneracaoVariavel', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_remuneracaoVariavel'); ?>
        <?php echo $form->textField(
            $model,
            'nome_remuneracaoVariavel',
            array('class' => 'm-wrap span6', 'maxlength' => 50)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_remuneracaoVariavel'); ?>
        <?php echo $form->textField(
            $model,
            'desc_remuneracaoVariavel',
            array('class' => 'm-wrap span6', 'maxlength' => 255)
        ); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->