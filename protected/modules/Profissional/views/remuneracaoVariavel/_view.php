<?php
/* @var $this RemuneracaoVariavelController */
/* @var $data RemuneracaoVariavel */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDRemuneracaoVariavel')); ?>:</b>
    <?php echo Html::link(
        Html::encode($data->IDRemuneracaoVariavel),
        array('view', 'id' => $data->IDRemuneracaoVariavel)
    ); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_remuneracaoVariavel')); ?>:</b>
    <?php echo Html::encode($data->nome_remuneracaoVariavel); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_remuneracaoVariavel')); ?>:</b>
    <?php echo Html::encode($data->desc_remuneracaoVariavel); ?>
    <br/>


</div>