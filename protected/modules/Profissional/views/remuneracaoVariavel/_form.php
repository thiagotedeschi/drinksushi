<?php
/* @var $this RemuneracaoVariavelController */
/* @var $model RemuneracaoVariavel */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'remuneracao-variavel-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_remuneracaoVariavel', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'nome_remuneracaoVariavel',
                array('class' => 'm-wrap span6', 'maxlength' => 50)
            ); ?>
            <?php echo $form->error($model, 'nome_remuneracaoVariavel', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_remuneracaoVariavel', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'desc_remuneracaoVariavel',
                array('class' => 'm-wrap span6', 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'desc_remuneracaoVariavel', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
