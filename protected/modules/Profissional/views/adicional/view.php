<?php
/* @var $this AdicionalController */
/* @var $model Adicional */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDAdicional',
            'nome_adicional',
            'desc_adicional',
            'porcentagem_adicional',
        ),
    )
); ?>
