<?php
/* @var $this AdicionalController */
/* @var $data Adicional */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDAdicional')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDAdicional), array('view', 'id' => $data->IDAdicional)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_adicional')); ?>:</b>
    <?php echo Html::encode($data->nome_adicional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_adicional')); ?>:</b>
    <?php echo Html::encode($data->desc_adicional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('porcentagem_adicional')); ?>:</b>
    <?php echo Html::encode($data->porcentagem_adicional); ?>
    <br/>


</div>