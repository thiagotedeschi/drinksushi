<?php
/* @var $this AdicionalController */
/* @var $model Adicional */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDAdicional'); ?>
        <?php echo $form->textField($model, 'IDAdicional', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_adicional'); ?>
        <?php echo $form->textField($model, 'nome_adicional', array('class' => 'm-wrap span6', 'maxlength' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_adicional'); ?>
        <?php echo $form->textField($model, 'desc_adicional', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'porcentagem_adicional'); ?>
        <?php echo $form->textField($model, 'porcentagem_adicional', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->