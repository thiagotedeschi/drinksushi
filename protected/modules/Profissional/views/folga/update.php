<?php
/* @var $this FolgaController */
/* @var $model Horario */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

if ($model->localHorarios->dt_fim < HData::hoje(HData::SQL_DATE_FORMAT)) {
    throw new CHttpException(403);
}

?>


<?php $this->renderPartial('_form', array('model' => $model, 'localHorario' => $localHorario)); ?>