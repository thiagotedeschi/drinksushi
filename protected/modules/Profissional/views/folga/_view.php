<?php
/* @var $this FolgaController */
/* @var $data Horario */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDHorario')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDHorario), array('view', 'id' => $data->IDHorario)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('inicio_horario')); ?>:</b>
    <?php echo Html::encode($data->inicio_horario); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('fim_horario')); ?>:</b>
    <?php echo Html::encode($data->fim_horario); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('segunda')); ?>:</b>
    <?php echo Html::encode($data->segunda); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('terca')); ?>:</b>
    <?php echo Html::encode($data->terca); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('quarta')); ?>:</b>
    <?php echo Html::encode($data->quarta); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('quinta')); ?>:</b>
    <?php echo Html::encode($data->quinta); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('sexta')); ?>:</b>
	<?php echo Html::encode($data->sexta); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('sabado')); ?>:</b>
	<?php echo Html::encode($data->sabado); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('domingo')); ?>:</b>
	<?php echo Html::encode($data->domingo); ?>
	<br />

	*/
    ?>

</div>