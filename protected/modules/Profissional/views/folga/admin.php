<?php
/* @var $this FolgaController */
/* @var $model Horario */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php $this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'horario-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDHorario',
            'localHorarios.iDLocal' => array(
                'value' => '$data->localHorarios->iDLocal->nome_local',
                'name' => 'nome_local',
                'header' => 'Local',
            ),
            'inicio_horario' => array(
                'value' => 'HData::formataData($data->inicio_horario, HData::BR_TIME_SIMPLE_FORMAT, HData::SQL_TIME_FORMAT)." às ".HData::formataData($data->fim_horario, HData::BR_TIME_SIMPLE_FORMAT, HData::SQL_TIME_FORMAT)',
                'header' => 'Horário',
                'filter' => false
            ),
            /*
            'segunda',
            'terca',
            'quarta',
            'quinta',
            'sexta',
            'sabado',
            'domingo',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao(
                                    'update'
                                ) ? '($data->localHorarios->dt_fim < HData::hoje(HData::SQL_DATE_FORMAT) ? false : true)' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
); ?>
