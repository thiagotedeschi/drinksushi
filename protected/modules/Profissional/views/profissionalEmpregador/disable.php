<?php
/* @var $this ProfissionalEmpregadorController */
/* @var $model ProfissionalEmpregador */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'profissional-empregador-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>

    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com * são obrigatórios.</p>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_desativacao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'dt_desativacao', array('class' => 'date-picker')); ?>
                <?php echo $form->error($model, 'dt_desativacao', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="form-actions">
            <?php echo Html::submitButton('Desativar Associação', array('class' => 'botao')); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>