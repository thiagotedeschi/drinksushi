<?php
/* @var $this ProfissionalEmpregadorController */
/* @var $model ProfissionalEmpregador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

<div id="modal-update" style="" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div style="padding: 10px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <iframe src="" style="width:95%; overflow: hidden; min-height:440px;" class="modal-body">
    </iframe>
</div>

<div id="modal-desativar" style="" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div style="padding: 10px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <iframe src="" style="width:95%; overflow: hidden; min-height:440px;" class="modal-body">
    </iframe>
</div>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada
    pesquisa.
</p>


<?php $this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'profissional-empregador-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            array(
                'header' => 'Empregadoor',
                'name' => 'IDEmpregador',
                'value' => '$data->iDEmpregador->nome_empregador',
                'filter' => ''
            ),
            array(
                'header' => 'Profissional',
                'name' => 'nome_profissional',
                'value' => '$data->iDProfissional->nome_profissional'
            ),
            array(
                'name' => 'dt_inicio',
                'filter' => '',
                'type' => 'date'
            ),
            'ativo' => array(
                'header' => 'Ativo?',
                'name' => 'ativo',
                'value' => '($data->ativo ? "Sim" : "Não")',
                'filter' => array('1' => 'Sim', '0' => 'Não'),
            ),
            /*
            'dt_fim',
            'dt_desativacao',
            'IDProfissionalEmpregador',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{desativar}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false'),
                            'isModal' => true
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                        'desativar' => array(
                            'options' => array(
                                'class' => 'botao desativar',
                                'title' => 'Ativar/Desativar Associação'
                            ),
                            'imageUrl' => false,
                            'label' => '"<i class=\'icon-power-off icon-large\'></i>"',
                            'url' => 'Yii::app()->createUrl("Profissional/profissionalEmpregador/desativar", array("id"=>$data->primaryKey,"in-modal"=>"in-modal"))',
                            'isModal' => true,
                            'visible' => ($this->verificaAcao(
                                    'disable'
                                ) ? '($data->ativo ? true : false)' : 'false')
                        ),
                    ),
            ),
        ),
    )
);

if (!Yii::app()->user->getState('IDEmpregador')) {
    Yii::app()->user->setFlash('error', array('Para visualizar essa página é necessário selecionar um Empregador.'));
}
?>

<script>
    $(document).ready(function () {
        getFlashes();
    })

    function openDesassociarDialog(a) {
        $a = jQuery(a);
        $iframe = $('#desassociarIFrame');
        $wrapper = $("#desassociarWrapper");
        $iframe.attr('src', $a.attr('href'));
        $wrapper.dialog("open");
        return false;
    }
</script>
