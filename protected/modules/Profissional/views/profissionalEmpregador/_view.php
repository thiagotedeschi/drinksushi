<?php
/* @var $this ProfissionalEmpregadorController */
/* @var $data ProfissionalEmpregador */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDProfissionalEmpregador')); ?>:</b>
    <?php echo Html::link(
        Html::encode($data->IDProfissionalEmpregador),
        array('view', 'id' => $data->IDProfissionalEmpregador)
    ); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDEmpregador')); ?>:</b>
    <?php echo Html::encode($data->IDEmpregador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDProfissional')); ?>:</b>
    <?php echo Html::encode($data->IDProfissional); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_inicio')); ?>:</b>
    <?php echo Html::encode($data->dt_inicio); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_fim')); ?>:</b>
    <?php echo Html::encode($data->dt_fim); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_desativacao')); ?>:</b>
    <?php echo Html::encode($data->dt_desativacao); ?>
    <br/>


</div>