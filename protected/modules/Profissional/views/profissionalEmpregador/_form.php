<?php
/* @var $this ProfissionalEmpregadorController */
/* @var $model ProfissionalEmpregador */
/* @var $form CActiveForm */
?>


<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'profissional-empregador-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
$model->dt_inicio = HData::sqlToBr($model->dt_inicio);
$model->dt_fim = HData::sqlToBr($model->dt_fim);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>
    <?php if ($model->isNewRecord): ?>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDProfissional', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                echo $form->dropDownList(
                    $model,
                    'IDProfissional',
                    Html::listData(Profissional::model()->findAll(), 'IDProfissional', 'nome_profissional'),
                    array(
                        'class' => 'm-wrap select2 span6',
                        'prompt' => 'Selecione o Profissional'
                    )
                );
                ?>
                <?php echo $form->error($model, 'IDProfissional', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                echo $form->dropDownList(
                    $model,
                    'IDEmpregador',
                    Html::listData(Empregador::model()->findAll(), 'IDEmpregador', 'nome_empregador'),
                    array(
                        'class' => 'm-wrap select2 span6',
                        'multiple' => true
                    )
                );
                ?>
                <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_inicio', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'dt_inicio', array('class' => 'm-wrap span6 date-picker')); ?>
            <?php echo $form->error($model, 'dt_inicio', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_fim', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'dt_fim', array('class' => 'm-wrap span6 date-picker')); ?>
            <?php echo $form->error($model, 'dt_fim', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
