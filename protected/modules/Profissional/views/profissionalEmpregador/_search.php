<?php
/* @var $this ProfissionalEmpregadorController */
/* @var $model ProfissionalEmpregador */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDEmpregador'); ?>
        <?php echo $form->textField($model, 'IDEmpregador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDProfissional'); ?>
        <?php echo $form->textField($model, 'IDProfissional', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_inicio'); ?>
        <?php echo $form->textField($model, 'dt_inicio', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_fim'); ?>
        <?php echo $form->textField($model, 'dt_fim', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_desativacao'); ?>
        <?php echo $form->textField($model, 'dt_desativacao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDProfissionalEmpregador'); ?>
        <?php echo $form->textField($model, 'IDProfissionalEmpregador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->