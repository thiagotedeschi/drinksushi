<?php
/* @var $this ProfissionalEmpregadorController */
/* @var $model ProfissionalEmpregador */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            array(
                'label' => 'Empregador',
                'type' => 'raw',
                'value' => Html::link(
                        $model->iDEmpregador->nome_empregador,
                        Yii::app()->createUrl(
                            'Empresa/empregador/view',
                            array('id' => $model->IDEmpregador)
                        ),
                        array('target' => '_blank')
                    ),
            ),
            array(
                'label' => 'Profissional',
                'type' => 'raw',
                'value' => Html::link(
                        $model->iDProfissional->nome_profissional,
                        Yii::app()->createUrl(
                            'Profissional/profissional/view',
                            array('id' => $model->IDProfissional)
                        ),
                        array('target' => '_blank')
                    ),
            ),
            'dt_inicio:date',
            'dt_fim:date',
            'dt_desativacao:date',
            'IDProfissionalEmpregador:text:ID Profissional x Empregador',
        ),
    )
); ?>
