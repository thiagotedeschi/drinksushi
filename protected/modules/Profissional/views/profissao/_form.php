<?php
/* @var $this ProfissaoController */
/* @var $model Profissao */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'profissao-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_profissao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'nome_profissao',
                array('class' => 'm-wrap span6', 'maxlength' => 50)
            ); ?>
            <?php echo $form->error($model, 'nome_profissao', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_profissao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'desc_profissao',
                array('class' => 'm-wrap span6', 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'desc_profissao', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'responsabilidadeProfissaos', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'responsabilidadeProfissaos',
                Html::listData(
                    ResponsabilidadeProfissao::model()->findAll(array('order' => '"nome_responsabilidadeProfissao"')),
                    'IDResponsabilidadeProfissao',
                    'nome_responsabilidadeProfissao'
                ),
                array('class' => 'select2', 'multiple' => 'multiple')
            ) ?>
            <?php echo $form->error($model, 'responsabilidadeProfissaos', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
