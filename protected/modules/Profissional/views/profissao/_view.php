<?php
/* @var $this ProfissaoController */
/* @var $data Profissao */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDProfissao')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDProfissao), array('view', 'id' => $data->IDProfissao)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_profissao')); ?>:</b>
    <?php echo Html::encode($data->nome_profissao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_profissao')); ?>:</b>
    <?php echo Html::encode($data->desc_profissao); ?>
    <br/>


</div>