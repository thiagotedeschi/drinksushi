<?php
/* @var $this ProfissaoController */
/* @var $model Profissao */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDProfissao',
            'nome_profissao',
            'desc_profissao',
            'responsabilidade' => array(
                'name' => 'Responsabilidades',
                'type' => 'raw',
                'value' => $model->viewResponsabilidades()
            )
        ),
    )
); ?>
