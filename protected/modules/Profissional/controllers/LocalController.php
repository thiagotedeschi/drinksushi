<?php

/**
 * @package base.Profissional
 */
class LocalController extends Controller
{
    public $acoesActions = array(
        'create' => '236',
        'search' => '237',
        'view' => '238',
        'update' => '239',
        'delete' => '240',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Visualizar #' . $id;
        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Local::model()->labelModel();
        $model = new Local;
        $endereco = new EnderecoProfissional;
        $horario = new Horario;
        $localHorario = new LocalHorario;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Local'], $_POST['EnderecoProfissional'][1], $_POST['LocalHorario'], $_POST['Horario'])) {

            $model->attributes = $_POST['Local'];
            $endereco->attributes = $_POST['EnderecoProfissional'][1];

            if ($endereco->save()) {
                $model->IDEndereco = $endereco->IDEndereco;

                $IDHorarios = array();
                foreach ($_POST['Horario'] as $key => $postHorario) {
                    $horario = new Horario;
                    $horario->attributes = $postHorario;
                    if (isset($postHorario['diaSemana'])) {
                        foreach ($postHorario['diaSemana'] as $dia) {
                            $horario->$dia = true;
                        }
                    }

                    $erro = true;

                    $localHorario->attributes = $_POST['LocalHorario'][$key];

                    if ($localHorario->validate(array('dt_inicio'))) {
                        if ($horario->save()) {
                            $IDHorarios[$key] = $horario->IDHorario;
                            $horario->diaSemana = array(
                                'domingo' => 'Domingo',
                                'segunda' => 'Segunda',
                                'terca' => 'Terça',
                                'quarta' => 'Quarta',
                                'quinta' => 'Quinta',
                                'sexta' => 'Sexta',
                                'sabado' => 'Sabado'
                            );
                            $erro = false;
                        }
                    }

                    if ($erro) {
                        $horario->diaSemana = array(
                            'domingo' => 'Domingo',
                            'segunda' => 'Segunda',
                            'terca' => 'Terça',
                            'quarta' => 'Quarta',
                            'quinta' => 'Quinta',
                            'sexta' => 'Sexta',
                            'sabado' => 'Sabado'
                        );
                        for ($i = 1; $i < $key; $i++) {
                            $horario = Horario::model()->findByPk($IDHorarios[$i]);
                            $horario->diaSemana = array(
                                'domingo' => 'Domingo',
                                'segunda' => 'Segunda',
                                'terca' => 'Terça',
                                'quarta' => 'Quarta',
                                'quinta' => 'Quinta',
                                'sexta' => 'Sexta',
                                'sabado' => 'Sabado'
                            );
                            if ($horario->delete()) {
                                unset($IDHorarios[$i]);
                            }
                        }
                        break;
                    }
                }

                if (count($IDHorarios) > 0) {
                    if ($model->save()) {

                        foreach ($_POST['LocalHorario'] as $key => $postLocalHorario) {
                            $localHorario = new LocalHorario;
                            $localHorario->dt_inicio = $postLocalHorario['dt_inicio'] != '' ? $postLocalHorario['dt_inicio'] : null;
                            $localHorario->dt_fim = $postLocalHorario['dt_fim'] != '' ? $postLocalHorario['dt_fim'] : null;
                            $localHorario->folga = $postLocalHorario['folga'];
                            $localHorario->IDLocal = $model->IDLocal;

                            $localHorario->IDHorario = $IDHorarios[$key];
                            $localHorario->save();
                        }


                        $this->redirect(array('view', 'id' => $model->IDLocal));

                    } else {
                        $horario->diaSemana = array(
                            'domingo' => 'Domingo',
                            'segunda' => 'Segunda',
                            'terca' => 'Terça',
                            'quarta' => 'Quarta',
                            'quinta' => 'Quinta',
                            'sexta' => 'Sexta',
                            'sabado' => 'Sabado'
                        );
                        for ($i = 1; $i < $key; $i++) {
                            $horario = Horario::model()->findByPk($IDHorarios[$i]);
                            $horario->diaSemana = array(
                                'domingo' => 'Domingo',
                                'segunda' => 'Segunda',
                                'terca' => 'Terça',
                                'quarta' => 'Quarta',
                                'quinta' => 'Quinta',
                                'sexta' => 'Sexta',
                                'sabado' => 'Sabado'
                            );
                            if ($horario->delete()) {
                                unset($IDHorarios[$i]);
                            }
                        }
                        $endereco->delete();
                    }
                } else {
                    $endereco->delete();
                }
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
                'endereco' => $endereco,
                'horario' => $horario,
                'localHorario' => $localHorario
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Local::model()->labelModel() . ' #' . $id;

        $model = Local::model()->with('localHorarios:naoeFolga:naoExcluido:ordemDtInicio')->findByPk($id);
        $endereco = $model->iDEndereco;
        $localHorario = $model->localHorarios;
        foreach ($localHorario as $objLocalHorario) {
            $horario[] = $objLocalHorario->iDHorario;
        }

        if (!isset($horario)) {
            $horario = new Horario;
        }

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model, $endereco);

        if (isset($_POST['Local'], $_POST['EnderecoProfissional'][1], $_POST['LocalHorario'], $_POST['Horario'])) {
            $model->attributes = $_POST['Local'];
            $endereco->attributes = $_POST['EnderecoProfissional'][1];
            if ($endereco->save()) {
                $model->IDEndereco = $endereco->IDEndereco;
            }
            $model->attributes = $_POST['Local'];
            if ($model->save()) {
                //Atualiza e exclui os Horários já cadastrados
                foreach ($localHorario as $key => $objLocalHorario) {
                    if (isset($_POST['LocalHorario'][$key])) {
                        $objLocalHorario->attributes = $_POST['LocalHorario'][$key];
                        $objLocalHorario->dt_inicio = $_POST['LocalHorario'][$key]['dt_inicio'] != '' ? $_POST['LocalHorario'][$key]['dt_inicio'] : null;
                        $objLocalHorario->dt_fim = $_POST['LocalHorario'][$key]['dt_fim'] != '' ? $_POST['LocalHorario'][$key]['dt_fim'] : null;

                        $objHorario = $objLocalHorario->iDHorario;
                        $objHorario->attributes = $_POST['Horario'][$key];
                        if (isset($_POST['Horario'][$key]['diaSemana'])) {
                            $objHorario->setDiasFalso();
                            foreach ($_POST['Horario'][$key]['diaSemana'] as $dia) {
                                $objHorario->$dia = true;
                            }
                        }

                        if ($objHorario->save() && $objLocalHorario->save()) {
                            unset($_POST['LocalHorario'][$key]);
                        }
                    } else {
                        $objLocalHorario->dt_inicio = HData::formataData(
                            $objLocalHorario->dt_inicio,
                            HData::BR_DATE_FORMAT,
                            HData::SQL_DATE_FORMAT
                        );
                        $objLocalHorario->dt_fim = HData::formataData(
                            $objLocalHorario->dt_fim,
                            HData::BR_DATE_FORMAT,
                            HData::SQL_DATE_FORMAT
                        );
                        $objLocalHorario->dt_exclusao = HData::hoje(HData::SQL_DATETIME_FORMAT);
                        $objLocalHorario->save();
                    }
                }

                $erro = false;
                //Cria os novos Horários
                foreach ($_POST['LocalHorario'] as $key => $postLocalHorario) {
                    $localHorario = new LocalHorario;
                    $localHorario->dt_inicio = $postLocalHorario['dt_inicio'] != '' ? $postLocalHorario['dt_inicio'] : null;
                    $localHorario->dt_fim = $postLocalHorario['dt_fim'] != '' ? $postLocalHorario['dt_fim'] : null;
                    $localHorario->folga = $postLocalHorario['folga'];

                    $horario = new Horario;
                    $horario->attributes = $_POST['Horario'][$key];
                    if (isset($_POST['Horario'][$key]['diaSemana'])) {
                        foreach ($_POST['Horario'][$key]['diaSemana'] as $dia) {
                            $horario->$dia = true;
                        }
                    }

                    $erro = true;
                    if ($localHorario->validate(array('dt_inicio'))) {
                        if ($horario->save()) {
                            $localHorario->IDLocal = $model->IDLocal;
                            $localHorario->IDHorario = $horario->IDHorario;
                            $localHorario->save();
                            $erro = false;
                        }
                    }
                    if ($erro) {
                        break;
                    }
                }

                if (!$erro) {

                    $this->redirect(array('view', 'id' => $model->IDLocal));
                } else {
                    unset($localHorario);
                    unset($horario);

                    $localHorario = $model->localHorarios;
                    foreach ($localHorario as $objLocalHorario) {
                        $objLocalHorario->iDHorario->diaSemana = array(
                            'domingo' => 'Domingo',
                            'segunda' => 'Segunda',
                            'terca' => 'Terça',
                            'quarta' => 'Quarta',
                            'quinta' => 'Quinta',
                            'sexta' => 'Sexta',
                            'sabado' => 'Sabado'
                        );
                        $horario[] = $objLocalHorario->iDHorario;
                    }
                }
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'endereco' => $endereco,
                'horario' => $horario,
                'localHorario' => $localHorario,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                foreach ($model->localHorarios as $localHorario) {
                    $localHorario->iDHorario->delete();
                }
                $model->delete();
            }
        } else {
            foreach ($model->localHorarios as $localHorario) {
                $localHorario->iDHorario->delete();
            }
            $model->delete();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Local::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Local('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Local'])) {
            $model->attributes = $_GET['Local'];
        }

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Local the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Local::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Local $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'local-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionHorario()
    {
        $horario = new Horario;
        $localHorario = new LocalHorario;

        $i = $_POST['i'] + 1;

        $this->widget(
            'application.widgets.WFormHorario',
            array(
                'horario' => $horario,
                'localHorario' => $localHorario,
                'label' => 'Horário',
                'id' => $i
            )
        );
    }
}
