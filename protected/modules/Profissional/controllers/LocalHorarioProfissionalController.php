<?php

/**
 * @package base.Profissional
 */
class LocalHorarioProfissionalController extends Controller
{

    public $acoesActions = array(
        'create' => '288',
        'search' => '290',
        'view' => '291',
        'update' => '289',
        'delete' => '292',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . LocalProfissionalHorario::model()->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . LocalProfissionalHorario::model()->labelModel();
        $model = new Horario;
        $localHorarioProfissional = new LocalProfissionalHorario;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model, $localHorarioProfissional);

        if (isset($_POST['Horario'], $_POST['LocalProfissionalHorario'])) {
            $localHorarioProfissional->attributes = $_POST['LocalProfissionalHorario'];
            $model->attributes = $_POST['Horario'];
            if (isset($_POST['Horario']['diaSemana'])) {
                foreach ($_POST['Horario']['diaSemana'] as $dia) {
                    $model->$dia = true;
                }
            }
            if ($model->validate() && $localHorarioProfissional->validate()) {
                $model->save();
                $localHorarioProfissional->IDHorario = $model->IDHorario;
                $localHorarioProfissional->dt_inicio = $_POST['LocalProfissionalHorario']['dt_inicio'] != '' ? $_POST['LocalProfissionalHorario']['dt_inicio'] : null;
                $localHorarioProfissional->dt_fim = $_POST['LocalProfissionalHorario']['dt_fim'] != '' ? $_POST['LocalProfissionalHorario']['dt_fim'] : null;
                if ($localHorarioProfissional->save()) {
                    $this->redirect(array('search', 'id' => $model->IDHorario));
                }
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
                'localHorarioProfissional' => $localHorarioProfissional
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . LocalProfissionalHorario::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);
        $localHorarioProfissional = $model->localProfissionalHorarios;


        $this->performAjaxValidation($model, $localHorarioProfissional);

        if (isset($_POST['Horario'], $_POST['LocalProfissionalHorario'])) {
            $localHorarioProfissional->attributes = $_POST['LocalProfissionalHorario'];
            $model->attributes = $_POST['Horario'];
            if (isset($_POST['Horario']['diaSemana'])) {
                $model->setDiasFalso();
                foreach ($_POST['Horario']['diaSemana'] as $dia) {
                    $model->$dia = true;
                }
            }
            if ($model->validate() && $localHorarioProfissional->validate()) {
                $model->save();
                $localHorarioProfissional->IDHorario = $model->IDHorario;
                $localHorarioProfissional->dt_inicio = $_POST['LocalProfissionalHorario']['dt_inicio'] != '' ? $_POST['LocalProfissionalHorario']['dt_inicio'] : null;
                $localHorarioProfissional->dt_fim = $_POST['LocalProfissionalHorario']['dt_fim'] != '' ? $_POST['LocalProfissionalHorario']['dt_fim'] : null;
                if ($localHorarioProfissional->save()) {
                    $this->redirect(array('search', 'id' => $model->IDHorario));
                }
            }
            $model->diaSemana = array(
                'domingo' => 'Domingo',
                'segunda' => 'Segunda',
                'terca' => 'Terça',
                'quarta' => 'Quarta',
                'quinta' => 'Quinta',
                'sexta' => 'Sexta',
                'sabado' => 'Sábado'
            );
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'localHorarioProfissional' => $localHorarioProfissional
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $model = $this->loadModel($id);
                $model->localProfissionalHorarios->dt_exclusao = HData::hoje(HData::SQL_DATETIME_FORMAT);
                $model->localProfissionalHorarios->dt_inicio = HData::formataData(
                    $model->localProfissionalHorarios->dt_inicio,
                    HData::BR_DATE_FORMAT,
                    HData::SQL_DATE_FORMAT
                );
                $model->localProfissionalHorarios->dt_fim = HData::formataData(
                    $model->localProfissionalHorarios->dt_fim,
                    HData::BR_DATE_FORMAT,
                    HData::SQL_DATE_FORMAT
                );
                $model->localProfissionalHorarios->save();
            }
        } else {
            $model = $this->loadModel($id);
            $model->localProfissionalHorarios->dt_exclusao = HData::hoje(HData::SQL_DATETIME_FORMAT);
            $model->localProfissionalHorarios->dt_inicio = HData::formataData(
                $model->localProfissionalHorarios->dt_inicio,
                HData::BR_DATE_FORMAT,
                HData::SQL_DATE_FORMAT
            );
            $model->localProfissionalHorarios->dt_fim = HData::formataData(
                $model->localProfissionalHorarios->dt_fim,
                HData::BR_DATE_FORMAT,
                HData::SQL_DATE_FORMAT
            );
            $model->localProfissionalHorarios->save();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . LocalProfissionalHorario::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Horario('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Horario'])) {
            $model->attributes = $_GET['Horario'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Horario the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Horario::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Horario $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'horario-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAjaxConfereHorarioLocalProfissional()
    {
        $data = "";

        if (isset($_POST['diaSemana']) && isset($_POST['inicio_horario']) && isset($_POST['fim_horario']) && isset($_POST['IDLocal'])) {
            if (is_array($_POST['diaSemana'])) {
                $diaSemana = $_POST['diaSemana'];
                $inicio_horario = $_POST['inicio_horario'];
                $fim_horario = $_POST['fim_horario'];
                $IDLocal = $_POST['IDLocal'];

                $localHorario = LocalHorario::model()->findAllByAttributes(
                    array('IDLocal' => $IDLocal)
                );

                $flagSegunda = false;
                $flagTerca = false;
                $flagQuarta = false;
                $flagQuinta = false;
                $flagSexta = false;
                $flagSabado = false;
                $flagDomingo = false;

                foreach ($localHorario as $localH) {
                    $horario = Horario::model()->findByPk($localH->IDHorario);

                    if (in_array("segunda", $diaSemana)) {
                        if ($horario->segunda) {
                            $flagSegunda = true;
                            if (!HData::VerificaHorarioIntervalo(
                                $inicio_horario,
                                $fim_horario,
                                $horario->inicio_horario,
                                $horario->fim_horario
                            )
                            ) {
                                $data .= "<b>Segunda:</b> Você está cadastrando um horário fora do expediente da empresa.<br>";
                            }
                        }
                    }
                    if (in_array("terca", $diaSemana)) {
                        if ($horario->terca) {
                            $flagTerca = true;
                            if (!HData::VerificaHorarioIntervalo(
                                $inicio_horario,
                                $fim_horario,
                                $horario->inicio_horario,
                                $horario->fim_horario
                            )
                            ) {
                                $data .= "<b>Terça:</b> Você está cadastrando um horário fora do expediente da empresa. <br>";
                            }
                        }
                    }
                    if (in_array("quarta", $diaSemana)) {
                        if ($horario->quarta) {
                            $flagQuarta = true;
                            if (!HData::VerificaHorarioIntervalo(
                                $inicio_horario,
                                $fim_horario,
                                $horario->inicio_horario,
                                $horario->fim_horario
                            )
                            ) {
                                $data .= "<b>Quarta:</b> Você está cadastrando um horário fora do expediente da empresa. <br>";
                            }
                        }
                    }
                    if (in_array("quinta", $diaSemana)) {
                        if ($horario->quinta) {
                            $flagQuinta = true;
                            if (!HData::VerificaHorarioIntervalo(
                                $inicio_horario,
                                $fim_horario,
                                $horario->inicio_horario,
                                $horario->fim_horario
                            )
                            ) {
                                $data .= "<b>Quinta:</b> Você está cadastrando um horário fora do expediente da empresa. <br>";
                            }
                        }
                    }
                    if (in_array("sexta", $diaSemana)) {
                        if ($horario->sexta) {
                            $flagSexta = true;
                            if (!HData::VerificaHorarioIntervalo(
                                $inicio_horario,
                                $fim_horario,
                                $horario->inicio_horario,
                                $horario->fim_horario
                            )
                            ) {
                                $data .= "<b>Sexta:</b> Você está cadastrando um horário fora do expediente da empresa. <br>";
                            }
                        }
                    }
                    if (in_array("sabado", $diaSemana)) {
                        if ($horario->sabado) {
                            $flagSabado = true;
                            if (!HData::VerificaHorarioIntervalo(
                                $inicio_horario,
                                $fim_horario,
                                $horario->inicio_horario,
                                $horario->fim_horario
                            )
                            ) {
                                $data .= "<b>Sábado:</b> Você está cadastrando um horário fora do expediente da empresa. <br>";
                            }
                        }
                    }
                    if (in_array("domingo", $diaSemana)) {
                        if ($horario->domingo) {
                            $flagDomingo = true;
                            if (!HData::VerificaHorarioIntervalo(
                                $inicio_horario,
                                $fim_horario,
                                $horario->inicio_horario,
                                $horario->fim_horario
                            )
                            ) {
                                $data .= "<b>Domingo:</b> Você está cadastrando um horário fora do expediente da empresa. <br>";
                            }
                        }
                    }

                }
                if (in_array("segunda", $diaSemana)) {
                    if (!$flagSegunda) {
                        $data .= "A empresa não tem expediente na <b>segunda</b> <br>";
                    }
                }
                if (in_array("terca", $diaSemana)) {
                    if (!$flagTerca) {
                        $data .= "A empresa não tem expediente na <b>terça</b> <br>";
                    }
                }
                if (in_array("quarta", $diaSemana)) {
                    if (!$flagQuarta) {
                        $data .= "A empresa não tem expediente na <b>quarta</b> <br>";
                    }
                }
                if (in_array("quinta", $diaSemana)) {
                    if (!$flagQuinta) {
                        $data .= "A empresa não tem expediente na <b>quinta</b> <br>";
                    }
                }
                if (in_array("sexta", $diaSemana)) {
                    if (!$flagSexta) {
                        $data .= "A empresa não tem expediente na <b>sexta</b> <br>";
                    }
                }
                if (in_array("sabado", $diaSemana)) {
                    if (!$flagSabado) {
                        $data .= "A empresa não tem expediente no <b>sábado</b> <br>";
                    }
                }
                if (in_array("domingo", $diaSemana)) {
                    if (!$flagDomingo) {
                        $data .= "A empresa não tem expediente no <b>domingo</b> <br>";
                    }
                }
            }
        }

        echo CJSON::encode($data);
    }

}
