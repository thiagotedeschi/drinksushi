<?php

/**
 * Controller do CRUD ProfissionalEmpregador
 *
 * @package base.Profissional
 */
class ProfissionalEmpregadorController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '434',
        'search' => '435',
        'view' => '438',
        'update' => '436',
        'delete' => '440',
        'desativar' => ''
    );


    public function actionDesativar($id)
    {
        $model = $this->loadModel($id);
        $model->scenario = 'desativacao';

        $this->performAjaxValidation($model);
        $this->pageTitle = 'Desativar ' . $model->labelModel() . " #" . $model->IDProfissionalEmpregador;

        if (isset($_POST['ProfissionalEmpregador'])) {
            $model->attributes = $_POST['ProfissionalEmpregador'];
            if ($model->save()) {
                $this->redirect('admin');
            }
        }

        $this->render(
            'disable',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . ProfissionalEmpregador::model()->labelModel();
        $model = new ProfissionalEmpregador;

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['ProfissionalEmpregador'])) {
            $model->attributes = $_POST['ProfissionalEmpregador'];
            if ($model->validate()) {
                if (!$model->dt_fim) {
                    $model->dt_fim = null;
                }
                foreach ($_POST['ProfissionalEmpregador']['IDEmpregador'] as $IDEmpregador) {
                    $profEmp = new ProfissionalEmpregador;
                    $profEmp->attributes = $model->attributes;
                    $profEmp->IDEmpregador = $IDEmpregador;
                    $profEmp->save();
                }
                $this->redirect(array('search', 'id' => $model->IDProfissionalEmpregador));
            }
        }


        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . ProfissionalEmpregador::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['ProfissionalEmpregador'])) {
            $model->attributes = $_POST['ProfissionalEmpregador'];
            if (!$model->dt_fim) {
                $model->dt_fim = null;
            }
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->IDProfissionalEmpregador));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . ProfissionalEmpregador::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new ProfissionalEmpregador('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['ProfissionalEmpregador'])) {
            $model->attributes = $_GET['ProfissionalEmpregador'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ProfissionalEmpregador the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ProfissionalEmpregador::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ProfissionalEmpregador $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'profissional-empregador-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
