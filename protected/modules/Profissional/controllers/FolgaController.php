<?php
/**
 * @package base.Profissional
 */
class FolgaController extends Controller
{
    public $acoesActions = array(
        'create' => '251',
        'search' => '252',
        'view' => '253',
        'update' => '254',
        'delete' => '255',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Visualizar #' . $id;
        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . 'Folga';
        $model = new Horario;
        $localHorario = new LocalHorario;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model, $localHorario);

        if (isset($_POST['Horario'], $_POST['LocalHorario'])) {
            $model->attributes = $_POST['Horario'];
            if (isset($_POST['Horario']['diaSemana'])) {
                foreach ($_POST['Horario']['diaSemana'] as $dia) {
                    $model->$dia = true;
                }
            }

            if ($model->save()) {
                $localHorario->IDHorario = $model->IDHorario;
                $localHorario->IDLocal = $_POST['LocalHorario']['IDLocal'];
                $localHorario->folga = true;
                $localHorario->dt_inicio = $_POST['LocalHorario']['dt_inicio'] != '' ? $_POST['LocalHorario']['dt_inicio'] : null;
                $localHorario->dt_fim = $_POST['LocalHorario']['dt_fim'] != '' ? $_POST['LocalHorario']['dt_fim'] : null;

                if ($localHorario->save()) {

                    $this->redirect(array('view', 'id' => $model->IDHorario));
                } else {
                    $model->delete();
                }
            }

            $model->diaSemana = array(
                'domingo' => 'Domingo',
                'segunda' => 'Segunda',
                'terca' => 'Terça',
                'quarta' => 'Quarta',
                'quinta' => 'Quinta',
                'sexta' => 'Sexta',
                'sabado' => 'Sábado'
            );
        }

        $this->render(
            'create',
            array(
                'model' => $model,
                'localHorario' => $localHorario
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . 'Folga' . ' #' . $id;
        $model = $this->loadModel($id);
        $localHorario = $model->localHorarios;

        $this->performAjaxValidation($model, $localHorario);

        if (isset($_POST['Horario'], $_POST['LocalHorario'])) {
            $model->attributes = $_POST['Horario'];
            if (isset($_POST['Horario']['diaSemana'])) {
                $model->setDiasFalso();
                foreach ($_POST['Horario']['diaSemana'] as $dia) {
                    $model->$dia = true;
                }
            }

            if ($model->save()) {
                $localHorario->IDHorario = $model->IDHorario;
                $localHorario->IDLocal = $_POST['LocalHorario']['IDLocal'];
                $localHorario->dt_inicio = $_POST['LocalHorario']['dt_inicio'] != '' ? $_POST['LocalHorario']['dt_inicio'] : null;
                $localHorario->dt_fim = $_POST['LocalHorario']['dt_fim'] != '' ? $_POST['LocalHorario']['dt_fim'] : null;

                if ($localHorario->save()) {

                    $this->redirect(array('view', 'id' => $model->IDHorario));
                }
            }
            $model->diaSemana = array(
                'domingo' => 'Domingo',
                'segunda' => 'Segunda',
                'terca' => 'Terça',
                'quarta' => 'Quarta',
                'quinta' => 'Quinta',
                'sexta' => 'Sexta',
                'sabado' => 'Sábado'
            );
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'localHorario' => $localHorario
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $model = $this->loadModel($id);
                $model->localHorarios->dt_exclusao = HData::hoje(HData::SQL_DATETIME_FORMAT);
                $model->localHorarios->dt_inicio = HData::formataData(
                    $model->localHorarios->dt_inicio,
                    HData::BR_DATE_FORMAT,
                    HData::SQL_DATE_FORMAT
                );
                $model->localHorarios->dt_fim = HData::formataData(
                    $model->localHorarios->dt_fim,
                    HData::BR_DATE_FORMAT,
                    HData::SQL_DATE_FORMAT
                );
                $model->localHorarios->save();
            }
        } else {
            $model = $this->loadModel($id);
            $model->localHorarios->dt_exclusao = HData::hoje(HData::SQL_DATETIME_FORMAT);
            $model->localHorarios->dt_inicio = HData::formataData(
                $model->localHorarios->dt_inicio,
                HData::BR_DATE_FORMAT,
                HData::SQL_DATE_FORMAT
            );
            $model->localHorarios->dt_fim = HData::formataData(
                $model->localHorarios->dt_fim,
                HData::BR_DATE_FORMAT,
                HData::SQL_DATE_FORMAT
            );
            $model->localHorarios->save();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . 'Folga';
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Horario('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Horario'])) {
            $model->attributes = $_GET['Horario'];
        }

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Horario the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Horario::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Horario $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'horario-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
