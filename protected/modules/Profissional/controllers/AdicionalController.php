<?php

/**
 * @package base.Profissional
 */
class AdicionalController extends Controller
{
    public $acoesActions = array(
        'create' => '256',
        'search' => '257',
        'view' => '260',
        'update' => '259',
        'delete' => '258',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Visualizar #' . $id;
        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Adicional::model()->labelModel();
        $model = new Adicional;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Adicional'])) {
            $model->attributes = $_POST['Adicional'];
            if ($model->save()) {

                $this->redirect(array('view', 'id' => $model->IDAdicional));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Adicional::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['Adicional'])) {
            $model->attributes = $_POST['Adicional'];
            if ($model->save()) {

                $this->redirect(array('view', 'id' => $model->IDAdicional));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Adicional::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Adicional('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Adicional'])) {
            $model->attributes = $_GET['Adicional'];
        }

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Adicional the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Adicional::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Adicional $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'adicional-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
