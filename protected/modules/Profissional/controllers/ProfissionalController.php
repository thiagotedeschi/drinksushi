<?php

/**
 * @package base.Profissional
 */
class ProfissionalController extends Controller
{

    public $acoesActions = array(
        'create' => '262',
        'search' => '263',
        'view' => '264',
        'update' => '265',
        'delete' => '266',
        'desativar' => '796',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Profissional::model()->labelModel();
        $model = new Profissional;
        $profEmpregador = new ProfissionalEmpregador;
        $endereco = new EnderecoProfissional;


        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model, $endereco, $profEmpregador);

        if (isset($_POST['Profissional'], $_POST['EnderecoProfissional'][1])) {

            $newValidator = new CRequiredValidator;
            if (isset($_REQUEST['Profissional']['nit_profissional'])) {
                $newValidator->attributes[] = 'nit_profissional';
            }
            if (isset($_REQUEST['Profissional']['inscricao_mteProfissional'])) {
                $newValidator->attributes[] = 'inscricao_mteProfissional';
            }
            if (isset($_REQUEST['Profissional']['org_classeProfissional'])) {
                $newValidator->attributes[] = 'org_classeProfissional';
            }
            if (isset($_REQUEST['Profissional']['n_registroProfissional'])) {
                $newValidator->attributes[] = 'n_registroProfissional';
            }
            if (isset($_REQUEST['Profissional']['uf_registro_Profissional'])) {
                $newValidator->attributes[] = 'uf_registro_Profissional';
            }
            if (isset($_POST['Profissional']['profissionalEmpregadors'])) {
                $model->profissionalEmpregadors = $_POST['Profissional']['profissionalEmpregadors'];
            }
            if (isset($newValidator->attributes[0])) {
                $model->validatorList->add($newValidator);
            }


            $model->attributes = $_POST['Profissional'];
            $endereco->attributes = $_POST['EnderecoProfissional'][1];
            $profEmpregador->attributes = $_POST['ProfissionalEmpregador'];
            !empty($model->rg_profissional) ? : $model->uf_rg_Profissinal = null;
            if ($profEmpregador->validate() && $endereco->save()) {
                $model->IDEndereco = $endereco->IDEndereco;
                $model->receber_emails = $_POST['Profissional']['receber_emails'] != '0' ? true : false;
                if ($model->save()) {
                    //salvar ProfissionalEmpregador
                    if (isset($model->profissionalEmpregadors)) {
                        foreach ($model->profissionalEmpregadors as $IDEmpregador) {
                            $profEmp = new ProfissionalEmpregador();
                            $profEmp->dt_inicio = $profEmpregador->dt_inicio;
                            if (!($profEmp->dt_fim = $profEmpregador->dt_fim)) {
                                $profEmp->dt_fim = null;
                            }
                            $profEmp->IDEmpregador = $IDEmpregador;
                            $profEmp->IDProfissional = $model->IDProfissional;
                            $profEmp->save();
                        }
                    }
                    $this->redirect(array('search', 'id' => $model->IDProfissional));
                } else {
                    $endereco->delete();
                }
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
                'endereco' => $endereco,
                'profEmpregador' => $profEmpregador
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Profissional::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);
        $endereco = $model->iDEndereco;
        $profEmpregador = ProfissionalEmpregador::model()->findAllByAttributes(
            array(
                'IDProfissional' => $model->IDProfissional,
                'dt_desativacao' => null
            )
        );
        $this->performAjaxValidation($model, $endereco);

        if (isset($_POST['Profissional'], $_POST['EnderecoProfissional'][1])) {
            $newValidator = new CRequiredValidator;
            if (isset($_REQUEST['Profissional']['nit_profissional'])) {
                $newValidator->attributes[] = 'nit_profissional';
            }
            if (isset($_REQUEST['Profissional']['inscricao_mteProfissional'])) {
                $newValidator->attributes[] = 'inscricao_mteProfissional';
            }
            if (isset($_REQUEST['Profissional']['org_classeProfissional'])) {
                $newValidator->attributes[] = 'org_classeProfissional';
            }
            if (isset($_REQUEST['Profissional']['n_registroProfissional'])) {
                $newValidator->attributes[] = 'n_registroProfissional';
            }
            if (isset($_REQUEST['Profissional']['uf_registro_Profissional'])) {
                $newValidator->attributes[] = 'uf_registro_Profissional';
            }
            if (isset($_POST['Profissional']['profissionalEmpregadors'])) {
                $model->profissionalEmpregadors = $_POST['Profissional']['profissionalEmpregadors'];
            } else {
                $model->profissionalEmpregadors = array();
            }

            if (isset($newValidator->attributes[0])) {
                $model->validatorList->add($newValidator);
            }

            $model->attributes = $_POST['Profissional'];

            $model->nit_profissional = isset($_POST['Profissional']['nit_profissional']) ? $_POST['Profissional']['nit_profissional'] : $model->nit_profissional;

            $endereco->attributes = $_POST['EnderecoProfissional'][1];
            if ($endereco->save()) {
                $model->IDEndereco = $endereco->IDEndereco;
                if ($model->save()) {
                    $antigas = ProfissionalEmpregador::model()->findAllByAttributes(
                        array('IDProfissional' => $model->IDProfissional, 'dt_desativacao' => null)
                    );
                    //desativa relações Profissional x Empregador
                    $antigas = array_keys(Html::listData($antigas, 'IDProfissionalEmpregador', ''));
                    $novas = $model->profissionalEmpregadors;
                    $desativarRelacao = array_diff($antigas, $novas);
                    foreach ($desativarRelacao as $desativar) {
                        $relacao = ProfissionalEmpregador::model()->findByPk($desativar);
                        $relacao->dt_desativacao = new CDbExpression('CURRENT_TIMESTAMP(0)');
                        $relacao->save(false);
                    }

                    $this->redirect(array('search', 'id' => $model->IDProfissional));
                }
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'endereco' => $endereco,
                'profEmpregador' => $profEmpregador
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Profissional::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Profissional('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Profissional'])) {
            $model->attributes = $_GET['Profissional'];
        }

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Profissional the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Profissional::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Profissional $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'profissional-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionMontaCampos()
    {
        if (isset($_REQUEST['IDProfissao']) && !empty($_REQUEST['IDProfissao'])) {
            if (isset($_REQUEST['IDProfissional'])) {
                $model = $this->loadModel($_REQUEST['IDProfissional']);
                $model->scenario = 'dadosProfissao';
            } else {
                $model = new Profissional('dadosProfissao');
            }

            $IDProfissao = $_REQUEST['IDProfissao'];

//            $profissao = new Profissao('dadosProfissao');
            $profissao = Profissao::model()->findByPk($IDProfissao);

            $camposResponsabilidades = ResponsabilidadeProfissao::model()->camposResponsabilidades();
            $campos = array();

            foreach ($profissao->responsabilidadeProfissaos as $responsabilidade) {
                foreach ($camposResponsabilidades as $nomeCampo => $responsabilidades) {
                    if (in_array($responsabilidade->IDResponsabilidadeProfissao, $responsabilidades)) {
                        $campos[] = $nomeCampo;
                    }
                }
                $campos = array_unique($campos);
            }

            $retorno = '';
            foreach ($campos as $nomeCampo) {
                $retorno .= '   <div class="control-group">'
                    . Html::activeLabelEx(
                        $model,
                        $nomeCampo,
                        array('class' => 'control-label required')
                    )
                    . '<div class="controls">'
                    . Html::activeTextField($model, $nomeCampo, array('class' => 'm-wrap span6'))
                    . Html::error($model, $nomeCampo, array('class' => 'help-inline'))
                    . '</div>
                                </div>';
            }

            echo $retorno;
        }
    }


    public function actionDesativar($id)
    {
        $profissional = $this->loadModel($id);
        $model = new HistoricoProfissional;

        $this->performAjaxValidation($model);
        $this->pageTitle = $profissional->labelAcaoDesativar . ' ' . Profissional::model()->labelModel() . ' #' . $id;

        if (isset($_POST['HistoricoProfissional'])) {
            $model->attributes = $_POST['HistoricoProfissional'];
            $model->IDUsuarioResponsavel = YII::app()->user->IDUsuario;
            $model->IDProfissional = $profissional->IDProfissional;
            $model->dt_modificacaoHistoricoProfissional = HData::hoje();

            $profissional->ativo_profissional = !$profissional->ativo_profissional;
            $model->ativacao_historicoProfissional = $profissional->ativo_profissional;

            if ($model->save()) {
                if ($profissional->save()) {
                    if (isset($profissional->iDUsuario)) {
                        $usuarioProfissional = $profissional->iDUsuario;
                        $usuarioProfissional->ativo_usuario = !$usuarioProfissional->ativo_usuario;
                        $usuarioProfissional->save();
                    }
                    $this->redirect(array('search', 'id' => $model->IDProfissional));
                }
            }
        }
        $avisos = '';

        $possuiVigencia = $profissional->vigenciaAbertas();
        $possuiExames = $profissional->exameAbertos();
        if (!empty($possuiVigencia)) {

            $avisos .= '<p class="button-toggle" id="vigencias-abertos" title="Click para visualizar">
                        <i class="icon-plus" id="icon-vigencias-abertos"></i>';
            $countVigencia = count($possuiVigencia);
            if ($countVigencia > 1) {
                $avisos .= ' ' . $countVigencia . ' documentos em responsabilidade deste profisssional, não concluídos.';
            } elseif ($countVigencia == 1) {
                $avisos .= ' ' . $countVigencia . ' documento em responsabilidade deste profisssional,  não concluído.';
            }

            $avisos .= '<div id="div-vigencias-abertos" style="display:none;"><ul>';
            foreach ($possuiVigencia as $vigencia) {
                if (is_a($vigencia, 'PPRA')) {
                    $ID = $vigencia->IDPPRA;
                    $tipo = 'PPRA';
                    $cod = $vigencia->cod_versaoPPRA;
                    $documento = $vigencia->iDVigenciaPPRA;
                } elseif (is_a($vigencia, 'PCMSO')) {
                    $ID = $vigencia->IDPCMSO;
                    $tipo = 'PCMSO';
                    $cod = $vigencia->cod_versaoPCMSO;
                    $documento = $vigencia->iDVigencia;
                }
                $avisos .= '<li><ul style="list-style-type: none;">';
                $avisos .= '<li><b>' . $tipo . ' #' . $ID . '</b></li>';
                $avisos .= '<li>' . $cod . '</li>';
                $avisos .= '<li>' . $documento->iDEmpregador . '</li>';
                $avisos .= '<li>De ' . HData::sqlToBr($documento->dt_inicioVigencia) . ' até ' . HData::sqlToBr(
                        $documento->dt_inicioVigencia
                    ) . '</li>';
                $avisos .= '</ul></li>';
            }
            $avisos .= '</ul></div>';
            $avisos .= '</p>';
        }
        if (!empty($possuiExames)) {

            $avisos .= '<p class="button-toggle" id="exames-abertos"title="Click para visualizar">
                        <i class="icon-plus" id="icon-exames-abertos"></i> ';
            $countExames = count($possuiExames);
            if ($countExames > 1) {
                $avisos .= ' ' . $countExames . ' exames em responsabilidade deste profisssional, não concluídos.';
            } elseif ($countExames == 1) {
                $avisos .= ' ' . $countExames . ' exame em responsabilidade deste profisssional, não concluído.';
            }
            $avisos .= '<div id="div-exames-abertos" style="display:none;"><ul>';
            foreach ($possuiExames as $exame) {
                $avisos .= '<li><ul style="list-style-type: none;">';
                $avisos .= '<li><b>Exame #' . $exame->IDExameClinico . '</b></li> ';
                $avisos .= '<li>' . $exame->iDTrabalhadorSetorFuncao->iDEmpregadorTrabalhador->iDTrabalhador . '</li> ';
                $avisos .= '<li>' . $exame->iDTrabalhadorSetorFuncao->iDEmpregadorTrabalhador->iDEmpregador . '</li>';
                $avisos .= '<li> Motivo: ' . $exame->labelMotivoClinico . ' (' . HData::sqlToBr(
                        $exame->dt_exame
                    ) . ')</li>';
                $avisos .= '</ul></li>';
            }
            $avisos .= '</ul></div>';
            $avisos .= '</p>';
        }

        $this->render(
            'disable',
            array(
                'model' => $model,
                'profissional' => $profissional,
                'avisos' => $avisos
            )
        );
    }

}
