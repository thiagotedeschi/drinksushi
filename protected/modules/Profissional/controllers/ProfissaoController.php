<?php

/**
 * @package base.Profissional
 */
class ProfissaoController extends Controller
{

    public $acoesActions = array(
        'create' => '225',
        'search' => '226',
        'view' => '227',
        'update' => '228',
        'delete' => '229',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . 'Profissão';
        $model = new Profissao;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Profissao'])) {
            $responsabilidades = isset($_POST['Profissao']['responsabilidadeProfissaos']) ? $_POST['Profissao']['responsabilidadeProfissaos'] : array();

            $model->attributes = $_POST['Profissao'];
            if ($model->save()) {
                $model->atualizaResponsabilidades($responsabilidades);


                $this->redirect(array('search', 'id' => $model->IDProfissao));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . 'Profissão' . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['Profissao'])) {
            $responsabilidades = isset($_POST['Profissao']['responsabilidadeProfissaos']) ? $_POST['Profissao']['responsabilidadeProfissaos'] : array();

            $model->attributes = $_POST['Profissao'];
            if ($model->save()) {
                $model->atualizaResponsabilidades($responsabilidades);


                $this->redirect(array('search', 'id' => $model->IDProfissao));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . 'Profissão';
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Profissao('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Profissao'])) {
            $model->attributes = $_GET['Profissao'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Profissao the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Profissao::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Profissao $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'profissao-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
