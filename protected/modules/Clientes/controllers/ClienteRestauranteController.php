<?php
/**
 * Controller do CRUD ClienteRestaurante
 *
 * @package base.Controllers
 */

class ClienteRestauranteController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '849',
        'search' => '850',
        'view' => '852',
        'update' => '851',
        'delete' => '853',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . ClienteRestaurante::model()->labelModel();
        $model = new ClienteRestaurante;

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['ClienteRestaurante'])) {

            $enderecoCliente = new EnderecoClienteRestaurante;
            $enderecoCliente->attributes = $_POST['EnderecoClienteRestaurante'];

            if ($enderecoCliente->validate()) {
                $enderecoCliente->save();

                $model->attributes = $_POST['ClienteRestaurante'];
                $model->IDEnderecoClienteRestaurante = $enderecoCliente->IDEnderecoClienteRestaurante;

                if ($model->save()) {
                    $this->redirect(array('search', 'id' => $model->IDClienteRestaurante));
                }else{
                 // inclui erro
                }
            }

        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . ClienteRestaurante::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['ClienteRestaurante'])) {
            $enderecoCliente = $model->iDEnderecoClienteRestaurante;
            $enderecoCliente->attributes = $_POST['EnderecoClienteRestaurante'];

            if ($enderecoCliente->validate()) {
                $enderecoCliente->save();

                $model->attributes = $_POST['ClienteRestaurante'];
                $model->IDEnderecoClienteRestaurante = $enderecoCliente->IDEnderecoClienteRestaurante;

                if ($model->save()) {
                    $this->redirect(array('search', 'id' => $model->IDClienteRestaurante));
                }else{
                    // inclui erro
                }
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . ClienteRestaurante::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new ClienteRestaurante('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['ClienteRestaurante'])) {
            $model->attributes = $_GET['ClienteRestaurante'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ClienteRestaurante the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ClienteRestaurante::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ClienteRestaurante $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cliente-restaurante-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}