<?php
/* @var $this ClienteRestauranteController */
/* @var $model ClienteRestaurante */
/* @var $form CActiveForm */
if($model->isNewRecord){
    $enderecoCliente = new EnderecoClienteRestaurante();
}else{
    $enderecoCliente = $model->iDEnderecoClienteRestaurante;
}

?>


<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'cliente-restaurante-form',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
            'class' => 'form-horizontal margin-0',
        )
    )); ?>
    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com * são obrigatórios.</p>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model,'nome_cliente',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'nome_cliente',array('class'=>'m-wrap span6','maxlength'=>200)); ?>
                <?php echo $form->error($model,'nome_cliente',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'ddd_telefone',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'ddd_telefone',array('class'=>'m-wrap span2 mask_ddd','maxlength'=>2)); ?>
                <?php echo $form->error($model,'ddd_telefone',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'telefone_cliente',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'telefone_cliente',array('class'=>'m-wrap span6 telefone-mask','maxlength'=>60)); ?>
                <?php echo $form->error($model,'telefone_cliente',array('class'=>'help-inline')); ?>
            </div>
        </div>

        <ul class="breadcrumb" style="background-color: #eee !important; border: 0px !important;">
            <li>
                <a href="#">Endereço</a>
            </li>
        </ul>

        <div class="control-group">
            <?php echo $form->labelEx($enderecoCliente, 'IDBairro', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $enderecoCliente,
                    'IDBairro',
                    Html::listData(Bairro::model()->findAll(), 'IDBairro', 'nome_bairro'),
                    array(
                        'class' => 'm-wrap select2 span8',
                        'data-role' => 1,
                        'prompt' => 'Selecione...'
                    )
                );?>
                <?php echo $form->error($enderecoCliente, 'IDBairro', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($enderecoCliente,'rua',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($enderecoCliente,'rua',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($enderecoCliente,'rua',array('class'=>'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($enderecoCliente,'numero',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($enderecoCliente,'numero',array('class'=>'m-wrap span6','maxlength'=>20)); ?>
                <?php echo $form->error($enderecoCliente,'numero',array('class'=>'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($enderecoCliente,'complemento',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($enderecoCliente,'complemento',array('class'=>'m-wrap span6','maxlength'=>100)); ?>
                <?php echo $form->error($enderecoCliente,'complemento',array('class'=>'help-inline')); ?>
            </div>
        </div>


    </div>

    <div class="form-actions">
        <?php echo Html::submitButton($model->isNewRecord ? 'Criar  '.$model->labelModel() : 'Salvar Alterações', array('class'=>'botao')); ?>
    </div>
<?php $this->endWidget(); ?>