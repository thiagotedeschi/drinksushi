
<?php
/* @var $this ClienteRestauranteController */
/* @var $data ClienteRestaurante */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDClienteRestaurante')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDClienteRestaurante), array('view', 'id'=>$data->IDClienteRestaurante)); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('nome_cliente')); ?>:</b>
    <?php echo Html::encode($data->nome_cliente); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('telefone_cliente')); ?>:</b>
    <?php echo Html::encode($data->telefone_cliente); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('IDEnderecoClienteRestaurante')); ?>:</b>
    <?php echo Html::encode($data->IDEnderecoClienteRestaurante); ?>
    <br />


</div>