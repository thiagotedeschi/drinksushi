
<?php
/* @var $this ClienteRestauranteController */
/* @var $model ClienteRestaurante */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        )); ?>

    <div class="row">
        <?php echo $form->label($model,'IDClienteRestaurante'); ?>
        <?php echo $form->textField($model,'IDClienteRestaurante',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'nome_cliente'); ?>
        <?php echo $form->textField($model,'nome_cliente',array('class'=>'m-wrap span6','maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'telefone_cliente'); ?>
        <?php echo $form->textField($model,'telefone_cliente',array('class'=>'m-wrap span6','maxlength'=>60)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'IDEnderecoClienteRestaurante'); ?>
        <?php echo $form->textField($model,'IDEnderecoClienteRestaurante',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->