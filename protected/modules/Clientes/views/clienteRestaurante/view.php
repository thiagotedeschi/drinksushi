<?php
/* @var $this ClienteRestauranteController */
/* @var $model ClienteRestaurante */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget('application.widgets.DetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'IDClienteRestaurante',
            'nome_cliente',
            'telefone_cliente',
            'IDEnderecoClienteRestaurante' => array(
                'value' => $model->iDEnderecoClienteRestaurante,
                'name' => 'IDEnderecoClienteRestaurante',
                'header' => 'Endereço'
            ),
        ),
    )); ?>