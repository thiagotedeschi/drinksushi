<?php
/**
 * Controller do CRUD PedidoPDV
 *
 * @package base.Controllers
 */

class PedidoPDVController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '839',
        'search' => '842',
        'view' => '840',
        'update' => '843',
        'delete' => '841',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . PedidoPDV::model()->labelModel();
        $model = new PedidoPDV;

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['PedidoPDV'])) {

            $model->attributes = $_POST['PedidoPDV'];
            $model->IDEstadoPedido = 4;

            $model->desconto = HTexto::formataSQLMoeda($model->desconto);
            $model->total_pedido = HTexto::formataSQLMoeda($model->total_pedido);
            $model->dinheiro = HTexto::formataSQLMoeda($model->dinheiro);
            $model->cartao_debito = HTexto::formataSQLMoeda($model->cartao_debito);
            $model->cartao_credito = HTexto::formataSQLMoeda($model->cartao_credito);
            $model->troco_dinheiro = HTexto::formataSQLMoeda($model->troco_dinheiro);

            if ($model->save()) {
                $pedidoprods = $_POST['PedidoProduto'];

                foreach ($pedidoprods as $pedidoprod) {
                    $pedidoProduto = new PedidoProduto;
                    $pedidoProduto->attributes = $pedidoprod;
                    $pedidoProduto->total = $pedidoProduto->quantidade * $pedidoProduto->preco_venda;
                    $pedidoProduto->IDPedido = $model->IDPedidoPDV;
                    $pedidoProduto->preco_custo = HTexto::formataSQLMoeda($pedidoProduto->preco_custo);
                    $pedidoProduto->preco_venda = HTexto::formataSQLMoeda($pedidoProduto->preco_venda);
                    $pedidoProduto->total = HTexto::formataSQLMoeda($pedidoProduto->total);
                    $pedidoProduto->save();
                }

                $this->redirect(array('search', 'id' => $model->IDPedidoPDV));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . PedidoPDV::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['PedidoPDV'])) {
            $model->attributes = $_POST['PedidoPDV'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->IDPedidoPDV));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . PedidoPDV::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new PedidoPDV('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['PedidoPDV'])) {
            $model->attributes = $_GET['PedidoPDV'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return PedidoPDV the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = PedidoPDV::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param PedidoPDV $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pedido-pdv-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Função AJAX para retornar os campos de um novo produto no formulário de um pedido novo
     * @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     * @since 1.0 06/06/2015
     */
    public function actionMontaPedido()
    {
        $pedidoProduto = new PedidoProduto();
        if (Yii::app()->request->isAjaxRequest && !empty($_REQUEST['i'])) {
            $i = $_REQUEST['i'];
            $data = array();
            $data['txt'] = "<tr>
                        <td>
                        " . Html::activeDropDownList(
                    $pedidoProduto,
                    '[' . $i . ']IDProduto',
                    Html::listData(Produto::model()->findAll(), 'IDProduto', 'nome_produto'),
                    array(
                        'class' => 'm-wrap select2 span12 itemProduto',
                        'data-role' => $i,
                        'prompt' => 'Selecione...'
                    )
                ) . "
                        </td>
                        <td>
                        " . Html::activeTextField(
                    $pedidoProduto,
                    '[' . $i . ']quantidade',
                    array('class' => 'm-wrap span6 qtd', 'value' => 1, 'data-role' => $i)
                ) . "
                        </td>
                        <td><div class='input-prepend input-append'>
                         <span class='add-on'>R$</span>
                        " . Html::activeTextField(
                    $pedidoProduto,
                    '[' . $i . ']preco_venda',
                    array('class' => 'm-wrap span6', 'value' => 0.00)
                ) . "</div>
                        </td>
                        <td><div class='input-prepend input-append'>
                         <span class='add-on'>R$</span>
                        " . Html::activeTextField(
                    $pedidoProduto,
                    '[' . $i . ']total',
                    array('class' => 'm-wrap span8 disabled', 'value' => 0.00)
                ) . "</div>
                        </td>
                  </tr>
                  <script>
                      $(function () {
                            $('.itemProduto').on('change', function(){
                                var i = $(this).attr('data-role');
                                var quantidade = $('#PedidoProduto_' + i + '_quantidade').val();
                                $.ajax({
                                    url: '" . CController::createUrl('pedidoPDV/buscaValorProduto') . "',
                                    type: 'POST',
                                    dataType: 'JSON',
                                    data: {
                                        IDProduto: $(this).val(),
                                    },
                                    success: function (html) {
                                        var total = quantidade * html.preco;
                                        $('#PedidoProduto_' + i + '_preco_venda').val(html.preco);
                                        $('#PedidoProduto_' + i + '_total').val(total);
                                        atualizaTotal();
                                    }
                                });
                            });

                            $('.qtd').on('change', function () {

                                var i = $(this).attr('data-role');
                                var preco = $('#PedidoProduto_' + i + '_preco_venda').val();

                                var total = $(this).val() * preco;

                                $('#PedidoProduto_' + i + '_total').val(total);
                                atualizaTotal();
                            });

                           function atualizaTotal(){
                                var totalProd = $('#produtos tr').length;
                                var subtotal = 0;
                                var i;
                                for(i= 1;i<totalProd; i++){
                                    subtotal = parseFloat($('#PedidoProduto_' + i + '_total').val()) + subtotal;
                                }
                                $('.subtotal').val(subtotal);
                                var desconto = parseFloat($('.descontoPed').val());
                                var total = subtotal - desconto;
                                $('.totPed').val(total);
                            }
                      });
                  </script>
        ";
            $data['prod'] = Html::activeId($pedidoProduto, "[" . $i . "]IDProduto");
            echo CJSON::encode($data);
        }
    }

    /**
     * Função AJAX para retornar os preços de um produto
     * @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     * @since 1.0 06/06/2015
     */
    public function actionBuscaValorProduto()
    {
        if (Yii::app()->request->isAjaxRequest && !empty($_REQUEST['IDProduto'])) {
            $IDProduto = $_REQUEST['IDProduto'];
            $produto = Produto::model()->findByPk($IDProduto);
            $data = array();
            $data['preco'] = $produto->preco_venda;
            echo CJSON::encode($data);
        }
    }

    /**
     * Função AJAX para atualizar o status de um pedido
     * @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     * @since 1.0 07/06/2015
     */
    public function actionAlteraStatus()
    {
        $IDPedidoPDV = $_POST['PedidoPDV']['IDPedidoPDV'];
        $pedidoPDV = PedidoPDV::model()->findByPk($IDPedidoPDV);
        $pedidoPDV->IDEstadoPedido = $_POST['PedidoPDV']['IDEstadoPedido'];
        $pedidoPDV->save();
        $this->redirect(array('search', 'id' => $IDPedidoPDV));
    }


    /**
     * Imprime o pedido PDV
     * @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     * @since 1.0 10/06/2015
     * @param $id
     */
    public function actionImprimePedidoPDV($id)
    {
        $model = $this->loadModel($id);

        $exportPdf = PPedidoPDV::doc();
        $exportPdf->setData(
            array('title' => 'Drink Sushi', 'model' => $model)
        );

        $pedidoPDV = $exportPdf->output('S');

        header('Content-Type: application/pdf;');
        header('Content-Transfer-Encoding: binary');

        echo $pedidoPDV;

        Yii::app()->end();
    }
}