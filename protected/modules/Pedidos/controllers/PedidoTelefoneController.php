<?php
/**
 * Controller do CRUD PedidoTelefone
 *
 * @package base.Controllers
 */

class PedidoTelefoneController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '844',
        'search' => '846',
        'view' => '847',
        'update' => '848',
        'delete' => '845',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle= 'Visualizar '.$model->labelModel().' #'.$id;
        $this->render('view',array(
                'model'=>$model,
            ));
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle= 'Criar '.PedidoTelefone::model()->labelModel();
        $model=new PedidoTelefone;

// Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['PedidoTelefone']))
        {

            $model->attributes = $_POST['PedidoTelefone'];
            $model->IDEstadoPedido = 4;

            $model->desconto = HTexto::formataSQLMoeda($model->desconto);
            $model->total_pedido = HTexto::formataSQLMoeda($model->total_pedido);
            $model->dinheiro =  HTexto::formataSQLMoeda($model->dinheiro);
            $model->cartao_debito =  HTexto::formataSQLMoeda($model->cartao_debito);
            $model->cartao_credito =  HTexto::formataSQLMoeda($model->cartao_credito);
            $model->troco_dinheiro = HTexto::formataSQLMoeda($model->troco_dinheiro);

            if ($model->save()) {
                $pedidoprods = $_POST['PedidoTelefoneProduto'];

                foreach($pedidoprods as $pedidoprod){
                    $pedidoProduto = new PedidoTelefoneProduto();
                    $pedidoProduto->attributes = $pedidoprod;
                    $pedidoProduto->total = $pedidoProduto->quantidade * $pedidoProduto->preco_venda;
                    $pedidoProduto->IDPedido = $model->IDPedidoTelefone;
                    $pedidoProduto->preco_custo = HTexto::formataSQLMoeda($pedidoProduto->preco_custo);
                    $pedidoProduto->preco_venda = HTexto::formataSQLMoeda($pedidoProduto->preco_venda);
                    $pedidoProduto->total = HTexto::formataSQLMoeda($pedidoProduto->total);
                    $pedidoProduto->save();
                }

                $this->redirect(array('search', 'id' => $model->IDPedidoTelefone));
            }
        }

        $this->render('create',array(
                'model'=>$model,
            ));
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle= 'Editar '.PedidoTelefone::model()->labelModel().' #'.$id;
        $model=$this->loadModel($id);


        $this->performAjaxValidation($model);

        if(isset($_POST['PedidoTelefone']))
        {
            $model->attributes=$_POST['PedidoTelefone'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->IDPedidoTelefone));
        }

        $this->render('update',array(
                'model'=>$model,
            ));
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest)
            foreach ($_POST['id'] as $id)
                $this->loadModel($id)->delete();
        else
            $this->loadModel($id)->delete();
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle= 'Buscar '.PedidoTelefone::model()->labelModel();
//seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize']))
        {
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            unset($_GET['pageSize']);  // unseta para evitar atribuições futuras desnecessárias;
        }
        $model=new PedidoTelefone('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['PedidoTelefone']))
            $model->attributes=$_GET['PedidoTelefone'];

        if(Yii::app()->request->isAjaxRequest)
            $this->renderPartial('admin', array('model' => $model));
        else
            $this->render('admin', array('model' => $model));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return PedidoTelefone the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=PedidoTelefone::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param PedidoTelefone $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='pedido-telefone-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Função AJAX para retornar os campos de um novo produto no formulário de um pedido novo
     * @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     * @since 1.0 10/06/2015
     */
    public function actionMontaPedido()
    {
        $pedidoProduto = new PedidoTelefoneProduto();
        if (Yii::app()->request->isAjaxRequest && !empty($_REQUEST['i'])) {
            $i = $_REQUEST['i'];
            $data = array();
            $data['txt'] = "<tr>
                        <td>
                        ".Html::activeDropDownList(
                    $pedidoProduto,
                    '[' . $i . ']IDProduto',
                    Html::listData(Produto::model()->findAll(), 'IDProduto', 'nome_produto'),
                    array(
                        'class' => 'm-wrap select2 span12 itemProduto',
                        'data-role' => $i,
                        'prompt' => 'Selecione...'
                    ) ) . "
                        </td>
                        <td>
                        ".Html::activeTextField(
                    $pedidoProduto,
                    '[' . $i . ']quantidade',
                    array('class' => 'm-wrap span6 qtd','value' => 1, 'data-role' => $i)
                )."
                        </td>
                        <td><div class='input-prepend input-append'>
                         <span class='add-on'>R$</span>
                        ".Html::activeTextField(
                    $pedidoProduto,
                    '[' . $i . ']preco_venda',
                    array('class' => 'm-wrap span6','value' => 0.00)
                )."</div>
                        </td>
                        <td><div class='input-prepend input-append'>
                         <span class='add-on'>R$</span>
                        ".Html::activeTextField(
                    $pedidoProduto,
                    '[' . $i . ']total',
                    array('class' => 'm-wrap span8 disabled','value' => 0.00)
                )."</div>
                        </td>
                  </tr>
                  <script>
                      $(function () {
                            $('.itemProduto').on('change', function(){
                                var i = $(this).attr('data-role');
                                var quantidade = $('#PedidoProduto_' + i + '_quantidade').val();
                                $.ajax({
                                    url: '".CController::createUrl('pedidoTelefone/buscaValorProduto')."',
                                    type: 'POST',
                                    dataType: 'JSON',
                                    data: {
                                        IDProduto: $(this).val(),
                                    },
                                    success: function (html) {
                                        var total = quantidade * html.preco;
                                        $('#PedidoProduto_' + i + '_preco_venda').val(html.preco);
                                        $('#PedidoProduto_' + i + '_total').val(total);
                                        atualizaTotal();
                                    }
                                });
                            });

                            $('.qtd').on('change', function () {

                                var i = $(this).attr('data-role');
                                var preco = $('#PedidoProduto_' + i + '_preco_venda').val();

                                var total = $(this).val() * preco;

                                $('#PedidoProduto_' + i + '_total').val(total);
                                atualizaTotal();
                            });

                           function atualizaTotal(){
                                var totalProd = $('#produtos tr').length;
                                var subtotal = 0;
                                var i;
                                for(i= 1;i<totalProd; i++){
                                    subtotal = parseFloat($('#PedidoProduto_' + i + '_total').val()) + subtotal;
                                }
                                $('.subtotal').val(subtotal);
                                var desconto = parseFloat($('.descontoPed').val());
                                var total = subtotal - desconto;
                                $('.totPed').val(total);
                            }
                      });
                  </script>
        ";
            $data['prod'] = Html::activeId($pedidoProduto, "[".$i."]IDProduto");
            echo CJSON::encode($data);
        }
    }

    /**
     * Função AJAX para retornar os preços de um produto
     * @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     * @since 1.0 10/06/2015
     */
    public function actionBuscaValorProduto()
    {
        if (Yii::app()->request->isAjaxRequest && !empty($_REQUEST['IDProduto'])) {
            $IDProduto = $_REQUEST['IDProduto'];
            $produto = Produto::model()->findByPk($IDProduto);
            $data = array();
            $data['preco'] = $produto->preco_venda;
            echo CJSON::encode($data);
        }
    }

    /**
     * Função AJAX para atualizar o status de um pedido
     * @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     * @since 1.0 10/06/2015
     */
    public function actionAlteraStatus()
    {

        $IDPedidoTelefone = $_POST['PedidoTelefone']['IDPedidoTelefone'];
        $pedidoTelefone = PedidoTelefone::model()->findByPk($IDPedidoTelefone);
        $pedidoTelefone->IDEstadoPedido = $_POST['PedidoTelefone']['IDEstadoPedido'];
//        echo "<pre>";
//        print_r($pedidoTelefone);
//        echo "</pre>";
//        die();
        $pedidoTelefone->save();
        $this->redirect(array('search', 'id' => $IDPedidoTelefone));

    }
}