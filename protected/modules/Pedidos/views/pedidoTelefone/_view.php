<?php
/* @var $this PedidoTelefoneController */
/* @var $data PedidoTelefone */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDPedidoTelefone')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDPedidoTelefone), array('view', 'id'=>$data->IDPedidoTelefone)); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('dt_aberturaPedido')); ?>:</b>
    <?php echo Html::encode($data->dt_aberturaPedido); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('embalagem')); ?>:</b>
    <?php echo Html::encode($data->embalagem); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('desconto')); ?>:</b>
    <?php echo Html::encode($data->desconto); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('total_pedido')); ?>:</b>
    <?php echo Html::encode($data->total_pedido); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('observacao_pedido')); ?>:</b>
    <?php echo Html::encode($data->observacao_pedido); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('dinheiro')); ?>:</b>
    <?php echo Html::encode($data->dinheiro); ?>
    <br />

    <?php /*
    <b><?php echo Html::encode($data->getAttributeLabel('cartao_debito')); ?>:</b>
    <?php echo Html::encode($data->cartao_debito); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('cartao_credito')); ?>:</b>
    <?php echo Html::encode($data->cartao_credito); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('troco_dinheiro')); ?>:</b>
    <?php echo Html::encode($data->troco_dinheiro); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('IDEstadoPedido')); ?>:</b>
    <?php echo Html::encode($data->IDEstadoPedido); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('motivo_cancelamento')); ?>:</b>
    <?php echo Html::encode($data->motivo_cancelamento); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('IDClienteRestaurante')); ?>:</b>
    <?php echo Html::encode($data->IDClienteRestaurante); ?>
    <br />

    */ ?>

</div>