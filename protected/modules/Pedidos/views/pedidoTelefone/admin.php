<?php
/* @var $this PedidoTelefoneController */
/* @var $model PedidoTelefone */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

    <p class="note note-warning">
        Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada pesquisa.
    </p>


<?php $this->widget('application.widgets.grid.GridView', array(
        'id'=>'pedido-telefone-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'selectableRows' => 2,
        'columns'=>array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),

            'IDPedidoTelefone',
            'dt_aberturaPedido:datetime',
            'total_pedido' => array(
                'value' => 'HTexto::formataMoeda(HTexto::MOEDA_BRL,$data->total_pedido)',
                'name' => 'total_pedido',
                'header' => 'Valor'
            ),
            'IDEstadoPedido' => array(
                'value' => '$data->iDEstadoPedido',
                'name' => 'IDEstadoPedido',
                'header' => 'Status do Pedido'
            ),
            'embalagem:boolean',
            /*
            'desconto',
            'total_pedido',
            'observacao_pedido',
            'dinheiro',
            'cartao_debito',
            'cartao_credito',
            'troco_dinheiro',
            'IDEstadoPedido',
            'motivo_cancelamento',
            'IDClienteRestaurante',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )); ?>