<?php
/* @var $model Pedidos */
?>

<!--inicion portlet Condiçao Trabalhador-->

<div class="portlet clearfix">
    <div class="portlet-title">
        <div class="caption">Produtos</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <ul class="unstyled row-fluid">
            <?php
            $pedidoprodutos = PedidoTelefoneProduto::model()->findAllByAttributes(array('IDPedido' => $model->IDPedidoTelefone));

            if ($pedidoprodutos){
                ?>
                <table class="table table-striped flip-scroll table-bordered">
                    <thead>
                    <th>Produto</th>
                    <th>Quantidade</th>
                    <th>Valor unitário</th>
                    <th>Valor total</th>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($pedidoprodutos as $pedidoproduto) {
                        $produto = Produto::model()->findByPk($pedidoproduto->IDProduto);
                        ?>
                        <tr>
                            <td><?= $produto->nome_produto; ?></td>
                            <td><?= $pedidoproduto->quantidade; ?></td>
                            <td><?= HTexto::formataMoeda(HTexto::MOEDA_BRL,$pedidoproduto->preco_venda); ?></td>
                            <td><?= HTexto::formataMoeda(HTexto::MOEDA_BRL,$pedidoproduto->total); ?></td>
                        </tr>
                    <?php
                    }
                    $subtotal = $model->total_pedido;
                    if($model->embalagem){
                        $subtotal = $subtotal + 3.50;
                    ?>
                    <tr>
                        <td colspan="2">Embalagem</td>
                        <td colspan="2">R$ 3,50</td>
                    </tr>
                    <?php
                    }
                    ?>
                    <tr>
                        <td colspan="2">Subtotal</td>
                        <td colspan="2"><?= HTexto::formataMoeda(HTexto::MOEDA_BRL,$subtotal); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Desconto</td>
                        <td colspan="2"><?= HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->desconto); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2">Total</td>
                        <td colspan="2"><?= HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->total_pedido); ?></td>
                    </tr>
                    </tbody>
                </table>
            <?php
            }else{
                ?>
                <li>
                    <span class="muted bold">Não existem produtos nesse pedido</span>
                </li>
            <?php
            }
            ?>
        </ul>

    </div>
</div>

<!--fim portlet Condição trabalhador-->