<?php
/* @var $this PedidoTelefoneController */
/* @var $model PedidoTelefone */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        )); ?>

    <div class="row">
        <?php echo $form->label($model,'IDPedidoTelefone'); ?>
        <?php echo $form->textField($model,'IDPedidoTelefone',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'dt_aberturaPedido'); ?>
        <?php echo $form->textField($model,'dt_aberturaPedido',array('class'=>'m-wrap span6','maxlength'=>6)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'embalagem'); ?>
        <?php echo $form->checkBox($model,'embalagem'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'desconto'); ?>
        <?php echo $form->textField($model,'desconto',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'total_pedido'); ?>
        <?php echo $form->textField($model,'total_pedido',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'observacao_pedido'); ?>
        <?php echo $form->textArea($model,'observacao_pedido',array('rows'=>6, 'cols'=>50,'class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'dinheiro'); ?>
        <?php echo $form->textField($model,'dinheiro',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'cartao_debito'); ?>
        <?php echo $form->textField($model,'cartao_debito',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'cartao_credito'); ?>
        <?php echo $form->textField($model,'cartao_credito',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'troco_dinheiro'); ?>
        <?php echo $form->textField($model,'troco_dinheiro',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'IDEstadoPedido'); ?>
        <?php echo $form->textField($model,'IDEstadoPedido',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'motivo_cancelamento'); ?>
        <?php echo $form->textArea($model,'motivo_cancelamento',array('rows'=>6, 'cols'=>50,'class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'IDClienteRestaurante'); ?>
        <?php echo $form->textField($model,'IDClienteRestaurante',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->