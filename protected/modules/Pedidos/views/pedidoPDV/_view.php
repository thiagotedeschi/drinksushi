
<?php
/* @var $this PedidoPDVController */
/* @var $data PedidoPDV */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDPedidoPDV')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDPedidoPDV), array('view', 'id'=>$data->IDPedidoPDV)); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('mesa')); ?>:</b>
    <?php echo Html::encode($data->mesa); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('dt_aberturaPedido')); ?>:</b>
    <?php echo Html::encode($data->dt_aberturaPedido); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('embalagem')); ?>:</b>
    <?php echo Html::encode($data->embalagem); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('desconto')); ?>:</b>
    <?php echo Html::encode($data->desconto); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('total_pedido')); ?>:</b>
    <?php echo Html::encode($data->total_pedido); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('observacao_pedido')); ?>:</b>
    <?php echo Html::encode($data->observacao_pedido); ?>
    <br />


</div>