<?php
/* @var $this PedidoPDVController */
/* @var $model PedidoPDV */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget('application.widgets.DetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'IDPedidoPDV',
            'mesa',
            'dt_aberturaPedido:datetime',
            'embalagem:boolean',
            'desconto' => array(
                'label' => $model->attributeLabels()['desconto'],
                'value' => HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->desconto)
            ),
            'total_pedido' => array(
                'label' => 'Valor do Pedido',
                'value' => HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->total_pedido)
            ),
            'cartao_debito' => array(
                'label' => 'Pago com Débito',
                'value' => HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->cartao_debito),
                'visible' => $model->cartao_debito == '0.00'? false : true
            ),
            'cartao_credito' => array(
                'label' => 'Pago com Débito',
                'value' => HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->cartao_credito),
                'visible' => $model->cartao_credito == '0.00'? false : true
            ),
            'dinheiro' => array(
                'label' => 'Pago em Dinheiro',
                'value' => HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->dinheiro),
                'visible' => $model->dinheiro == '0.00'? false : true
            ),
            'troco_dinheiro' => array(
                'label' => 'Troco',
                'value' => HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->troco_dinheiro),
                'visible' => $model->troco_dinheiro == '0.00'? false : true
            ),
            'observacao_pedido',


        ),
    )); ?>

<!-- end PAGE TITLE -->
<div class="portlet-body main-portlet clearfix">

    <div class="span12">
        <hr>

        <ul class="breadcrumb" style="background-color: green !important; color: #FFFFFF !important; border: 0px !important;">
            <li>
                <a href="#" style="color: #fafafa !important;">Alterar o Status do pedido</a>
            </li>
        </ul>
        <form action="<?= $this->createUrl('PedidoPDV/alteraStatus')?>" method="post">
        <?php
        $pedidopdv = new PedidoPDV();
        echo Html::activeDropDownList(
            $pedidopdv,
            'IDEstadoPedido',
            Html::listData(EstadoPedido::model()->findAll(), 'IDEstadoPedido', 'tipo_estado'),
            array(
                'class' => 'm-wrap select2 span6',
                'prompt' => 'Selecione o estado do pedido'
            )
        );
        echo Html::activeHiddenField(
            $pedidopdv,
            'IDPedidoPDV',
            array('value' => $model->IDPedidoPDV)
        );

        ?>
            <input type="submit" class="btn yellow" name="alterar" value="Atualizar Status">
        </form>
    </div>
    <div class="span3">

    </div>
    <div class="span9">
        <?php $this->renderPartial(
            'visualizacao/_produtos',
            array(
                'model' => $model
            )
        ) ?>
    </div>

    <div class="span9">
        <?php $this->renderPartial(
            'visualizacao/_pagamento',
            array(
                'model' => $model
            )
        ) ?>
    </div>
</div>
<!-- END PAGE CONTENT-->
