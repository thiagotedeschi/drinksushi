<?php
/* @var $this PedidoPDVController */
/* @var $model PedidoPDV */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        )); ?>

    <div class="row">
        <?php echo $form->label($model,'IDPedidoPDV'); ?>
        <?php echo $form->textField($model,'IDPedidoPDV',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'mesa'); ?>
        <?php echo $form->textField($model,'mesa',array('class'=>'m-wrap span6','maxlength'=>100)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'dt_aberturaPedido'); ?>
        <?php echo $form->textField($model,'dt_aberturaPedido',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'embalagem'); ?>
        <?php echo $form->checkBox($model,'embalagem'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'desconto'); ?>
        <?php echo $form->textField($model,'desconto',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'total_pedido'); ?>
        <?php echo $form->textField($model,'total_pedido',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'observacao_pedido'); ?>
        <?php echo $form->textArea($model,'observacao_pedido',array('rows'=>6, 'cols'=>50,'class'=>'m-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->