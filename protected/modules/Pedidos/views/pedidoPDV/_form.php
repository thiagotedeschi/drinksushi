<?php
/* @var $this PedidoPDVController */
/* @var $model PedidoPDV */
/* @var $form CActiveForm */
$pedidoProduto = new PedidoProduto();
?>

<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'pedido-pdv-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
); ?>

    <div class="form clearfix positionRelative">
    <ul class="breadcrumb" style="background-color: #eee !important; border: 0px !important;">
        <li>
            <a href="#">Novo Pedido</a>
        </li>
    </ul>
    <?php echo $form->errorSummary($model); ?>

    <table>
        <tr>
            <td>
                <div class="control-group">
                    <?php echo $form->labelEx($model, 'mesa', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField(
                            $model,
                            'mesa',
                            array('class' => 'm-wrap span6', 'maxlength' => 100)
                        ); ?>
                        <?php echo $form->error($model, 'mesa', array('class' => 'help-inline')); ?>
                    </div>
                </div>
            </td>
            <td>
                <div class="control-group">
                    <?php echo $form->labelEx($model, 'dt_aberturaPedido', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField(
                            $model,
                            'dt_aberturaPedido',
                            array('class' => 'm-wrap span8', 'disabled' => true, 'value' => HData::hoje())
                        ); ?>
                        <?php echo $form->error($model, 'dt_aberturaPedido', array('class' => 'help-inline')); ?>
                    </div>
                </div>
            </td>
            <td>
                <div class="control-group">
                    <?php echo $form->labelEx($model, 'hora', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField(
                            $model,
                            'hora',
                            array('class' => 'm-wrap span8', 'disabled' => true, 'value' => HData::hora())
                        ); ?>
                        <?php echo $form->error($model, 'hora', array('class' => 'help-inline')); ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <hr>

    <ul class="breadcrumb" style="background-color: #eee !important; border: 0px !important;">
        <li>
            <a href="#">Produtos</a>
        </li>
    </ul>

    <table id="produtos">
        <tr>
            <th>
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $pedidoProduto,
                        'IDProduto',
                        array('class' => 'control-label')
                    ); ?>
                </div>
            </th>
            <th>
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $pedidoProduto,
                        'quantidade',
                        array('class' => 'control-label')
                    ); ?>
                </div>
            </th>
            <th>
                <div class="control-group">
                    <?php echo $form->labelEx(
                        $pedidoProduto,
                        'preco_venda',
                        array('class' => 'control-label')
                    ); ?>
                </div>
            </th>
            <th>
                <div class="control-group">
                    <?php echo $form->labelEx($pedidoProduto, 'total', array('class' => 'control-label')); ?>
                </div>
            </th>
        </tr>
        <tr>
            <td>

                <?php
                echo $form->dropDownList(
                    $pedidoProduto,
                    '[1]IDProduto',
                    Html::listData(Produto::model()->findAll(), 'IDProduto', 'nome_produto'),
                    array(
                        'class' => 'm-wrap select2 span12 itemProduto2',
                        'data-role' => 1,
                        'prompt' => 'Selecione...'
                    )
                );
                ?>
                <?php echo $form->error($pedidoProduto, 'IDProduto', array('class' => 'help-inline')); ?>

            </td>

            <td>
                <?php echo $form->textField(
                    $pedidoProduto,
                    '[1]quantidade',
                    array('class' => 'm-wrap span6 qtd2', 'maxlength' => 11, 'value' => 1,'data-role' => 1)
                ); ?>
                <?php echo $form->error($pedidoProduto, 'quantidade', array('class' => 'help-inline')); ?>


            </td>

            <td>
                <div class="input-prepend input-append">
                    <span class="add-on">R$</span>
                    <?php echo $form->textField(
                        $pedidoProduto,
                        '[1]preco_venda',
                        array('class' => 'm-wrap span6 ')
                    ); ?>
                </div>
                <?php echo $form->error($pedidoProduto, 'preco_venda', array('class' => 'help-inline')); ?>


            </td>

            <td>
                <div class="input-prepend input-append">
                    <span class="add-on">R$</span>
                    <?php echo $form->textField(
                        $pedidoProduto,
                        '[1]total',
                        array('class' => 'm-wrap span8', 'disabled' => true)
                    ); ?>
                </div>
                <?php echo $form->error($pedidoProduto, 'total', array('class' => 'help-inline')); ?>


            </td>

        </tr>
    </table>


    <a style="cursor: pointer;" id="addItem"><input type="button" class="btn yellow"
                                                    value="Adicionar novo Item"></a>

    <hr>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'subtotal', array('class' => 'control-label')); ?>
        <div class="controls">
            <div class="input-prepend input-append">
                <span class="add-on">R$</span>
                <?php echo $form->textField(
                    $model,
                    'subtotal',
                    array('class' => 'm-wrap span8 subtotal', 'maxlength' => 11, 'disabled' => true, 'value' => 0)
                ); ?>
            </div>
            <?php echo $form->error($model, 'subtotal', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'embalagem', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->checkBox($model, 'embalagem',array('class' => 'embalagemPedido')); ?><span>(R$ 3,50)</span>
            <?php echo $form->error($model, 'embalagem', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'desconto', array('class' => 'control-label')); ?>
        <div class="controls">
            <div class="input-prepend input-append">
                <span class="add-on">R$</span>
                <?php echo $form->textField(
                    $model,
                    'desconto',
                    array('class' => 'm-wrap span6 descontoPed ', 'maxlength' => 11, 'value' => 0.00)
                ); ?>
            </div>
            <?php echo $form->error($model, 'desconto', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'total_ped', array('class' => 'control-label')); ?>
        <div class="controls">
            <div class="input-prepend input-append">
                <span class="add-on">R$</span>
                <?php echo $form->textField(
                    $model,
                    'total_ped',
                    array('class' => 'm-wrap span8 totPed', 'disabled' => true, 'maxlength' => 11, 'value' => 0.00)
                ); ?>
            </div>
            <?php echo $form->error($model, 'total_ped', array('class' => 'help-inline')); ?>
        </div>
    </div>
    <?php
    echo Html::activeHiddenField(
        $model,
        'total_pedido',
        array('class' => 'totPed', 'value' => 0.00)
    );
    ?>

    <hr>

    <ul class="breadcrumb" style="background-color: #eee !important; border: 0px !important;">
        <li>
            <a href="#">Formas de Pagamento</a>
        </li>
    </ul>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'cartao_credito', array('class' => 'control-label')); ?>
        <div class="controls">
            <div class="input-prepend input-append">
                <span class="add-on">R$</span>
                <?php echo $form->textField(
                    $model,
                    'cartao_credito',
                    array('class' => 'm-wrap span8 ', 'maxlength' => 11, 'value' => 0.00)
                ); ?>
            </div>
            <?php echo $form->error($model, 'cartao_credito', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'cartao_debito', array('class' => 'control-label')); ?>
        <div class="controls">
            <div class="input-prepend input-append">
                <span class="add-on">R$</span>
                <?php echo $form->textField(
                    $model,
                    'cartao_debito',
                    array('class' => 'm-wrap span8 ', 'maxlength' => 11, 'value' => 0.00)
                ); ?>
            </div>
            <?php echo $form->error($model, 'cartao_debito', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'dinheiro', array('class' => 'control-label')); ?>
        <div class="controls">
            <div class="input-prepend input-append">
                <span class="add-on">R$</span>
                <?php echo $form->textField(
                    $model,
                    'dinheiro',
                    array('class' => 'm-wrap span8 ', 'maxlength' => 11, 'value' => 0.00)
                ); ?>
            </div>
            <?php echo $form->error($model, 'dinheiro', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'troco_din', array('class' => 'control-label')); ?>
        <div class="controls">
            <div class="input-prepend input-append">
                <span class="add-on">R$</span>
                <?php echo $form->textField(
                    $model,
                    'troco_din',
                    array('class' => 'm-wrap span8 ','disabled' => true, 'maxlength' => 11, 'value' => 0.00)
                ); ?>
            </div>
            <?php echo $form->error($model, 'troco_din', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <?php
    echo Html::activeHiddenField(
        $model,
        'troco_dinheiro',
        array('value' => 0.00)
    );
    ?>

    <hr>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'observacao_pedido', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'observacao_pedido',
                array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($model, 'observacao_pedido', array('class' => 'help-inline')); ?>
        </div>
    </div>
    </div>

    <div class="form-actions">
        <?php echo Html::submitButton(
            $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
            array('class' => 'botao')
        ); ?>
    </div>

    <script>
        $(function () {
            $("#addItem").click(function () {
                $.ajax({
                    url: "<?=CController::createUrl('pedidoPDV/MontaPedido')?>",
                    type: "POST",
                    dataType: 'JSON',
                    data: {
                        i: $("#produtos tr").length,
                    },
                    success: function (html) {
                        var div = $("#produtos");
                        $(html.txt).appendTo(div);
                        var i = $("#produtos tr").length;
                        $('#' + html.prod).select2();
                    }
                });
            });

            $('.itemProduto2').on("change", function () {
                var i = $(this).attr('data-role');
                var quantidade = $('#PedidoProduto_' + i + '_quantidade').val();
                $.ajax({
                    url: "<?=CController::createUrl('pedidoPDV/buscaValorProduto')?>",
                    type: "POST",
                    dataType: 'JSON',
                    data: {
                        IDProduto: $(this).val(),
                    },
                    success: function (html) {
                        var total = quantidade * html.preco;
                        $('#PedidoProduto_' + i + '_preco_venda').val(html.preco);
                        $('#PedidoProduto_' + i + '_total').val(total);
                        atualizaTotal();
                        retornaTroco()
                    }
                });

            });

            $('.qtd2').on("change", function () {

                var i = $(this).attr('data-role');
                var preco = $('#PedidoProduto_' + i + '_preco_venda').val();

                var total = $(this).val() * preco;

                $('#PedidoProduto_' + i + '_total').val(total);
                atualizaTotal();
                retornaTroco()
            });

            function atualizaTotal(){
                var totalProd = $('#produtos tr').length;
                var subtotal = 0;
                var i;
                for(i= 1;i<totalProd; i++){
                    subtotal = parseFloat($('#PedidoProduto_' + i + '_total').val()) + subtotal;
                }
                $('.subtotal').val(subtotal);
                var desconto = parseFloat($('.descontoPed').val());
                var total = subtotal - desconto;

                if($('.embalagemPedido').attr('checked')){
                    total = total + 3.5;
                }
                $('.totPed').val(total);

            }

            $('.descontoPed').on("change", function () {
                atualizaTotal();
                retornaTroco()
            });

            $('.embalagemPedido').on("click", function () {
                atualizaTotal();
                retornaTroco()
            });

            $('#<?= Html::activeId($model, "dinheiro")?>').on("change", function () {
                retornaTroco();
            });
            $('#<?= Html::activeId($model, "cartao_debito")?>').on("change", function () {
                retornaTroco();
            });
            $('#<?= Html::activeId($model, "cartao_credito")?>').on("change", function () {
                retornaTroco();
            });

            function retornaTroco(){
                var totalDoPedido =  parseFloat($('#<?= Html::activeId($model, "total_pedido")?>').val());
                var dinheiro =  parseFloat($('#<?= Html::activeId($model, "dinheiro")?>').val());
                var cartao_debito =  parseFloat($('#<?= Html::activeId($model, "cartao_debito")?>').val());
                var cartao_credito =  parseFloat($('#<?= Html::activeId($model, "cartao_credito")?>').val());
                var totalPagar = dinheiro + cartao_credito + cartao_debito;
                if(totalPagar>totalDoPedido){
                    var troconaoformatado = totalPagar - totalDoPedido;
                    var troco = formatReal(troconaoformatado);
                    $('#<?= Html::activeId($model, "troco_dinheiro")?>').val(troconaoformatado);
                    $('#<?= Html::activeId($model, "troco_din")?>').val(troco);
                }else{
                    $('#<?= Html::activeId($model, "troco_dinheiro")?>').val('0.00');
                    $('#<?= Html::activeId($model, "troco_din")?>').val('0,00');
                }

            }


            function formatReal(mixed) {
                var int = parseInt(mixed.toFixed(2).toString().replace(/[^\d]+/g, ''));
                var tmp = int + '';
                tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
                if (tmp.length > 6)
                    tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

                return tmp;
            }

        });

    </script>
<?php $this->endWidget(); ?>