<?php
/* @var $this PedidoPDVController */
/* @var $model PedidoPDV */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

    <p class="note note-warning">
        Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada pesquisa.
    </p>


<?php $this->widget('application.widgets.grid.GridView', array(
        'id'=>'pedido-pdv-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'selectableRows' => 2,
        'columns'=>array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),

            'IDPedidoPDV',
            'mesa',
            'dt_aberturaPedido:datetime',
            'total_pedido' => array(
                'value' => 'HTexto::formataMoeda(HTexto::MOEDA_BRL,$data->total_pedido)',
                'name' => 'total_pedido',
                'header' => 'Valor'
            ),
            'IDEstadoPedido' => array(
                'value' => '$data->iDEstadoPedido',
                'name' => 'IDEstadoPedido',
                'header' => 'Status do Pedido'
            ),

            'embalagem:boolean',
            /*
            'desconto',
            'total_pedido',
            'observacao_pedido',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{printer}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'printer' => array(
                            'options' => array(
                                'class' => 'botao desativar',
                                'title' => 'Imprimir Pedido',
                                'target' => '_blank'
                            ),
                            'imageUrl' => false,
                            'label' => '"<i class=\'icon-print icon-large\'></i>"',
                            'url' => 'Yii::app()->createUrl("Pedidos/pedidoPDV/imprimePedidoPDV", array("id"=>$data->primaryKey))',
                            'visible' => 'true'
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )); ?>