<?php
/* @var $model Pedidos */
?>

<!--inicion portlet Condiçao Trabalhador-->

<div class="portlet clearfix">
    <div class="portlet-title">
        <div class="caption">Formas de Pagamento</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"></a>
        </div>
    </div>
    <div class="portlet-body">
        <ul class="unstyled row-fluid">

                <table class="table table-striped flip-scroll table-bordered">
                    <thead>
                    <th>Tipo</th>
                    <th>Valor</th>
                    </thead>
                    <tbody>

                    <tr>
                        <td>Cartão de Crédito</td>
                        <td><?= HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->cartao_credito); ?></td>
                    </tr>
                    <tr>
                        <td>Cartão de Débito</td>
                        <td><?= HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->cartao_debito); ?></td>
                    </tr>
                    <tr>
                        <td>Dinheiro</td>
                        <td><?= HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->dinheiro); ?></td>
                    </tr>
                    <tr>
                        <td>Troco</td>
                        <td><?= HTexto::formataMoeda(HTexto::MOEDA_BRL,$model->troco_dinheiro); ?></td>
                    </tr>
                    </tbody>
                </table>

        </ul>

    </div>
</div>

<!--fim portlet Condição trabalhador-->