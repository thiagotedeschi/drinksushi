<?php

class SetorController extends Controller
{

    public $acoesActions = array(
        'create' => '203',
        'search' => '205',
        'view' => '204',
        'update' => '206',
        'delete' => '207',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Setor::model()->labelModel();
        $model = new Setor;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Setor'])) {
            $model->attributes = $_POST['Setor'];
            if ($model->save()) {

                $this->redirect(array('search', 'id' => $model->IDSetor));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Setor::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);

        $this->performAjaxValidation(array($model));

        if (isset($_POST['Setor'])) {
            $model->attributes = $_POST['Setor'];
            if ($model->save()) {
                $this->redirect(array('search', 'id' => $model->IDSetor));
            }
        }
        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            $lista = '';
            foreach ($_POST['id'] as $id) {
                $model = $this->loadModel($id);
                $ativo = false;
                //verifica se pelo menos um setorFuncao vinculado a esse Setor está ativo
                foreach ($model->setorFuncaos as $sf) {
                    if ($sf->ativo_setorFuncao) {
                        $ativo = true;
                        break;
                    }
                }
                //se existe algum ativo, adiciona o nome do Setor na lista
                if ($ativo) {
                    if ($lista) {
                        $lista .= ', ';
                    }
                    $lista .= $model;
                } //se não deleta o setor
                else {
                    $model->delete();
                }
            }
            if ($lista) {
                Yii::app()->user->setFlash(
                    'error',
                    array('Os seguinte setores possuem pelo menos uma associação ativa com funções, e por isso não foram excluídos:<br><b>' . $lista . '</b><br><br>Para excluir um setor, certifique-se de desativar ou excluir todas as associações envolvendo o mesmo.')
                );
            }
        } else {
            $model = $this->loadModel($id);
            $ativo = false;
            //verifica se pelo menos um setorFuncao vinculado a esse Setor está ativo
            foreach ($model->setorFuncaos as $sf) {
                if ($sf->ativo_setorFuncao) {
                    $ativo = true;
                    break;
                }
            }
            if (!$ativo) {
                $model->delete();
            } else {
                Yii::app()->user->setFlash(
                    'error',
                    array('Você não pode excluir um setor que esteja associado a uma função!<br>Para excluir um setor, certifique-se de desativar ou excluir todas as associações envolvendo o mesmo.')
                );
            }
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Setor::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Setor('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Setor'])) {
            $model->attributes = $_GET['Setor'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Setor the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Setor::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        $model->dt_levantamentoSetor = HData::formataData(
            $model->dt_levantamentoSetor,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATE_FORMAT
        );
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Setor $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'setor-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
