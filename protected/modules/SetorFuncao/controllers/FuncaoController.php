<?php

class FuncaoController extends Controller
{

    public $acoesActions = array(
        'create' => '208',
        'search' => '210',
        'view' => '209',
        'update' => '211',
        'delete' => '212',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $model = new Funcao;
        $this->pageTitle = 'Criar ' . $model->labelModel();

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Funcao'])) {
            $model->attributes = $_POST['Funcao'];
            if ($model->save()) {
                foreach ($_POST['Funcao']['setores_funcao'] as $s) {
                    $sf = new SetorFuncao;
                    $sf->IDFuncao = $model->IDFuncao;
                    $sf->IDSetor = $s;
                    $sf->save();
                }

                $this->redirect(array('search', 'id' => $model->IDFuncao));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Editar ' . $model->labelModel() . ' #' . $id;
        $this->performAjaxValidation([$model]);

        $setorFuncao = SetorFuncao::model()->findAllByAttributes(array("IDFuncao" => $id));
        $setores = array();

        foreach ($setorFuncao as $setF) {
            $setores[] = $setF->IDSetor;
        }
        $model->setores_funcao = $setores;

        if (isset($_POST['Funcao'])) {
            $model->attributes = $_POST['Funcao'];

            if ($model->save()) {

                $this->redirect(array('search', 'id' => $model->IDFuncao));
            }
        }
        $this->render(
            'update',
            array(
                'model' => $model
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $model = new Funcao('search');
        $this->pageTitle = 'Buscar ' . $model->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }

        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Funcao'])) {
            $model->attributes = $_GET['Funcao'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Funcao the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Funcao::model()->findByPk($id);
        $model->dt_levantamentoFuncao = HData::formataData(
            $model->dt_levantamentoFuncao,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATE_FORMAT
        );
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Funcao $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'funcao-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
