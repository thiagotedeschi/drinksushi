<?php

class SetorFuncaoController extends Controller
{

    public $acoesActions = array(
        'create' => '261',
        'search' => '261',
        'view' => '261',
        'update' => '261',
        'delete' => '261',
        'disable' => '261'
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $model = new SetorFuncao;
        $this->pageTitle = 'Criar ' . $model->labelModel();


        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['SetorFuncao'])) {
            $error = false;
            $model->attributes = $_POST['SetorFuncao'];
            if (isset($_POST['SetorFuncao']['IDFuncao'])) {
                foreach ($_POST['SetorFuncao']['IDFuncao'] as $IDFuncao) {
                    $setorFuncao = new SetorFuncao;
                    $setorFuncao->attributes = $_POST['SetorFuncao'];
                    $setorFuncao->IDFuncao = $IDFuncao;
                    if (!$setorFuncao->save()) {
                        $error = true;
                    }
                }
            } else {
                $error = true;
            }
            if (!$error) {
                $this->redirect(array('search', 'id' => $model->IDSetorFuncao));
            } else {
                $model->validate();
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Editar ' . $model->labelModel() . ' #' . $id;

        $this->performAjaxValidation(array($model));

        if (isset($_POST['SetorFuncao'])) {
            $model->attributes = $_POST['SetorFuncao'];
            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->IDSetorFuncao));
            }
        }
        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }


    public function actionDisable($id)
    {
        $setorFuncao = $this->loadModel($id);
        $model = new HistoricoSetorFuncao;

        $this->performAjaxValidation($model);
        $this->pageTitle = $setorFuncao->labelAcao . ' ' . $model->labelModel() . ' ' . $id;

        if (isset($_POST['HistoricoSetorFuncao'])) {
            $model->attributes = $_POST['HistoricoSetorFuncao'];
            $model->IDUsuarioResponsavel = YII::app()->user->IDUsuario;
            $model->IDSetorFuncao = $setorFuncao->IDSetorFuncao;

            $setorFuncao->ativo_setorFuncao = !$setorFuncao->ativo_setorFuncao;
            $model->ativacao_historicoSF = $setorFuncao->ativo_setorFuncao;

            if ($model->save() && $setorFuncao->save()) {
                $this->redirect(array('view', 'id' => $model->IDSetorFuncao));
            }
        }

        $this->render(
            'disable',
            array(
                'model' => $model,
                'setorFuncao' => $setorFuncao,
            )
        );
    }

    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            $lista = '';
            foreach ($_POST['id'] as $id) {
                $model = $this->loadModel($id);
                $IDFuncoes = SetorFuncao::model()->findAllByAttributes(array('IDFuncao' => $model->IDFuncao));
                if (count($IDFuncoes) != 1) {
                    $model->delete();
                } else {
                    $lista .= '<br />' . $model->labelSetorFuncao;
                }
            }
            if ($lista) {
                Yii::app()->user->setFlash(
                    'error',
                    array('Não foi possível excluir as seguintes associações:<b>' . $lista . '</b><br />Por favor, tente primeiro excluir a Função relacionada')
                );
            }
        } else {
            $model = $this->loadModel($id);
            $IDFuncoes = SetorFuncao::model()->findAllByAttributes(array('IDFuncao' => $model->IDFuncao));
            if (count($IDFuncoes) != 1) {
                $model->delete();
            } else {
                Yii::app()->user->setFlash(
                    'error',
                    array('Não foi possível excluir essa associação. Por favor tente primeiro excluir a Função relacionada.')
                );
            }
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $model = new SetorFuncao('search');
        $this->pageTitle = 'Buscar ' . $model->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['SetorFuncao'])) {
            $model->attributes = $_GET['SetorFuncao'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SetorFuncao the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = SetorFuncao::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param SetorFuncao $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && ($_POST['ajax'] === 'setor-funcao-form' || $_POST['ajax'] === 'historico-setor-funcao-disable-form')) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionAjaxFuncoesBySetor()
    {
        $IDSetor = $_POST["IDSetor"];
        if (Yii::app()->request->isAjaxRequest && $IDSetor) {
            $data = Html::listData(
                SetorFuncao::model()->ativo()->findAllByAttributes(['IDSetor' => $IDSetor]),
                'IDFuncao',
                'iDFuncao'
            );
            if ($data) {
                foreach ($data as $value => $name) {
                    echo Html::tag('option', array('value' => $value), Html::encode($name), true);
                }
            }
        }
    }

    public function actionAjaxFuncoesBySetorSemAtividade()
    {
        $IDSetor = $_POST["IDSetor"];
        if (Yii::app()->request->isAjaxRequest && $IDSetor) {
            $data = Html::listData(
                SetorFuncao::model()->ativo()->semDescricaoAtividade()->findAllByAttributes(['IDSetor' => $IDSetor]),
                'IDFuncao',
                'iDFuncao'
            );
            if ($data) {
                foreach ($data as $value => $name) {
                    echo Html::tag('option', array('value' => $value), Html::encode($name), true);
                }
            }
        }
    }

    public function actionAjaxFuncoesBySetorSemAmbiente()
    {
        $IDSetor = $_POST["IDSetor"];
        if (Yii::app()->request->isAjaxRequest && $IDSetor) {
            $data = Html::listData(
                SetorFuncao::model()->ativo()->semDescricaoAmbiente()->findAllByAttributes(['IDSetor' => $IDSetor]),
                'IDFuncao',
                'iDFuncao'
            );
            if ($data) {
                foreach ($data as $value => $name) {
                    echo Html::tag('option', array('value' => $value), Html::encode($name), true);
                }
            }
        }
    }

    public function actionAjaxSetorByEmpregador()
    {
        $IDEmpregador = $_POST["IDEmpregador"];
        if (Yii::app()->request->isAjaxRequest && $IDEmpregador) {
            $data = Html::listData(
                Setor::model()->getSetoresEmpregador($IDEmpregador),
                'IDSetor',
                'nome_setor'
            );
            if ($data) {
                foreach ($data as $value => $name) {
                    echo Html::tag('option', array('value' => $value), Html::encode($name), true);
                }
            }
        }
    }

    public function actionAjaxSetoresFuncoesByEmpregador()
    {
        $IDEmpregador = $_POST["IDEmpregador"];
        if (Yii::app()->request->isAjaxRequest && $IDEmpregador) {
            $setores = Html::listData(
                Setor::model()->getSetoresEmpregador($IDEmpregador),
                'IDSetor',
                'nome_setor'
            );
            $funcoes = Html::listData(
                Funcao::model()->findByEmpregador($IDEmpregador),
                'IDFuncao',
                'nome_funcao'
            );
            $data = array(
                "setor" => $setores,
                "funcao" => $funcoes
            );
            echo CJSON::encode($data);
        }

    }

}
