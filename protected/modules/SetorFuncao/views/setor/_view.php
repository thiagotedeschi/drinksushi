<?php
/* @var $this SetorController */
/* @var $data Setor */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDSetor')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDSetor), array('view', 'id' => $data->IDSetor)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_setor')); ?>:</b>
    <?php echo Html::encode($data->nome_setor); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_levantamentoSetor')); ?>:</b>
    <?php echo Html::encode($data->dt_levantamentoSetor); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_cadastroSetor')); ?>:</b>
    <?php echo Html::encode($data->dt_cadastroSetor); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_ultimaModificacao')); ?>:</b>
    <?php echo Html::encode($data->dt_ultimaModificacao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDEmpregador')); ?>:</b>
    <?php echo Html::encode($data->IDEmpregador); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('ativo_setor')); ?>:</b>
    <?php echo Html::encode($data->ativo_setor); ?>
    <br/>


</div>