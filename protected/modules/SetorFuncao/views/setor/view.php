<?php

/* @var $this SetorController */
/* @var $model Setor */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

$funcoes = implode(', ', $model->funcoes());

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDSetor',
            'nome_setor',
            'dt_levantamentoSetor',
            'dt_cadastroSetor:date',
            'iDEmpregador:text:Empregador',
            'funcoes' => array('value' => $funcoes, 'label' => 'Funções')
        ),
    )
);
?>
