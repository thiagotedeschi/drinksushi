<?php
/* @var $this SetorController */
/* @var $model Setor */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'setor-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>

<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php if (!$model->isNewRecord) { ?>
        <?php echo $form->errorSummary(array($model)); ?>
    <?php
    } else {
        echo $form->errorSummary($model);
    } ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDEmpregador',
                Yii::app()->session['empregadores'],
                array(
                    'class' => 'm-wrap span6 select2',
                    'options' => array(
                        Yii::app()->user->getState('IDEmpregador') => array('selected' => true)
                    ),
                    'prompt' => 'Selecione um Empregador'
                )
            ); ?>
            <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_setor', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'nome_setor', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'nome_setor', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_levantamentoSetor', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'dt_levantamentoSetor',
                array('class' => 'date-picker', 'maxlength' => 10)
            ); ?>
            <?php echo $form->error($model, 'dt_levantamentoSetor', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
