<?php
/* @var $this SetorController */
/* @var $model Setor */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDSetor'); ?>
        <?php echo $form->textField($model, 'IDSetor', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_setor'); ?>
        <?php echo $form->textField($model, 'nome_setor', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_levantamentoSetor'); ?>
        <?php echo $form->textField($model, 'dt_levantamentoSetor', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_cadastroSetor'); ?>
        <?php echo $form->textField($model, 'dt_cadastroSetor', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_ultimaModificacao'); ?>
        <?php echo $form->textField($model, 'dt_ultimaModificacao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDEmpregador'); ?>
        <?php echo $form->textField($model, 'IDEmpregador', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ativo_setor'); ?>
        <?php echo $form->checkBox($model, 'ativo_setor'); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->