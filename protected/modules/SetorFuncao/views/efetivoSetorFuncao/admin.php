<?php
/* @var $this EfetivoSetorFuncaoController */
/* @var $model SetorFuncao */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

<div id="modal-disable" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div clas="col-md-12">
                <iframe src="" style="width:100%; overflow: hidden; min-height:400px;"></iframe>
            </div>
        </div>
    </div>
</div>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada
    pesquisa.
</p>

<?php if (!Yii::app()->user->getState('IDEmpregador')) { ?>
    <p class="note note-danger">
        Para visualizar essa página é necessário selecionar um <b>Empregador</b>.
    </p>
<?php } ?>

<?php $this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'setor-funcao-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'addNew' => false,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'Setor' => array(
                'header' => 'Setor',
                'name' => 'Setor',
                'value' => '$data->iDSetor',
            ),
            'Funcao' => array(
                'header' => 'Função',
                'name' => 'Funcao',
                'value' => '$data->iDFuncao',
            ),
            array(
                'name' => 'ativo_setorFuncao',
                'filter' => [1 => 'Sim', 0 => 'Não'],
                'type' => 'boolean',
            ),
            'nr_declaradoTrabalhadores',
            array(
                'value' => 'SetorFuncao::model()->getNrRealTrabalhadores($data->IDSetorFuncao)',
                'header' => 'Número Real de Trabalhadores',
                'filter' => '',
            ),
            /*
            'dt_associacao',
            */
            array(
                'header' => array('10' => '10', '20' => '20', '50' => '50', '100' => '100'),
                'class' => 'ButtonColumn',
                'template' => '{view}{update}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false'),
                            'isModal' => true
                        ),
                    ),
            ),
        ),
    )
);
?>