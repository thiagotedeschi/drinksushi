<?php
/* @var $this EfetivoSetorFuncaoController */
/* @var $model SetorFuncao */
/* @var $form CActiveForm */
?>


<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'setor-funcao-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
); ?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>



    <div class="control-group">
        <?php echo $form->labelEx($model, 'nr_declaradoTrabalhadores', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'nr_declaradoTrabalhadores', array('class' => 'm-wrap span6')); ?>
            <?php echo $form->error($model, 'nr_declaradoTrabalhadores', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
