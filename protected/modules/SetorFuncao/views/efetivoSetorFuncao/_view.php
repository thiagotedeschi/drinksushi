<?php
/* @var $this EfetivoSetorFuncaoController */
/* @var $data SetorFuncao */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDSetorFuncao')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDSetorFuncao), array('view', 'id' => $data->IDSetorFuncao)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDSetor')); ?>:</b>
    <?php echo Html::encode($data->IDSetor); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDFuncao')); ?>:</b>
    <?php echo Html::encode($data->IDFuncao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_associacao')); ?>:</b>
    <?php echo Html::encode($data->dt_associacao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('ativo_setorFuncao')); ?>:</b>
    <?php echo Html::encode($data->ativo_setorFuncao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nr_declaradoTrabalhadores')); ?>:</b>
    <?php echo Html::encode($data->nr_declaradoTrabalhadores); ?>
    <br/>


</div>