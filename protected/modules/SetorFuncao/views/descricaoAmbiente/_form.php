<?php
/* @var $this DescricaoAmbienteController */
/* @var $model DescricaoAmbiente */
/* @var $form CActiveForm */
?>


<?php
if (!$model->isNewRecord) {
    $model->dt_levantamentoDescAmbiente = HData::sqlToBr($model->dt_levantamentoDescAmbiente);
}
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'descricao-ambiente-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDEmpregador',
                Yii::app()->session['empregadores'],
                array(
                    'class' => 'm-wrap span6 select2',
                    'options' => array(
                        Yii::app()->user->getState('IDEmpregador') => array('selected' => true)
                    ),
                    'disabled' => $model->isNewRecord ? '' : 'true',
                    'prompt' => 'Selecione um Empregador',
                    'ajax' => array(
                        'type' => 'POST', //request type
                        'url' => Yii::app()->createURL('SetorFuncao/setorFuncao/ajaxSetorByEmpregador'), //url to call.
                        'update' => 'select#' . Html::activeId($model, 'IDSetor'), //selector to update
                        'beforeSend' => "function(){ $('#" . Html::activeId(
                                $model,
                                'IDSetor'
                            ) . "').attr('disabled', true)}",
                        'data' => array('IDEmpregador' => 'js:this.value'),
                        'complete' => "function(){ $('#" . Html::activeId(
                                $model,
                                'IDSetor'
                            ) . "').trigger('change').attr('disabled', false)}"
                    )
                )
            ); ?>
            <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDSetor', array('class' => 'control-label')); ?>
        <?php echo $this->retornaAjuda(17) ?>
        <div class="controls">
            <?php
            if (isset($model->IDEmpregador)) {
                $IDEmpregador = $model->IDEmpregador;
            } else {
                $IDEmpregador = Yii::app()->user->getState('IDEmpregador');
            }
            echo $form->dropDownList(
                $model,
                'IDSetor',
                Html::listData(
                    $IDEmpregador ? SetorFuncao::model()->with('iDSetor')->ativo()->semDescricaoAmbiente()->findAll(
                        '"iDSetor"."IDEmpregador" = ' . $IDEmpregador
                    ) : array(),
                    'IDSetor',
                    'iDSetor.nome_setor'
                ),
                array(
                    'class' => 'm-wrap select2 span4',
                    'multiple' => true,
                    'disabled' => $model->isNewRecord ? '' : 'true',
                    'ajax' => array(
                        'type' => 'POST',
                        //request type
                        'url' => $this->createUrl('ajaxFuncoesBySetorSemAmbiente'),
                        //url to call.
                        'update' => 'select#' . Html::activeId($model, 'funcoes'),
                        //selector to update
                        'beforeSend' => "function(){ $('#" . Html::activeId(
                                $model,
                                'funcoes'
                            ) . "').attr('disabled', true)}",
                        'data' => 'js:{IDSetors: $("#' . Html::activeId($model, 'IDSetor') . '").val()}',
                        'complete' => "function(){ $('#" . Html::activeId(
                                $model,
                                'funcoes'
                            ) . "').trigger('change').attr('disabled', false)}"
                    )
                )
            );
            ?>
            <?php echo $form->error($model, 'IDSetor', array('class' => 'help-inline')); ?>
        </div>
    </div>


    <div class="control-group">
        <?php echo $form->labelEx($model, 'funcoes', array('class' => 'control-label')); ?>
        <?php echo $this->retornaAjuda(10);
        if (isset($model->funcoes) && $model->isNewRecord) {
            $funcoes = Html::listData(
                SetorFuncao::model()->semDescricaoAmbiente()->findAllByAttributes(['IDSetor' => $model->IDSetor]),
                'IDFuncao',
                'iDFuncao'
            );
        } else {
            $funcoes = Html::listData(
                SetorFuncao::model()->findAllByAttributes(['IDSetor' => $model->IDSetor]),
                'IDFuncao',
                'iDFuncao'
            );
        }
        ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'funcoes',
                $funcoes,
                array('class' => 'select2', 'multiple' => 'true', 'disabled' => $model->isNewRecord ? '' : 'true')
            ); ?>
            <?php echo $form->error($model, 'funcoes', array('class' => 'help-inline')); ?>
        </div>
    </div>


    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_levantamentoDescAmbiente', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'dt_levantamentoDescAmbiente',
                array('class' => 'date-picker', 'maxlength' => 10)
            ); ?>
            <?php echo $form->error($model, 'dt_levantamentoDescAmbiente', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_alvenaria', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'desc_alvenaria',
                array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($model, 'desc_alvenaria', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_iluminacao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'desc_iluminacao',
                array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($model, 'desc_iluminacao', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_ventilacao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'desc_ventilacao',
                array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($model, 'desc_ventilacao', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_mobiliario', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'desc_mobiliario',
                array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($model, 'desc_mobiliario', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_janelas', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'desc_janelas',
                array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($model, 'desc_janelas', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_outros', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'desc_outros',
                array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($model, 'desc_outros', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'finalidade_localDescAmbiente', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'finalidade_localDescAmbiente',
                array('class' => 'm-wrap span6', 'maxlength' => 512)
            ); ?>
            <?php echo $form->error($model, 'finalidade_localDescAmbiente', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'funcao_localDescAmbiente', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'funcao_localDescAmbiente',
                array('class' => 'm-wrap span6', 'maxlength' => 512)
            ); ?>
            <?php echo $form->error($model, 'funcao_localDescAmbiente', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
