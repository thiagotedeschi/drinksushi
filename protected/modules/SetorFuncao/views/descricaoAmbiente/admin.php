<?php
/* @var $this DescricaoAmbienteController */
/* @var $model DescricaoAmbiente */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

<div id="modal-disable" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div clas="col-md-12">
                <iframe src="" style="width:100%; overflow: hidden; min-height:400px;"></iframe>
            </div>
        </div>
    </div>
</div>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada
    pesquisa.
</p>

<?php if (!Yii::app()->user->getState('IDEmpregador')) { ?>
    <p class="note note-danger">
        Para visualizar essa página é necessário selecionar um <b>Empregador</b>.
    </p>
<?php
}

$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'descricao-ambiente-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDDescricaoAmbiente' => array(
                'name' => 'IDDescricaoAmbiente',
                'header' => 'Buscar por ID'
            ),
            /* 'dt_levantamentoDescAmbiente',
              'dt_cadastroDescAmbiente',

              'desc_alvenaria',
              'desc_iluminacao',
              'desc_ventilacao',
              'desc_mobiliario',
              'desc_janelas',
              'desc_outros',
              'finalidade_localDescAmbiente',
              'funcao_localDescAmbiente', */
            'Setor' => array(
                'header' => 'Setor',
                'name' => 'Setor',
                'value' => '$data->iDSetorFuncao->iDSetor',
            ),
            'Funcao' => array(
                'header' => 'Função',
                'name' => 'Funcao',
                'value' => '$data->iDSetorFuncao->iDFuncao',
            ),
            'Empregador' => array(
                'header' => 'Empregador',
                'name' => 'iDSetorFuncao.iDSetor.iDEmpregador',
                'value' => '$data->iDSetorFuncao->iDSetor->iDEmpregador',
            ),
            'Ativo' => array(
                'header' => 'Ativo?',
                'name' => 'ativo_descAmbiente',
                'value' => '$data->ativo_descAmbiente?"Sim":"Não"',
                'filter' => array(
                    '0' => 'Não',
                    '1' => 'Sim'
                )
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{disable}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                        'disable' => array(
                            'options' => array('class' => 'botao desativar', 'title' => 'Ativar/Desativar'),
                            'imageUrl' => false,
                            'label' => '"<i class=\'$data->icone icon-large\'></i>"',
                            'isModal' => true
                        ),
                    ),
            ),
        ),
    )
);
?>