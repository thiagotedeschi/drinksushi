<?php

/* @var $this DescricaoAmbienteController */
/* @var $model DescricaoAmbiente */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDDescricaoAmbiente',
            'dt_levantamentoDescAmbiente:date',
            'dt_cadastroDescAmbiente:date',
            'desc_alvenaria',
            'desc_iluminacao',
            'desc_ventilacao',
            'desc_mobiliario',
            'desc_janelas',
            'desc_outros',
            'finalidade_localDescAmbiente',
            'funcao_localDescAmbiente',
            'IDSetorFuncao',
            'IDSetorFuncao' => array(
                'label' => 'Setor/Função',
                'value' => Html::link(
                        $model->iDSetorFuncao->labelSetorFuncao,
                        Yii::app()->createUrl('SetorFuncao/setorFuncao/view', array('id' => $model->IDSetorFuncao)),
                        array('target' => '_blank')
                    ),
                'type' => 'raw'
            ),
            'historicoDescricaoAmbiente' => array(
                'value' => $this->renderPartial(
                        'historico',
                        array(
                            'model' => $model,
                        ),
                        true
                    ),
                'type' => 'raw',
                'name' => 'Histórico'
            ),
        ),
    )
);
?>
