<?php
/* @var $this DescricaoAmbienteController */
/* @var $model DescricaoAmbiente */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDDescricaoAmbiente'); ?>
        <?php echo $form->textField($model, 'IDDescricaoAmbiente', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_levantamentoDescAmbiente'); ?>
        <?php echo $form->textField($model, 'dt_levantamentoDescAmbiente', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_cadastroDescAmbiente'); ?>
        <?php echo $form->textField($model, 'dt_cadastroDescAmbiente', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_alvenaria'); ?>
        <?php echo $form->textField($model, 'desc_alvenaria', array('class' => 'm-wrap span6', 'maxlength' => 512)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_iluminacao'); ?>
        <?php echo $form->textField($model, 'desc_iluminacao', array('class' => 'm-wrap span6', 'maxlength' => 512)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_ventilacao'); ?>
        <?php echo $form->textField($model, 'desc_ventilacao', array('class' => 'm-wrap span6', 'maxlength' => 512)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_mobiliario'); ?>
        <?php echo $form->textField($model, 'desc_mobiliario', array('class' => 'm-wrap span6', 'maxlength' => 512)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_janelas'); ?>
        <?php echo $form->textField($model, 'desc_janelas', array('class' => 'm-wrap span6', 'maxlength' => 512)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_outros'); ?>
        <?php echo $form->textField($model, 'desc_outros', array('class' => 'm-wrap span6', 'maxlength' => 512)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'finalidade_localDescAmbiente'); ?>
        <?php echo $form->textField(
            $model,
            'finalidade_localDescAmbiente',
            array('class' => 'm-wrap span6', 'maxlength' => 512)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'funcao_localDescAmbiente'); ?>
        <?php echo $form->textField(
            $model,
            'funcao_localDescAmbiente',
            array('class' => 'm-wrap span6', 'maxlength' => 512)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDSetorFuncao'); ?>
        <?php echo $form->textField($model, 'IDSetorFuncao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->