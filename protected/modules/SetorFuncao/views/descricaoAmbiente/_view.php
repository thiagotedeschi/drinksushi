<?php
/* @var $this DescricaoAmbienteController */
/* @var $data DescricaoAmbiente */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDDescricaoAmbiente')); ?>:</b>
    <?php echo Html::link(
        Html::encode($data->IDDescricaoAmbiente),
        array('view', 'id' => $data->IDDescricaoAmbiente)
    ); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_levantamentoDescAmbiente')); ?>:</b>
    <?php echo Html::encode($data->dt_levantamentoDescAmbiente); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_cadastroDescAmbiente')); ?>:</b>
    <?php echo Html::encode($data->dt_cadastroDescAmbiente); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_alvenaria')); ?>:</b>
    <?php echo Html::encode($data->desc_alvenaria); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_iluminacao')); ?>:</b>
    <?php echo Html::encode($data->desc_iluminacao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_ventilacao')); ?>:</b>
    <?php echo Html::encode($data->desc_ventilacao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_mobiliario')); ?>:</b>
    <?php echo Html::encode($data->desc_mobiliario); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('desc_janelas')); ?>:</b>
	<?php echo Html::encode($data->desc_janelas); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('desc_outros')); ?>:</b>
	<?php echo Html::encode($data->desc_outros); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('finalidade_localDescAmbiente')); ?>:</b>
	<?php echo Html::encode($data->finalidade_localDescAmbiente); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('funcao_localDescAmbiente')); ?>:</b>
	<?php echo Html::encode($data->funcao_localDescAmbiente); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDSetorFuncao')); ?>:</b>
	<?php echo Html::encode($data->IDSetorFuncao); ?>
	<br />

	*/
    ?>

</div>