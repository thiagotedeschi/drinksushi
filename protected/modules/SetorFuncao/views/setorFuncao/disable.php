<?php
/* @var $this HistoricoSetorFuncaoController */
/* @var $model HistoricoSetorFuncao */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'historico-setor-funcao-disable-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>

    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com * são obrigatórios.</p>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'dt_modificacaoHistoricoSF', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'dt_modificacaoHistoricoSF',
                    array('class' => 'date-picker', 'maxlength' => 10)
                ); ?>
                <?php echo $form->error($model, 'dt_modificacaoHistoricoSF', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'motivo_historicoSF', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textArea($model, 'motivo_historicoSF'); ?>
                <?php echo $form->error($model, 'motivo_historicoSF', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <?php echo Html::submitButton($setorFuncao->labelAcao . ' Setor X Função', array('class' => 'botao')); ?>
    </div>

<?php $this->endWidget(); ?>