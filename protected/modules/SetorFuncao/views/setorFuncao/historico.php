<?php
/**
 * Tabela que reúne as informações do histórico
 * @var $model SetorFuncao
 */
if ($model->historicoSetorFuncaos) {
    ?>
    <table class="table table-centered table-bordered">
        <thead>
        <tr>
            <th>Data de Modificação</th>
            <th>Motivo</th>
            <th>Tipo de Modificação</th>
            <th>Usuário Responsável</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($model->historicoSetorFuncaos as $value) {
            if ($value->ativacao_historicoSF) {
                $tipoAlteracao = 'Ativação';
            } else {
                $tipoAlteracao = 'Desativação';
            }
            ?>
            <tr>
                <td><?=
                    HData::formataData(
                        $value->dt_modificacaoHistoricoSF,
                        HData::BR_DATE_FORMAT,
                        HData::SQL_DATE_FORMAT
                    ) ?></td>
                <td><?= $value->motivo_historicoSF ?></td>
                <td><?= $tipoAlteracao ?></td>
                <td><?= $value->iDUsuarioResponsavel->login_usuario ?></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
<?php
} else {
    echo "Nenhuma alteração foi realizada";
}
?>