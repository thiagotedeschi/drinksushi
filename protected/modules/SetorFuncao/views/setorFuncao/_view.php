<?php
/* @var $this SetorFuncaoController */
/* @var $data SetorFuncao */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDSetorFuncao')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDSetorFuncao), array('view', 'id' => $data->IDSetorFuncao)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDSetor')); ?>:</b>
    <?php echo Html::encode($data->IDSetor); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDFuncao')); ?>:</b>
    <?php echo Html::encode($data->IDFuncao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_atividadeSetorFuncao')); ?>:</b>
    <?php echo Html::encode($data->desc_atividadeSetorFuncao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_associacao')); ?>:</b>
    <?php echo Html::encode($data->dt_associacao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('ativo_setorFuncao')); ?>:</b>
    <?php echo Html::encode($data->ativo_setorFuncao); ?>
    <br/>


</div>