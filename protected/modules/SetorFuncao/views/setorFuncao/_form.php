<?php
/* @var $this SetorFuncaoController */
/* @var $model SetorFuncao */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'setor-funcao-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>

<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <?php if (!$model->isNewRecord) {
        $model->dt_associacao = HData::formataData(
            $model->dt_associacao,
            HData::BR_DATE_FORMAT,
            HData::SQL_DATE_FORMAT
        );
    } else {
        ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'IDEmpregador',
                    Yii::app()->session['empregadores'],
                    array(
                        'class' => 'm-wrap span6 select2',
                        'prompt' => 'Selecione um Empregador',
                        'options' => array(
                            Yii::app()->user->getState('IDEmpregador') => array('selected' => true)
                        )
                    )
                ); ?>
                <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDSetor', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                if (isset($model->IDEmpregador)) {
                    $IDEmpregador = $model->IDEmpregador;
                } else {
                    $IDEmpregador = Yii::app()->user->getState('IDEmpregador');
                }
                echo $form->dropDownList(
                    $model,
                    'IDSetor',
                    Html::listData(
                        $IDEmpregador ? Setor::model()->getSetoresEmpregador($IDEmpregador) :
                            array(),
                        'IDSetor',
                        'nome_setor'
                    ),
                    array(
                        'class' => 'm-wrap select2 span4',
                        'prompt' => 'Selecione um Setor',
                    )
                );
                ?>
                <?php echo $form->error($model, 'IDSetor', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDFuncao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
                echo $form->dropDownList(
                    $model,
                    'IDFuncao',
                    Html::listData(
                        $IDEmpregador ? Funcao::model()->findByEmpregador($IDEmpregador) : array(),
                        'IDFuncao',
                        'nome_funcao'
                    ),
                    array(
                        'class' => 'm-wrap select2 span4',
                        'multiple' => true
                    )
                );
                ?>
                <?php echo $form->error($model, 'IDSetor', array('class' => 'help-inline')); ?>
            </div>
        </div>
    <?php } ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_associacao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'dt_associacao', array('class' => 'date-picker', 'maxlength' => 10)); ?>
            <?php echo $form->error($model, 'dt_associacao', array('class' => 'help-inline')); ?>
        </div>
    </div>

</div>

<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>

<script>
    $('select#<?= Html::activeId($model, 'IDEmpregador')?>').change(function () {
        $.ajax({
            url: "<?=$this->createUrl('ajaxSetoresFuncoesByEmpregador')?>",
            type: 'POST',
            dataType: 'JSON',
            data: {"IDEmpregador": $(this).val()},
            success: function (data) {
                //atualiza setores
                $selectSetor = $('select#<?=Html::activeId($model, 'IDSetor')?>');
                $selectSetor.html('<option selected>Selecione um Setor</option>').trigger('change');
                $.each(data.setor, function (i, value) {
                    $selectSetor.append($('<option>').text(value).attr('value', i));
                });
                //atualiza funções
                $selectFuncao = $('select#<?=Html::activeId($model, 'IDFuncao')?>');
                $selectFuncao.html('').trigger('change');
                $.each(data.funcao, function (i, value) {
                    $selectFuncao.append($('<option>').text(value).attr('value', i));
                });
            }
        })
    });
</script>
