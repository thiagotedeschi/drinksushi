<?php
/* @var $this SetorFuncaoController */
/* @var $model SetorFuncao */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

<div id="modal-disable" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="col-md-12">
                <iframe src="" style="width:100%; overflow: hidden; min-height:400px;"></iframe>
            </div>
        </div>
    </div>
</div>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>

<?php if (!Yii::app()->user->getState('IDEmpregador')) { ?>
    <p class="note note-danger">
        Para visualizar essa página é necessário selecionar um <b>Empregador</b>.
    </p>
<?php } ?>


<?php
$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'setor-funcao-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'Setor' => array(
                'header' => 'Setor',
                'name' => 'Setor',
                'value' => '$data->iDSetor',
            ),
            'Funcao' => array(
                'header' => 'Função',
                'name' => 'Funcao',
                'value' => '$data->iDFuncao',
            ),
            'dt_associacao' => array(
                'value' => '$data->dt_associacao',
                'header' => 'Data de Associação',
                'filter' => false,
                'type' => 'date',
                'name' => 'dt_associacao'
            ),
            'iDSetor.iDEmpregador:text:Empregador',
            'ativo_setorFuncao' => array(
                'value' => '$data->ativo_setorFuncao',
                'type' => 'boolean',
                'name' => 'ativo_setorFuncao',
                'filter' => array(
                    '0' => 'Não',
                    '1' => 'Sim'
                )
            ),
            array(
                'class' => 'ButtonColumn',
                'header' => array('10' => '10', '20' => '20', '50' => '50', '100' => '100'),
                'template' => '{view}{update}{disable}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false'),
                        ),
//                'update' => array(
//                    'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
//                ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                        'disable' => array(
                            'options' => array('class' => 'botao desativar', 'title' => 'Ativar/Desativar'),
                            'imageUrl' => false,
                            'label' => '"<i class=\'$data->icone icon-large\'></i>"',
                            'isModal' => true
                        ),
                        'update' => array(
                            'isModal' => true
                        )
                    ),
            ),
        ),
    )
);

?>

<script>

    function openDesassociarDialog(a) {
        $a = jQuery(a);
        $iframe = $('#desassociarIFrame');
        $wrapper = $("#desassociarWrapper");
        $iframe.attr('src', $a.attr('href'));
        $wrapper.dialog("open");
        return false;
    }
</script>