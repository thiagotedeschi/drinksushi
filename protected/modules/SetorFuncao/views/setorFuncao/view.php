<?php
/* @var $this SetorFuncaoController */
/* @var $model SetorFuncao */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDSetorFuncao',
            'IDSetor' => array(
                'label' => 'Setor',
                'value' => Html::link(
                        $model->iDSetor,
                        Yii::app()->createUrl('SetorFuncao/Setor/view', array('id' => $model->IDSetor)),
                        array('target' => '_blank')
                    ),
                'type' => 'raw',
            ),
            'IDFuncao' => array(
                'label' => 'Funcão',
                'value' => Html::link(
                        $model->iDFuncao,
                        Yii::app()->createUrl('SetorFuncao/Funcao/view', array('id' => $model->IDFuncao)),
                        array('target' => '_blank')
                    ),
                'type' => 'raw',
            ),
            'dt_associacao:date',
            'ativo_setorFuncao' => array(
                'value' => $model->ativo_setorFuncao,
                'type' => 'boolean',
                'name' => 'ativo_setorFuncao',
                'filter' => array(
                    '0' => 'Não',
                    '1' => 'Sim'
                )
            ),
            'historicoSetorFuncaos' => array(
                'value' => $this->renderPartial(
                        '../setorFuncao/historico',
                        array(
                            'model' => $model,
                        ),
                        true
                    ),
                'type' => 'raw',
                'name' => 'Histórico'
            ),
            'nr_declaradoTrabalhadores',
            array(
                'value' => $model->nrRealTrabalhadores,
                'label' => 'Número Real de Trabalhadores',
            ),
        ),
    )
); ?>
