<?php
/* @var $this SetorFuncaoController */
/* @var $model SetorFuncao */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDSetorFuncao'); ?>
        <?php echo $form->textField($model, 'IDSetorFuncao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDSetor'); ?>
        <?php echo $form->textField($model, 'IDSetor', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDFuncao'); ?>
        <?php echo $form->textField($model, 'IDFuncao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_associacao'); ?>
        <?php echo $form->textField($model, 'dt_associacao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ativo_setorFuncao'); ?>
        <?php echo $form->checkBox($model, 'ativo_setorFuncao'); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->