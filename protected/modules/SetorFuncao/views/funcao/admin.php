<?php

/* @var $this FuncaoController */
/* @var $model Funcao */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
//$model->empresaFuncao;
?>
<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>

<?php if (!Yii::app()->user->getState('IDEmpregador')) { ?>
    <p class="note note-danger">
        Para visualizar essa página é necessário selecionar um <b>Empregador</b>.
    </p>
<?php } ?>


<?php

$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'funcao-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'nome_funcao',
            'dt_levantamentoFuncao' => array(
                'filter' => '',
                'header' => 'Data de Levantamento',
                'type' => 'Date',
                'name' => 'dt_levantamentoFuncao',
                'value' => '$data->dt_levantamentoFuncao'
            ),
            'empresaFuncao' => array(
                'header' => 'Empregador',
                'filter' => '',
                'value' => '$data->empresaFuncao',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false'),
                            'isModal' => true
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>