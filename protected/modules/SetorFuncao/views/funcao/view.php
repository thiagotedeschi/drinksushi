<?php

/* @var $this FuncaoController */
/* @var $model Funcao */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

$setores = implode(', ', $model->setores());

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDFuncao',
            'nome_funcao',
            'dt_cadastroFuncao:date',
            'dt_levantamentoFuncao:date',
            'setores' => array('value' => $setores, 'label' => 'Setores ')
        ),
    )
);
?>
