<?php
/* @var $this FuncaoController */
/* @var $model Funcao */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'funcao-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>

    <?= $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDEmpregador',
                Yii::app()->session['empregadores'],
                array(
                    'class' => 'm-wrap span6 select2',
                    'prompt' => 'Selecione um Empregador',
                    'options' => array(
                        Yii::app()->user->getState('IDEmpregador') => array('selected' => true)
                    ),
                    'ajax' => array(
                        'type' => 'POST', //request type
                        'url' => Yii::app()->createURL('SetorFuncao/setorFuncao/ajaxSetorByEmpregador'), //url to call.
                        'update' => 'select#' . Html::activeId($model, 'setores_funcao'), //selector to update
                        'data' => array('IDEmpregador' => 'js:this.value'),
                        'complete' => "$('.select2-search-choice').remove()"
                    )
                )
            ); ?>
            <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">

        <?php echo $form->labelEx($model, 'setores_funcao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            if (isset($model->IDEmpregador)) {
                $IDEmpregador = $model->IDEmpregador;
            } else {
                $IDEmpregador = Yii::app()->user->getState('IDEmpregador');
            }
            echo $form->dropDownList(
                $model,
                'setores_funcao',
                Html::listData(
                    $IDEmpregador ? Setor::model()->getSetoresEmpregador($IDEmpregador) :
                        array(),
                    'IDSetor',
                    'nome_setor'
                ),
                array('class' => 'm-wrap span6 select2', 'multiple' => 'multiple')
            );
            echo $form->error($model, 'setores_funcao', array('class' => 'help-inline'));

            ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_funcao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model, 'nome_funcao', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'nome_funcao', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_levantamentoFuncao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'dt_levantamentoFuncao',
                array('class' => 'date-picker', 'maxlength' => 10)
            ); ?>
            <?php echo $form->error($model, 'dt_levantamentoFuncao', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>
<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar Função ' : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
