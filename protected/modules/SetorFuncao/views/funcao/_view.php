<?php
/* @var $this FuncaoController */
/* @var $data Funcao */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDFuncao')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDFuncao), array('view', 'id' => $data->IDFuncao)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_funcao')); ?>:</b>
    <?php echo Html::encode($data->nome_funcao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_levantamentoFuncao')); ?>:</b>
    <?php echo Html::encode($data->dt_levantamentoFuncao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_cadastroFuncao')); ?>:</b>
    <?php echo Html::encode($data->dt_cadastroFuncao); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('ativo_funcao')); ?>:</b>
    <?php echo Html::encode($data->ativo_funcao); ?>
    <br/>

</div>