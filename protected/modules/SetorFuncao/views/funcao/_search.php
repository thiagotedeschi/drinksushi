<?php
/* @var $this FuncaoController */
/* @var $model Funcao */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDFuncao'); ?>
        <?php echo $form->textField($model, 'IDFuncao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_funcao'); ?>
        <?php echo $form->textField($model, 'nome_funcao', array('class' => 'm-wrap span6', 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_levantamentoFuncao'); ?>
        <?php echo $form->textField($model, 'dt_levantamentoFuncao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_cadastroFuncao'); ?>
        <?php echo $form->textField($model, 'dt_cadastroFuncao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ativo_funcao'); ?>
        <?php echo $form->checkBox($model, 'ativo_funcao'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_ultimaModificacaoFuncao'); ?>
        <?php echo $form->textField($model, 'dt_ultimaModificacaoFuncao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->