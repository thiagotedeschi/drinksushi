<?php
/* @var $this DescricaoAtividadeController */
/* @var $model DescricaoAtividade */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDDescricaoAtividade'); ?>
        <?php echo $form->textField($model, 'IDDescricaoAtividade', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'desc_atividade'); ?>
        <?php echo $form->textArea(
            $model,
            'desc_atividade',
            array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_levantamentoDescAtividade'); ?>
        <?php echo $form->textField($model, 'dt_levantamentoDescAtividade', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'dt_associacaoDescAtividade'); ?>
        <?php echo $form->textField($model, 'dt_associacaoDescAtividade', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ativo_descAtividade'); ?>
        <?php echo $form->checkBox($model, 'ativo_descAtividade'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDSetorFuncao'); ?>
        <?php echo $form->textField($model, 'IDSetorFuncao', array('class' => 'm-wrap span6')); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->