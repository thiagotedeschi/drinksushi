<?php
/* @var $this DescricaoAtividadeController */
/* @var $data DescricaoAtividade */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDDescricaoAtividade')); ?>:</b>
    <?php echo Html::link(
        Html::encode($data->IDDescricaoAtividade),
        array('view', 'id' => $data->IDDescricaoAtividade)
    ); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('desc_atividade')); ?>:</b>
    <?php echo Html::encode($data->desc_atividade); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_levantamentoDescAtividade')); ?>:</b>
    <?php echo Html::encode($data->dt_levantamentoDescAtividade); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_associacaoDescAtividade')); ?>:</b>
    <?php echo Html::encode($data->dt_associacaoDescAtividade); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('ativo_descAtividade')); ?>:</b>
    <?php echo Html::encode($data->ativo_descAtividade); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDSetorFuncao')); ?>:</b>
    <?php echo Html::encode($data->IDSetorFuncao); ?>
    <br/>


</div>