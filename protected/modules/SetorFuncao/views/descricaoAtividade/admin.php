<?php
/* @var $this DescricaoAtividadeController */
/* @var $model DescricaoAtividade */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

<style type="text/css">
    .popovers-desc-atividades {
        cursor: pointer;
    }

    .popover {
        max-width: 600px;
    }
</style>

<div id="modal-disable" class="modal hide container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div clas="col-md-12">
                <iframe src="" style="width:100%; overflow: hidden; min-height:400px;"></iframe>
            </div>
        </div>
    </div>
</div>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada
    pesquisa.
</p>


<?php if (!Yii::app()->user->getState('IDEmpregador')) { ?>
    <p class="note note-danger">
        Para visualizar essa página é necessário selecionar um <b>Empregador</b>.
    </p>
<?php } ?>


<?php
$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'descricao-atividade-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'afterAjaxUpdate' => 'function(id, data){getFlashes();getPopOversDescAtividade();}',
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'desc_atividade' => array(
                'header' => 'Descrição de Atividade',
                'name' => 'desc_atividade',
                'value' => '$data->reduzDesc()',
                'type' => 'raw',
            ),
            'Setor' => array(
                'header' => 'Setor',
                'name' => 'Setor',
                'value' => '$data->iDSetorFuncao->iDSetor',
            ),
            'Funcao' => array(
                'header' => 'Função',
                'name' => 'Funcao',
                'value' => '$data->iDSetorFuncao->iDFuncao',
            ),
            'Empregador' => array(
                'header' => 'Empregador',
                'name' => 'iDSetorFuncao.iDSetor.iDEmpregador',
                'value' => '$data->iDSetorFuncao->iDSetor->iDEmpregador',
            ),
            'Ativo' => array(
                'header' => 'Ativo?',
                'name' => 'ativo_descAtividade',
                'value' => '$data->ativo_descAtividade?"Sim":"Não"',
                'filter' => array(
                    '0' => 'Não',
                    '1' => 'Sim'
                )
            ),
            /*
              'dt_levantamentoDescAtividade',
              'IDDescricaoAtividade',
              'dt_associacaoDescAtividade',
              'ativo_descAtividade',
              'IDSetorFuncao',
             */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{disable}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                        'disable' => array(
                            'options' => array('class' => 'botao desativar', 'title' => 'Ativar/Desativar'),
                            'imageUrl' => false,
                            'label' => '"<i class=\'$data->icone icon-large\'></i>"',
                            'isModal' => true
                        ),
                    ),
            ),
        ),
    )
);

?>

<script>

    function getPopOversDescAtividade() {
        jQuery('.popovers-desc-atividades').popover({
            trigger: 'hover',
            placement: 'bottom',
            title: 'Descrição de Atividade'
        });
    }

</script>