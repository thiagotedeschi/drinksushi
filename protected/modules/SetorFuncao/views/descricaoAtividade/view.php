<?php

/* @var $this DescricaoAtividadeController */
/* @var $model DescricaoAtividade */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDDescricaoAtividade',
            'desc_atividade',
            'dt_levantamentoDescAtividade:date',
            'dt_cadastroDescAtividade:date',
            'ativo_descAtividade' => array(
                'label' => 'Ativo?',
                'value' => $model->ativo_descAtividade ? 'Sim' : 'Não',
            ),
            'IDSetorFuncao' => array(
                'label' => 'Setor/Função',
                'value' => Html::link(
                        $model->iDSetorFuncao->labelSetorFuncao,
                        Yii::app()->createUrl('SetorFuncao/setorFuncao/view', array('id' => $model->IDSetorFuncao)),
                        array('target' => '_blank')
                    ),
                'type' => 'raw'
            ),
            'historicoDescricaoAtividades' => array(
                'value' => $this->renderPartial(
                        'historico',
                        array(
                            'model' => $model,
                        ),
                        true
                    ),
                'type' => 'raw',
                'name' => 'Histórico'
            ),
        ),
    )
);
?>
