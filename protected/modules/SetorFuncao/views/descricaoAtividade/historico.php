<?php
/**
 * Tabela que reúne as informações do histórico
 * @var $model SetorFuncao
 */
if ($model->historicoDescricaoAtividades) {
    ?>
    <table class="table table-centered table-bordered">
        <thead>
        <tr>
            <th>Data de Modificação</th>
            <th>Motivo</th>
            <th>Tipo de Modificação</th>
            <th>Usuário Responsável</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($model->historicoDescricaoAtividades as $value) {
            if ($value->ativacao_historicoDescAtividade) {
                $tipoAlteracao = 'Ativação';
            } else {
                $tipoAlteracao = 'Desativação';
            }
            ?>
            <tr>
                <td><?=
                    HData::formataData(
                        $value->dt_modificacaoHistoricoDescAtividade,
                        HData::BR_DATE_FORMAT,
                        HData::SQL_DATE_FORMAT
                    ) ?></td>
                <td><?= $value->motivo_historicoDescAtividade ?></td>
                <td><?= $tipoAlteracao ?></td>
                <td><?= $value->iDUsuarioResponsavel->login_usuario ?></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
<?php
} else {
    echo "Nenhuma alteração foi realizada";
}
?>