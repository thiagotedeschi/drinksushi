<?php
/* @var $this DescricaoAtividadeController */
/* @var $model DescricaoAtividade */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'descricao-atividade-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>

<?php
if (!$model->isNewRecord):
    $model->dt_levantamentoDescAtividade = HData::formataData(
        $model->dt_levantamentoDescAtividade,
        HData::BR_DATE_FORMAT,
        HData::SQL_DATE_FORMAT
    );
endif;
?>

<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com * são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDEmpregador',
                Yii::app()->session['empregadores'],
                array(
                    'class' => 'm-wrap span6 select2',
                    'options' => array(
                        Yii::app()->user->getState('IDEmpregador') => array('selected' => true)
                    ),
                    'disabled' => $model->isNewRecord ? '' : 'true',
                    'prompt' => 'Selecione um Empregador',
                    'ajax' => array(
                        'type' => 'POST',
                        //request type
                        'url' => Yii::app()->createURL('SetorFuncao/setorFuncao/ajaxSetorByEmpregador'),
                        //url to call.
                        'update' => 'select#' . Html::activeId($model, 'IDSetor'),
                        //selector to update
                        'beforeSend' => "function(){ $('#" . Html::activeId(
                                $model,
                                'IDSetor'
                            ) . "').attr('disabled', true)}",
                        'data' => array('IDEmpregador' => 'js:this.value'),
                        'complete' => "function(){ $('#" . Html::activeId(
                                $model,
                                'IDSetor'
                            ) . "').trigger('change').attr('disabled', false)}"
                    )
                )
            ); ?>
            <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDSetor', array('class' => 'control-label')); ?>
        <?php echo $this->retornaAjuda(17) ?>
        <div class="controls">
            <?php
            if (isset($model->IDEmpregador)) {
                $IDEmpregador = $model->IDEmpregador;
            } else {
                $IDEmpregador = Yii::app()->user->getState('IDEmpregador');
            }
            $modelSetor = new Setor();
            echo $form->dropDownList(
                $model,
                'IDSetor',
                Html::listData(
                    $IDEmpregador ? $modelSetor->findByEmpregador($IDEmpregador) : array(),
                    'IDSetor',
                    'nome_setor'
                ),
                array(
                    'class' => 'select2 span4',
                    'prompt' => 'Selecione um Setor',
                    'disabled' => $model->isNewRecord ? '' : 'true',
                    'ajax' => array(
                        'type' => 'POST',
                        //request type
                        'url' => $this->createUrl('ajaxFuncoesBySetorSemAtividade'),
                        //url to call.
                        'update' => 'select#' . Html::activeId($model, 'funcoes'),
                        //selector to update
                        'beforeSend' => "function(){ $('#" . Html::activeId(
                                $model,
                                'funcoes'
                            ) . "').attr('disabled', true)}",
                        'data' => 'js:{IDSetor: $("#' . Html::activeId($model, 'IDSetor') . '").val()}',
                        'complete' => "function(){ $('#" . Html::activeId(
                                $model,
                                'funcoes'
                            ) . "').trigger('change').attr('disabled', false)}"
                    )
                )
            );
            ?>
            <?php echo $form->error($model, 'IDSetor', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'funcoes', array('class' => 'control-label')); ?>
        <?php echo $this->retornaAjuda(10);
        if (isset($model->funcoes) && $model->isNewRecord) {
            $funcoes = Html::listData(
                SetorFuncao::model()->semDescricaoAtividade()->findAllByAttributes(['IDSetor' => $model->IDSetor]),
                'IDFuncao',
                'iDFuncao'
            );
        } else {
            $funcoes = Html::listData(
                SetorFuncao::model()->findAllByAttributes(['IDSetor' => $model->IDSetor]),
                'IDFuncao',
                'iDFuncao'
            );
        }
        ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'funcoes',
                $funcoes,
                array('class' => 'select2 span4', 'multiple' => 'true', 'disabled' => $model->isNewRecord ? '' : 'true')
            ); ?>
            <?php echo $form->error($model, 'funcoes', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'desc_atividade', array('class' => 'control-label')); ?>
        <?php echo $this->retornaAjuda(12) ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'desc_atividade',
                array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($model, 'desc_atividade', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'dt_levantamentoDescAtividade', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'dt_levantamentoDescAtividade',
                array('class' => 'date-picker', 'maxlength' => 10)
            ); ?>
            <?php echo $form->error($model, 'dt_levantamentoDescAtividade', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>

<script type="text/javascript">
    $('#<?= Html::activeId($model, 'desc_atividade') ?>').blur(function () {
        n_caracteres = $('#<?= Html::activeId($model, 'desc_atividade') ?>').val().length
        if (n_caracteres > 400) {
            $('.botaoAjuda:first').trigger('click');
        }
    });
</script>
<?php $this->endWidget(); ?>
