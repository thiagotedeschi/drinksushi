<?php
/* @var $this HistoricoDescricaoAtividadeController */
/* @var $model HistoricoDescricaoAtividade */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'historico-setor-funcao-disable-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>

    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com * são obrigatórios.</p>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx(
                $model,
                'dt_modificacaoHistoricoDescAtividade',
                array('class' => 'control-label')
            ); ?>
            <div class="controls">
                <?php echo $form->textField(
                    $model,
                    'dt_modificacaoHistoricoDescAtividade',
                    array('class' => 'date-picker', 'maxlength' => 10)
                ); ?>
                <?php echo $form->error(
                    $model,
                    'dt_modificacaoHistoricoDescAtividade',
                    array('class' => 'help-inline')
                ); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'motivo_historicoDescAtividade', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textArea($model, 'motivo_historicoDescAtividade'); ?>
                <?php echo $form->error($model, 'motivo_historicoDescAtividade', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php echo Html::submitButton(
            $descAtividade->labelAcao . ' Descrição de Atividade',
            array('class' => 'botao')
        ); ?>
    </div>

<?php $this->endWidget(); ?>