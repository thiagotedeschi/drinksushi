<?php


class DicaController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => 22,
        'search' => 23,
        'view' => 29,
        'update' => 30,
        'delete' => 31,
    );

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar' . $model->labelModel() . ' #' . $id;

        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Dica;
        $this->pageTitle = 'Criar ' . $model->labelModel();

        $this->performAjaxValidation($model);

        if (isset($_POST['Dica'])) {
            $model->attributes = $_POST['Dica'];
            if ($model->save()) {

                $this->redirect(array('search', 'id' => $model->IDDica));
            }
        }

        //retorna todos os tipos de dicas existentes
        $array_tipoDicas = Yii::app()->db->createCommand()
            ->select('IDTipo_dica, nome_tipoDica')
            ->from('public.Tipo_dica')
            ->queryAll();
        //transforma o resultado da consulta em um array chave/valor para montar o dropdown
        foreach ($array_tipoDicas as $tipoDica) {
            $tipoDicas[$tipoDica['IDTipo_dica']] = $tipoDica['nome_tipoDica'];
        }
        //retorna todos os tipos de dicas existentes
        $array_acao = Yii::app()->db->createCommand()
            ->select('IDAcao, nome_acao')
            ->from('public.Acao')
            ->queryAll();
        //transforma o resultado da consulta em um array chave/valor para montar o dropdown 
        foreach ($array_acao as $acao) {
            $acoes[$acao['IDAcao']] = $acao['nome_acao'];
        }

        $this->render(
            'create',
            array(
                'model' => $model,
                'tipoDicas' => $tipoDicas, //array chave/valor para o dropdown de Tipo de Dicas
                'acoes' => $acoes, //array chave/valor para o dropdown de Ação
            )
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Editar ' . $model->labelModel() . ' #' . $id;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Dica'])) {
            $model->attributes = $_POST['Dica'];
            if ($model->save()) {

                $this->redirect(array('view', 'id' => $model->IDDica));
            }
        }

        $array_tipoDicas = Yii::app()->db->createCommand()
            ->select('IDTipo_dica, nome_tipoDica')
            ->from('public.Tipo_dica')
            ->queryAll();

        foreach ($array_tipoDicas as $tipoDica) {
            $tipoDicas[$tipoDica['IDTipo_dica']] = $tipoDica['nome_tipoDica'];
        }

        $array_acao = Yii::app()->db->createCommand()
            ->select('IDAcao, nome_acao')
            ->from('public.Acao')
            ->queryAll();

        foreach ($array_acao as $acao) {
            $acoes[$acao['IDAcao']] = $acao['nome_acao'];
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'tipoDicas' => $tipoDicas,
                'acoes' => $acoes,
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    /**
     * Manages all models.
     */
    public function actionSearch()
    {
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Dica('search');
        $this->pageTitle = 'Buscar ' . $model->labelModel();

        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Dica'])) {
            $model->attributes = $_GET['Dica'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Dica the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Dica::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Dica $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'dica-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
