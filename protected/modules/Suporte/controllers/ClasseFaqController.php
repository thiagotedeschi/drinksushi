<?php


class ClasseFaqController extends Controller
{
    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => '45',
        'search' => '46',
        'view' => '47',
        'update' => '48',
        'delete' => '49',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $actionDefault = 'search';

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $model = new ClasseFaq;
        $this->pageTitle = 'Criar ' . $model->labelModel();

        $this->performAjaxValidation($model);

        if (isset($_POST['ClasseFaq'])) {
            $model->attributes = $_POST['ClasseFaq'];
            if ($model->save()) {

                $this->redirect(array('search', 'id' => $model->IDClasseFaq));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Editar ' . $model->labelModel() . ' #' . $id;


        $this->performAjaxValidation($model);

        if (isset($_POST['ClasseFaq'])) {
            $model->attributes = $_POST['ClasseFaq'];
            if ($model->save()) {

                $this->redirect(array('view', 'id' => $model->IDClasseFaq));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new ClasseFaq('search');
        $this->pageTitle = 'Buscar ' . $model->labelModel();
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['ClasseFaq'])) {
            $model->attributes = $_GET['ClasseFaq'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ClasseFaq the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ClasseFaq::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ClasseFaq $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'classe-faq-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
