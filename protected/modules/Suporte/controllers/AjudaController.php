<?php


class AjudaController extends Controller
{

    /**
     * @var Array Ações necessárias (valor) para execução das Actions listadas (chave)
     */
    public $acoesActions = array(
        'create' => 26,
        'search' => 27,
        'view' => 33,
        'update' => 32,
        'delete' => 34
    );
    /**
     * @var string Action Padrão
     */
    public $defaultAction = 'search';

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . Ajuda::model()->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Ajuda;

        $this->performAjaxValidation($model);

        if (isset($_POST['Ajuda'])) {
            $model->attributes = $_POST['Ajuda'];
            if ($model->save()) {
                $this->redirect(array('search'));
            }
        }

        $array_acao = Yii::app()->db->createCommand()
            ->select('IDAcao, nome_acao')
            ->from('public.Acao')
            ->order('nome_acao')
            ->queryAll();

        foreach ($array_acao as $acao) {
            $acoes[$acao['IDAcao']] = $acao['nome_acao'];
        }

        $this->pageTitle = 'Criar ' . $model->labelModel();
        $this->render(
            'create',
            array(
                'model' => $model,
                'acoes' => $acoes,
            )
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Editar' . $model->labelModel() . ' #' . $id;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Ajuda'])) {
            $model->attributes = $_POST['Ajuda'];
            if ($model->save()) {

                $this->redirect(array('view', 'id' => $model->IDAjuda));
            }
        }

        $array_acao = Yii::app()->db->createCommand()
            ->select('IDAcao, nome_acao')
            ->from('public.Acao')
            ->order('nome_acao')
            ->queryAll();

        foreach ($array_acao as $acao) {
            $acoes[$acao['IDAcao']] = $acao['nome_acao'];
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'acoes' => $acoes,
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }


    /**
     * Manages all models.
     */
    public function actionSearch()
    {
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }

        $model = new Ajuda('search');
        $this->pageTitle = 'Buscar ' . $model->labelModel();
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Ajuda'])) {
            $model->attributes = $_GET['Ajuda'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Ajuda the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Ajuda::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Ajuda $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'ajuda-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
