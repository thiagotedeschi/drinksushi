<?php

/* @var $this DicaController */
/* @var $model Dica */

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'titulo_dica',
            'texto_dica',
            'IDTipo_dica' => array(
                'name' => 'Tipo Dica',
                'type' => 'raw',
                'value' => $model->TipoDica->nome_tipoDica
            ),
            'IDAcao' => array(
                'name' => 'Ação',
                'type' => 'raw',
                'value' => $model->iDAcao->nome_acao
            ),
        ),
    )
);
?>
