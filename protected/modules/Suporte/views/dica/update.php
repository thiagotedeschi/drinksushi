<?php

/* @var $this DicaController */
/* @var $model Dica */
/* @var $tipoDicas array */
/* @var $acoes array */

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

$this->renderPartial('_form', array('model' => $model, 'tipoDicas' => $tipoDicas, 'acoes' => $acoes,));
?>