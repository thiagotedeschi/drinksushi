<?php
/* @var $this DicaController */
/* @var $data Dica */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDDica')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDDica), array('view', 'id' => $data->IDDica)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('titulo_dica')); ?>:</b>
    <?php echo Html::encode($data->titulo_dica); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('texto_dica')); ?>:</b>
    <?php echo Html::encode($data->texto_dica); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDTipo_dica')); ?>:</b>
    <?php echo Html::encode($data->IDTipo_dica); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDAcao')); ?>:</b>
    <?php echo Html::encode($data->IDAcao); ?>
    <br/>


</div>