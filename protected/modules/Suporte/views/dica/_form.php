<?php
/* @var $this DicaController */
/* @var $model Dica */
/* @var $form CActiveForm */
/* @var $tipoDicas array */
/* @var $acoes array */

$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'dica-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>

        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'titulo_dica', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textField($model, 'titulo_dica', array('class' => 'm-wrap span6',)); ?>
                <?php echo $form->error($model, 'titulo_dica', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'texto_dica', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->textArea(
                    $model,
                    'texto_dica',
                    array('class' => 'm-wrap span6', 'rows' => 6, 'cols' => 50)
                ); ?>
                <?php echo $form->error($model, 'texto_dica', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDTipo_dica', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo Html::activeDropDownList($model, 'IDTipo_dica', $tipoDicas, array('class' => 'select2')) ?>
                <?php echo $form->error($model, 'IDTipo_dica', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDAcao', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo Html::activeDropDownList($model, 'IDAcao', $acoes, array('class' => 'select2')) ?>
                <?php echo $form->error($model, 'IDAcao', array('class' => 'help-inline')); ?>
            </div>
        </div>
    </div>

    <div class="form-actions ">
        <?php echo Html::submitButton(
            $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações',
            array('class' => 'botao')
        ); ?>
    </div>
<?php $this->endWidget(); ?>