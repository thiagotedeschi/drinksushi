<?php
/* @var $this ClasseFaqController */
/* @var $data ClasseFaq */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDClasseFaq')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDClasseFaq), array('view', 'id' => $data->IDClasseFaq)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_classeFaq')); ?>:</b>
    <?php echo Html::encode($data->nome_classeFaq); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('icone_classeFaq')); ?>:</b>
    <?php echo Html::encode($data->icone_classeFaq); ?>
    <br/>


</div>