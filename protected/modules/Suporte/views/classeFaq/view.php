<?php
/* @var $this ClasseFaqController */
/* @var $model ClasseFaq */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDClasseFaq',
            'nome_classeFaq',
            'icone_classeFaq' => array(
                'name' => 'Icone Classe FAQ',
                'value' => '<i class=\'icon-' . $model->icone_classeFaq . '\'></i>',
                'type' => 'raw'
            ),
        ),
    )
); ?>
