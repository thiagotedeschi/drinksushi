<?php
/* @var $this ClasseFaqController */
/* @var $model ClasseFaq */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'classe-faq-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_classeFaq', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'nome_classeFaq',
                array('class' => 'm-wrap span6', 'size' => 60, 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'nome_classeFaq', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php $this->widget(
            'application.widgets.WIcones',
            array('model' => $model, 'form' => $form, 'field' => 'icone_classeFaq')
        ); ?>
    </div>
</div>

<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
