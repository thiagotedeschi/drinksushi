<?php

/* @var $this AjudaController */
/* @var $model Ajuda */
/* @var $acoes array */

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

$this->renderPartial('_form', array('model' => $model, 'acoes' => $acoes));
?>