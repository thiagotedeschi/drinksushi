<?php

/* @var $this AjudaController */
/* @var $model Ajuda */

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDAjuda',
            'texto_ajuda',
            'video_ajuda' => array(
                'name' => 'Vídeo',
                'type' => 'raw',
                'value' => $model->geraIframe()
            ),
            'IDAcao' => array(
                'name' => 'Ação',
                'type' => 'raw',
                'value' => $model->iDAcao->nome_acao
            ),
        ),
    )
);
?>
