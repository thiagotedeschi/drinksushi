<?php

/* @var $this AjudaController */
/* @var $model Ajuda */

//Carrega a Widget que monta as dicas na pagina
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>) No início de cada
    pesquisa.
</p>

<?php

$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'ajuda-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDAjuda',
            'texto_ajuda',
            array(
                'name' => 'nome_acao',
                'value' => '$data->iDAcao',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
