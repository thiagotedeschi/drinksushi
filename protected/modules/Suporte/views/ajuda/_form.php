<?php
/* @var $this AjudaController */
/* @var $model Ajuda */
/* @var $form CActiveForm */
/* @var $acoes array */

$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'ajuda-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>

    <?php
    echo $form->errorSummary($model);
    echo $this->retornaAjuda(1);
    echo $this->retornaAjuda(3);
    ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'texto_ajuda', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'texto_ajuda',
                array('class' => 'm-wrap span6', 'rows' => 6, 'cols' => 50)
            ); ?>
            <?php echo $form->error($model, 'texto_ajuda', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'video_ajuda', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'video_ajuda',
                array('class' => 'm-wrap span6', 'size' => 60, 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'video_ajuda', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDAcao', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeDropDownList($model, 'IDAcao', $acoes) ?>
            <?php echo $form->error($model, 'IDAcao', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
