<?php
/* @var $this AjudaController */
/* @var $data Ajuda */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDAjuda')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDAjuda), array('view', 'id' => $data->IDAjuda)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('texto_ajuda')); ?>:</b>
    <?php echo Html::encode($data->texto_ajuda); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('video_ajuda')); ?>:</b>
    <?php echo Html::encode($data->video_ajuda); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDAcao')); ?>:</b>
    <?php echo Html::encode($data->IDAcao); ?>
    <br/>


</div>