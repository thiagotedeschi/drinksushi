<?php
/* @var $this FaqController */
/* @var $model Faq */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDFaq',
            'pergunta_faq',
            'resposta_faq',
            'IDClasseFaq' => array(
                'name' => 'Classe FAQ',
                'value' => $model->iDClasseFaq->nome_classeFaq
            ),
        ),
    )
); ?>
