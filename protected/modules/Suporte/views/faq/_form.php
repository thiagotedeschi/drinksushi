<?php
/* @var $this FaqController */
/* @var $model Faq */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'faq-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>
    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'pergunta_faq', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'pergunta_faq',
                array('size' => 60, 'maxlength' => 255, 'class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($model, 'pergunta_faq', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'resposta_faq', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea(
                $model,
                'resposta_faq',
                array('rows' => 6, 'cols' => 50, 'class' => 'm-wrap span6')
            ); ?>
            <?php echo $form->error($model, 'resposta_faq', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDClasseFaq', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo Html::activeDropDownList(
                $model,
                'IDClasseFaq',
                Html::listData(
                    ClasseFaq::model()->findAll(array('order' => '"nome_classeFaq"')),
                    'IDClasseFaq',
                    'nome_classeFaq'
                ),
                array('class' => 'm-wrap select2')
            ) ?>
            <?php echo $form->error($model, 'IDClasseFaq', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
