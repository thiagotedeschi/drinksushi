<?php
/* @var $this FaqController */
/* @var $data Faq */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDFaq')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDFaq), array('view', 'id' => $data->IDFaq)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('pergunta_faq')); ?>:</b>
    <?php echo Html::encode($data->pergunta_faq); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('resposta_faq')); ?>:</b>
    <?php echo Html::encode($data->resposta_faq); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDClasseFaq')); ?>:</b>
    <?php echo Html::encode($data->IDClasseFaq); ?>
    <br/>


</div>