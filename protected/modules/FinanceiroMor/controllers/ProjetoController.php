<?php

/**
 * @package base.FinanceiroMor
 */
class ProjetoController extends Controller
{

    public $acoesActions = array(
        'create' => '178',
        'search' => '179',
        'view' => '180',
        'update' => '181',
        'delete' => '182',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . Projeto::model()->labelModel();
        $model = new Projeto;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Projeto'])) {
            $parcelas = isset($_POST['Parcela']) ? $_POST['Parcela'] : array();

            $modulos = array();
            $model->ativo_projeto = false;
            if (isset($_POST['Modulos'])) {
                $modulos = $_POST['Modulos'];
                $model->ativo_projeto = true;
            }

            $model->attributes = $_POST['Projeto'];
            $model->preparaImg();
            $model->ativo_projeto = true;
            if (empty($_POST['Projeto']['dt_fimProjeto'])) {
                $model->dt_fimProjeto = null;
            }

            if ($model->save()) {
                //função para fazer a associação dos servicos com o Projeto
                $model->atualizaParcelas($parcelas);
                //função para fazer a associação dos modulos com o Projeto
                $model->atualizaModulos($modulos);


                $this->redirect(array('search', 'id' => $model->IDProjeto));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Projeto::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['Projeto'])) {
            $parcelas = isset($_POST['Parcela']) ? $_POST['Parcela'] : array();

            $modulos = array();
            $model->ativo_projeto = false;
            if (isset($_POST['Modulos'])) {
                $modulos = $_POST['Modulos'];
                $model->ativo_projeto = true;
            }

            $model->attributes = $_POST['Projeto'];
            if (empty($_POST['Projeto']['dt_fimProjeto'])) {
                $model->dt_fimProjeto = null;
            }

            $model->preparaImg();


            if ($model->save()) {
                //função para fazer a associação dos servicos com a prestacao
                $model->atualizaParcelas($parcelas);
                //função para fazer a associação dos modulos com o Projeto
                $model->atualizaModulos($modulos);

//                if (!empty($_FILES['img_projeto'])) {
//                    $model->uploadFoto($_FILES['img_projeto']);
//                }


                $this->redirect(array('search', 'id' => $model->IDProjeto));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxRequest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Projeto::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
            unset($_GET['pageSize']); // unseta para evitar atribuições futuras desnecessárias;
        }
        $model = new Projeto('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Projeto'])) {
            $model->attributes = $_GET['Projeto'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Projeto the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Projeto::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Projeto $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'projeto-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionDisable($id)
    {
        $model = $this->loadModel($id);
        $model->ativo_projeto = !$model->ativo_projeto;
        $model->update(['ativo_projeto']);

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('search'));
        }
    }

    public function actionImgProjeto($slug)
    {
        $model = Projeto::model()->find(
            array(
                'select' => "img_projeto",
                'condition' => '"slug_projeto" = :slug',
                'params' => array(':slug' => $slug)
            )
        );
        if ($model == null) {
            return;
        }
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Transfer-Encoding: binary');
        header('Content-Type: ' . 'image/png');
        if ($model->img_projeto != '') {
            $stringImagem = stream_get_contents($model->img_projeto);
            echo base64_decode($stringImagem);
        } else {
            echo file_get_contents(ASSETS_LINK . '/images/main/usuarios/default_male.png');
        }
    }

}
