<?php

/**
 * Controller para ações no Cliente
 * @package base.FinanceiroMor
 */
class ClienteController extends Controller
{

    public $acoesActions = array(
        'create' => '67',
        'search' => '69',
        'view' => '68',
        'update' => '70',
        'delete' => '71',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;
        $this->render(
            'view',
            array(
                'model' => $model,
            )
        );
    }

    public function actionCreate()
    {

        $this->pageTitle = 'Criar ' . Cliente::model()->labelModel();
        $cliente = new Cliente;
        $enderecoC = new EnderecoCliente;
        $enderecoF = new EnderecoCliente;

        // Valida os 3 models
        $this->performAjaxValidation($cliente, $enderecoC, $enderecoF);

        if (isset($_POST['Cliente'], $_POST['EnderecoCliente'][1])) {

            $cliente->attributes = $_POST['Cliente']; //preenche o cliente
            $enderecoC->attributes = $_POST['EnderecoCliente'][1]; //preenche o endereco principal (obrigatório)
            if ($enderecoC->save()) //se inserir o endereco certinho
            {
                $cliente->IDEndereco_correspondencia = $enderecoC->IDEndereco;
            }
            if (isset($_POST['EnderecoCliente'][2])) //se o usuário tiver optado por um endereço fiscal diferente
            {
                $enderecoF = new EnderecoCliente();
                $enderecoF->attributes = $_POST['EnderecoCliente'][2]; //preenche o novo endereco
                if ($valid = $enderecoF->save()) //salva o novo enderecso
                {
                    $cliente->IDEndereco_fiscal = $enderecoF->IDEndereco;
                }
            } else //se ele não tiver preenchido o endereco fiscal
            {
                //guarda na posição referente ao End. Fiscal
                //o já cadastrado endereco de correspondencia
                $cliente->IDEndereco_fiscal = $enderecoC->IDEndereco;
            }
            if ($cliente->save()) //se der tudo certo
            {

                $this->redirect(array('view', 'id' => $cliente->IDCliente));
            }
        }
        $this->render(
            'create',
            array(
                'cliente' => $cliente,
                'enderecoC' => $enderecoC,
                'enderecoF' => $enderecoF,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Da mesma forma que a actionCreate, neste controller temos que tratar
     * vários possíveis modulos e, caso algum deles não seja mais necessário, excluí-lo
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . Cliente::model()->labelModel() . ' #' . $id;
        $cliente = $this->loadModel($id); //carrega o cliente
        $enderecoC = $cliente->iDEnderecoCorrespondencia; //carrega o end. Corresp
        $enderecoF = $cliente->iDEnderecoFiscal; //carrega o End. Fiscal
        $this->performAjaxValidation($cliente, $enderecoC, $enderecoF); // Valida os 3 models
        $valid = false;
        if (isset($_POST['Cliente'], $_POST['EnderecoCliente'][1])) {

            $cliente->attributes = $_POST['Cliente'];
            $enderecoC->attributes = $_POST['EnderecoCliente'][1];
            if (isset($_POST['EnderecoCliente'][2])) //se estiver setado o endereco 2, referente ao fiscal
            {
                $enderecoF = new EnderecoCliente();
                $enderecoF->attributes = $_POST['EnderecoCliente'][2]; //atribui os dados ao model enderecoF
                if ($valid = $enderecoF->save()) //se salvar direitinho
                    /*
                     * Duas possibilidades: ou o End Fiscal já existia e o ID é sobreescrevido por ele mesmo
                     * ou caso ele fosse o mesmo que o end de Corresp, agora não é mais
                     */ {
                    $cliente->IDEndereco_fiscal = $enderecoF->IDEndereco;
                }
            } else //se não tiver End 2 setado
            {
                $valid = true;
                $cliente->IDEndereco_fiscal = $enderecoC->IDEndereco; //End F fica sendo o mesmo que o End C
                if ($enderecoF->IDEndereco != '' && $enderecoF->IDEndereco != $enderecoC->IDEndereco) //Se existia end F que era diferente do end C
                {
                    $enderecoF->delete();
                } //Exclui ele
            }

            $valid = $cliente->validate() && $valid;
            $valid = $enderecoC->validate() && $valid;
            if ($valid) {

                if ($enderecoC->save() && $cliente->save()) //se tudo validar certinho, vamos ver as alterações na view
                {

                    $this->redirect(array('view', 'id' => $cliente->IDCliente));
                }
            }
        }
        $this->render(
            'update',
            array(
                'cliente' => $cliente,
                'enderecoC' => $enderecoC,
                'enderecoF' => $enderecoF,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxResquest) {
            foreach ($_POST['id'] as $id) {
                $cli = $this->loadModel($id);
                if (($endC = $cli->iDEnderecoCorrespondencia) != '') {
                    $endC->delete();
                }
                if (($endF = $cli->iDEnderecoFiscal) != '') {
                    $endF->delete();
                }
                $cli->delete();
            }
        } else {
            $cli = $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . Cliente::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
        }
        $model = new Cliente('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Cliente'])) {
            $model->attributes = $_GET['Cliente'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Cliente the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Cliente::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Cliente $model the model to be validated
     */
    protected function performAjaxValidation($cliente, $enderecoC, $enderecoF)
    {

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cliente-form') {
            if (isset($_POST['EnderecoCliente'][2])) {
                echo CActiveForm::validate(array($enderecoC, $cliente, $enderecoF));
                Yii::app()->end();
            } else {
                echo CActiveForm::validate(array($cliente, $enderecoC));
                Yii::app()->end();
            }
        }
    }

}

