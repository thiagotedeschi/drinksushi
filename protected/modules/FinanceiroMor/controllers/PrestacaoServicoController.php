<?php


class PrestacaoServicoController extends Controller
{

    public $acoesActions = array(
        'create' => '82',
        'search' => '83',
        'view' => '84',
        'update' => '85',
        'delete' => '86',
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Visualizar ' . $model->labelModel() . ' #' . $id;

        $servicos = '';
        foreach ($this->loadModel($id)->servicoPontuals as $servicoPontual) {
            if ($servicos == '') {
                $servicos .= $servicoPontual->nome_servico;
            } else {
                $servicos .= ' - ' . $servicoPontual->nome_servico;
            }
        }
        $this->render(
            'view',
            array(
                'model' => $model,
                'servicos' => $servicos
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . PrestacaoServico::model()->labelModel();
        $model = new PrestacaoServico;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['PrestacaoServico'])) {
            $parcelas = isset($_POST['Parcela']) ? $_POST['Parcela'] : array();
            $servicoPontuals = isset($_POST['PrestacaoServico']['servicoPontuals']) ? $_POST['PrestacaoServico']['servicoPontuals'] : array();
            $model->attributes = $_POST['PrestacaoServico'];
            if (empty($_POST['PrestacaoServico']['dt_fim'])) {
                $model->dt_fim = null;
            }

            if ($model->save()) {
                //função para fazer a associação dos servicos com a prestacao
                $model->atualizaServicos($servicoPontuals);
                $model->atualizaParcelas($parcelas);


                $this->redirect(array('search', 'id' => $model->IDPrestacao_servico));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . PrestacaoServico::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if (isset($_POST['PrestacaoServico'])) {
            $servicoPontuals = isset($_POST['PrestacaoServico']['servicoPontuals']) ? $_POST['PrestacaoServico']['servicoPontuals'] : array();
            $parcelas = isset($_POST['Parcela']) ? $_POST['Parcela'] : array();

            $model->attributes = $_POST['PrestacaoServico'];
            if (empty($_POST['PrestacaoServico']['dt_fim'])) {
                $model->dt_fim = null;
            }

            if ($model->save()) {
                //função para fazer a associação dos servicos com a prestacao
                $model->atualizaServicos($servicoPontuals);
                $model->atualizaParcelas($parcelas);


                $this->redirect(array('search', 'id' => $model->IDPrestacao_servico));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'nr_parcelas' => count($model->parcelaPrestacaoServicos),
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxResquest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }

    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . PrestacaoServico::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
        }
        $model = new PrestacaoServico('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['PrestacaoServico'])) {
            $model->attributes = $_GET['PrestacaoServico'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return PrestacaoServico the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = PrestacaoServico::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        $model->dt_inicio = HData::formataData($model->dt_inicio, HData::BR_DATE_FORMAT, HData::SQL_DATE_FORMAT);
        if (!is_null($model->dt_fim)) {
            $model->dt_fim = HData::formataData($model->dt_fim, HData::BR_DATE_FORMAT, HData::SQL_DATE_FORMAT);
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param PrestacaoServico $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'prestacao-servico-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    public function actionGerarParcelas()
    {
        $nr_parcelas = $_POST['nr_parcela'];
        $vl_prestacao = ($_POST['vl_prestacao'] / $nr_parcelas);
        $vl_prestacao = number_format($vl_prestacao, 2, ',', '');

        $this->renderPartial(
            '_tabela',
            array(
                'nr_parcelas' => $nr_parcelas,
                'vl_prestacao' => $vl_prestacao
            )
        );
    }


    public function actionGerarData()
    {
        $dtAnterior = DateTime::createFromFormat('d/m/Y', $_POST['dtAnterior']);
        $data = array();
        for ($i = $_POST['nrLinhas'] + 1; $i <= $_POST['nrParcelas']; $i++) {
            $dtAnterior->modify("+1 month");
            $data[$i] = $dtAnterior->format('d/m/Y');
        }
        echo CJSON::encode($data);
    }

}
