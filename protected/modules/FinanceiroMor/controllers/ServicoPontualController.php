<?php


class ServicoPontualController extends Controller
{
    public $acoesActions = array(
        'create' => 55,
        'search' => 56,
        'view' => 58,
        'update' => 60,
        'delete' => 61,
    );

    /**
     * Por padrão a view default é a search
     * @var string view default
     */
    public $defaultAction = 'search';
    public $pageTitle;

    /**
     * Mostra os dados de um Model específico
     * @param integer $id o ID do model a ser visualizado
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Visualizar ' . ServicoPontual::model()->labelModel() . '#' . $id;
        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    /**
     * Cria um novo modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     */
    public function actionCreate()
    {
        $this->pageTitle = 'Criar ' . ServicoPontual::model()->labelModel();
        $model = new ServicoPontual;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['ServicoPontual'])) {
            $model->attributes = $_POST['ServicoPontual'];
            if ($model->save()) {

                $this->redirect(array('view', 'id' => $model->IDServico_pontual));
            }
        }

        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Edita (atualiza) um modelo.
     * Se funcionar, o usuário será redirecionado para página de visualização.
     * @param integer $id o ID do model a ser editado
     */
    public function actionUpdate($id)
    {
        $this->pageTitle = 'Editar ' . ServicoPontual::model()->labelModel() . ' #' . $id;
        $model = $this->loadModel($id);


        $this->performAjaxValidation($model);

        if (isset($_POST['ServicoPontual'])) {
            $model->attributes = $_POST['ServicoPontual'];
            if ($model->save()) {

                $this->redirect(array('view', 'id' => $model->IDServico_pontual));
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Remover um model.
     * If Se a deleção funcionar, o usuário será redirecionado para a página de busca
     * @param integer $id o ID do model a ser removido
     */
    public function actionDelete($id)
    {
        if ($id == null && isset($_POST['id']) && Yii::app()->request->isAjaxResquest) {
            foreach ($_POST['id'] as $id) {
                $this->loadModel($id)->delete();
            }
        } else {
            $this->loadModel($id)->delete();
        }
    }


    /**
     * Gerencia e busca todos os modelos.
     */
    public function actionSearch()
    {
        $this->pageTitle = 'Buscar ' . ServicoPontual::model()->labelModel();
        //seta o número de registros por página selecionado anteriormente
        if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize', (int)$_GET['pageSize']);
        }
        $model = new ServicoPontual('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['ServicoPontual'])) {
            $model->attributes = $_GET['ServicoPontual'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            $this->renderPartial('admin', array('model' => $model));
        } else {
            $this->render('admin', array('model' => $model));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ServicoPontual the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ServicoPontual::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ServicoPontual $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'servico-pontual-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCriarTipoServicoDinamico()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $tipoServico = new TipoServico;
            $tipoServico->IDTipo_servico = 2;
            $tipoServico->nome_tipoServico = $_POST['nome_tipoServico'];

            $tipoServico->save();

            $data = TipoServico::model()->findAll();
            $data = Html::listData($data, 'IDTipo_servico', 'nome_tipoServico');

            foreach ($data as $value => $name) {
                echo Html::tag('option', array('value' => $value), Html::encode($name), true);
            }
        }
    }
}
