<?php
/* @var $this ProjetoController */
/* @var $model Projeto */
?>
<table class="table table-striped table-hover table-bordered">
    <thead>
    <tr>
        <th id="parcela-projeto-grid_c0">
            Data Referência Parcela
        </th>
        <th id="parcela-projeto-grid_c1">
            Valor Parcela
        </th>
        <th id="parcela-projeto-grid_c2">
            Desconto Parcela
        </th>
    </tr>
    </thead>
    <tbody>

    <?php
    foreach ($model->parcelaProjetos as $k => $parcPrestacao) {
        echo "
                <tr>
                    <td>" . HData::formataData(
                $parcPrestacao->dt_referenciaParcela,
                HData::BR_DATE_FORMAT,
                HData::SQL_DATE_FORMAT
            ) . "</td>
                    <td>" . number_format($parcPrestacao->vl_parcela, 2, ',', '.') . "</td>
                    <td>" . (is_null($parcPrestacao->desconto_parcela) ? '-' : $parcPrestacao->desconto_parcela . '%') . "</td>
                </tr>
            ";
    }
    ?>
    </tbody>
</table>