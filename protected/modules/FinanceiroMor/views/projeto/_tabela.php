<?php
/* @var $this ProjetoController */
/* @var $model Projeto */
/* @var $nr_parcelas int */
/* @var $vl_prestacao float */
?>

<table class="table table-striped table-hover table-bordered" id="myTable">
    <thead>
    <tr>
        <th>Data de Vencimento</th>
        <th>Valor da Parcela</th>
        <th>Desconto da Parcela</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if (isset($model) && !$model->isNewRecord) {
        $i = 1;
        $parcelaPrestacaoServicos = $model->parcelaProjetos;
        foreach ($parcelaPrestacaoServicos as $k => $parcPrestacao) {
            if (strtotime($parcPrestacao->dt_referenciaParcela) < strtotime(date('Y-m-t'))) {
                $disabled = 'disabled';
            } else {
                $disabled = '';
            }

            ?>
            <tr>
                <td><?php echo Html::textField(
                        'Parcela[update][' . $parcPrestacao->IDParcela . '][dtParcela]',
                        HData::formataData($parcPrestacao->dt_referenciaParcela, 'd/m/Y', 'Y-m-d'),
                        array(
                            'placeholder' => 'Data Parcela',
                            'id' => 'dtParcela' . $i,
                            'disabled' => "$disabled"
                        )
                    )?>
                </td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on">R$</span>
                        <?php echo Html::textField(
                            'Parcela[update][' . $parcPrestacao->IDParcela . '][vlParcela]',
                            number_format($parcPrestacao->vl_parcela, 2, ',', ''),
                            array(
                                'placeholder' => 'Valor Parcela',
                                'onChange' => 'atualizaValor(' . $i . ')',
                                'onBlur' => 'blurVlPrestacao(this)',
                                'id' => 'vlParcela' . $i,
                                'disabled' => "$disabled"
                            )
                        )
                        ?>
                    </div>
                </td>
                <td>
                    <div class="input-append">
                        <?php echo Html::textField(
                            'Parcela[update][' . $parcPrestacao->IDParcela . '][descParcela]',
                            $parcPrestacao->desconto_parcela,
                            array(
                                'class' => 'appendedPrependedInput',
                                'id' => 'descParcela' . $i,
                                'disabled' => "$disabled"
                            )
                        ) ?>
                        <span class="add-on">%</span>
                    </div>
                </td>
            </tr>
            <?php
            $i++;
        }
    } else {
        for ($i = 1; $i <= $nr_parcelas; $i++) {
            $dt_venc = strftime("%d/%m/%Y", strtotime("+" . $i . " month"));
            ?>
            <tr>
                <td><?php echo Html::textField(
                        'Parcela[nova][' . $i . '][dtParcela]',
                        $dt_venc,
                        array('placeholder' => 'Data Parcela', 'id' => 'dtParcela' . $i)
                    ) ?></td>
                <td>
                    <div class="input-prepend">
                        <span class="add-on">R$</span>
                        <?php echo Html::textField(
                            'Parcela[nova][' . $i . '][vlParcela]',
                            $vl_prestacao,
                            array(
                                'placeholder' => 'Valor Parcela',
                                'onChange' => 'atualizaValor(' . $i . ')',
                                'onBlur' => 'blurVlPrestacao(this)',
                                'id' => 'vlParcela' . $i
                            )
                        ) ?>
                    </div>
                </td>
                <td>
                    <div class="input-append">
                        <?php echo Html::textField(
                            'Parcela[nova][' . $i . '][descParcela]',
                            '',
                            array('class' => 'appendedPrependedInput', 'id' => 'descParcela' . $i)
                        ) ?>
                        <span class="add-on">%</span>
                    </div>
                </td>
            </tr>
        <?php
        }
    }
    ?>
    </tbody>
</table>