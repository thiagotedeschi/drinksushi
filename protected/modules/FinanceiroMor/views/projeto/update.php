<?php
/* @var $this ProjetoController */
/* @var $model Projeto */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));

$model->dt_inicioProjeto = HData::formataData($model->dt_inicioProjeto, HData::BR_DATE_FORMAT, HData::SQL_DATE_FORMAT);
$model->dt_fimProjeto = HData::formataData($model->dt_fimProjeto, HData::BR_DATE_FORMAT, HData::SQL_DATE_FORMAT);

?>


<?php $this->renderPartial('_form', array('model' => $model, 'nr_parcelas' => count($model->parcelaProjetos))); ?>