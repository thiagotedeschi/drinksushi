<?php
/* @var $this ProjetoController */
/* @var $data Projeto */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDProjeto')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDProjeto), array('view', 'id' => $data->IDProjeto)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_inicioProjeto')); ?>:</b>
    <?php echo Html::encode($data->dt_inicioProjeto); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('dt_fimProjeto')); ?>:</b>
    <?php echo Html::encode($data->dt_fimProjeto); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('vl_projeto')); ?>:</b>
    <?php echo Html::encode($data->vl_projeto); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDCliente')); ?>:</b>
    <?php echo Html::encode($data->IDCliente); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('vl_mensalProjeto')); ?>:</b>
    <?php echo Html::encode($data->vl_mensalProjeto); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('usuario_projeto')); ?>:</b>
    <?php echo Html::encode($data->usuario_projeto); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('senha_projeto')); ?>:</b>
	<?php echo Html::encode($data->senha_projeto); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('ativo_projeto')); ?>:</b>
	<?php echo Html::encode($data->ativo_projeto); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('max_vidasAtivasProjeto')); ?>:</b>
	<?php echo Html::encode($data->max_vidasAtivasProjeto); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('max_empresasAtivasProjeto')); ?>:</b>
	<?php echo Html::encode($data->max_empresasAtivasProjeto); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('slug_projeto')); ?>:</b>
	<?php echo Html::encode($data->slug_projeto); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('max_usuariosAtivosProjeto')); ?>:</b>
	<?php echo Html::encode($data->max_usuariosAtivosProjeto); ?>
	<br />

	*/
    ?>

</div>