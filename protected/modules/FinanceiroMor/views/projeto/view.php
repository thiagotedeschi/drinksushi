<?php
/* @var $this ProjetoController */
/* @var $model Projeto */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDProjeto',
            'slug_projeto',
            'dt_inicioProjeto:date',
            'dt_fimProjeto:date',
            'vl_projeto',
            'IDCliente' => array(
                'name' => 'Cliente',
                'value' => $model->iDCliente->nome_cliente
            ),
            'vl_mensalProjeto',
            'usuario_projeto',
            ($this->verificaAcao(183) ? 'senha_projeto' : ''),
            'ativo_projeto' => array(
                'name' => 'Projeto Ativo?',
                'value' => ($model->ativo_projeto ? 'Sim' : 'Não')
            ),
            'max_vidasAtivasProjeto',
            'max_empresasAtivasProjeto',
            'max_usuariosAtivosProjeto',
            'modulosProjeto' => array(
                'name' => 'Modulos',
                'type' => 'raw',
                'value' => $model->viewModulos()
            ),
            'imgProjeto' => array(
                'name' => 'Logo',
                'type' => 'raw',
                'value' => '<img src="'.$this->createUrl('imgProjeto', array('slug' => $model->slug_projeto)).'" />'
            ),
            'parcelaProjetos' => array(
                'name' => 'Parcelas',
                'type' => 'raw',
                'value' => $this->renderPartial(
                        '_viewParcelas',
                        array(
                            'model' => $model,
                        ),
                        true
                    )
            ),
        ),
    )
); ?>
