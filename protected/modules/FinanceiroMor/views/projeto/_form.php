<?php
/* @var $this ProjetoController */
/* @var $model Projeto */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'projeto-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0',
            'enctype' => 'multipart/form-data'
        ),
    )
);
if ($model->isNewRecord) {
    $disabled = '';
} else {
    $disabled = 'disabled';
}
?>
<script type="text/javascript" src="<?= ASSETS_LINK; ?>/js/accounting.js"></script>
<div class="form clearfix positionRelative">
<p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>

<?php echo $form->errorSummary($model); ?>

<div class="control-group">
    <?php echo $form->labelEx($model, 'slug_projeto', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $model,
            'slug_projeto',
            array('class' => 'span6 m-wrap' . $disabled, 'maxlength' => 255, 'disabled' => $disabled)
        ); ?>
        <?php echo $form->error($model, 'slug_projeto', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'dt_inicioProjeto', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'dt_inicioProjeto', array('class' => 'span6 m-wrap date-picker')) ?>
        <?php echo $form->error($model, 'dt_inicioProjeto', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'dt_fimProjeto', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'dt_fimProjeto', array('class' => 'span6 m-wrap date-picker')) ?>
        <?php echo $form->error($model, 'dt_fimProjeto', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'IDCliente', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->dropDownList(
            $model,
            'IDCliente',
            Html::listData(Cliente::model()->findAll(array('order' => 'nome_cliente')), 'IDCliente', 'nome_cliente'),
            array('class' => 'select2', 'prompt' => 'Escolha um Cliente')
        ) ?>
        <?php echo $form->error($model, 'IDCliente', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="span11">
    <div id="modulos-portlet" class="portlet box light-grey">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-reorder"></i>Módulos do projeto
            </div>
        </div>
        <div class="portlet-body" style="min-height: 250px;">
            <div class="row-fluid">
                <div class="alert lead" style="text-align: center">
                    <span>Arraste e solte um Módulo do menu ou Super Módulo para adicionar a esse Projeto.</span><br/>
                </div>
                <?php
                if (!$model->isNewRecord) {
                    echo '<div class="row-fluid">';
                foreach ($model->modulos as $mod) {
                    $modProjeto = ProjetosTemModulos::model()->findByAttributes(
                        array('IDModulo' => $mod->IDModulo, 'IDProjeto' => $model->IDProjeto)
                    );
                    ?>
                    <div class="icon-btn span2 clearfix height-170">
                        <span onclick="removeModulos(this)" style="" class="badge badge-inverse">X</span>
                        <a href="#">
                            <i class="icon-<?= $mod->icone_modulo . ' MOD' . $mod->IDModulo ?>"></i>

                            <div><?= $mod->nome_modulo ?></div>
                        </a>
                        <span style="padding: 9px 0 0 10px;float: left;">De:</span><input class="span9 m-wrap"
                                                                                          placeholder="dd/mm/aaaa"
                                                                                          name="Modulos[<?= $mod->IDModulo ?>][dtInicio]"
                                                                                          type="text"
                                                                                          value="<?=
                                                                                          HData::formataData(
                                                                                              $modProjeto->dt_inicio,
                                                                                              HData::BR_DATE_FORMAT,
                                                                                              HData::SQL_DATE_FORMAT
                                                                                          ) ?>">
                        <span style="padding: 9px 0 0 10px;float: left;">à:</span>&nbsp;&nbsp;<input
                            class="span9 m-wrap" placeholder="dd/mm/aaaa" name="Modulos[<?= $mod->IDModulo ?>][dtFim]"
                            type="text"
                            value="<?=
                            HData::formataData(
                                $modProjeto->dt_fim,
                                HData::BR_DATE_FORMAT,
                                HData::SQL_DATE_FORMAT
                            ) ?>">
                    </div>
                    <script>
                        $(document).ready(function () {
                            var ui = $("#menu i.icon-<?= $mod->icone_modulo?>.MOD<?=$mod->IDModulo?>").parent();
                            $favoritos = $("#modulos-portlet");
                            ui.draggable({
                                revert: true,
                                revertDuration: 0,
                                start: function () {
                                    $favoritos.addClass('grid-view-loading');
                                    $favoritos.css('border-style', 'dashed');
                                    $(this).css('z-index', '1000');
                                },
                                stop: function () {
                                    $favoritos.removeClass('grid-view-loading');
                                    $favoritos.css('border-style', 'solid');
                                }
                            });
                            ui.addClass('undraggable');
                            ui.draggable('disable');
                            ui.css('font-weight', '600');
                        });
                    </script>
                <?php
                }
                echo '</div>';
                } else {
                $modulos = Modulo::model()->findAll('"eh_basicoModulo" = :bool', array(':bool' => true));

                foreach ($modulos as $mod) {
                ?>
                    <div class="icon-btn span2 clearfix height-170">
                        <span onclick="removeModulos(this)" style="" class="badge badge-inverse">X</span>
                        <a href="#">
                            <i class="icon-<?= $mod->icone_modulo . ' MOD' . $mod->IDModulo ?>"></i>

                            <div><?= $mod->nome_modulo ?></div>
                        </a>
                        <span style="padding: 9px 0 0 10px;float: left;">De:</span><input class="span9 m-wrap"
                                                                                          placeholder="dd/mm/aaaa"
                                                                                          name="Modulos[<?= $mod->IDModulo ?>][dtInicio]"
                                                                                          type="text"
                                                                                          onBlur="Data(this);"
                                                                                          maxlength="8" value=""><br>
                        <span style="padding: 9px 0 0 10px;float: left;">à:</span>&nbsp;&nbsp;<input
                            class="span9 m-wrap" placeholder="dd/mm/aaaa" name="Modulos[<?= $mod->IDModulo ?>][dtFim]"
                            type="text" onBlur="Data(this);" maxlength="8" value="">
                    </div>
                    <script>
                        $(document).ready(function () {
                            var ui = $("#menu i.icon-<?= $mod->icone_modulo?>.MOD<?=$mod->IDModulo?>").parent();
                            $favoritos = $("#modulos-portlet");
                            ui.draggable({
                                revert: true,
                                revertDuration: 0,
                                start: function () {
                                    $favoritos.addClass('grid-view-loading');
                                    $favoritos.css('border-style', 'dashed');
                                    $(this).css('z-index', '1000');
                                },
                                stop: function () {
                                    $favoritos.removeClass('grid-view-loading');
                                    $favoritos.css('border-style', 'solid');
                                }
                            });
                            ui.addClass('undraggable');
                            ui.draggable('disable');
                            ui.css('font-weight', '600');
                        });
                    </script>
                <?php
                }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'vl_projeto', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'vl_projeto', array('class' => 'span6 m-wrap', 'disabled' => $disabled)); ?>
        <?php echo $form->error($model, 'vl_projeto', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group <?php echo $model->isNewRecord ? 'hide' : '' ?>" id="divParcelas">
    <fieldset>
        <legend>Parcelas</legend>
        <?php echo Html::label('Número de parcelas', 'nr_parcelas', array('class' => 'control-label')) ?>
        <div class="controls">
            <?php echo Html::numberField('nr_parcelas', $nr_parcelas, array('class' => 'span6 m-wrap')) ?>
        </div>
        <div id="parcelasGeradas">
            <?php $model->isNewRecord ? '' : $this->renderPartial(
                '_tabela',
                array('model' => $model, 'nr_parcelas' => $nr_parcelas)
            ); ?>
        </div>
    </fieldset>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'vl_mensalProjeto', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'vl_mensalProjeto', array('class' => 'span6 m-wrap')); ?>
        <?php echo $form->error($model, 'vl_mensalProjeto', array('class' => 'help-inline')); ?>
    </div>
</div>

<?php if ($model->isNewRecord || $this->verificaAcao(183)) { ?>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'usuario_projeto', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'usuario_projeto',
                array('class' => 'span6 m-wrap', 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'usuario_projeto', array('class' => 'help-inline')); ?>
        </div>
    </div>
<?php } ?>

<div class="control-group">
    <?php echo $form->labelEx($model, 'senha_projeto', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->passwordField(
            $model,
            'senha_projeto',
            array('class' => 'span6 m-wrap', 'maxlength' => 255)
        ); ?>
        <?php echo $form->error($model, 'senha_projeto', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'max_vidasAtivasProjeto', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'max_vidasAtivasProjeto', array('class' => 'span6 m-wrap')); ?>
        <div class="sliderVidAtivas span6"
             style="min-height: 13px !important;background: #f8f8f8; border: 1px solid #ccc !important; margin: 0 20px 0 0 !important;"></div>
        <?php echo $form->error($model, 'max_vidasAtivasProjeto', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'max_empresasAtivasProjeto', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'max_empresasAtivasProjeto', array('class' => 'span6 m-wrap')); ?>
        <div class="sliderEmpAtivas span6"
             style="min-height: 13px !important;background: #f8f8f8; border: 1px solid #ccc !important; margin: 0 20px 0 0 !important;"></div>
        <?php echo $form->error($model, 'max_empresasAtivasProjeto', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'max_usuariosAtivosProjeto', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField($model, 'max_usuariosAtivosProjeto', array('class' => 'span6 m-wrap')); ?>
        <div class="sliderUsuAtivos span6"
             style="min-height: 13px !important;background: #f8f8f8; border: 1px solid #ccc !important; margin: 0 20px 0 0 !important;"></div>
        <?php echo $form->error($model, 'max_usuariosAtivosProjeto', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($model, 'img_projeto', array('class' => 'control-label')); ?>
    <div class="controls">
        <?=
        !$model->isNewRecord ? Html::image($this->createUrl('imgProjeto', array('slug' => $model->slug_projeto))) : ''?>
        <?= Html::fileField('Projeto[img_projeto]') ?>
        <?php echo $this->retornaAjuda(3) ?>
    </div>
</div>
</div>

<div class="form-actions">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    );
    ?>
</div>
<?php $this->endWidget(); ?>

<script>
$(function () {
    $(".sliderVidAtivas").slider({
        max: 10000,
        min: 0,
        value: <?= $model->isNewRecord ? '0' : $model->max_vidasAtivasProjeto?>,
        step: 10,
        animate: "slow",
        slide: function () {
            $("#<?= Html::activeId($model, 'max_vidasAtivasProjeto') ?>").val($(".sliderVidAtivas").slider("value"));
        }
    });
    $("#<?= Html::activeId($model, 'max_vidasAtivasProjeto') ?>").val($(".sliderVidAtivas").slider("value"));
});
$(function () {
    $(".sliderEmpAtivas").slider({
        max: 10000,
        min: 0,
        value: <?= $model->isNewRecord ? '0' : $model->max_empresasAtivasProjeto?>,
        step: 10,
        animate: "slow",
        slide: function () {
            $("#<?= Html::activeId($model, 'max_empresasAtivasProjeto') ?>").val($(".sliderEmpAtivas").slider("value"));
        }
    });
    $("#<?= Html::activeId($model, 'max_empresasAtivasProjeto') ?>").val($(".sliderEmpAtivas").slider("value"));
});
$(function () {
    $(".sliderUsuAtivos").slider({
        max: 10000,
        min: 0,
        value: <?= $model->isNewRecord ? '0' : $model->max_usuariosAtivosProjeto?>,
        step: 10,
        animate: "slow",
        slide: function () {
            $("#<?= Html::activeId($model, 'max_usuariosAtivosProjeto') ?>").val($(".sliderUsuAtivos").slider("value"));
        }
    });
    $("#<?= Html::activeId($model, 'max_usuariosAtivosProjeto') ?>").val($(".sliderUsuAtivos").slider("value"));
});

/**
 * Colocar o Link no portlet de favoritos (draggable)
 */
$(document).ready(function () {
    $favoritos = $("#modulos-portlet");
    $("a", "#menu").not('.undraggable').draggable({
        revert: true,
        revertDuration: 0,
        start: function () {
            $favoritos.addClass('grid-view-loading');
            $favoritos.css('border-style', 'dashed');
            $(this).css('z-index', '1000');
        },
        stop: function () {
            $favoritos.removeClass('grid-view-loading');
            $favoritos.css('border-style', 'solid');
        }
    });
    /* ******** fim colocar link no favoritos ******** */

    /** Receber  o link nos favoritos (droppable) */
    $('.portlet-body', $favoritos).droppable({
        drop: function (event, ui) {
            var classMenu = ui.draggable.children("i").attr("class");

            if (classMenu.indexOf('SMOD') > 0) {
                var nrModulos = ui.draggable.parent().children('ul').children('li').length;
                for (var i = 0; i < nrModulos; i++) {
                    classMenu = ui.draggable.parent().children('ul').children('li')[i];

                    if (!$(classMenu).children('a').hasClass('undraggable')) {
                        var text = $(classMenu).children('a').text();
                        var classe = $(classMenu).children('a').children('i').attr("class");
                        $close = $('<span onclick="removeModulos(this)" style="" class="badge badge-inverse">X</span>');
                        $label = $('<div>' + text + '</div>');
                        $icon = $('<i class="' + classe + '"></i>');
                        $container = $('<div class="icon-btn span2 clearfix height-170"></div>');
                        $dt_inicio = $('<span style="padding: 9px 0 0 10px;float: left;">De:</span><input class="span9 m-wrap" placeholder="dd/mm/aaaa" name="Modulos[' + classe.replace(/[^\d]+/g, '') + '][dtInicio]" type="text" onBlur="Data(this)" size="10"   maxlength="8" value="' + $("#<?= Html::activeId($model, 'dt_inicioProjeto') ?>").val() + '">');
                        $dt_fim = $('<span style="padding: 9px 0 0 10px;float: left;">à:</span>&nbsp;&nbsp;<input class="span9 m-wrap" placeholder="dd/mm/aaaa" name="Modulos[' + classe.replace(/[^\d]+/g, '') + '][dtFim]" type="text" onBlur="Data(this)" size="10"   maxlength="8" value="' + $("#<?= Html::activeId($model, 'dt_fimProjeto') ?>").val() + '">');
                        $a = $('<a href="#"></a>');
                        $a.append($icon);
                        $a.append($label);
                        $container.append($close);
                        $container.append($a);
                        $container.append($dt_inicio);
                        $container.append($dt_fim);
                        $('.row-fluid:last', this).append($container);
                        $(classMenu).children('a').addClass('undraggable');
                        $(classMenu).children('a').draggable('disable');
                        $(classMenu).children('a').css('font-weight', '600');
                    }
                }
            } else if (classMenu.indexOf('MOD') > 0) {
                var text = ui.draggable.text();
                var link = ui.draggable.attr("href");
                $close = $('<span onclick="removeModulos(this)" style="" class="badge badge-inverse">X</span>');
                $label = $('<div>' + text + '</div>');
                $icon = $('<i class="' + classMenu + '"></i>');
                $container = $('<div class="icon-btn span2 clearfix height-170">');
                $dt_inicio = $('<span style="padding: 9px 0 0 10px;float: left;">De:</span><input class="span9 m-wrap" placeholder="dd/mm/aaaa" name="Modulos[' + classMenu.replace(/[^\d]+/g, '') + '][dtInicio]" type="text" onBlur="Data(this);"   maxlength="8" value="' + $("#<?= Html::activeId($model, 'dt_inicioProjeto') ?>").val() + '"><br>');
                $dt_fim = $('<span style="padding: 9px 0 0 10px;float: left;">à:</span>&nbsp;&nbsp;<input class="span9 m-wrap" placeholder="dd/mm/aaaa" name="Modulos[' + classMenu.replace(/[^\d]+/g, '') + '][dtFim]" type="text" onBlur="Data(this);"  maxlength="8" value="' + $("#<?= Html::activeId($model, 'dt_fimProjeto') ?>").val() + '">');
                $a = $('<a href="' + link + '"></a>');
                $a.append($icon);
                $a.append($label);
                $container.append($close);
                $container.append($a);
                $container.append($dt_inicio);
                $container.append($dt_fim);
                $('.row-fluid:last', this).append($container)
                ui.draggable.addClass('undraggable');
                ui.draggable.draggable('disable');
                ui.draggable.css('font-weight', '600');
            }
        }
    });
})

/** Remover link dos modulos */
function removeModulos(span) {
    //remove visualmente

    var classe = $(span).parent().children('a').children('i').attr("class");

    var link = $("i[class='" + classe + "']", "#menu").parent();

    $(link).removeClass('undraggable');
    $(link).draggable('enable');
    $(link).css('font-weight', '300');

    $(span).parent().remove();
    return false;
}
/* ******** fim remover link no favoritos ******** */

/* Função para a mascará dos campos data sem DatePicker */
function Data(obj) {
    data = $(obj).val();
    data = data.replace(/\D/g, "");
    data = data.replace(/(\d{2})(\d)/, "$1/$2");
    data = data.replace(/(\d{2})(\d)/, "$1/$2");

    $(obj).val(data);

    dia = (data.substring(0, 2));
    mes = (data.substring(3, 5));
    ano = (data.substring(6, 10));

    // verifica o dia valido para cada mes
    if (dia < 01 || dia > 31) {
        $(obj).addClass('error');
        return false;
    }

    // verifica se o mes e valido
    if (mes < 01 || mes > 12) {
        $(obj).addClass('error');

        return false;
    }

    // verifica se e ano bissexto
    if (mes == 2 && (dia < 01 || dia > 29 || (dia > 28 && (parseInt(ano / 4) != ano / 4)))) {
        $(obj).addClass('error');
        return false;
    }

    $(obj).removeClass('error');
}

/* atualizar a barra/Slider */
$("#<?= Html::activeId($model, 'max_vidasAtivasProjeto') ?>").keyup(function () {
    var min = $(".sliderVidAtivas").slider("option", "min");
    var max = $(".sliderVidAtivas").slider("option", "max");
    if ($(this).val() > min || $(this).val() < max)
        $(".sliderVidAtivas").slider("option", "value", $(this).val());
    if ($(this).val() > max) {
        $(".sliderVidAtivas").slider("option", "value", max);
        $(this).val(max);
    }
    if ($(this).val() < min) {
        $(".sliderVidAtivas").slider("option", "value", min);
        $(this).val(min);
    }
});
$("#<?= Html::activeId($model, 'max_empresasAtivasProjeto') ?>").keyup(function () {
    var min = $(".sliderEmpAtivas").slider("option", "min");
    var max = $(".sliderEmpAtivas").slider("option", "max");
    if ($(this).val() > min || $(this).val() < max)
        $(".sliderEmpAtivas").slider("option", "value", $(this).val());
    if ($(this).val() > max) {
        $(".sliderEmpAtivas").slider("option", "value", max);
        $(this).val(max);
    }
    if ($(this).val() < min) {
        $(".sliderEmpAtivas").slider("option", "value", min);
        $(this).val(min);
    }
});
$("#<?= Html::activeId($model, 'max_usuariosAtivosProjeto') ?>").keyup(function () {
    var min = $(".sliderUsuAtivos").slider("option", "min");
    var max = $(".sliderUsuAtivos").slider("option", "max");
    if ($(this).val() > min || $(this).val() < max)
        $(".sliderUsuAtivos").slider("option", "value", $(this).val());
    if ($(this).val() > max) {
        $(".sliderUsuAtivos").slider("option", "value", max);
        $(this).val(max);
    }
    if ($(this).val() < min) {
        $(".sliderUsuAtivos").slider("option", "value", min);
        $(this).val(min);
    }
});

/* set o valor da imagem para a validacao ajax do framework */
$('#img_projeto').change(function () {
    $('#Projeto_img_projeto').val($(this).val());
});
</script>

<?php
Yii::app()->clientScript->registerScript(
    '',
    "$('#" . Html::activeId($model, 'vl_projeto') . "').change(function(){
        if($(this).val() != '')
        {
            $('#divParcelas').removeClass('hide');
            $(\"#nr_parcelas\").val(1);
            $.ajax({
                url:\"" . CController::createUrl('prestacaoServico/gerarParcelas') . "\",
                type:\"POST\",
                data:{ nr_parcela:1,
                       vl_prestacao:$(\"#" . Html::activeId($model, 'vl_projeto') . "\").val()
                },
                beforeSend:function(){
                     $(\"#nr_parcelas\").attr('disabled', true);
                     $(\"#divParcelas\").addClass('grid-view-loading');
                },
                success:function(html){
                    $(\"#parcelasGeradas\").html(html);
                },
                complete:function(){
                     $(\"#nr_parcelas\").attr('disabled', false);
                     $(\"#divParcelas\").removeClass('grid-view-loading');
                },
            })
        }
        else
            $('#divParcelas').addClass('hide');
    })"
    ,
    CClientScript::POS_END
)
?>

<?php
if ($model->isNewRecord) /**
 * Script para a criação e edição das parcelas quando está se criando um novo Projeto
 */ {
    Yii::app()->clientScript->registerScript(
        'geraParcela',
        "$(\"#nr_parcelas\").change(function(){
                   if(parseFloat($(this).val()) <= 0)
                       $(this).val(1);
                   $.ajax({
                       url:\"" . CController::createUrl('prestacaoServico/gerarParcelas') . "\",
                type:\"POST\",
                data:{ nr_parcela:$(\"#nr_parcelas\").val(),
                       vl_prestacao:$(\"#" . Html::activeId($model, 'vl_projeto') . "\").val()
                },
                beforeSend:function(){
                     $(\"#nr_parcelas\").attr('disabled', true);
                     $(\"#divParcelas\").addClass('grid-view-loading');
                },
                success:function(html){
                    $(\"#parcelasGeradas\").html(html);
                },
                complete:function(){
                     $(\"#nr_parcelas\").attr('disabled', false);
                     $(\"#divParcelas\").removeClass('grid-view-loading');
                },
            })
        })",
        CClientScript::POS_END
    );
} else /**
 * Script para a criação e edição das parcelas quando está se edita um Projeto
 */ {
    Yii::app()->clientScript->registerScript(
        'geraParcela',
        "$(\"#nr_parcelas\").change(function(event){
            if(parseFloat($(this).val()) <= 0) {
               $(this).val(1);
            }
            var stringData;
            var nrLinhas = parseFloat($(\"#divParcelas table tbody tr\").length);
            var nrParcelas = parseFloat($(this).val());
            var vlPago = 0;
            var j = 0;

            for (var i = 1; i <= nrLinhas; i++)
            {
               if(!$(\"#vlParcela\"+i).attr('disabled'))
               {
                   j = i-1;
                   if(j < 1 && i > 1)
                       j = 1;
                   break;
               }
               vlPago += parseFloat($(\"#vlParcela\"+i).val());
            }
            if((parseFloat($(\"#" . Html::activeId($model, 'vl_prestacao') . "\").val()) - vlPago) > 0)
            {
                if(nrLinhas < nrParcelas)
                {
                    var valor = 0;

                    $.ajax({
                        url:\"" . CController::createUrl('prestacaoServico/gerarData') . "\",
                        type:\"POST\",
                        dataType: \"JSON\",
                        data:{
                            dtAnterior:$(\"#dtParcela\"+nrLinhas).val(),
                            nrParcelas:nrParcelas,
                            nrLinhas:nrLinhas,
                        },
                        beforeSend:function(){
                            $(\"#nr_parcelas\").attr('disabled', true);
                        },
                        success:function(data){
                            for(i = nrLinhas + 1; i <= nrParcelas ; i++) {
                                var row = '<tr><td><input placeholder=\"Data Parcela\" id=\"dtParcela'+i+'\" value=\"'+data[i]+'\" type=\"text\" name=\"Parcela[nova]['+i+'][dtParcela]\"></td><td><div class=\"input-prepend\"><span class=\"add-on\">R$</span><input placeholder=\"Valor Parcela\" onchange=\"atualizaValor('+i+')\" id=\"vlParcela'+i+'\" type=\"text\" name=\"Parcela[nova]['+i+'][vlParcela]\"></div></td><td><div class=\"input-append\"><input class=\"appendedPrependedInput\" id=\"descParcela'+i+'\" type=\"text\" name=\"Parcela[nova]['+i+'][descParcela]\"><span class=\"add-on\">%</span></div></td></tr>';
                                $('#myTable > tbody:last').append(row);
                            }
                            valor = (parseFloat($(\"#" . Html::activeId($model, 'vl_projeto') . "\").val()) - vlPago) / (nrParcelas - j);
                            $(\"#vlParcela\"+(j+1)).val(valor);
                            atualizaValor((j+1));
                        },
                        complete:function(){
                            $(\"#nr_parcelas\").attr('disabled', false);
                        },
                    });
                } else if(nrLinhas > nrParcelas) {
                    for(nrLinhas; nrLinhas > nrParcelas; nrLinhas--) {
                        if(!$(\"#vlParcela\"+nrLinhas).attr('disabled')) {
                            var valor = parseFloat($(\"#vlParcela\"+nrLinhas).val().replace(',', '.')) + parseFloat($(\"#vlParcela\"+(nrLinhas-1)).val().replace(',', '.'));
                            if(!$(\"#vlParcela\"+(nrLinhas-1)).attr('disabled'))
                            {
                                $(\"#vlParcela\"+(nrLinhas-1)).val(valor.toFixed(2).toString().replace('.',','));
                                $('#myTable > tbody > tr:last').remove();
                            } else
                            {
                                $(this).val(nrLinhas);
                            }
                        } else {
                            $(this).val(nrLinhas+1);
                        }
                    }
                }
            } else {
                $(this).val(nrLinhas);
            }
        });",
        CClientScript::POS_END
    );
}
?>

<?php
/**
 * Script que muda os valores das parcelas
 */
Yii::app()->clientScript->registerScript(
    'atualizaValor',
    "function atualizaValor(i)
                   {
                       var nrLinhas = parseFloat($(\"#divParcelas table tbody tr\").length);
                       if( parseFloat($('#vlParcela'+i).val().replace(',','.')) <= 0 )
                       {
                           alert(\"O Valor da parcela não pode ser 0!\");
                           $('#vlParcela'+i).focus();
                           $('#submit').attr('disabled', true);
                       } else
                       {
                           var vl_prestacao = $('#" . Html::activeId($model, 'vl_projeto') . "').val();
                        var nr_parc = $('#nr_parcelas').val();
                        var vl_pago = 0;

                        for(var j = 1; j <= i; j++)
                        {
                            vl_pago += parseFloat($('#vlParcela'+j).val().replace(',','.'));
                        }

                        $('#vlParcela'+i).val(accounting.formatMoney( $('#vlParcela'+i).val(),'',2,'',','));

                        var vl_reparc = (vl_prestacao - vl_pago)/(nr_parc - i);
                        vl_reparc = vl_reparc.toFixed(2);
                        for(j = i+1; j <= nr_parc; j++)
                        {
                            if(vl_reparc < 0)
                                break;
                            $('#vlParcela'+j).val(accounting.formatMoney(vl_reparc,'',2,'',','));
                            vl_pago += parseFloat(vl_reparc);
                        }

                        diff = vl_prestacao - vl_pago;
                        if(diff > 0)
                        {
                            diff = diff.toFixed(2);
                            if(diff  <= 1)
                            {
                                vl_reparc = parseFloat(vl_reparc) + parseFloat(diff);
                                $('#vlParcela'+nr_parc).val(accounting.formatMoney(vl_reparc,'',2,'',','));
                            }
                            else
                            {
                                alert('Faltam $ '+accounting.formatMoney(diff,'',2,'',',')+' no somatorio das parcelas!')                                
                                $.ajax({
                                        url:\"" . CController::createUrl('prestacaoServico/gerarData') . "\",
                                        type:\"POST\",
                                        dataType: \"JSON\",
                                        data:{ 
                                             dtAnterior:$(\"#dtParcela\"+nrLinhas).val(),
                                             nrParcelas:(nrLinhas+1),
                                             nrLinhas:nrLinhas,
                                         },
                                        beforeSend:function(){
                                             $(\"#nr_parcelas\").attr('disabled', true);
                                             $(\"#divParcelas\").addClass('grid-view-loading');
                                        },                   
                                        success:function(data){

                                            var i = nrLinhas + 1

                                            var row = '<tr><td><input placeholder=\"Data Parcela\" id=\"dtParcela'+i+'\" value=\"'+data[i]+'\" type=\"text\" name=\"Parcela[nova]['+i+'][dtParcela]\"></td><td><div class=\"input-prepend\"><span class=\"add-on\">R$</span><input placeholder=\"Valor Parcela\" onchange=\"atualizaValor('+i+')\" id=\"vlParcela'+i+'\" type=\"text\" name=\"Parcela[nova]['+i+'][vlParcela]\" value=\"'+accounting.formatMoney(diff,'',2,'',',')+'\"></div></td><td><div class=\"input-append\"><input class=\"appendedPrependedInput\" id=\"descParcela'+i+'\" type=\"text\" name=\"Parcela[nova]['+i+'][descParcela]\"><span class=\"add-on\">%</span></div></td></tr>';
                                            $('#myTable > tbody:last').append(row);
                                            $(\"#nr_parcelas\").val(i);
                                        },
                                        complete:function(){
                                             $(\"#nr_parcelas\").attr('disabled', false);
                                             $(\"#divParcelas\").removeClass('grid-view-loading');
                                        },         
                                    });
                            }
                        }
                        if(diff < 0)
                        {
                            diff *= -1;
                            diff = diff.toFixed(2);
                            if(diff  <= 1)
                            {
                                vl_reparc -= diff;
                                $('#vlParcela'+nr_parc).val(accounting.formatMoney(vl_reparc,'',2,'',','));
                            }
                            else
                            {
                                alert('No somatorio das parcelas $ '+accounting.formatMoney(diff,'',2,'',',')+' estão excedendo o valor total!')
                                vl_reparc = parseFloat($('#vlParcela'+nr_parc).val()) - diff;
                                $('#vlParcela'+nr_parc).val(accounting.formatMoney(vl_reparc,'',2,'',','));
                            }
                        }
                    }   
                }",
    CClientScript::POS_END
)
?>
<?php
/**
 * Substitui o ponto (.) pela vírgula e evita parcelas que tenham valor igual a zero
 */
Yii::app()->clientScript->registerScript(
    'blurVlPrestacao',
    "function blurVlPrestacao(obj)
           {
               $(obj).blur(function (){
                   if( parseFloat($(obj).val().replace(',','.')) <= 0)
                      $(obj).focus();
               })
           }",
    CClientScript::POS_END
)
?>
