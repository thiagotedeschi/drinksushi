<?php
/* @var $this TipoServicoController */
/* @var $data TipoServico */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDTipo_servico')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDTipo_servico), array('view', 'id' => $data->IDTipo_servico)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_tipoServico')); ?>:</b>
    <?php echo Html::encode($data->nome_tipoServico); ?>
    <br/>


</div>