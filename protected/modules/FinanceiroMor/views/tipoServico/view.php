<?php
/* @var $this TipoServicoController */
/* @var $model TipoServico */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDTipo_servico',
            'nome_tipoServico',
        ),
    )
); ?>
