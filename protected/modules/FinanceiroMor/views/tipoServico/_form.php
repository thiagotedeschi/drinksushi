<?php
/* @var $this TipoServicoController */
/* @var $model TipoServico */
/* @var $form CActiveForm */
?>


<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'tipo-servico-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0', //classes form-horizontal e margin-0 no form
        )
    )
); ?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_tipoServico', array('class' => 'control-label')); ?>
        <div class="control">
            <?php echo $form->textField(
                $model,
                'nome_tipoServico',
                array('class' => 'span6 m-wrap', 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'nome_tipoServico', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
