<?php

/* @var $this GruposEconomicosController */
/* @var $model GrupoEconomico */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDGrupo_economico',
            'nome_grupoEconomico',
            'IDResponsavel_grupoEconomico' => array(
                'name' => $model->attributeLabels()['IDResponsavel_grupoEconomico'],
                'value' => $model->iDResponsavelGrupoEconomico->nome_cliente,
            ),
        ),
    )
);
?>
