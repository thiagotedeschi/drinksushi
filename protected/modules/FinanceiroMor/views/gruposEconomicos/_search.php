<?php
/* @var $this GruposEconomicosController */
/* @var $model GrupoEconomico */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php
    $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    );
    ?>

    <div class="row">
        <?php echo $form->label($model, 'IDGrupo_economico'); ?>
        <?php echo $form->textField($model, 'IDGrupo_economico'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_grupoEconomico'); ?>
        <?php echo $form->textField($model, 'nome_grupoEconomico', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDResponsavel_grupoEconomico'); ?>
        <?php echo $form->textField($model, 'IDResponsavel_grupoEconomico'); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->