<?php
/* @var $this GruposEconomicosController */
/* @var $model GrupoEconomico */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'grupo-economico-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0', //classes form-horizontal e margin-0 no form
        )
    )
);
?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_grupoEconomico', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'nome_grupoEconomico',
                array('class' => 'span6 m-wrap', 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'nome_grupoEconomico', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDResponsavel_grupoEconomico', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $model,
                'IDResponsavel_grupoEconomico',
                Html::listData(Cliente::model()->findAll(), 'IDCliente', 'nome_cliente'),
                array('class' => 'select2')
            ); ?>
            <?php echo $form->error($model, 'IDResponsavel_grupoEconomico', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>

<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
