<?php

/* @var $this ClienteController */
/* @var $model Cliente */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php

$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'cliente-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDCliente',
            'nome_cliente',
            'razao_cliente',
            array(
                'id' => 'tipo_pessoaCliente',
                'name' => 'tipo_pessoaCliente',
                'value' => '$data->getLabelTipoPessoa()',
                'filter' => Cliente::getTiposPessoa()
            ),
            /*
              'email_cliente',
              'documento_cliente',
              'email_cliente',
              'tel_cliente',
              'ddd_telCliente',
              'IDEndereco_correspondencia',
              'IDGrupo_economico',
              'IDEndereco_fiscal',
              'site_cliente',
              'ins_estadualCliente',
              'ins_municipalCliente',
              'NFEgrupo_cliente',
              'cnae_principal',
             */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
