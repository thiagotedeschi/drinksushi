


<?php
Yii::app()->clientScript->registerScript(
    'mascara',
    'function mudaPessoa($dropDown) {
           tipoPessoa = jQuery("option:selected",$dropDown).val();
           if(tipoPessoa == "f")
           {
               jQuery("#Cliente_documento_cliente").unmask();
               jQuery("#Cliente_documento_cliente").mask("' . HTexto::MASK_CPF . '");
            jQuery(".cnpj").hide();' . '
            jQuery(".cnpj input, span").each(function(){
                $(this).val("");
                $(this).removeClass("checked");
            })' . '
        }
        else
        {
            jQuery("#Cliente_documento_cliente").unmask();
            jQuery("#Cliente_documento_cliente").mask("' . HTexto::MASK_CNPJ . '");
            jQuery(".cnpj").show();
        }
        
    }',
    CClientScript::POS_HEAD
);

Yii::app()->clientScript->registerScript(
    'habilitaNfeGrupo',
    'function habilitaNfeGrupo($dropDown){
       grupoEconomico = jQuery("option:selected",$dropDown).val()
       if(grupoEconomico == 0)
       {
           jQuery("#' . Html::activeId($cliente, 'NFEgrupo_cliente') . '").attr("disabled", "disabled");
        jQuery("#' . Html::activeId($cliente, 'NFEgrupo_cliente') . '").parent().removeClass("checked");

    }
    else
    {
        jQuery("#' . Html::activeId($cliente, 'NFEgrupo_cliente') . '").attr("disabled", false);
        jQuery("#' . Html::activeId($cliente, 'NFEgrupo_cliente') . '").closest("div").removeClass("disabled")
    }
    }',
    CClientScript::POS_HEAD
);

$form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'cliente-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0', //classes form-horizontal e margin-0 no form
        )
    )
);


Yii::app()->clientScript->registerScript(
    'primeiraMascara',
    'mudaPessoa(jQuery("#' . Html::activeId('Cliente', 'tipo_pessoaCliente') . '"))',
    CClientScript::POS_READY
);
?>

<div class='form clearfix positionRelative'>

<p class="note note-warning">Campos com <b>*</b> são obrigatórios.</p>
<?php echo $form->errorSummary($cliente); ?>

<div class="control-group">
    <?php echo $form->labelEx($cliente, 'nome_cliente', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $cliente,
            'nome_cliente',
            array('maxlength' => 55, 'class' => 'span6 m-wrap')
        ); ?>
        <?php echo $form->error($cliente, 'nome_cliente', array('class' => 'help-inline')); ?>
    </div>
</div>


<div class="control-group">
    <?php echo $form->labelEx(
        $cliente,
        'tipo_pessoaCliente',
        array('class' => 'control-label')
    ); //3 tipos padrão: fisica, juridica e publica. se for edição do registro, o campo fica read-only
    ?>
    <div class="controls">
        <?php echo $form->dropDownList(
            $cliente,
            'tipo_pessoaCliente',
            Cliente::getTiposPessoa(),
            array(
                'onchange' => $cliente->isNewRecord ? 'mudaPessoa(this)' : '',
                'class' => !$cliente->isNewRecord ? 'disabled' : '',
                'disabled' => !$cliente->isNewRecord ? 'disabled' : ''
            )
        ); ?>
        <?php echo $form->error($cliente, 'tipo_pessoaCliente', array('class' => 'help-inline')); ?>
    </div>
</div>
<div class="control-group">
    <?php echo $form->labelEx($cliente, 'documento_cliente', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php
        $this->widget(
            'CMaskedTextField',
            array(
                'model' => $cliente,
                'attribute' => 'documento_cliente',
                'mask' => $cliente->isNewRecord ? HTexto::MASK_CNPJ : ($cliente->tipo_pessoaCliente == 'f' ? HTexto::MASK_CPF : HTexto::MASK_CNPJ),
                'htmlOptions' => array('size' => 6, 'class' => 'span6 m-wrap')
            )
        );
        ?>
        <?php echo $form->error($cliente, 'documento_cliente', array('class' => 'help-inline')); ?>
    </div>
</div>
<div class="cnpj">
    <div class="control-group">
        <?php echo $form->labelEx($cliente, 'razao_cliente', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $cliente,
                'razao_cliente',
                array('size' => 80, 'maxlength' => 255, 'class' => 'span6 m-wrap')
            ); ?>
            <?php echo $form->error($cliente, 'razao_cliente', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($cliente, 'ins_estadualCliente', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $cliente,
                'ins_estadualCliente',
                array('size' => 50, 'maxlength' => 50, 'class' => 'span6 m-wrap')
            ); ?>
            <?php echo $form->error($cliente, 'ins_estadualCliente', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($cliente, 'ins_municipalCliente', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $cliente,
                'ins_municipalCliente',
                array('size' => 60, 'maxlength' => 255, 'class' => 'span6 m-wrap')
            ); ?>
            <?php echo $form->error($cliente, 'ins_municipalCliente', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($cliente, 'cnae_principal', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php
            $this->widget(
                'CMaskedTextField',
                array(
                    'model' => $cliente,
                    'attribute' => 'cnae_principal',
                    'mask' => HTexto::MASK_CNAE,
                    'htmlOptions' => array('size' => 6, 'class' => 'span6 m-wrap')
                )
            );
            ?>
            <?php echo $form->error($cliente, 'cnae_principal', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($cliente, 'IDGrupo_economico', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->dropDownList(
                $cliente,
                'IDGrupo_economico',
                Html::listData(GrupoEconomico::model()->findAll(), 'IDGrupo_economico', 'nome_grupoEconomico'),
                array('empty' => array('' => 'Nenhum'), 'onchange' => 'habilitaNfeGrupo(this)')
            ); ?>
            <?php echo $form->error($cliente, 'IDGrupo_economico', array('class' => 'help-inline')); ?>
        </div>
    </div>


    <div class="control-group">
        <?php echo $form->labelEx($cliente, 'NFEgrupo_cliente', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->checkBox(
                $cliente,
                'NFEgrupo_cliente',
                $cliente->isNewRecord || (!$cliente->isNewRecord && $cliente->IDGrupo_economico == '') ? array('disabled' => 'disabled') : array()
            ); ?>
            <?php echo $form->error($cliente, 'NFEgrupo_cliente', array('class' => 'help-inline')); ?>
        </div>
    </div>


</div>
<div class="control-group">
    <?php echo $form->labelEx($cliente, 'email_cliente', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $cliente,
            'email_cliente',
            array('size' => 60, 'maxlength' => 255, 'class' => 'span6 m-wrap')
        ); ?>
        <?php echo $form->error($cliente, 'email_cliente', array('class' => 'help-inline')); ?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->labelEx($cliente, 'tel_cliente', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php
        $this->widget(
            'application.widgets.WCampoTelefone',
            array(
                'model' => $cliente,
                'form' => $form,
                'atributoDDD' => 'ddd_telCliente',
                'atributoTel' => 'tel_cliente',
            )
        )
        ?>
    </div>
</div>


<div class="control-group">
    <?php echo $form->labelEx($cliente, 'site_cliente', array('class' => 'control-label')); ?>
    <div class="controls">
        <?php echo $form->textField(
            $cliente,
            'site_cliente',
            array('size' => 60, 'maxlength' => 255, 'class' => 'span6 m-wrap')
        ); ?>
        <?php echo $form->error($cliente, 'site_cliente', array('class' => 'help-inline')); ?>
    </div>
</div>
<?php
$formEnderecoC = $this->widget(
    'application.widgets.WFormEndereco',
    array(
        'endereco' => $enderecoC,
        'form' => $form,
        'label' => $cliente->attributeLabels()['IDEndereco_correspondencia'],
        'id' => '1'
    )
);
?>

<?php
$this->widget(
    'application.widgets.WFormEndereco',
    array(
        'endereco' => $enderecoC,
        'form' => $form,
        'label' => $cliente->attributeLabels()['IDEndereco_fiscal'],
        'id' => '2',
        'aproveitarEndereco' => $formEnderecoC->id,
        'desabilitarForm' => ($cliente->IDEndereco_correspondencia == $cliente->IDEndereco_fiscal && $cliente->IDEndereco_fiscal != '')
    )
);
?>
</div>
</div>
<div class="form-actions">
    <?php echo Html::submitButton(
        $cliente->isNewRecord ? 'Criar  ' . $cliente->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget('cliente-form'); ?>

