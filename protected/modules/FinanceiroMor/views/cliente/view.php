<?php

/* @var $this ClienteController */
/* @var $model Cliente */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php

$this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDCliente',
            'nome_cliente',
            'razao_cliente',
            'tipo_pessoaCliente' => array(
                'label' => $model->attributeLabels()['tipo_pessoaCliente'],
                'type' => 'raw',
                'value' => $model->getLabelTipoPessoa()
            ),
            'documento_cliente',
            'email_cliente',
            'tel_cliente' => array(
                'label' => $model->attributeLabels()['tel_cliente'],
                'type' => 'raw',
                'value' => HTexto::formataTelefone($model->ddd_telCliente, $model->tel_cliente)
            ),
            'IDEndereco_correspondencia' => array(
                'label' => $model->attributeLabels()['IDEndereco_correspondencia'],
                'type' => 'raw',
                'value' => $model->iDEnderecoCorrespondencia
            ),
            'IDEndereco_fiscal' => array(
                'label' => $model->attributeLabels()['IDEndereco_fiscal'],
                'type' => 'raw',
                'value' => $model->iDEnderecoFiscal
            ),
            'IDGrupo_economico' => array(
                'label' => $model->attributeLabels()['IDGrupo_economico'],
                'type' => 'raw',
                'value' => ($model->IDGrupo_economico == '' ? '' : $model->iDGrupoEconomico->nome_grupoEconomico)
            ),
            'site_cliente',
            'ins_estadualCliente',
            'ins_municipalCliente',
            'NFEgrupo_cliente:boolean',
            'cnae_principal',
        ),
    )
);
?>
