<?php
/* @var $this ClienteController */
/* @var $model Cliente */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDCliente'); ?>
        <?php echo $form->textField($model, 'IDCliente'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_cliente'); ?>
        <?php echo $form->textField($model, 'nome_cliente', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'razao_cliente'); ?>
        <?php echo $form->textField($model, 'razao_cliente', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'tipo_pessoaCliente'); ?>
        <?php echo $form->textField($model, 'tipo_pessoaCliente', array('size' => 1, 'maxlength' => 1)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'documento_cliente'); ?>
        <?php echo $form->textField($model, 'documento_cliente', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'email_cliente'); ?>
        <?php echo $form->textField($model, 'email_cliente', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'tel_cliente'); ?>
        <?php echo $form->textField($model, 'tel_cliente', array('size' => 60, 'maxlength' => 60)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ddd_telCliente'); ?>
        <?php echo $form->textField($model, 'ddd_telCliente', array('size' => 5, 'maxlength' => 5)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDEndereco_correspondencia'); ?>
        <?php echo $form->textField($model, 'IDEndereco_correspondencia'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDGrupo_economico'); ?>
        <?php echo $form->textField($model, 'IDGrupo_economico'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDEndereco_fiscal'); ?>
        <?php echo $form->textField($model, 'IDEndereco_fiscal'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'site_cliente'); ?>
        <?php echo $form->textField($model, 'site_cliente', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ins_estadualCliente'); ?>
        <?php echo $form->textField($model, 'ins_estadualCliente', array('size' => 50, 'maxlength' => 50)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'ins_municipalCliente'); ?>
        <?php echo $form->textField($model, 'ins_municipalCliente', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'NFEgrupo_cliente'); ?>
        <?php echo $form->checkBox($model, 'NFEgrupo_cliente'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'cnae_principal'); ?>
        <?php echo $form->textField($model, 'cnae_principal', array('size' => 20, 'maxlength' => 20)); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->