<?php
/* @var $this ClienteController */
/* @var $model Cliente */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php $this->renderPartial(
    '_form',
    array(
        'cliente' => $cliente,
        'enderecoC' => $enderecoC,
        'enderecoF' => $enderecoF

    )
); ?>