<?php
/* @var $this ClienteController */
/* @var $data Cliente */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDCliente')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDCliente), array('view', 'id' => $data->IDCliente)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_cliente')); ?>:</b>
    <?php echo Html::encode($data->nome_cliente); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('razao_cliente')); ?>:</b>
    <?php echo Html::encode($data->razao_cliente); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('tipo_pessoaCliente')); ?>:</b>
    <?php echo Html::encode($data->tipo_pessoaCliente); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('documento_cliente')); ?>:</b>
    <?php echo Html::encode($data->documento_cliente); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('email_cliente')); ?>:</b>
    <?php echo Html::encode($data->email_cliente); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('tel_cliente')); ?>:</b>
    <?php echo Html::encode($data->tel_cliente); ?>
    <br/>

    <?php /*
	<b><?php echo Html::encode($data->getAttributeLabel('ddd_telCliente')); ?>:</b>
	<?php echo Html::encode($data->ddd_telCliente); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDEndereco_correspondencia')); ?>:</b>
	<?php echo Html::encode($data->IDEndereco_correspondencia); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDGrupo_economico')); ?>:</b>
	<?php echo Html::encode($data->IDGrupo_economico); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('IDEndereco_fiscal')); ?>:</b>
	<?php echo Html::encode($data->IDEndereco_fiscal); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('site_cliente')); ?>:</b>
	<?php echo Html::encode($data->site_cliente); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('ins_estadualCliente')); ?>:</b>
	<?php echo Html::encode($data->ins_estadualCliente); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('ins_municipalCliente')); ?>:</b>
	<?php echo Html::encode($data->ins_municipalCliente); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('NFEgrupo_cliente')); ?>:</b>
	<?php echo Html::encode($data->NFEgrupo_cliente); ?>
	<br />

	<b><?php echo Html::encode($data->getAttributeLabel('cnae_principal')); ?>:</b>
	<?php echo Html::encode($data->cnae_principal); ?>
	<br />

	*/
    ?>

</div>