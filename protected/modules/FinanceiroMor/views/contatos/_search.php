<?php
/* @var $this ContatosController */
/* @var $model ContatoCliente */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'IDContato'); ?>
        <?php echo $form->textField($model, 'IDContato'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'nome_contato'); ?>
        <?php echo $form->textField($model, 'nome_contato', array('size' => 60, 'maxlength' => 255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'principal_contato'); ?>
        <?php echo $form->textField($model, 'principal_contato', array('size' => 1, 'maxlength' => 1)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'IDCliente'); ?>
        <?php echo $form->textField($model, 'IDCliente'); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->