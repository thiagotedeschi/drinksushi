<?php
/* @var $this ContatosController */
/* @var $model ContatoCliente */
/* @var $form CActiveForm */
?>


<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'contato-cliente-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form-horizontal margin-0', //classes form-horizontal e margin-0 no form
        )
    )
); ?>
<div class="form clearfix positionRelative">
    <p class="note note-warning">Campos com <span class="required">*</span> são obrigatórios.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'nome_contato', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField(
                $model,
                'nome_contato',
                array('class' => 'span6 m-wrap', 'maxlength' => 255)
            ); ?>
            <?php echo $form->error($model, 'nome_contato', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'principal_contato', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->checkBox($model, 'principal_contato'); ?>
            <?php echo $form->error($model, 'principal_contato', array('class' => 'help-inline')); ?>
        </div>
    </div>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'IDCliente', array('class' => 'control-label')); ?>
        <div class="controls">
                <span><?php echo $form->dropDownList(
                        $model,
                        'IDCliente',
                        Html::listData(Cliente::model()->findAll(), 'IDCliente', 'nome_cliente')
                    ); ?>
                    <?= $this->retornaAjuda(2) ?></span>
            <?php echo $form->error($model, 'IDCliente', array('class' => 'help-inline')); ?>
        </div>
    </div>
</div>
<div class="form-actions ">
    <?php echo Html::submitButton(
        $model->isNewRecord ? 'Criar  ' . $model->labelModel() : 'Salvar Alterações',
        array('class' => 'botao')
    ); ?>
</div>
<?php $this->endWidget(); ?>
