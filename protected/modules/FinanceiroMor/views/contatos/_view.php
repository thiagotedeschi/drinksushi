<?php
/* @var $this ContatosController */
/* @var $data ContatoCliente */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDContato')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDContato), array('view', 'id' => $data->IDContato)); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('nome_contato')); ?>:</b>
    <?php echo Html::encode($data->nome_contato); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('principal_contato')); ?>:</b>
    <?php echo Html::encode($data->principal_contato); ?>
    <br/>

    <b><?php echo Html::encode($data->getAttributeLabel('IDCliente')); ?>:</b>
    <?php echo Html::encode($data->IDCliente); ?>
    <br/>


</div>