<?php
/* @var $this ContatosController */
/* @var $model ContatoCliente */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>


<?php $this->widget(
    'application.widgets.DetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'IDContato',
            'nome_contato',
            'principal_contato' => array(
                'label' => $model->attributeLabels()['principal_contato'],
                'value' => $model->principal_contato ? 'Sim' : 'Não'
            ),
            'IDCliente' => array(
                'label' => $model->attributeLabels()['IDCliente'],
                'value' => $model->iDCliente->nome_cliente
            ),
        ),
    )
); ?>
