<?php

/* @var $this ContatosController */
/* @var $model ContatoCliente */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));
?>

<p class="note note-warning">
    Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> e <b>&lt;&gt;</b>
    ) No início de cada pesquisa.
</p>


<?php

$this->widget(
    'application.widgets.grid.GridView',
    array(
        'id' => 'contato-cliente-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'selectableRows' => 2,
        'columns' => array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDContato',
            'nome_contato',
            'principal_contato' => array(
                'name' => 'principal_contato',
                'id' => 'principal_contato',
                'value' => '($data->principal_contato) ? "Sim" : "Não"',
                'filter' => array('t' => 'Sim', 'f' => 'Não')
            ),
            array(
                'id' => 'IDCliente',
                'name' => 'IDCliente',
                'value' => '$data->iDCliente()->nome_cliente',
                'filter' => Html::listData(Cliente::model()->findAll(), 'IDCliente', 'nome_cliente')
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )
);
?>
