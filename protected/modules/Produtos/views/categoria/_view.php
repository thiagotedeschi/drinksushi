<?php
/* @var $this CategoriaController */
/* @var $data Categoria */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDCategoria')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDCategoria), array('view', 'id'=>$data->IDCategoria)); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('IDEmpregador')); ?>:</b>
    <?php echo Html::encode($data->IDEmpregador); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('nome_categoria')); ?>:</b>
    <?php echo Html::encode($data->nome_categoria); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('titulo_mega')); ?>:</b>
    <?php echo Html::encode($data->titulo_mega); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('mega_tag')); ?>:</b>
    <?php echo Html::encode($data->mega_tag); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('meta_description')); ?>:</b>
    <?php echo Html::encode($data->meta_description); ?>
    <br />


</div>