<?php
/* @var $this CategoriaController */
/* @var $model Categoria */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        )); ?>

    <div class="row">
        <?php echo $form->label($model,'IDCategoria'); ?>
        <?php echo $form->textField($model,'IDCategoria',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'IDEmpregador'); ?>
        <?php echo $form->textField($model,'IDEmpregador',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'nome_categoria'); ?>
        <?php echo $form->textField($model,'nome_categoria',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'titulo_mega'); ?>
        <?php echo $form->textField($model,'titulo_mega',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'mega_tag'); ?>
        <?php echo $form->textField($model,'mega_tag',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'meta_description'); ?>
        <?php echo $form->textField($model,'meta_description',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->