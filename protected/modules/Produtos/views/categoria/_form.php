<?php
/* @var $this CategoriaController */
/* @var $model Categoria */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'categoria-form',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
            'class' => 'form-horizontal margin-0',
        )
    )); ?>
    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com * são obrigatórios.</p>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDEmpregador', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'IDEmpregador',
                    Yii::app()->session['empregadores'],
                    array(
                        'class' => 'm-wrap span6 select2',
                        'options' => array(
                            Yii::app()->user->getState('IDEmpregador') => array('selected' => true)
                        ),
                        'prompt' => 'Selecione um Restaurante'
                    )
                ); ?>
                <?php echo $form->error($model, 'IDEmpregador', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model,'nome_categoria',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'nome_categoria',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'nome_categoria',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'titulo_mega',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'titulo_mega',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'titulo_mega',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'mega_tag',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'mega_tag',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'mega_tag',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'meta_description',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'meta_description',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'meta_description',array('class'=>'help-inline')); ?>
            </div>
        </div>

    </div>

    <div class="form-actions">
        <?php echo Html::submitButton($model->isNewRecord ? 'Criar  '.$model->labelModel() : 'Salvar Alterações', array('class'=>'botao')); ?>
    </div>
<?php $this->endWidget(); ?>