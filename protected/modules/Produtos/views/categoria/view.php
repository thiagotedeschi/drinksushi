
<?php
/* @var $this CategoriaController */
/* @var $model Categoria */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget('application.widgets.DetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'IDCategoria',
            'IDEmpregador',
            'nome_categoria',
            'titulo_mega',
            'mega_tag',
            'meta_description',
        ),
    )); ?>