<?php
/* @var $this CategoriaController */
/* @var $model Categoria */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

    <p class="note note-warning">
        Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada pesquisa.
    </p>


<?php $this->widget('application.widgets.grid.GridView', array(
        'id'=>'categoria-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'selectableRows' => 2,
        'columns'=>array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDCategoria' => array('value' => '$data->IDCategoria','header' => 'ID da Categoria'),
            'IDEmpregador' => array(
                'value' => '$data->iDEmpregador',
                'name' => 'IDEmpregador',
                'header' => 'Restaurante'
            ),
            'nome_categoria',
            /*
            'titulo_mega',
            'mega_tag',
            'meta_description',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )); ?>