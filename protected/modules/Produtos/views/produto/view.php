<?php
/* @var $this ProdutoController */
/* @var $model Produto */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget('application.widgets.DetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'IDProduto',
            'IDSubcategoria',
            'nome_produto',
            'descricao_produto',
            'imagem_produto',
            'preco_custo',
            'preco_venda',
            'unidade_venda',
            'quantidade_minima',
            'produto_habilitado',
            'somente_clubeCompra',
            'fator_paraClubeCompra',
            'titulo_meta',
            'meta_tag',
            'meta_description',
        ),
    )); ?>