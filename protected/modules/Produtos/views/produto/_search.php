<?php
/* @var $this ProdutoController */
/* @var $model Produto */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        )); ?>

    <div class="row">
        <?php echo $form->label($model,'IDProduto'); ?>
        <?php echo $form->textField($model,'IDProduto',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'IDSubcategoria'); ?>
        <?php echo $form->textField($model,'IDSubcategoria',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'nome_produto'); ?>
        <?php echo $form->textField($model,'nome_produto',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'descricao_produto'); ?>
        <?php echo $form->textArea($model,'descricao_produto',array('rows'=>6, 'cols'=>50,'class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'imagem_produto'); ?>
        <?php echo $form->textField($model,'imagem_produto',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'preco_custo'); ?>
        <?php echo $form->textField($model,'preco_custo',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'preco_venda'); ?>
        <?php echo $form->textField($model,'preco_venda',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'unidade_venda'); ?>
        <?php echo $form->textField($model,'unidade_venda',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'quantidade_minima'); ?>
        <?php echo $form->textField($model,'quantidade_minima',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'produto_habilitado'); ?>
        <?php echo $form->checkBox($model,'produto_habilitado'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'somente_clubeCompra'); ?>
        <?php echo $form->checkBox($model,'somente_clubeCompra'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'fator_paraClubeCompra'); ?>
        <?php echo $form->textField($model,'fator_paraClubeCompra',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'titulo_meta'); ?>
        <?php echo $form->textField($model,'titulo_meta',array('class'=>'m-wrap span6','maxlength'=>65)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'meta_tag'); ?>
        <?php echo $form->textField($model,'meta_tag',array('class'=>'m-wrap span6','maxlength'=>200)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'meta_description'); ?>
        <?php echo $form->textField($model,'meta_description',array('class'=>'m-wrap span6','maxlength'=>156)); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->