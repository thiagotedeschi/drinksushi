<?php
/* @var $this ProdutoController */
/* @var $model Produto */
/* @var $form CActiveForm */
$categorias = Html::listData(Categoria::model()->findAll(), 'IDCategoria', 'nome_categoria');
$subcategorias = Html::listData(Subcategoria::model()->findAll(), 'IDSubcategoria', 'nome_subcategoria');
?>


<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'produto-form',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
            'class' => 'form-horizontal margin-0',
        )
    )); ?>
    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com * são obrigatórios.</p>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDCategoria', array('class' => 'control-label')) ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'IDCategoria',
                    $categorias,
                    array(
                        'class' => 'select2 span4',
                        'prompt' => 'Escolha uma Categoria',
                        'disabled' => $model->isNewRecord ? null : 'disabled'
                        )
                ) ?>
                <span style="margin-left: 1%;" id="spinner"><i class="icon-spinner icon-2x icon-spin"></i></span>
                <?php echo $form->error($model, 'IDCategoria', array('class' => 'help-inline')); ?>
            </div>
        </div>

        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDSubcategoria', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'IDSubcategoria',
                    $subcategorias,
                    array(
                        'class' => 'm-wrap span6 select2',
                        'prompt' => 'Selecione uma Subcategoria'
                    )
                ); ?>
                <?php echo $form->error($model, 'IDSubcategoria', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'nome_produto',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'nome_produto',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'nome_produto',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'descricao_produto',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textArea($model,'descricao_produto',array('rows'=>6, 'cols'=>50,'class'=>'m-wrap span6')); ?>
                <?php echo $form->error($model,'descricao_produto',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'imagem_produto',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'imagem_produto',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'imagem_produto',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'preco_custo',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'preco_custo',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
                <?php echo $form->error($model,'preco_custo',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'preco_venda',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'preco_venda',array('class'=>'m-wrap span6','maxlength'=>11)); ?>
                <?php echo $form->error($model,'preco_venda',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'unidade_venda',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'unidade_venda',array('class'=>'m-wrap span6')); ?>
                <?php echo $form->error($model,'unidade_venda',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'quantidade_minima',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'quantidade_minima',array('class'=>'m-wrap span6')); ?>
                <?php echo $form->error($model,'quantidade_minima',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'produto_habilitado',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->checkBox($model,'produto_habilitado'); ?>
                <?php echo $form->error($model,'produto_habilitado',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'somente_clubeCompra',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->checkBox($model,'somente_clubeCompra'); ?>
                <?php echo $form->error($model,'somente_clubeCompra',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'fator_paraClubeCompra',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'fator_paraClubeCompra',array('class'=>'m-wrap span6')); ?>
                <?php echo $form->error($model,'fator_paraClubeCompra',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'titulo_meta',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'titulo_meta',array('class'=>'m-wrap span6','maxlength'=>65)); ?>
                <?php echo $form->error($model,'titulo_meta',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'meta_tag',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'meta_tag',array('class'=>'m-wrap span6','maxlength'=>200)); ?>
                <?php echo $form->error($model,'meta_tag',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'meta_description',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'meta_description',array('class'=>'m-wrap span6','maxlength'=>156)); ?>
                <?php echo $form->error($model,'meta_description',array('class'=>'help-inline')); ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php echo Html::submitButton($model->isNewRecord ? 'Criar  '.$model->labelModel() : 'Salvar Alterações', array('class'=>'botao')); ?>
    </div>

    <script>
        jQuery(document).ready(function () {
            $('#<?= Html::activeId($model, "IDCategoria")?>').change(function(){
                $.ajax({
                    url: "<?= $this->createUrl('RetornaSubcategorias')?>",
                    type: "POST",
                    dataType: 'JSON',
                    data: {IDCategoria: $('#<?= Html::activeId($model, "IDCategoria")?>').val()},
                    success: function (data) {
                        if (data) {
                            $('#<?= Html::activeId($model, "IDSubcategoria")?>').html(data.txt);
                        }
                    },
                    complete: function () {

                        $('#spinner').addClass('hide');

                        if (<?= $model->IDSubcategoria != '' ? 'true' : 'false'?>)
                            $('#<?= Html::activeId($model, "IDSubcategoria")?>').select2('val', '<?= $model->IDSubcategoria?>');
                    }
                });
            });

        });

    </script>

<?php $this->endWidget(); ?>