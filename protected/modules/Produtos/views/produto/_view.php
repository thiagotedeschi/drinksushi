<?php
/* @var $this ProdutoController */
/* @var $data Produto */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDProduto')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDProduto), array('view', 'id'=>$data->IDProduto)); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('IDSubcategoria')); ?>:</b>
    <?php echo Html::encode($data->IDSubcategoria); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('nome_produto')); ?>:</b>
    <?php echo Html::encode($data->nome_produto); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('descricao_produto')); ?>:</b>
    <?php echo Html::encode($data->descricao_produto); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('imagem_produto')); ?>:</b>
    <?php echo Html::encode($data->imagem_produto); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('preco_custo')); ?>:</b>
    <?php echo Html::encode($data->preco_custo); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('preco_venda')); ?>:</b>
    <?php echo Html::encode($data->preco_venda); ?>
    <br />

    <?php /*
    <b><?php echo Html::encode($data->getAttributeLabel('unidade_venda')); ?>:</b>
    <?php echo Html::encode($data->unidade_venda); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('quantidade_minima')); ?>:</b>
    <?php echo Html::encode($data->quantidade_minima); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('produto_habilitado')); ?>:</b>
    <?php echo Html::encode($data->produto_habilitado); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('somente_clubeCompra')); ?>:</b>
    <?php echo Html::encode($data->somente_clubeCompra); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('fator_paraClubeCompra')); ?>:</b>
    <?php echo Html::encode($data->fator_paraClubeCompra); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('titulo_meta')); ?>:</b>
    <?php echo Html::encode($data->titulo_meta); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('meta_tag')); ?>:</b>
    <?php echo Html::encode($data->meta_tag); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('meta_description')); ?>:</b>
    <?php echo Html::encode($data->meta_description); ?>
    <br />

    */ ?>

</div>