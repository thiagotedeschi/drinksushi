<?php
/* @var $this ProdutoController */
/* @var $model Produto */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

    <p class="note note-warning">
        Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada pesquisa.
    </p>


<?php $this->widget('application.widgets.grid.GridView', array(
        'id'=>'produto-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'selectableRows' => 2,
        'columns'=>array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),
            'IDCategoria' => array(
                'value' => '$data->iDSubcategoria->iDCategoria',
                'name' => 'IDCategoria',
                'header' => 'Categoria'
            ),
            'IDSubcategoria' => array(
                'value' => '$data->iDSubcategoria',
                'name' => 'IDSubcategoria',
                'header' => 'Subcategoria'
            ),
//            'IDProduto',
            'nome_produto',
            /*
            'descricao_produto',
            'imagem_produto',
            'preco_custo',
            'preco_venda',
            'unidade_venda',
            'quantidade_minima',
            'produto_habilitado',
            'somente_clubeCompra',
            'fator_paraClubeCompra',
            'titulo_meta',
            'meta_tag',
            'meta_description',
            */
            array(
                'class' => 'ButtonColumn',
                'template' => '{view}{update}{delete}',
                'buttons' =>
                    array(
                        'view' => array(
                            'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                        ),
                        'update' => array(
                            'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                        ),
                        'delete' => array(
                            'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                        ),
                    ),
            ),
        ),
    )); ?>