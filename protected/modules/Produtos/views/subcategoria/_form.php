<?php
/* @var $this SubcategoriaController */
/* @var $model Subcategoria */
/* @var $form CActiveForm */

$categorias = Html::listData(Categoria::model()->findAll(), 'IDCategoria', 'nome_categoria');
?>


<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'subcategoria-form',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
            'class' => 'form-horizontal margin-0',
        )
    )); ?>
    <div class="form clearfix positionRelative">
        <p class="note note-warning">Campos com * são obrigatórios.</p>
        <?php echo $form->errorSummary($model); ?>

        <div class="control-group">
            <?php echo $form->labelEx($model,'nome_subcategoria',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'nome_subcategoria',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'nome_subcategoria',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model, 'IDCategoria', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList(
                    $model,
                    'IDCategoria',
                    $categorias,
                    array(
                        'class' => 'm-wrap span6 select2',
                        'prompt' => 'Selecione uma Categoria'
                    )
                ); ?>
                <?php echo $form->error($model, 'IDCategoria', array('class' => 'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'titulo_mega',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'titulo_mega',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'titulo_mega',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'meta_tag',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'meta_tag',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'meta_tag',array('class'=>'help-inline')); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $form->labelEx($model,'meta_description',array('class'=>'control-label'));?>
            <div class="controls">
                <?php echo $form->textField($model,'meta_description',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
                <?php echo $form->error($model,'meta_description',array('class'=>'help-inline')); ?>
            </div>
        </div>
    </div>

    <div class="form-actions">
        <?php echo Html::submitButton($model->isNewRecord ? 'Criar  '.$model->labelModel() : 'Salvar Alterações', array('class'=>'botao')); ?>
    </div>
<?php $this->endWidget(); ?>