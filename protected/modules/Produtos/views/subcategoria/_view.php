<?php
/* @var $this SubcategoriaController */
/* @var $data Subcategoria */
?>

<div class="view">

    <b><?php echo Html::encode($data->getAttributeLabel('IDSubcategoria')); ?>:</b>
    <?php echo Html::link(Html::encode($data->IDSubcategoria), array('view', 'id'=>$data->IDSubcategoria)); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('nome_subcategoria')); ?>:</b>
    <?php echo Html::encode($data->nome_subcategoria); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('IDCategoria')); ?>:</b>
    <?php echo Html::encode($data->IDCategoria); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('titulo_mega')); ?>:</b>
    <?php echo Html::encode($data->titulo_mega); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('meta_tag')); ?>:</b>
    <?php echo Html::encode($data->meta_tag); ?>
    <br />

    <b><?php echo Html::encode($data->getAttributeLabel('meta_description')); ?>:</b>
    <?php echo Html::encode($data->meta_description); ?>
    <br />


</div>