<?php
/* @var $this SubcategoriaController */
/* @var $model Subcategoria */

//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>


<?php $this->widget('application.widgets.DetailView', array(
        'data'=>$model,
        'attributes'=>array(
            'IDSubcategoria',
            'nome_subcategoria',
            'IDCategoria',
            'titulo_mega',
            'meta_tag',
            'meta_description',
        ),
    )); ?>