<?php
/* @var $this SubcategoriaController */
/* @var $model Subcategoria */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        )); ?>

    <div class="row">
        <?php echo $form->label($model,'IDSubcategoria'); ?>
        <?php echo $form->textField($model,'IDSubcategoria',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'nome_subcategoria'); ?>
        <?php echo $form->textField($model,'nome_subcategoria',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'IDCategoria'); ?>
        <?php echo $form->textField($model,'IDCategoria',array('class'=>'m-wrap span6')); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'titulo_mega'); ?>
        <?php echo $form->textField($model,'titulo_mega',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'meta_tag'); ?>
        <?php echo $form->textField($model,'meta_tag',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'meta_description'); ?>
        <?php echo $form->textField($model,'meta_description',array('class'=>'m-wrap span6','maxlength'=>255)); ?>
    </div>

    <div class="row buttons">
        <?php echo Html::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->