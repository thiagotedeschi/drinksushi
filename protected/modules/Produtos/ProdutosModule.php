<?php
/**
 * Módulo Categoria.
 *
 * @package base.Categoria
 */

class ProdutosModule extends CWebModule
{
    /**
     * Inicializa o Módulo.
     * Este método é chamado quando o modulo está sendo criado. Por isso,
     * código com objetivo de verificar permissões, setar definições e preparar o funcionamento
     * deste módulo podem ser criado aqui.
     */
    public function init()
    {

    }

    /**
     * Evento disparado imediatamente antes de qualquer Action deste módulo ser chamada.
     * Verificações de autorização e pré requisitos podem ser instanciadas aqui.
     * @param $controller Controller chamado
     * @param $action Action chamada
     * @return boolean Se a execução da Action {$action} deve prosseguir ou não
     */
    public function beforeControllerAction($controller, $action)
    {
        return parent::beforeControllerAction($controller, $action);
    }
}