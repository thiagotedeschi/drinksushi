<?php

$d = DIRECTORY_SEPARATOR;

// O array $argv possui os parametros passados pelo comando
// garantido pela classe CConsole, a ultima possição do array será sempre o nome do CLIENTE.
define('CLIENTE', array_pop($argv));
define('MAIN_LINK', 'http://' . CLIENTE . '.homologacao.intranet.gv/');

// change the following paths if necessary
$yiic = dirname(__FILE__) . $d . '..' . $d . '..' . $d . 'yii-core' . $d . 'framework' . $d . 'yii.php';
$config = dirname(__FILE__) . $d . 'config' . $d . 'console.php';

//Inclui a classe Yii na aplicação
require_once($yiic);
//Cria a aplicação com a configuração setada em protected/config/main.php
Yii::createConsoleApplication($config)->run();
