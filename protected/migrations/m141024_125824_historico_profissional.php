<?php

class m141024_125824_historico_profissional extends CDbMigration
{
    public function up()
    {
        $this->createTable(
            CLIENTE . '.Historico_Profissional',
            array(
                'IDHistoricoProfissional' => 'serial4 PRIMARY KEY',
                'dt_modificacaoHistoricoProfissional' => 'date NOT NULL',
                'motivo_historicoProfissional' => 'varchar NOT NULL',
                'ativacao_historicoProfissional' => 'bool NOT NULL',
                'IDProfissional' => 'int4 NOT NULL',
                'IDUsuarioResponsavel' => 'int4 NOT NULL'
            )
        );
        $this->addForeignKey(
            'fk_historico_profissional',
            CLIENTE . '.Historico_Profissional',
            'IDProfissional',
            CLIENTE . '.Profissional',
            'IDProfissional',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_usuario_responsavel_historico_profissional',
            CLIENTE . '.Historico_Profissional',
            'IDUsuarioResponsavel',
            CLIENTE . '.Usuario',
            'IDUsuario',
            'SET NULL',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable(CLIENTE . '.Historico_Profissional');
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}