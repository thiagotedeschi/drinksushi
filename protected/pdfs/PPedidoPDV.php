<?php
/**
 * Class PPedidoPDV
 * Cria o pdf para impressão do PedidoPDV
 * @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
 * @since 1.0 10/06/2015
 */

class PPedidoPDV extends PdfDocument
{

    /**
     * @var $dt_hoje data atual
     */
    public $dt_hoje;

    /**
     * Gera as informações do documento.
     * @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     * @since 1.0 10/06/2015
     */
    protected function initDocInfo()
    {
        parent::initDocInfo();
        $this->dt_hoje = HData::hoje();
    }

    /**
     *  @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     * @since 1.0 10/06/2015
     */
    public function renderPdf()
    {

        $model = ($this->getDataItem('model'));

        if (!is_object($model)) {
            return false;
        }

        $this->getPdf()->setPrintHeader(false);
        $this->addPage(null,null);

        $this->getPdf()->writeHTML(
            Html::tag('h2', array(), Html::encode("Drink Sushi"), true)
        );
        $this->getPdf()->writeHTML(
            Html::tag(
                'h4',
                array(),
                $this->addBreak(1),
                true
            )
        );

        $this->dadosDoPedido($model);
    }

    /**
     * @author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     * @since 1.0 10/06/2015
     * @return string|void
     */
    public function dadosDoPedido($model)
    {
        $this->table(
            array("Pedido Nº: #" . $model->IDPedidoPDV),
            array(),
            false
        );

        $pedidoProdutos = PedidoProduto::model()->findAllByAttributes(array('IDPedido' => $model->IDPedidoPDV));

        $dataPedido = '<ul>';
        foreach($pedidoProdutos as $pedidoProduto){
            $dataPedido .= '<li>' . $pedidoProduto->getLabelPedidoProduto() . '</li>';
        }
        $dataPedido .= '</ul>';
//        echo "<pre>";
//        print_r($pedidoProdutos);
//        echo "</pre>";
//        die();

        $this->table(
            [
                'Produto',
                'Data',
                'Mesa'
            ],
            array(
                [
                    $dataPedido,
                    $model->dt_aberturaPedido,
                    $model->mesa,
                ]
            ),
            false
        );
    }


}