<?php


class PdfDocument extends EPdfFactoryDoc
{
    /**
     * @const Indentation padrão dos documentos
     */
    const DEFAULT_INDENTATION = '&nbsp;&nbsp;&nbsp;&nbsp;';

    /**
     * @const HR padrão para as assinaturas
     */
    const DEFAULT_HR = '_________________________________________________';

    /**
     * @const Font Family padrão dos documentos
     */
    //const DEFAULT_FONT_FAMILY = 'dejavusans';
    const DEFAULT_FONT_FAMILY = '';

    /**
     * @const Fonte Size padrão dos documentos
     */
    const DEFAULT_FONT_SIZE = '10';

    /**
     * @const Fonte Decoration padrão dos documentos - Pode ser B (negrito), I (itálico) ou vazio
     */
    const DEFAULT_FONT_DECORATION = '';

    /**
     * @var Imagem padrão para os documentos
     */
    public $imagemHeader = null;

    /**
     * @var array fonte padrão para o footer
     */
    public $fonteFooter = [self::DEFAULT_FONT_FAMILY, '', 9];

    /**'
     * @var array fonte padrão para os documentos
     */
    public $defaultFont = [self::DEFAULT_FONT_FAMILY, self::DEFAULT_FONT_DECORATION, self::DEFAULT_FONT_SIZE];

    /**
     * @var array fonte inspirada na tag HTML <h1> para os documentos
     */
    public $fonteH1 = [self::DEFAULT_FONT_FAMILY, 'B', 16];

    /**
     * @var array fonte inspirada na tag HTML <h2> para os documentos
     */
    public $fonteH2 = [self::DEFAULT_FONT_FAMILY, 'B', 13];

    /**
     * @var array fonte inspirada na tag HTML <h3> para os documentos
     */
    public $fonteH3 = [self::DEFAULT_FONT_FAMILY, 'B', 12];

    /**
     * @var array fonte inspirada na tag HTML <h4> para os documentos
     */
    public $fonteH4 = [self::DEFAULT_FONT_FAMILY, '', 11];

    /**
     * @var array fonte inspirada na tag HTML <h5> para os documentos
     */
    public $fonteH5 = [self::DEFAULT_FONT_FAMILY, '', 9];

    /**
     * @var array fonte inspirada na tag HTML <h6> para os documentos
     */
    public $fonteH6 = [self::DEFAULT_FONT_FAMILY, '', 8];

    /**
     * @var string caminho absoluto para os assets da aplicação
     */
    public $assetsPath = '';


    public function init()
    {
        $d = DIRECTORY_SEPARATOR;
        $this->assetsPath = PATH . $d . 'assets' . $d;
        if ($this->imagemHeader == null) {
            $projeto = Projeto::model()->findByAttributes(['slug_projeto' => CLIENTE]);
            $this->imagemHeader = '@' . base64_decode(stream_get_contents($projeto->img_projeto));
            //$this->imagemHeader = Yii::app()->createAbsoluteUrl('FinanceiroMor/projeto/imgProjeto',array('slug' => CLIENTE));
        }
    }


    public function getCss($cssFile = 'pdf-table.css')
    {
        $d = DIRECTORY_SEPARATOR;
        $cssFilePath = ($this->assetsPath . 'css' . $d . 'pdf' . $d . $cssFile);
        $html = '<style>' . file_get_contents($cssFilePath) . '</style>';
        return $html;
    }


    public function initHeader()
    {
        $pdf = $this->getPdf();

        $function = function (&$pdfDocument) {
            if ($pdfDocument->header_xobjid === false) {
                //start a new XObject Template
                $pdfDocument->header_xobjid = $pdfDocument->startTemplate($pdfDocument->w, $pdfDocument->tMargin);
                $headerfont = $pdfDocument->getHeaderFont();
                $headerdata = $pdfDocument->headerFunctionData;
                $pdfDocument->y = $pdfDocument->header_margin;
                if ($pdfDocument->rtl) {
                    $pdfDocument->x = $pdfDocument->w - $pdfDocument->original_rMargin;
                } else {
                    $pdfDocument->x = $pdfDocument->original_lMargin;
                }
                $pdfDocument->SetLineWidth(0.4);
                $pdfDocument->SetDrawColor(90, 90, 90);
                $cell_width = $pdfDocument->w - $pdfDocument->original_lMargin - $pdfDocument->original_rMargin - ($headerdata['logo_width'] * 1.1);
                $cell_height = $pdfDocument->getCellHeight($headerfont[2]) * 1.20;
                if (($headerdata['logo']) AND ($headerdata['logo'] != K_BLANK_IMAGE)) {
                    $imgtype = TCPDF_IMAGES::getImageFileType(K_PATH_IMAGES . $headerdata['logo']);
                    if (($imgtype == 'eps') OR ($imgtype == 'ai')) {
                        $pdfDocument->ImageEps(K_PATH_IMAGES . $headerdata['logo'], '', '', $headerdata['logo_width']);
                    } elseif ($imgtype == 'svg') {
                        $pdfDocument->ImageSVG(K_PATH_IMAGES . $headerdata['logo'], '', '', $headerdata['logo_width']);
                    } else {
                        $pdfDocument->Image(
                            K_PATH_IMAGES . $headerdata['logo'],
                            $pdfDocument->x * 1.05,
                            $pdfDocument->y * 1.20,
                            $headerdata['logo_width'],
                            '',
                            '',
                            '',
                            '',
                            false,
                            300,
                            '',
                            false,
                            false,
                            1
                        );
                    }
                    $pdfDocument->Cell(
                        $headerdata['logo_width'] * 1.1,
                        $cell_height,
                        '',
                        1,
                        0,
                        'C',
                        0,
                        '',
                        0,
                        0,
                        false,
                        'T',
                        'M'
                    );
                }
                // set starting margin for text data cell
                if ($pdfDocument->getRTL()) {
                    $header_x = $pdfDocument->original_rMargin + ($headerdata['logo_width'] * 1.1);
                } else {
                    $header_x = $pdfDocument->original_lMargin + ($headerdata['logo_width'] * 1.1);
                }
                $pdfDocument->SetTextColorArray($pdfDocument->header_text_color);
                // header title
                $pdfDocument->SetFont($headerfont[0], 'B', $headerfont[2]);
                $pdfDocument->SetX($header_x);
                $pdfDocument->MultiCell(
                    $cell_width * 0.5,
                    $cell_height,
                    $headerdata['title'],
                    1,
                    'C',
                    false,
                    0,
                    '',
                    '',
                    true,
                    0,
                    false,
                    true,
                    $cell_height,
                    'M',
                    false
                );
                // header string
                $pdfDocument->SetFont($headerfont[0], '', $headerfont[2] + 1);
                $pdfDocument->SetX($header_x + $cell_width * 0.5);
                $pdfDocument->MultiCell(
                    $cell_width * 0.5,
                    $cell_height * 0.5,
                    $headerdata['string'],
                    1,
                    'C',
                    false,
                    1,
                    '',
                    '',
                    true,
                    0,
                    false,
                    true,
                    $cell_height * 0.5,
                    'M',
                    false
                );
                // header string
                $pdfDocument->SetFont($headerfont[0], '', $headerfont[2] + 1);
                $pdfDocument->SetX($header_x + $cell_width * 0.5);
                $pdfDocument->MultiCell(
                    $cell_width * 0.5,
                    $cell_height * 0.5,
                    $headerdata['data'],
                    1,
                    'C',
                    false,
                    0,
                    '',
                    '',
                    true,
                    0,
                    false,
                    true,
                    $cell_height * 0.5,
                    'M',
                    false
                );

                $pdfDocument->endTemplate();
            }
            // print header template
            $x = 0;
            $dx = 0;
            if (!$pdfDocument->header_xobj_autoreset AND $pdfDocument->booklet AND (($pdfDocument->page % 2) == 0)) {
                // adjust margins for booklet mode
                $dx = ($pdfDocument->original_lMargin - $pdfDocument->original_rMargin);
            }
            if ($pdfDocument->rtl) {
                $x = $pdfDocument->w + $dx;
            } else {
                $x = 0 + $dx;
            }
            $pdfDocument->printTemplate($pdfDocument->header_xobjid, $x, 0, 0, 0, '', '', false);
            if ($pdfDocument->header_xobj_autoreset) {
                // reset header xobject template at each page
                $pdfDocument->header_xobjid = false;
            }
        };

        $this->setDataItem('logo', $this->imagemHeader);
        $this->setDataItem('logo_width', 36);
        $pdf->setHeaderData(
            $function,
            $this->getHeaderData()
        );
    }


    public function initFooter()
    {
        $pdf = $this->getPdf();
        $pdf->setFooterFont($this->fonteFooter);
        $pdf->setFooterData(HAtributos::getLocal('FOOTER_PADRAO_PDF'));
    }


    public function table(
        $header,
        $data,
        $ln = true,
        $returnHtml = false,
        $htmlOptions = ['class' => 'table'],
        $includeCss = true
    ) {
        //necessário para impedir a quebra da tabela no salto entre uma página e outra
        $trOptions = [];
        $headContent = '';
        $headerContent = '';
        if ($header) {
            foreach ($header as $option) {
                if (is_array($option) && !empty($option)) {
                    $head = isset($option['value']) ? $option['value'] : '';
                    unset($option['value']);
                } else {
                    $head = $option;
                    $option = [];
                }
                $headContent .= Html::tag('th', $option, $head);
            }
            $headerContent = Html::tag('thead', ['nobr' => "true"], Html::tag('tr', ['nobr' => "true"], $headContent));
        }
        $trs = '';
        $rowspan = 0;
        $odd = false;
        foreach ($data as $tr) {
            $tds = '';
            ($rowspan > 0) ? $rowspan-- : $odd = !$odd;
            $trOptions['class'] = ($odd) ? '' : 'odd';
            if (is_array($tr)) {
                foreach ($tr as $option) {
                    if (is_array($option) && !empty($option)) {
                        $value = isset($option['value']) ? $option['value'] : '';
                        unset($option['value']);
                        !isset($option['rowspan']) ? : $rowspan = $option['rowspan'] - 1;
                    } else {
                        $value = $option;
                        $option = [];
                    }
                    $tds .= Html::tag('td', $option, $value);
                }
            } else {
                $tds .= Html::tag('td', ['nobr' => "true"], $tr);
            }
            $trOptions['nobr'] = 'true';
            $trs .= Html::tag('tr', $trOptions, $tds);
        }

        $table = Html::tag('table', $htmlOptions, $headerContent . $trs);
        if ($includeCss) {
            $table .= $this->getCss();
        }
        if ($returnHtml) {
            return $table;
        }
        $this->getPdf()->writeHTML($table, $ln);

    }


    public function clear()
    {
        $this->setFont($this->defaultFont);
    }

    /**
     * Retorna ou renderiza o timestamp de um documento
     * @param bool $getUsuario se o timestamp deve incluir o usuário e o Ip correspondentes à requisição
     * @param bool $return se o timestamp deve ser retornado ou impresso diretamente no pdf
     * @return string o timestamp gerado
     */
    public function getTimestamp($getUsuario = false, $return = false)
    {
        $timestamp = 'Gerado em ' . date(HData::BR_DATETIME_FORMAT);
        if ($getUsuario) {
            $timestamp .= ' por ' . Yii::app()->user->login_usuario;
            $timestamp .= ' [' . $_SERVER['REMOTE_ADDR'] . ']';
        }
        if ($return) {
            return $timestamp;
        }
        $this->setFont($this->fonteH6);
        $this->getPdf()->SetY((PDF_MARGIN_BOTTOM + 4) * -1);
        $this->getPdf()->Write(2, $timestamp);
        $this->clear();
    }

    public function setFont($params = self::defaultFont)
    {
        $this->getPdf()->setFont($params[0], $params[1], $params[2]);
    }


    protected function initFont()
    {
        $this->getPdf()->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->setFont($this->defaultFont);
    }


    protected function initDocInfo()
    {
        $pdf = $this->getPdf();
        $pdf->SetAuthor(CLIENTE);
        $pdf->SetTitle($this->getPdfName());
        $pdf->SetSubject($this->getPdfName());
        $pdf->SetProducer(HAtributos::getGlobal('COPYRIGHT'));
        $pdf->SetCreator(HAtributos::getLocal('COPYRIGHT'));
    }


    public function signature()
    {
        $pdfDocument = $this->getPdf();

        $signaturedata = $this->getSignatureData();
        $headerfont = $pdfDocument->getHeaderFont();

        $pdfDocument->SetLineWidth(0.4);
        $pdfDocument->SetDrawColor(90, 90, 90);

        $cell_width = ($pdfDocument->w - $pdfDocument->original_lMargin - $pdfDocument->original_rMargin) / count(
                $signaturedata
            );
        $cell_height = $pdfDocument->getCellHeight($headerfont[2]) + 2;

        $pdfDocument->SetFont($headerfont[0], 'B', $headerfont[2] - 1);

        $pdfDocument->MultiCell(
            $cell_width,
            $cell_height,
            $signaturedata['data'],
            1,
            'C',
            false,
            0,
            '',
            '',
            true,
            0,
            false,
            true,
            $cell_height,
            'M',
            false
        );


        $pdfDocument->MultiCell(
            $cell_width,
            $cell_height,
            $signaturedata['nome'],
            1,
            'C',
            false,
            0,
            '',
            '',
            true,
            0,
            false,
            true,
            $cell_height,
            'M',
            false
        );

        $pdfDocument->SetFont($headerfont[0], 'B', $headerfont[2] - 2);
        $pdfDocument->MultiCell(
            $cell_width,
            $cell_height,
            $signaturedata['assinatura'],
            1,
            'C',
            false,
            0,
            '',
            '',
            true,
            0,
            false,
            true,
            $cell_height,
            'B',
            false
        );
        $this->clear();
    }

    public function getHeaderData()
    {
        return [
            'logo' => $this->imagemHeader,
            'logo_width' => 36,
            'title' => $this->getDataItem('title'),
            'string' => HAtributos::getLocal('HEADER_PADRAO_PDF'),
            'data' => HData::hoje()
        ];
    }


    public function getSignatureData()
    {
        return [
            'data' => HData::hoje(),
            'nome' => '',
            'assinatura' => 'Assinatura',
        ];
    }


    public function addTOC(
        $page = 1,
        $numbersfont = self::DEFAULT_FONT_FAMILY,
        $filler = '.',
        $toc_name = 'Sumário',
        $style = 'B',
        $color = array(0, 0, 0),
        $orientation = 'P'
    ) {
        $this->getPdf()->addTOCPage($orientation);

        $this->getPdf()->Ln();
        $this->getPdf()->MultiCell(0, 1, "<b>$toc_name</b>", 0, 'C', 0, 1, '', '', true, 0, true);
        $this->getPdf()->Ln();

        $this->getPdf()->addTOC($page, $numbersfont, $filler, $toc_name, $style, $color);
        $this->getPdf()->endTOCPage();
    }


    public function addBreak($n = 2)
    {
        $retorno = '<br/>';
        for ($i = 1; $i < $n; $i++) {
            $retorno .= '<br/>';
        }
        return $retorno;
    }
}