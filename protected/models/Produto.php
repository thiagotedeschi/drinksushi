<?php

/**
 * This is the model class for table "drink.Produto".
 *
 * The followings are the available columns in table 'drink.Produto':
 * @property integer $IDProduto
 * @property integer $IDSubcategoria
 * @property string $nome_produto
 * @property string $descricao_produto
 * @property string $imagem_produto
 * @property string $preco_custo
 * @property string $preco_venda
 * @property integer $unidade_venda
 * @property integer $quantidade_minima
 * @property boolean $produto_habilitado
 * @property boolean $somente_clubeCompra
 * @property integer $fator_paraClubeCompra
 * @property string $titulo_meta
 * @property string $meta_tag
 * @property string $meta_description
 *
 * The followings are the available model relations:
 * @property Subcategoria $iDSubcategoria
 * @package base.Models
 */
class Produto extends ActiveRecord
{

//    variável para permitir a pesquisa pela categoria
    public $IDCategoria;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Produto';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array(
                'IDSubcategoria, nome_produto, preco_venda, unidade_venda, quantidade_minima, produto_habilitado, somente_clubeCompra',
                'required'
            ),
            array(
                'IDSubcategoria, unidade_venda, quantidade_minima, fator_paraClubeCompra',
                'numerical',
                'integerOnly' => true
            ),
            array('nome_produto, imagem_produto', 'length', 'max' => 255),
            array('preco_custo, preco_venda', 'length', 'max' => 11),
            array('titulo_meta', 'length', 'max' => 65),
            array('meta_tag', 'length', 'max' => 200),
            array('meta_description', 'length', 'max' => 156),
            array('descricao_produto', 'safe'),
// @todo Please remove those attributes that should not be searched.
            array(
                'IDProduto, IDSubcategoria, nome_produto, descricao_produto, imagem_produto, preco_custo, preco_venda, unidade_venda, quantidade_minima, produto_habilitado, somente_clubeCompra, fator_paraClubeCompra, titulo_meta, meta_tag, meta_description',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDSubcategoria' => array(self::BELONGS_TO, 'Subcategoria', 'IDSubcategoria'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDProduto' => 'Produto',
            'IDSubcategoria' => 'Subcategoria',
            'nome_produto' => 'Nome do Produto',
            'descricao_produto' => 'Descrição do Produto',
            'imagem_produto' => 'Imagem do Produto',
            'preco_custo' => 'Preco de Custo',
            'preco_venda' => 'Preco de Venda',
            'unidade_venda' => 'Unidade de Venda',
            'quantidade_minima' => 'Quantidade Mínima',
            'produto_habilitado' => 'Produto Habilitado?',
            'somente_clubeCompra' => 'Somente para Clube de Compra?',
            'fator_paraClubeCompra' => 'Fator Para Clube de Compra',
            'titulo_meta' => 'Título',
            'meta_tag' => 'Meta Tag',
            'meta_description' => 'Meta Description',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('"IDProduto"', HTexto::tiraLetras($this->IDProduto));
        $criteria->compare('"IDSubcategoria"', $this->IDSubcategoria);
        $criteria->compare('LOWER("nome_produto")', mb_strtolower($this->nome_produto), true);
        $criteria->compare('LOWER("descricao_produto")', mb_strtolower($this->descricao_produto), true);
        $criteria->compare('LOWER("imagem_produto")', mb_strtolower($this->imagem_produto), true);
        $criteria->compare('LOWER("preco_custo")', mb_strtolower($this->preco_custo), true);
        $criteria->compare('LOWER("preco_venda")', mb_strtolower($this->preco_venda), true);
        $criteria->compare('"unidade_venda"', $this->unidade_venda);
        $criteria->compare('"quantidade_minima"', $this->quantidade_minima);
        $criteria->compare('"produto_habilitado"', $this->produto_habilitado);
        $criteria->compare('"somente_clubeCompra"', $this->somente_clubeCompra);
        $criteria->compare('"fator_paraClubeCompra"', $this->fator_paraClubeCompra);
        $criteria->compare('LOWER("titulo_meta")', mb_strtolower($this->titulo_meta), true);
        $criteria->compare('LOWER("meta_tag")', mb_strtolower($this->meta_tag), true);
        $criteria->compare('LOWER("meta_description")', mb_strtolower($this->meta_description), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDProduto" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Produto the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
