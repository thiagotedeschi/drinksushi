<?php

/**
 * This is the model class for table "drink.Trabalhador_Condicao".
 *
 * The followings are the available columns in table 'drink.Trabalhador_Condicao':
 * @property string $IDTrabalhadorCondicao
 * @property integer $IDTipoCondicao
 * @property string $IDTrabalhador
 * @property string $dt_inicioCondicao
 * @property string $dt_fimCondicao
 *
 * The followings are the available model relations:
 * @property TipoCondicao $iDTipoCondicao
 * @property Trabalhador $iDTrabalhador
 * @package base.Models
 */
class TrabalhadorCondicao extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Trabalhador_Condicao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('dt_inicioCondicao', 'required'),
            array('IDTipoCondicao', 'numerical', 'integerOnly' => true),
            array('IDTrabalhador, dt_fimCondicao', 'safe'),
            // @todo Please remove those attributes that should not be searched.
            array(
                'IDTrabalhadorCondicao, IDTipoCondicao, IDTrabalhador, dt_inicioCondicao, dt_fimCondicao',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDTipoCondicao' => array(self::BELONGS_TO, 'TipoCondicao', 'IDTipoCondicao'),
            'iDTrabalhador' => array(self::BELONGS_TO, 'Trabalhador', 'IDTrabalhador'),
        );
    }


    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTrabalhadorCondicao' => 'Id trabalhador Condicao',
            'IDTipoCondicao' => 'Tipo de Condiçao do Trabalhador',
            'IDTrabalhador' => 'Id do trabalhador',
            'dt_inicioCondicao' => 'Data de Início',
            'dt_fimCondicao' => 'Data Término da condição',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('LOWER("IDTrabalhadorCondicao")', mb_strtolower($this->IDTrabalhadorCondicao), true);
        $criteria->compare('"IDTipoCondicao"', $this->IDTipoCondicao);
        $criteria->compare('LOWER("IDTrabalhador")', mb_strtolower($this->IDTrabalhador), true);
        $criteria->compare('LOWER("dt_inicioCondicao")', mb_strtolower($this->dt_inicioCondicao), true);
        $criteria->compare('LOWER("dt_fimCondicao")', mb_strtolower($this->dt_fimCondicao), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TrabalhadorCondicao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function findCondicaoByTrabalhador($IDTrabalhador)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('"IDTrabalhador"', $IDTrabalhador);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
}
