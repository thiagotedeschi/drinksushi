<?php
/**
 * @package base.Models
 */
class EnderecoProfissional extends Endereco
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Endereco';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}