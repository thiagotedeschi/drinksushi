<?php

/**
 * This is the model class for table "drink.Email_contato".
 *
 * The followings are the available columns in table 'drink.Email_contato':
 * @property integer $IDEmail_contato
 * @property string $email_contato
 * @property string $principal_emailContato
 * @property integer $IDContato
 *
 * The followings are the available model relations:
 * @property Contato $iDContato
 * @package base.Models
 */
class EmailContato extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Email_contato';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('email_contato', 'required'),
            array('IDContato', 'numerical', 'integerOnly' => true),
            array('email_contato', 'length', 'max' => 255),
        );
    }


    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDContato' => array(self::BELONGS_TO, 'Contato', 'IDContato'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDEmail_contato' => 'Email do Contato',
            'email_contato' => 'Email Contato',
            'principal_emailContato' => 'Principal Email Contato',
            'IDContato' => 'Idcontato',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EmailContato the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
