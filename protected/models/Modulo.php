<?php

/**
 * This is the model class for table "public.Modulo".
 *
 * The followings are the available columns in table 'public.Modulo':
 * @property integer $IDModulo
 * @property string $nome_modulo
 * @property string $desc_modulo
 * @property string $slug_modulo
 * @property boolean $eh_basicoModulo
 * @property integer $n_ordemModulo
 * @property integer $IDTipo_modulo
 * @property string $icone_modulo
 *
 * The followings are the available model relations:
 * @property TipoModulo $IDTipoModulo
 * @property ItemMenu[] $itemMenus
 * @property Projeto[] $projetos
 * @package base.Models
 */
class Modulo extends ActiveRecord
{

    public $buscar_ehbasicoModulo;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Modulo';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome_modulo, desc_modulo, slug_modulo, icone_modulo, n_ordemModulo', 'required'),
            array('n_ordemModulo, IDTipo_modulo', 'numerical', 'integerOnly' => true),
            array('nome_modulo', 'length', 'max' => 30),
            array('slug_modulo, icone_modulo', 'length', 'max' => 20),
            array(
                'IDModulo, nome_modulo, IDTipo_modulo, buscar_ehbasicoModulo',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'IDTipoModulo' => array(
                self::BELONGS_TO,
                'TipoModulo',
                'IDTipo_modulo',
                'order' => '"nome_tipoModulo" ASC'
            ),
            'itensMenu' => array(self::HAS_MANY, 'ItemMenu', 'IDModulo', 'order' => 'nome_menu ASC'),
            'projetos' => array(self::MANY_MANY, 'Projeto', 'Projetos_tem_modulos(IDModulo, IDProjeto)'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDModulo' => 'Módulo',
            'nome_modulo' => 'Nome do Módulo',
            'desc_modulo' => 'Descrição do Módulo',
            'slug_modulo' => 'Slug do Módulo',
            'eh_basicoModulo' => 'Módulo Básico?',
            'buscar_ehbasicoModulo' => 'Módulo Básico?',
            'n_ordemModulo' => 'N° Ordem Módulo',
            'IDTipo_modulo' => 'Super Módulo',
            'icone_modulo' => 'Ícone do Módulo',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('("IDModulo")', HTexto::tiraLetras($this->IDModulo));
        $criteria->compare('LOWER("nome_modulo")', mb_strtolower($this->nome_modulo), true);
        $criteria->compare('("IDTipo_modulo")', mb_strtolower($this->IDTipo_modulo));

        if ($this->buscar_ehbasicoModulo === '0') {
            $criteria->addCondition('NOT "eh_basicoModulo"');
        } else {
            if ($this->buscar_ehbasicoModulo === '1') {
                $criteria->addCondition('"eh_basicoModulo"');
            }
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDModulo" DESC',
                'attributes' => array(
                    'buscar_ehbasicoModulo' => array(
                        'asc' => 'NOT "eh_basicoModulo"',
                        'desc' => '"eh_basicoModulo"',
                    ),
                    '*',
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Modulo the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
