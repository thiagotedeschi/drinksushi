<?php

/**
 * This is the model class for table "drink.Observacao_Trabalhador".
 *
 * The followings are the available columns in table 'drink.Observacao_Trabalhador':
 * @property integer $IDObservacaoTrabalhador
 * @property integer $IDEmpregadorTrabalhador
 * @property string $observacao_trabalhador
 * @property string $nome_responsavelObservacao
 * @property string $email_responsavelObservacao
 * @property boolean $visualizar_prontuarioObservacao
 * @property string $dt_desativacaoObservacao
 * @property string $dt_cadastroObservacao
 * @property string $motivo_desativacaoObservacao
 *
 * The followings are the available model relations:
 * @property EmpregadorTrabalhador $iDEmpregadorTrabalhador
 * @property Profissional[] Profissionals
 * @package base.Models
 */
class ObservacaoTrabalhador extends ActiveRecord
{

    public $nome_trabalhador;

    /**
     * @var $ativo boolean informa se o Agente está ativo ou não
     */
    public $ativo;
    /**
     * @var int $IDEmpregador Usada apenas na pesquisa. Traz o ID do Empregador a ser pesquisado
     */
    public $IDEmpregador;
    /**
     * @var int $funcoes guarda o ID do setor
     */
    public $IDTrabalhador;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Observacao_Trabalhador';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array(
                'IDEmpregadorTrabalhador, nome_responsavelObservacao, observacao_trabalhador, dt_cadastroObservacao',
                'required'
            ),
            array('IDEmpregadorTrabalhador', 'numerical', 'integerOnly' => true),
            array('nome_responsavelObservacao, email_responsavelObservacao', 'length', 'max' => 80),
            array('motivo_desativacaoObservacao', 'length', 'max' => 255),
            array('dt_desativacaoObservacao, motivo_desativacaoObservacao', 'required', 'on' => 'disable'),
            array(
                'dt_desativacaoObservacao',
                'compare',
                'compareAttribute' => 'dt_cadastroObservacao',
                'operator' => '>=',
                'allowEmpty' => true,
                'message' => '{attribute} tem que ser igual ou maior que {compareAttribute}.'
            ),
            array(
                'observacao_trabalhador, IDEmpregador, visualizar_prontuarioObservacao, dt_desativacaoObservacao',
                'safe'
            ),
            array(
                'IDObservacaoTrabalhador, ativo, IDEmpregador, IDTrabalhador, dt_cadastroObservacao, nome_trabalhador',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDEmpregadorTrabalhador' => array(
                self::BELONGS_TO,
                'EmpregadorTrabalhador',
                'IDEmpregadorTrabalhador',
                'with' => array('iDEmpregador', 'iDTrabalhador')
            ),
            'Profissionals' => array(
                self::MANY_MANY,
                'Profissional',
                CLIENTE . '.Profissional_Observacao_Trabalhador(IDObservacaoTrabalhador, IDProfissional)'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDObservacaoTrabalhador' => 'Observação do Trabalhador',
            'IDEmpregadorTrabalhador' => 'Trabalhador',
            'observacao_trabalhador' => 'Observação do Trabalhador',
            'nome_responsavelObservacao' => 'Nome do Responsável',
            'email_responsavelObservacao' => 'Email do Responsável',
            'visualizar_prontuarioObservacao' => 'Visualizar no Exame Clínico?',
            'dt_desativacaoObservacao' => 'Data de Desativação',
            'dt_cadastroObservacao' => 'Data de Cadastro',
            'motivo_desativacaoObservacao' => 'Motivo da Desativação',
            'Profissionals' => 'Profissionais',
            'nome_trabalhador' => 'Trabalhador',
            'ativo' => 'Ativo?',
            'IDEmpregador' => 'Empregador'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->with = array('iDEmpregadorTrabalhador');

        // Como filtrar os resultados pro empregador selecionado.
        $emp = Yii::app()->user->getState('IDEmpregador');
        if ($emp == null) {
            $criteria->addInCondition('"iDEmpregadorTrabalhador"."IDEmpregador"', null);
        } else {
            $criteria->compare('"iDEmpregadorTrabalhador"."IDEmpregador"', $emp);
        }

        $criteria->compare('"IDObservacaoTrabalhador"', HTexto::tiraLetras($this->IDObservacaoTrabalhador));
        $criteria->compare('"IDEmpregadorTrabalhador"', $this->IDEmpregadorTrabalhador);
//        $criteria->compare('"iDEmpregadorTrabalhador"."IDTrabalhador"', $this->IDTrabalhador);
        $criteria->compare('LOWER("iDTrabalhador"."nome_trabalhador")', mb_strtolower($this->nome_trabalhador), true);


        if ($this->ativo === '1') {
            $criteria->addCondition('"dt_desativacaoObservacao" IS NULL');
        } elseif ($this->ativo === '0') {
            $criteria->addCondition('"dt_desativacaoObservacao" IS NOT NULL');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDObservacaoTrabalhador" DESC',
                'attributes' => array(
                    'nome_trabalhador' => array(
                        'asc' => '"iDTrabalhador"."nome_trabalhador"',
                        'desc' => '"iDTrabalhador"."nome_trabalhador" DESC',
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ObservacaoTrabalhador the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function afterFind()
    {
        if (!empty($this->dt_desativacaoObservacao)) {
            $this->ativo = false;
        } else {
            $this->ativo = true;
        }
        return parent::afterFind();
    }


    public function atualizaProfissionais($IDProfissionais)
    {
        $antigos = ProfissionalObservacaoTrabalhador::model()->findAllByAttributes(
            array('IDObservacaoTrabalhador' => $this->IDObservacaoTrabalhador)
        );

        $antigos = array_keys(Html::listData($antigos, 'IDProfissional', ''));
        $excluirProf = array_diff($antigos, $IDProfissionais);
        $addProf = array_diff($IDProfissionais, $antigos);


        foreach ($excluirProf as $excluir) {
            $r = ProfissionalObservacaoTrabalhador::model()->findByAttributes(
                array("IDProfissional" => $excluir, 'IDObservacaoTrabalhador' => $this->IDObservacaoTrabalhador)
            );
            if ($r != null) {
                $r->delete();
            }
        }

        $usuarios = array();
        foreach ($addProf as $add) {
            $a = new ProfissionalObservacaoTrabalhador();
            $a->IDObservacaoTrabalhador = $this->IDObservacaoTrabalhador;
            $a->IDProfissional = $add;
            if ($a->save()) {
                $usuario = Usuario::model()->findByAttributes(['IDProfissional' => $add]);
                if ($usuario) {
                    $usuarios[] = $usuario->IDUsuario;
                }
            }
        }

        Notificacao::enviaNotificacao(
            $usuarios,
            'NOVA_OBS_TRABALHADOR',
            array('nome_trabalhador' => $this->iDEmpregadorTrabalhador->iDTrabalhador),
            'file-text',
            Yii::app()->createUrl(
                'Trabalhador/observacaoTrabalhador/view',
                array('id' => $this->IDObservacaoTrabalhador)
            )
        );
        //fim da inserção

    }

    public function getListaProfissionais()
    {
        return Html::htmlList(Html::listData($this->Profissionals, 'IDProfissional', 'nome_profissional'), array());
    }
}
