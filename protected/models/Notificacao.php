<?php

/**
 * This is the model class for table "drink.Notificacao".
 *
 * The followings are the available columns in table 'drink.Notificacao':
 * @property integer $IDNotificacao
 * @property string $icone_notificacao
 * @property integer $IDProfissional
 * @property string $dt_envioNotificacao
 * @property string $dt_leituraNotificacao
 * @property string $conteudo_notificacao
 * @property string $link_notificacao
 * @property string $tipo_notificacao
 *
 * The followings are the available model relations:
 * @property Profissional $iDProfissional
 * @package base.Models
 */
class Notificacao extends ActiveRecord
{
    /**
     * Gera notificação com ícone Verde
     */
    const NT_GREEN = 'success';

    /**
     * Gera notificação com ícone Azul
     */
    const NT_BLUE = 'info';

    /**
     * Gera notificação com ícone Vermelho
     */
    const NT_RED = 'important';

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Notificacao';
    }


    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'icone_notificacao, IDProfissional, dt_envioNotificacao, conteudo_notificacao, tipo_notificacao',
                'required'
            ),
            array('IDProfissional', 'numerical', 'integerOnly' => true),
            array('icone_notificacao, tipo_notificacao', 'length', 'max' => 20),
            array('conteudo_notificacao, link_notificacao', 'length', 'max' => 255),
            array('dt_leituraNotificacao', 'safe'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDProfissional' => array(self::BELONGS_TO, 'Profissional', 'IDProfissional'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDNotificacao' => 'Notificação',
            'icone_notificacao' => 'Icone Notificacao',
            'IDProfissional' => 'Profissional',
            'dt_envioNotificacao' => 'Dt Envio Notificacao',
            'dt_leituraNotificacao' => 'Dt Leitura Notificacao',
            'conteudo_notificacao' => 'Conteudo Notificacao',
            'link_notificacao' => 'Link Notificacao',
            'tipo_notificacao' => 'Tipo Notificacao',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Notificacao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function enviaNotificacao(
        $profissionais,
        $conteudo,
        $parametros = array(),
        $icone = 'bullhorn',
        $link = '#',
        $cor = Notificacao::NT_BLUE
    ) {
        foreach ($profissionais as $profissional) {
            $notificacao = new Notificacao;
            $notificacao->dt_envioNotificacao = new CDbExpression('CURRENT_TIMESTAMP(0)');
            $notificacao->IDProfissional = $profissional;
            //$notificacao->conteudo_notificacao = ComunicadoController::getConteudoComunicado($conteudo, $parametros);
            $notificacao->icone_notificacao = $icone;
            $notificacao->link_notificacao = $link;
            $notificacao->tipo_notificacao = $cor;
            $notificacao->onsave();
        }
    }

    public function onsave()
    {
        if ($this->save()) {
            if ($this->iDProfissional()->receber_emails) {
                $console = new CConsole();
                $console->runCommand('email notificacao', array('--id=' . $this->IDNotificacao));
            }
        }
    }


}
