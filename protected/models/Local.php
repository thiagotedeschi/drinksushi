<?php

/**
 * This is the model class for table "drink.Local".
 *
 * The followings are the available columns in table 'drink.Local':
 * @property integer $IDLocal
 * @property string $nome_local
 * @property integer $IDEndereco
 *
 * The followings are the available model relations:
 * @property EnderecoProfissional $iDEndereco
 * @property LocalHorario[] $localHorarios
 * @property LocalProfissionalHorario[] $localProfissionalHorarios
 * @package base.Models
 */
class Local extends ActiveRecord
{
    /*Endereço do local de trabalho, utilizada principalmente na search
     * @var busca_endereco
     * */
    public $busca_endereco;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Local';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('nome_local', 'required'),
            array('IDEndereco', 'numerical', 'integerOnly' => true),
            array('nome_local', 'length', 'max' => 50),
// The following rule is used by search().
            array('IDLocal, nome_local, IDEndereco, busca_endereco', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDEndereco' => array(self::BELONGS_TO, 'EnderecoProfissional', 'IDEndereco'),
            'localHorarios' => array(self::HAS_MANY, 'LocalHorario', 'IDLocal'),
            'localProfissionalHorarios' => array(self::HAS_MANY, 'LocalProfissionalHorario', 'IDLocal'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDLocal' => 'Local de Trabalho',
            'nome_local' => 'Nome Local',
            'IDEndereco' => 'Endereço',
            'busca_endereco' => 'Endereço',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->with = ['iDEndereco', 'iDEndereco.iDCidade', 'iDEndereco.iDCidade.iDEstadoCidade'];

        $criteria->compare('"IDLocal"', HTexto::tiraLetras($this->IDLocal));
        $criteria->compare('LOWER("nome_local")', mb_strtolower($this->nome_local), true);
        $criteria->compare('"IDEndereco"', $this->IDEndereco);
        $criteria->compare('LOWER("iDEndereco"."logradouro_endereco")', mb_strtolower($this->busca_endereco), true);
        $criteria->compare('LOWER("iDEndereco"."numero_endereco")', mb_strtolower($this->busca_endereco), true, 'OR');
        $criteria->compare(
            'LOWER("iDEndereco"."complemento_endereco")',
            mb_strtolower($this->busca_endereco),
            true,
            'OR'
        );
        $criteria->compare('LOWER("iDCidade"."nome_cidadeIbge")', mb_strtolower($this->busca_endereco), true, 'OR');
        $criteria->compare('LOWER("iDEstadoCidade"."sigla_estado")', mb_strtolower($this->busca_endereco), true, 'OR');

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDLocal" DESC',
                'attributes' => array(
                    'busca_endereco' => array(
                        'asc' => '"iDEndereco"."logradouro_endereco"',
                        'desc' => '"iDEndereco"."logradouro_endereco" DESC',
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Local the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
