<?php

/**
 * This is the model class for table "drink.Empregador".
 *
 * The followings are the available columns in table 'drink.Empregador':
 * @property integer $IDEmpregador
 * @property string $nome_empregador
 * @property string $razao_empregador
 * @property string $tipo_pessoaEmpregador
 * @property string $documento_empregador
 * @property string $email_empregador
 * @property string $site_empregador
 * @property string $NFEgrupo_empregador
 * @property string $tel_empregador
 * @property string $ddd_telEmpregador
 * @property string $ins_estadualEmpregador
 * @property string $ins_municipalEmpregador,
 * @property string $CNO_empregador
 * @property string $CAEPF_empregador
 * @property integer $IDEndereco_correspondencia
 * @property integer $IDEndereco_fiscal
 * @property integer $IDGrupo_economico
 * @property integer $IDClassificacaoTributaria
 * @property boolean $optante_simplesEmpregador
 *
 * The followings are the available model relations:
 * @property EmpregadorTemCnaes[] $empregadorTemCnaes
 * @property Endereco $iDEnderecoCorrespondencia
 * @property Endereco $iDEnderecoFiscal
 * @property GrupoEconomico $iDGrupoEconomico
 * @property ClassificacaoTributaria $iDClassificacao_Tributaria
 * @property Setor[] $setors
 * @property ResponsavelLegal[] $responsavelLegals
 * @property ProfissionalEmpregador[] $profissionalEmpregadors
 * @property Contato[] $contatos
 * @property GrupoEconomico[] $grupoEconomicos
 * @package base.Models
 */
class Empregador extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Empregador';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'nome_empregador, tipo_pessoaEmpregador, documento_empregador, IDEndereco_correspondencia, IDEndereco_fiscal',
                'required'
            ),
            array(
                'IDEndereco_correspondencia, IDEndereco_fiscal, IDGrupo_economico, CNO_empregador, IDClassificacaoTributaria',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'nome_empregador, razao_empregador, documento_empregador, email_empregador, site_empregador, NFEgrupo_empregador, ins_municipalEmpregador,
                , CAEPF_empregador',
                'length',
                'max' => 255
            ),
            array('tipo_pessoaEmpregador', 'length', 'max' => 1),
            array('tel_empregador', 'length', 'max' => 60),
            array('ddd_telEmpregador', 'length', 'max' => 5),
            array('ins_estadualEmpregador', 'length', 'max' => 50),
            array('documento_empregador', 'application.components.validators.ValidadorDocumento'),
            array('optante_simplesEmpregador', 'safe'),
            // The following rule is used by search().
            array(
                'IDEmpregador, nome_empregador, razao_empregador,',
                'safe',
                'on' => 'search'
            ),
        );
    }

    public function scopes()
    {
        return array(
            'sIDEmpregador' => array(
                'order' => '"IDEmpregador"'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'empregadorTemCnaes' => array(self::HAS_MANY, 'EmpregadorTemCnaes', 'IDEmpregador'),
            'iDEnderecoCorrespondencia' => array(self::BELONGS_TO, 'EnderecoEmpregador', 'IDEndereco_correspondencia'),
            'iDClassificacaoTributaria' => array(
                self::BELONGS_TO,
                'ClassificacaoTributaria',
                'IDClassificacaoTributaria'
            ),
            'responsavelLegalPPPs' => array(self::HAS_MANY, 'ResponsavelLegalPPP', 'IDEmpregador'),
            'iDEnderecoFiscal' => array(self::BELONGS_TO, 'EnderecoEmpregador', 'IDEndereco_fiscal'),
            'iDGrupoEconomico' => array(self::BELONGS_TO, 'GrupoEconomicoEmpregador', 'IDGrupo_economico'),
            'setors' => array(self::HAS_MANY, 'Setor', 'IDEmpregador'),
            'responsavelLegals' => array(self::HAS_MANY, 'ResponsavelLegal', 'IDEmpregador'),
            'profissionalEmpregadors' => array(self::HAS_MANY, 'ProfissionalEmpregador', 'IDEmpregador'),
            'contatos' => array(self::HAS_MANY, 'Contato', 'IDEmpregador'),
            'grupoEconomicos' => array(self::HAS_MANY, 'GrupoEconomico', 'IDResponsavel_grupoEconomico'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDEmpregador' => 'Empregador',
            'nome_empregador' => 'Nome Fantasia',
            'razao_empregador' => 'Razão Social',
            'tipo_pessoaEmpregador' => 'Tipo de Pessoa Empregador',
            'documento_empregador' => 'Documento do Empregador',
            'email_empregador' => 'Email do Empregador (Nota Fiscal)',
            'site_empregador' => 'Site do Empregador',
            'NFEgrupo_empregador' => 'NFe para o grupo?',
            'tel_empregador' => 'Telefone Principal',
            'ddd_telEmpregador' => 'DDD Telefone',
            'ins_estadualEmpregador' => 'Insc. Estadual',
            'ins_municipalEmpregador' => 'Insc. Municipal',
            'IDEndereco_correspondencia' => 'Endereco de Correspondência',
            'IDEndereco_fiscal' => 'Endereco Fiscal',
            'IDGrupo_economico' => 'Grupo Econômico',
            'IDClassificacaoTributaria' => 'Classificação Tributária',
            'CNO_empregador' => 'CNO',
            'CAEPF_empregador' => 'CAEPF',
            'optante_simplesEmpregador' => 'Optante pelo Simples?'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('"IDEmpregador"', HTexto::tiraLetras($this->IDEmpregador));
        $criteria->compare('LOWER("nome_empregador")', mb_strtolower($this->nome_empregador), true);
        $criteria->compare('LOWER("razao_empregador")', mb_strtolower($this->razao_empregador), true);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
                'Pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                    //mude o número de registros por página aqui
                ),
                'sort' => array(
                    'defaultOrder' => '"IDEmpregador" DESC',
                )
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Empregador the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getLabelEmpregador($showId = true, $razao = false)
    {
        $return = '';
        if ($showId) {
            $return = '(' . $this->IDEmpregador . ') - ';
        }
        if ($razao && !empty($this->razao_empregador)) {
            $return .= $this->razao_empregador;
        } else {
            $return .= $this->nome_empregador;
        }

        return $return;
    }

    public function getLabelEmpregadores()
    {
        $labels = array();
        $empregadores = $this->sIDEmpregador()->findAll();
        foreach ($empregadores as $empregador) {
            $labels[$empregador->IDEmpregador] = $empregador->getLabelEmpregador();
        }
        return $labels;
    }

    public function getLabelTipoPessoa()
    {
        return HTexto::$tiposPessoa[$this->tipo_pessoaEmpregador];
    }

    public function save($param1 = true, $param2 = null)
    {
        $this->documento_empregador = HTexto::somenteNumeros($this->documento_empregador);
        return parent::save($param1, $param2);
    }

    public function getCnaePrincipal()
    {
        return array_pop($this->empregadorTemCnaes(array('condition' => '"principal_CNAE"', 'limit' => '1')));
    }

    public function getCnaesSecundarios()
    {
        return $this->empregadorTemCnaes(array('condition' => 'NOT "principal_CNAE"'));
    }

    public function getCnaes()
    {
        return $this->empregadorTemCnaes();
    }

    /**
     * Wrapper para a função getLabelEmpregador
     * @return String label do Empregador
     */
    public function __toString()
    {
        return $this->getLabelEmpregador();
    }

    public function atualizaResp($controller, $dados)
    {
        $error = false;
        //zera o array de responsaveis para fazer a validação
        $responsaveis = array();
        //valida responsaveis legais
        foreach ($dados['ResponsavelLegal'] as $responsavelLegal) {
            if ($responsavelLegal['IDResponsavel_Legal'] != '') {
                $responsavel = ResponsavelLegal::model()->findByAttributes(
                    [
                        'IDResponsavel_Legal' => $responsavelLegal['IDResponsavel_Legal'],
                        'IDEmpregador' => $this->IDEmpregador,
                    ]
                );
                $responsavel->attributes = $responsavelLegal;

            } else {
                $responsavel = new ResponsavelLegal();
                $responsavel->attributes = $responsavelLegal;
                $responsavel->IDEmpregador = $this->IDEmpregador;
            }
            //guarda o responsavel de volta no array, depois de validado
            if (!$responsavel->validate()) {
                $error = true;
            }
            $responsaveis[] = $responsavel;
        }

        if (!$error) {
            //responsaveis antigos
            $antigos = ResponsavelLegal::model()->findAllByAttributes(
                array('IDEmpregador' => $this->IDEmpregador)
            );
            $antigos = array_keys(CHtml::listData($antigos, 'IDResponsavel_Legal', ''));
            $novos = array_keys(CHtml::listData($dados['ResponsavelLegal'], 'IDResponsavel_Legal', ''));
            $excluirResp = array_diff($antigos, $novos);
            //exclui responsáveis removidos
            foreach ($excluirResp as $excluir) {
                $r = ResponsavelLegal::model()->findByAttributes(
                    array("IDResponsavel_Legal" => $excluir, 'IDEmpregador' => $this->IDEmpregador)
                );
                if ($r != null) {
                    $r->delete();
                }
            }
            //salva as mudanças
            foreach ($responsaveis as $responsavel) {
                $responsavel->save(false);
            }
            $controller->redirect(array('view', 'id' => $this->IDEmpregador));
        }
        return $responsaveis;
    }

    public function findAllByProfissional($IDProfissional, $ativos = true)
    {
        $criteria = new CDbCriteria;
        $profissional = Profissional::model()->findByPk($IDProfissional);
        if ($profissional->todas_empresasProfissional == false) {
            $criteria->join = 'INNER JOIN ' . CLIENTE . '."Profissional_Empregador" "pe" ON
            "pe"."IDEmpregador" = "t"."IDEmpregador"';
            $criteria->compare('"pe"."IDProfissional"', $IDProfissional);
            if ($ativos) {
                $criteria->compare('"pe"."dt_desativacao"', null);
                $criteria->addCondition(
                    "((dt_fim IS NULL) OR (dt_fim > now())) AND ((dt_inicio IS NULL) OR (dt_inicio < now()))"
                );
            }
        } elseif ($ativos) {
            $criteria->with = array('profissionalEmpregadors');
            $criteria->compare('"dt_desativacao"', null);
            $criteria->addCondition(
                "((dt_fim IS NULL) OR (dt_fim > now())) AND ((dt_inicio IS NULL) OR (dt_inicio < now()))"
            );
        }
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function getDocumentoEmpregador()
    {
        $mask = $this->tipo_pessoaEmpregador == 'f' ? HTexto::MASK_CPF : HTexto::MASK_CNPJ;
        return HTexto::formataString($mask, $this->documento_empregador);

    }

    public function getNrRealTrabalhadores()
    {
        $consulta = $this->getComandoNrRealTrabalhadores()->queryRow();;
        return $consulta['nr_trab'];
    }

    public function getComandoNrRealTrabalhadores()
    {
        $consulta = Yii::app()->db->createCommand()
            ->select('count(DISTINCT empt."IDEmpregadorTrabalhador") AS NR_TRAB')
            ->from(CLIENTE . '.Empregador e')
            ->join(CLIENTE . '.Empregador_Trabalhador empt', 'empt."IDEmpregador" = e."IDEmpregador"')
            ->join(
                CLIENTE . '.Trabalhador_Setor_Funcao tsf',
                'tsf."IDEmpregadorTrabalhador" = empt."IDEmpregadorTrabalhador"'
            )
            ->join(
                CLIENTE . '.Situacao_Trabalhador st',
                'st."IDTrabalhadorSetorFuncao" = tsf."IDTrabalhadorSetorFuncao"'
            )
            ->join('public.Tipo_Situacao tipo', 'tipo."IDTipoSituacao" = st."IDTipoSituacao"')
            ->join(CLIENTE . '.Trabalhador trab', 'trab."IDTrabalhador" = empt."IDTrabalhador"')
            ->where(
                '"tipo"."status_situacao" != 1 AND "e"."IDEmpregador" = :IDEmpregador',
                ['IDEmpregador' => $this->IDEmpregador]
            );
        return $consulta;
    }

    public function getNrTrabalhadoresByGeneroIdade($genero = true, $maior = true)
    {
        if ($maior) {
            $operator = '>';
        } else {
            $operator = '<';
        }

        $consulta = $this->getComandoNrRealTrabalhadores()->andWhere(
            'EXTRACT(year from AGE(NOW(), trab."dt_nascimentoTrabalhador" )) ' . $operator . ' ' . HData::MAIOR_IDADE
        );

        if ($genero) {
            $consulta->andWhere(
                'trab."genero_trabalhador"'
            );
        } else {
            $consulta->andWhere(
                'trab."genero_trabalhador" = false'
            );
        }

        $consulta = $consulta->queryRow();
        return $consulta['nr_trab'];
    }

    public function getNrTrabalhadoresDeficientes()
    {
        $consulta = $this->getComandoNrRealTrabalhadores()->andWhere('trab."eh_PNETrabalhador" = true');
        $consulta = $consulta->queryRow();
        return $consulta['nr_trab'];
    }

    public function getNrDeclaradoTrabalhadores()
    {
        $consulta = Yii::app()->db->createCommand()
            ->select('sum(sf."nr_declaradoTrabalhadores") AS NR_TRAB')
            ->from(CLIENTE . '.Setor_Funcao sf')
            ->join(CLIENTE . '.Setor s', 's."IDSetor" = sf."IDSetor"')
            ->where(
                'sf."ativo_setorFuncao" AND s."IDEmpregador" = :IDEmpregador',
                ['IDEmpregador' => $this->IDEmpregador]
            )
            ->queryRow();
        return $consulta['nr_trab'];
    }

    public function getVigenciaAtual()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t."dt_inicioVigencia" <= now() AND t."dt_fimVigencia" >= now()');
        $criteria->compare('t."IDEmpregador"', $this->IDEmpregador);
        return Vigencia::model()->find($criteria);
    }
}
