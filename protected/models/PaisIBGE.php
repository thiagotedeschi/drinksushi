<?php

/**
 * This is the model class for table "drink.Pais_ibge".
 *
 * The followings are the available columns in table 'drink.Pais_ibge':
 * @property integer $IDPais_ibge
 * @property string $nome_pais
 * @property string $codigo_paisIbge
 * @property string $sigla_paisIbge
 *
 * The followings are the available model relations:
 * @property Endereco[] $enderecos
 * @package base.Models
 */
class PaisIBGE extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Pais_ibge';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome_pais, codigo_paisIbge, sigla_paisIbge', 'required'),
            array('nome_pais', 'length', 'max' => 255),
            array('codigo_paisIbge', 'length', 'max' => 50),
            array('sigla_paisIbge', 'length', 'max' => 3),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'enderecos' => array(self::HAS_MANY, 'Endereco', 'IDPais_ibge'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDPais_ibge' => 'País',
            'nome_pais' => 'Nome Pais',
            'codigo_paisIbge' => 'Codigo Pais Ibge',
            'sigla_paisIbge' => 'Sigla Pais Ibge',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PaisIBGE the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
