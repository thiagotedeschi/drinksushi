<?php

/**
 * This is the model class for table "drink.Concessao_grupo".
 *
 * The followings are the available columns in table 'drink.Concessao_grupo':
 * @package base.Models
 * @property integer $IDConcessao_grupo
 * @property integer $IDGrupo
 * @property integer $IDUsuario
 * @property string $dt_inicio
 * @property string $dt_fim
 * @property integer $IDUsuario_concedeu
 *
 * The followings are the available model relations:
 * @property Grupo $iDGrupo
 * @property Usuario $iDUsuario
 */
class ConcessaoGrupo extends ActiveRecord
{
    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Concessao_grupo';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('IDGrupo, IDUsuario, dt_inicio, IDUsuario_concedeu', 'required'),
            array('IDGrupo, IDUsuario, IDUsuario_concedeu', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDGrupo' => array(self::BELONGS_TO, 'Grupo', 'IDGrupo'),
            'iDUsuario' => array(self::BELONGS_TO, 'Usuario', 'IDUsuario'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDConcessao_grupo' => 'Concessão para Grupo',
            'IDGrupo' => 'IDGrupo',
            'IDUsuario' => 'IDUsuario',
            'dt_inicio' => 'Inicio',
            'dt_fim' => 'Fim',
            'IDUsuario_concedeu' => 'IDUsuario que Concedeu',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ConcessaoGrupo the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
