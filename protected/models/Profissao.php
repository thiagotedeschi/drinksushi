<?php

/**
 * This is the model class for table "drink.Profissao".
 *
 * The followings are the available columns in table 'drink.Profissao':
 * @property integer $IDProfissao
 * @property string $nome_profissao
 * @property string $desc_profissao
 *
 * The followings are the available model relations:
 * @property Profissional[] $profissionals
 * @property ResponsabilidadeProfissao[] $responsabilidadeProfissaos
 * @package base.Models
 */
class Profissao extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Profissao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome_profissao, desc_profissao', 'required'),
            array('nome_profissao', 'length', 'max' => 50),
            array('desc_profissao', 'length', 'max' => 255),
            // The following rule is used by search().
            array('IDProfissao, nome_profissao, desc_profissao', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'profissionals' => array(self::HAS_MANY, 'Profissional', 'IDProfissao'),
            'responsabilidadeProfissaos' => array(
                self::MANY_MANY,
                'ResponsabilidadeProfissao',
                CLIENTE . '.Responsabilidade_Profissao(IDProfissao, IDResponsabilidade)'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDProfissao' => 'Profissão',
            'nome_profissao' => 'Nome Profissão',
            'desc_profissao' => 'Descrição Profissão',
            'responsabilidade' => 'Responsabilidades',
            'responsabilidadeProfissaos' => 'Responsabilidade da Profissão'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDProfissao"', HTexto::tiraLetras($this->IDProfissao));
        $criteria->compare('LOWER("nome_profissao")', mb_strtolower($this->nome_profissao), true);
        $criteria->compare('LOWER("desc_profissao")', mb_strtolower($this->desc_profissao), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDProfissao" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Profissao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function atualizaResponsabilidades($responsabilidades)
    {
        $iDResponsabilidade = Yii::app()->db->createCommand()
            ->select("IDResponsabilidade")
            ->from(CLIENTE . '.Responsabilidade_Profissao')
            ->where('"IDProfissao" = :IDProfissao', array(':IDProfissao' => $this->IDProfissao))
            ->queryColumn();

        //pega os ids das Responsabilidades que não estão mais na Profissão
        $excluirResponsabilidades = array_diff($iDResponsabilidade, $responsabilidades);
        //os ids a serem acresentados eliminandos os já existentes
        $addResponsabilidades = array_diff($responsabilidades, $iDResponsabilidade);

        foreach ($addResponsabilidades as $iDResponsabilidade) {
            $respTemProfissao = new ResponsabilidadeTemProfissao;
            $respTemProfissao->IDProfissao = $this->IDProfissao;
            $respTemProfissao->IDResponsabilidade = $iDResponsabilidade;
            $respTemProfissao->save();
        }

        foreach ($excluirResponsabilidades as $iDResponsabilidade) {
            $prestacaoTemServico = ResponsabilidadeTemProfissao::model()->findByAttributes(
                array('IDResponsabilidade' => $iDResponsabilidade, 'IDProfissao' => $this->IDProfissao)
            );
            $prestacaoTemServico->delete();
        }
    }

    public function viewResponsabilidades()
    {
        $this->responsabilidadeProfissaos;
        $retorno = '';
        foreach ($this->responsabilidadeProfissaos as $responsabilidadeProfissao) {
            $retorno .= "<ul class='margin-0'>- " . $responsabilidadeProfissao . ';</ul>';
        }
        return $retorno;
    }


    public function getLabelProfissao()
    {
        return $this->nome_profissao;
    }


    public function __toString()
    {
        return $this->getLabelProfissao();
    }
}