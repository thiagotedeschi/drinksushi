<?php

/**
* This is the model class for table "public.Bairro".
*
* The followings are the available columns in table 'public.Bairro':
    * @property integer $IDBairro
    * @property integer $IDCidade_ibge
    * @property string $nome_bairro
    *
    * The followings are the available model relations:
            * @property CidadeIbge $iDCidadeIbge
    * @package base.Models
*/
class Bairro extends ActiveRecord
{

/**
* Retorna o nome da tabela representada pelo Modelo.
*
* @return string nome da tabela
*/
public function tableName()
{
return 'public.Bairro';
}

/**
* Retorna as regras de validação para o Modelo
* @return Array Regras de Validação.
*/
public function rules()
{
return array(
    array('IDCidade_ibge', 'required'),
    array('IDCidade_ibge', 'numerical', 'integerOnly'=>true),
    array('nome_bairro', 'length', 'max'=>255),
// @todo Please remove those attributes that should not be searched.
array('IDBairro, IDCidade_ibge, nome_bairro', 'safe', 'on'=>'search'),
);
}

/**
* Retorna as relações do modelo
* @return Array relações
*/
public function relations()
{
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
return array(
    'iDCidadeIbge' => array(self::BELONGS_TO, 'CidadeIbge', 'IDCidade_ibge'),
);
}

/**
* Retorna as labels dos atributos do modelo no formato (atributo=>label)
* @return Array labels dos atributos.
*/
public function attributeLabels()
{
return array(
    'IDBairro' => 'Idbairro',
    'IDCidade_ibge' => 'Idcidade Ibge',
    'nome_bairro' => 'Nome Bairro',
);
}

/**
* Retorna uma lista de modelos baseada nas definições de filtro da tabela
* @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
*/
public function search()
{
// @todo Please modify the following code to remove attributes that should not be searched.

$criteria=new CDbCriteria;

		$criteria->compare('"IDBairro"',HTexto::tiraLetras($this->IDBairro));
		$criteria->compare('"IDCidade_ibge"',$this->IDCidade_ibge);
		$criteria->compare('LOWER("nome_bairro")',mb_strtolower($this->nome_bairro),true);

return new CActiveDataProvider($this, array(
'criteria'=>$criteria,
'Pagination' => array(
'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
),
'sort' => array(
        'defaultOrder' => '"IDBairro" DESC',
    )));
}

/**
* Returns the static model of the specified AR class.
* Please note that you should have this exact method in all your CActiveRecord descendants!
* @param string $className active record class name.
* @return Bairro the static model class
*/
public static function model($className=__CLASS__)
{
return parent::model($className);
}
}
