<?php

/**
 * This is the model class for table "public.Dica".
 *
 * The followings are the available columns in table 'public.Dica':
 * @property integer $IDDica
 * @property string $titulo_dica
 * @property string $texto_dica
 * @property integer $IDTipo_dica
 * @property integer $IDAcao
 *
 * The followings are the available model relations:
 * @property TipoDica $iDTipoDica
 * @property Acao $iDAcao
 * @package base.Models
 */
class Dica extends ActiveRecord
{
    /**
     * nome da ação (utilizada principalmente na search)
     * @var $nome_acao
     */
    public $nome_acao;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Dica';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('titulo_dica, texto_dica', 'required'),
            array('IDTipo_dica, IDAcao', 'numerical', 'integerOnly' => true),
            array('titulo_dica', 'length', 'max' => 100),
            // The following rule is used by search().
            array('IDDica, titulo_dica, IDTipo_dica, IDAcao, nome_acao', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'TipoDica' => array(self::BELONGS_TO, 'TipoDica', 'IDTipo_dica'),
            'iDAcao' => array(self::BELONGS_TO, 'Acao', 'IDAcao'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDDica' => 'Dica',
            'titulo_dica' => 'Título da Dica',
            'texto_dica' => 'Texto da Dica',
            'IDTipo_dica' => 'Tipo de Dica',
            'IDAcao' => 'Ação Associada',
            'nome_acao' => 'Ação Associada',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->with = (['iDAcao']);

        $criteria->compare('"IDDica"', HTexto::tiraLetras($this->IDDica));
        $criteria->compare('LOWER("titulo_dica")', mb_strtolower($this->titulo_dica), true);
        $criteria->compare('"IDTipo_dica"', $this->IDTipo_dica);
        $criteria->compare('LOWER("iDAcao"."nome_acao")', mb_strtolower($this->nome_acao), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDDica" DESC',
                'attributes' => array(
                    'nome_acao' => array(
                        'asc' => '"iDAcao"."nome_acao"',
                        'desc' => '"iDAcao"."nome_acao" DESC',
                    ),
                    '*',
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Dica the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
