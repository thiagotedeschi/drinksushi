<?php
/**
 * @package base.Models
 */
class FMudarSenha extends CFormModel
{

    public $currentPassword;
    public $newPassword;
    public $newPassword_repeat;
    private $_user;


    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array(
                'currentPassword',
                'compareCurrentPassword'
            ),
            array(
                'newPassword',
                'compareLogin'
            ),
            array(
                'currentPassword, newPassword, newPassword_repeat',
                'required',
                //'message'=>'Introduzca su {attribute}.',
            ),
            array('newPassword', 'length', 'min' => 7, 'max' => 60),
            array(
                'newPassword_repeat',
                'compare',
                'compareAttribute' => 'newPassword',
                'message' => 'As senhas não coincidem.',
            ),
            array(
                'newPassword',
                'compare',
                'compareAttribute' => 'currentPassword',
                'operator' => '!=',
                'message' => 'A nova senha não pode ser igual a antiga',
            ),
        );
    }

    public function compareCurrentPassword($attribute, $params)
    {
        $user = Usuario::model()->FindBySql(
            'SELECT * FROM "' . CLIENTE . '"."Usuario" WHERE crypt(\'' . $this->currentPassword . '\', senha_usuario) = senha_usuario '
        );
        if ($user === null) {
            $this->addError($attribute, 'Senha Incorreta');
        }
    }

    public function verificaPassword($attribute, $params)
    {
        $user = Usuario::model()->FindBySql(
            'SELECT * FROM "' . CLIENTE . '"."Usuario" WHERE crypt(\'' . $this->currentPassword . '\', senha_usuario) = senha_usuario '
        );
        if ($user === null) {
            $this->addError($attribute, 'Senha Incorreta');
        }
    }


    public function compareLogin($attribute, $params)
    {
        $login = Yii::app()->user->getState('login_usuario');
        if (is_int(strpos($this->newPassword, $login))) {
            $this->addError($attribute, 'A senha não pode conter o Login do Usuário');
        }
    }

    public function init()
    {
        $this->_user = Usuario::model()->findByAttributes(array('IDUsuario' => Yii::app()->User->IDUsuario));
    }

    public function attributeLabels()
    {
        return array(
            'currentPassword' => 'Senha Atual',
            'newPassword' => 'Nova Senha',
            'newPassword_repeat' => 'Confirme a senha',
        );
    }

    public function changePassword()
    {
        if ($this->_user->foto_usuario) {
            $this->_user->foto_usuario = stream_get_contents($this->_user->foto_usuario);
        }
        $this->_user->senha_usuario = new CDbExpression("crypt('" . $this->newPassword . "', gen_salt('md5'))");
        $this->_user->dt_ultimaTrocaSenhaUsuario = new CDbExpression('CURRENT_TIMESTAMP(0)');
        $this->_user->save();
        Yii::app()->user->logout();
    }

}