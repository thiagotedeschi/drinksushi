<?php

/**
 * This is the model class for table "drink.Permissao".
 *
 * The followings are the available columns in table 'drink.Permissao':
 * @property integer $IDPermissao
 * @property integer $IDGrupo
 * @property integer $IDAcao
 * @property string $dt_associacaoPermissao
 * @property integer $IDUsuario_concedeu
 * @property string $dt_fimPermissao
 * @property string $dt_inicioPermissao
 *
 * The followings are the available model relations:
 * @property Grupo $iDGrupo
 * @property Usuario $iDUsuarioConcedeu
 * @property Acao $iDAcao
 * @package base.Models
 */
class Permissao extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Permissao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('IDGrupo, IDAcao, dt_associacaoPermissao, IDUsuario_concedeu', 'required'),
            array('IDGrupo, IDAcao, IDUsuario_concedeu', 'numerical', 'integerOnly' => true),
            array('dt_fimPermissao, dt_inicioPermissao', 'safe'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDGrupo' => array(self::BELONGS_TO, 'Grupo', 'IDGrupo'),
            'iDUsuarioConcedeu' => array(self::BELONGS_TO, 'Usuario', 'IDUsuario_concedeu'),
            'iDAcao' => array(self::BELONGS_TO, 'Acao', 'IDAcao'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDPermissao' => 'Permissão',
            'IDGrupo' => 'Idgrupo',
            'IDAcao' => 'Idacao',
            'dt_associacaoPermissao' => 'Dt Associacao Permissao',
            'IDUsuario_concedeu' => 'Idusuario Concedeu',
            'dt_fimPermissao' => 'Dt Fim Permissao',
            'dt_inicioPermissao' => 'Dt Inicio Permissao',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Permissao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
