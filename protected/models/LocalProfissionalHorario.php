<?php

/**
 * This is the model class for table "drink.Local_Profissional_Horario".
 *
 * The followings are the available columns in table 'drink.Local_Profissional_Horario':
 * @property integer $IDLocalProfissional
 * @property integer $IDProfissional
 * @property integer $IDLocal
 * @property integer $IDHorario
 * @property string $dt_inicio
 * @property string $dt_fim
 * @property string $dt_exclusao
 * @property boolean $folga
 * The followings are the available model relations:
 * @property Local $iDLocal
 * @property Horario $iDHorario
 * @property Profissional $iDProfissional
 * @package base.Models
 */
class LocalProfissionalHorario extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Local_Profissional_Horario';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDProfissional, IDLocal, dt_inicio', 'required'),
            array('IDProfissional, IDLocal, IDHorario', 'numerical', 'integerOnly' => true),
            array('dt_fim, dt_exclusao, folga', 'safe'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDLocal' => array(self::BELONGS_TO, 'Local', 'IDLocal'),
            'iDHorario' => array(self::BELONGS_TO, 'Horario', 'IDHorario'),
            'iDProfissional' => array(self::BELONGS_TO, 'Profissional', 'IDProfissional'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDLocalProfissional' => 'Local de Trabalho',
            'IDProfissional' => 'Profissional',
            'IDLocal' => 'Local',
            'IDHorario' => 'Horário',
            'dt_inicio' => 'Data de Início',
            'dt_fim' => 'Data de Término',
            'dt_exclusao' => 'Data Exclusão',
            'folga' => 'Folga',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LocalProfissionalHorario the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function labelModel()
    {
        return 'Local/Horário do Profissional';
    }

}
