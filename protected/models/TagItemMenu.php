<?php

/**
 * This is the model class for table "Tag_Item_Menu".
 *
 * The followings are the available columns in table 'Tag_Item_Menu':
 * @property integer $IDTagItemMenu
 * @property integer $IDItemMenu
 * @property string $valor_tagItemMenu
 *
 * The followings are the available model relations:
 * @property ItemMenu $iDItemMenu
 * @package base.Models
 */
class TagItemMenu extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'Tag_Item_Menu';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDItemMenu, valor_tagItemMenu', 'required'),
            array('IDItemMenu', 'numerical', 'integerOnly' => true),
            array('valor_tagItemMenu', 'length', 'max' => 25),
// @todo Please remove those attributes that should not be searched.
            array('IDTagItemMenu, IDItemMenu, valor_tagItemMenu', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDItemMenu' => array(self::BELONGS_TO, 'ItemMenu', 'IDItemMenu'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTagItemMenu' => 'Tag Item Menu',
            'IDItemMenu' => 'Item Menu',
            'valor_tagItemMenu' => 'Tag',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('"IDTagItemMenu"', HTexto::tiraLetras($this->IDTagItemMenu));
        $criteria->compare('"IDItemMenu"', $this->IDItemMenu);
        $criteria->compare('LOWER("valor_tagItemMenu")', mb_strtolower($this->valor_tagItemMenu), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDTagItemMenu" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TagItemMenu the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function __toString()
    {
        return $this->valor_tagItemMenu;
    }
}
