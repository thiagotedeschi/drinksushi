<?php

/**
 * Formulário para o usuário que esqeuceu sua senha poder pedir outra antes de logar no sistema
 * @package base.Models
 */
class FEsqueciMinhaSenha extends CFormModel
{

    public $emailUsuario;
    public $captcha;
    public $message;

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('emailUsuario', 'required', 'message' => 'Email não pode ser vazio'),
            array('emailUsuario', 'existeUsarioEmail'),
            array('emailUsuario', 'email'),
            array('captcha', 'captcha'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'emailUsuario' => 'Confirme Seu Email',
            'captcha' => 'Confirme que você não é um robô',
        );
    }


    public function changePassword($user)
    {
        $senha = HPassword::gerarPassword();
        $user->scenario = 'esqueciSenha';
        $user->senha_usuario = new CDbExpression("crypt('" . $senha . "', gen_salt('md5'))");
        $user->dt_ultimaTrocaSenhaUsuario = new CDbExpression('CURRENT_TIMESTAMP(0)');
        if ($user->save()) {
            return $senha;
        }
    }

    public function existeUsarioEmail($attribute, $params)
    {
        $profissional = Profissional::model()->findByAttributes(array('email_profissional' => $this->$attribute));
        if ($profissional == null) {
            $this->addError($attribute, 'Este email não está cadastrado no sistema');
        }
    }

}
