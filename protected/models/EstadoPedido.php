<?php

/**
 * This is the model class for table "drink.Estado_Pedido".
 *
 * The followings are the available columns in table 'drink.Estado_Pedido':
 * @property integer $IDEstadoPedido
 * @property string $tipo_estado
 *
 * The followings are the available model relations:
 * @property PedidoPDV[] $pedidoPDVs
 * @package base.Models
 */
class EstadoPedido extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Estado_Pedido';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('tipo_estado', 'length', 'max'=>100),
// @todo Please remove those attributes that should not be searched.
            array('IDEstadoPedido, tipo_estado', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'pedidoPDVs' => array(self::HAS_MANY, 'PedidoPDV', 'IDEstadoPedido'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDEstadoPedido' => 'Idestado Pedido',
            'tipo_estado' => 'Tipo Estado',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDEstadoPedido"',HTexto::tiraLetras($this->IDEstadoPedido));
        $criteria->compare('LOWER("tipo_estado")',mb_strtolower($this->tipo_estado),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDEstadoPedido" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EstadoPedido the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /*
    *@return o nome do estado
    *@author Thiago Tedeschi <thiagotedeschi@hotmail.com>
    *@since 1.0 03/06/2015
   */
    public function getLabelEstado()
    {
        return $this->tipo_estado;
    }

    /*
     *@return Retorna a função getLabelEstado
     *@author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     *@since 1.0 03/06/2015
    */
    public function __toString()
    {
        return $this->getLabelEstado();
    }
}