<?php

/**
 * This is the model class for table "drink.Trabalhador".
 *
 * The followings are the available columns in table 'drink.Trabalhador':
 * @property string $IDTrabalhador
 * @property string $nome_trabalhador
 * @property string $dt_nascimentoTrabalhador
 * @property boolean $genero_trabalhador
 * @property string $cpf_trabalhador
 * @property string $ctps_trabalhador
 * @property string $n_serieCtpsTrabalhador
 * @property string $rg_trabalhador
 * @property string $orgao_expedidorRgTrabalhador
 * @property integer $uf_rgTrabalhador
 * @property string $nome_maeTrabalhador
 * @property string $nome_paiTrabalhador
 * @property string $nit_trabalhador
 * @property string $resp_legalTrabalhador
 * @property string $contato_respLegalTrabalhador
 * @property string $ddd_respLegalTrabalhador
 * @property string $telefone_trabalhador
 * @property string $ddd_telefoneTrabalhador
 * @property string $email_trabalhador
 * @property integer $IDEndereco
 * @property integer $IDRacaCor
 * @property string $foto_trabalhador
 * @property string $eh_PNETrabalhador
 *
 * The followings are the available model relations:
 * @property TrabalhadorCondicao[] $trabalhadorCondicaos
 * @property RacaCor $iDRacaCor
 * @property Endereco $iDEndereco
 * @property Estado $ufRgTrabalhador
 * @property EmpregadorTrabalhador[] $empregadorTrabalhadors
 * @package base.Models
 */
class Trabalhador extends ActiveRecord
{
    /*
     * @var $situacaoTrabalhador
     * */
    public $situacaoTrabalhador;

    /*
     * @var $ddd_respLegalTrabalhador
     * */
    public $ddd_respLegalTrabalhador;

    /*
     * @var $IDEmpregadorTrabalhador
     * */
    public $IDEmpregadorTrabalhador;

    /* Setor do Trabalhador (usado principalmente na search)
     * @var $buscar_setor
     * */
    public $buscar_setor;

    /* Função do Trabalhaador (usado principalmente na search)
     * @var $buscar_funcao
     * */
    public $buscar_funcao;

    /* Função do Trabalhaador (usado principalmente na search)
     * @var $setorFuncao
     * */

    public $setorFuncao;

    /* Função do Trabalhaador (usado para selecionar o documento obrigatório)
     * @var $tipoDocObrigatorio
     * */
    public $tipoDocObrigatorio;

    /**
     * @var int $IDEmpregador Usada apenas na pesquisa. Traz o ID do Empregador a ser pesquisado
     */

    CONST FOTO_X = 354;
    CONST FOTO_Y = 472;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Trabalhador';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('orgao_expedidorRgTrabalhador', 'verificaOrgaoExp'),
            array('tipoDocObrigatorio', 'verificaCTPSObrigatorio'),
            array('n_serieCtpsTrabalhador', 'verificaNSerieCtps'),
            array('resp_legalTrabalhador, contato_respLegalTrabalhador, ddd_respLegalTrabalhador', 'verificaIdade'),
            array('dt_nascimentoTrabalhador', 'date', 'format' => 'dd/MM/yyyy'),
            array('nome_trabalhador, dt_nascimentoTrabalhador, cpf_trabalhador, tipoDocObrigatorio', 'required'),
            array('uf_rgTrabalhador, IDEndereco', 'numerical', 'integerOnly' => true),
            array(
                'nome_trabalhador, nome_maeTrabalhador, nome_paiTrabalhador, resp_legalTrabalhador',
                'length',
                'max' => 100
            ),
            array(
                'cpf_trabalhador',
                'filter',
                'filter' => function ($v) {
                        return HTexto::somenteNumeros($v);
                    }
            ),
            array('cpf_trabalhador', 'unique'),
            array('cpf_trabalhador', 'application.components.validators.ValidadorDocumento'),
            array('ctps_trabalhador, contato_respLegalTrabalhador, telefone_trabalhador', 'length', 'max' => 20),
            array('ddd_respLegalTrabalhador', 'length', 'max' => 5),
            array('n_serieCtpsTrabalhador, orgao_expedidorRgTrabalhador', 'length', 'max' => 10),
            array('rg_trabalhador', 'length', 'max' => 12),
            array('ddd_telefoneTrabalhador', 'length', 'max' => 5),
            array('nit_trabalhador', 'length', 'max' => 15),
            array('email_trabalhador', 'length', 'max' => 60),
            array(
                'genero_trabalhador, foto_trabalhador, IDRacaCor, eh_PNETrabalhador, IDTipoCondicao, dt_inicioCondicao',
                'safe'
            ),
            array(
                'foto_trabalhador',
                'file',
                'types' => 'png',
                'maxSize' => 1024 * 1024 * 0.4, // 400kb
                'allowEmpty' => true,
                'skipOnError' => true,
            ),
// The following rule is used by search().
            array(
                'nome_trabalhador, dt_nascimentoTrabalhador, cpf_trabalhador, setorFuncao',
                'safe',
                'on' => 'search'
            ),
        );
    }

    public function verificaCTPS($attribute, $params)
    {
        if ($this->$attribute == '') {
            if ($this->ctps_trabalhador == '') {
                $this->addError($attribute, 'Você precisa preencher o RG ou o CTPS');
            }
        }
    }

    public function verificaRG($attribute, $params)
    {
        if ($this->$attribute == '') {
            if ($this->rg_trabalhador == '' || $this->orgao_expedidorRgTrabalhador == '') {
                $this->addError($attribute, 'Você precisa preencher o RG ou o CTPS');
            }
        }
    }


    public function verificaOrgaoExp($attribute, $params)
    {
        if ($this->rg_trabalhador != '' && $this->$attribute == '') {
            $this->addError($attribute, "Ao utilizar o RG, o Orgão Expedidor não pode estar vazio");
        }
    }

    public function verificaNSerieCtps($attribute, $params)
    {
        if ($this->ctps_trabalhador != '' && $this->$attribute == '') {
            $this->addError($attribute, "Ao utilizar o CTPS, o N° de Série não pode estar vazio");
        }
    }


    public function verificaIdade($attribute, $params)
    {
        if (!HData::ehMaiorDeIdade($this->dt_nascimentoTrabalhador, HData::BR_DATE_FORMAT)) {
            if (empty($this->$attribute)) {
                $this->addError($attribute, "Informação Obrigatória para Trabalhador Menor de Idade");
            }
        }
    }

    public function verificaTipoDocObrigatorio($attribute, $params)
    {
        if (empty($this->$attribute)) {
            $this->addError($attribute, 'Você deve escolher no mínimo um documento a ser preenchido');
        }
    }

    public function verificaCTPSObrigatorio($attribute, $params)
    {
        if (!empty($this->tipoDocObrigatorio)) {
            if (in_array("ctps", $this->tipoDocObrigatorio) && in_array("rg", $this->tipoDocObrigatorio)) {
                if ($this->ctps_trabalhador == '' || $this->rg_trabalhador == '') {
                    $this->addError($attribute, 'Você deve preencher os dados do RG e da CTPS');
                }
            } else {
                if (in_array("rg", $this->tipoDocObrigatorio)) {
                    if ($this->rg_trabalhador == '') {
                        $this->addError($attribute, 'Você deve preencher os dados do RG');
                    }
                } else {
                    if (in_array("ctps", $this->tipoDocObrigatorio)) {
                        if ($this->ctps_trabalhador == '') {
                            $this->addError($attribute, 'Você deve preencher os dados da CTPS');
                        }
                    }
                }
            }
        } else {
            $this->addError($attribute, 'Você deve selecionar e preencher no mínimo um documento obrigatório.');
        }
    }

    public function getIdade()
    {
        return HData::getIdade($this->dt_nascimentoTrabalhador);
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'trabalhadorCondicaos' => array(self::HAS_MANY, 'TrabalhadorCondicao', 'IDTrabalhador'),
            'ufRgTrabalhador' => array(self::BELONGS_TO, 'Estado', 'uf_rgTrabalhador'),
            'iDEndereco' => array(self::BELONGS_TO, 'EnderecoTrabalhador', 'IDEndereco'),
            'iDRacaCor' => array(self::BELONGS_TO, 'RacaCor', 'IDRacaCor'),
            'empregadorTrabalhadors' => array(self::HAS_MANY, 'EmpregadorTrabalhador', 'IDTrabalhador'),
            'iDEmpregadorTrabalhador' => array(self::BELONGS_TO, 'EmpregadorTrabalhador', 'IDEmpregadorTrabalhador'),

        );
    }

    /**
     * Retorna a declaração de escopos nomeados.
     * Um escopo nomeado representa um critério de consulta que podem ser conectadas
     * com outros escopos nomeados e aplicados a uma consulta.
     * @return Array regras de escopo
     */
    public function scopes()
    {
        return array(
            'empregadorTrabalhadors' => array(
                'order' => '"empregadorTrabalhadors"."dt_cadastro" DESC',
                'limit' => '1'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTrabalhador' => 'Trabalhador',
            'nome_trabalhador' => 'Nome do Trabalhador',
            'dt_nascimentoTrabalhador' => 'Data de Nascimento do Trabalhador',
            'genero_trabalhador' => 'Gênero do Trabalhador',
            'cpf_trabalhador' => 'CPF do Trabalhador',
            'ctps_trabalhador' => 'CTPS do Trabalhador',
            'n_serieCtpsTrabalhador' => 'N° Série do CTPS',
            'rg_trabalhador' => 'RG do Trabalhador',
            'orgao_expedidorRgTrabalhador' => 'Orgão Expedidor',
            'uf_rgTrabalhador' => 'UF do RG',
            'nome_maeTrabalhador' => 'Nome da Mãe do Trabalhador',
            'nome_paiTrabalhador' => 'Nome do Pai do Trabalhador',
            'nit_trabalhador' => 'NIT do Trabalhador',
            'resp_legalTrabalhador' => 'Responsável Legal do Trabalhador',
            'contato_respLegalTrabalhador' => 'Contato do Responsável Legal',
            'ddd_respLegalTrabalhador' => 'DDD Contato Responsável',
            'telefone_trabalhador' => 'Telefone do Trabalhador',
            'email_trabalhador' => 'Email do Trabalhador',
            'IDEndereco' => 'Endereço',
            'foto_trabalhador' => 'Foto do Trabalhador',
            'IDRacaCor' => 'Raça/Cor',
            'eh_PNETrabalhador' => 'Trabalhador PNE?',
            'situacaoTrabalhador' => 'Situação',
            'buscar_setor' => 'Setor',
            'buscar_funcao' => 'Função',
            'tipoDocObrigatorio' => 'Selecione o documento a ser preenchido',
            'setorFuncao' => 'Setor/Função',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 'DISTINCT t.*';
        $criteria->join = 'LEFT OUTER JOIN ' . CLIENTE . '."Empregador_Trabalhador" "empregadorTrabalhadors" ON "empregadorTrabalhadors"."IDTrabalhador" = "t"."IDTrabalhador"';

        $criteria->compare('t."IDTrabalhador"', HTexto::tiraLetras($this->IDTrabalhador));
        $criteria->compare('LOWER("nome_trabalhador")', mb_strtolower($this->nome_trabalhador), true);

        $countCriteria = '';
        if (!is_null(Yii::app()->user->getState('IDEmpregador'))) {
            $criteria->compare('"empregadorTrabalhadors"."IDEmpregador"', Yii::app()->user->getState('IDEmpregador'));
            $countCriteria = '"empregadorTrabalhadors"."IDEmpregador" = ' . Yii::app()->user->getState('IDEmpregador');
        } else {
            $criteria->addInCondition(
                '"empregadorTrabalhadors"."IDEmpregador"',
                array_keys(Yii::app()->session['empregadores'])
            );
            $criteria->addCondition('"empregadorTrabalhadors"."IDEmpregador" IS NULL', 'OR');
        }
        $criteria->compare('"genero_trabalhador"', $this->genero_trabalhador);


        if (!empty($this->cpf_trabalhador)) {
            $criteria->compare('"cpf_trabalhador"', HTexto::somenteNumeros($this->cpf_trabalhador), true);
        }

        if (!empty($this->dt_nascimentoTrabalhador)) {
            $criteria->compare('"dt_nascimentoTrabalhador"', HData::brToSql($this->dt_nascimentoTrabalhador));
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'countCriteria' => array(
                'join' => 'LEFT OUTER JOIN ' . CLIENTE . '."Empregador_Trabalhador" "empregadorTrabalhadors" ON "empregadorTrabalhadors"."IDTrabalhador" = "t"."IDTrabalhador"',
                'condition' => $countCriteria,
                'select' => 'DISTINCT "t".*',
                'group' => '"t"."IDTrabalhador"'
            ),
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"t"."IDTrabalhador" DESC',
                'attributes' => [
                    'IDTrabalhador' => [
                        'asc' => '"t"."IDTrabalhador"',
                        'desc' => '"t"."IDTrabalhador" DESC'
                    ],
                    'nome_trabalhador' => [
                        'asc' => '"t"."nome_trabalhador"',
                        'desc' => '"t"."nome_trabalhador" DESC'
                    ],
                    'dt_nascimentoTrabalhador' => [
                        'asc' => '"t"."dt_nascimentoTrabalhador"',
                        'desc' => '"t"."dt_nascimentoTrabalhador" DESC'
                    ],
                ]
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Trabalhador the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function beforeSave()
    {
        $this->cpf_trabalhador = str_replace(array('.', '-'), '', $this->cpf_trabalhador);
        $this->preparaFoto();
        return parent::beforeSave();
    }

    public function preparaFoto()
    {
        Yii::import('ext.upload.Upload');
        if (!isset($_FILES['Trabalhador'])) {
            $this->foto_trabalhador = (stream_get_contents($this->foto_trabalhador));
            return;
        }
        $file = HFile::organizaFile($_FILES['Trabalhador']);
        $imagem = new Upload($file);
        if ($file['tmp_name'] == '' || !is_uploaded_file($file['tmp_name'])) {
            if ($this->foto_trabalhador == '') {
                return;
            } else {
                $this->foto_trabalhador = (stream_get_contents($this->foto_trabalhador));
            }
        } else {
            $imagem->image_ratio_x = self::FOTO_X;
            $imagem->image_ratio_y = self::FOTO_Y;
            $imagem->image_resize = true;
            $this->foto_trabalhador = base64_encode($imagem->process());
        }
    }

    public function getGenero()
    {
        return ($this->genero_trabalhador) ? "Masculino" : "Feminino";
    }

    public function getLabelTrabalhador()
    {
        return $this->nome_trabalhador;
    }

    public function __toString()
    {
        return $this->getLabelTrabalhador();
    }

    public function atualizaCondicoes($IDTipoCondicao)
    {

        $condicoesAntigas = TrabalhadorCondicao::model()->findCondicaoByTrabalhador($this->IDTrabalhador)->findAll();
        $condicoesAntigas = array_keys(Html::listData($condicoesAntigas, 'IDTrabalhadorCondicao', ''));

        //Pega os resultados obtidos por post
        if ($IDTipoCondicao) {
            $condicoesNovas = array_keys($IDTipoCondicao);
        } else {
            $condicoesNovas = array();
        }

        //Calcula os resultados a serem excluídos
        $excluirCondicoes = array_diff($condicoesAntigas, $condicoesNovas);
        //Calcula os resultados a serem incluídos
        $addCondicoes = array_diff($condicoesNovas, $condicoesAntigas);

        //Inclui novos resultados
        if (!empty($addCondicoes)) {

            foreach ($addCondicoes as $IDCondicao) {
                if ($IDTipoCondicao) {
                    $trabalhadorCondicaos = new TrabalhadorCondicao();
                    $trabalhadorCondicaos->IDTrabalhador = $this->IDTrabalhador;
                    $trabalhadorCondicaos->IDTipoCondicao = $IDTipoCondicao[$IDCondicao];
                    $trabalhadorCondicaos->dt_inicioCondicao = HData::brToSql(
                        $IDTipoCondicao[$trabalhadorCondicaos->IDTipoCondicao]
                    );
                    $trabalhadorCondicaos->save();
                }
            }

        }

        //Excluir resultados removidos
        foreach ($excluirCondicoes as $delCondicao) {
            $r = TrabalhadorCondicao::model()->findCondicaoByTrabalhador($this->IDTrabalhador)->findByAttributes(
                array('IDTrabalhadorCondicao' => $delCondicao)
            );
            if ($r != null) {
                $r->delete();
            }
        }

    }

    public function findAllByStatusSituacao($IDEmpregadorTrabalhador = null)
    {
        $criteria = new CDbCriteria;
        $criteria->join = ' INNER JOIN "Empregador_Trabalhador" AS et ';
        $criteria->join .= ' ON "et"."IDTrabalhador" = "t"."IDTrabalhador"';
        $criteria->join = ' INNER JOIN "Trabalhador_SetorFuncao" AS ts ';
        $criteria->join .= ' ON "ts"."IDEmpregadorTrabalhador" = "et"."IDEmpregadorTrabalhador"';
        $criteria->join = ' INNER JOIN "Trabalhador_SetorFuncao" AS ts ';
        $criteria->join .= ' ON "ts"."IDEmpregadorTrabalhador" = "et"."IDEmpregadorTrabalhador"';
        $criteria->join = ' INNER JOIN "Trabalhador_SetorFuncao" AS ts ';
        $criteria->join .= ' ON "ts"."IDEmpregadorTrabalhador" = "et"."IDEmpregadorTrabalhador"';
        $criteria->compare('et."IDEmpregadorTrabalhador"', $IDEmpregadorTrabalhador);
        $criteria->addInCondition('et."IDEmpregadorTrabalhador"', $IDEmpregadorTrabalhador);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function getLabelEmpregadors()
    {
        if (count($this->empregadorTrabalhadors) == 1) {
            return $this->empregadorTrabalhadors[0]->iDEmpregador;
        } else {
            return Html::activeList($this, 'empregadorTrabalhadors', 'iDEmpregador');
        }
    }

    public function afterFind()
    {
        if (!empty($this->rg_trabalhador)) {
            $this->tipoDocObrigatorio[] = 'rg';
        }

        if (!empty($this->ctps_trabalhador)) {
            $this->tipoDocObrigatorio[] = 'ctps';
        }
        return parent::afterFind();
    }

    public function findAllSemVinculo($IDEmpregador = null)
    {
        $criteria = new CDbCriteria;
        $criteria->join = 'LEFT OUTER JOIN ' . CLIENTE . '."Empregador_Trabalhador" "empTrab" ON "empTrab"."IDTrabalhador" = "t"."IDTrabalhador"';
        if (!is_null($IDEmpregador)) {
            if (is_array($IDEmpregador)) {
                $criteria->addInCondition('"empTrab"."IDEmpregador"', $IDEmpregador, 'OR');
            } else {
                $criteria->addCondition('"empTrab"."IDEmpregador" != ' . $IDEmpregador, 'OR');
            }
        }
        $criteria->addCondition('"empTrab"."IDEmpregador" IS NULL', 'OR');
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
}
