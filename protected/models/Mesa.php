<?php

/**
* This is the model class for table "drink.Mesa".
*
* The followings are the available columns in table 'drink.Mesa':
    * @property integer $IDMesa
    * @property string $numero_mesa
    * @property string $localizacao_mesa
    * @property integer $IDEmpregador
    *
    * The followings are the available model relations:
            * @property Empregador $iDEmpregador
    * @package base.Models
*/
class Mesa extends ActiveRecord
{

/**
* Retorna o nome da tabela representada pelo Modelo.
*
* @return string nome da tabela
*/
public function tableName()
{
return CLIENTE.'.Mesa';
}

/**
* Retorna as regras de validação para o Modelo
* @return Array Regras de Validação.
*/
public function rules()
{
return array(
    array('numero_mesa, IDEmpregador', 'required'),
    array('IDEmpregador', 'numerical', 'integerOnly'=>true),
    array('numero_mesa', 'length', 'max'=>50),
    array('localizacao_mesa', 'length', 'max'=>255),
// @todo Please remove those attributes that should not be searched.
array('numero_mesa, localizacao_mesa, IDEmpregador', 'safe', 'on'=>'search'),
);
}

/**
* Retorna as relações do modelo
* @return Array relações
*/
public function relations()
{
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
return array(
    'iDEmpregador' => array(self::BELONGS_TO, 'Empregador', 'IDEmpregador'),
);
}

/**
* Retorna as labels dos atributos do modelo no formato (atributo=>label)
* @return Array labels dos atributos.
*/
public function attributeLabels()
{
return array(
    'IDMesa' => 'Mesa',
    'numero_mesa' => 'Número da Mesa',
    'localizacao_mesa' => 'Localização da Mesa',
    'IDEmpregador' => 'Restaurante',
);
}

/**
* Retorna uma lista de modelos baseada nas definições de filtro da tabela
* @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
*/
public function search()
{
// @todo Please modify the following code to remove attributes that should not be searched.

$criteria=new CDbCriteria;

		$criteria->compare('"IDMesa"',HTexto::tiraLetras($this->IDMesa));
		$criteria->compare('LOWER("numero_mesa")',mb_strtolower($this->numero_mesa),true);
		$criteria->compare('LOWER("localizacao_mesa")',mb_strtolower($this->localizacao_mesa),true);
		$criteria->compare('"IDEmpregador"',$this->IDEmpregador);

return new CActiveDataProvider($this, array(
'criteria'=>$criteria,
'Pagination' => array(
'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
),
'sort' => array(
        'defaultOrder' => '"IDMesa" DESC',
    )));
}

/**
* Returns the static model of the specified AR class.
* Please note that you should have this exact method in all your CActiveRecord descendants!
* @param string $className active record class name.
* @return Mesa the static model class
*/
public static function model($className=__CLASS__)
{
return parent::model($className);
}
}
