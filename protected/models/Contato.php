<?php

/**
 * This is the model class for table "drink.Contato".
 *
 * The followings are the available columns in table 'drink.Contato':
 * @property integer $IDContato
 * @property string $nome_contato
 * @property string $principal_contato
 * @property integer $IDEmpregador
 *
 * The followings are the available model relations:
 * @property TelefoneContato[] $telefoneContatos
 * @property Empregador $iDEmpregador
 * @property EmailContato[] $emailContatos
 * @property ResponsabilidadeContato[] drink.ResponsabilidadeContatos
 * @package base.Models
 */
class Contato extends ActiveRecord
{
    /*
        * nome da Empresa Responsavel , utilizado principalmente na search
        * @var $nome_empregador
        * */
    public $email_contato;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Contato';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome_contato, principal_contato, IDEmpregador', 'required'),
            array('IDEmpregador', 'numerical', 'integerOnly' => true),
            array('nome_contato', 'length', 'max' => 255),
            array('principal_contato', 'length', 'max' => 1),
            // The following rule is used by search().
            array('IDContato, nome_contato, principal_contato, IDEmpregador, email_contato', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'telefoneContatos' => array(self::HAS_MANY, 'TelefoneContato', 'IDContato'),
            'iDEmpregador' => array(self::BELONGS_TO, 'Empregador', 'IDEmpregador'),
            'emailContatos' => array(self::HAS_MANY, 'EmailContato', 'IDContato'),
            'ResponsabilidadeContatos' => array(
                self::MANY_MANY,
                'ResponsabilidadeContato',
                CLIENTE . '.Contatos_tem_responsabilidades(IDContato_Contato, IDResponsabilidade_contato)'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDContato' => 'Contato',
            'nome_contato' => 'Nome',
            'principal_contato' => 'Contato Principal deste Empregador?',
            'IDEmpregador' => 'Empregador',
            'email_contato' => 'Email Principal'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->select = 'DISTINCT "t".*, "ec".*';
        $criteria->join = 'LEFT OUTER JOIN "' . CLIENTE . '"."Email_contato" "ec" ON ("ec"."IDContato" = "t"."IDContato")';
        // Como filtrar os resultados pro empregador selecionado.
        $emp = Yii::app()->user->getState('IDEmpregador');
        if ($emp == null) {
            $criteria->addInCondition('"IDEmpregador"', array_keys(Yii::app()->session['empregadores']));
        } else {
            $criteria->compare('"IDEmpregador"', $emp);
        }

        $criteria->compare('"IDContato"', HTexto::tiraLetras($this->IDContato));
        $criteria->compare('LOWER("nome_contato")', mb_strtolower($this->nome_contato), true);
        $criteria->compare('LOWER("principal_contato")', mb_strtolower($this->principal_contato), true);
        if (!empty($this->email_contato)) {
            $criteria->compare('LOWER("ec"."email_contato")', mb_strtolower($this->email_contato), true);
            $criteria->addCondition('"ec"."principal_emailContato"');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"t"."IDContato" DESC',
                'attributes' => array(
                    'email_contato' => array(
                        'asc' => '"ec"."email_contato"',
                        'desc' => '"ec"."email_contato" DESC'
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Contato the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getEmailPrincipal()
    {
        $emailPrincipal = $this->emailContatos(array('condition' => '"principal_emailContato"', 'limit' => 1));
        return count($emailPrincipal) > 0 ? $emailPrincipal[0]->email_contato : '';
    }

    public function getEmailsSecundarios()
    {
        $emailsSecundarios = $this->emailContatos(array('condition' => 'NOT "principal_emailContato"'));
        $emails = array();
        foreach ($emailsSecundarios as $email) {
            $emails[] = $email->email_contato;
        }
        return $emails;
    }

    public function getTelefonePrincipal()
    {
        $telPrincipal = $this->telefoneContatos(array('condition' => '"principal_telefoneContato"', 'limit' => 1));
        return count($telPrincipal) > 0 ? $telPrincipal[0] : new TelefoneContato;
    }

    public function getTelefonesSecundarios()
    {
        $telsSecundarios = $this->telefoneContatos(array('condition' => 'NOT "principal_telefoneContato"'));
        return $telsSecundarios;
    }

    public function getLabelContato()
    {
        return $this->nome_contato . ' - ' . $this->getTelefonePrincipal();
    }

    public function  __toString()
    {
        return $this->getLabelContato();
    }

    public function findAllByTipoDocumento($IDTipoDocumento)
    {
        $criteria = new CDbCriteria();
        $criteria->distinct = true;
        $criteria->join = 'INNER JOIN ' . CLIENTE . '."Contatos_tem_responsabilidades" AS ctr ON ctr."IDContato_Contato" = "t"."IDContato"';
        $criteria->join .= ' INNER JOIN "Responsabilidade_contato" AS rc ON ctr."IDResponsabilidade_contato" = rc."IDResponsabilidade_contato"';
        $criteria->join .= ' INNER JOIN "Responsabilidade_Contato_Tipo_Documento" AS rctd ON rctd."IDResponsabilidadeContato" = rc."IDResponsabilidade_contato"';
        $criteria->compare('rctd."IDTipoDocumento"', $IDTipoDocumento);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function findAllByResponsabilidade($IDResponsabilidadeContato)
    {
        $criteria = new CDbCriteria();
        $criteria->distinct = true;
        $criteria->join = 'INNER JOIN ' . CLIENTE . '."Contatos_tem_responsabilidades" AS ctr ON ctr."IDContato_Contato" = "t"."IDContato"';
        $criteria->join .= ' INNER JOIN "Responsabilidade_contato" AS rc ON ctr."IDResponsabilidade_contato" = rc."IDResponsabilidade_contato"';
        $criteria->compare('rc."IDResponsabilidade_contato"', $IDResponsabilidadeContato);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }


}
