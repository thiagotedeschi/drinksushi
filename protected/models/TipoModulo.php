<?php

/**
 * This is the model class for table "public.Tipo_modulo".
 *
 * The followings are the available columns in table 'public.Tipo_modulo':
 * @property integer $IDTipo_modulo
 * @property string $nome_tipoModulo
 * @property integer $n_ordemTipoModulo
 * @property string $icone_tipoModulo
 * @property string $classe_tipoModulo
 *
 * The followings are the available model relations:
 * @property Modulo[] $modulos
 * @package base.Models
 */
class TipoModulo extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Tipo_modulo';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome_tipoModulo, n_ordemTipoModulo, icone_tipoModulo', 'required'),
            array('n_ordemTipoModulo', 'numerical', 'integerOnly' => true),
            array('nome_tipoModulo', 'length', 'max' => 255),
            array('icone_tipoModulo', 'length', 'max' => 20),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'modulos' => array(self::HAS_MANY, 'Modulo', 'IDTipo_modulo'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTipo_modulo' => 'Tipo de Modulo',
            'nome_tipoModulo' => 'Nome Tipo Modulo',
            'n_ordemTipoModulo' => 'N° Ordem Tipo Modulo',
            'icone_tipoModulo' => 'Ícone Tipo Modulo',
            'classe_tipoModulo' => 'Classe Tipo Modulo',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TipoModulo the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
