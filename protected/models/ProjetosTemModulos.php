<?php

/**
 * This is the model class for table "Projetos_tem_modulos".
 *
 * The followings are the available columns in table 'Projetos_tem_modulos':
 * @property integer $IDModulo
 * @property integer $IDProjeto
 * @property string $dt_insercaoModulo
 * @property string $dt_inicio
 * @property string $dt_fim
 * The followings are the available model relations:
 * @property Modulo $modulo
 * @property Projeto $projeto
 * @package base.Models
 */
class ProjetosTemModulos extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'Projetos_tem_modulos';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDProjeto', 'required'),
            array('IDProjeto', 'numerical', 'integerOnly' => true),
            array('dt_inicio, dt_fim', 'date', 'format' => 'dd/MM/yyyy'),
            array('dt_inicio, dt_fim', 'safe'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'modulo' => array(self::BELONGS_TO, 'Modulo', 'IDModulo'),
            'projeto' => array(self::BELONGS_TO, 'Projeto', 'IDProjeto')
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDModulo' => 'Módulo',
            'IDProjeto' => 'Projeto',
            'dt_insercaoModulo' => 'Data Inserção Modulo',
            'dt_inicio' => 'Data Inicio',
            'dt_fim' => 'Data Fim',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProjetosTemModulos the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getLabelModulo()
    {
        $retorno = '<ul>' . $this->modulo->slug_modulo . ' - ';
        if ($this->dt_inicio != '') {
            $retorno .= HData::formataData($this->dt_inicio, HData::BR_DATE_FORMAT, HData::SQL_DATE_FORMAT) . ' à ';

            if ($this->dt_fim != '') {
                $retorno .= HData::formataData($this->dt_fim, HData::BR_DATE_FORMAT, HData::SQL_DATE_FORMAT);
            } else {
                $retorno .= 'Indefinido';
            }
        } else {
            $retorno .= 'Indefinido';
        }
        $retorno .= '</ul>';

        return $retorno;
    }

}
