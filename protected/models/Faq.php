<?php

/**
 * This is the model class for table "Faq".
 *
 * The followings are the available columns in table 'Faq':
 * @property integer $IDFaq
 * @property string $pergunta_faq
 * @property string $resposta_faq
 * @property integer $IDClasseFaq
 *
 * The followings are the available model relations:
 * @property ClasseFaq $iDClasseFaq
 * @package base.Models
 */
class Faq extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Faq';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('pergunta_faq, resposta_faq, IDClasseFaq', 'required'),
            array('IDClasseFaq', 'numerical', 'integerOnly' => true),
            array('pergunta_faq', 'length', 'max' => 255),
            // The following rule is used by search().
            array('IDFaq, pergunta_faq, resposta_faq, IDClasseFaq', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDClasseFaq' => array(self::BELONGS_TO, 'ClasseFaq', 'IDClasseFaq'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDFaq' => 'Perguntas Frequentes (FAQ)',
            'pergunta_faq' => 'Pergunta da FAQ',
            'resposta_faq' => 'Resposta da FAQ',
            'IDClasseFaq' => 'Classe FAQ',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('"IDFaq"', HTexto::tiraLetras($this->IDFaq));
        $criteria->compare('LOWER("pergunta_faq")', mb_strtolower($this->pergunta_faq), true);
        $criteria->compare('LOWER("resposta_faq")', mb_strtolower($this->resposta_faq), true);
        $criteria->compare('"IDClasseFaq"', $this->IDClasseFaq);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDFaq" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Faq the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
