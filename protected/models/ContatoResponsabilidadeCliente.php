<?php

/**
 * This is the model class for table "Contatos_tem_responsabilidades".
 *
 * The followings are the available columns in table 'Contatos_tem_responsabilidades':
 * @property integer $IDResponsabilidade_contato
 * @property integer $IDContato_Contato
 * @package base.Models
 */
class ContatoResponsabilidadeCliente extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Contatos_tem_responsabilidades';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDResponsabilidade_contato, IDContato_Contato', 'required'),
            array('IDResponsabilidade_contato, IDContato_Contato', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array();
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDResponsabilidade_contato' => 'Responsabilidade do Contato',
            'IDContato_Contato' => 'Idcontato Contato',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContatoResponsabilidadeCliente the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
