<?php

/**
 * This is the model class for table "drink.Funcao".
 *
 * The followings are the available columns in table 'drink.Funcao':
 * @property integer $IDFuncao
 * @property string $nome_funcao
 * @property string $dt_levantamentoFuncao
 * @property string $dt_cadastroFuncao
 *
 * The followings are the available model relations:
 * @property SetorFuncao[] $setorFuncaos
 * @package base.Models
 */
class Funcao extends ActiveRecord
{

    /**
     * Setores desta função.
     * Propriedade utilizada para receber os setores inseridos em uma criação e
     * alteração dos setores já vinculados.
     * @var Array
     */
    public $setores_funcao = [];
    /**
     * @var string $IDEmpregador guarda o ID do Empregador para filtrar setores
     */
    public $IDEmpregador;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Funcao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('nome_funcao, dt_levantamentoFuncao', 'required'),
            array('setores_funcao', 'required', 'on' => 'insert'),
            array('nome_funcao', 'length', 'max' => 255),
            array('dt_levantamentoFuncao', 'date', 'format' => 'dd/MM/yyyy'),
            array('IDEmpregador', 'safe'),
// The following rule is used by search().
            array('nome_funcao, dt_levantamentoFuncao', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'setorFuncaos' => array(self::HAS_MANY, 'SetorFuncao', 'IDFuncao'),
            'setores' => array(self::MANY_MANY, 'Setor', CLIENTE . '.Setor_Funcao(IDFuncao, IDSetor)'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDFuncao' => 'Função',
            'nome_funcao' => 'Nome da Função',
            'dt_levantamentoFuncao' => 'Data de Cadastro',
            'dt_cadastroFuncao' => 'Data de Cadastro',
            'setores_funcao' => 'Setores',
            'empresa' => 'Empresa',
            'IDEmpregador' => 'Empregador'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->select = "DISTINCT \"t\".*";
        $criteria->join = 'left outer JOIN ' . CLIENTE . '."Setor_Funcao" "sf" ON "sf"."IDFuncao" = "t"."IDFuncao"';
        $criteria->join .= ' left outer JOIN ' . CLIENTE . '."Setor" "s" ON "s"."IDSetor" = "sf"."IDSetor"';
        // $criteria->join .= 'INNER JOIN '.CLIENTE.'."Setor_Funcao" "sff" ON "sff"."IDFuncao" = "t"."IDFuncao" AND "sff"."IDSetor" = "s"."IDSetor"';
        // Como filtrar os resultados pro empregador selecionado.
        $emp = Yii::app()->user->getState('IDEmpregador');
        if ($emp == null) {
            $criteria->addInCondition('"s"."IDEmpregador"', null);
        } else {
            $criteria->compare('"s"."IDEmpregador"', $emp);
        }
        /** Fim do Critério de empregador */
        $criteria->compare('"IDFuncao"', HTexto::tiraLetras($this->IDFuncao));
        $criteria->compare('LOWER("nome_funcao")', mb_strtolower($this->nome_funcao), true);
        $criteria->compare('LOWER("dt_levantamentoFuncao")', mb_strtolower($this->dt_levantamentoFuncao), true);
        $criteria->compare('LOWER("dt_cadastroFuncao")', mb_strtolower($this->dt_cadastroFuncao), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDFuncao" DESC',
                'attributes' => array(
                    'dt_levantamentoFuncao' => array(
                        'asc' => '"dt_levantamentoFuncao"',
                        'desc' => '"dt_levantamentoFuncao" DESC',
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Funcao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getIdSetores()
    {
        $setores = array();
        $sfs = $this->setores();
        foreach ($sfs as $sf) {
            $setores[] = $sf->IDSetor;
        }
        return $setores;
    }


    public function getLabelFuncao()
    {
        return $this->nome_funcao;
    }

    public function __toString()
    {
        return $this->getLabelFuncao();
    }


    public function getEmpresaFuncao()
    {
        $s = $this->setores(array('limit' => '1'));
        foreach ($s as $ss) {
            return ($ss->iDEmpregador);
        }
    }


    public function findByEmpregador($emp = null)
    {
        $criteria = new CDbCriteria;
        $criteria->join = 'left outer JOIN ' . CLIENTE . '."Setor_Funcao" "sf" ON "sf"."IDFuncao" = "t"."IDFuncao"';
        $criteria->join .= ' left outer JOIN ' . CLIENTE . '."Setor" "s" ON "s"."IDSetor" = "sf"."IDSetor"';
        if ($emp !== null) {
            if (is_array($emp)) {
                $criteria->addInCondition('"s"."IDEmpregador"', array_keys($emp));
            } else {
                $criteria->compare('"IDEmpregador"', $emp);
            }
        } else {
            $emp = Yii::app()->user->getState('IDEmpregador');
            if ($emp == null) {
                $criteria->addInCondition('"s"."IDEmpregador"', array_keys(Yii::app()->session['empregadores']));
            } else {
                $criteria->compare('"s"."IDEmpregador"', $emp);
            }
        }
        return Funcao::model()->findAll($criteria);
    }


    public function getFuncaobyTrabalhador($IDTrabalhador)
    {
        $setorFuncao = array();
        $empregadorTrabalhador = EmpregadorTrabalhador::model()->findAllByAttributes(
            array("IDTrabalhador" => $IDTrabalhador)
        );

        foreach ($empregadorTrabalhador as $emp) {

            $TrabSetFunc = TrabalhadorSetorFuncao::model()->findAllByAttributes(
                array("IDEmpregadorTrabalhador" => $emp->IDEmpregadorTrabalhador)
            );

            foreach ($TrabSetFunc as $trab) {
                $setorFuncao[] = array(
                    "setor" => $trab->iDSetorFuncao->IDSetor,
                    "funcao" => $trab->iDSetorFuncao->IDFuncao
                );
            }

        }
        return $setorFuncao;
    }

}
