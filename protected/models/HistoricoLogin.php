<?php

/**
 * This is the model class for table "Historico_login".
 *
 * The followings are the available columns in table 'drink.Historico_login':
 * @property integer $IDHistorico_login
 * @property string $dt_historicoLogin
 * @property integer $IDUsuario
 * @property string $ip_historicoLogin
 * @property integer $error_historicoLogin
 *
 * The followings are the available model relations:
 * @property Usuario $iDUsuario
 * @package base.Models
 */
class HistoricoLogin extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Historico_login';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('IDUsuario, ip_historicoLogin, error_historicoLogin', 'required'),
            array('IDUsuario, error_historicoLogin', 'numerical', 'integerOnly' => true),
            array('ip_historicoLogin', 'length', 'max' => 20),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDUsuario' => array(self::BELONGS_TO, 'Usuario', 'IDUsuario'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDHistorico_login' => 'Registro de Login',
            'dt_historicoLogin' => 'Data Registro',
            'IDUsuario' => 'IDUsuario',
            'ip_historicoLogin' => 'Ip de Origem',
            'error_historicoLogin' => 'Código do Erro',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return HistoricoLogin the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
