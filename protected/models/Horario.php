<?php

/**
 * This is the model class for table "drink.Horario".
 *
 * The followings are the available columns in table 'drink.Horario':
 * @property integer $IDHorario
 * @property string $inicio_horario
 * @property string $fim_horario
 * @property boolean $segunda
 * @property boolean $terca
 * @property boolean $quarta
 * @property boolean $quinta
 * @property boolean $sexta
 * @property boolean $sabado
 * @property boolean $domingo
 * @property array $diaSemana
 * The followings are the available model relations:
 * @property LocalHorario[] $localHorarios
 * @property LocalProfissionalHorario[] $localProfissionalHorarios
 * @property Local[] $iDLocal
 * @property Local[] $iDLocalProfissional
 * @property Profissional[] $iDProfissional
 * @package base.Models
 */
class Horario extends ActiveRecord
{

    /*
     * Variaveis usadas apenas para pesquisa no metodo search()
     * */
    public $nome_local;
    public $nome_profissional;
    /**
     * dias da semana em ordem
     * @var diaSemana array
     */
    public $diaSemana = array(
        'domingo' => 'Domingo',
        'segunda' => 'Segunda',
        'terca' => 'Terça',
        'quarta' => 'Quarta',
        'quinta' => 'Quinta',
        'sexta' => 'Sexta',
        'sabado' => 'Sabado'
    );

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Horario';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('inicio_horario, fim_horario, diaSemana', 'required'),
            array('segunda, terca, quarta, quinta, sexta, sabado, domingo', 'safe'),
//            array(
//                'fim_horario',
//                'compare',
//                'compareAttribute' => 'inicio_horario',
//                'operator' => '>=',
//                'allowEmpty' => true,
//                'message' => '{attribute} tem que ser maior que "{compareValue}".'
//            ),
// The following rule is used by search().
            array(
                'IDHorario, inicio_horario, fim_horario, segunda, terca, quarta, quinta, sexta, sabado, domingo, nome_profissional, nome_local ',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'localHorarios' => array(self::HAS_ONE, 'LocalHorario', 'IDHorario'),
            'localProfissionalHorarios' => array(self::HAS_ONE, 'LocalProfissionalHorario', 'IDHorario'),
            'iDLocal' => array(self::MANY_MANY, 'Local', CLIENTE . '.Local_Horario(IDLocal, IDHorario)'),
            'iDLocalProfissional' => array(
                self::MANY_MANY,
                'Local',
                CLIENTE . '.Local_Profissional_Horario(IDLocal, IDHorario)'
            ),
            'iDProfissional' => array(
                self::MANY_MANY,
                'Profissional',
                CLIENTE . '.Local_Profissional_Horario(IDProfissional, IDHorario)'
            ),
        );
    }

    /**
     * Retorna a declaração de escopos nomeados.
     * Um escopo nomeado representa um critério de consulta que podem ser conectadas
     * com outros escopos nomeados e aplicados a uma consulta.
     * @return Array regras de escopo
     */
    public function scopes()
    {
        return array(
            'ordemInicio' => array(
                'order' => '"inicio_horario"'
            )
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDHorario' => 'Horário',
            'inicio_horario' => 'Horário de Início',
            'fim_horario' => 'Horário de Término',
            'diaSemana' => 'Dias da Semana',
            'segunda' => 'Segunda',
            'terca' => 'Terça',
            'quarta' => 'Quarta',
            'quinta' => 'Quinta',
            'sexta' => 'Sexta',
            'sabado' => 'Sábado',
            'domingo' => 'Domingo',
            'nome_local' => 'Nome Local',
            'nome_profissional' => 'Nome Profissional'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search($relacionamento = 'Local', $eFolga = true)
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        if ($relacionamento == 'Local') {
            $criteria->join = ' left outer JOIN ' . CLIENTE . '."Local_Horario" "lh" ON "lh"."IDHorario" = "t"."IDHorario"'
                . ' left JOIN ' . CLIENTE . '."Local" "lo" ON "lo"."IDLocal" = "lh"."IDLocal"';
            $criteria->compare('"lh"."folga"', $eFolga);
            $criteria->addCondition('"lh"."dt_exclusao" IS NULL');
            if (isset($_REQUEST['IDLocal'])) {
                $criteria->compare('"lh"."IDLocal"', $_REQUEST['IDLocal']);
            }
            $criteria->compare('LOWER("lo"."nome_local")', mb_strtolower($this->nome_local), true);
        }

        if ($relacionamento == 'Profissional') {
            $criteria->join = ' inner JOIN ' . CLIENTE . '."Local_Profissional_Horario" "lhp" ON "lhp"."IDHorario" = "t"."IDHorario"'
                . 'left JOIN ' . CLIENTE . '."Profissional" "p" ON "p"."IDProfissional" = "lhp"."IDProfissional"'
                . 'left JOIN ' . CLIENTE . '."Local" "lo" ON "lo"."IDLocal" = "lhp"."IDLocal"';

            $criteria->addCondition('"lhp"."dt_exclusao" IS NULL');
            if ($eFolga) {
                $criteria->addCondition('"lhp"."folga"');
            } else {
                $criteria->addCondition('NOT "lhp"."folga"');
            }

            $criteria->compare('LOWER("lo"."nome_local")', mb_strtolower($this->nome_local), true);
            $criteria->compare('LOWER("p"."nome_profissional")', mb_strtolower($this->nome_profissional), true);
        }


        $criteria->compare('"t"."IDHorario"', HTexto::tiraLetras($this->IDHorario));
        $criteria->compare('LOWER("inicio_horario")', mb_strtolower($this->inicio_horario), true);
        $criteria->compare('LOWER("fim_horario")', mb_strtolower($this->fim_horario), true);
        $criteria->compare('"segunda"', $this->segunda);
        $criteria->compare('"terca"', $this->terca);
        $criteria->compare('"quarta"', $this->quarta);
        $criteria->compare('"quinta"', $this->quinta);
        $criteria->compare('"sexta"', $this->sexta);
        $criteria->compare('"sabado"', $this->sabado);
        $criteria->compare('"domingo"', $this->domingo);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDHorario" DESC',
                'attributes' => array(
                    'nome_profissional' => array(
                        'asc' => '"p"."nome_profissional"',
                        'desc' => '"p"."nome_profissional" DESC',
                    ),
                    'nome_local' => array(
                        'asc' => '"lo"."nome_local"',
                        'desc' => '"lo"."nome_local" DESC',
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Horario the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function setDiasFalso()
    {
        $this->domingo = false;
        $this->segunda = false;
        $this->terca = false;
        $this->quarta = false;
        $this->quinta = false;
        $this->sexta = false;
        $this->sabado = false;
    }

    public function getDias()
    {
        $retorno = '';
        foreach ($this->diaSemana as $dia => $label) {
            if ($this->$dia) {
                $retorno .= substr($label, 0, 3) . " - ";
            }
        }
        $retorno = substr($retorno, 0, -3);

        return $retorno;

    }


    public function __toString()
    {
        $retorno = '<ul>';
        foreach ($this->diaSemana as $dia => $label) {
            if ($this->$dia) {
                $retorno .= substr($label, 0, 3) . ", ";
            }
        }
        $retorno = substr($retorno, 0, -2);
        $retorno .= " das " . HData::formataData(
                $this->inicio_horario,
                HData::BR_TIME_SIMPLE_FORMAT,
                HData::SQL_TIME_FORMAT
            ) . " às " . HData::formataData(
                $this->fim_horario,
                HData::BR_TIME_SIMPLE_FORMAT,
                HData::SQL_TIME_FORMAT
            ) . "</ul>";
        return $retorno;
    }
}
