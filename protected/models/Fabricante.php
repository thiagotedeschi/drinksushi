<?php

/**
 * This is the model class for table "drink.Fabricante".
 *
 * The followings are the available columns in table 'drink.Fabricante':
 * @property integer $IDFabricante
 * @property string $nome_fabricante
 * @property string $cnpj_fabricante
 * @property integer $IDEndereco
 * @property string $telefone_principalFabricante
 * @property string $telefone_secundarioFabricante
 *
 * The followings are the available model relations:
 * @property Endereco $iDEndereco
 * @property FonteGeradora[] $fonteGeradoras
 * @package base.Models
 */
class Fabricante extends ActiveRecord
{
    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Fabricante';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('nome_fabricante, cnpj_fabricante', 'required'),
            array('IDEndereco', 'numerical', 'integerOnly' => true),
            array('nome_fabricante', 'length', 'max' => 255),
            array('cnpj_fabricante', 'length', 'max' => 18),
            array('cnpj_fabricante', 'application.components.validators.ValidadorDocumento'),
            array('telefone_principalFabricante, telefone_secundarioFabricante', 'length', 'max' => 20),
// The following rule is used by search().
            array(
                'IDFabricante, nome_fabricante, cnpj_fabricante',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDEndereco' => array(self::BELONGS_TO, 'EnderecoFabricante', 'IDEndereco'),
            'fonteGeradoras' => array(self::HAS_MANY, 'FonteGeradora', 'IDFabricante'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDFabricante' => 'Fabricante',
            'nome_fabricante' => 'Nome do Fabricante',
            'cnpj_fabricante' => 'CNPJ do Fabricante',
            'IDEndereco' => 'Endereço',
            'telefone_principalFabricante' => 'Telefone Principal',
            'telefone_secundarioFabricante' => 'Telefone Secundário',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDFabricante"', HTexto::tiraLetras($this->IDFabricante));
        $criteria->compare('LOWER("nome_fabricante")', mb_strtolower($this->nome_fabricante), true);
        $criteria->compare('LOWER("cnpj_fabricante")', HTexto::somenteNumeros($this->cnpj_fabricante), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDFabricante" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Fabricante the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function beforeSave()
    {
        $this->cnpj_fabricante = str_replace(array('.', '-', '/'), '', $this->cnpj_fabricante);
        return parent::beforeSave();
    }

    public function __toString()
    {
        return $this->nome_fabricante;
    }

}
