<?php

/**
* This is the model class for table "drink.Frete_Motoboy".
*
* The followings are the available columns in table 'drink.Frete_Motoboy':
    * @property integer $IDFreteMotoboy
    * @property integer $IDBairro
    * @property string $valor_frete
    *
    * The followings are the available model relations:
            * @property Bairro $iDBairro
    * @package base.Models
*/
class FreteMotoboy extends ActiveRecord
{

/**
* Retorna o nome da tabela representada pelo Modelo.
*
* @return string nome da tabela
*/
public function tableName()
{
return CLIENTE.'.Frete_Motoboy';
}

/**
* Retorna as regras de validação para o Modelo
* @return Array Regras de Validação.
*/
public function rules()
{
return array(
    array('IDBairro, valor_frete', 'required'),
    array('IDBairro', 'numerical', 'integerOnly'=>true),
    array('valor_frete', 'length', 'max'=>11),
// @todo Please remove those attributes that should not be searched.
array('IDFreteMotoboy, IDBairro, valor_frete', 'safe', 'on'=>'search'),
);
}

/**
* Retorna as relações do modelo
* @return Array relações
*/
public function relations()
{
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
return array(
    'iDBairro' => array(self::BELONGS_TO, 'Bairro', 'IDBairro'),
);
}

/**
* Retorna as labels dos atributos do modelo no formato (atributo=>label)
* @return Array labels dos atributos.
*/
public function attributeLabels()
{
return array(
    'IDFreteMotoboy' => 'Idfrete Motoboy',
    'IDBairro' => 'Idbairro',
    'valor_frete' => 'Valor Frete',
);
}

/**
* Retorna uma lista de modelos baseada nas definições de filtro da tabela
* @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
*/
public function search()
{
// @todo Please modify the following code to remove attributes that should not be searched.

$criteria=new CDbCriteria;

		$criteria->compare('"IDFreteMotoboy"',HTexto::tiraLetras($this->IDFreteMotoboy));
		$criteria->compare('"IDBairro"',HTexto::tiraLetras($this->IDBairro));
		$criteria->compare('LOWER("valor_frete")',mb_strtolower($this->valor_frete),true);

return new CActiveDataProvider($this, array(
'criteria'=>$criteria,
'Pagination' => array(
'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
),
'sort' => array(
        'defaultOrder' => '"IDBairro" DESC',
    )));
}

/**
* Returns the static model of the specified AR class.
* Please note that you should have this exact method in all your CActiveRecord descendants!
* @param string $className active record class name.
* @return FreteMotoboy the static model class
*/
public static function model($className=__CLASS__)
{
return parent::model($className);
}
}
