<?php

/**
 * This is the model class for table "drink.Trabalhador_Setor_Funcao".
 *
 * The followings are the available columns in table 'drink.Trabalhador_Setor_Funcao':
 * @property string $IDTrabalhadorSetorFuncao
 * @property integer $IDSetorFuncao
 * @property string $dt_cadastro
 * @property string $IDEmpregadorTrabalhador
 * @property integer $IDOcupacao
 * @property string $cargo_TrabSetFunc
 * @property string $dt_inicioTrabSetFunc
 * @property string $dt_fimTrabSetFunc
 * @property integer $IDSetor
 *
 * The followings are the available model relations:
 * @property SituacaoTrabalhador[] $situacaoTrabalhadors
 * @property EmpregadorTrabalhador $iDEmpregadorTrabalhador
 * @property OcupacaoCBO $iDOcupacao
 * @property SetorFuncao $iDSetorFuncao
 * @package base.Models
 */
class TrabalhadorSetorFuncao extends ActiveRecord
{

    /*
     * @var $IDEmpregadorTrabalhador
     * */
    public $IDEmpregadorTrabalhador;

    /*
     * @var $dt_inicioTrabSetFunc
     * */
    public $dt_inicioTrabSetFunc;

    /**
     * @var $IDSetor
     */
    public $IDSetor;

    /**
     * @var $IDFuncao
     */
    public $IDFuncao;

    /**
     * @var $ativoTrabalhador
     */
    public $ativoTrabalhador;

    /**
     * @var $nomeTrabalhador
     */
    public $nomeTrabalhador;

    /**
     * @var $nomeSetor
     */
    public $nomeSetor;

    /**
     * @var $nomeFuncao
     */
    public $nomeFuncao;

    /**
     * @var $IDSetorFuncaoOrigem
     */
    public $IDSetorFuncaoOrigem;

    public function afterFind()
    {
        parent::afterFind();
        $this->IDSetor = $this->iDSetorFuncao->IDSetor;
        $this->IDFuncao = $this->iDSetorFuncao->IDFuncao;
        $this->dt_inicioTrabSetFunc = HData::sqlToBr($this->dt_inicioTrabSetFunc);
        $this->dt_fimTrabSetFunc = HData::sqlToBr($this->dt_fimTrabSetFunc);
    }

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Trabalhador_Setor_Funcao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDSetorFuncao, IDSetor, dt_inicioTrabSetFunc', 'required'),
            array('IDFuncao', 'validaSetorFuncao'),
            array('IDSetorFuncao, IDOcupacao, IDEmpregadorTrabalhador, IDSetor', 'numerical', 'integerOnly' => true),
            array('cargo_TrabSetFunc', 'length', 'max' => 30),
            array('dt_fimTrabSetFunc', 'comparaData', 'compareTo' => 'dt_inicioTrabSetFunc', 'on' => 'disable'),
            array('dt_cadastro, IDFuncao, ativoTrabalhador, IDSetorFuncaoOrigem, dt_ultimoExameClinicoTrabEmp', 'safe'),
            array('IDSetorFuncaoOrigem', 'required', 'on' => 'mudarFuncao'),
            array(
                'IDTrabalhadorSetorFuncao, nomeTrabalhador, nomeFuncao, nomeSetor, IDSetorFuncao, dt_cadastro, IDEmpregadorTrabalhador, IDOcupacao, cargo_TrabSetFunc, dt_inicioTrabSetFunc, dt_fimTrabSetFunc, IDSetorFuncaoOrigem',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'exameClinicos' => array(self::HAS_MANY, 'ExameClinico', 'IDTrabalhadorSetorFuncao'),
            'situacaoTrabalhadors' => array(self::HAS_MANY, 'SituacaoTrabalhador', 'IDTrabalhadorSetorFuncao'),
            'pendenciaExameComplementars' => array(self::HAS_MANY, 'PendenciaExameComplementar', 'IDTrabalhadorSetorFuncao'),
            'iDSetorFuncao' => array(self::BELONGS_TO, 'SetorFuncao', 'IDSetorFuncao'),
            'iDEmpregadorTrabalhador' => array(self::BELONGS_TO, 'EmpregadorTrabalhador', 'IDEmpregadorTrabalhador'),
            'iDOcupacao' => array(self::BELONGS_TO, 'OcupacaoCBO', 'IDOcupacao'),
        );
    }

    /**
     * Retorna a declaração de escopos nomeados.
     * Um escopo nomeado representa um critério de consulta que podem ser conectadas
     * com outros escopos nomeados e aplicados a uma consulta.
     * @return Array regras de escopo
     */
    public function scopes()
    {
        return array(
            'recently' => array(
                'order' => '"dt_fimTrabSetFunc" DESC',
            ),
            'ultimoInativo' => array(
                'order' => '"dt_fimTrabSetFunc" DESC',
                // 'condition' => '"dt_fimTrabSetFunc" IS NOT NULL AND "dt_fimTrabSetFunc" <= now()'
            )
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTrabalhadorSetorFuncao' => 'Função do Trabalhador',
            'IDSetorFuncao' => 'Setor/Função',
            'IDSetorFuncaoOrigem' => 'Setor/Função de Origem',
            'dt_cadastro' => 'Data de Cadastro',
            'IDEmpregadorTrabalhador' => 'Trabalhador',
            'IDOcupacao' => 'Ocupação CBO',
            'cargo_TrabSetFunc' => 'Cargo do Trabalhador',
            'dt_inicioTrabSetFunc' => 'Data de Início',
            'dt_fimTrabSetFunc' => 'Data de Término',
            'IDSetor' => 'Setor de Destino',
            'IDFuncao' => 'Função de Destino',
            'nomeSetor' => 'Setor',
            'nomeFuncao' => 'Função',
            'nomeTrabalhador' => 'Trabalhador',
            'dt_ultimoExameClinicoTrabEmp' => 'Data do Último Exame Clínico'

        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->with = array(
            'iDEmpregadorTrabalhador.iDTrabalhador',
            'iDSetorFuncao.iDFuncao',
            'iDSetorFuncao.iDSetor'
        );

        $criteria->compare('LOWER("IDTrabalhadorSetorFuncao")', mb_strtolower($this->IDTrabalhadorSetorFuncao), true);
        $criteria->compare('"IDSetorFuncao"', HTexto::tiraLetras($this->IDSetorFuncao));
        $criteria->compare('LOWER(t."dt_cadastro")', mb_strtolower($this->dt_cadastro), true);
        $criteria->compare('LOWER("IDEmpregadorTrabalhador")', mb_strtolower($this->IDEmpregadorTrabalhador), true);
        $criteria->compare('"IDOcupacao"', $this->IDOcupacao);
        $criteria->compare('LOWER("cargo_TrabSetFunc")', mb_strtolower($this->cargo_TrabSetFunc), true);
        $criteria->compare('LOWER("iDTrabalhador"."nome_trabalhador")', mb_strtolower($this->nomeTrabalhador), true);
        $criteria->compare('LOWER("iDFuncao"."nome_funcao")', mb_strtolower($this->nomeFuncao), true);
        $criteria->compare('LOWER("iDSetor"."nome_setor")', mb_strtolower($this->nomeSetor), true);
        $criteria->compare('"dt_inicioTrabSetFunc"', HData::brToSql($this->dt_inicioTrabSetFunc));
        $criteria->compare('LOWER("dt_fimTrabSetFunc")', mb_strtolower($this->dt_fimTrabSetFunc), true);

        //Filtrar por empregadores
        $emp = Yii::app()->user->getState('IDEmpregador');
        if ($emp == null) {
            $criteria->addInCondition('"iDEmpregadorTrabalhador"."IDEmpregador"', null);
        } else {
            $criteria->compare('"iDEmpregadorTrabalhador"."IDEmpregador"', $emp);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']),
            ), //mude o número de registros por página aqui
            'sort' => array(
                'defaultOrder' => '"t"."dt_cadastro" DESC',
                'attributes' => array(
                    'nomeTrabalhador' => array(
                        'asc' => '"iDTrabalhador"."nome_trabalhador"',
                        'desc' => '"iDTrabalhador"."nome_trabalhador" DESC'
                    ),
                    'nomeSetor' => array(
                        'asc' => '"iDSetor"."nome_setor"',
                        'desc' => '"iDSetor"."nome_setor" DESC'
                    ),
                    'nomeFuncao' => array(
                        'asc' => '"iDFuncao"."nome_funcao"',
                        'desc' => '"iDFuncao"."nome_funcao" DESC'
                    ),
                    'dt_inicioTrabSetFunc' => array(
                        'asc' => '"t"."dt_inicioTrabSetFunc"',
                        'desc' => '"t"."dt_inicioTrabSetFunc" DESC'
                    ),
                    '*'
                ),
            )

        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TrabalhadorSetorFuncao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getTrabalhadores($IDSetorFuncao)
    {
        $criteria = new CDbCriteria();
        if (is_array($IDSetorFuncao)) {
            $criteria->addCondition('t."IDSetorFuncao" IN (' . implode(',', $IDSetorFuncao) . ')');
        } else {
            $criteria->compare('t."IDSetorFuncao"', $IDSetorFuncao);
        }
        $criteria->join .= 'INNER JOIN ' . CLIENTE . '."Empregador_Trabalhador" AS "empTrab" ON "empTrab"."IDEmpregadorTrabalhador" = t."IDEmpregadorTrabalhador" ';
        $criteria->join .= 'INNER JOIN ' . CLIENTE . '."Situacao_Trabalhador" AS "sitTrab" ON "sitTrab"."IDTrabalhadorSetorFuncao" = t."IDTrabalhadorSetorFuncao" ';
        $criteria->join .= 'INNER JOIN "public"."Tipo_Situacao" AS "tipoSit" ON "tipoSit"."IDTipoSituacao" = "sitTrab"."IDTipoSituacao" ';
        $criteria->compare('"tipoSit".status_situacao', 0);
        $criteria->addCondition(
            '("dt_fimTrabSetFunc" IS NULL OR "dt_fimTrabSetFunc" >= now()) AND "dt_inicioTrabSetFunc" <= now()'
        );
        $criteria->addCondition('"sitTrab"."IDTrabalhadorSetorFuncao"', 'GROUP BY');

        $this->getDbCriteria()->mergeWith($criteria);

        return $this;
    }


    public function validaSetorFuncao($attribute, $params)
    {
        if ($this->isNewRecord && $this->IDSetor && $this->IDFuncao && $this->iDEmpregadorTrabalhador) {
            $setorFuncao = SetorFuncao::model()->findByAttributes(
                ['IDSetor' => $this->IDSetor, 'IDFuncao' => $this->IDFuncao]
            );
            if ($setorFuncao) {
                $setorFuncaoAtual = $this->iDEmpregadorTrabalhador->getSetorFuncao();
                foreach ($setorFuncaoAtual as $setorFuncaoAtuals) {
                    if ($setorFuncao->IDSetorFuncao == $setorFuncaoAtuals->IDSetorFuncao) {
                        $this->addError(
                            $attribute,
                            'A função selecionada deve ser diferente da função atual do Trabalhador'
                        );
                    }
                }
            }
        }
    }


    public function getSituacao($visivel = null)
    {
        $condition = '';
        if ($visivel) {
            $condition = '"visivel_situacaoTrabalhador"';
        } else {
            if ($visivel === false) {
                $condition = 'NOT "visivel_situacaoTrabalhador"';
            }
        }

        return array_shift(
            $this->situacaoTrabalhadors(array('order' => '"dt_situacaoTrabalhador" DESC', 'condition' => $condition))
        );
    }


    public function getLabelSetorFuncaoSituacao()
    {
        $retorno = $this->iDSetorFuncao->labelSetorFuncao;
        if ($situacao = $this->getSituacao()) {
            $retorno .= ' (' . $situacao->iDTipoSituacao . ')';
        }
        return $retorno;
    }


    public function getUltimoSetorFuncao($IDTrabalhador)
    {
        $empTrab = EmpregadorTrabalhador::model()->findByAttributes(array("IDTrabalhador" => $IDTrabalhador));
        $setFunc = $this->findByAttributes(array("IDEmpregadorTrabalhador" => $empTrab->IDEmpregadorTrabalhador));
        $criteria = new CDbCriteria();
        $criteria->order = 't."IDTrabalhadorSetorFuncao" DESC';
        $criteria->limit = '1';
        $this->getDbCriteria()->mergeWith($criteria);

        $situacao = SituacaoTrabalhador::model()->getSituacaoByTrabalhadorSetorFuncao(
            $setFunc->IDTrabalhadorSetorFuncao
        );

        return $setFunc->iDSetorFuncao . '<br>[' . $situacao . ']';
    }

    public function findAllByStatusSituacao($IDStatusSituacao = null)
    {
        $criteria = new CDbCriteria;
        $criteria->join = ' INNER JOIN ' . CLIENTE . '."Situacao_Trabalhador" AS st ';
        $criteria->join .= ' ON "st"."IDTrabalhadorSetorFuncao" = "t"."IDTrabalhadorSetorFuncao"';
        $criteria->join .= ' INNER JOIN "public"."Tipo_Situacao" AS ts ';
        $criteria->join .= ' ON "ts"."IDTipoSituacao" = "st"."IDTipoSituacao"';
        $criteria->compare('st."visivel_situacaoTrabalhador"', true);
        $criteria->compare('st."situacao_atualTrabalhador"', true);
        empty($IDStatusSituacao) ? : $criteria->addInCondition('ts."status_situacao"', $IDStatusSituacao);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function findAllByTipoSituacao($IDTiposSituacao = null)
    {
        $criteria = new CDbCriteria;
        $criteria->join = ' INNER JOIN ' . CLIENTE . '."Situacao_Trabalhador" AS st ';
        $criteria->join .= ' ON "st"."IDTrabalhadorSetorFuncao" = "t"."IDTrabalhadorSetorFuncao"';
        $criteria->join .= ' INNER JOIN "public"."Tipo_Situacao" AS ts ';
        $criteria->join .= ' ON "ts"."IDTipoSituacao" = "st"."IDTipoSituacao"';
        $criteria->compare('st."visivel_situacaoTrabalhador"', true);
        $criteria->compare('st."situacao_atualTrabalhador"', true);
        $criteria->addCondition('st."dt_situacaoTrabalhador" <= now()');
        empty($IDTiposSituacao) ? : $criteria->addInCondition('ts."IDTipoSituacao"', $IDTiposSituacao);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }


    public function findAllBySetorFuncao($IDSetorFuncao)
    {
        $criteria = new CDbCriteria;
        $criteria->join = ' INNER JOIN ' . CLIENTE . '."Setor_Funcao" AS sf on "sf"."IDSetorFuncao" = "t"."IDSetorFuncao" ';
        $criteria->compare('"t"."IDSetorFuncao"', $IDSetorFuncao);
        $criteria->compare('"sf"."ativo_setorFuncao"', true);
        $criteria->addCondition(
            '"t"."dt_inicioTrabSetFunc"  <= now()'
        );
        $criteria->addCondition('"t"."dt_fimTrabSetFunc" IS NULL OR "t"."dt_fimTrabSetFunc" >= now()');
        $criteria->order = '"t"."dt_inicioTrabSetFunc" DESC';
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }


    public function findAllByEmpregador($emp = null, $filtro = null, $limite = null)
    {
        $criteria = new CDbCriteria;
        $criteria->with = ['iDEmpregadorTrabalhador', 'iDEmpregadorTrabalhador.iDTrabalhador'];
        if ($emp !== null) {
            if (is_array($emp)) {
                $criteria->addInCondition('"iDEmpregadorTrabalhador"."IDEmpregador"', array_keys($emp));
            } else {
                $criteria->compare('"iDEmpregadorTrabalhador"."IDEmpregador"', $emp);
            }
        } else {
            $emp = Yii::app()->user->getState('IDEmpregador');
            if ($emp == null) {
                $criteria->addInCondition(
                    '"iDEmpregadorTrabalhador"."IDEmpregador"',
                    array_keys(Yii::app()->session['empregadores'])
                );
            } else {
                $criteria->compare('"iDEmpregadorTrabalhador"."IDEmpregador"', $emp);
            }
        }
        //filtra por string
        if ($filtro) {
            $criteria->with = 'iDTrabalhador';
            $criteria->addCondition(
                'LOWER("iDTrabalhador"."nome_trabalhador") LIKE \'%' . mb_strtolower($filtro) . '%\''
            );
        }
        $criteria->limit = $limite;
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }
}
