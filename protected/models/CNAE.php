<?php

/**
 * This is the model class for table "CNAE".
 *
 * The followings are the available columns in table 'CNAE':
 * @property integer $CNAE
 * @property integer $IDGrupo
 * @property string $desc_CNAE
 * @property integer $sat_CNAE
 *
 * The followings are the available model relations:
 * @property GrupoCNAE $iDGrupo
 * @package base.Models
 */
class CNAE extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.CNAE';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('CNAE, IDGrupo, desc_CNAE', 'required'),
            array('CNAE, IDGrupo, sat_CNAE', 'numerical', 'integerOnly' => true),
            array('desc_CNAE', 'length', 'max' => 255),
// The following rule is used by search().
            array('CNAE, IDGrupo, desc_CNAE, sat_CNAE', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDGrupo' => array(self::BELONGS_TO, 'GrupoCNAE', 'IDGrupo'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'CNAE' => 'CNAE',
            'IDGrupo' => 'Grupo',
            'desc_CNAE' => 'Descrição',
            'sat_CNAE' => 'SAT',
            'grau_riscoGrupo' => 'Grau de Risco',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->with = array('iDGrupo');
        $gr = isset($_REQUEST['CNAE']['grau_riscoGrupo']) ? $_REQUEST['CNAE']['grau_riscoGrupo'] : '';
        $criteria->compare('"CNAE"', HTexto::somenteNumeros($this->CNAE));
        $criteria->compare(
            'LOWER(CONCAT(cast("iDGrupo"."IDGrupo" as text), \' - \', "iDGrupo".descricao_grupo))',
            mb_strtolower($this->IDGrupo),
            true
        );
        $criteria->compare('LOWER("desc_CNAE")', mb_strtolower($this->desc_CNAE), true);
        $criteria->compare('"sat_CNAE"', $this->sat_CNAE);
        $criteria->compare('"iDGrupo"."grau_riscoGrupo"', $gr);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"CNAE" DESC',
                'attributes' => array(
                    'grau_riscoGrupo' => array(
                        'asc' => '"iDGrupo"."grau_riscoGrupo"',
                        'desc' => '"iDGrupo"."grau_riscoGrupo" DESC',
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CNAE the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getNomeGrupo()
    {
        return $this->iDGrupo->descricao_grupo;
    }


    public function getLabelGrupo()
    {
        return $this->iDGrupo->getLabelGrupo();
    }


    public function getLabelCnae()
    {
        return $this->getCnae() . ' - ' . $this->desc_CNAE;
    }


    public function getCnae()
    {
        return HTexto::formataString(HTexto::MASK_CNAE, $this->CNAE, "0", 7, STR_PAD_LEFT);
    }

    public function __toString()
    {
        return $this->getLabelCnae();
    }

}
