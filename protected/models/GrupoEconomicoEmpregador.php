<?php

/**
 * This is the model class for table "drink.Grupo_economico".
 *
 * The followings are the available columns in table 'drink.Grupo_economico':
 * @property integer $IDGrupo_economico
 * @property string $nome_grupoEconomico
 * @property integer $IDResponsavel_grupoEconomico
 *
 * The followings are the available model relations:
 * @property Empregador[] $empregadors
 * @property Empregador $iDResponsavelGrupoEconomico
 * @package base.Models
 */
class GrupoEconomicoEmpregador extends ActiveRecord
{

    /*
     * nome da Empresa Responsavel , utilizado principalmente na search
     * @var $nome_empregador
     * */
    public $nome_empregador;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Grupo_economico';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('nome_grupoEconomico, IDResponsavel_grupoEconomico', 'required'),
            array('IDResponsavel_grupoEconomico', 'numerical', 'integerOnly' => true),
            array('nome_grupoEconomico', 'length', 'max' => 255),
// The following rule is used by search().
            array(
                'IDGrupo_economico, nome_grupoEconomico, IDResponsavel_grupoEconomico, nome_empregador',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'empregador' => array(self::HAS_MANY, 'Empregador', 'IDGrupo_economico'),
            'iDResponsavelGrupoEconomico' => array(self::BELONGS_TO, 'Empregador', 'IDResponsavel_grupoEconomico'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDGrupo_economico' => 'Grupo Econômico',
            'nome_grupoEconomico' => 'Nome',
            'IDResponsavel_grupoEconomico' => 'Empresa Responsável',
            'nome_empregador' => 'Empresa Responsável',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->with = ['iDResponsavelGrupoEconomico'];

        $criteria->compare('"t"."IDGrupo_economico"', HTexto::tiraLetras($this->IDGrupo_economico));
        $criteria->compare('LOWER("nome_grupoEconomico")', mb_strtolower($this->nome_grupoEconomico), true);
        $criteria->compare(
            'LOWER("nome_empregador")',
            mb_strtolower($this->nome_empregador),
            true
        );
        $criteria->compare('IDResponsavel_grupoEconomico"', $this->IDResponsavel_grupoEconomico);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"t"."IDGrupo_economico" DESC',
                'attributes' => array(
                    'nome_empregador' => array(
                        'asc' => '"iDResponsavelGrupoEconomico"."nome_empregador"',
                        'desc' => '"iDResponsavelGrupoEconomico"."nome_empregador" DESC'
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return GrupoEconomicoEmpregador the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    function __toString()
    {
        return $this->getLabelGrupoEconomico();
    }

    function getLabelGrupoEconomico()
    {
        return $this->nome_grupoEconomico;
    }

    public function beforeDelete()
    {
        $return = true;

        $mensagem = [];

        $empregadors = Empregador::model()->findAllByAttributes(['IDGrupo_economico' => $this->IDGrupo_economico]);
        if (!empty($empregadors)) {
            $return = false;
            $arrayEmp = [];
            foreach ($empregadors as $empregador) {
                $arrayEmp[] = '#' . $empregador->IDEmpregador;
            }
            $mensagem[] = 'Erro! Para excluir esse Grupo Econômico, deve primeiro desassociá-lo dos seguintes Empregadores: ' . implode(
                    ', ',
                    $arrayEmp
                );
        }
        Yii::app()->user->setFlash('error', $mensagem);

        if ($return) {
            return parent::beforeDelete();
        } else {
            return false;
        }
    }
}
