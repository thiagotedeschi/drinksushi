<?php

/**
 * This is the model class for table "Telefone_contato".
 *
 * The followings are the available columns in table 'Telefone_contato':
 * @property integer $IDTelefone_contato
 * @property string $ddd_telefoneContato
 * @property string $numero_telefoneContato
 * @property string $tipo_telefoneContato
 * @property string $principal_telefoneContato
 * @property string $ramal_telefoneContato
 * @property integer $IDContato
 *
 * The followings are the available model relations:
 * @property Contato $iDContato
 * @package base.Models
 */
class TelefoneContatoCliente extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Telefone_contato';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('numero_telefoneContato, tipo_telefoneContato, principal_telefoneContato', 'required'),
            array('IDContato', 'numerical', 'integerOnly' => true),
            array('ddd_telefoneContato', 'length', 'max' => 5),
            array('numero_telefoneContato', 'length', 'max' => 12),
            array('tipo_telefoneContato', 'length', 'max' => 1),
            array('principal_telefoneContato', 'length', 'max' => 2),
            array('ramal_telefoneContato', 'safe'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDContato' => array(self::BELONGS_TO, 'Contato', 'IDContato'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTelefone_contato' => 'Telefone do Contato',
            'ddd_telefoneContato' => 'Ddd Telefone Contato',
            'numero_telefoneContato' => 'Numero Telefone Contato',
            'tipo_telefoneContato' => 'Tipo Telefone Contato',
            'principal_telefoneContato' => 'Principal Telefone Contato',
            'ramal_telefoneContato' => 'Ramal Telefone Contato',
            'IDContato' => 'Idcontato',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TelefoneContatoCliente the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
