<?php

/**
 * This is the model class for table "Endereco".
 *
 * The followings are the available columns in table 'Endereco':
 * @property integer $IDEndereco
 * @property string $logradouro_endereco
 * @property string $cep_endereco
 * @property string $numero_endereco
 * @property string $complemento_endereco
 * @property string $referencia_endereco
 * @property integer $IDCidade
 * @property integer $IDPais
 * @property string $bairro_endereco
 */
class EnderecoFabricante extends Endereco
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Endereco';
    }

}
