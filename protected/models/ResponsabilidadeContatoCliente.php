<?php

/**
 * This is the model class for table "Responsabilidade_contato".
 *
 * The followings are the available columns in table 'Responsabilidade_contato':
 * @property integer $IDResponsabilidade_contato
 * @property string $nome_resp
 *
 * The followings are the available model relations:
 * @property Contato[] $contatos
 * @package base.Models
 */
class ResponsabilidadeContatoCliente extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Responsabilidade_contato';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('nome_resp', 'required'),
            array('nome_resp', 'length', 'max' => 255),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'contatos' => array(
                self::MANY_MANY,
                'Contato',
                'Contatos_tem_responsabilidades(IDResponsabilidade_contato, IDContato_Contato)'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDResponsabilidade_contato' => 'Idresponsabilidade Contato',
            'nome_resp' => 'Nome Resp',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ResponsabilidadeContatoCliente the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
