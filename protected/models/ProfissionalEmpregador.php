<?php

/**
 * This is the model class for table "drink.Profissional_Empregador".
 *
 * The followings are the available columns in table 'drink.Profissional_Empregador':
 * @property integer $IDEmpregador
 * @property integer $IDProfissional
 * @property string $dt_inicio
 * @property string $dt_fim
 * @property string $dt_desativacao
 * @property integer $IDProfissionalEmpregador
 *
 * The followings are the available model relations:
 * @property Empregador $iDEmpregador
 * @property Profissional $iDProfissional
 * @package base.Models
 */
class ProfissionalEmpregador extends ActiveRecord
{
    /**
     * @var $ativo boolean informa se o Agente está ativo ou não
     */
    public $ativo;

    public $nome_profissional;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Profissional_Empregador';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('dt_inicio', 'required', 'except' => 'desativacao'),
            array('IDProfissional', 'numerical', 'integerOnly' => true),
            array('dt_fim, dt_desativacao', 'safe'),
            array('dt_desativacao', 'required', 'on' => 'desativacao'),
            array('dt_fim, dt_inicio', 'date', 'format' => 'dd/MM/yyyy', 'except' => 'desativacao'),
            array(
                'IDEmpregador, IDProfissional, dt_inicio, ativo, nome_profissional',
                'safe',
                'on' => 'search'
            ),
            array(
                'dt_fim',
                'compare',
                'compareAttribute' => 'dt_inicio',
                'operator' => '>=',
                'allowEmpty' => true,
                'message' => '{attribute} tem que ser igual ou maior que {compareAttribute}.'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDEmpregador' => array(self::BELONGS_TO, 'Empregador', 'IDEmpregador'),
            'iDProfissional' => array(self::BELONGS_TO, 'Profissional', 'IDProfissional'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDEmpregador' => 'Empregador',
            'IDProfissional' => 'Profissional',
            'nome_profissional' => 'Profissional',
            'dt_inicio' => 'Data de Início',
            'dt_fim' => 'Data de Fim',
            'dt_desativacao' => 'Data de Desativação',
            'IDProfissionalEmpregador' => 'Associação Profissional x Empregador',
            'ativo' => 'Ativo?'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->with = array('iDEmpregador', 'iDProfissional');


        // Como filtrar os resultados pro empregador selecionado.
        $emp = Yii::app()->user->getState('IDEmpregador');
        if ($emp == null) {
            $criteria->addInCondition('"iDEmpregador"."IDEmpregador"', null);
        } else {
            $criteria->compare('"iDEmpregador"."IDEmpregador"', $emp);
        }

        //$criteria->compare('"IDEmpregador"', $this->IDEmpregador);
        $criteria->compare(
            'LOWER("iDProfissional"."nome_profissional")',
            mb_strtolower($this->nome_profissional),
            true
        );
        $criteria->compare('"dt_inicio"', HData::brToSql($this->dt_inicio), true);
        $criteria->compare('"IDProfissionalEmpregador"', HTexto::tiraLetras($this->IDProfissionalEmpregador));

        if ($this->ativo === '1') {
            $criteria->addCondition('"dt_desativacao" IS NULL');
        } elseif ($this->ativo === '0') {
            $criteria->addCondition('"dt_desativacao" IS NOT NULL');
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDProfissionalEmpregador" DESC',
                'attributes' => array(
                    'nome_profissional' => array(
                        'asc' => '"iDProfissional"."nome_profissional"',
                        'desc' => '"iDProfissional"."nome_profissional" DESC',
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProfissionalEmpregador the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function afterFind()
    {
        if (!empty($this->dt_desativacao)) {
            $this->ativo = false;
            return parent::afterFind();
        }
        $dt_fim = $this->dt_fim;
        if (!empty($dt_fim)) {
            $dt_fim = new DateTime($dt_fim);
        }
        $dt_inicio = $this->dt_inicio;
        if (!empty($dt_inicio)) {
            $dt_inicio = new DateTime($dt_inicio);
        }
        $now = new DateTime();
        $this->ativo = (($dt_fim == null || $dt_fim > $now) && ($dt_inicio == null || $dt_inicio < $now));
        return parent::afterFind();
    }
}
