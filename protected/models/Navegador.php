<?php

/**
 * This is the model class for table "Navegador".
 *
 * The followings are the available columns in table 'Navegador':
 * @property integer $IDNavegador
 * @property string $nome_navegador
 *
 * The followings are the available model relations:
 * @property Teste[] $testes
 * @package base.Models
 */
class Navegador extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'Navegador';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDNavegador, nome_navegador', 'required'),
            array('IDNavegador', 'numerical', 'integerOnly' => true),
            array('nome_navegador', 'length', 'max' => 60),
// @todo Please remove those attributes that should not be searched.
            array('IDNavegador, nome_navegador', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'testes' => array(self::MANY_MANY, 'Teste', 'Teste_Navegador(IDNavegador, IDTeste)'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDNavegador' => 'Idnavegador',
            'nome_navegador' => 'Nome Navegador',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('"IDNavegador"', HTexto::tiraLetras($this->IDNavegador));
        $criteria->compare('LOWER("nome_navegador")', mb_strtolower($this->nome_navegador), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDNavegador" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Navegador the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
