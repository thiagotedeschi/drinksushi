<?php

/**
 * This is the model class for table "GrupoCNAE".
 *
 * The followings are the available columns in table 'GrupoCNAE':
 * @property integer $IDGrupo
 * @property string $descricao_grupo
 * @property integer $grau_riscoGrupo
 *
 * The followings are the available model relations:
 * @property CidCnae[] $cidCnaes
 * @property CNAE[] $cNAEs
 * @package base.Models
 */
class GrupoCNAE extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.GrupoCNAE';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDGrupo, descricao_grupo, grau_riscoGrupo', 'required'),
            array('IDGrupo, grau_riscoGrupo', 'numerical', 'integerOnly' => true),
            array('descricao_grupo', 'length', 'max' => 255),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        return array(
            'cidCnaes' => array(self::HAS_MANY, 'CidCnae', 'IDGrupoCnae'),
            'cNAEs' => array(self::HAS_MANY, 'CNAE', 'IDGrupo'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDGrupo' => 'Grupo de CNAEs',
            'descricao_grupo' => 'Descricao Grupo',
            'grau_riscoGrupo' => 'Grau Risco Grupo',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return GrupoCNAE the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getLabelGrupo()
    {
        $idgrupo = $this->getIDGrupo();
        return $idgrupo . ' - ' . $this->descricao_grupo . '(GR. Risco ' . $this->grau_riscoGrupo . ')';
    }

    public function getIDGrupo()
    {
        return HTexto::formataString(HTexto::GRUPO_CNAE_MASK, $this->IDGrupo, "0", 5, STR_PAD_LEFT);
    }

}
