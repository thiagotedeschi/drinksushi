<?php

/**
 * This is the model class for table "Projeto".
 *
 * The followings are the available columns in table 'Projeto':
 * @property integer $IDProjeto
 * @property string $dt_inicioProjeto
 * @property string $dt_fimProjeto
 * @property double $vl_projeto
 * @property integer $IDCliente
 * @property double $vl_mensalProjeto
 * @property string $usuario_projeto
 * @property string $senha_projeto
 * @property string $ativo_projeto
 * @property integer $max_vidasAtivasProjeto
 * @property integer $max_empresasAtivasProjeto
 * @property string $slug_projeto
 * @property string $img_projeto
 * @property integer $max_usuariosAtivosProjeto
 *
 * The followings are the available model relations:
 * @property Modulo[] $modulos
 * @property PrestacaoServico[] $prestacaoServicos
 * @property ParcelaProjeto[] $parcelaProjetos
 * @property Cliente $iDCliente
 * @property Cobranca[] $cobrancas
 * @package base.Models
 */
class Projeto extends ActiveRecord
{

    const NOME_PROJETO_BASE = 1;

    CONST IMG_X = 600;
    CONST IMG_Y = 200;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Projeto';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('slug_projeto', 'unique'),
            array(
                'img_projeto,slug_projeto, dt_inicioProjeto, vl_projeto, vl_mensalProjeto, max_vidasAtivasProjeto, max_empresasAtivasProjeto, max_usuariosAtivosProjeto,
                IDCliente',
                'required'
            ),
            array('dt_inicioProjeto, dt_fimProjeto', 'date', 'format' => 'dd/MM/yyyy'),
            array(
                'IDCliente, max_vidasAtivasProjeto, max_empresasAtivasProjeto, max_usuariosAtivosProjeto',
                'numerical',
                'integerOnly' => true
            ),
            array('vl_projeto, vl_mensalProjeto', 'numerical'),
            array('usuario_projeto, senha_projeto, slug_projeto', 'length', 'max' => 255),
            array('ativo_projeto', 'length', 'max' => 1),
//            array('dt_fimProjeto', 'safe'),
            array(
                'dt_fimProjeto',
                'compare',
                'compareAttribute' => 'dt_inicioProjeto',
                'operator' => '>=',
                'allowEmpty' => true,
                'message' => '{attribute} tem que ser maior que "{compareValue}".'
            ),
// The following rule is used by search().
            array(
                'IDProjeto, dt_inicioProjeto, ativo_projeto, slug_projeto',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'modulos' => array(self::MANY_MANY, 'Modulo', 'Projetos_tem_modulos(IDProjeto, IDModulo)'),
            'prestacaoServicos' => array(self::HAS_MANY, 'PrestacaoServico', 'IDProjeto'),
            'parcelaProjetos' => array(self::HAS_MANY, 'ParcelaProjeto', 'IDProjeto'),
            'iDCliente' => array(self::BELONGS_TO, 'Cliente', 'IDCliente'),
            'cobrancas' => array(self::HAS_MANY, 'Cobranca', 'IDProjeto'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDProjeto' => 'Projeto',
            'dt_inicioProjeto' => 'Data Inicio Projeto',
            'dt_fimProjeto' => 'Data Fim Projeto',
            'vl_projeto' => 'Valor Projeto',
            'IDCliente' => 'Cliente',
            'vl_mensalProjeto' => 'Valor Mensal Projeto',
            'usuario_projeto' => 'Usuário Projeto',
            'senha_projeto' => 'Senha Projeto',
            'ativo_projeto' => 'Ativo Projeto',
            'max_vidasAtivasProjeto' => 'Max Vidas Ativas Projeto',
            'max_empresasAtivasProjeto' => 'Max Empresas Ativas Projeto',
            'slug_projeto' => 'Slug Projeto',
            'max_usuariosAtivosProjeto' => 'Max Usuarios Ativos Projeto',
            'img_projeto' => 'Logo do Projeto'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDProjeto"', HTexto::tiraLetras($this->IDProjeto));
        $criteria->compare('"dt_inicioProjeto"', $this->dt_inicioProjeto, true);
        $criteria->compare('LOWER("slug_projeto")', mb_strtolower($this->slug_projeto), true);
        $criteria->compare('"ativo_projeto"', $this->ativo_projeto);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDProjeto" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Projeto the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function atualizaParcelas($parcelas)
    {
        if (isset($parcelas['update'])) {
            foreach ($parcelas['update'] as $id => $parc) {
                $parcPrestacao = ParcelaProjeto::model()->findByPk($id);

                $parcPrestacao->dt_referenciaParcela = $parc['dtParcela'];
                $parcPrestacao->vl_parcela = (float)str_replace(',', '.', $parc['vlParcela']);
                $parcPrestacao->desconto_parcela = $parc['descParcela'];

                $parcPrestacao->save();
            }

            $updates = Yii::app()->db->createCommand()
                ->select("IDParcela")
                ->from('Parcela_projeto')
                ->where(
                    array("and", '"IDProjeto" = :IDProjeto', '"dt_referenciaParcela" > :hj'),
                    array(':IDProjeto' => $this->IDProjeto, ':hj' => date('Y-m-d'))
                )
                ->queryColumn();

            foreach ($updates as $id) {
                if (!isset($parcelas['update'][$id])) {
                    $parcPrestacao = ParcelaProjeto::model()->findByPk($id);
                    $parcPrestacao->delete();
                }
            }
        }
        if (isset($parcelas['nova'])) {
            foreach ($parcelas['nova'] as $parc) {
                $parcPrestacao = new ParcelaProjeto;

                $parcPrestacao->dt_referenciaParcela = $parc['dtParcela'];
                $parcPrestacao->vl_parcela = (float)str_replace(',', '.', $parc['vlParcela']);
                $parcPrestacao->desconto_parcela = $parc['descParcela'];
                $parcPrestacao->IDProjeto = $this->IDProjeto;

                $parcPrestacao->save();
            }
        }
    }

    public function atualizaModulos($modulos)
    {
        foreach ($this->modulos as $mod) {
            $modProjeto = ProjetosTemModulos::model()->findByAttributes(
                array('IDModulo' => $mod->IDModulo, 'IDProjeto' => $this->IDProjeto)
            );

            if (array_key_exists($mod->IDModulo, $modulos)) {

                $modProjeto->dt_inicio = $modulos[$mod->IDModulo]['dtInicio'] == '' ? null : $modulos[$mod->IDModulo]['dtInicio'];
                $modProjeto->dt_fim = $modulos[$mod->IDModulo]['dtFim'] == '' ? null : $modulos[$mod->IDModulo]['dtFim'];

                $modProjeto->save();

                unset($modulos[$mod->IDModulo]);
            } else {
                $modProjeto->delete();
            }
        }
        foreach ($modulos as $id => $mod) {
            $modProjeto = new ProjetosTemModulos;

            $modProjeto->IDModulo = $id;
            $modProjeto->IDProjeto = $this->IDProjeto;
            $modProjeto->dt_inicio = $mod['dtInicio'] == '' ? null : $mod['dtInicio'];
            $modProjeto->dt_fim = $mod['dtFim'] == '' ? null : $mod['dtFim'];

            $modProjeto->save();
        }
    }

    public function viewModulos()
    {
        $projetoTemModulos = $this->projetoTemModulos();
        $retorno = '';
        foreach ($projetoTemModulos as $obj) {
            $retorno .= $obj->getLabelModulo();
        }
        return $retorno;
    }


    public function projetoTemModulos()
    {
        $projetoTemModulos = ProjetosTemModulos::model()->findAllByAttributes(array('IDProjeto' => $this->IDProjeto));
        return $projetoTemModulos;
    }


    public function preparaImg()
    {
        Yii::import('ext.upload.Upload');
        if (!isset($_FILES['Projeto'])) {
            $this->img_projeto = (stream_get_contents($this->img_projeto));
            return false;
        }
        $file = HFile::organizaFile($_FILES['Projeto']);
        $imagem = new Upload($file);
        if ($file['tmp_name'] == '' || !is_uploaded_file($file['tmp_name'])) {
            if ($this->img_projeto == '') {
                return false;
            } else {
                $this->img_projeto = (stream_get_contents($this->img_projeto));
            }
        } else {
            $imagem->image_resize  = true;
            $imagem->image_x       = 250;
            $imagem->image_y       = 100;
            $imagem->image_ratio   = false;
            $this->img_projeto = base64_encode($imagem->process());
            return true;
        }
    }

}
