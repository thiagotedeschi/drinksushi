<?php

/**
 * This is the model class for table "Cliente".
 *
 * The followings are the available columns in table 'Cliente':
 * @package base.Models
 * @property integer $IDCliente
 * @property string $nome_cliente
 * @property string $razao_cliente
 * @property string $tipo_pessoaCliente
 * @property string $documento_cliente
 * @property string $email_cliente
 * @property string $tel_cliente
 * @property string $ddd_telCliente
 * @property integer $IDEndereco_correspondencia
 * @property integer $IDGrupo_economico
 * @property integer $IDEndereco_fiscal
 * @property string $site_cliente
 * @property string $ins_estadualCliente
 * @property string $ins_municipalCliente
 * @property boolean $NFEgrupo_cliente
 * @property string $cnae_principal
 *
 * The followings are the available model relations:
 * @property Endereco $iDEnderecoCorrespondencia
 * @property Endereco $iDEnderecoFiscal
 * @property GrupoEconomico $iDGrupoEconomico
 * @property ResponsavelLegal[] $responsavelLegals
 * @property GrupoEconomico[] $grupoEconomicos
 * @property Projeto[] $projetos
 * @property Contato[] $contatos
 */
class Cliente extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Cliente';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('nome_cliente, tipo_pessoaCliente, documento_cliente', 'required'),
            array(
                'IDEndereco_correspondencia, IDGrupo_economico, IDEndereco_fiscal',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'nome_cliente, razao_cliente, documento_cliente, email_cliente, site_cliente, ins_municipalCliente',
                'length',
                'max' => 255
            ),
            array('tipo_pessoaCliente', 'length', 'max' => 1),
            array('tel_cliente', 'length', 'max' => 60),
            array('ddd_telCliente', 'length', 'max' => 5),
            array('ins_estadualCliente', 'length', 'max' => 50),
            array('cnae_principal', 'length', 'max' => 20),
            array('NFEgrupo_cliente', 'safe'),
            array('email_cliente', 'CEmailValidator'),
            array('documento_cliente', 'application.components.validators.ValidadorDocumento'),
// The following rule is used by search().
            array(
                'IDCliente, nome_cliente, razao_cliente, tipo_pessoaCliente',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDEnderecoCorrespondencia' => array(self::BELONGS_TO, 'EnderecoCliente', 'IDEndereco_correspondencia'),
            'iDEnderecoFiscal' => array(self::BELONGS_TO, 'EnderecoCliente', 'IDEndereco_fiscal'),
            'iDGrupoEconomico' => array(self::BELONGS_TO, 'GrupoEconomico', 'IDGrupo_economico'),
            'responsavelLegals' => array(self::HAS_MANY, 'ResponsavelLegal', 'IDCliente'),
            'grupoEconomicos' => array(self::HAS_MANY, 'GrupoEconomico', 'IDResponsavel_grupoEconomico'),
            'projetos' => array(self::HAS_MANY, 'Projeto', 'IDCliente'),
            'contatos' => array(self::HAS_MANY, 'Contato', 'IDCliente'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDCliente' => 'Cliente',
            'nome_cliente' => 'Nome Fantasia',
            'razao_cliente' => 'Razão Social',
            'tipo_pessoaCliente' => 'Tipo de Pessoa',
            'documento_cliente' => 'CNPJ/CPF',
            'email_cliente' => 'Email Principal',
            'tel_cliente' => 'Telefone Principal',
            'ddd_telCliente' => 'DDD Telefone Principal',
            'IDEndereco_correspondencia' => 'Endereço Correspondência',
            'IDGrupo_economico' => 'Grupo Econômico',
            'IDEndereco_fiscal' => 'Endereço Fiscal',
            'site_cliente' => 'Site do Cliente',
            'ins_estadualCliente' => 'Insc. Estadual',
            'ins_municipalCliente' => 'Insc. Municipal',
            'NFEgrupo_cliente' => 'NFe para o grupo?',
            'cnae_principal' => 'CNAE',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDCliente"', HTexto::tiraLetras($this->IDCliente));
        $criteria->compare('LOWER("nome_cliente")', mb_strtolower($this->nome_cliente), true);
        $criteria->compare('LOWER("razao_cliente")', mb_strtolower($this->razao_cliente), true);
        $criteria->compare('LOWER("tipo_pessoaCliente")', mb_strtolower($this->tipo_pessoaCliente), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDCliente" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Cliente the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public static function getTiposPessoa()
    {
        return array(
            'j' => 'Jurídica',
            'f' => 'Física',
            'p' => 'Pública'
        );
    }

    /**
     * Retorna a String equivalente ao tipo de pessoa
     * @return String tipo de pessoa por extenso
     */
    public function getLabelTipoPessoa()
    {
        return self::getTiposPessoa()[$this->tipo_pessoaCliente];
    }

    public function save($param1 = true, $param2 = null)
    {
        $this->documento_cliente = HTexto::somenteNumeros($this->documento_cliente);
        return parent::save($param1, $param2);
    }

}
