<?php

/**
 * This is the model class for table "public.Item_menu".
 *
 * The followings are the available columns in table 'public.Item_menu':
 * @property integer $IDItem_menu
 * @property string $nome_menu
 * @property string $link_menu
 * @property integer $n_ordemItemMenu
 * @property integer $IDMenu_pai
 * @property integer $IDModulo
 * @property string $icone_itemMenu
 * @property string $tags
 *
 * The followings are the available model relations:
 * @property Acao[] $acaos
 * @property ItemMenu $iDMenuPai
 * @property ItemMenu[] $itemMenus
 * @property Modulo $iDModulo
 * @property Dica[] $dicas
 * @property Ajuda[] $ajudas
 * @property TagItemMenu[] $tagItemMenus
 * @package base.Models
 */
class ItemMenu extends ActiveRecord
{
    public $tags;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Item_menu';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome_menu, n_ordemItemMenu', 'required'),
            array('n_ordemItemMenu, IDMenu_pai, IDModulo', 'numerical', 'integerOnly' => true),
            array('nome_menu, link_menu', 'length', 'max' => 255),
            array('icone_itemMenu', 'length', 'max' => 20),
            // The following rule is used by search().
            array(
                'nome_menu, IDMenu_pai, IDModulo',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'acoes' => array(self::HAS_MANY, 'Acao', 'IDItem_menu'),
            'IDMenuPai' => array(self::BELONGS_TO, 'ItemMenu', 'IDMenu_pai', 'order' => 'nome_menu ASC'),
            'itensMenu' => array(self::HAS_MANY, 'ItemMenu', 'IDMenu_pai', 'order' => 'nome_menu ASC'),
            'IDModulo' => array(self::BELONGS_TO, 'Modulo', 'IDModulo', 'order' => 'nome_modulo ASC'),
            'dicas' => array(self::HAS_MANY, 'Dica', 'IDItem_menu'),
            'ajudas' => array(self::HAS_MANY, 'Ajuda', 'IDItem_menu'),
            'tagItemMenus' => array(self::HAS_MANY, 'TagItemMenu', 'IDItemMenu'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDItem_menu' => 'Item de Menu',
            'nome_menu' => 'Nome do Item',
            'link_menu' => 'Link do Item',
            'n_ordemItemMenu' => 'Nº Ordem Item Menu',
            'IDMenu_pai' => 'Item de Menu Pai',
            'IDModulo' => 'IDModulo',
            'icone_itemMenu' => 'Ícone do Item',
            'tags' => 'Tags'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('LOWER("nome_menu")', mb_strtolower($this->nome_menu), true);
        $criteria->compare('("IDMenu_pai")', mb_strtolower($this->IDMenu_pai));
        $criteria->compare('("IDModulo")', mb_strtolower($this->IDModulo));

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDItem_menu" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ItemMenu the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * devolve a tag <a> do link descrito no item
     * @param string $text Texto para aparecer na url
     * @param array $htmlOptions Opções de HTML no estilo Html::link
     * @return string tag <a> se houver link, caso contrário o nome_menu
     */
    public function getLink($text = '', $htmlOptions = array())
    {
        if ($text == null) {
            $text = $this->nome_menu;
        }
        $url = $this->getUrl();
        if ($url) {
            return Html::link($text, $url, $htmlOptions);
        } else {
            return $this->nome_menu;
        }
    }

    /**
     * Retorna a Url um item de menu
     * @return String link (index.php?r...) se existir, falso caso contrário
     */
    public function getUrl()
    {
        if ($this->link_menu != '') {
            $superModulo = $this->IDModulo()->slug_modulo;
            return '/index.php?r=' . $superModulo . $this->link_menu;
        } else {
            return false;
        }
    }


    public function ehFavorito()
    {
        if ($this->link_menu != null) {
            $f = FavoritoUsuario::model()->findByPk(
                array(
                    'IDUsuario' => Yii::app()->user->IDUsuario,
                    'IDItemMenu' => $this->IDItem_menu
                )
            );
            if ($f == null) {
                return false;
            }
            return true;
        }
        return false;
    }

    public function atualizaTagItemMenu($IDTagItemMenus)
    {
        $antigas = TagItemMenu::model()->findAllByAttributes(
            array('IDItemMenu' => $this->IDItem_menu)
        );

        $antigas = array_keys(Html::listData($antigas, 'valor_tagItemMenu', ''));
        $excluirEfeito = array_diff($antigas, $IDTagItemMenus);
        $addEfeito = array_diff($IDTagItemMenus, $antigas);


        foreach ($excluirEfeito as $excluir) {
            $r = TagItemMenu::model()->findByAttributes(
                array("valor_tagItemMenu" => $excluir, 'IDItemMenu' => $this->IDItem_menu)
            );
            if ($r != null) {
                $r->delete();
            }
        }

        foreach ($addEfeito as $add) {
            $a = new TagItemMenu();
            $a->IDItemMenu = $this->IDItem_menu;
            $a->valor_tagItemMenu = $add;
            $a->save();
        }
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->tags = implode(',', $this->tagItemMenus);
    }
}
