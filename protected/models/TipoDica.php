<?php

/**
 * This is the model class for table "public.Tipo_dica".
 *
 * The followings are the available columns in table 'public.Tipo_dica':
 * @property integer $IDTipo_dica
 * @property string $nome_tipoDica
 * @property string $imagem_tipoDica
 *
 * The followings are the available model relations:
 * @property Dica[] $dicas
 * @package base.Models
 */
class TipoDica extends ActiveRecord
{
    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Tipo_dica';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome_tipoDica, imagem_tipoDica', 'required'),
            array('nome_tipoDica, imagem_tipoDica', 'length', 'max' => 255),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'dicas' => array(self::HAS_MANY, 'Dica', 'IDTipo_dica'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDTipo_dica' => 'Tipo de Dica',
            'nome_tipoDica' => 'Nome Tipo Dica',
            'imagem_tipoDica' => 'Imagem Tipo Dica',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TipoDica the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
