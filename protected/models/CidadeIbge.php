<?php

/**
 * This is the model class for table "public.Cidade_ibge".
 *
 * The followings are the available columns in table 'public.Cidade_ibge':
 * @property integer $IDCidade_ibge
 * @property string $nome_cidadeIbge
 * @property integer $IDEstado_cidade
 *
 * The followings are the available model relations:
 * @property Endereco[] $enderecos
 * @property Estado $iDEstadoCidade
 * @package base.Models
 */
class CidadeIbge extends ActiveRecord
{


    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Cidade_ibge';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDCidade_ibge, nome_cidadeIbge, IDEstado_cidade', 'required'),
            array('IDCidade_ibge, IDEstado_cidade', 'numerical', 'integerOnly' => true),
            array('nome_cidadeIbge', 'length', 'max' => 255),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'enderecos' => array(self::HAS_MANY, 'Endereco', 'IDCidade'),
            'iDEstadoCidade' => array(self::BELONGS_TO, 'Estado', 'IDEstado_cidade'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDCidade_ibge' => 'Cidade',
            'nome_cidadeIbge' => 'Nome da Cidade',
            'IDEstado_cidade' => 'IDEstado',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CidadeIbge the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getLabelCidade()
    {
        return $this->nome_cidadeIbge;
    }

    public function __toString()
    {
        return $this->getLabelCidade();
    }

}
