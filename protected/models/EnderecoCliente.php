<?php

/**
 * This is the model class for table "Endereco".
 *
 * The followings are the available columns in table 'Endereco':
 * @property integer $IDEndereco
 * @property string $logradouro_endereco
 * @property string $cep_endereco
 * @property string $numero_endereco
 * @property string $complemento_endereco
 * @property string $referencia_endereco
 * @property integer $IDCidade
 * @property integer $IDPais
 * @property string $bairro_endereco
 *
 * The followings are the available model relations:
 * @property Cliente[] $clientes
 * @property Cliente[] $clientes1
 * @property CidadeIbge $iDCidade
 * @property PaisIbge $iDPais
 * @package base.Models
 */
class EnderecoCliente extends Endereco
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Endereco';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('logradouro_endereco, IDCidade, IDPais, bairro_endereco', 'required'),
            array('IDCidade, IDPais', 'numerical', 'integerOnly' => true),
            array('logradouro_endereco, referencia_endereco, bairro_endereco', 'length', 'max' => 255),
            array('cep_endereco', 'length', 'max' => 30),
            array('numero_endereco', 'length', 'max' => 10),
            array('complemento_endereco', 'length', 'max' => 100),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'clientes' => array(self::HAS_MANY, 'Cliente', 'IDEndereco_correspondencia'),
            'clientes1' => array(self::HAS_MANY, 'Cliente', 'IDEndereco_fiscal'),
            'iDCidade' => array(self::BELONGS_TO, 'CidadeIbge', 'IDCidade'),
            'iDPais' => array(self::BELONGS_TO, 'PaisIbge', 'IDPais'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDEndereco' => 'IDEndereco',
            'logradouro_endereco' => 'Logradouro',
            'cep_endereco' => 'CEP',
            'numero_endereco' => 'Número',
            'complemento_endereco' => 'Complemento',
            'referencia_endereco' => 'Ponto de Referência',
            'IDCidade' => 'Cidade',
            'IDPais' => 'País',
            'bairro_endereco' => 'Bairro',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EnderecoCliente the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function __toString()
    {
        $endereco = $this->logradouro_endereco . ' ' . $this->numero_endereco . ', ' . $this->complemento_endereco;
        $cidade = $this->iDCidade;
        $estado = $cidade->iDEstadoCidade;
        $endereco .= ' - ' . $cidade->nome_cidadeIbge . '/' . $estado->sigla_estado;
        $endereco .= $this->referencia_endereco != '' ? '<br /><i>' . $this->referencia_endereco . '</i>' : '';
        return $endereco;
    }

}
