<?php

/**
 * This is the model class for table "drink.Log".
 *
 * The followings are the available columns in table 'drink.Log':
 * @property integer $IDLog
 * @property integer $IDAcao
 * @property string $dt_log
 * @property integer $IDUsuario
 * @property string $data_log
 *
 * The followings are the available model relations:
 * @property Acao $iDAcao
 * @property Usuario $iDUsuario
 * @package base.Models
 */
class Log extends ActiveRecord
{

    public $inicioPeriodoLog;
    public $fimPeriodoLog;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Log';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDAcao, dt_log, IDUsuario', 'required'),
            array('IDAcao, IDUsuario', 'numerical', 'integerOnly' => true),
            array('fimPeriodoLog, inicioPeriodoLog', 'safe'),
            array('fimPeriodoLog, inicioPeriodoLog', 'length', 'max' => '225'),
            array(
                'IDAcao, dt_log, IDUsuario, data_log, id_dataLog',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDAcao' => array(self::BELONGS_TO, 'Acao', 'IDAcao'),
            'iDUsuario' => array(self::BELONGS_TO, 'Usuario', 'IDUsuario'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDLog' => 'Log',
            'IDAcao' => 'Ação Executada',
            'dt_log' => 'Data da Execução',
            'IDUsuario' => 'Usuário Executor',
            'data_log' => 'Dados envolvidos na transação',
            'id_dataLog' => 'Provável ID da Entidade Envolvida',
            'dt_inicioPeriodoLog' => 'Período do Log',
            'dt_fimPeriodoLog' => 'Fim do Período do Log',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDAcao"', HTexto::tiraLetras($this->IDAcao));
        $criteria->compare('"IDUsuario"', $this->IDUsuario);
        $criteria->compare('("id_dataLog")', ($this->id_dataLog));
        if ($this->inicioPeriodoLog != '') {
            $criteria->addBetweenCondition('"dt_log"', $this->inicioPeriodoLog, $this->fimPeriodoLog);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDLog" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Log the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function registrarLog($idAcao)
    {
        if (count($_POST) > 0 && !isset($_POST['ajax']) && isset(YII::app()->user->IDUsuario)) {
            $log = new Log;
            $log->IDUsuario = YII::app()->user->IDUsuario;
            $log->IDAcao = $idAcao;
            $data = base64_encode(gzdeflate(CJSON::encode($_POST)));
            $log->dt_log = new CDbExpression('CURRENT_TIMESTAMP(0)');
            $log->data_log = $data;
            $log->id_dataLog = Log::getPossivelID(Yii::app()->request->requestUri);
            if (!$log->save()) { //se o salvamento não der certo, tenta sem o data_log
                $log->data_log = null;
                if (!$log->save()) { //se o log não der certo de novo, tenta sem o id_dataLog
                    $log->id_dataLog = null;
                    $log->save(); // senão der certo, desiste
                }
            }
        }
    }


    public static function getPossivelID($uri)
    {
        $paramId = '&id=';
        $end = '&';
        $data = explode($paramId, $uri);
        if (!isset($data[1])) {
            return null;
        }
        $chunk = explode($end, $data[1]);
        if (isset($chunk[0])) {
            return $chunk[0];
        } else {
            return null;
        }
    }

    public function getDataLog()
    {
        if ($this->data_log) {
            return (CJSON::decode(gzinflate(base64_decode(stream_get_contents($this->data_log)))));
        } else {
            return array();
        }
    }

}
