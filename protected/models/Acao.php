<?php

/**
 * This is the model class for table "public.Acao".
 *
 * The followings are the available columns in table 'public.Acao':
 * @property integer $IDAcao
 * @property string $nome_acao
 * @property string $desc_acao
 * @property integer $IDItem_menu
 *
 * The followings are the available model relations:
 * @property ItemMenu $iDItemMenu
 * @property Permissao $IDPermissao
 * @package base.Models
 */
class Acao extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Acao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome_acao, IDItem_menu', 'required'),
            array('IDItem_menu', 'numerical', 'integerOnly' => true),
            array('nome_acao, desc_acao', 'length', 'max' => 255),
            // The following rule is used by search().
            array('IDAcao, nome_acao, desc_acao, IDItem_menu', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDItemMenu' => array(self::BELONGS_TO, 'ItemMenu', 'IDItem_menu', 'order' => 'nome_menu ASC'),
            'IDPermissoes' => array(self::HAS_MANY, 'Permissao', 'IDAcao'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */

    public function attributeLabels()
    {
        return array(
            'IDAcao' => 'Ação',
            'nome_acao' => 'Nome da Ação',
            'desc_acao' => 'Descrição da Ação',
            'IDItem_menu' => 'Item de Menu',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->compare('"IDAcao"', HTexto::tiraLetras($this->IDAcao));
        $criteria->compare('LOWER("nome_acao")', mb_strtolower($this->nome_acao), true);
        $criteria->compare('LOWER("desc_acao")', mb_strtolower($this->desc_acao), true);
        $criteria->compare('"IDItem_menu"', $this->IDItem_menu);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDAcao" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Acao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    function getNomeItemMenu()
    {
        return $this->iDItemMenu->nome_menu;
    }

    function getNomeModulo()
    {
        return $this->iDItemMenu->IDModulo()->nome_modulo;
    }


    public function scopes()
    {
        return array(
            'ordemAlfabetica' => array(
                'order' => 'nome_acao ASC'
            )
        );
    }


    public function getLabelAcao()
    {
        return $this->nome_acao;
    }

    public function __toString()
    {
        return $this->getLabelAcao();
    }
}
