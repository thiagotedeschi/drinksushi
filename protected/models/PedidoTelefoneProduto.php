<?php

/**
* This is the model class for table "drink.PedidoTelefone_Produto".
*
* The followings are the available columns in table 'drink.PedidoTelefone_Produto':
    * @property integer $IDPedido
    * @property integer $IDProduto
    * @property integer $quantidade
    * @property string $preco_custo
    * @property string $preco_venda
    * @property string $total
    *
    * The followings are the available model relations:
            * @property PedidoTelefone $iDPedido
            * @property Produto $iDProduto
    * @package base.Models
*/
class PedidoTelefoneProduto extends ActiveRecord
{

/**
* Retorna o nome da tabela representada pelo Modelo.
*
* @return string nome da tabela
*/
public function tableName()
{
return CLIENTE.'.PedidoTelefone_Produto';
}

/**
* Retorna as regras de validação para o Modelo
* @return Array Regras de Validação.
*/
public function rules()
{
return array(
    array('IDPedido, IDProduto, quantidade', 'required'),
    array('IDPedido, IDProduto, quantidade', 'numerical', 'integerOnly'=>true),
    array('preco_custo, preco_venda, total', 'length', 'max'=>11),
// @todo Please remove those attributes that should not be searched.
array('IDPedido, IDProduto, quantidade, preco_custo, preco_venda, total', 'safe', 'on'=>'search'),
);
}

/**
* Retorna as relações do modelo
* @return Array relações
*/
public function relations()
{
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
return array(
    'iDPedido' => array(self::BELONGS_TO, 'PedidoTelefone', 'IDPedido'),
    'iDProduto' => array(self::BELONGS_TO, 'Produto', 'IDProduto'),
);
}

/**
* Retorna as labels dos atributos do modelo no formato (atributo=>label)
* @return Array labels dos atributos.
*/
public function attributeLabels()
{
return array(
    'IDPedido' => 'Idpedido',
    'IDProduto' => 'Idproduto',
    'quantidade' => 'Quantidade',
    'preco_custo' => 'Preco Custo',
    'preco_venda' => 'Preco Venda',
    'total' => 'Total',
);
}

/**
* Retorna uma lista de modelos baseada nas definições de filtro da tabela
* @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
*/
public function search()
{
// @todo Please modify the following code to remove attributes that should not be searched.

$criteria=new CDbCriteria;

		$criteria->compare('"IDPedido"',$this->IDPedido);
		$criteria->compare('"IDProduto"',$this->IDProduto);
		$criteria->compare('"quantidade"',$this->quantidade);
		$criteria->compare('LOWER("preco_custo")',mb_strtolower($this->preco_custo),true);
		$criteria->compare('LOWER("preco_venda")',mb_strtolower($this->preco_venda),true);
		$criteria->compare('LOWER("total")',mb_strtolower($this->total),true);

return new CActiveDataProvider($this, array(
'criteria'=>$criteria,
'Pagination' => array(
'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
),
));
}

/**
* Returns the static model of the specified AR class.
* Please note that you should have this exact method in all your CActiveRecord descendants!
* @param string $className active record class name.
* @return PedidoTelefoneProduto the static model class
*/
public static function model($className=__CLASS__)
{
return parent::model($className);
}
}
