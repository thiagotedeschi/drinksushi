<?php

/**
 * This is the model class for table "public.Estado".
 *
 * The followings are the available columns in table 'public.Estado':
 * @property integer $IDEstado
 * @property string $nome_estado
 * @property string $sigla_estado
 *
 * The followings are the available model relations:
 * @property CidadeIbge[] $cidadeIbges
 * @package base.Models
 */
class Estado extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Estado';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDEstado, nome_estado, sigla_estado', 'required'),
            array('IDEstado', 'numerical', 'integerOnly' => true),
            array('nome_estado', 'length', 'max' => 30),
            array('sigla_estado', 'length', 'max' => 2),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'cidadeIbges' => array(self::HAS_MANY, 'CidadeIbge', 'IDEstado_cidade'),
        );
    }


    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDEstado' => 'Estado',
            'nome_estado' => 'Nome Estado',
            'sigla_estado' => 'Sigla Estado',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('"IDEstado"', HTexto::tiraLetras($this->IDEstado));
        $criteria->compare('LOWER("nome_estado")', mb_strtolower($this->nome_estado), true);
        $criteria->compare('LOWER("sigla_estado")', mb_strtolower($this->sigla_estado), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDEstado" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Estado the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getEstado($IDEstado)
    {
        $estado = null;
        if (!empty($IDEstado)) {
            $criteria = new CDbCriteria();
            $criteria->compare('"IDEstado"', HTexto::tiraLetras($IDEstado));
            $estado = Estado::model()->find($criteria)->sigla_estado;
        }
        return $estado;
    }

}
