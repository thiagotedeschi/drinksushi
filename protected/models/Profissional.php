<?php

/**
 * This is the model class for table "drink.Profissional".
 *
 * The followings are the available columns in table 'drink.Profissional':
 * @property integer $IDProfissional
 * @property string $nome_profissional
 * @property string $genero_profissional
 * @property string $cpf_profissional
 * @property string $rg_profissional
 * @property string $todas_empresasProfissional
 * @property string $org_classeProfissional
 * @property string $n_registroProfissional
 * @property string $nit_profissional
 * @property string $agencia_profissional
 * @property string $conta_profissional
 * @property string $variacao
 * @property integer $IDBanco
 * @property integer $IDProfissao
 * @property integer $IDEndereco
 * @property string $inscricao_mteProfissional
 * @property string $telefone_profissional
 * @property string $ramal_profissional
 * @property string $celular_profissional
 * @property string $email_profissional
 * @property string $ddd_telefoneProfissional
 * @property string $ddd_celularProfissional
 * @property boolean $receber_emails
 * @property array $genero
 * @property boolean $ativo_profissional
 *
 * The followings are the available model relations:
 * @property Notificacao[] $notificacoes
 * @property SalarioProfissional[] $salarioProfissionals
 * @property LocalProfissionalHorario[] $localProfissionalHorarios
 * @property Banco $iDBanco
 * @property Profissao $iDProfissao
 * @property Endereco $iDEndereco
 * @property Usuario $iDUsuario
 * @property ProfissionalEmpregador[] $profissionalEmpregadors
 * @property TipoDocumentoVigencia[] $responsavelTipoDocumentoVigencias
 * @property TipoDocumentoVigencia[] $tipoDocumentoVigencias
 * @property ExameClinico[] $exameClinicos
 * @package base.Models
 */
class Profissional extends ActiveRecord
{
    /*Nome da profissao, utilizada principalmente na search
     * @var $nome_profissao
     * */
    public $nome_profissao;

    public $genero = array('M' => 'Masculino', 'F' => 'Feminino');

    public $classesPerfil = array('M' => 'male-profile', 'F' => 'female-profile');

    /**
     * @var $ativo boolean informa se o Profissional está ativo ou não
     */
    public $ativo;

    public function afterFind()
    {
        parent::afterFind();
        $this->ativo = $this->estaAtivo();
    }

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Profissional';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array(
                'nome_profissional, genero_profissional, email_profissional, cpf_profissional',
                'required'
            ),
            array('email_profissional', 'unique'),
            array('IDProfissao', 'required', 'on' => 'search'),
            array('IDBanco, IDProfissao, IDEndereco', 'numerical', 'integerOnly' => true),
//            array('todas_empresasProfissional', 'numerical', 'integerOnly' => true, 'except' => 'profile'),
            array('nome_profissional', 'length', 'max' => 255),
            array('genero_profissional', 'length', 'max' => 1),
            array('cpf_profissional', 'length', 'max' => 14),
            array('rg_profissional, org_classeProfissional, inscricao_mteProfissional', 'length', 'max' => 15),
            array('n_registroProfissional, agencia_profissional, conta_profissional', 'length', 'max' => 10),
            array('variacao, ddd_telefoneProfissional, ddd_celularProfissional', 'length', 'max' => 3),
            array('telefone_profissional, celular_profissional', 'length', 'max' => 20),
            array('ramal_profissional', 'length', 'max' => 5),
            array('email_profissional', 'email'),
            array('email_profissional', 'length', 'max' => 60),
            array('receber_emails, IDProfissao, ativo_profissional', 'safe'),
            array('cpf_profissional', 'application.components.validators.ValidadorDocumento'),
            // The following rule is used by search().
            array(
                'IDProfissional, nome_profissional, cpf_profissional, nome_profissao, ativo_profissional',
                'safe',
                'on' => 'search'
            ),
            array(
                'nit_profissional, inscricao_mteProfissional, org_classeProfissional, n_registroProfissional, uf_registro_Profissional, profissionalEmpregadors',
                'required',
                'on' => 'dadosProfissao'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'notificacoes' => array(self::HAS_MANY, 'Notificacao', 'IDProfissional'),
            'salarioProfissionals' => array(self::HAS_MANY, 'SalarioProfissional', 'IDProfissional'),
            'localProfissionalHorarios' => array(self::HAS_MANY, 'LocalProfissionalHorario', 'IDProfissional'),
            'iDBanco' => array(self::BELONGS_TO, 'Banco', 'IDBanco'),
            'iDProfissao' => array(self::BELONGS_TO, 'Profissao', 'IDProfissao'),
            'iDEndereco' => array(self::BELONGS_TO, 'EnderecoProfissional', 'IDEndereco'),
            'iDUsuario' => array(self::HAS_ONE, 'Usuario', 'IDProfissional'),
            'profissionalEmpregadors' => array(self::HAS_MANY, 'ProfissionalEmpregador', 'IDProfissional'),
            'responsavelTipoDocumentoVigencias' => array(
                self::MANY_MANY,
                'TipoDocumentoVigencia',
                CLIENTE . '.Responsavel_Vigencia_Documento(IDProfissional, IDTipoDocumentoVigencia)'
            ),
            'tipoDocumentoVigencias' => array(self::HAS_MANY, 'TipoDocumentoVigencia', 'IDProfissionalCoordernador'),
            'exameClinicos' => array(self::HAS_MANY, 'ExameClinico', 'IDMedico'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDProfissional' => 'Profissional',
            'nome_profissional' => 'Nome',
            'genero_profissional' => 'Gênero',
            'cpf_profissional' => 'CPF',
            'rg_profissional' => 'RG',
            'org_classeProfissional' => 'Org. Classe',
            'n_registroProfissional' => 'N° Registro',
            'agencia_profissional' => 'Agência Bancária',
            'conta_profissional' => 'Conta Bancária',
            'variacao' => 'Variação Poupança',
            'IDBanco' => 'Banco',
            'IDProfissao' => 'Profissão',
            'IDEndereco' => 'Endereço',
            'inscricao_mteProfissional' => 'Insc. MTE',
            'telefone_profissional' => 'Telefone',
            'ramal_profissional' => 'Ramal',
            'celular_profissional' => 'Celular',
            'email_profissional' => 'Email',
            'ddd_telefoneProfissional' => 'DDD Telefone',
            'ddd_celularProfissional' => 'DDD Celular',
            'receber_emails' => 'Receber Emails?',
            'nit_profissional' => 'NIT Profissional',
            'profissionalEmpregadors' => 'Empregador',
            'todas_empresasProfissional' => 'Todas as Empresas',
            'nome_profissao' => 'Profissão',
            'ativo_profissional' => 'Profissional Ativo?',
            'uf_rg_Profissinal' => 'UF do RG',
            'orgao_expedidor_Profissional' => 'Orgão Expedidor',
            'uf_registro_Profissional' => 'UF do Registro'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->with = array('iDProfissao');

        $criteria->compare('"IDProfissional"', HTexto::tiraLetras($this->IDProfissional));
        $criteria->compare('LOWER("nome_profissional")', mb_strtolower($this->nome_profissional), true);
        $criteria->compare(
            'LOWER("cpf_profissional")',
            HTexto::somenteNumeros(mb_strtolower($this->cpf_profissional)),
            true
        );
        $criteria->compare('LOWER("iDProfissao"."nome_profissao")', mb_strtolower($this->nome_profissao), true);
        $criteria->compare('"t"."ativo_profissional"', $this->ativo_profissional);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDProfissional" DESC',
                'attributes' => array(
                    'nome_profissao' => array(
                        'asc' => '"iDProfissao"."nome_profissao"',
                        'desc' => '"iDProfissao"."nome_profissao" DESC',
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Profissional the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function findDesassociados($newRecord = false, $IDProfissional = '')
    {
        $profs = $this->findAll(
            array('condition' => '"IDProfissional" NOT IN (SELECT "' . CLIENTE . '"."Usuario"."IDProfissional" FROM "' . CLIENTE . '"."Usuario")')
        );
        if (!$newRecord) //se o usuário já existe, talvez queiramos colocar o profissional dele nessa lista
        {
            $profs[] = $this->findByPk($IDProfissional);
        }

        if ($profs == null) {
            return array();
        }
        return $profs;
    }

    /**
     * Sobreescrevendo o método base de inserção no banco de dados para sempre limpar e preparar o cpf do profissional antes de colocá-lo no banco de dados
     * @param boolean $runValidation se deve-se ou não rodar a validação do Model antes de salvar
     * @param array $attributes lista de atributos que devem tentar serem salvos/alterados
     */
    public function save($runValidation = true, $attributes = null)
    {
        $this->cpf_profissional = HTexto::somenteNumeros($this->cpf_profissional);
        return parent::save($runValidation, $attributes);
    }

    public function getLabelEmpregadores()
    {
        $empregadores = Empregador::model()->findAllByProfissional($this->IDProfissional)->findAll();
        $labels = array();
        foreach ($empregadores as $empregador) {
            $labels[$empregador->IDEmpregador] = $empregador->getLabelEmpregador();
        }
        return $labels;
    }

    public static function temEmpregador($IDEmpregador)
    {
        if (Yii::app()->user->getState('todas_empresas')) {
            return true;
        } else {
            $empregadores = Yii::app()->session['empregadores'];
            foreach ($empregadores as $empregadorID => $label) {
                if ($empregadorID == $IDEmpregador) {
                    return true;
                }
            }
        }
        return false;
    }

    public function beforeDelete()
    {
        $return = true;
        $Vigencia = $this->vigenciaAbertas();
        $Exames = $this->exameAbertos();
        $mensagem = [];

        $usuario = Usuario::model()->findByAttributes(['IDProfissional' => $this->IDProfissional]);
        if (!empty($Vigencia) || !empty($Exames)) {
            $texto = '<a href="' . Yii::app()->createUrl(
                    "/Profissional/Profissional/create"
                ) . '" target="_blank">Não é possível excluir <b>' . $this->nome_profissional . '</b>, pois o Profissional possui ';
            $texto .= empty($Vigencia)
                ? (empty($Exames) ? : count($Exames) . ((count($Exames) > 1) ? ' exames' : ' exame'))
                : count($Vigencia) .
                ((count($Vigencia) > 1) ? ' documentos' : ' documento') .
                (empty($Exames)
                    ? ''
                    : ' e ' . count($Exames) . ((count($Exames) > 1) ? ' exames' : ' exame'));
            $texto .= ' em sua responsabilidade. Para visualizar click aqui!</a>';
            $mensagem[] = $texto;
            $return = false;
        } elseif ($usuario) {
            if ($usuario->login_usuario == Yii::app()->user->getState('login_usuario')) {
                $mensagem[] = 'Você não pode excluir o seu próprio Profissional!';
            } else {
                $mensagem[] = 'Você não pode excluir o Profissional <b>' . $this->nome_profissional . ',</b> pois ele está associado ao usuário <b>' . $usuario->login_usuario . '</b>!';
            }
            $return = false;
        }

        Yii::app()->user->setFlash('error', $mensagem);

        if ($return) {
            return parent::beforeDelete();
        } else {
            return false;
        }
    }

    public function findAllByResponsabilidade($IDResponsabilidade)
    {
        $criteria = new CDbCriteria;
        $criteria->join = 'INNER JOIN ' . CLIENTE . '."Profissao" "p" ON "p"."IDProfissao" = "t"."IDProfissao"';
        $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Responsabilidade_Profissao" "rp" ON "p"."IDProfissao" = "rp"."IDProfissao"';
        $criteria->compare('"rp"."IDResponsabilidade"', $IDResponsabilidade);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }


    public function findAllByEmpregador($IDEmpregador, $ativos = true)
    {
        $criteria = new CDbCriteria();
        $criteria->distinct = true;
        $criteria->join = 'LEFT JOIN ' . CLIENTE . '."Profissional_Empregador" pe on pe."IDProfissional" = t."IDProfissional"';
        $criteria->addCondition('t."todas_empresasProfissional" = true OR pe."IDEmpregador"=' . $IDEmpregador);
        if ($ativos) {
            $criteria->addCondition('pe."dt_desativacao" IS NULL');
            $criteria->addCondition(
                '((dt_fim IS NULL) OR (dt_fim > now())) AND ((dt_inicio IS NULL) OR (dt_inicio < now()))'
            );
        }
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function getLabelProfissional($regClass = false)
    {
        $label = $this->nome_profissional;
        if ($regClass) {
            $label .= ' ' . $this->getRegistroClasse();
        }
        return $label;
    }


    public function getRegistroClasse()
    {
        return $this->org_classeProfissional . ' ' . $this->n_registroProfissional;
    }



    public function getLabelMedico()
    {
        if ($this->genero_profissional == 'M') {
            $tit = 'Dr. ';
        } else {
            $tit = 'Dra. ';
        }
        return $tit . $this->getLabelProfissional(true);
    }


    public function __toString()
    {
        return $this->nome_profissional;
    }

    public function getClassePerfil()
    {
        return $this->classesPerfil[$this->genero_profissional];
    }

    public function realizaExameClinico()
    {
        $listaPermitidos = Profissional::model()->findAllByResponsabilidade(25)->findAll();
        foreach ($listaPermitidos as $prof) {
            if ($prof->IDProfissional = $this->IDProfissional) {
                return true;
            }
        }
        return false;

    }

    public function findAllByProfissionalAtivo()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('"t"."ativo_profissional"', true);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }


    public function estaAtivo()
    {
        if ($this->ativo_profissional) {
            return true;
        } else {
            return false;
        }
    }

    public function getLabelAcaoDesativar()
    {
        if ($this->estaAtivo()) {
            return 'Desativar';
        } else {
            return 'Ativar';
        }
    }


    public function vigenciaAbertas()
    {
        $vigenciasAbertas = [];
        foreach ($this->tipoDocumentoVigencias as $tipoVigencia) {
            if (!$tipoVigencia->iDVigencia->vigenciaFinalizadaPPRA()) {
                $vigenciasAbertas[] = $tipoVigencia->iDVigencia->iDPPRA;
            }
            if (!$tipoVigencia->iDVigencia->vigenciaFinalizadaPCMSO()) {
                $vigenciasAbertas[] = $tipoVigencia->iDVigencia->iDPCMSO;
            }
        }
        foreach ($this->responsavelTipoDocumentoVigencias as $tipoVigencia) {
            if (!$tipoVigencia->iDVigencia->vigenciaFinalizadaPPRA()) {
                $vigenciasAbertas[] = $tipoVigencia->iDVigencia->iDPPRA;
            }
            if (!$tipoVigencia->iDVigencia->vigenciaFinalizadaPCMSO()) {
                $vigenciasAbertas[] = $tipoVigencia->iDVigencia->iDPCMSO;
            }
        }

        return $vigenciasAbertas;
    }

    public function exameAbertos()
    {
        $examesAbertos = [];
        foreach ($this->exameClinicos as $exame) {
            if (!$exame->exameClinicoFinalizado()) {
                $examesAbertos[] = $exame;
            }
        }
        return $examesAbertos;
    }

}