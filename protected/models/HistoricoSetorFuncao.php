<?php

/**
 * This is the model class for table "drink.Historico_Setor_Funcao".
 *
 * The followings are the available columns in table 'drink.Historico_Setor_Funcao':
 * @property integer $IDHistoricoSetorFuncao
 * @property string $dt_correnteHistoricoSF
 * @property string $dt_modificacaoHistoricoSF
 * @property string $motivo_historicoSF
 * @property integer $IDSetorFuncao
 * @property boolean $ativacao_historicoSF
 * @property integer $IDUsuarioResponsavel
 *
 * The followings are the available model relations:
 * @property SetorFuncao $iDSetorFuncao
 * @property Usuario $iDUsuarioResponsavel
 * @package base.Models
 */
class HistoricoSetorFuncao extends ActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return CLIENTE . '.Historico_Setor_Funcao';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('dt_modificacaoHistoricoSF, motivo_historicoSF, IDUsuarioResponsavel', 'required'),
            array('IDUsuarioResponsavel', 'numerical', 'integerOnly' => true),
            array('motivo_historicoSF', 'length', 'max' => 255),
            array('ativacao_historicoSF', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDSetorFuncao' => array(self::BELONGS_TO, 'SetorFuncao', 'IDSetorFuncao'),
            'iDUsuarioResponsavel' => array(self::BELONGS_TO, 'Usuario', 'IDUsuarioResponsavel'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'IDHistoricoSetorFuncao' => 'Histórico Setor Função',
            'dt_correnteHistoricoSF' => 'Data de Criação do Histórico',
            'dt_modificacaoHistoricoSF' => 'Data de Modificação',
            'motivo_historicoSF' => 'Motivo da Alteração',
            'IDSetorFuncao' => 'Setor x Função',
            'ativacao_historicoSF' => 'Ativado?',
            'IDUsuarioResponsavel' => 'Usuário Responsável',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return HistoricoSetorFuncao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
