<?php

/**
 * This is the model class for table "drink.Salario_Profissional".
 *
 * The followings are the available columns in table 'drink.Salario_Profissional':
 * @property integer $IDSalarioProfissional
 * @property string $dt_inicioSalarioProfissional
 * @property string $dt_fimSalarioProfissional
 * @property string $dt_desativacaoSalarioProfissional
 * @property integer $IDProfissional
 * @property double $valor_salarioProfissional
 *
 * The followings are the available model relations:
 * @property Profissional $iDProfissional
 * @property RemuneracaoSalario[] $remuneracaoSalarios
 * @property Adicional[] drink.Adicionals
 * @package base.Models
 */
class SalarioProfissional extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Salario_Profissional';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('dt_inicioSalarioProfissional, IDProfissional, valor_salarioProfissional', 'required'),
            array('IDProfissional', 'numerical', 'integerOnly' => true),
//            array('valor_salarioProfissional', 'numerical'),
            array('dt_fimSalarioProfissional, dt_desativacaoSalarioProfissional', 'safe'),
// The following rule is used by search().
            array(
                'IDSalarioProfissional, dt_inicioSalarioProfissional, dt_fimSalarioProfissional, valor_salarioProfissional',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    /**
     * @return array relational rules.
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDProfissional' => array(self::BELONGS_TO, 'Profissional', 'IDProfissional'),
            'remuneracaoSalarios' => array(self::HAS_MANY, 'RemuneracaoSalario', 'IDSalario'),
            'Adicionals' => array(
                self::MANY_MANY,
                'Adicional',
                CLIENTE . '.Salario_tem_Adicional(IDSalario, IDAdicional)'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDSalarioProfissional' => 'Salário do Profissional',
            'dt_inicioSalarioProfissional' => 'Data Início Salário Profissional',
            'dt_fimSalarioProfissional' => 'Data Fim Salário Profissional',
            'dt_desativacaoSalarioProfissional' => 'Data Desativação Salário Profissional',
            'IDProfissional' => 'Profissional',
            'valor_salarioProfissional' => 'Valor Salário Profissional',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDSalarioProfissional"', HTexto::tiraLetras($this->IDSalarioProfissional));
        $criteria->compare(
            'LOWER("dt_inicioSalarioProfissional")',
            mb_strtolower($this->dt_inicioSalarioProfissional),
            true
        );
        $criteria->compare('LOWER("dt_fimSalarioProfissional")', mb_strtolower($this->dt_fimSalarioProfissional), true);
        $criteria->compare('"valor_salarioProfissional"', $this->valor_salarioProfissional);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDSalarioProfissional" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SalarioProfissional the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
