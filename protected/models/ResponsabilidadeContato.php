<?php

/**
 * This is the model class for table "drink.Responsabilidade_contato".
 *
 * The followings are the available columns in table 'drink.Responsabilidade_contato':
 * @property integer $IDResponsabilidade_contato
 * @property string $nome_resp
 *
 * The followings are the available model relations:
 * @property Contato[] drink.Contatos
 * @package base.Models
 */
class ResponsabilidadeContato extends ActiveRecord
{

    CONST ID_RESPONSABILIDADE_GERAL = 1;
    CONST ID_RESPONSABILIDADE_FINANCEIRO = 2;
    CONST ID_RESPONSABILIDADE_SEGURANCA = 3;
    CONST ID_RESPONSABILIDADE_RH = 7;
    CONST ID_RESPONSABILIDADE_MEDICINA = 8;
    CONST ID_RESPONSABILIDADE_DP = 9;


    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'Responsabilidade_contato';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome_resp', 'required'),
            array('nome_resp', 'length', 'max' => 255),
            // The following rule is used by search().
            array('IDResponsabilidade_contato, nome_resp', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'Contatos' => array(
                self::MANY_MANY,
                'Contato',
                'Contatos_tem_responsabilidades(IDResponsabilidade_contato, IDContato_Contato)'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDResponsabilidade_contato' => 'Responsabilidade de Contatos',
            'nome_resp' => 'Nome Responsabilidade',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDResponsabilidade_contato"', HTexto::tiraLetras($this->IDResponsabilidade_contato));
        $criteria->compare('LOWER("nome_resp")', mb_strtolower($this->nome_resp), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDResponsabilidade_contato" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ResponsabilidadeContato the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function beforeDelete()
    {
        $return = true;

        $mensagem = [];

        $contatos = ContatosTemResponsabilidades::model()->findAllByAttributes(
            ['IDResponsabilidade_contato' => $this->IDResponsabilidade_contato]
        );
        if (!empty($contatos)) {
            $return = false;
            $mensagem[] = 'Não foi possível excluir a Responsabildade pois está associado a ' .
                count($contatos) . ' Contato' . ((count($contatos) > 1) ? 's' : '') . '.';
        }
        Yii::app()->user->setFlash('error', $mensagem);

        if ($return) {
            return parent::beforeDelete();
        } else {
            return false;
        }
    }
}
