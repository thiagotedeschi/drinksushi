<?php

/**
 * This is the model class for table "drink.Profissional_Observacao_Trabalhador".
 *
 * The followings are the available columns in table 'drink.Profissional_Observacao_Trabalhador':
 * @property integer $IDObservacaoTrabalhador
 * @property integer $IDProfissional
 * @package base.Models
 */
class ProfissionalObservacaoTrabalhador extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Profissional_Observacao_Trabalhador';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDObservacaoTrabalhador, IDProfissional', 'required'),
            array('IDObservacaoTrabalhador, IDProfissional', 'numerical', 'integerOnly' => true),
            array('IDObservacaoTrabalhador, IDProfissional', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array();
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDObservacaoTrabalhador' => 'Idobservacao Trabalhador',
            'IDProfissional' => 'Idprofissional',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDObservacaoTrabalhador"', HTexto::tiraLetras($this->IDObservacaoTrabalhador));
        $criteria->compare('"IDProfissional"', HTexto::tiraLetras($this->IDProfissional));

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDProfissional" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProfissionalObservacaoTrabalhador the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
