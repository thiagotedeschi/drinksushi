<?php

/**
 * This is the model class for table "public.Ajuda".
 *
 * The followings are the available columns in table 'public.Ajuda':
 * @property integer $IDAjuda
 * @property string $texto_ajuda
 * @property string $video_ajuda
 * @property integer $IDAcao
 *
 * The followings are the available model relations:
 * @property Acao $iDAcao
 * @package base.Models
 */
class Ajuda extends ActiveRecord
{

    /* nome da ação, utilizada para pesquisa na search
     * @var $codigo_subversao
     * */
    public $nome_acao;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Ajuda';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('texto_ajuda', 'required'),
            array('IDAcao', 'numerical', 'integerOnly' => true),
            array('video_ajuda', 'length', 'max' => 255),
            array('IDAjuda, texto_ajuda, IDAcao, nome_acao', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        return array(
            'iDAcao' => array(self::BELONGS_TO, 'Acao', 'IDAcao'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDAjuda' => 'Ajuda',
            'texto_ajuda' => 'Texto da Ajuda',
            'video_ajuda' => 'Vídeo da Ajuda',
            'IDAcao' => 'Ação Relacionada',
            'nome_acao' => 'Ação Relacionada',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->with = ['iDAcao'];

        $criteria->compare('"IDAjuda"', HTexto::somenteNumeros($this->IDAjuda));
        $criteria->compare('lower("texto_ajuda")', mb_strtolower($this->texto_ajuda), true);
        $criteria->compare('"IDAcao"', $this->IDAcao);
        $criteria->compare('LOWER("iDAcao"."nome_acao")', mb_strtolower($this->nome_acao), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDAjuda" DESC',
                'attributes' => array(
                    'nome_acao' => array(
                        'asc' => '"iDAcao"."nome_acao"',
                        'desc' => '"iDAcao"."nome_acao" DESC'
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Ajuda the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function geraIframe($width = '270', $height = '135')
    {
        if (!empty($this->video_ajuda)) {
            $iframe = "<iframe width='$width' height='$height' src='" . $this->video_ajuda . "' frameborder='0' allowfullscreen></iframe>";
            return $iframe;
        } else {
            return null;
        }
    }

}

  