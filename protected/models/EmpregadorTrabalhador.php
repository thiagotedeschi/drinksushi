<?php

/**
 * This is the model class for table "drink.Empregador_Trabalhador".
 *
 * The followings are the available columns in table 'drink.Empregador_Trabalhador':
 * @property integer $IDEmpregadorTrabalhador
 * @property integer $IDEmpregador
 * @property integer $IDTrabalhador
 * @property string $dt_cadastro
 * @property string $regime_revezamentoTrabEmp
 * @property string $jornada_trabEmp
 * @property integer $folga_trabEmp
 * @property string $matricula_trabEmp
 * @property string $dt_admissaoTrabEmp
 * @property string $matricula_esocialTrabEmp
 *
 * The followings are the available model relations:
 * @property CipaTrabalhador[] $cipaTrabalhadors
 * @property CAT[] $cATs
 * @property SalarioTrabalhador[] $salarioTrabalhadors
 * @property Empregador $iDEmpregador
 * @property Trabalhador $iDTrabalhador
 * @property TrabalhadorSetorFuncao[] $trabalhadorSetorFuncaos
 * @package base.Models
 */
class EmpregadorTrabalhador extends ActiveRecord
{

    public $exameComplementars;

    public $nome_empregador;

    public $nome_trabalhador;

    public $setorFuncao;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Empregador_Trabalhador';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDEmpregador, IDTrabalhador, dt_admissaoTrabEmp', 'required'),
            array('IDEmpregador, IDTrabalhador', 'numerical', 'integerOnly' => true),
            array('folga_trabEmp', 'numerical', 'integerOnly' => true, 'max' => 6),
            array('regime_revezamentoTrabEmp, jornada_trabEmp, matricula_trabEmp', 'length', 'max' => 20),
            array('matricula_esocialTrabEmp', 'length', 'max' => 35),
            array('dt_cadastro, dt_admissaoTrabEmp, exameComplementars', 'safe'),
            // The following rule is used by search().
            array(
                'nome_empregador,nome_trabalhador,setorFuncao',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'exameClinicos' => array(self::HAS_MANY, 'ExameClinico', 'IDEmpregadorTrabalhador'),
            'laudoDeficiencias' => array(self::HAS_MANY, 'LaudoDeficiencia', 'IDEmpregadorTrabalhador'),
            'resultadoExames' => array(self::HAS_MANY, 'ResultadoExame', 'IDEmpregadorTrabalhador'),
            'pendenciaExameComplementars' => array(
                self::HAS_MANY,
                'PendenciaExameComplementar',
                'IDEmpregadorTrabalhador'
            ),
            'encaminhamentos' => array(self::HAS_MANY, 'Encaminhamento', 'IDTrabalhadorEmpregador'),
            'observacaoTrabalhadors' => array(self::HAS_MANY, 'ObservacaoTrabalhador', 'IDEmpregadorTrabalhador'),
            'cipaTrabalhadors' => array(self::HAS_MANY, 'CipaTrabalhador', 'IDEmpregadorTrabalhador'),
            'cATs' => array(self::HAS_MANY, 'CAT', 'IDEmpregadorTrabalhador'),
            'salarioTrabalhadors' => array(self::HAS_MANY, 'SalarioTrabalhador', 'IDTrabEmp', 'with' => 'recently'),
            'iDEmpregador' => array(
                self::BELONGS_TO,
                'Empregador',
                'IDEmpregador',
            ),
            'trabalhadorSetorFuncaos' => array(self::HAS_MANY, 'TrabalhadorSetorFuncao', 'IDEmpregadorTrabalhador'),
            'iDTrabalhador' => array(self::BELONGS_TO, 'Trabalhador', 'IDTrabalhador'),
        );
    }

    /**
     * Retorna a declaração de escopos nomeados.
     * Um escopo nomeado representa um critério de consulta que podem ser conectadas
     * com outros escopos nomeados e aplicados a uma consulta.
     * @return Array regras de escopo
     */
    public function scopes()
    {
        return array(
            'recently' => array(
                'order' => '"dt_cadastro" DESC',
            )
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDEmpregadorTrabalhador' => 'Empregador/Trabalhador',
            'IDEmpregador' => 'Empregador',
            'IDTrabalhador' => 'Trabalhador',
            'dt_cadastro' => 'Data de Cadastro',
            'regime_revezamentoTrabEmp' => 'Regime Revezamento',
            'jornada_trabEmp' => 'Jornada do Trabalhador',
            'folga_trabEmp' => 'Folga',
            'matricula_trabEmp' => 'Matrícula do Trabalhador',
            'dt_admissaoTrabEmp' => 'Data de Admissão',
            'matricula_esocialTrabEmp' => 'Matrícula do eSocial',
            'exameComplementars' => 'Exames Complementares',
            'iDTrabalhador' => 'Trabalhador',
            'iDEmpregador' => 'Empregador',
            'labelSetorFuncao' => 'Setor/Função',
            'nome_empregador' => 'Empregador',
            'nome_trabalhador' => 'Trabalhador',
            'setorFuncao' => 'Setor/Função',
            'trabalhadorSetorFuncaos' => 'Formulário de cadastro de Setor/Função'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('"IDEmpregadorTrabalhador"', HTexto::tiraLetras($this->IDEmpregadorTrabalhador));
        $criteria->compare('"dt_admissaoTrabEmp"', $this->dt_admissaoTrabEmp);
        if (Yii::app()->user->getState('IDEmpregador')) {

            if (!empty($this->nome_trabalhador)) {

                $criteria->with = array("iDTrabalhador", "iDEmpregador");
                $criteria->compare(
                    'LOWER("iDTrabalhador"."nome_trabalhador")',
                    mb_strtolower($this->nome_trabalhador),
                    true
                );
                $criteria->compare('"iDEmpregador"."IDEmpregador"', Yii::app()->user->getState('IDEmpregador'));
            } else {
                $criteria->with = "iDEmpregador";
                $criteria->compare('"iDEmpregador"."IDEmpregador"', Yii::app()->user->getState('IDEmpregador'));
            }
        } else {
            if (!empty($this->nome_empregador)) {
                $criteria->with = "iDEmpregador";
                $criteria->compare(
                    'LOWER("iDEmpregador"."nome_empregador")',
                    mb_strtolower($this->nome_empregador),
                    true
                );
            }

            if (!empty($this->nome_trabalhador)) {
                $criteria->with = "iDTrabalhador";
                $criteria->compare(
                    'LOWER("iDTrabalhador"."nome_trabalhador")',
                    mb_strtolower($this->nome_trabalhador),
                    true
                );
            }

            if (!empty($this->setorFuncao)) {
                $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Trabalhador_Setor_Funcao" AS tsf ON t."IDEmpregadorTrabalhador" = tsf."IDEmpregadorTrabalhador"';
                $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Setor_Funcao" AS sf ON tsf."IDSetorFuncao" = sf."IDSetorFuncao"';
                $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Setor" AS s ON sf."IDSetor" = s."IDSetor"';
                $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Funcao" AS f ON sf."IDFuncao" = f."IDFuncao"';

                $criteria->compare('LOWER(s."nome_setor")', mb_strtolower($this->setorFuncao), true);
                $criteria->compare('LOWER(f."nome_funcao")', mb_strtolower($this->setorFuncao), true, 'or');
            }
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDEmpregadorTrabalhador" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EmpregadorTrabalhador the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function findAllByEmpregador($emp = null, $filtro = null, $limite = null)
    {
        $criteria = new CDbCriteria;
        $criteria->distinct = true;
        if ($emp !== null) {
            if (is_array($emp)) {
                $criteria->addInCondition('"IDEmpregador"', array_keys($emp));
            } else {
                $criteria->compare('"IDEmpregador"', $emp);
            }
        } else {
            $emp = Yii::app()->user->getState('IDEmpregador');
            if ($emp == null) {
                $criteria->addInCondition('"IDEmpregador"', array_keys(Yii::app()->session['empregadores']));
            } else {
                $criteria->compare('"IDEmpregador"', $emp);
            }
        }
        //filtra por string
        if ($filtro) {
            $criteria->with = 'iDTrabalhador';
            $criteria->addCondition(
                'LOWER("iDTrabalhador"."nome_trabalhador") LIKE \'%' . mb_strtolower($filtro) . '%\''
            );
        }
        $criteria->limit = $limite;
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }


    public function findAllNaoVinculadosEmpregador($emp, $filtro = null, $limite = null)
    {
        $criteria = new CDbCriteria;
        if ($emp !== null) {

            $criteria->addCondition(
                '"IDEmpregador" !=' . $emp
            );

            //filtra por string
            if ($filtro) {
                $criteria->with = 'iDTrabalhador';
                $criteria->addCondition(
                    'LOWER("iDTrabalhador"."nome_trabalhador") LIKE \'%' . mb_strtolower($filtro) . '%\''
                );
            }
            $criteria->limit = $limite;
        }
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function getSetorFuncao()
    {
        $criteria = new CDbCriteria;
        $criteria->join = 'INNER JOIN ' . CLIENTE . '."Trabalhador_Setor_Funcao" AS tsf ON tsf."IDSetorFuncao" = "t"."IDSetorFuncao"';
        $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Empregador_Trabalhador" AS et ON et."IDEmpregadorTrabalhador" = tsf."IDEmpregadorTrabalhador"';
        $criteria->compare('et."IDEmpregadorTrabalhador"', $this->IDEmpregadorTrabalhador);
        $criteria->addCondition('"ativo_setorFuncao"');
        $criteria->addCondition('"dt_inicioTrabSetFunc" <= now()');
        $criteria->addCondition('"dt_fimTrabSetFunc" IS NULL OR "dt_fimTrabSetFunc" > now()');
        $SetorFuncao = SetorFuncao::model()->findAll($criteria);

        //Se a variavel SetorFuncao não for um array, o objeto e colocado em um
        if (is_array($SetorFuncao)) {
            $arr_setorFuncao = $SetorFuncao;
        } else {
            $arr_setorFuncao[] = $SetorFuncao;
        }
        return $arr_setorFuncao;
    }


    public function getTrabalhadorSetorFuncao($mudancaFuncao = true)
    {
        $criteria = new CDbCriteria();
        $criteria->with = array('iDSetorFuncao.iDSetor', 'iDSetorFuncao.iDFuncao');
        $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Setor_Funcao" AS sf ON t."IDSetorFuncao" = sf."IDSetorFuncao"';
        $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Empregador_Trabalhador" AS et ON et."IDEmpregadorTrabalhador" = t."IDEmpregadorTrabalhador"';
        $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Setor" AS s ON "sf"."IDSetor" = s."IDSetor" AND s."IDEmpregador" = et."IDEmpregador"';
        if (!$mudancaFuncao) {
            $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Situacao_Trabalhador" AS st ON "t"."IDTrabalhadorSetorFuncao" = st."IDTrabalhadorSetorFuncao"';
            $criteria->addCondition('st."IDTipoSituacao" !=' . PendenciaExameComplementar::ID_SITUACAO_MUDANCA_FUNCAO);
        }
        $criteria->compare('et."IDEmpregadorTrabalhador"', $this->IDEmpregadorTrabalhador);
        $criteria->addCondition('sf."ativo_setorFuncao"');
        $criteria->addCondition('"dt_inicioTrabSetFunc" <= now()');
        $criteria->addCondition('"dt_fimTrabSetFunc" IS NULL OR "dt_fimTrabSetFunc" > now()');
        $trabalhadorSetorFuncao = TrabalhadorSetorFuncao::model()->findAll($criteria);
        return $trabalhadorSetorFuncao;
    }


    public function getSituacaoTrabalhador()
    {
        $criteria = new CDbCriteria();
        $criteria->join .= 'INNER JOIN ' . CLIENTE . '."Empregador_Trabalhador" AS et ON t."IDEmpregadorTrabalhador" = et."IDEmpregadorTrabalhador"';
        $criteria->compare('et."IDEmpregadorTrabalhador"', $this->IDEmpregadorTrabalhador);
        $criteria->compare('t."visivel_situacaoTrabalhador"', true);
        $criteria->addCondition('t."dt_situacaoTrabalhador" <= now()');
        $criteria->order = 't."dt_situacaoTrabalhador" DESC';
        $situacaoTrabalhador = SituacaoTrabalhador::model()->find($criteria);

        if (isset($situacaoTrabalhador)) {
            return $situacaoTrabalhador;
        } else {
            return null;
        }
    }


    public function findAllBySetorFuncao($IDSetorFuncao)
    {
        $criteria = new CDbCriteria;
        $criteria->join = ' INNER JOIN ' . CLIENTE . '."Trabalhador_Setor_Funcao" AS tsf ';
        $criteria->join .= ' ON tsf."IDEmpregadorTrabalhador" = "t"."IDEmpregadorTrabalhador"';
        $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Setor_Funcao" AS sf on sf."IDSetorFuncao" = tsf."IDSetorFuncao" ';
        $criteria->compare('tsf."IDSetorFuncao"', $IDSetorFuncao);
        $criteria->compare('sf."ativo_setorFuncao"', true);
        $criteria->addCondition(
            'tsf."dt_inicioTrabSetFunc"  <= now()'
        );
        $criteria->addCondition('tsf."dt_fimTrabSetFunc" IS NULL OR tsf."dt_fimTrabSetFunc" >= now()');
        $criteria->order = 'tsf."dt_inicioTrabSetFunc" DESC';
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }


    public function findAllBySetor($IDSetor)
    {
        $criteria = new CDbCriteria;
        $criteria->join = ' INNER JOIN ' . CLIENTE . '."Trabalhador_Setor_Funcao" AS tsf ';
        $criteria->join .= ' ON tsf."IDEmpregadorTrabalhador" = "t"."IDEmpregadorTrabalhador"';
        $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Setor_Funcao" AS sf on sf."IDSetorFuncao" = tsf."IDSetorFuncao" ';
        $criteria->compare('sf."IDSetor"', $IDSetor);
        $criteria->compare('sf."ativo_setorFuncao"', true);
        $criteria->addCondition(
            'tsf."dt_inicioTrabSetFunc" <= now()'
        );
        $criteria->addCondition('tsf."dt_fimTrabSetFunc" IS NULL OR tsf."dt_fimTrabSetFunc" >= now()');
        $criteria->order = 'tsf."dt_inicioTrabSetFunc" DESC';
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function findAllBySituacao($IDTiposSituacao)
    {
        $criteria = new CDbCriteria;
        $criteria->select = 'DISTINCT on (trab."nome_trabalhador") *';
        $criteria->join = ' INNER JOIN ' . CLIENTE . '."Situacao_Trabalhador" AS st ON st."IDEmpregadorTrabalhador" = "t"."IDEmpregadorTrabalhador"';
        $criteria->join .= 'INNER JOIN ' . CLIENTE . '."Trabalhador" AS trab ON trab."IDTrabalhador" = t."IDTrabalhador" ';
        $criteria->join .= ' INNER JOIN "public"."Tipo_Situacao" AS ts ON st."IDTipoSituacao" = ts."IDTipoSituacao"';
        $criteria->compare('st."visivel_situacaoTrabalhador"', true);
        $criteria->addCondition('st."dt_situacaoTrabalhador" <= now()');
        $criteria->addInCondition('ts."IDTipoSituacao"', $IDTiposSituacao);
        $criteria->order = 'trab."nome_trabalhador" ASC, st."dt_situacaoTrabalhador" DESC';
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }


    public function findAllByStatusSituacao($IDStatusSituacao)
    {
        if (!empty($IDStatusSituacao)) {
            $criteria = new CDbCriteria;
            $criteria->join = ' INNER JOIN ' . CLIENTE . '."Trabalhador_Setor_Funcao" AS tsf ON "tsf"."IDEmpregadorTrabalhador" = "t"."IDEmpregadorTrabalhador"';
            $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Situacao_Trabalhador" AS st ON "st"."IDTrabalhadorSetorFuncao" = "tsf"."IDTrabalhadorSetorFuncao"';
            $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Trabalhador" AS trab ON "trab"."IDTrabalhador" = "t"."IDTrabalhador" ';
            $criteria->join .= ' INNER JOIN "public"."Tipo_Situacao" AS ts ON "st"."IDTipoSituacao" = "ts"."IDTipoSituacao"';
            $criteria->compare('"st"."visivel_situacaoTrabalhador"', true);
            $criteria->addInCondition('"ts"."status_situacao"', $IDStatusSituacao);
            $this->getDbCriteria()->mergeWith($criteria);
        }
        return $this;
    }

    public function findAllByTrabalhadoresAtivos()
    {
        return $this->findAllByStatusSituacao([0, 2]);
    }


    public function getLabelSetorFuncao()
    {
        if (count($this->trabalhadorSetorFuncaos) > 1) {
            return Html::activeList(
                $this,
                'trabalhadorSetorFuncaos',
                'iDSetorFuncao'
            );
        } else {
            return $this->trabalhadorSetorFuncaos[0]->iDSetorFuncao;
        }
    }

    public function getLabel()
    {
        return $this->iDTrabalhador . ' (' . $this->iDEmpregador->nome_empregador . ')';
    }

    public function getExamesClinicos($TrabalhadorSetorFuncao = null)
    {
        $findArray = [];
        empty($findArray) ? : $findArray["IDTrabalhadorSetorFuncao"] = $TrabalhadorSetorFuncao;
        return ExameClinico::model()->findAllByTrabalhador($this->IDEmpregadorTrabalhador)->findAllByAttributes(
            $findArray
        );
    }

    public function getResultadosExames()
    {
        return ResultadoExame::model()->findAllByTrabalhador($this->IDEmpregadorTrabalhador)->findAll();
    }
}
