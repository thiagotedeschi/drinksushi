<?php

/**
 * This is the model class for table "drink.Local_Horario".
 *
 * The followings are the available columns in table 'drink.Local_Horario':
 * @property integer $IDLocalHorario
 * @property integer $IDLocal
 * @property integer $IDHorario
 * @property string $dt_inicio
 * @property string $dt_fim
 * @property string $dt_exclusao
 * @property boolean $folga
 * The followings are the available model relations:
 * @property Local $iDLocal
 * @property Horario $iDHorario
 * @package base.Models
 */
class LocalHorario extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Local_Horario';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDLocal, IDHorario, dt_inicio', 'required'),
            array('IDLocal, IDHorario', 'numerical', 'integerOnly' => true),
            array('dt_inicio, dt_fim', 'date', 'format' => 'dd/MM/yyyy'),
            array('dt_fim, dt_exclusao, folga', 'safe'),
            array(
                'dt_fim',
                'compare',
                'compareAttribute' => 'dt_inicio',
                'operator' => '>=',
                'allowEmpty' => true,
                'message' => '{attribute} tem que ser maior que "{compareValue}".'
            ),
// The following rule is used by search().
            array(
                'IDLocalHorario, IDLocal, IDHorario, dt_inicio, dt_fim, dt_exclusao, folga',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDLocal' => array(self::BELONGS_TO, 'Local', 'IDLocal'),
            'iDHorario' => array(self::BELONGS_TO, 'Horario', 'IDHorario'),
        );
    }

    /**
     * Retorna a declaração de escopos nomeados.
     * Um escopo nomeado representa um critério de consulta que podem ser conectadas
     * com outros escopos nomeados e aplicados a uma consulta.
     * @return Array regras de escopo
     */
    public function scopes()
    {
        return array(
            'naoeFolga' => array(
                'condition' => '"folga" = false'
            ),
            'eFolga' => array(
                'condition' => '"folga"'
            ),
            'naoExcluido' => array(
                'condition' => '"dt_exclusao" IS NULL'
            ),
            'excluido' => array(
                'condition' => '"dt_exclusao" IS NOT NULL'
            ),
            'ordemDtInicio' => array(
                'order' => '"dt_inicio"'
            )
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDLocalHorario' => 'Idlocal Horário',
            'IDLocal' => 'Local',
            'IDHorario' => 'IdHorario',
            'dt_inicio' => 'Data Início',
            'dt_fim' => 'Data Fim',
            'dt_exclusao' => 'Data Exclusão',
            'folga' => 'Folga',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDLocalHorario"', HTexto::tiraLetras($this->IDLocalHorario));
        $criteria->compare('"IDLocal"', HTexto::tiraLetras($this->IDLocal));
        $criteria->compare('"IDHorario"', HTexto::tiraLetras($this->IDHorario));
        $criteria->compare('LOWER("dt_inicio")', mb_strtolower($this->dt_inicio), true);
        $criteria->compare('LOWER("dt_fim")', mb_strtolower($this->dt_fim), true);
        $criteria->compare('LOWER("dt_exclusao")', mb_strtolower($this->dt_exclusao), true);
        $criteria->compare('"folga"', $this->folga);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDHorario" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LocalHorario the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
