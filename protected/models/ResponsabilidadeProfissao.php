<?php

/**
 * This is the model class for table "Responsabilidade_Profissao".
 *
 * The followings are the available columns in table 'Responsabilidade_Profissao':
 * @property integer $IDResponsabilidadeProfissao
 * @property string $nome_responsabilidadeProfissao
 * @property string $desc_responsabilidadeProfissao
 * @package base.Models
 */
class ResponsabilidadeProfissao extends ActiveRecord
{
    CONST ID_RESPONSABILIDADE_ASSINAR_CLINICO = 25;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Responsabilidade_Profissao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('nome_responsabilidadeProfissao', 'required'),
            array('nome_responsabilidadeProfissao', 'length', 'max' => 70),
            array('desc_responsabilidadeProfissao', 'length', 'max' => 255),
// The following rule is used by search().
            array(
                'IDResponsabilidadeProfissao, nome_responsabilidadeProfissao, desc_responsabilidadeProfissao',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'profissaos' => array(
                self::MANY_MANY,
                'Profissao',
                CLIENTE . '.Responsabilidade_Profissao(IDProfissao, IDResponsabilidade)'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDResponsabilidadeProfissao' => 'Responsabilidade para Profissões',
            'nome_responsabilidadeProfissao' => 'Nome Responsabilidade Profissão',
            'desc_responsabilidadeProfissao' => 'Desc. Responsabilidade Profissão',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDResponsabilidadeProfissao"', HTexto::tiraLetras($this->IDResponsabilidadeProfissao));
        $criteria->compare(
            'LOWER("nome_responsabilidadeProfissao")',
            mb_strtolower($this->nome_responsabilidadeProfissao),
            true
        );
        $criteria->compare(
            'LOWER("desc_responsabilidadeProfissao")',
            mb_strtolower($this->desc_responsabilidadeProfissao),
            true
        );

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDResponsabilidadeProfissao" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ResponsabilidadeProfissao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function __toString()
    {
        return $this->nome_responsabilidadeProfissao;
    }


    public static function camposResponsabilidades()
    {
        return array(
            'nit_profissional' => array(
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24
            ),
            'inscricao_mteProfissional' => array(8, 9, 10, 11, 12),
            'org_classeProfissional' => array(1, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24),
            'n_registroProfissional' => array(1, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24),
            'uf_registro_Profissional' => array(1, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24)
        );
    }


    public function beforeDelete()
    {
        $return = true;
        $mensagem = [];

        $flagProfissional = false;
        $profissoes = [];
        //verifica se existe profissioanais associados
        //a profissões da responsabilidade a ser excluida
        foreach ($this->profissaos as $profissao) {
            foreach ($profissao->profissionals as $profissional) {
                foreach ($profissional->profissionalEmpregadors as $profissionalEmp) {
                    if ($profissionalEmp->ativo) {
                        $profissoes[$profissao->IDProfissao] = '#' . $profissao->IDProfissao;
                        $flagProfissional = true;
                    }
                }
            }
        }
        if ($flagProfissional) {
            $return = false;
            $mensagem[] = 'Erro! Para realizar a exclussão, retire a Responsabilidade das seguintes Profissões: '
                . implode(
                    "  ",
                    $profissoes
                );
        }
        Yii::app()->user->setFlash('error', $mensagem);

        if ($return) {
            return parent::beforeDelete();
        } else {
            return false;
        }
    }
}

