<?php

/**
 * This is the model class for table "drink.Responsabilidade_Profissao".
 *
 * The followings are the available columns in table 'drink.Responsabilidade_Profissao':
 * @property integer $IDResponsabilidade
 * @property integer $IDProfissao
 *
 * The followings are the available model relations:
 * @property Profissao $iDResponsabilidade
 * @package base.Models
 */
class ResponsabilidadeTemProfissao extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Responsabilidade_Profissao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDResponsabilidade, IDProfissao', 'required'),
            array('IDResponsabilidade, IDProfissao', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDResponsabilidade' => array(self::BELONGS_TO, 'ResponsabilidadeProfissao', 'IDResponsabilidade'),
            'iDProfissao' => array(self::BELONGS_TO, 'Profissao', 'IDProfissao'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDResponsabilidade' => 'Responsabilidade de Profissões',
            'IDProfissao' => 'Idprofissao',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ResponsabilidadeTemProfissao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
