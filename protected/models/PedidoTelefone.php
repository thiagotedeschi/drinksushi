<?php

/**
* This is the model class for table "drink.Pedido_Telefone".
*
* The followings are the available columns in table 'drink.Pedido_Telefone':
    * @property integer $IDPedidoTelefone
    * @property string $dt_aberturaPedido
    * @property boolean $embalagem
    * @property string $desconto
    * @property string $total_pedido
    * @property string $observacao_pedido
    * @property string $dinheiro
    * @property string $cartao_debito
    * @property string $cartao_credito
    * @property string $troco_dinheiro
    * @property integer $IDEstadoPedido
    * @property string $motivo_cancelamento
    * @property integer $IDClienteRestaurante
    *
    * The followings are the available model relations:
            * @property EstadoPedido $iDEstadoPedido
    * @package base.Models
*/
class PedidoTelefone extends ActiveRecord
{

    public $hora;

    public $subtotal;

    public $total_ped;

    public $troco_din;

/**
* Retorna o nome da tabela representada pelo Modelo.
*
* @return string nome da tabela
*/
public function tableName()
{
return CLIENTE.'.Pedido_Telefone';
}

/**
* Retorna as regras de validação para o Modelo
* @return Array Regras de Validação.
*/
public function rules()
{
return array(
    array('IDClienteRestaurante', 'required'),
    array('IDEstadoPedido, IDClienteRestaurante', 'numerical', 'integerOnly'=>true),
    array('dt_aberturaPedido', 'length', 'max'=>6),
    array('desconto, total_pedido, dinheiro, cartao_debito, cartao_credito, troco_dinheiro', 'length', 'max'=>11),
    array('embalagem, observacao_pedido, motivo_cancelamento, IDEstadoPedido', 'safe'),
// @todo Please remove those attributes that should not be searched.
array('IDPedidoTelefone, dt_aberturaPedido, embalagem, desconto, total_pedido, observacao_pedido, dinheiro, cartao_debito, cartao_credito, troco_dinheiro, IDEstadoPedido, motivo_cancelamento, IDClienteRestaurante', 'safe', 'on'=>'search'),
);
}

/**
* Retorna as relações do modelo
* @return Array relações
*/
public function relations()
{
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
return array(
    'iDEstadoPedido' => array(self::BELONGS_TO, 'EstadoPedido', 'IDEstadoPedido'),
    'pedidoTelefoneProdutos' => array(self::HAS_MANY, 'PedidoTelefoneProduto', 'IDPedido'),
);
}

/**
* Retorna as labels dos atributos do modelo no formato (atributo=>label)
* @return Array labels dos atributos.
*/
public function attributeLabels()
{
return array(
    'IDPedidoTelefone' => 'Pedido Por Telefone',
    'dt_aberturaPedido' => 'Data',
    'embalagem' => 'Embalagem',
    'desconto' => 'Desconto',
    'total_pedido' => 'Total Pedido',
    'observacao_pedido' => 'Observacao Pedido',
    'hora' => 'Hora',
    'dinheiro' => 'Dinheiro',
    'cartao_debito' => 'Cartão de Débito',
    'cartao_credito' => 'Cartão de Crédito',
    'troco_dinheiro' => 'Troco',
    'IDEstadoPedido' => 'Status do Pedido',
    'motivo_cancelamento' => 'Motivo do Cancelamento',
    'IDClienteRestaurante' => 'Cliente',
);
}

/**
* Retorna uma lista de modelos baseada nas definições de filtro da tabela
* @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
*/
public function search()
{
// @todo Please modify the following code to remove attributes that should not be searched.

$criteria=new CDbCriteria;

		$criteria->compare('"IDPedidoTelefone"',HTexto::tiraLetras($this->IDPedidoTelefone));
		$criteria->compare('LOWER("dt_aberturaPedido")',mb_strtolower($this->dt_aberturaPedido),true);
		$criteria->compare('"embalagem"',$this->embalagem);
		$criteria->compare('LOWER("desconto")',mb_strtolower($this->desconto),true);
		$criteria->compare('LOWER("total_pedido")',mb_strtolower($this->total_pedido),true);
		$criteria->compare('LOWER("observacao_pedido")',mb_strtolower($this->observacao_pedido),true);
		$criteria->compare('LOWER("dinheiro")',mb_strtolower($this->dinheiro),true);
		$criteria->compare('LOWER("cartao_debito")',mb_strtolower($this->cartao_debito),true);
		$criteria->compare('LOWER("cartao_credito")',mb_strtolower($this->cartao_credito),true);
		$criteria->compare('LOWER("troco_dinheiro")',mb_strtolower($this->troco_dinheiro),true);
		$criteria->compare('"IDEstadoPedido"',$this->IDEstadoPedido);
		$criteria->compare('LOWER("motivo_cancelamento")',mb_strtolower($this->motivo_cancelamento),true);
		$criteria->compare('"IDClienteRestaurante"',$this->IDClienteRestaurante);

return new CActiveDataProvider($this, array(
'criteria'=>$criteria,
'Pagination' => array(
'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
),
'sort' => array(
        'defaultOrder' => '"IDPedidoTelefone" DESC',
    )));
}

/**
* Returns the static model of the specified AR class.
* Please note that you should have this exact method in all your CActiveRecord descendants!
* @param string $className active record class name.
* @return PedidoTelefone the static model class
*/
public static function model($className=__CLASS__)
{
return parent::model($className);
}
}
