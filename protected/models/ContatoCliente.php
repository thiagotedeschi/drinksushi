<?php

/**
 * This is the model class for table "Contato".
 *
 * The followings are the available columns in table 'Contato':
 * @property integer $IDContato
 * @property string $nome_contato
 * @property string $principal_contato
 * @property integer $IDCliente
 *
 * The followings are the available model relations:
 * @property TelefoneContato[] $telefoneContatos
 * @property EmailContato[] $emailContatos
 * @property Cliente $iDCliente
 * @property ResponsabilidadeContato[] $responsabilidadeContatos
 * @package base.Models
 */
class ContatoCliente extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Contato';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('nome_contato, principal_contato, IDCliente', 'required'),
            array('IDCliente', 'numerical', 'integerOnly' => true),
            array('nome_contato', 'length', 'max' => 255),
            array('principal_contato', 'length', 'max' => 1),
// The following rule is used by search().
            array('IDContato, nome_contato, principal_contato, IDCliente', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'telefoneContatos' => array(self::HAS_MANY, 'TelefoneContato', 'IDContato'),
            'emailContatos' => array(self::HAS_MANY, 'EmailContato', 'IDContato'),
            'iDCliente' => array(self::BELONGS_TO, 'Cliente', 'IDCliente'),
            'responsabilidadeContatos' => array(
                self::MANY_MANY,
                'ResponsabilidadeContato',
                'Contatos_tem_responsabilidades(IDContato_Contato, IDResponsabilidade_contato)'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDContato' => 'Contato',
            'nome_contato' => 'Nome do Contato',
            'principal_contato' => 'Contato Principal?',
            'IDCliente' => 'Cliente',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDContato"', HTexto::tiraLetras($this->IDContato));
        $criteria->compare('LOWER("nome_contato")', mb_strtolower($this->nome_contato), true);
        $criteria->compare('"principal_contato"', $this->principal_contato);
        $criteria->compare('"IDCliente"', $this->IDCliente);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDContato" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContatoCliente the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
