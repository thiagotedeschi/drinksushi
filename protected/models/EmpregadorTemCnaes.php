<?php

/**
 * This is the model class for table "drink.Empregador_tem_Cnaes".
 *
 * The followings are the available columns in table 'drink.Empregador_tem_Cnaes':
 * @property integer $IDEmpregador
 * @property integer $IDCnae
 * @property boolean $principal_CNAE
 * @package base.Models
 */
class EmpregadorTemCnaes extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Empregador_tem_Cnaes';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDEmpregador, IDCnae', 'required'),
            array('IDEmpregador, IDCnae', 'numerical', 'integerOnly' => true),
            array('principal_CNAE', 'safe'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'cnaes' => array(self::BELONGS_TO, 'CNAE', 'IDCnae'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDEmpregador' => 'Empregador',
            'IDCnae' => 'Idcnae',
            'principal_CNAE' => 'Principal Cnae',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EmpregadorTemCnaes the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getCnae()
    {
        return $this->cnaes()->getCnae();
    }

    public function getLabelCnae()
    {
        return $this->cnaes()->getLabelCnae();
    }

    public function __toString()
    {
        return $this->getLabelCnae();
    }

}
