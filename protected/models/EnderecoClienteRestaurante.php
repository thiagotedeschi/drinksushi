<?php

/**
 * This is the model class for table "drink.Endereco_ClienteRestaurante".
 *
 * The followings are the available columns in table 'drink.Endereco_ClienteRestaurante':
 * @property integer $IDEnderecoClienteRestaurante
 * @property integer $IDBairro
 * @property string $rua
 * @property string $numero
 * @property string $complemento
 *
 * The followings are the available model relations:
 * @property ClienteRestaurante[] $clienteRestaurantes
 * @package base.Models
 */
class EnderecoClienteRestaurante extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Endereco_ClienteRestaurante';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDBairro', 'required'),
            array('IDBairro', 'numerical', 'integerOnly' => true),
            array('rua', 'length', 'max' => 255),
            array('numero', 'length', 'max' => 20),
            array('complemento', 'length', 'max' => 100),
// @todo Please remove those attributes that should not be searched.
            array('IDEnderecoClienteRestaurante, IDBairro, rua, numero, complemento', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'clienteRestaurantes' => array(self::HAS_MANY, 'ClienteRestaurante', 'IDEnderecoClienteRestaurante'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDEnderecoClienteRestaurante' => 'Endereço',
            'IDBairro' => 'Bairro',
            'rua' => 'Rua',
            'numero' => 'Número',
            'complemento' => 'Complemento',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('"IDEnderecoClienteRestaurante"', HTexto::tiraLetras($this->IDEnderecoClienteRestaurante));
        $criteria->compare('"IDBairro"', $this->IDBairro);
        $criteria->compare('LOWER("rua")', mb_strtolower($this->rua), true);
        $criteria->compare('LOWER("numero")', mb_strtolower($this->numero), true);
        $criteria->compare('LOWER("complemento")', mb_strtolower($this->complemento), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDEnderecoClienteRestaurante" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EnderecoClienteRestaurante the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /*
    *@return o endereço do cliente
    *@author Thiago Tedeschi <thiagotedeschi@hotmail.com>
    *@since 1.0 09/06/2015
   */
    public function getLabelEnderecoCliente()
    {
        $bairro = Bairro::model()->findByPk($this->IDBairro);
        $data = '
                    Bairro: '.$bairro->nome_bairro.',
                    Rua: '.$this->rua.',
                    Número: '.$this->numero.',
                    Complemento: '.$this->complemento.'

        ';

        return $data;
    }

    /*
     *@return Retorna a função getLabelEnderecoCliente
     *@author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     *@since 1.0 09/06/2015
    */
    public function __toString()
    {
        return $this->getLabelEnderecoCliente();
    }
}
