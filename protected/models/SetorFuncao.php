<?php

/**
 * This is the model class for table "drink.Setor_Funcao".
 *
 * The followings are the available columns in table 'drink.Setor_Funcao':
 * @property integer $IDSetorFuncao
 * @property integer $IDSetor
 * @property integer $IDFuncao
 * @property integer $nr_declaradoTrabalhadores
 * @property string $dt_associacao
 * @property boolean $ativo_setorFuncao
 *
 *

 * @package base.Models
 */
class SetorFuncao extends ActiveRecord
{
    /**
     * @var string $Setor guarda o nome do Setor para usar na pesquisa
     */
    public $Setor;
    /**
     * @var string $Setor guarda o nome da Função para usar na pesquisa
     */
    public $Funcao;
    /**
     * @var string $IDEmpregador guarda o ID do Empregador para filtrar setores
     */
    public $IDEmpregador;

    public $nrRealTrabalhadores;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Setor_Funcao';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('IDSetor, IDFuncao', 'required'),
            array('dt_associacao, IDEmpregador', 'safe'),
            array('IDSetor, IDFuncao, nr_declaradoTrabalhadores', 'numerical', 'integerOnly' => true),
// The following rule is used by search().
            array(
                'IDSetorFuncao, IDSetor, IDFuncao, Setor, Funcao, dt_associacao, ativo_setorFuncao, nr_declaradoTrabalhadores',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'acaoPreventivaRecomendadas' => array(self::HAS_MANY, 'AcaoPreventivaRecomendada', 'IDSetorFuncao'),
            'quantificacaoRecomendadas' => array(self::HAS_MANY, 'QuantificacaoRecomendada', 'IDSetorFuncao'),
            'epcRecomendados' => array(self::HAS_MANY, 'EPCRecomendado', 'IDSetorFuncao'),
            'epiRecomendados' => array(self::HAS_MANY, 'EPIRecomendado', 'IDSetorFuncao'),
            'mcoRecomendadas' => array(self::HAS_MANY, 'MCORecomendada', 'IDSetorFuncao'),
            'mcaRecomendadas' => array(self::HAS_MANY, 'MCARecomendada', 'IDSetorFuncao'),
            'treinamentoRecomendados' => array(self::HAS_MANY, 'TreinamentoRecomendado', 'IDSetorFuncao'),
            'resultadoQuantificacaoVibracaos' => array(
                self::MANY_MANY,
                'ResultadoQuantificacaoVibracao',
                CLIENTE . '.Resultado_Quantificacao_Vibracao_Setor_Funcao(IDSetorFuncao, IDResultadoQuantificacao)'
            ),
            'resultadoQuantificacaoQuimicos' => array(
                self::MANY_MANY,
                'ResultadoQuantificacaoQuimico',
                CLIENTE . '.Resultado_Quantificacao_Quimico_Setor_Funcao(IDSetorFuncao, IDResultadoQuantificacao)'
            ),
            'resultadoQuantificacaoCalorSetorFuncaos' => array(
                self::HAS_MANY,
                'ResultadoQuantificacaoCalorSetorFuncao',
                'IDSetorFuncao'
            ),
            'mapeamentoFrios' => array(
                self::MANY_MANY,
                'MapeamentoFrio',
                CLIENTE . '.Mapeamento_Frio_Setor_Funcao(IDSetorFuncao, IDMapeamentoFrio)'
            ),
            'resultadoQuantificacaoRuidos' => array(
                self::MANY_MANY,
                'ResultadoQuantificacaoRuido',
                CLIENTE . '.Resultado_Quantificacao_Ruido_Setor_Funcao(IDSetorFuncao, IDResultadoQuantificacao)'
            ),
            'mcaExistentes' => array(self::HAS_MANY, 'MCAExistente', 'IDSetorFuncao'),
            'identificacaoRiscos' => array(self::HAS_MANY, 'IdentificacaoRisco', 'IDSetorFuncao'),
            'epcExistentes' => array(self::HAS_MANY, 'EPCExistente', 'IDSetorFuncao'),
            'mapeamentoRuidos' => array(
                self::MANY_MANY,
                'MapeamentoRuido',
                CLIENTE . '.Mapeamento_Ruido_Setor_Funcao(IDSetorFuncao, IDMapeamentoRuido)'
            ),
            'iDSetor' => array(self::BELONGS_TO, 'Setor', 'IDSetor'),
            'iDFuncao' => array(self::BELONGS_TO, 'Funcao', 'IDFuncao'),
            'descricaoAtividades' => array(self::HAS_MANY, 'DescricaoAtividade', 'IDSetorFuncao'),
            'mcoExistentes' => array(self::HAS_MANY, 'MCOExistente', 'IDSetorFuncao'),
            'treinamentoExistentes' => array(self::HAS_MANY, 'TreinamentoExistente', 'IDSetorFuncao'),
            'mapeamentoIluminancias' => array(
                self::MANY_MANY,
                'MapeamentoIluminancia',
                CLIENTE . '.Mapeamento_Iluminancia_Setor_Funcao(IDSetorFuncao, IDMapeamentoIluminancia)'
            ),
            'setorFuncaoExameComplementars' => array(self::HAS_MANY, 'SetorFuncaoExameComplementar', 'IDSetorFuncao'),
            'mapeamentoConfortoAcusticos' => array(
                self::MANY_MANY,
                'MapeamentoConfortoAcustico',
                CLIENTE . '.Mapeamento_Conforto_Acustico_Setor_Funcao(IDSetorFuncao, IDMapeamentoConfortoAcustico)'
            ),
            'descricaoAmbientes' => array(self::HAS_MANY, 'DescricaoAmbiente', 'IDSetorFuncao'),
            'trabalhadorSetorFuncaos' => array(self::HAS_MANY, 'TrabalhadorSetorFuncao', 'IDSetorFuncao'),
            'epiExistentes' => array(self::HAS_MANY, 'EPIExistente', 'IDSetorFuncao'),
            'historicoSetorFuncaos' => array(self::HAS_MANY, 'HistoricoSetorFuncao', 'IDSetorFuncao'),
            'mapeamentoTemperaturaEfetivas' => array(
                self::MANY_MANY,
                'MapeamentoTemperaturaEfetiva',
                CLIENTE . '.Mapeamento_Temperatura_Efetiva_Setor_Funcao(IDSetorFuncao, IDMapeamentoTemperaturaEfetiva)'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDSetorFuncao' => 'Setor x Função',
            'IDSetor' => 'Setor',
            'IDFuncao' => 'Função',
            'dt_associacao' => 'Data de Associação',
            'ativo_setorFuncao' => 'Ativo?',
            'nr_declaradoTrabalhadores' => 'Número Declarado de Trabalhadores',
            'Setor' => 'Setor',
            'Funcao' => 'Funcao',
            'IDEmpregador' => 'Empregador'
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->with = array('iDSetor', 'iDFuncao');

        // Como filtrar os resultados pro empregador selecionado.
        $emp = Yii::app()->user->getState('IDEmpregador');
        if ($emp == null) {
            $criteria->addInCondition('"iDSetor"."IDEmpregador"', null);
        } else {
            $criteria->compare('"iDSetor"."IDEmpregador"', $emp);
        }
        $criteria->compare('"IDSetorFuncao"', HTexto::tiraLetras($this->IDSetorFuncao));
        $criteria->compare('LOWER("iDSetor"."nome_setor")', mb_strtolower($this->Setor), true);
        $criteria->compare('LOWER("iDFuncao"."nome_funcao")', mb_strtolower($this->Funcao), true);
        $criteria->compare('LOWER("dt_associacao")', mb_strtolower($this->dt_associacao), true);
        $criteria->compare('"nr_declaradoTrabalhadores"', HTexto::tiraLetras($this->nr_declaradoTrabalhadores));
        $criteria->compare('"ativo_setorFuncao"', $this->ativo_setorFuncao);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDSetorFuncao" DESC',
                'attributes' => array(
                    'Setor' => array(
                        'asc' => '"iDSetor"."nome_setor"',
                        'desc' => '"iDSetor"."nome_setor" DESC',
                    ),
                    'Funcao' => array(
                        'asc' => '"iDFuncao"."nome_funcao"',
                        'desc' => '"iDFuncao"."nome_funcao" DESC',
                    ),
                    'dt_associacao' => array(
                        'asc' => '"dt_associacao"',
                        'desc' => '"dt_associacao" DESC',
                    ),
                    'nrRealTrabalhadores' => array(
                        'asc' => '"nrRealTrabalhadores"',
                        'desc' => '"nrRealTrabalhadores" DESC',
                    ),
                    '*'
                ),

            )
        ));
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SetorFuncao the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getLabelAcao()
    {
        if ($this->ativo_setorFuncao) {
            return 'Desativar';
        } else {
            return 'Ativar';
        }
    }


    public function getIcone()
    {
        if ($this->ativo_setorFuncao) {
            return 'icon-expand';
        } else {
            return 'icon-compress';
        }
    }


    public function getLabelSetorFuncao()
    {
        return $this->iDSetor . '/' . $this->iDFuncao;
    }


    public function scopes()
    {

        return array(
            'semDescricaoAmbiente' => array(
                'with' => 'descricaoAmbientes',
                'condition' => '"descricaoAmbientes" is NULL',
            ),
            'semDescricaoAtividade' => array(
                'with' => 'descricaoAtividades',
                'condition' => '"descricaoAtividades" is NULL',
            ),
            'ativo' => array(
                'condition' => '"ativo_setorFuncao"'
            ),
            'setorFuncao' => array(
                'order' => '"iDSetor"."nome_setor"'
            )
        );
    }


    public function __toString()
    {
        return $this->getLabelSetorFuncao();
    }


    public function getNrRealTrabalhadores($IDSetorFuncao = null)
    {
        if (!is_null($IDSetorFuncao)) {
            $results = TrabalhadorSetorFuncao::model()->getTrabalhadores($IDSetorFuncao)->count();
        } else {
            $results = TrabalhadorSetorFuncao::model()->getTrabalhadores($this->IDSetorFuncao)->count();
        }
        if ($results > 0) {
            return $results;
        } else {
            return null;
        }
    }


    public function getNrTrabalhadores($IDSetorFuncao = null, $mostraNrRealTrabalhadores = false)
    {
        if (!is_null($IDSetorFuncao)) {
            $consulta = Yii::app()->db->createCommand()
                ->select(
                    'COALESCE (NULLIF (count ("t"."IDTrabalhadorSetorFuncao"), 0), "sf"."nr_declaradoTrabalhadores") AS nr_trabalhadores'
                )
                ->from(CLIENTE . '.Setor_Funcao sf')
                ->leftJoin(CLIENTE . '.Trabalhador_Setor_Funcao t', '"t"."IDSetorFuncao" = "sf"."IDSetorFuncao"')
                ->where(
                    '"sf"."IDSetorFuncao" = :IDSetorFuncao AND "t"."dt_fimTrabSetFunc" ISNULL',
                    ['IDSetorFuncao' => $IDSetorFuncao]
                )
                ->group('sf.nr_declaradoTrabalhadores')
                ->queryRow();

            return $consulta;
        } else {
            $nrRealTrabalhadores = is_null($this->getNrRealTrabalhadores()) ? 0 : $this->getNrRealTrabalhadores();
            $nrDeclarado = is_null($this->nr_declaradoTrabalhadores) ? 0 : $this->nr_declaradoTrabalhadores;
            if ($mostraNrRealTrabalhadores) {
                return $nrRealTrabalhadores;
            } else {
                return $nrDeclarado;
            }
        }
    }


    public function findAllByEmpregador($IDEmpregador)
    {
        $criteria = new CDbCriteria();
        $criteria->join = 'INNER JOIN ' . CLIENTE . '."Setor" AS s ON "t"."IDSetor" = s."IDSetor"';
        $criteria->join .= ' INNER JOIN ' . CLIENTE . '."Setor" AS e ON s."IDEmpregador" = e."IDEmpregador"';
        $criteria->compare('e."IDEmpregador"', $IDEmpregador);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }


    public function getReconhecimentoFisicos($ativos = true)
    {
        if ($ativos) {
            $sql = '"dt_desativacao" IS NULL AND "motivo_desativacao" IS NULL';
        } else {
            $sql = '"dt_desativacao" IS NOT NULL AND "motivo_desativacao" IS NOT NULL';
        }

        return ReconhecimentoFisico::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll($sql);
    }

    public function getGhFisicos()
    {
        return GHFisico::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll();
    }


    public function getReconhecimentoQuimicos($ativos = true)
    {
        if ($ativos) {
            $sql = '"dt_desativacao" IS NULL AND "motivo_desativacao" IS NULL';
        } else {
            $sql = '"dt_desativacao" IS NOT NULL AND "motivo_desativacao" IS NOT NULL';
        }

        return ReconhecimentoQuimico::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll($sql);
    }

    public function getGhQuimicos()
    {
        return GHQuimico::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll();
    }


    public function getReconhecimentoBiologicos($ativos = true)
    {
        if ($ativos) {
            $sql = '"dt_desativacao" IS NULL AND "motivo_desativacao" IS NULL';
        } else {
            $sql = '"dt_desativacao" IS NOT NULL AND "motivo_desativacao" IS NOT NULL';
        }

        return ReconhecimentoBiologico::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll($sql);
    }

    public function getGhBiologicos()
    {
        return GHBiologico::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll();
    }


    public function getReconhecimentoBiologicoNR32s($ativos = true)
    {
        if ($ativos) {
            $sql = '"dt_desativacao" IS NULL AND "motivo_desativacao" IS NULL';
        } else {
            $sql = '"dt_desativacao" IS NOT NULL AND "motivo_desativacao" IS NOT NULL';
        }

        return ReconhecimentoBiologicoNR32::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll($sql);
    }

    public function getGhBiologicoNR32s()
    {
        return GHBiologicoNR32::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll();
    }


    public function getReconhecimentoErgonomicos($ativos = true)
    {
        if ($ativos) {
            $sql = '"dt_desativacao" IS NULL AND "motivo_desativacao" IS NULL';
        } else {
            $sql = '"dt_desativacao" IS NOT NULL AND "motivo_desativacao" IS NOT NULL';
        }

        return ReconhecimentoErgonomico::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll($sql);
    }


    public function getGhErgonomicos()
    {
        return GHErgonomico::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll();
    }

    public function getReconhecimentoAcidentes($ativos = true)
    {
        if ($ativos) {
            $sql = '"dt_desativacao" IS NULL AND "motivo_desativacao" IS NULL';
        } else {
            $sql = '"dt_desativacao" IS NOT NULL AND "motivo_desativacao" IS NOT NULL';
        }

        return ReconhecimentoAcidente::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll($sql);
    }

    public function getGhAcidentes()
    {
        return GHAcidente::model()->findBySetorFuncao($this->IDSetorFuncao)->findAll();
    }


    public function getRiscoByPeriodo($dt_inicio, $dt_fim)
    {
        return AgenteIdentificacaoRisco::model()->with(array('iDIdentificacao'))->orderAlfa()
            ->findAllByPeriodo($dt_inicio, $dt_fim)->findAll(
                '"iDIdentificacao"."IDSetorFuncao" = :IDSetorFuncao',
                array('IDSetorFuncao' => $this->IDSetorFuncao)
            );
    }


    public function getTrabalhadores($tipoSituacao = [])
    {
        $trabalhadores = TrabalhadorSetorFuncao::model()->findAllBySetorFuncao(
            $this->IDSetorFuncao
        )->findAllByTipoSituacao($tipoSituacao)->findAll();
        return $trabalhadores;
    }


    public function getTrabalhadoresDocumento()
    {
        return $this->getTrabalhadores(
            [0, 1, 3, 4, 5, 8, 9, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 26, 28, 30]
        );
    }

}
