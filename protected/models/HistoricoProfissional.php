<?php

/**
 * This is the model class for table "drink.Historico_Profissional".
 *
 * The followings are the available columns in table 'drink.Historico_Profissional':
 * @property integer $IDHistoricoProfissional
 * @property string $dt_correnteHistoricoProfissional
 * @property string $dt_modificacaoHistoricoProfissional
 * @property string $motivo_historicoProfissional
 * @property boolean $ativacao_historicoProfissional
 * @property integer $IDProfissional
 * @property integer $IDUsuarioResponsavel
 *
 * The followings are the available model relations:
 * @property Profissional $iDUsuarioResponsavel
 * @property Profissional $iDProfissional
 * @package base.Models
 */
class HistoricoProfissional extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Historico_Profissional';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('motivo_historicoProfissional', 'required'),
            array('IDHistoricoProfissional, IDProfissional, IDUsuarioResponsavel', 'numerical', 'integerOnly' => true),
            array('motivo_historicoProfissional', 'length', 'max' => 300),
            array('ativacao_historicoProfissional', 'safe')
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDUsuarioResponsavel' => array(self::BELONGS_TO, 'Profissional', 'IDUsuarioResponsavel'),
            'iDProfissional' => array(self::BELONGS_TO, 'Profissional', 'IDProfissional'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDHistoricoProfissional' => 'Histórico Profissional',
            'dt_correnteHistoricoProfissional' => 'Data Corrente',
            'dt_modificacaoHistoricoProfissional' => 'Data da Modificação',
            'motivo_historicoProfissional' => 'Motivo',
            'ativacao_historicoProfissional' => 'Ativação',
            'IDProfissional' => 'Profissional',
            'IDUsuarioResponsavel' => 'Usuário Responsável',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return HistoricoProfissional the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
