<?php

/**
 * This is the model class for table "drink.Atributo".
 *
 * The followings are the available columns in table 'drink.Atributo':
 * @property integer $IDAtributo
 * @property string $atributo
 * @property string $valor_atributo
 * @package base.Models
 */
class AtributoLocal extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Atributo';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('atributo', 'required'),
            array('atributo', 'length', 'max' => 50),
            array('valor_atributo', 'length', 'max' => 255),
            // The following rule is used by search().
            array('IDAtributo, atributo, valor_atributo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDAtributo' => 'Atributo Local',
            'atributo' => 'Nome do Atributo',
            'valor_atributo' => 'Valor do Atributo',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDAtributo"', HTexto::tiraLetras($this->IDAtributo));
        $criteria->compare('LOWER("atributo")', mb_strtolower($this->atributo), true);
        $criteria->compare('LOWER(valor_atributo)', mb_strtolower($this->valor_atributo), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDAtributo" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AtributoLocal the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
