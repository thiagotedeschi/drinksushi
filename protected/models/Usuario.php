<?php

/**
 * This is the model class for table "drink.Usuario".
 *
 * The followings are the available columns in table 'drink.Usuario':
 * @property integer $IDUsuario
 * @property string $login_usuario
 * @property string $grant_usuario
 * @property string $dt_criacaoUsuario
 * @property integer $tempo_utilizacaoUsuario
 * @property string $dt_ultimaTrocaSenhaUsuario
 * @property boolean $ativo_usuario
 * @property string $foto_usuario
 * @property string $senha_usuario
 * @property integer $IDProfissional
 * @property boolean $admin_usuario
 *
 * The followings are the available model relations:
 * @property Mensagem[] $mensagensRecebidas
 * @property Mensagem[] $mensagensEnviadas
 * @property Profissional $iDProfissional
 * @property Log[] $logs
 * @property ConcessaoGrupo[] $concessaoGrupos
 * @property Permissao[] $permissaos
 * @property HistoricoLogin[] $historicoLogins
 * @package base.Models
 */
class Usuario extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Usuario';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('login_usuario', 'unique', 'caseSensitive' => false),
            array('login_usuario, grant_usuario, IDProfissional', 'required'),
            array('tempo_utilizacaoUsuario, IDProfissional', 'numerical', 'integerOnly' => true),
            array('login_usuario, senha_usuario', 'length', 'max' => 255),
            array('grant_usuario', 'length', 'max' => 1),
            array('foto_usuario', 'safe', 'except' => 'esqueciSenha'),
            array('dt_ultimaTrocaSenhaUsuario, admin_usuario, ativo_usuario', 'safe'),
            // The following rule is used by search().
            array(
                'IDUsuario, login_usuario, ativo_usuario, IDProfissional',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'mensagemsEnviadas' => array(self::HAS_MANY, 'Mensagem', 'IDUsuarioRemetente'),
            'mensagensRecebidas' => array(self::HAS_MANY, 'Mensagem', 'IDUsuarioDestinatario'),
            'iDProfissional' => array(self::BELONGS_TO, 'Profissional', 'IDProfissional'),
            'logs' => array(self::HAS_MANY, 'Log', 'IDUsuario'),
            'concessaoGrupos' => array(self::HAS_MANY, 'ConcessaoGrupo', 'IDUsuario'),
            'permissaos' => array(self::HAS_MANY, 'Permissao', 'IDUsuario_concedeu'),
            'historicoLogins' => array(self::HAS_MANY, 'HistoricoLogin', 'IDUsuario'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDUsuario' => 'Usuário',
            'login_usuario' => 'Login Usuário',
            'grant_usuario' => 'Permissão de Concessão?',
            'dt_criacaoUsuario' => 'Data de Criação',
            'tempo_utilizacaoUsuario' => 'Tempo de Utilização (dias)',
            'dt_ultimaTrocaSenhaUsuario' => 'Última troca de senha',
            'ativo_usuario' => 'Usuário Ativo?',
            'foto_usuario' => 'Foto do Usuario',
            'senha_usuario' => 'Senha do Usuario',
            'IDProfissional' => 'Profissional',
            'admin_usuario' => 'Usuário Administrador?',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDUsuario"', HTexto::tiraLetras($this->IDUsuario));
        $criteria->compare('LOWER("login_usuario")', mb_strtolower($this->login_usuario), true);
        $criteria->compare('"ativo_usuario"', mb_strtolower($this->ativo_usuario));
        $criteria->compare('"IDProfissional"', HTexto::tiraLetras($this->IDProfissional));

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDUsuario" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Usuario the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getFoto()
    {
        if ($this->foto_usuario != '') {
            $stringImagem = stream_get_contents($this->foto_usuario);
            return 'data:image/png;base64,' . $stringImagem;
        } else {
            return $this->getImagemPadrao();
        }
    }

    public function getImagemPadrao()
    {
        if ($this->iDProfissional->genero_profissional == 'M') {
            return ASSETS_LINK . '/images/main/usuarios/default_male.png';
        } else {
            return ASSETS_LINK . '/images/main/usuarios/default_female.png';
        }
    }

    public function getTempoSenhaExpirar()
    {
        return $data = HData::tempoSenhaExpirar(
            $this->dt_ultimaTrocaSenhaUsuario == '' ? $this->dt_criacaoUsuario : $this->dt_ultimaTrocaSenhaUsuario
        );
    }

    /**
     * @return string tag HTML com para renderizar a data que a senha irá expirar
     */
    public function getLabelTempoSenhaExpirar()
    {
        $data = $this->getTempoSenhaExpirar();
        if ($data == '') {
            return '<span class="label label-warning">Indeterminada</span>';
        }
        $data = new DateTime($data);
        $hoje = new DateTime();
        if ($data < $hoje) {
            $label = "inverse";
        } else {
            if ($data->modify("-15 days") < $hoje) {
                $label = "important";
            } else {
                $label = 'success';
            }
        }
        return '<span class="label label-' . $label . '">' . $data->format(HData::BR_DATE_FORMAT) . '</span>';
    }


    public function getGrupos()
    {
        $concessoes = ($this->concessaoGrupos(
            array('condition' => "((dt_fim IS NULL) OR (dt_fim > now())) AND ((dt_inicio IS NULL) OR (dt_inicio < now()))")
        ));
        $grupos = array();
        foreach ($concessoes as $concessao) {
            $grupos[] = $concessao->iDGrupo();
        }
        return $grupos;
    }

    public function getIdGrupos()
    {
        $concessoes = ($this->concessaoGrupos(
            array('condition' => "((dt_fim IS NULL) OR (dt_fim > now())) AND ((dt_inicio IS NULL) OR (dt_inicio < now()))")
        ));
        $grupos = array();
        foreach ($concessoes as $concessao) {
            $grupos[] = $concessao->iDGrupo()->IDGrupo;
        }
        return $grupos;
    }


    public function scopes()
    {
        return array(
            'ordemAlfabetica' => array(
                'order' => 'login_usuario ASC'
            )
        );
    }

    public static function temPermissao($IDAcao)
    {

        if (is_null($IDAcao)) //verifica se o valor e nulo ou não
        {
            return true;
        } //caso seja null, o usuário estará liberado para acessar a pagina
        else {
            return isset(Yii::app()->session['acoes'][$IDAcao]);
        }
    }
}
