<?php

/**
 * This is the model class for table "drink.Grupo".
 *
 * The followings are the available columns in table 'drink.Grupo':
 * @property integer $IDGrupo
 * @property string $nome_grupo
 * @property string $desc_grupo
 * @property string $dt_criacaoGrupo
 * @property string $ativo_grupo
 *
 * The followings are the available model relations:
 * @property ConcessaoGrupo[] $concessaoGrupos
 * @property Permissao[] $permissaos
 * @package base.Models
 */
class Grupo extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Grupo';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nome_grupo, dt_criacaoGrupo, ativo_grupo', 'required'),
            array('nome_grupo', 'length', 'max' => 60),
            array('desc_grupo', 'length', 'max' => 255),
            array('ativo_grupo', 'length', 'max' => 1),
            // The following rule is used by search().
            array('IDGrupo, nome_grupo, desc_grupo, ativo_grupo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'concessaoGrupos' => array(self::HAS_MANY, 'ConcessaoGrupo', 'IDGrupo'),
            'permissaos' => array(self::HAS_MANY, 'Permissao', 'IDGrupo'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDGrupo' => 'Grupo de Usuários',
            'nome_grupo' => 'Nome do Grupo',
            'desc_grupo' => 'Descrição do Grupo',
            'dt_criacaoGrupo' => 'Data Criacao do Grupo',
            'ativo_grupo' => 'Grupo Ativo?',
            'associar_grupo' => 'Associar este Usuário aos Grupos:',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDGrupo"', HTexto::tiraLetras($this->IDGrupo));
        $criteria->compare('LOWER("nome_grupo")', mb_strtolower($this->nome_grupo), true);
        $criteria->compare('LOWER("desc_grupo")', mb_strtolower($this->desc_grupo), true);
        $criteria->compare('"ativo_grupo"', ($this->ativo_grupo), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDGrupo" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Grupo the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getAcoes()
    {
        $permissoes = $this->permissaos(
            array('condition' => '(("dt_fimPermissao" IS NULL) OR ("dt_fimPermissao" > now())) AND (("dt_inicioPermissao" IS NULL) OR ("dt_fimPermissao" < now()))')
        );
        $acoes = array();
        foreach ($permissoes as $permissao) {
            $acoes[] = $permissao->IDAcao;
        }
        return ($acoes);
    }

    public function getListAcoes()
    {
        $permissoes = $this->permissaos(
            array('condition' => '(("dt_fimPermissao" IS NULL) OR ("dt_fimPermissao" > now())) AND (("dt_inicioPermissao" IS NULL) OR ("dt_fimPermissao" < now()))')
        );
        $acoes = '';
        foreach ($permissoes as $permissao) {
            $acoes .= $permissao->iDAcao->nome_acao . ', ';
        }
        return ($acoes);
    }

}
