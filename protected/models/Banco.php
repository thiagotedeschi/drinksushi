<?php

/**
 * This is the model class for table "Banco".
 *
 * The followings are the available columns in table 'Banco':
 * @property integer $IDBanco
 * @property string $nome_banco
 * @property string $cod_banco
 * @property string $site_banco
 * @package base.Models
 */
class Banco extends ActiveRecord
{


    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return 'public.Banco';
    }


    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('nome_banco', 'required'),
            array('nome_banco', 'length', 'max' => 255),
            array('cod_banco', 'length', 'max' => 5),
            array('site_banco', 'length', 'max' => 40),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array();
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDBanco' => 'Banco',
            'nome_banco' => 'Nome Banco',
            'cod_banco' => 'Cod Banco',
            'site_banco' => 'Site Banco',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Banco the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    function __toString()
    {
        return $this->nome_banco;
    }

}
