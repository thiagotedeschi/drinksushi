<?php

/**
 * This is the model class for table "drink.Caracteristicas_Estabelecimento".
 *
 * The followings are the available columns in table 'drink.Caracteristicas_Estabelecimento':
 * @property integer $IDCaracteristicasEstabelecimento
 * @property string $introducao_estabelecimento
 * @property string $objetivo_estabelecimento
 * @property string $titulo_outrasInformacoesEstabelecimento
 * @property string $outras_informacoesEstabelecimento
 * @property string $descricao_basicaEstabelecimento
 * @property string $localidade_estabelecimento
 * @property string $observacao_estabelecimento
 * @property integer $grau_riscoPredominanteEstabelecimento
 * @property integer $IDVigencia
 *
 * The followings are the available model relations:
 * @property Vigencia $iDVigencia
 * @package base.Models
 */
class CaracteristicasEstabelecimento extends ActiveRecord
{

    /**
     * @var int $IDEmpregador Usada apenas na pesquisa. Traz o ID do Empregador a ser pesquisado
     */
    public $IDEmpregador;

    /**
     *
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Caracteristicas_Estabelecimento';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('introducao_estabelecimento, IDVigencia, localidade_estabelecimento, IDEmpregador', 'required'),
            array('grau_riscoPredominanteEstabelecimento, IDVigencia', 'numerical', 'integerOnly' => true),
            array(
                'objetivo_estabelecimento, IDEmpregador, titulo_outrasInformacoesEstabelecimento, outras_informacoesEstabelecimento, descricao_basicaEstabelecimento, observacao_estabelecimento',
                'safe'
            ),
            array(
                'introducao_estabelecimento, IDVigencia',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDVigencia' => array(self::BELONGS_TO, 'Vigencia', 'IDVigencia'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDCaracteristicasEstabelecimento' => 'Características Estabelecimento',
            'introducao_estabelecimento' => 'Introdução',
            'objetivo_estabelecimento' => 'Objetivo',
            'titulo_outrasInformacoesEstabelecimento' => 'Título Outras Informações',
            'outras_informacoesEstabelecimento' => 'Outras Informações',
            'descricao_basicaEstabelecimento' => 'Descrição Básica da Atividade do Estabelecimento',
            'localidade_estabelecimento' => 'Localidade',
            'observacao_estabelecimento' => 'Observação',
            'grau_riscoPredominanteEstabelecimento' => 'Grau de Risco Predominante',
            'IDVigencia' => 'Vigência',
            'IDEmpregador' => 'Empregador',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->with = array("iDVigencia");

        // Como filtrar os resultados pro empregador selecionado.
        $emp = Yii::app()->user->getState('IDEmpregador');
        if ($emp == null) {
            if (isset($this->IDEmpregador)) {
                $criteria->compare('"iDVigencia"."IDEmpregador"', $this->IDEmpregador);
            } else {
                $criteria->addInCondition(
                    '"iDVigencia"."IDEmpregador"',
                    array_keys(Yii::app()->session['empregadores'])
                );
            }
        } else {
            $criteria->compare('"iDVigencia"."IDEmpregador"', $emp);
        }

        $criteria->compare(
            'LOWER("introducao_estabelecimento")',
            mb_strtolower($this->introducao_estabelecimento),
            true
        );
        $criteria->compare('"IDVigencia"', $this->IDVigencia);

        return new CActiveDataProvider(
            $this, array(
                'criteria' => $criteria,
                'Pagination' => array(
                    'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                    //mude o número de registros por página aqui
                ),
                'sort' => array(
                    'defaultOrder' => '"IDCaracteristicasEstabelecimento" DESC',
                )
            )
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CaracteristicasEstabelecimento the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function reduzIntro($length = 80)
    {
        $introducao = substr($this->introducao_estabelecimento, 0, $length);
        if (isset($this->introducao_estabelecimento{$length})) {
            $introducao = trim($introducao);
            $introducao .= '...';
        }

        return $introducao;
    }


    public function findByUltimaVigencia($IDVigencia, $IDEmpregador)
    {
        $vigencia = Vigencia::model()->findByPk($IDVigencia);

        $criteria = new CDbCriteria();
        $criteria->join = ' INNER JOIN ' . CLIENTE . '."Vigencia" AS v ON "t"."IDVigencia" = "v"."IDVigencia"';
        $criteria->join .= ' AND v."IDEmpregador" =' . $IDEmpregador;
        $criteria->addCondition('v."dt_fimVigencia" < \'' . $vigencia->dt_inicioVigencia . '\'');
        $criteria->order = 'v."dt_fimVigencia" DESC';

        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->IDEmpregador = $this->iDVigencia->IDEmpregador;
    }
}
