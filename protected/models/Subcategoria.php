<?php

/**
 * This is the model class for table "drink.Subcategoria".
 *
 * The followings are the available columns in table 'drink.Subcategoria':
 * @property integer $IDSubcategoria
 * @property string $nome_subcategoria
 * @property integer $IDCategoria
 * @property string $titulo_mega
 * @property string $meta_tag
 * @property string $meta_description
 *
 * The followings are the available model relations:
 * @property Categoria $iDCategoria
 * @package base.Models
 */
class Subcategoria extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Subcategoria';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('nome_subcategoria, IDCategoria', 'required'),
            array('IDCategoria', 'numerical', 'integerOnly' => true),
            array('nome_subcategoria, titulo_mega, meta_tag, meta_description', 'length', 'max' => 255),
// @todo Please remove those attributes that should not be searched.
            array(
                'IDSubcategoria, nome_subcategoria, IDCategoria, titulo_mega, meta_tag, meta_description',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDCategoria' => array(self::BELONGS_TO, 'Categoria', 'IDCategoria'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDSubcategoria' => 'Subcategoria',
            'nome_subcategoria' => 'Nome da Subcategoria',
            'IDCategoria' => 'Categoria',
            'titulo_mega' => 'Título',
            'meta_tag' => 'Meta Tag',
            'meta_description' => 'Meta Description',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('"IDSubcategoria"', HTexto::tiraLetras($this->IDSubcategoria));
        $criteria->compare('LOWER("nome_subcategoria")', mb_strtolower($this->nome_subcategoria), true);
        $criteria->compare('"IDCategoria"', $this->IDCategoria);
        $criteria->compare('LOWER("titulo_mega")', mb_strtolower($this->titulo_mega), true);
        $criteria->compare('LOWER("meta_tag")', mb_strtolower($this->meta_tag), true);
        $criteria->compare('LOWER("meta_description")', mb_strtolower($this->meta_description), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDSubcategoria" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Subcategoria the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /*
     *@return o nome da subcategoria
     *@author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     *@since 1.0 04/06/2015
    */
    public function getLabelSubcategoria()
    {
        return $this->nome_subcategoria;
    }

    /*
     *@return Retorna a função getLabelSubcategoria
     *@author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     *@since 1.0 04/06/2015
    */
    public function __toString()
    {
        return $this->getLabelSubcategoria();
    }
}
