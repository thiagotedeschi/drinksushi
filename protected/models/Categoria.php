<?php

/**
 * This is the model class for table "drink.Categoria".
 *
 * The followings are the available columns in table 'drink.Categoria':
 * @property integer $IDCategoria
 * @property integer $IDEmpregador
 * @property string $nome_categoria
 * @property string $titulo_mega
 * @property string $mega_tag
 * @property string $meta_description
 *
 * The followings are the available model relations:
 * @property Empregador $iDEmpregador
 * @property Subcategoria[] $subcategorias
 * @package base.Models
 */
class Categoria extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE.'.Categoria';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDEmpregador, nome_categoria', 'required'),
            array('IDEmpregador', 'numerical', 'integerOnly'=>true),
            array('nome_categoria, titulo_mega, mega_tag, meta_description', 'length', 'max'=>255),
// @todo Please remove those attributes that should not be searched.
            array('IDCategoria, IDEmpregador, nome_categoria, titulo_mega, mega_tag, meta_description', 'safe', 'on'=>'search'),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDEmpregador' => array(self::BELONGS_TO, 'Empregador', 'IDEmpregador'),
            'subcategorias' => array(self::HAS_MANY, 'Subcategoria', 'IDCategoria'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDCategoria' => 'Categoria',
            'IDEmpregador' => 'Restaurante',
            'nome_categoria' => 'Nome da Categoria',
            'titulo_mega' => 'Titulo Mega',
            'mega_tag' => 'Mega Tag',
            'meta_description' => 'Meta Description',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('"IDCategoria"',HTexto::tiraLetras($this->IDCategoria));
        $criteria->compare('"IDEmpregador"',$this->IDEmpregador);
        $criteria->compare('LOWER("nome_categoria")',mb_strtolower($this->nome_categoria),true);
        $criteria->compare('LOWER("titulo_mega")',mb_strtolower($this->titulo_mega),true);
        $criteria->compare('LOWER("mega_tag")',mb_strtolower($this->mega_tag),true);
        $criteria->compare('LOWER("meta_description")',mb_strtolower($this->meta_description),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize']) //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDCategoria" DESC',
            )));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Categoria the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /*
     *@return o nome da categoria
     *@author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     *@since 1.0 03/06/2015
    */
    public function getLabelCategoria()
    {
        return $this->nome_categoria;
    }

    /*
     *@return Retorna a função getLabelCategoria
     *@author Thiago Tedeschi <thiagotedeschi@hotmail.com>
     *@since 1.0 03/06/2015
    */
    public function __toString()
    {
        return $this->getLabelCategoria();
    }
}