<?php

/**
 * This is the model class for table "drink.Setor".
 *
 * The followings are the available columns in table 'drink.Setor':
 * @property integer $IDSetor
 * @property string $nome_setor
 * @property string $dt_levantamentoSetor
 * @property string $dt_cadastroSetor
 * @property integer $IDEmpregador
 *
 * The followings are the available model relations:
 * @property Empregador $iDEmpregador
 * @property SetorFuncao[] $setorFuncaos
 * @package base.Models
 */
class Setor extends ActiveRecord
{

    public function init()
    {
        $this->IDEmpregador = Yii::app()->user->getState('IDEmpregador');
    }

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Setor';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            //    array('IDEmpregador','default','value'=>),
            array('nome_setor, dt_levantamentoSetor, IDEmpregador', 'required'),
            array('dt_levantamentoSetor', 'date', 'format' => 'dd/MM/yyyy'),
            array('IDEmpregador', 'numerical', 'integerOnly' => true),
            array('nome_setor', 'length', 'max' => 255),
// The following rule is used by search().
            array(
                'nome_setor, dt_levantamentoSetor, IDEmpregador',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDEmpregador' => array(self::BELONGS_TO, 'Empregador', 'IDEmpregador'),
            'setorFuncaos' => array(self::HAS_MANY, 'SetorFuncao', 'IDSetor'),
            'funcoes' => array(self::MANY_MANY, 'Funcao', CLIENTE . '.Setor_Funcao(IDSetor, IDFuncao)')
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDSetor' => 'Setor',
            'nome_setor' => 'Nome do Setor',
            'dt_levantamentoSetor' => 'Data de Cadastro',
            'dt_cadastroSetor' => 'Data de Cadastro',
            'IDEmpregador' => 'Empregador',
            'iDEmpregador' => 'Empregador',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;
        $criteria->compare('LOWER("nome_setor")', mb_strtolower($this->nome_setor), true);
        $criteria->compare('"dt_levantamentoSetor"', mb_strtolower($this->dt_levantamentoSetor));

        // Como filtrar os resultados pro empregador selecionado.
        $emp = Yii::app()->user->getState('IDEmpregador');
        if ($emp == null) {
            $criteria->addInCondition('"IDEmpregador"', null);
        } else {
            $criteria->compare('"IDEmpregador"', $emp);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDSetor" DESC',
                'attributes' => array(
                    'dt_levantamentoSetor' => array(
                        'asc' => '"dt_levantamentoSetor"',
                        'desc' => '"dt_levantamentoSetor" DESC',
                    ),
                    '*'
                ),
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Setor the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public static function getSetoresEmpregador($IDEmpregador)
    {
        if ($IDEmpregador == '') {
            return false;
        }
        return $setores = Empregador::model()->findByPk($IDEmpregador)->setors();
    }


    public function getLabelSetor()
    {
        return $this->nome_setor;
    }


    public function __toString()
    {
        return $this->getLabelSetor();
    }


    public function findByEmpregador($emp = null)
    {
        $criteria = new CDbCriteria;
        if ($emp !== null) {
            if (is_array($emp)) {
                $criteria->addInCondition('"IDEmpregador"', array_keys($emp));
            } else {
                $criteria->compare('"IDEmpregador"', $emp);
            }
        } else {
            $emp = Yii::app()->user->getState('IDEmpregador');
            if ($emp == null) {
                $criteria->addInCondition('"IDEmpregador"', array_keys(Yii::app()->session['empregadores']));
            } else {
                $criteria->compare('"IDEmpregador"', $emp);
            }
        }
        return Setor::model()->findAll($criteria);
    }

}
