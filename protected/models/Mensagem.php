<?php

/**
 * This is the model class for table "drink.Mensagem".
 *
 * The followings are the available columns in table 'drink.Mensagem':
 * @property integer $IDMensagem
 * @property string $conteudo_mensagem
 * @property integer $IDUsuarioRemetente
 * @property integer $IDUsuarioDestinatario
 * @property string $dt_envioMensagem
 * @property string $dt_leituraMensagem
 * @property integer $IDMensagemResposta
 *
 * The followings are the available model relations:
 * @property Usuario $iDUsuarioRemetente
 * @property Usuario $iDUsuarioDestinatario
 * @property Mensagem $iDMensagemResposta
 * @property Mensagem[] $mensagems
 * @package base.Models
 */
class Mensagem extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Mensagem';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('conteudo_mensagem, IDUsuarioRemetente, IDUsuarioDestinatario, dt_envioMensagem', 'required'),
            array('IDUsuarioRemetente, IDUsuarioDestinatario, IDMensagemResposta', 'numerical', 'integerOnly' => true),
            array('dt_leituraMensagem', 'safe'),
            array(
                'IDMensagem, conteudo_mensagem, IDUsuarioRemetente, IDUsuarioDestinatario, dt_envioMensagem, dt_leituraMensagem, IDMensagemResposta',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iDUsuarioRemetente' => array(self::BELONGS_TO, 'Usuario', 'IDUsuarioRemetente'),
            'iDUsuarioDestinatario' => array(self::BELONGS_TO, 'Usuario', 'IDUsuarioDestinatario'),
            'iDMensagemResposta' => array(self::BELONGS_TO, 'Mensagem', 'IDMensagemResposta'),
            'mensagems' => array(self::HAS_MANY, 'Mensagem', 'IDMensagemResposta'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDMensagem' => 'Mensagem',
            'conteudo_mensagem' => 'Conteudo Mensagem',
            'IDUsuarioRemetente' => 'Idusuario Remetente',
            'IDUsuarioDestinatario' => 'Idusuario Destinatario',
            'dt_envioMensagem' => 'Dt Envio Mensagem',
            'dt_leituraMensagem' => 'Dt Leitura Mensagem',
            'IDMensagemResposta' => 'Idmensagem Resposta',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('IDMensagem', $this->IDMensagem);
        $criteria->compare('conteudo_mensagem', $this->conteudo_mensagem, true);
        $criteria->compare('IDUsuarioRemetente', $this->IDUsuarioRemetente);
        $criteria->compare('IDUsuarioDestinatario', $this->IDUsuarioDestinatario);
        $criteria->compare('dt_envioMensagem', $this->dt_envioMensagem, true);
        $criteria->compare('dt_leituraMensagem', $this->dt_leituraMensagem, true);
        $criteria->compare('IDMensagemResposta', $this->IDMensagemResposta);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Mensagem the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
