<?php

/**
 * This is the model class for table "drink.Remuneracao_Variavel".
 *
 * The followings are the available columns in table 'drink.Remuneracao_Variavel':
 * @property integer $IDRemuneracaoVariavel
 * @property string $nome_remuneracaoVariavel
 * @property string $desc_remuneracaoVariavel
 *
 * The followings are the available model relations:
 * @property RemuneracaoSalario[] $remuneracaoSalarios
 * @package base.Models
 */
class RemuneracaoVariavel extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Remuneracao_Variavel';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('nome_remuneracaoVariavel, desc_remuneracaoVariavel', 'required'),
            array('nome_remuneracaoVariavel', 'length', 'max' => 50),
            array('desc_remuneracaoVariavel', 'length', 'max' => 255),
// The following rule is used by search().
            array(
                'IDRemuneracaoVariavel, nome_remuneracaoVariavel, desc_remuneracaoVariavel',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'remuneracaoSalarios' => array(self::HAS_MANY, 'RemuneracaoSalario', 'IDRemuneracaoVariavel'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDRemuneracaoVariavel' => 'Remuneração Variável',
            'nome_remuneracaoVariavel' => 'Nome Remuneração Variável',
            'desc_remuneracaoVariavel' => 'Descrição Remuneração Variável',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDRemuneracaoVariavel"', HTexto::tiraLetras($this->IDRemuneracaoVariavel));
        $criteria->compare('LOWER("nome_remuneracaoVariavel")', mb_strtolower($this->nome_remuneracaoVariavel), true);
        $criteria->compare('LOWER("desc_remuneracaoVariavel")', mb_strtolower($this->desc_remuneracaoVariavel), true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDRemuneracaoVariavel" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RemuneracaoVariavel the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
