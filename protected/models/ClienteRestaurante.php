<?php

/**
 * This is the model class for table "drink.Cliente_Restaurante".
 *
 * The followings are the available columns in table 'drink.Cliente_Restaurante':
 * @property integer $IDClienteRestaurante
 * @property string $nome_cliente
 * @property string $telefone_cliente
 * @property integer $IDEnderecoClienteRestaurante
 *
 * The followings are the available model relations:
 * @property EnderecoClienteRestaurante $iDEnderecoClienteRestaurante
 * @package base.Models
 */
class ClienteRestaurante extends ActiveRecord
{

    public $bairro;
    public $rua;
    public $numero;
    public $complemento;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Cliente_Restaurante';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('nome_cliente', 'required'),
            array('IDEnderecoClienteRestaurante', 'numerical', 'integerOnly' => true),
            array('nome_cliente', 'length', 'max' => 200),
            array('telefone_cliente', 'length', 'max' => 60),
            array('ddd_telefone', 'length', 'max' => 5),
// @todo Please remove those attributes that should not be searched.
            array(
                'IDClienteRestaurante, nome_cliente, telefone_cliente, IDEnderecoClienteRestaurante, ddd_telefone',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDEnderecoClienteRestaurante' => array(
                self::BELONGS_TO,
                'EnderecoClienteRestaurante',
                'IDEnderecoClienteRestaurante'
            ),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDClienteRestaurante' => 'Cliente',
            'nome_cliente' => 'Nome',
            'telefone_cliente' => 'Telefone',
            'IDEnderecoClienteRestaurante' => 'Endereço',
            'ddd_telefone' => 'Ddd Telefone',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('"IDClienteRestaurante"', HTexto::tiraLetras($this->IDClienteRestaurante));
        $criteria->compare('LOWER("nome_cliente")', mb_strtolower($this->nome_cliente), true);
        $criteria->compare('LOWER("telefone_cliente")', mb_strtolower($this->telefone_cliente), true);
        $criteria->compare('"IDEnderecoClienteRestaurante"', $this->IDEnderecoClienteRestaurante);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDClienteRestaurante" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ClienteRestaurante the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
