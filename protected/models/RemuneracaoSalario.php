<?php

/**
 * This is the model class for table "drink.Remuneracao_salario".
 *
 * The followings are the available columns in table 'drink.Remuneracao_salario':
 * @property integer $IDRemuneracaoSalario
 * @property integer $IDSalario
 * @property integer $IDRemuneracaoVariavel
 * @property double $valor_remuneracao
 * @property string $dt_referenciaRemuneracao
 * @property string $obs_remuneracao
 * @property integer $mes_referenciaRemuneracao
 *
 * The followings are the available model relations:
 * @property RemuneracaoVariavel $iDRemuneracaoVariavel
 * @property SalarioProfissional $iDSalario
 * @package base.Models
 */
class RemuneracaoSalario extends ActiveRecord
{

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Remuneracao_salario';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array(
                'IDSalario, IDRemuneracaoVariavel, valor_remuneracao, dt_referenciaRemuneracao, mes_referenciaRemuneracao',
                'required'
            ),
            array('IDSalario, IDRemuneracaoVariavel, mes_referenciaRemuneracao', 'numerical', 'integerOnly' => true),
            array('valor_remuneracao', 'numerical'),
            array('obs_remuneracao', 'length', 'max' => 255),
// The following rule is used by search().
            array(
                'IDRemuneracaoSalario, IDSalario, IDRemuneracaoVariavel, valor_remuneracao, dt_referenciaRemuneracao, obs_remuneracao, mes_referenciaRemuneracao',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'iDRemuneracaoVariavel' => array(self::BELONGS_TO, 'RemuneracaoVariavel', 'IDRemuneracaoVariavel'),
            'iDSalario' => array(self::BELONGS_TO, 'SalarioProfissional', 'IDSalario'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDRemuneracaoSalario' => 'Remuneração de Profissionais',
            'IDSalario' => 'IDSalário',
            'IDRemuneracaoVariavel' => 'IDRemuneração Variável',
            'valor_remuneracao' => 'Valor Remuneração',
            'dt_referenciaRemuneracao' => 'Data Referência Remuneração',
            'obs_remuneracao' => 'Obs Remuneração',
            'mes_referenciaRemuneracao' => 'Mês Referência Remuneração',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('"IDRemuneracaoSalario"', HTexto::tiraLetras($this->IDRemuneracaoSalario));
        $criteria->compare('"IDSalario"', HTexto::tiraLetras($this->IDSalario));
        $criteria->compare('"IDRemuneracaoVariavel"', HTexto::tiraLetras($this->IDRemuneracaoVariavel));
        $criteria->compare('"valor_remuneracao"', $this->valor_remuneracao);
        $criteria->compare('LOWER("dt_referenciaRemuneracao")', mb_strtolower($this->dt_referenciaRemuneracao), true);
        $criteria->compare('LOWER("obs_remuneracao")', mb_strtolower($this->obs_remuneracao), true);
        $criteria->compare('"mes_referenciaRemuneracao"', $this->mes_referenciaRemuneracao);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDRemuneracaoVariavel" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RemuneracaoSalario the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
