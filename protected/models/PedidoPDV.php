<?php

/**
 * This is the model class for table "drink.Pedido_PDV".
 *
 * The followings are the available columns in table 'drink.Pedido_PDV':
 * @property integer $IDPedidoPDV
 * @property string $mesa
 * @property string $dt_aberturaPedido
 * @property boolean $embalagem
 * @property string $desconto
 * @property string $total_pedido
 * @property string $observacao_pedido
 *
 * The followings are the available model relations:
 * @property PedidoProduto[] $pedidoProdutos
 * @package base.Models
 */
class PedidoPDV extends ActiveRecord
{

    public $hora;

    public $subtotal;

    public $total_ped;

    public $troco_din;

    /**
     * Retorna o nome da tabela representada pelo Modelo.
     *
     * @return string nome da tabela
     */
    public function tableName()
    {
        return CLIENTE . '.Pedido_PDV';
    }

    /**
     * Retorna as regras de validação para o Modelo
     * @return Array Regras de Validação.
     */
    public function rules()
    {
        return array(
            array('IDEstadoPedido', 'numerical', 'integerOnly' => true),
            array('mesa', 'length', 'max' => 100),
            array(
                'desconto, total_pedido, dinheiro, cartao_debito, cartao_credito, troco_dinheiro',
                'length',
                'max' => 11
            ),
            array(
                'dt_aberturaPedido, embalagem, observacao_pedido, total_pedido, desconto, dinheiro, cartao_debito, cartao_credito, troco_dinheiro',
                'safe'
            ),
// @todo Please remove those attributes that should not be searched.
            array(
                'IDPedidoPDV, mesa, dt_aberturaPedido, embalagem, desconto, total_pedido, observacao_pedido, dinheiro, cartao_debito, cartao_credito, troco_dinheiro, IDEstadoPedido, motivo_cancelamento',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retorna as relações do modelo
     * @return Array relações
     */
    public function relations()
    {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'pedidoProdutos' => array(self::HAS_MANY, 'PedidoProduto', 'IDPedido'),
            'iDEstadoPedido' => array(self::BELONGS_TO, 'EstadoPedido', 'IDEstadoPedido'),
        );
    }

    /**
     * Retorna as labels dos atributos do modelo no formato (atributo=>label)
     * @return Array labels dos atributos.
     */
    public function attributeLabels()
    {
        return array(
            'IDPedidoPDV' => 'Pedido Pdv',
            'mesa' => 'Mesa',
            'dt_aberturaPedido' => 'Data',
            'embalagem' => 'Embalagem',
            'desconto' => 'Desconto',
            'total_pedido' => 'Total Pedido',
            'observacao_pedido' => 'Observacao Pedido',
            'hora' => 'Hora',
            'dinheiro' => 'Dinheiro',
            'cartao_debito' => 'Cartão de Débito',
            'cartao_credito' => 'Cartão de Crédito',
            'troco_dinheiro' => 'Troco',
            'IDEstadoPedido' => 'Status do Pedido',
            'motivo_cancelamento' => 'Motivo do Cancelamento',
        );
    }

    /**
     * Retorna uma lista de modelos baseada nas definições de filtro da tabela
     * @return CActiveDataProvider o DataProvider para a renderização da tabela (com models ou não)
     */
    public function search()
    {
// @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('"IDPedidoPDV"', HTexto::tiraLetras($this->IDPedidoPDV));
        $criteria->compare('LOWER("mesa")', mb_strtolower($this->mesa), true);
        $criteria->compare('LOWER("dt_aberturaPedido")', mb_strtolower($this->dt_aberturaPedido), true);
        $criteria->compare('"embalagem"', $this->embalagem);
        $criteria->compare('LOWER("desconto")', mb_strtolower($this->desconto), true);
        $criteria->compare('LOWER("total_pedido")', mb_strtolower($this->total_pedido), true);
        $criteria->compare('LOWER("observacao_pedido")', mb_strtolower($this->observacao_pedido), true);
        $criteria->compare('LOWER("dinheiro")', mb_strtolower($this->dinheiro), true);
        $criteria->compare('LOWER("cartao_debito")', mb_strtolower($this->cartao_debito), true);
        $criteria->compare('LOWER("cartao_credito")', mb_strtolower($this->cartao_credito), true);
        $criteria->compare('LOWER("troco_dinheiro")', mb_strtolower($this->troco_dinheiro), true);
        $criteria->compare('"IDEstadoPedido"', $this->IDEstadoPedido);
        $criteria->compare('LOWER("motivo_cancelamento")', mb_strtolower($this->motivo_cancelamento), true);
        $criteria->addCondition('"IDEstadoPedido" != 3', 'AND');

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'Pagination' => array(
                'pageSize' => Yii::app()->user->getState('pageSize', Yii::app()->params['defaultPageSize'])
                //mude o número de registros por página aqui
            ),
            'sort' => array(
                'defaultOrder' => '"IDPedidoPDV" DESC',
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PedidoPDV the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
