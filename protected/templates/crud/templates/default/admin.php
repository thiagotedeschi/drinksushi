<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label=$this->pluralize($this->class2name($this->modelClass));
?>
//Carrega a Widget para as Dicas
$this->widget('application.widgets.WDicas', array('dicas' => $this->dicas));


?>

<p class="note note-warning">
Você pode também usar caracteres comparadores (<b>&lt;</b>, <b>&gt;</b> ou <b>&lt;&gt;</b>) No início de cada pesquisa.
</p>


<?php echo "<?php"; ?> $this->widget('application.widgets.grid.GridView', array(
	'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
            'selectableRows' => 2,
	'columns'=>array(
            array(
                'id' => 'selectedIds',
                'class' => 'CheckBoxColumn'
            ),

<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==4)
		echo "\t\t/*\n";
	echo "\t\t'".$column->name."',\n";
}
if($count>=4)
	echo "\t\t*/\n";
?>
            array(
                 'class' => 'ButtonColumn',
                 'template' => '{view}{update}{delete}',
                 'buttons' =>
                 array(
                      'view' => array(
                           'visible' => ($this->verificaAcao('view') ? 'true' : 'false')
                      ),
                      'update' => array(
                           'visible' => ($this->verificaAcao('update') ? 'true' : 'false')
                      ),
                      'delete' => array(
                           'visible' => ($this->verificaAcao('delete') ? 'true' : 'false')
                      ),
                 ),
            ),
	),
)); ?>
