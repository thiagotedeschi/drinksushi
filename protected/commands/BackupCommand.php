<?php


class BackupCommand extends ConsoleCommand
{
    /**
     * @var array tabelas que não vão entrar no backup
     */
    public $excludeTables = array("Log", "Historico_Login");
    /**
     * @var Arquivos que farão parte do bakcup
     */
    public $includeFiles = array('/etc/init.d/networking');
    /**
     * @var array pastas que farão parte do backup
     */
    public $includeFolders = array('/etc/apache2', '/etc/postgresql');
    /**
     * @var array schemas que farão parte do backup (padrão todos os relacionados a projetos ativos + public)
     */
    public $includeSchemas = array();

    /**
     * @var string nome do arquivo de log
     */
    public $logFileName = 'backup.log';
    /**
     * @var string prefixo das unidades de backup
     */
    public $folderNamePrefix = 'bkp_';
    /**
     * @var formato padrão do nome das unidades de backup
     */
    public $folderTimeStampName = 'dxmxY-His';
    /**
     * @var diretório no qual será executado o processo backup
     */
    public $workingDirectory = '';
    /**
     * @var configuração da conexão ao banco de dados
     */
    public $dbConfig = array();
    /**
     * @var string nome da pasta onde estarão os arquivos do backup
     */
    public $filesFolderName = 'files';
    /**
     * @var string nome da pasta onde estarão os SQL do backup
     */
    public $dataFolderName = 'data';

    /**
     * @var string nome completo da pasta onde está sendo executado o backup
     */
    public $backupFolderName = '';
    /**
     * @var bool se deve ser feito o backup da svn ou não
     */
    public $backupSvn = true;
    /**
     * @var caminho do repositório svn
     */
    public $svnPath = '/var/svn/projetos';
    /**
     * @var string nome do arquivo output do repositório svn
     */
    public $svnFileName = 'svn';
    /**
     * @var string pasta onde ficarão salvos os backups produzidos
     */
    public $backupWarehouse = '/backup';
    /**
     * @var array comandos executados e suas saidas para fins de log
     */
    public $commandOutputStack = [];


    public function initConfig($iFiles, $iFolders, $iSchemas, $eTables, $svn)
    {
        $config = HTexto::explodeDbConnectionString();
        $config['username'] = Yii::app()->db->username;
        $config['password'] = Yii::app()->db->password;
        $this->dbConfig = $config;
        $this->workingDirectory = Yii::app()->runtimePath;
        $this->backupFolderName = $this->folderNamePrefix . date($this->folderTimeStampName);

        if (!empty($iFiles)) {
            $this->includeFiles = explode(',', $iFiles);
            if (array_search('none', $this->includeFiles) !== false) {
                $this->includeFiles = [];
            }
        }
        if (!empty($iFolders)) {
            $this->includeFolders = explode(',', $iFolders);
            if (array_search('none', $this->includeFolders) !== false) {
                $this->includeFolders = [];
            }
        }
        if (!empty($iSchemas)) {
            $this->includeSchemas = explode(',', $iSchemas);
            if (array_search('none', $this->includeSchemas) !== false) {
                $this->includeSchemas = [];
            }
        } else {
            $schemas = Yii::app()->db->createCommand()
                ->select('slug_projeto')
                ->from('public.Projeto')
                ->where(
                    '(("dt_fimProjeto" IS NULL) OR ("dt_fimProjeto" > now()))
                     AND (("dt_inicioProjeto" IS NULL)
                     OR ("dt_inicioProjeto" < now())) AND ativo_projeto=true'
                )
                ->queryColumn();

            $schemas[] = 'public';
            $this->includeSchemas = $schemas;
        }
        if (!empty($iTables)) {
            $this->includeTables = explode(',', $iTables);
        }
        if (!empty($eTables)) {
            $this->excludeTables = explode(',', $eTables);
            if (array_search('none', $this->excludeTables) !== false) {
                $this->excludeTables = [];
            }
        }
        if (!empty($svn)) {
            $this->backupSvn = $svn == 'true' ? true : false;
        }

    }



    public function actionExecBackup($iFi = '', $iFo = '', $iSc = '', $eTa = '', $svn = '')
    {
        ob_start();

        //** configuração e criação das pastas */
        $this->initConfig($iFi, $iFo, $iSc, $eTa, $svn);
        $folder = $this->getBackupDirectoryPath();
        $dataFolder = $this->getDataDirectoryPath();
        $fileFolder = $this->getFileDirectoryPath();
        $this->exec('mkdir ' . $folder);
        $this->exec('mkdir ' . $fileFolder);
        $this->exec('mkdir ' . $dataFolder);
        /** fim da configuração e criação das pastas */

        //** backup dos arquivos */
        foreach ($this->includeFiles as $file) {
            HDiretorio::copy($file, $fileFolder);
        }
        /** fim do backup dos arquivos */

        /** backup das pastas */
        foreach ($this->includeFolders as $aFolder) {
            HDiretorio::copy($aFolder, $fileFolder);
        }
        /** fim do backup das pastas */

        /** backup dos schemas */
        $passwd = ("export PGPASSWORD=\"{$this->dbConfig['password']}\"");

        echo 'Schemas incluidos no backup: \n\n';
        print_r($this->includeSchemas);

        foreach ($this->includeSchemas as $schema) {

            $eTables = empty($this->excludeTables) ? '' : ' --exclude-table-data="' . $schema . '.' . implode(
                    '" --exclude-table-data="' . $schema . '.',
                    $this->excludeTables
                ) . '"';
            echo '\n\nbackup do ' . $schema . ': \n\n';
            $dump = $this->getDataDirectoryPath() . DIRECTORY_SEPARATOR . $schema . '.dump';
            $this->exec(
                "$passwd ;pg_dump -Fc -w -n $schema -U {$this->dbConfig['username']} -h {$this->dbConfig['host']} -p {$this->dbConfig['port']} $eTables {$this->dbConfig['dbname']} > $dump"
            );
        }
        /** fim do backup das schemas */


        /** backup do repositório svn */
        if ($this->backupSvn) {
            $dump = $folder . DIRECTORY_SEPARATOR . $this->svnFileName . '.svndump';
            $this->exec("svnadmin dump $this->svnPath > $dump");
        }
        /** fim do backup do svn */

        /** registrando o log */
        $logPath = $folder . DIRECTORY_SEPARATOR . $this->logFileName;

        echo '\n\n Comandos (e outputs) que foram executados: ';
        print_r($this->commandOutputStack);
        $history = ob_get_flush();
        $this->exec("echo \"$history\" > $logPath");
        /** fim do registro do log */

        /** compatando, movendo e limpando o backup */
        if (!is_dir($this->backupWarehouse)) {
            $this->exec("mkdir $this->backupWarehouse -m 700");
        }
        $backupFileName = $this->backupWarehouse . DIRECTORY_SEPARATOR . $this->backupFolderName;

        $this->exec("cd $folder; tar -zcvf $backupFileName.tar.gz ./");

        $this->exec('rm -R ' . $folder);
    }

    public function getBackupDirectoryPath()
    {
        return $this->workingDirectory . DIRECTORY_SEPARATOR . $this->backupFolderName;
    }


    public function getFileDirectoryPath()
    {
        return $this->getBackUpDirectoryPath() . DIRECTORY_SEPARATOR . $this->filesFolderName;
    }


    public function getDataDirectoryPath()
    {
        return $this->getBackupDirectoryPath() . DIRECTORY_SEPARATOR . $this->dataFolderName;
    }


    public function exec($command)
    {
        exec($command, $out, $cod);
        $this->commandOutputStack[$command] = 'Status #' . $cod . ': ' . print_r($out, true);
    }


}
