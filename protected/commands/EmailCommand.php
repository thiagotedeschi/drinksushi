<?php


class EmailCommand extends ConsoleCommand
{

    public $layout = 'email';

    public $templatePath = 'application.views.email';


    public static function enviaEmail($dados)
    {
        $mail = Yii::app()->Smtpmail;
        $mail->Subject = '[' . CLIENTE . '] ' . $dados['assunto'];
        $mail->MsgHTML($dados['conteudo']);
        $mail->FromName = HAtributos::getGlobal('NOME_SISTEMA');
        foreach ($dados['destinatarios'] as $email => $nome) {
            $mail->AddAddress($email, $nome);
        }
        return ($mail->Send());
    }


    public function actionNotificacao($id)
    {
        $notificacao = Notificacao::model()->findByPk($id);
        if ($notificacao != null) {
            $prof = $notificacao->iDProfissional();
            self::enviaEmail(
                array(
                    'assunto' => 'Você Recebeu uma Notificação do Sistema',
                    'destinatarios' => array($prof->email_profissional => $prof->nome_profissional),
                    'conteudo' => $this->render(
                            'notificacao',
                            array(
                                'prof' => $prof,
                                'notificacao' => $notificacao
                            ),
                            true
                        )
                )
            );
        }
    }


    public function actionNovoUsuario($id, $senha)
    {
        $usuario = Usuario::model()->findByPk($id);
        $prof = $usuario->iDProfissional();
        self::enviaEmail(
            array(
                'assunto' => 'Usuário Cadastrado',
                'destinatarios' => array($prof->email_profissional => $prof->nome_profissional),
                'conteudo' => $this->render(
                        'novoUsuario',
                        array(
                            'prof' => $prof,
                            'usuario' => $usuario,
                            'senha' => $senha
                        ),
                        true
                    )
            )
        );

    }


    public function actionTrocaSenhaForcada($id, $senha)
    {
        $usuario = Usuario::model()->findByPk($id);
        $prof = $usuario->iDProfissional();
        self::enviaEmail(
            array(
                'assunto' => 'Troca Senha Forçada',
                'destinatarios' => array($prof->email_profissional => $prof->nome_profissional),
                'conteudo' => $this->render(
                        'trocaSenhaForcada',
                        array(
                            'prof' => $prof,
                            'usuario' => $usuario,
                            'senha' => $senha
                        ),
                        true
                    ),
            )
        );
    }


    public function actionEsqueciMinhaSenha($profissional, $senha)
    {
        $profissional = Profissional::model()->findByPk($profissional);
        $usuario = $profissional->iDUsuario;
        self::enviaEmail(
            array(
                'assunto' => 'Pedido de Troca de Senha',
                'destinatarios' => array($profissional->email_profissional => $profissional->nome_profissional),
                'conteudo' => $this->render(
                        'esqueciMinhaSenha',
                        array(
                            'prof' => $profissional,
                            'usuario' => $usuario,
                            'senha' => $senha
                        ),
                        true
                    ),
            )
        );
    }


    public function actionContato($id, $msg)
    {
        $prof = Profissional::model()->findByPk($id);
        $result = self::enviaEmail(
            array(
                'assunto' => 'Contato do ' . HAtributos::getGlobal('NOME_REDUZIDO_SISTEMA'),
                'destinatarios' => array(HAtributos::getGlobal('EMAIL_CONTATO') => 'Contato'),
                'conteudo' => $this->render(
                        'contato',
                        array(
                            'prof' => $prof,
                            'mensagem' => $msg
                        ),
                        true
                    ),
            )
        );
    }


    public function actionComunicado($IDContato, $conteudo, $assunto)
    {
        $contato = Contato::model()->findByPk($IDContato);
        if ($contato) {
            self::enviaEmail(
                array(
                    'assunto' => $assunto,
                    'destinatarios' => array($contato->nome_contato, $contato->emailPrincipal),
                    'conteudo' => $this->render('comunicado', array('mensagem' => $conteudo), true)
                )
            );
        }
    }
}
