<?php

class ConsoleCommand extends CConsoleCommand
{

    protected function renderPartial($template, array $data = array())
    {
        $path = Yii::getPathOfAlias($this->templatePath) . DIRECTORY_SEPARATOR . $template . '.php';
        if (!file_exists($path)) {
            throw new Exception('Template ' . $path . ' does not exist.');
        }
        return $this->renderFile($path, $data, true);
    }

    protected function render($template, array $data = array())
    {
        $output = $this->renderPartial($template, $data);
        return $layout = $this->renderPartial($this->layout, array('content' => $output));
    }

    public function init()
    {
        UserIdentity::setAtributos();
        return parent::init();
    }

}
