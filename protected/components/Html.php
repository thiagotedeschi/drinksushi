<?php


class Html extends CHtml
{


    public static function  htmlList($data, $label = '', $htmlOptions = array(), $listTag = 'ul')
    {
        if (count($data) > 0) {
            $lista = '';
            foreach ($data as $item) {
                if (is_string($item)) {
                    $value = $item;
                } else {
                    $value = $item->$label;
                }
                $lista .= self::tag(
                    'li',
                    array(),
                    $value
                );
            }
            $lista = self::tag($listTag, $htmlOptions, $lista);
            return $lista;
        }
        return null;
    }


    public static function activeList($model, $attribute, $label, $htmlOptions = array(), $listTag = 'ul')
    {
        if (isset($model->$attribute)) {
            return self::htmlList($model->$attribute, $label, $htmlOptions, $listTag);
        }
    }

}