<?php

/**
 * @package base.Validators
 */
class ValidadorDocumento extends CValidator
{

    protected function validateAttribute($object, $attribute)
    {
        $cpf = str_replace(array('.', '-', '/'), '', $object->$attribute);
        $cpfValido = $this->validaCpf($cpf);

        $cnpj = str_replace(array('.', '-', '/'), '', $object->$attribute);
        $cnpjValido = $this->validaCnpj($cnpj);
        if (!$cnpjValido && !$cpfValido) {
            $this->addError($object, $attribute, 'Documento Inválido.');
        }
    }

    public function clientValidateAttribute($object, $attribute)
    {
        $txt = "
                var cpf = value;
            cpf = cpf.replace(/[^\d]+/g,'');
            if(cpf == '') return false;

            if (cpf.length != 11 || 
                cpf == '00000000000' || 
                cpf == '11111111111' || 
                cpf == '22222222222' || 
                cpf == '33333333333' || 
                cpf == '44444444444' || 
                cpf == '55555555555' || 
                cpf == '66666666666' || 
                cpf == '77777777777' || 
                cpf == '88888888888' || 
                cpf == '99999999999')
                messages.push(" . CJSON::encode('Documento Inválido.') . ");

            add = 0;
            for (i=0; i < 9; i ++)
                add += parseInt(cpf.charAt(i)) * (10 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != parseInt(cpf.charAt(9)))
                messages.push(" . CJSON::encode('Documento Inválido.') . ");

            add = 0;
            for (i = 0; i < 10; i ++)
                add += parseInt(cpf.charAt(i)) * (11 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != parseInt(cpf.charAt(10)))
                messages.push(" . CJSON::encode('Documento Inválido.') . ");

            return true;
        ";
        return $txt;
    }

    private function validaCpf($cpf)
    {
        // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
        if (strlen(
                $cpf
            ) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999'
        ) {
            return false;
        }

        // Calcula os números para verificar se o CPF é verdadeiro
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }

            $d = ((10 * $d) % 11) % 10;

            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }

    private function validaCnpj($cnpj)
    {

        // valida nÃºmero sequencial 1111... 22222 ......
        for ($x = 0; $x < 10; $x++) {
            if ($cnpj == str_repeat($x, 14)) {
                return false;
            }
        }

        // Verifica se nenhuma das sequÃªncias abaixo foi digitada, caso seja, retorna falso
        if (strlen($cnpj) != 14) {
            return false;
        } else { // Calcula os nÃºmeros para verificar se o CNPJ Ã© verdadeiro
            for ($t = 12; $t < 14; $t++) {
                $d = 0;
                $c = 0;
                for ($m = $t - 7; $m >= 2; $m--, $c++) {
                    $d += $cnpj{$c} * $m;
                }
                for ($m = 9; $m >= 2; $m--, $c++) {
                    $d += $cnpj{$c} * $m;
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cnpj{$c} != $d) {
                    return false;
                }
            }
            return true;
        }
    }

}

?>
