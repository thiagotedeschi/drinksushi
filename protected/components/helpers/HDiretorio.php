<?php

/**
 * Este Helper engloba funções estáticas auxiliares usadas nas operações
 * relacionadas a diretórios e arquivos do sistema operacional
 * @package base.Helpers
 */
class HDiretorio
{


    public function limparPasta($pasta)
    {
        if (is_dir($pasta)) {
            $diretorio = dir($pasta);

            while ($arquivo = $diretorio->read()) {
                if (($arquivo != '.') && ($arquivo != '..') && !is_dir($arquivo)) {
                    unlink($pasta . $arquivo);
                }
            }

            $diretorio->close();
        } else {
            return false;
        }
        return true;
    }


    public static function isLinux()
    {
        return (DIRECTORY_SEPARATOR == '/');
    }


    public static function copy($from, $to)
    {
        if (self::isLinux()) {
            if (is_dir($from)) {
                $command = "cp -R $from $to";
            } else {
                $command = "cp $from $to";
            }
            exec($command, $output);
            return ["$command" => implode(' \n ', $output)];
        }
        return copy($from, $to);

    }

}
