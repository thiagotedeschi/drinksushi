<?php

class HPassword
{
    //CONSTANTES QUE DEFINEM O TAMANHO MÍNIMO DA SENHA PARA SE ENCAIXAR EM CADA CATEGORIA
    const WEAK_PASS_LENGTH = 4;
    const AVERAGE_PASS_LENGTH = 6;
    const STRONG_PASS_LENGTH = 8;
    const BADASS_PASS_LENGTH = 10;

    static function gerarPassword($length = self::STRONG_PASS_LENGTH)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%";
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

}

?>
