<?php


class HAtributos
{

    public static function getGlobal($param)
    {
        $session = Yii::app()->session;
        if (isset($session['atributosGlobais'][$param])) {
            return $session['atributosGlobais'][$param];
        } else {
            return null;
        }
    }

    public static function getLocal($param)
    {
        $session = Yii::app()->session;
        if (isset($session['atributosCliente'][$param])) {
            return $session['atributosCliente'][$param];
        } else {
            return self::getGlobal($param);
        }
    }

}

?>
