<?php


class HTexto
{

    /**
     * Codificação Default de todo o sistema
     */
    CONST DEFAULT_ENCODING = 'utf-8';

    /**
     * Linguagem padrão de todo o sistema
     */
    CONST DEFAULT_LANGUAGE = 'pt-br';

    /**
     * Máscara padrão para CPF
     */
    CONST MASK_CPF = '999.999.999-99';

    /**
     * Máscara Padrão para CNPJ
     */
    CONST MASK_CNPJ = '99.999.999/9999-99';

    /**
     * Máscara Padrão para CNAE 2.0
     */
    CONST MASK_CNAE = '9999-9/99';

    /**
     * Máscara Padrão para o grupo do CNAE 2.0
     */
    CONST GRUPO_CNAE_MASK = '9999-9';

    /**
     * Máscara Padrão para CEP
     */
    CONST MASK_CEP = '99999-999';

    /**
     * Prefixo do formato padrão de moeda
     */
    CONST MOEDA_BRL = 'R$';

    static $tiposPessoa = array(
        'j' => 'Jurídica',
        'f' => 'Física',
        'p' => 'Pública'
    );


    public static function tiraAcentos($string)
    {
        return preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities($string));
    }


    public static function tiraLetras($string, $manterComparadores = true, $manterFloats = false)
    {
        if ($manterComparadores) {
            return preg_replace("/[^0-9><-]/", "", $string);
        }
        if ($manterFloats) {
            return preg_replace("/[^0-9.><-]/", "", $string);
        }
        return self::somenteNumeros($string);
    }


    public static function formataTelefone($ddd = '', $tel)
    {
        $tel = str_replace(array('.', '-'), '', $tel);
        $ddd = str_replace(array('.', '-'), '', $ddd);
        if ($ddd != '') {
            $ddd = '(' . trim($ddd) . ') ';
        }
        $tel = substr($tel, 0, strlen($tel) - 4) . '-' . substr($tel, -4); // retorna "ef"
        return $ddd . $tel;
    }


    public static function somenteNumeros($string)
    {
        return preg_replace("/[^0-9]/", "", $string);
    }

    public static function formataString($mascara, $string, $pad = '', $pad_length = 0, $pad_type = STR_PAD_RIGHT)
    {
        $mascara = str_replace("9", "#", $mascara);
        if ($pad != '') {
            $string = str_pad($string, $pad_length, $pad, $pad_type);
        }

        $string = str_replace(" ", "", $string);
        for ($i = 0; $i < strlen($string); $i++) {
            $mascara[strpos($mascara, "#")] = $string[$i];
        }
        return $mascara;
    }

    /**
     * Retorna a string $string sem nenhum número, mantendo todos os outros tipos de caracteres
     * @param String $string a ser processada
     * @return string texto sem caracteres alfanuméricos
     */
    public static function somenteLetras($string)
    {
        return str_replace(range(0, 9), null, $string);
    }

    public static function bindValues($texto, $parametros = array())
    {
        foreach ($parametros as $parametro => $valor) {
            $texto = str_replace('{{' . $parametro . '}}', $valor, $texto);
        }
        return $texto;
    }


    public static function explodeDbConnectionString($connectionString = '')
    {
        if (empty($connectionString)) {
            $connectionString = Yii::app()->db->connectionString;
        }
        $data = explode(':', $connectionString);
        $return['db'] = $data[0];
        $data = $data[1];
        $data = explode(';', $data);

        foreach ($data as $field) {
            $values = explode('=', $field);
            $return[$values[0]] = $values[1];
        }
        return $return;
    }


    public static function getTextoPadrao($context, $text, $params = array())
    {
        return Yii::app()->controller->renderPartial(
            'application.views.textoPadrao.' . $context . '._' . $text,
            $params,
            true
        );
    }


    public static function formataMoeda($prefixo, $valor)
    {
        $valorFormatado = number_format($valor,2,',','.');
        $valorFormatado = $prefixo . " " . $valorFormatado;
        return $valorFormatado;
    }


    public static function formataSQLMoeda($valor)
    {
        $valorFormatado = number_format($valor,2,'.',',');
        return $valorFormatado;
    }


    public static function formataMoedaSemPrefixo($valor)
    {
        $valorFormatado = number_format($valor,2,',','.');
        return $valorFormatado;
    }


}

?>
