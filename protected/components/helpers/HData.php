<?php


class HData
{

    /**
     * Formato sql de data<BR />
     * Exemplo: 2013-10-28
     */
    CONST SQL_DATE_FORMAT = 'Y-m-d';

    /**
     * Formato brasileiro de data<BR />
     * Exemplo: 28/10/2013
     */
    CONST BR_DATE_FORMAT = 'd/m/Y';

    /**
     * Formato simplificado de data brasileiro<BR />
     * Exemplo: 28/10
     */
    CONST BR_DATE_SIMPLE_FORMAT = 'd/m';

    /**
     * Formato sql de hora<BR />
     * Exemplo: 15:12:59
     */
    CONST SQL_TIME_FORMAT = 'H:i:s';

    /**
     * Formato brasileiro de hora<BR />
     * Exemplo: 15:12:59
     */
    CONST BR_TIME_FORMAT = 'H:i:s';

    /**
     * Formato brasileiro simplificado de hora<BR />
     * Exemplo: 15:12
     */
    CONST BR_TIME_SIMPLE_FORMAT = 'H:i';

    /**
     * Formato sql de data e hora<BR />
     * Exemplo: 2013-10-28 15:12:59
     */
    CONST SQL_DATETIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * Formato brasileiro de data e hora<BR />
     * Exemplo: 28/10/2013 15:12:59
     */
    CONST BR_DATETIME_FORMAT = 'd/m/Y H:i:s';

    /**
     * Formato brasileiro simplificado de data e hora<BR />
     * Exemplo: 28/10 15:12
     */
    CONST BR_DATETIME_SIMPLE_FORMAT = 'd/m H:i';

    /**
     * dias da semana em ordem
     * @var diaSemana array
     */
    static $diaSemana = array('Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado');

    /**
     * dias da semana abreviados e em ordem
     * @var diaSemanaAbrev array
     */
    static $diaSemanaAbrev = array('Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab');

    /**
     * Meses do ano em ordem
     * @var $mes array
     */
    static $mes = array(
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'
    );


    static $periodos = array(
        '1' => 'Diário',
        '7' => 'Semanal',
        '15' => 'Quinzenal',
        '30' => 'Mensal',
        '90' => 'Trimestral',
        '180' => 'Semestral',
        '365' => 'Anual',
        '730' => 'Bianual',
        '1095' => 'Trienal',
        '1460' => 'Quadrienal',
        null => 'Indeterminada',
    );


    static $periodosMeses = array(
        '2' => 'Bimestral',
        '3' => 'Trimestal',
        '6' => 'Semestral',
        '12' => 'Anual',
        '24' => 'Bianual',
        '36' => 'Trienal',
        '60' => 'Quinquenal',
    );
    /**
     * Meses do ano abreviados e em ordem
     * @var $mesAbrev array
     */
    static $mesAbrev = array('Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez');

    /**
     * Idade para ser considerado penalmente imputável
     */
    CONST MAIOR_IDADE = 18;


    public static function dataInteligente($data)
    {
        $hoje = new DateTime(self::hoje(HData::SQL_DATE_FORMAT));
        $data = new DateTime(explode(' ', $data)[0]);
        $diff = $hoje->diff($data);
        $dias = $diff->d;
        //   var_dump($diff);die;
        if ($dias == 0) {
            return 'Hoje';
        }

        if ($dias == 1 && $data < $hoje) {
            return 'Ontem';
        }

        if ($dias == 1 && $data > $hoje) {
            return 'Amanhã';
        }


        if ($data->format('W') == $hoje->format('W')) {
            return HData::$diaSemana[$data->format('w') - 1];
        }

        if ($dias < 5 && $dias > 0) {
            return $dias == 1 ? 'Há ' . $dias . ' dia' : 'Há ' . $dias . ' dias';
        }

        if ($dias < -2) {
            return 'Daqui há ' . $dias * -1 . ' dias';
        }

        if ($dias > 5 && ($hoje->format('Y') == $data->format('Y'))) {
            return $data->format('d') . ' de ' . HData::$mesAbrev[$data->format('m') - 1];
        }

        return $data->format(HData::BR_DATE_FORMAT);
    }


    public static function horaInteligente($hora)
    {
        $hoje = new DateTime();
        $data = new DateTime($hora);
        $segundos = $hoje->format('U') - $data->format('U');
        $minutos = round($segundos / 60);
        $horas = round($segundos / (60 * 60));

        if ($minutos < 20 && $minutos > 0) {
            return 'Há poucos minutos';
        }

        if ($minutos < 30 && $minutos > 0) {
            return 'Cerca de meia hora';
        }

        if ($minutos < 59 && $minutos > 0) {
            return 'Cerca de uma hora';
        }

        if ($horas > 0 && $horas < 12) {
            return $horas == 1 ? 'Há ' . $horas . ' Hora' : 'Há ' . $horas . ' Horas';
        }

        if ($horas > 12) {
            return $data->format('H:m');
        }
        return $data->format('H:m');
    }


    public static function dataHoraInteligente($dataHora)
    {
        $hoje = new DateTime();
        $data = new DateTime($dataHora);
        if ($data->format(HData::BR_DATE_FORMAT) == $hoje->format(HData::BR_DATE_FORMAT)) //se a data é de hoje
        {
            return HData::horaInteligente($dataHora);
        } //função para formatar a hora
        else //se a data é de outro dia
        {
            return HData::dataInteligente($dataHora);
        } //função para formatar a data
    }


    public static function formataData(
        $data,
        $formatoDestino = HData::BR_DATETIME_FORMAT,
        $formatoOrigem = HData::SQL_DATETIME_FORMAT
    ) {
        if ($data == null) {
            return null;
        }
        $d = DateTime::createFromFormat($formatoOrigem, $data);
        if ($d != null) {
            return $d->format($formatoDestino);
        }
        return $data;
    }

    public static function tempoSenhaExpirar($dt_ultimaTroca)
    {
        $session = Yii::app()->session;
        if (isset($session['atributosCliente']['TEMPO_TROCA_SENHA'])) {
            if ($session['atributosCliente']['TEMPO_TROCA_SENHA'] === "") {
                return null;
            } else {
                $dias = $session['atributosCliente']['TEMPO_TROCA_SENHA'];
                $data = new DateTime($dt_ultimaTroca);
                $data->modify("+" . $dias . " day");
                return $data->format(self::SQL_DATE_FORMAT);
            }
        } else {
            if (isset($session['atributosGlobais']['TEMPO_TROCA_SENHA'])) {
                if ($session['atributosGlobais']['TEMPO_TROCA_SENHA'] === "") {
                    return null;
                } else {
                    $dias = $session['atributosGlobais']['TEMPO_TROCA_SENHA'];
                    $data = new DateTime($dt_ultimaTroca);
                    $data->modify("+" . $dias . " day");
                    return $data->format(self::SQL_DATE_FORMAT);
                }
            } else {
                return null;
            }
        }
    }


    public static function hoje($formatoRetorno = HData::BR_DATE_FORMAT)
    {
        $hoje = new DateTime();
        return $hoje->format($formatoRetorno);
    }


    public static function hora($formatoRetorno = HData::BR_TIME_FORMAT)
    {
        $hoje = new DateTime();
        return $hoje->format($formatoRetorno);
    }


    public static function getIdade($dtNascimento, $formatoOrigem = HData::SQL_DATE_FORMAT, $dt_calculo = '')
    {
        if ($dtNascimento == '') {
            return;
        }
        if ($dt_calculo === '') {
            $hoje = new DateTime();
        } else {
            $hoje = DateTime::createFromFormat($formatoOrigem, $dt_calculo);
        }

        if ($hoje === false) {
            return;
        }
        $diff = $hoje->diff(DateTime::createFromFormat($formatoOrigem, $dtNascimento));
        $anos = $diff->y;
        return $anos;
    }


    public static function ehMaiorDeIdade($dtNascimento, $formatoOrigem = HData::SQL_DATE_FORMAT, $dt_calculo = '')
    {
        $idade = self::getIdade($dtNascimento, $formatoOrigem, $dt_calculo);
        return ($idade > HData::MAIOR_IDADE);
    }


    public static function agora($formatoRetorno = HData::BR_DATETIME_FORMAT)
    {
        return self::hoje($formatoRetorno);
    }


    public static function sqlToBr($data)
    {
        return self::formataData($data, HData::BR_DATE_FORMAT, HData::SQL_DATE_FORMAT);
    }


    public static function brToSql($data)
    {
        return self::formataData($data, HData::SQL_DATE_FORMAT, HData::BR_DATE_FORMAT);
    }


    public static function dateTimeBrToSql($data)
    {
        return self::formataData($data, HData::SQL_DATETIME_FORMAT, HData::BR_DATETIME_FORMAT);
    }


    public static function dateTimeSqlToBr($data)
    {
        return self::formataData($data, HData::BR_DATETIME_FORMAT, HData::SQL_DATETIME_FORMAT);
    }


    public static function timeToMinutes($periodo, $limite = true)
    {
        if ($limite) {
            $horario = new DateTime($periodo);
            if ($horario != null) {
                return $horario->format('i') + $horario->format('h') * 60;
            }
        } else {
            /*
             * Caso haja a necessidade de não se ter o limite de horas.
             * Quando se trabalha com a Classe DateTime o intervalo de horas permitido é 0 à 23.
             */
            $time = explode(':', $periodo);
            return ($time[0] * 60 + $time[1]);
        }
    }


    public static function minutesToTime($mins)
    {
        // Se os minutos estiverem negativos
        if ($mins < 0) {
            $min = abs($mins);
        } else {
            $min = $mins;
        }

        // Arredonda a hora
        $h = floor($min / 60);
        $m = ($min - ($h * 60)) / 100;
        $horas = $h + $m;

        // Matemática da quinta série
        // Detalhe: Aqui também pode se usar o abs()
        if ($mins < 0) {
            $horas *= -1;
        }

        // Separa a hora dos minutos
        $sep = explode('.', $horas);
        $h = $sep[0];
        if (empty($sep[1])) {
            $sep[1] = 00;
        }

        $m = $sep[1];

        // Aqui um pequeno artifício pra colocar um zero no final
        if (strlen($m) < 2) {
            $m = $m . 0;
        }

        return sprintf('%02d:%02d', $h, $m);
    }

    public static function getPeriodoByDias($qtDias)
    {
        if ($qtDias) {
            if (isset(HData::$periodos[$qtDias])) {
                return HData::$periodos[$qtDias];
            }
            return $qtDias . " dias";
        } else {
            return null;
        }
    }


    public static function getPeriodoByMeses($qtMeses)
    {
        if ($qtMeses) {
            if (isset(HData::$periodosMeses[$qtMeses])) {
                return HData::$periodosMeses[$qtMeses];
            }
            return $qtMeses . " meses";
        } else {
            return null;
        }
    }


    public static function calculaDiffData($dt_inicio, $dt_fim)
    {
        if (empty($dt_inicio) || empty($dt_fim)) {
            return null;
        } else {

            $dataInicial = date_create(self::brToSql($dt_inicio));
            $dataFinal = date_create(self::brToSql($dt_fim));
            $data = date_diff($dataInicial, $dataFinal);

            return $data->format('%a') . (($data->format('%a') < 2) ? ' dia' : ' dias');
        }
    }

    /**
     * Retorna um array com os meses entre as datas informada.
     * @param $dt_inicio string Inicio do Período
     * @param $dt_fim string Fim do Período
     * @param bool $mesAbrev escolha se utilizará o nome do mês abreviado ou o nome inteiro
     * @param bool $chaves em caso de true o array retornado terá como chave o formato de mês/ano
     * @return array
     */
    public static function getMesesPeriodo($dt_inicio, $dt_fim, $mesAbrev = false, $chaves = false)
    {
        $dataInicial = date_create(self::brToSql($dt_inicio));
        $dataFinal = date_create(self::brToSql($dt_fim));
        if ($dataInicial == $dataFinal) {
            $nr_mes = $dataInicial->format('m');
            return array($nr_mes => self::$mesAbrev[$nr_mes]);
        } else {
            if ($dataInicial > $dataFinal) {
                return array();
            } else {
                $retorno = array();
                $dataInicial->modify('first day of this month');
                $dataFinal->modify('last day of this month');
                while ($dataInicial < $dataFinal) {
                    $nr_mes = (int)$dataInicial->format('m');
                    if ($mesAbrev) {
                        $nomeMes = self::$mesAbrev[$nr_mes - 1];
                    } else {
                        $nomeMes = self::$mes[$nr_mes - 1];
                    }
                    if ($chaves) {
                        $retorno[$dataInicial->format('m/Y')] = $nomeMes;
                    } else {
                        $retorno[] = $nomeMes;
                    }
                    date_modify($dataInicial, '+1 months');
                }
                return $retorno;
            }
        }
    }


    public static function retornaAnos($nr_anos = 35)
    {
        $anos = array();
        $hoje = new DateTime();;
        while ($nr_anos > 0) {
            $anos[$hoje->format('Y')] = $hoje->format('Y');
            date_modify($hoje, '-1 years');
            $nr_anos--;
        }
        return $anos;
    }

    public static function VerificaHorarioIntervalo($hr_inicio, $hr_fim, $inicio_intervalo, $fim_intervalo)
    {
        if (empty($hr_inicio) || empty($hr_fim) || empty($inicio_intervalo) || empty($fim_intervalo)) {
            return null;
        } else {
            if (strtotime($inicio_intervalo) <= strtotime($hr_inicio) && strtotime($fim_intervalo) >= strtotime(
                $hr_fim
                )
            ) {
                return true;
            }
            return false;
        }
    }
}

?>