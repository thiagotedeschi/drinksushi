<?php


class ActiveRecord extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return EmailContato the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function labelModel()
    {
        if (isset($this->attributeLabels()[$this->getPrimaryKeyAttribute()])) {
            return $this->attributeLabels()[$this->getPrimaryKeyAttribute()];
        }
        return null;
    }


    public function getPrimaryKeyAttribute()
    {
        if ($this->tableName() == null) {
            return null;
        }
        $table = $this->getMetaData()->tableSchema;
        if (is_string($table->primaryKey)) {
            return $table->primaryKey;
        } elseif (is_array($table->primaryKey)) {
            foreach ($table->primaryKey as $key => $name) {
                return $key;
            }
        } else {
            return null;
        }
    }

    public function dumpMe($die = true, $depth = 10, $highlight = true)
    {
        CVarDumper::dump($this, $depth, $highlight);
        if ($die) {
            die();
        }
    }


    public function comparaData($attribute, $params)
    {
        if (!$attribute || !isset($params['compareTo'])) {
            return null;
        }
        if (!isset($params['operator'])) {
            $params['operator'] = '>=';
        }
        if (!isset($params['format'])) {
            $params['format'] = HData::BR_DATE_FORMAT;
        }
        $data1 = dateTime::createFromFormat($params['format'], $this->$attribute);
        $data2 = dateTime::createFromFormat($params['format'], $this->$params['compareTo']);

        switch ($params['operator']) {
            case '=':
            case '==':
                if ($data1 == $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser igual a "{compareValue}".';
                }
                break;
            case '!=':
                if ($data1 != $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser diferente de "{compareValue}".';
                }
                break;
            case '>':
                if ($data1 > $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser maior que "{compareValue}".';
                }
                break;
            case '>=':
                if ($data1 >= $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser maior ou igual a "{compareValue}".';
                }
                break;
            case '<':
                if ($data1 < $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser menor que "{compareValue}".';
                }
                break;
            case '<=':
                if ($data1 <= $data2) {
                    $message = null;
                } else {
                    $message = '{attribute} deve ser menor ou igual a "{compareValue}".';
                }
                break;
            default:
                throw new CException(Yii::t('yii', 'Invalid operator "{operator}".', array('{operator}' => $operator)));
        }

        $message = Yii::t(
            'yii',
            $message,
            array(
                '{attribute}' => $this->getAttributeLabel($attribute),
                '{compareValue}' => $this->$params['compareTo']
            )
        );

        if (!empty($message)) {
            $this->addError(
                $attribute,
                $message,
                array('{attribute}' => $attribute, '{compareValue}' => $params['compareTo'])
            );
        }
    }
}
