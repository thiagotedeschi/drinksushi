<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 * @package base.Components
 */
class Controller extends CController
{

    CONST MODAL_LAYOUT_FILE = '//layouts/iframe';
    CONST CLASSE_PADRAO_TIPO_MODULO = 'tipo-modulo-default';
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var int a ação que será executada neste exato momento. Vazio significa que estamos na home;
     */
    public $IDAcao = '';

    /**
     * @var Array crumbs da página
     */
    public $crumbs = array();

    /**
     * @var array array que terá todas as actions do controller com seus respectivos ID's de ação
     */
    public $acoesActions = array();

    /**
     * @var array Estará setado com as dicas cadastradas para a Ação
     */
    public $dicas = array();

    /**
     * @var array Estará setado com as ajudas cadastradas para a Ação
     */
    public $ajudas = array();


    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
            'VerificaSenhaExpirada - mudarSenhaExpirada', // Valida se o usuário pode executar aquela ação
            'VerificaAcao - error, login, logout' // Valida se o usuário pode executar aquela ação
        );
    }

    public function filterVerificaAcao($filterChain)
    {
        $action = $filterChain->controller->getAction()->getId(); //Pega a action que está sendo acessada
        $this->IDAcao = $this->getActions($action); //retorna o id da ação da action foi requisitada
        if (is_null($this->IDAcao) || empty($this->IDAcao)) {
            $filterChain->run();
        } //caso seja null, o usuário estará liberado para acessar a pagina
        else {
            $this->retornaDicas($this->IDAcao);
            if (isset(Yii::app()->session['acoes'][$this->IDAcao])
            ) { //comparação de ids de ação o qual o usuário tem permissão e o que está tentando acessar
                $filterChain->run(); //caso ele possua essa ação a página e libera e se encerra
                exit(0);
            }
            throw new CHttpException(403); //caso o usuário não tenha a ação ele e redirecionada para a página de erro.
        }
    }


    public function filterVerificaSenhaExpirada($filterChain)
    {
        if (Yii::app()->user->getState('senhaExpirada') == true) {
            Yii::app()->user->logout();
            $this->redirect($this->createUrl('site/login'));
            exit(0);
        } else {
            $filterChain->run();
            exit(0);
        }
    }

    public function beforeAction($action)
    {
        Log::registrarLog($this->IDAcao); //chamada a função para registro do Log. Só deve acontecer se a ação existir!
        return parent::beforeAction($action);
    }

    public function accessRules()
    {
        return array(
            array(
                'allow', //permitir o acesso
                'actions' => array('*'), // todas as ações
                'users' => array('@'), // '@' são os usuários autenticados
            ),
            array(
                'allow', //permitir o acesso
                'actions' => array('login', 'logout', 'captcha', 'testeStress'), // apenas à essas ações
                'users' => array('*'), // '*' e qualquer tipo de usuário
            ),
            array(
                'deny', // nega acesso
                'users' => array('?'), //'?' Usuários que não identificados
            ),
        );
    }

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
    }


    protected function getMenu()
    {
        if (!is_null(Yii::app()->session['menu'])) //caso não exista essa entrada na session...
        {
            return Yii::app()->session['menu'];
        } //menu
        if (isset(YII::app()->user->IDUsuario)) { //caso o usuário esteja autenticado
            $acoes = $this->getAcoesUsuario(YII::app()->user->IDUsuario); //pega todas as ações delegadas ao usuário.
            YII::app()->session['acoes'] = $acoes;
            $menu = ($this->getItensMenu($acoes)); //monta o menu baseado nas ações 
            YII::app()->session['menu'] = $menu; //guarda o menu na session para usos futuros
            return $menu; //retorna o menu
        } else {
            return array();
        }
    }

    public function getAcoesUsuario($IDUsuario)
    {
        if (!is_null(Yii::app()->session['acoes'])) //caso não exista essa entrada na session...
        {
            return Yii::app()->session['acoes'];
        } //ações
        $user = new Usuario();
        $user = Usuario::model()->findByPk($IDUsuario); //busca o usuário referente àquele ID
        //busca-se apenas concessões válidas no dia de hoje
        $concessoes = $user->concessaoGrupos(
            array('condition' => "((dt_fim IS NULL) OR (dt_fim > now())) AND ((dt_inicio IS NULL) OR (dt_inicio < now()))")
        );
        if ($user->admin_usuario) { //se o usuário for administrador, retornar todas as ações associadas ao cliente
            Yii::app()->session['acoes'] = $this->getAcoesCliente(CLIENTE);
            return Yii::app()->session['acoes'];
        }
        if (count($concessoes) == 0) //se ele não tiver nenhuma concessão a nenhum grupo
        {
            return Yii::app()->session['acoes'] = array();
        }

        $acoes = array();
        $todasAcoesDisponiveis = $this->getAcoesCliente(CLIENTE);
        foreach ($concessoes as $concessao) { //para cada concessão, busca-se todas as permissões válidas no dia de hoje
            $permissoes = Permissao::model()->findAllByAttributes(
                array('IDGrupo' => $concessao->IDGrupo),
                array('condition' => "((\"dt_fimPermissao\" IS NULL) OR (\"dt_fimPermissao\" > now())) AND ((\"dt_inicioPermissao\" IS NULL) OR (\"dt_inicioPermissao\" < now()))")
            );
            foreach ($permissoes as $permissao) { //para cada permissão, guarda-se as ações devidas
                $acao = $permissao->iDAcao();
                foreach ($todasAcoesDisponiveis as $acaoDisponivel) {
                    if ($acao->IDAcao == $acaoDisponivel->IDAcao) {
                        $acoes[$acao->IDAcao] = $acao;
                    }
                }
            }
        }
        Yii::app()->session['acoes'] = $acoes; // guarda as ações para usos futuros
        return Yii::app()->session['acoes']; // ações
    }


    protected function getItensMenu($acoes)
    {
        $items = array();
        $htmlItems = array();
        if (count($acoes) == 0) // se não tiver ações, menu vazio
        {
            return array();
        }

        foreach ($acoes as $acao) { //pra cada ação
            $colocar = true;
            $item = $acao->iDItemMenu();
            foreach ($items as $itemArray) {
                //verificação para não colocar items de menu repetidos no array
                //como um item de menu pode ter várias ações, é necessário verificar isso
                if ($itemArray->IDItem_menu == $item->IDItem_menu) {
                    $colocar = false;
                }
            }
            if ($colocar) {
                $items[] = $item;
            }
        }
        foreach ($items as $itemArray) {
            if ($itemArray->IDMenuPai() != ''
            ) //guarda items de menu filhos um array dentro de outro com indice IDMenu_pai
            {
                $itemMenuPai[$itemArray->IDMenu_pai][] = $itemArray->IDItem_menu;
            } else {
                $itemMenuPai[$itemArray->IDItem_menu] = '';
            } // se ele não tiver filhos, guarda do mesmo jeito, mas apontando pra nada
        }
        foreach ($itemMenuPai as $IDMenu => $submenus) { //para cada item de menu pai
            $IDModulo = ItemMenu::model()->findByPk(
                $IDMenu
            )->IDModulo; //guarda cada um deles dentro de outro array com indice IDModulo
            $modulos[$IDModulo][$IDMenu] = $itemMenuPai[$IDMenu];
        }
        foreach ($modulos as $IDModulo => $IDMenu) { // para cada módulo
            $IDTipo_modulo = Modulo::model()->findByPk(
                $IDModulo
            )->IDTipo_modulo; //guarda o array do modulo dentro de outro, com indice IDTipo_menu
            $tipo_modulos[$IDTipo_modulo][$IDModulo] = $modulos[$IDModulo];
        }
        //estrutura do menu pronta dentro de $tipo_modulo, agora é fazer o html de cada item de menu aninhando seus filhos dentro

        foreach ($tipo_modulos as $tipo_modulo => $modulos) {
            $tipoModulo = TipoModulo::model()->findByPk($tipo_modulo);
            $htmlTipoModulo = $this->montaArrayTipoModulo($tipoModulo); //cria o item html de cada tipo_modulo
            $htmlModulos = array();
            foreach ($modulos as $modulo => $itemMenu) { //pra modulo dentro dele
                $modulo = Modulo::model()->findByPk($modulo);
                $htmlModulo = $this->montaArrayModulo($modulo); //monta o item html de cada modulo
                $htmlMenuPais = array(); //limpa o array para não misturar os items de menu
                foreach ($itemMenu as $itemMenuPai => $itemMenuFilhos) { //para cada item menu
                    $itemMenuPai = ItemMenu::model()->findByPk($itemMenuPai);
                    $htmlMenuPai = $this->montaArrayItem($itemMenuPai);
                    if ($itemMenuFilhos != '') { //se ele tiver filhos
                        $htmlMenuFilhos = array(); //limpa o array a cada novo itemMenu pai
                        foreach ($itemMenuFilhos as $itemMenuFilho) { //para cada item de menu filho
                            $itemMenuFilho = ItemMenu::model()->findByPk($itemMenuFilho);
                            $htmlMenuFilho = $this->montaArrayItem($itemMenuFilho); //monta html de cada filho
                            if (isset($htmlMenuFilhos[$itemMenuFilho->n_ordemItemMenu])) {
                                $keyMenuFilho = $itemMenuFilho->nome_menu;
                            } else {
                                $keyMenuFilho = $itemMenuFilho->n_ordemItemMenu;
                            }
                            $htmlMenuFilhos[$keyMenuFilho] = $htmlMenuFilho; //guarda todos num array
                        }
                        ksort($htmlMenuFilhos);
                        $htmlMenuPai['items'] = $htmlMenuFilhos; //para cada menu pai, guarda na posição items os filhos dele                        
                    }
                    if (isset($htmlMenuPais[$itemMenuPai->n_ordemItemMenu])) {
                        $keyMenuPai = $itemMenuPai->nome_menu;
                    } else {
                        $keyMenuPai = $itemMenuPai->n_ordemItemMenu;
                    }
                    $htmlMenuPais[$keyMenuPai] = $htmlMenuPai; //guarda cada item de menu pai já com seus filhos nesse array
                }
                ksort($htmlMenuPais);
                $htmlModulo['items'] = $htmlMenuPais; //guarda todos os items de menu já com seus filhos em cada modulo
                if (isset($htmlModulos[$modulo->n_ordemModulo])) {
                    $keyModulo = $modulo->nome_modulo;
                } else {
                    $keyModulo = $modulo->n_ordemModulo;
                }
                $htmlModulos[$keyModulo] = $htmlModulo; //guarda cada modulo já com seus items de menu (pais e filhos) nesse array
            }
            ksort($htmlModulos);
            $htmlTipoModulo['items'] = $htmlModulos; //guarda os modulos já com tudo que ele deveria ter em seu devido tipo_modulo
            if (isset($htmlTipoModulos[$tipoModulo->n_ordemTipoModulo])) {
                $keyTipoModulo = $tipoModulo->nome_tipoModulo;
            } else {
                $keyTipoModulo = $tipoModulo->n_ordemTipoModulo;
            }
            $htmlTipoModulos[$keyTipoModulo] = $htmlTipoModulo; //guarda o tipo de modulo com sua cambada toda, nesse array
        }
        ksort($htmlTipoModulos);
        return $htmlTipoModulos;
    }


    protected function montaArrayItem($item)
    {
        $slug = $item->IDModulo()->slug_modulo;
        $acoes = '';
        foreach ($item->acoes() as $acao) {
            $acoes .= ' I' . $acao->IDAcao;
        }
        $label = '<i class="icon-' . $item->icone_itemMenu . ' ' . $acoes . " IM" . $item->IDItem_menu . '"></i><span class="title">' . $item->nome_menu . '</span><span class="selected"></span><span class=""></span>';
        $linkOptions = $item->link_menu == '' ? array('href' => '#') : array('href' => 'index.php?r=' . $slug . $item->link_menu);
        if ($item->link_menu != '' && $item->ehFavorito()) {
            $linkOptions['class'] = 'undraggable';
            $linkOptions['style'] = 'font-weight:600';
        }
        $linkOptions['alt'] = $item->tags;
        $arrayItem = array('label' => $label, 'linkOptions' => $linkOptions);
        return $arrayItem;
    }


    protected function montaArrayTipoModulo($tipoModulo)
    {
        $label = '<i class="icon-' . $tipoModulo->icone_tipoModulo . ' SMOD"></i><span class="title">' . $tipoModulo->nome_tipoModulo . '</span><span class="selected"></span><span class=""></span>';
        $linkOptions = array('href' => '#');
        $arrayTipo_modulo = array(
            'label' => $label,
            'linkOptions' => $linkOptions,
            'itemOptions' => array('class' => $tipoModulo->classe_tipoModulo . ' tipo-modulo'),
        );
        return $arrayTipo_modulo;
    }


    protected function montaArrayModulo($modulo)
    {
        $label = '<i class="icon-' . $modulo->icone_modulo . ' MOD' . $modulo->IDModulo . '"></i><span title="' . $modulo->desc_modulo . '" class="title">' . $modulo->nome_modulo . '</span><span class="selected"></span><span class=""></span>';
        $linkOptions = array('href' => '#');
        $arrayModulo = array('label' => $label, 'linkOptions' => $linkOptions);
        return $arrayModulo;
    }


    public function getAcoesCliente($slug)
    {

        $auxAcoes = Acao::model()->findAllBySql(
            'SELECT DISTINCT
                "AC".*
                    FROM
                "Projetos_tem_modulos" "PM"
                    INNER JOIN "Projeto" "PJ" ON "PJ".slug_projeto = \'' . $slug . '\' AND "PJ"."IDProjeto" = "PM"."IDProjeto"
        INNER JOIN "Modulo" "MO" ON "PM"."IDModulo" = "MO"."IDModulo"
        INNER JOIN "Item_menu" "IM" ON "IM"."IDModulo" = "MO"."IDModulo"
        INNER JOIN "Acao" "AC" ON "AC"."IDItem_menu" = "IM"."IDItem_menu"
        WHERE ("PM".dt_fim > now() OR "PM".dt_fim IS NULL) AND ("PM".dt_inicio < now()  OR "PM".dt_inicio IS NULL)'
        );

        foreach ($auxAcoes as $acao) {
            $acoes[$acao->IDAcao] = $acao;
        }
        return $acoes;
    }


    public function getActions($action)
    {
        return !empty($this->acoesActions[$action]) ? $this->acoesActions[$action] : null;
    }


    public function verificaAcao($action)
    {
        if (is_int($action)) {
            $IDAcao = $action;
        } else {
            $IDAcao = $this->getActions($action);
        } //retorna o id da ação da action foi requisitada

        if (is_null($IDAcao)) //verifica se o valor e nulo ou não
        {
            return true;
        } //caso seja null, o usuário estará liberado para acessar a pagina
        else {
            return isset(Yii::app()->session['acoes'][$IDAcao]);
        }
    }


    public function retornaDicas($acao)
    {
        if (!empty($acao)) //Carrega as dicas, caso existam, para aquela view
        {
            $this->dicas = Dica::model()->with('TipoDica')->findAll('"IDAcao" = :acao', array(':acao' => $acao));
        } else {
            $this->dicas = null;
        }
    }

    public function carregaAjuda($id)
    {
        //Carrega as ajudas, caso existam, para aquela view        
        $this->ajudas = Ajuda::model()->findByPk($id);
    }


    public function retornaAjuda($id)
    {
        $this->carregaAjuda($id);
        $ajuda = $this->ajudas;
        if ($ajuda == null) {
            return false;
        }
        if ($ajuda->video_ajuda != null) {
            $title = '<b>' . $ajuda->texto_ajuda . '</b>';
            $content = $ajuda->geraIframe();
        } else {
            $title = '<b>Ajuda</b>';
            $content = $ajuda->texto_ajuda;
        }
        $retorno = '<i class="icon-question-circle popovers icon-large botaoAjuda" data-html="true" data-original-title="' . $title . '" data-content="' . $content . '"></i>';
        return $retorno;

    }


    public function render($view, $data = null, $return = false)
    {
        if ($this->isModalRequest()) {
            $this->layout = self::MODAL_LAYOUT_FILE;
        }
        parent::render($view, $data, $return);
    }


    public function isModalRequest()
    {
        return (isset($_REQUEST['in-modal']) && $_REQUEST['in-modal'] === 'in-modal');
    }


    public function redirect($url, $terminate = true, $statusCode = 302)
    {
        if ($this->isModalRequest()) {
            $script = "window.parent.$('.modal').on('hidden', function(){\n"
                . "window.parent.$('.grid-view').yiiGridView('update');\n"
                . "window.location = window.location\n"
                . "});";
            $script .= "window.parent.$('.modal').modal('hide');";
            echo Html::script($script);
            YII::app()->end();
        }
        parent::redirect($url, $terminate = true, $statusCode = 302);
    }


    public function getClassTipoModulo()
    {
        $crumbs = $this->getCrumbs();
        if ($crumbs == null) {
            return self::CLASSE_PADRAO_TIPO_MODULO;
        }
        if (isset($crumbs[1]['classe'])) {
            return $crumbs[1]['classe'];
        }
        return self::CLASSE_PADRAO_TIPO_MODULO;

    }

    public function createAbsoluteRequestUrl()
    {
        return $this->createAbsoluteUrl(isset($_GET['r']) ? '/' . $_GET['r'] : '', $_GET);
    }


    public function getCrumbs()
    {
        if (empty($crumbs)) {
            //Crumb página inicial, sempre vai ter
            $this->crumbs[0] = array('nome' => 'Home', 'icone' => 'home', 'url' => 'site/index');
            if ($this->IDAcao == '') {
                return $this->crumbs;
            }
            //consulta para trazer a trilha da ação até o tipo de módulo
            $dados_crumbs = Yii::app()->db->createCommand(
                'SELECT
                        "public"."Acao".nome_acao as "NomeAcao",
                        "public"."Acao".desc_acao as "DescAcao",
                        "public"."Item_menu"."IDItem_menu" as "IDMenu",
                        "public"."Item_menu".nome_menu as "NomeMenu",
                        "public"."Item_menu".link_menu as "LinkMenu",
                        "public"."Modulo".nome_modulo as "NomeModulo",
                        "public"."Modulo".slug_modulo as "SlugModulo",
                        "public"."Tipo_modulo"."nome_tipoModulo" as "NomeTipoModulo",
                        "public"."Tipo_modulo"."icone_tipoModulo" as "IconeTipoModulo",
                        "public"."Tipo_modulo"."classe_tipoModulo" as "ClasseTipoModulo",
                        "public"."Modulo".icone_modulo as "IconeModulo",
                        "public"."Item_menu"."icone_itemMenu" as "IconeItemMenu",
                        "public"."Item_menu"."IDMenu_pai" as "MenuPai"
                FROM
                "public"."Acao"
                INNER JOIN "public"."Item_menu" ON "public"."Acao"."IDItem_menu" = "public"."Item_menu"."IDItem_menu"
                INNER JOIN "public"."Modulo" ON "public"."Item_menu"."IDModulo" = "public"."Modulo"."IDModulo"
                INNER JOIN "public"."Tipo_modulo" ON "public"."Modulo"."IDTipo_modulo" = "public"."Tipo_modulo"."IDTipo_modulo"
                WHERE "public"."Acao"."IDAcao" = ' . $this->IDAcao
            )->queryRow();
            //crumb tipo modulo
            $this->crumbs[] = array(
                'nome' => $dados_crumbs['NomeTipoModulo'],
                'icone' => $dados_crumbs['IconeTipoModulo'],
                'classe' => $dados_crumbs['ClasseTipoModulo'],
                'url' => ''
            );
            //crumb modulo
            $this->crumbs[] = array(
                'nome' => $dados_crumbs['NomeModulo'],
                'icone' => $dados_crumbs['IconeModulo'],
                'url' => $dados_crumbs['SlugModulo']
            );
            //se esse cara tiver item de menu pai, fazer a crumb dele tambem
            if ($dados_crumbs['MenuPai'] != '') {
                $item = ItemMenu::model()->findByPk($dados_crumbs['MenuPai']);
                $this->crumbs[] = array('nome' => $item->nome_menu, 'icone' => $item->icone_itemMenu, 'url' => '');
            }
            //crumb item de menu em si
            $this->crumbs[] = array(
                'nome' => $dados_crumbs['NomeMenu'],
                'icone' => $dados_crumbs['IconeItemMenu'],
                'url' => $dados_crumbs['SlugModulo'] . $dados_crumbs['LinkMenu']
            );
            //crumb da acao
            $this->crumbs[] = array(
                'nome' => $dados_crumbs['NomeAcao'],
                'icone' => '',
                'url' => '',
                'desc' => $dados_crumbs['DescAcao']
            );
        }
        return $this->crumbs;
    }

    /**
     * Retorna o Ícone para a página atual
     * Caso não seja possível setar o ícone a partir do penultima posição das Crumbs,
     * o ícone reorder será retornado
     * @return string icone para a página atual
     */
    public function getIconePagina()
    {
        $crumbs = $this->getCrumbs();
        if (isset($crumbs[count($crumbs) - 2])) {
            $icone = $crumbs[count($crumbs) - 2]['icone'];
        } else {
            $icone = 'reorder';
        }
        return $icone;

    }


    public function getModulosProjeto()
    {
        if (!is_null(Yii::app()->session['modulos'])) {
            return Yii::app()->session['modulos'];
        }

        $IDModulos = Yii::app()->db->createCommand()
            ->selectDistinct('IDModulo')
            ->from('Projetos_tem_modulos')
            ->join('Projeto', '"Projeto"."IDProjeto" = "Projetos_tem_modulos"."IDProjeto"')
            ->where(
                '"Projeto"."slug_projeto" = :cliente AND ("Projetos_tem_modulos"."dt_inicio" <= now() OR "Projetos_tem_modulos"."dt_inicio" IS NULL) AND ("Projetos_tem_modulos"."dt_fim" >= now() OR "Projetos_tem_modulos"."dt_fim" IS NULL)',
                array(':cliente' => CLIENTE)
            )
            ->queryAll();

        foreach ($IDModulos as $IDModulo) {
            $modulos[] = $IDModulo['IDModulo'];
        }

        Yii::app()->session['modulos'] = $modulos;
        return Yii::app()->session['modulos'];
    }
}
