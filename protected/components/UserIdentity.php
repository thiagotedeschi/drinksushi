<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 * @package base.Components
 */
class UserIdentity extends CUserIdentity
{

    const ERROR_USER_EXPIRED = 3;
    const ERROR_USER_DISABLED = 4;
    const ERROR_USER_BLOCKED = 5;
    const ERROR_PROJECT_INVALID = 6;

    private $tempoBloqueio;

    /**
     * Autenticação do usuário e set informações do usuário
     * que serão necessarias para o sistema e o registro do historico de Login
     * @since 0.2 16/10/2013
     * @return boolean whether authentication succeeds.
     */
    public function authenticate()
    {
        $projeto = Yii::app()->db->createCommand()
            ->select('p.IDProjeto, cliente.razao_cliente')
            ->from('public.Projeto p')
            ->join('public.Cliente cliente', '"cliente"."IDCliente" = "p"."IDCliente"')
            ->where(
                '"p"."slug_projeto" = :projeto AND (("p"."dt_fimProjeto" IS NULL) OR ("p"."dt_fimProjeto" > now()))
                 AND (("p"."dt_inicioProjeto" IS NULL)
                 OR ("p"."dt_inicioProjeto" < now())) AND "p"."ativo_projeto"=true',
                array(':projeto' => CLIENTE)
            )
            ->queryRow();

        //Consulta para a verificação do login
        $IDUsuario = Yii::app()->db->createCommand()
            ->select('*')
            ->from(CLIENTE . '.Usuario')
            ->where("login_usuario = :login", array(':login' => $this->username))
            ->queryRow();

        if ($IDUsuario) {
            $historicoLogin = new HistoricoLogin;
            $historicoLogin->dt_historicoLogin = null;
            $historicoLogin->ip_historicoLogin = Yii::app()->request->getUserHostAddress();
            $historicoLogin->IDUsuario = $IDUsuario['IDUsuario'];
            $historicoLogin->iDUsuario = $IDUsuario['IDUsuario'];

            //Verificação de quantas tentativas de login
            $nrTentativas = Yii::app()->db->createCommand()
                ->select('CURRENT_TIMESTAMP(0) as agr, "dt_historicoLogin"')
                ->from(CLIENTE . '.Historico_login')
                ->where(
                    array(
                        'AND',
                        '"IDUsuario" = :id',
                        '"dt_historicoLogin" > CURRENT_TIMESTAMP - interval \'3 minutes\'',
                        '"error_historicoLogin" = 2'
                    ),
                    array(":id" => $IDUsuario['IDUsuario'])
                )
                ->limit('3')
                ->queryAll();
            /* Caso aquele login tenha três tentativas ou mais nos ultimos 3 minutos
             * ficará bloqueado para tentar novamente, até que a primeira tentativa saia
             * do intervalo de 3 minutos
             */
            if (count($nrTentativas) >= 2) {
                /* Contas para a verificação do tempo em que
                 * ficará bloquado as tentativas de login
                 */
                $dt_historicoLogin = new DateTime($nrTentativas[0]['dt_historicoLogin'] . '-02');
                $limite = new DateTime($nrTentativas[1]['agr']);
                $limite->add(DateInterval::createFromDateString('3 minutes'));
                $diff = date_diff($dt_historicoLogin, $limite);

                $tempoBloqueio = $diff->i + $diff->s;

                //set o tempo de bloqueio
                $this->setTempoBloqueio($tempoBloqueio);
                $this->errorCode = self::ERROR_USER_BLOCKED; //Erro caso usuario tenha 3 tentativas nos ultimos 3 min

                $historicoLogin->error_historicoLogin = $this->errorCode; //set o erro do Login
                $historicoLogin->save(); //Grava no BD a tentativa de login
                $profissional = Usuario::model()->findByPk($historicoLogin->IDUsuario)->IDProfissional;
                Notificacao::enviaNotificacao(
                    array($profissional),
                    'MAX_TENTATIVAS_LOGIN'
                );
                return !$this->errorCode; //retorna o erro e encerra a tentativa de login
            } else {
                //Consulta de verificação da senha digitada
                $senha = Yii::app()->db->createCommand()
                    ->select("senha_usuario = crypt(:senha, senha_usuario)")
                    ->from(CLIENTE . '.Usuario')
                    ->where(
                        "login_usuario = :login",
                        array(':login' => $IDUsuario['login_usuario'], ':senha' => $this->password)
                    )
                    ->queryScalar();

                //Verificação do resultado da consulta de verificação da senha
                if (!$senha) {
                    $this->errorCode = self::ERROR_PASSWORD_INVALID; //Erro caso a consulta nao consiga encontrar o usuário
                    $historicoLogin->error_historicoLogin = $this->errorCode;
                    $historicoLogin->save();
                    return !$this->errorCode;
                }

                if ($IDUsuario['ativo_usuario'] != 1) {
                    $this->errorCode = self::ERROR_USER_DISABLED;
                    $historicoLogin->error_historicoLogin = $this->errorCode;
                }

                if (isset($IDUsuario['tempo_utilizacaoUsuario'])) {
                    $dtCriacao = new DateTime($IDUsuario['dt_criacaoUsuario']);
                    $hj = new DateTime(date("Y-m-d H:i:s"));
                    $diff = $dtCriacao->diff($hj);
                    if ($diff->format('%a') >= $IDUsuario['tempo_utilizacaoUsuario']) {
                        $this->errorCode = self::ERROR_USER_EXPIRED;
                        $historicoLogin->error_historicoLogin = $this->errorCode;
                    }
                }

                if (!$projeto) {
                    $this->errorCode = self::ERROR_PROJECT_INVALID; //Erro caso a consulta nao consiga encontrar o usuário
                    return !$this->errorCode;
                } else {
                    Yii::app()->session['id_projeto'] = $projeto['IDProjeto'];
                    Yii::app()->session['razao_cliente'] = $projeto['razao_cliente'];
                }

                if ($this->errorCode == 100) {
                    $this->guardaInfoUsuarioSession($IDUsuario);
                    $this->setAtributos();
                    $this->errorCode = self::ERROR_NONE; //retorna em caso do usuário ser autenticado
                    $historicoLogin->error_historicoLogin = $this->errorCode;
                }

                $historicoLogin->save();
            }
        } else {
            $this->errorCode = self::ERROR_USERNAME_INVALID; //Erro caso a consulta nao consiga encontrar o usuário            
        }

        return !$this->errorCode;
    }

    function guardaInfoUsuarioSession($IDUsuario)
    {
        //Profissional relacionado ao usuário
        $profissional = Profissional::model()->findByPk($IDUsuario['IDProfissional']);
        //Informações do usuários pertinentes para o sistema
        Yii::app()->user->setState('login_usuario', $IDUsuario['login_usuario']);
        Yii::app()->user->setState('IDProfissional', $IDUsuario['IDProfissional']);
        Yii::app()->user->setState('IDUsuario', $IDUsuario['IDUsuario']);
        Yii::app()->user->setState('todas_empresas', $profissional->todas_empresasProfissional);
        if (!$profissional->todas_empresasProfissional) {
            Yii::app()->session['empregadores'] = $profissional->getLabelEmpregadores();
        } else {
            Yii::app()->session['empregadores'] = Empregador::model()->getLabelEmpregadores();
        }
    }

    public function setTempoBloqueio($tempoBloqueio)
    {
        $this->tempoBloqueio = $tempoBloqueio;
    }

    public function getTempoBloqueio()
    {
        return $this->tempoBloqueio;
    }


    public function setAtributos()
    {
        //Verifica se já existe atributos, relativos ao Cliente, na Session 
        if (!isset(Yii::app()->session['atributosCliente'])) {
            $atributosCliente = Yii::app()->db->createCommand()
                ->select('*')
                ->from(CLIENTE . '.Atributo')
                ->queryAll();
            $array = array();
            foreach ($atributosCliente as $Atributo) {
                $array[$Atributo['atributo']] = $Atributo['valor_atributo'];
            }
            Yii::app()->session['atributosCliente'] = $array;
        }
        //Verifica se os atributos publics já estão carregados na session
        if (!isset(Yii::app()->session['atributosGlobais'])) {
            $atributosPublic = Yii::app()->db->createCommand()
                ->select('*')
                ->from('public.Atributo')
                ->queryAll();
            $array = array();
            foreach ($atributosPublic as $Atributo) {
                $array[$Atributo['atributo']] = $Atributo['valor_atributo'];
            }
            Yii::app()->session['atributosGlobais'] = $array;
        }
    }

}
