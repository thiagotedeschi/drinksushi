<?php

/**
 * @package base.Controllers
 */
class FavoritosController extends Controller
{

    public function actionCreate()
    {
        $data = $_POST['classe'];
        $classes = explode(' ', $data);
        $id = null;
        foreach ($classes as $class) {
            if (strpos($class, 'IM') !== false) {
                $id = HTexto::somenteNumeros($class);
                $f = new FavoritoUsuario();
                $f->IDUsuario = Yii::app()->user->IDUsuario;
                $f->IDItemMenu = $id;
                $f->dt_insercaoFavorito = new CDbExpression('current_timestamp(0)');
                if (!$f->save()) {
                    var_dump($f->errors);
                }
                break;
            }
        }
        unset(Yii::app()->session['menu']);
    }

    public function actionRemove()
    {
        $data = $_POST['classe'];
        $classes = explode(' ', $data);
        $id = null;
        foreach ($classes as $class) {
            if (strpos($class, 'IM') !== false) {
                $id = HTexto::somenteNumeros($class);
                $f = FavoritoUsuario::model()->findByPk(
                    array(
                        'IDUsuario' => Yii::app()->user->IDUsuario,
                        'IDItemMenu' => $id
                    )
                )->delete();
                break;
            }
        }
        unset(Yii::app()->session['menu']);
    }

}
