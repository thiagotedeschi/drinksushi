<?php

/**
 * @package base.Controllers
 */
class EnderecoController extends Controller
{

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Endereco the loaded model
     * @throws CHttpException
     *
     */
    public function loadModel($id)
    {
        $model = Endereco::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    public function actions()
    {
        return array(
            'buscaPorCep' => 'ext.correios.actions.BuscaPorCepAction'
        );
    }

    public function actionCidadesPorEstado()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $IDEstado = Estado::model()->findAll('"IDEstado" = ' . $_POST['estado'])[0]->IDEstado;
            $data = CidadeIbge::model()->findAll('"IDEstado_cidade" = ' . $IDEstado . '');
            $data = Html::listData($data, 'IDCidade_ibge', 'nome_cidadeIbge');
            foreach ($data as $value => $name) {
                echo Html::tag('option', array('value' => $value), Html::encode($name), true);
            }
        }
    }

}
