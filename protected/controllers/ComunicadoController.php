<?php


class ComunicadoController extends Controller
{

    public static $TEXTOS_COMUNICADO = array(
        'NOVA_OBS_TRABALHADOR' => 'O Trabalhador {{nome_trabalhador}} recebeu uma nova observação.',
        'NOVA_DEFICIENCIA_TRABALHADOR' => 'Um novo Laudo de Deficiência foi cadastrado para o Trabalhador {{nome_trabalhador}}.',
        'DESATIVAR_OBS_TRABALHADOR' => 'A observação do Trabalhador {{nome_trabalhador}} foi desativada.',
        'MAX_TENTATIVAS_LOGIN' => 'Seu usuário foi bloqueado por execeder o máximo de tentativas de Login.'
    );

    /**
     * Envia notificações para profissionais e email para contatos associados a uma lista de
     * Documentos e Responsabilidades
     * @param array $IDDocumentos array contendo ids dos documentos envolvidos na notificação
     * @param array $IDResponsabilidades array contendo ids das responsabilidades envolvidas na notificação
     * @param integer $IDEmpregador ID do Empregador em questão
     * @param string $conteudo conteudo da notificação/email
     * @param string $icone ícone (class icon-* do bootstrap)
     * @param string $link link (padrão para #)
     * @param string $cor cor que define o tipo de notificação (padrão para azul, que significa info);
     */
    public static function enviaComunicados(
        $IDDocumentos,
        $IDResponsabilidadesProfissao,
        $IDResponsabilidadesContato,
        $IDEmpregador,
        $conteudo,
        $parametros = array(),
        $icone = 'bullhorn',
        $link = '#',
        $cor = Notificacao::NT_BLUE
    ) {
        $profissionais = array();
        $contatos = array();
        $empregador = Empregador::model()->findByPk($IDEmpregador);
        $vigencia = $empregador->getVigenciaAtual();

        if ($vigencia) {
            foreach ($IDDocumentos as $IDDocumento) {
                $cordenadores = $vigencia->getCordenadorDocumento($IDDocumento);
                foreach ($cordenadores as $cordenador) {
                    $profissionais[$cordenador->IDProfissional] = $cordenador->IDProfissional;
                }
                $contatosAssociados = Contato::model()->findAllByTipoDocumento($IDDocumento)->findAll();
                foreach ($contatosAssociados as $contatoAssociado) {
                    $contatos[$contatoAssociado->IDContato] = $contatoAssociado;
                }
            }
        }

        foreach ($IDResponsabilidadesContato as $IDResponsabilidade) {
            $contatosResponsaveis = Contato::model()->findAllByResponsabilidade($IDResponsabilidade)->findAll();
            foreach ($contatosResponsaveis as $contatoResponsavel) {
                $contatos[$contatoResponsavel->IDContato] = $contatoResponsavel;
            }
        }

        foreach ($IDResponsabilidadesProfissao as $IDResponsabilidade) {
            $profissionaisResponsaveis = Profissional::model()->findAllByResponsabilidade($IDResponsabilidade)->findAll(
            );
            foreach ($profissionaisResponsaveis as $profissionalResponsavel) {
                $profissionais[$profissionalResponsavel->IDProfissional] = $profissionalResponsavel;
            }
        }

        if (!empty($contatos)) {
            $dadosEmail = array();
            $dadosEmail['conteudo'] = self::getConteudoComunicado($conteudo, $parametros);
            $dadosEmail['assunto'] = 'Notificação do Restaurante';
            $console = new CConsole();
            foreach ($contatos as $contato) {
                $console->runCommand(
                    'email comunicado',
                    array(
                        '--IDContato=' . $contato->IDContato,
                        '--conteudo=' . $dadosEmail['conteudo'],
                        '--assunto=' . $dadosEmail['assunto']
                    )
                );
            }
        }

        if (!empty($profissionais)) {
            Notificacao::enviaNotificacao($profissionais, $conteudo, $parametros, $icone, $link, $cor);
        }

    }


    /**
     * Inclui os parâmetros do comunicado no texto do mesmo
     * @param string $conteudo conteudo da notificação/email
     */
    public static function getConteudoComunicado($chaveTexto, $parametros = array())
    {
        if (isset(self::$TEXTOS_COMUNICADO[$chaveTexto])) {
            $texto = self::$TEXTOS_COMUNICADO[$chaveTexto];
        } else {
            $texto = $chaveTexto;
        }
        return HTexto::bindValues($texto, $parametros);
    }

}