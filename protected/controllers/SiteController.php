<?php

/**
 * Controller Default da aplicação. páginas básicas ou estáticas são renderizadas por aqui.
 * @package base.Controllers
 */
class SiteController extends Controller
{

    public $outrasCrumbs = array();

    public function __construct($id, $module = null)
    {
        parent::__construct($id, $module);
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        //  $this->IDAcao = 1;

        $usuario = Usuario::model()->findByPk(Yii::app()->user->IDUsuario)->with('iDProfissional');
        $profissional = $usuario->iDProfissional();
        $this->render('index', array('usuario' => $usuario, 'profissional' => $profissional));
    }

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
        );
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            header("HTTP/1.0 " . $error['code']);
            $this->pageTitle = 'Erro - ' . $error['code'];
            if (!Yii::app()->request->isAjaxRequest) {
                $this->render('error', $error);
                exit();
            }
            echo 'Erro - ' . $error['code'];
        }
    }


    public function actionLogin()
    {
        $this->pageTitle = 'Login';
        //Consulta para a verificar se o dominio digitado e valido
        $schema = Yii::app()->db->createCommand()
            ->selectDistinct('schemaname AS esquema')
            ->from('pg_catalog.pg_tables')
            ->where('schemaname = :cliente', array(':cliente' => CLIENTE))
            ->queryRow();
        //Em caso de o dominio ser inválido a pagina e redirecionada para pagina de erro
        if (!$schema) {
            $this->render('application.views.site.error', array('code' => '404', 'externo' => 'TRUE'));
            exit();
        }

        //Quando o usuario preencher os formulario com seus dados começa o processor para logar
        if (isset($_POST['Login'])) {
            $identity = new UserIdentity($_POST['Login']['username'], $_POST['Login']['password']);
            //função que autentica o usuário
            $identity->authenticate();
            if ($identity->errorCode === UserIdentity::ERROR_NONE) {
                Yii::app()->user->login($identity);
                Yii::app()->user->setState('cliente', $schema['esquema']);
                //Verifica se a senha expirou
                $user = Yii::app()->db->createCommand()
                    ->select('DATE_PART(\'day\', now() - "dt_ultimaTrocaSenhaUsuario") as "periodo_trocaSenha"')
                    ->from(CLIENTE . '.Usuario')
                    ->where("login_usuario = :login", array(':login' => $_POST['Login']['username']))
                    ->queryRow();
                if (isset($user['periodo_trocaSenha'])) {
                    if ($user['periodo_trocaSenha'] >= HAtributos::getLocal('TEMPO_TROCA_SENHA')) {
                        $this->layout = '//layouts/center';
                        Yii::app()->user->setState('senhaExpirada', true);
                        $this->render('application.views.site.senhaExpirada');
                        exit();
                    }
                }
                //fim verificação
                $this->getAcoesUsuario(Yii::app()->user->IDUsuario);
                $this->getModulosProjeto();
                $this->getMenu();

                Yii::app()->request->redirect(
                    Yii::app()->user->returnUrl
                ); //redireciona o usuario para a pagina que estava tentando acessar
            } else {
                //variavel que retornara em caso de mais de 3 tentativas com erro
                //o tempo que aquele usuário ficará sem poder tentar novamente
                $tempo = null;
                //Mensagens de erro para o Login
                switch ($identity->errorCode) {
                    case UserIdentity::ERROR_USER_EXPIRED:
                        $msgErro = 'Conta expirada por tempo';
                        break;

                    case UserIdentity::ERROR_PROJECT_INVALID:
                        $msgErro = 'Esse projeto expirou ou está desativado';
                        break;

                    case UserIdentity::ERROR_USER_DISABLED:
                        $msgErro = 'Conta desativada';
                        break;

                    case UserIdentity::ERROR_USERNAME_INVALID:
                        $msgErro = 'Usuário ou senha incorretos';
                        break;

                    case UserIdentity::ERROR_PASSWORD_INVALID:
                        $msgErro = 'Usuário ou senha incorretos';
                        break;

                    case UserIdentity::ERROR_USER_BLOCKED:
                        $msgErro = 'Seu usuário está bloqueado pelos próximos: ';
                        $tempo = $identity->getTempoBloqueio();
                        break;

                    default:
                        $msgErro = 'Usuário ou senha incorretos';
                        break;
                }
                $this->layout = '//layouts/center';
                $this->render(
                    'login',
                    array(
                        'msgErro' => $msgErro,
                        'tempo' => $tempo
                    )
                );
                exit(0);
            }
        }
        $esqueciMinhaSenha = new FEsqueciMinhaSenha();
        if (isset($_POST['FEsqueciMinhaSenha'])) {
            $esqueciMinhaSenha->attributes = $_POST['FEsqueciMinhaSenha'];
            if ($esqueciMinhaSenha->validate()) {
                unset($_POST['FEsqueciMinhaSenha']);
                $esqueciMinhaSenha->message = '<div class="alert alert-success">Uma mensagem '
                    . 'foi enviada para o email fornecido com informações sobre o procedimento para troca da senha.</div>';

                $profissional = Profissional::model()->with('iDUsuario')->findByAttributes(
                    array('email_profissional' => $esqueciMinhaSenha->emailUsuario)
                );
                if (isset($profissional)) {
                    if (isset($profissional->iDUsuario)) {
                        $usuario = $profissional->iDUsuario;
                        $senha = $esqueciMinhaSenha->changePassword($usuario);
                        if ($senha != null) {
                            $console = new CConsole();
                            $console->runCommand(
                                'email esqueciMinhaSenha',
                                array('--profissional=' . $profissional->IDProfissional, '--senha=' . $senha)
                            );
                        }
                    }
                }
            }
        }
        $this->layout = '//layouts/center';
        $this->render('login', array('esqueciMinhaSenha' => $esqueciMinhaSenha));
    }


    public function actionLogout()
    {
        Yii::app()->user->logout(); //Onde o usuario realmente e deslogado
        $this->pageTitle = 'Logout';
        $this->actionLogin();
    }

    public function actionFaq()
    {
        $this->outrasCrumbs = array(array('nome' => 'Perguntas Frequentes', 'icone' => 'comment', 'url' => 'site/faq'));
        $faqs = ClasseFaq::model()->with('faqs')->findAll();
        $this->pageTitle = 'Perguntas Frequentes';
        $this->render('faq', array('faqs' => $faqs));
    }


    public function actionContatos()
    {
        $this->outrasCrumbs = array(array('nome' => 'Contato', 'icone' => 'phone', 'url' => ''));
        $this->pageTitle = 'Entre em Contato';
        $this->render('contatos');
    }

    public function actionMudaEmpregador()
    {
        $IDEmpregador = null;
        if (empty($_POST['IDEmpregador'])) {
            Yii::app()->user->setState('IDEmpregador', $IDEmpregador);
            return;
        }
        if (Profissional::temEmpregador(($_POST['IDEmpregador']))) {
//            $IDEmpregador = $_POST['IDEmpregador'];
            Yii::app()->user->setState('IDEmpregador', $_POST['IDEmpregador']);
            echo $this->widget(
                'application.widgets.WContatoEmpregador',
                array(
                    'view' => 'ContatoEmpregadorHeader',
                    'IDEmpregador' => $IDEmpregador
                ),
                true
            );
        } else {
            throw new CHttpException(403, 'Você não tem acesso à este empregador.');
        }
    }


    public function actionGetFlashes()
    {
        echo CJSON::encode((Yii::app()->user->getFlashes(true)));
    }


    public function actionAjuda()
    {
        $this->pageTitle = 'Ajuda';
        $this->outrasCrumbs = array(array('nome' => 'Ajuda', 'icone' => 'question', 'url' => 'site/ajuda'));
        $this->render('ajuda');
    }


    public function actionHistoricoVersionamento()
    {
        $this->pageTitle = 'Histórico de Versionamento';
        $this->outrasCrumbs = array(
            array(
                'nome' => 'Histórico de Versionamento',
                'icone' => 'desktop',
                'url' => 'site/historicoVersionamento'
            )
        );
        $this->render('historicoVersionamento');
    }


    public function actionContato($IDProfissional)
    {
        if (Yii::app()->request->isAjaxRequest) {

            $msg = $_REQUEST['msg'];

            if (!empty($msg)) {
                $console = new CConsole();
                $console->runCommand('email contato', array('--id=' . $IDProfissional, ' --msg=' . $msg));
            }

            echo CJSON::encode(array('error' => false));
        }
    }

    public function actionTesteStress()
    {
        $trabalhadors = Trabalhador::model()->findAll();
        $empregadors = Empregador::model()->findAll();
    }
}
