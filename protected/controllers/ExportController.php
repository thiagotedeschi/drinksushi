<?php
//aumenta o max_input_vars para poder processar mais registros 
putenv('max_input_vars = 5000');


class ExportController extends Controller
{

    public function actionExport()
    {
        $formato = (isset($_POST['target'])) ? $_POST['target'] : '';
        if ($formato != '') {
            $this->$formato();
        } //chama dinamicamente o método certo para exportar no formato especificado
        else {
            die("erro");
        }
    }

    public function PDF()
    {
        $tempName = uniqid('pdf_') . '.pdf';
        //cria um arquivo temporário que será excluído assim que lido
        $temp = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . $tempName;
        $title = (isset($_POST['title']) && $_POST['title'] != null ? $_POST['title'] : $tempName);

        $exportPdf = PExport::doc();
        $exportPdf->setData(array('title' => $_POST['title'], 'data' => $_POST));
        $exportPdf->output('F', $temp);

        echo $this->createUrl('view', array('temp' => $tempName, 'type' => 'pdf', 'title' => $title));
        Yii::app()->end();
    }


    public function XML()
    {
        echo __METHOD__;
    }


    public function CSV()
    {
        $tempName = uniqid('csv_') . '.pdf';
        //cria um arquivo temporário que será excluído assim que lido
        $f = fopen(Yii::app()->basePath . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . $tempName, 'w+');

        // Imprimindo os cabeçalhos
        if (isset($_POST['header'])) {
            fputcsv($f, $_POST['header'], ";");
        }

        if (isset($_POST['rows'])) {
            foreach ($_POST['rows'] as $line) {
                if (is_array($line)) //somente tentar imprimir se o resultado for um array
                {
                    fputcsv($f, $line, ";");
                }
            }
        }
        fclose($f); //fecha a conexão com o arquivo para economizar memória
        $title = (isset($_POST['title']) && $_POST['title'] != null ? $_POST['title'] : $tempName);
        echo $this->createUrl('view', array('temp' => $tempName, 'type' => 'csv', 'title' => $title));
        Yii::app()->end();
    }

    public function actionView($temp, $title, $type)
    {
        //headers padrão para o navegador entender qual é o tipo de arquivo que será gerado
        header('Content-Type: application/' . $type . '; charset=utf-8');
        header('Content-Disposition: attachement; filename="' . $title . '.' . $type . '"');
        header('Content-Transfer-Encoding: binary');

        //path padrão para arquivos temporários
        $path = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'runtime' . DIRECTORY_SEPARATOR . $temp;
        //se o arquivo realmente existir e for um arquivo
        if (file_exists($path) && is_file($path)) {
            echo $type === 'csv' ? "\xEF\xBB\xBF" : null; // hack maldito pra codificação no Excel
            echo file_get_contents($path);
            unlink($path);
        } else {
            echo 'erro';
        }
        Yii::app()->end();
    }

}

?>
