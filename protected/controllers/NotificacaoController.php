<?php


class NotificacaoController extends Controller
{

    /**
     * Action default, não faz nada além de renderizar uma view default gerada pelo Gii
     */
    public function actionIndex()
    {
        $this->render('index');
    }


    public function actionLer($id)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $notificacao = Notificacao::model()->findByPk($id);
            if ($notificacao->dt_leituraNotificacao == '') {
                $notificacao->dt_leituraNotificacao = new CDbExpression('CURRENT_TIMESTAMP(0)'); //date('y-m-d H:i:s');
                $notificacao->save();
            }
        }
    }

}