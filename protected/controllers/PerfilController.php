<?php
/**
 * @package base.Controllers
 */
class PerfilController extends Controller
{

    public $outrasCrumbs = array(array('nome' => 'Perfil', 'icone' => 'user', 'url' => ''));

//    public $IDAcao = '1';
    public function actionIndex($IDUsuario = '')
    {
        $IDUsuario != null ? : $IDUsuario = Yii::app()->user->IDUsuario;
        if ($IDUsuario !== '') {
            $usuario = Usuario::model()->with('iDProfissional')->findByPk($IDUsuario);
            if ($usuario != null) {
                $profissional = Profissional::model()->with('iDProfissao')->findByPk(
                    $usuario->IDProfissional
                ); //$usuario->iDProfissional();
                $this->render('index', array('usuario' => $usuario, 'profissional' => $profissional));
            } else {
                throw new CHttpException(404);
            }
        } else {
            throw new CHttpException(404);
        }
    }

    /**
     * Muda os dados do profissional via perfil.
     * Existirá outra action de update do profissional, só que bem mais completa
     * Esta ação apenas trabalha com os dados que o proprio profissional pode alterar
     * @param int $id identificador do profissional
     */
    public function actionUpdateProfissional($id)
    {
        $model = Profissional::model()->findByPk($id);
        $model->scenario = "profile";
        $endereco = EnderecoProfissional::model()->findByPk($model->IDEndereco);
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'profissional-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['EnderecoProfissional'][1])) {
            $endereco->attributes = $_POST['EnderecoProfissional'][1];
            if ($endereco->save()) {
                $model->IDEndereco = $endereco->IDEndereco;
            }
        }

        if (isset($_POST['Profissional'])) {
            $model->attributes = $_POST['Profissional'];
            $model->receber_emails = (int)$_POST['Profissional']['receber_emails'];
            if ($model->save()) {
                $this->redirect(Yii::app()->createUrl('/perfil'));
            }
            $this->redirect(Yii::app()->createUrl('/perfil'));
        }
    }

}
