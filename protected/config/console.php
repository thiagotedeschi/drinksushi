<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Ambiente de Homologacao',
    'language' => 'pt_br',
    // preloading 'log' component
    'preload' => array('log'),
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.components.helpers.*',
        'application.commands.*',
    ),
    // application components
    'components' => array(
        'session' => array(
            'class' => 'CHttpSession',
            'timeout' => '100',
            'autoStart' => 'false',
            'cookieMode' => 'only'
        ),
        'db' => include 'bd.php',
        'Smtpmail' => include 'email.php',
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    'commandMap' => array(
        'migrate' => array(
            'class' => 'system.cli.commands.MigrateCommand',
            'migrationPath' => 'application.migrations',
            'migrationTable' => 'tbl_migration',
            'connectionID' => 'db',
//            'templateFile' => 'application.migrations.template',
        ),
    ),
);