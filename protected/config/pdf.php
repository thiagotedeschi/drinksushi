<?php
return array(
    'class' => 'ext.pdffactory.EPdfFactory',
    //the cache duration
    'cacheHours' => -1, //-1 = cache disabled, 0 = never expires, hours if >0

    //The alias path to the directory, where the pdf files should be created
    'pdfPath' => 'application.runtime',
    'tcpdfOptions' => array(
        'format' => 'A4',
        'orientation' => 'P', //=Portrait or 'L' = landscape
        'unit' => 'mm', //measure unit: mm, cm, inch, or point
        'unicode' => true,
        'encoding' => 'UTF-8',
        'diskcache' => false,
        'pdfa' => false,

    )
);
