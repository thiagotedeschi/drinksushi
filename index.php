<?php
mb_internal_encoding('UTF8');
// Se necessário, mude o Path para o core do Yii//
$yii = '../yii-core/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';



//remove the following lines when in production mode//
defined('YII_DEBUG') or define('YII_DEBUG', true);

/** * Cliente que está tentando acessar o sistema */
//define('CLIENTE', explode('.', $_SERVER["HTTP_HOST"])[0]);
define('CLIENTE', 'drink');

if (CLIENTE == 'public' || CLIENTE == 'information_schema' || CLIENTE == 'pg_catalog') {
    header("Status: 404 not found");
    header("location: erro.php");
    exit(0);
}
define('ASSETS_LINK', 'http://' . CLIENTE . '.localhost/Homologacao/assets/');

/** * Link principal d0a aplicação */
define('MAIN_LINK', 'http://' . CLIENTE . '.localhost/Homologacao/');

/** * Caminho para a raiz do yii */
define('PATH', 'C:\wamp\www\Homologacao\\');

// ***fim das definições personalizadas*** \\


// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
//Inclui a classe Yii na aplicação
require_once($yii);
//Cria a aplicação com a configuração setada em protected/config/main.php

Yii::createWebApplication($config)->run();


